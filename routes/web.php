<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/send-notification', array('as' => 'send-notification', 'uses' => 'HomeController@send_notification'));

Route::get('auth/google', array('as' => 'auth.google', 'uses' => 'GoogleController@redirectToGoogle'));
Route::get('auth/google/callback', array('as' => 'auth.google.callback', 'uses' => 'GoogleController@handleGoogleCallback'));

/*=================================================== Web Route Start =============*/
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('blog-details/{slug}', array('as' => 'blog-details', 'uses' => 'HomeController@blog_details'));
Route::get('faq', array('as' => 'faq', 'uses' => 'HomeController@faq'));
Route::get('legal-information', array('as' => 'legal-information', 'uses' => 'HomeController@legal_information'));
Route::get('offer-just-for-you-list', array('as' => 'offer-just-for-you-list', 'uses' => 'HomeController@offer_just_list'));
Route::get('offer-just-for-you-details/{slug}', array('as' => 'offer-just-for-you-details', 'uses' => 'HomeController@offer_just_details'));


Route::get('/home', array('as' => 'home', 'uses' => 'HomeController@index'));
   Route::get('invoice/{id}','Api\OrderController@invoice_create');
 
Route::get('/upload-prescription', array('as' => 'upload-prescription', 'uses' => 'UserDashboardController@upload_prescription'));
Route::post('/save-upload-prescription', array('as' => 'save-upload-prescription', 'uses' => 'UserDashboardController@upload_prescription_save'));


Route::get('/list/{cate_slug}/{sub_cate_slug?}', array('as' => 'list', 'uses' => 'ProductController@product_list'));

Route::post('/add-to-wishlist', array('as' => 'wishlist', 'uses' => 'ProductController@add_wishlist'));
Route::post('/add-subscription-plan', array('as' => 'add-subscription-plan', 'uses' => 'CartController@add_subscription_plan'));

Route::get('/product-details/{slug}', array('as' => 'list', 'uses' => 'ProductController@product_details'));
Route::get('product-ajax', array('as' => 'product-ajax', 'uses' => 'ProductController@product_ajax'));
Route::post('brand-ajax', array('as' => 'brand-ajax', 'uses' => 'ProductController@brand_search'));
Route::post('manufacturer-ajax', array('as' => 'manufacturer-ajax', 'uses' => 'ProductController@manufacturers_search'));

//Route::post('sorting-ajax', array('as' => 'sorting-ajax', 'uses' => 'ProductController@product_ajax'));

Route::post('newsletter', array('as' => 'newsletters','uses' => 'HomeController@newsletter'));
Route::get('products/autocompleteajax','HomeController@autoCompleteAjax');

Route::get('/add-new-product-by-doctor/{slug}/{id}/{order_id}', array('as' => 'add-new-product-by-doctor', 'uses' => 'ProductController@NewProductOrderAddByDoctor'));

Route::get('/extra-product-payment/{order_id}', array('as' => 'extra-product-payment', 'uses' => 'ProductController@extra_product_payment'));


/*Cart*/
    Route::get('cart', array('as' => 'cart', 'uses' => 'CartController@index'));
    Route::post('add-to-cart', array('as' => 'add-to-cart', 'uses' => 'CartController@addToCart'));
    Route::post('re-order', array('as' => 're-order', 'uses' => 'CartController@ReorderaddToCart'));
    Route::post('update-cart', array('as' => 'update-cart', 'uses' => 'CartController@update_cart'));
    Route::post('remove-from-cart', array('as' => 'remove-from-cart', 'uses' => 'CartController@remove'));

    Route::post('remove-plan', array('as' => 'remove-plan', 'uses' => 'CartController@remove_plan'));
   // Route::post('re-order', array('as' => 're-order', 'uses' => 'CartController@ReorderaddToCart'));

    Route::post('clear-cart', array('as' => 'clear-cart', 'uses' => 'CartController@clear_cart'));
    Route::get('clearcart', array('as' => 'clearcart', 'uses' => 'CartController@clear_cart'));
    Route::post('promocode-calculation', array('as' => 'promocode-calculation', 'uses' => 'CartController@promocode_calculation'));
    Route::post('save-for-later', array('as' => 'save-for-later', 'uses' => 'CartController@save_for_later'));
    Route::post('coupon-minimum-value', array('as' => 'coupon-minimum-value', 'uses' => 'CartController@coupon_minimum_value'));

/*Cart End*/
 

 /*=================================================================*/
 
//Register Vendor
Route::get('vendor/account/register', array('as' => 'vendor.account.register', 'uses' => 'Vendor\LoginController@register'));
    Route::post('vendor/account/signup', array('as' => 'vendor.account.signup.post', 'uses' => 'Vendor\LoginController@do_register'));

//Register Docter
Route::get('doctor/account/register', array('as' => 'doctor.account.register', 'uses' => 'Doctor\DoctorRegisterController@register_view'));
    Route::post('doctor/account/signup', array('as' => 'doctor.account.signup.post', 'uses' => 'Doctor\DoctorRegisterController@do_register'));
 /*=================================================================*/

/*Customer Login & Register Routes*/
Route::group(['prefix' => 'customer'], function () {
    Route::get('account/login', array('as' => 'account.login', 'uses' => 'LoginController@index'));
    Route::post('account/sendotp', array('as' => 'account.sendotp.post', 'uses' => 'LoginController@sendOtp'));

    Route::post('account/login', array('as' => 'account.login.post', 'uses' => 'LoginController@login'));

    Route::post('account/register', array('as' => 'account.register.post', 'uses' => 'LoginController@doRegister'));

    Route::post('account/session-forgot', array('as' => 'account.session-forgot.post', 'uses' => 'LoginController@destroy_sessions'));

});
/*Page Manager Module*/
Route::get('p/{slug}', array('as' => 'staticpages','uses' => 'HomeController@staticpages'));

/*User Route*/
Route::group(['prefix'=>'/user','middleware'=>['user_check']],function(){

    Route::post('change-password', array('as' => 'user.change-password', 'uses' => 'UserDashboardController@change_password'));
    Route::get('prescription', array('as' => 'user.prescription', 'uses' => 'UserDashboardController@user_prescription'));

    Route::get('subscription', array('as' => 'user.subscription', 'uses' => 'UserDashboardController@subscription'));
    Route::get('subscription-plan', array('as' => 'user.subscription-plan', 'uses' => 'UserDashboardController@subscription_plan'));


    Route::get('profile', array('as' => 'user.profile', 'uses' => 'UserDashboardController@index'));
    Route::post('update_profile_user', array('as' => 'user.update_profile_user', 'uses' => 'UserDashboardController@update_profile_user'));
    //Delivery Address
    Route::get('addresses', array('as' => 'user.addresses', 'uses' => 'UserDashboardController@addresses'));
    //delete prescription
    Route::post('deletePrescription/{id}', array('as' => 'user.prescription.delete', 'uses' => 'UserDashboardController@prescription_delete'));

    Route::post('deleteDeliveryAddress/{id}', array('as' => 'user.deleteDeliveryAddress', 'uses' => 'UserDashboardController@DeleteDeliveryAddress'));
       // Delivery Address Update
        Route::get('edit/{id}', array('as' => 'user.edit', 'uses' => 'UserDashboardController@edit'));

        Route::post('edit/{id}', array('as' => 'user.edit.post', 'uses' => 'UserDashboardController@update'));

    Route::get('add-new-address', array('as' => 'user.add-new-address', 'uses' => 'UserDashboardController@add_delivery_address'));
    Route::post('save-delivery-address', array('as' => 'user.save-delivery-address', 'uses' => 'UserDashboardController@save_delivery_address'));
     
    //Patient Info
    Route::post('save-patient-info', array('as' => 'user.save-patient-info', 'uses' => 'UserDashboardController@save_patient_info')); 
    Route::get('patient-manage', array('as' => 'user.patient-manage', 'uses' => 'UserDashboardController@patient_manage')); 

    Route::post('select-address', array('as' => 'user.select-address', 'uses' => 'UserDashboardController@selecte_address'));
    Route::post('select-patient', array('as' => 'user.select-patient', 'uses' => 'UserDashboardController@selecte_patient'));
    /*Payment Methode*/
    Route::get('payment-method', array('as' => 'user.payment-method', 'uses' => 'UserDashboardController@payment_method'));
    Route::post('payment-method', array('as' => 'user.payment-method.post', 'uses' => 'UserDashboardController@payment_method_save'));
    //delete
     Route::post('deletePayment/{id}', array('as' => 'user.deletePayment', 'uses' => 'UserDashboardController@Delete'));
       //Update
     Route::post('payment-method-edit/{id}', array('as' => 'user.payment-method-edit.post', 'uses' => 'UserDashboardController@payment_edit'));

          /*Notification*/
    Route::get('notification', array('as' => 'user.notification', 'uses' => 'UserDashboardController@notification'));
     //legal-information user 
    Route::get('legal-information', array('as' => 'user.legal-information', 'uses' => 'UserDashboardController@legal_information'));

     Route::post('view_all/{id}', array('as' => 'user.view-all', 'uses' => 'UserDashboardController@view_all'));

    Route::get('orders', array('as' => 'user.orders', 'uses' => 'UserDashboardController@order_list'));
    Route::get('order-detail/{id}', array('as' => 'user.order-detail.', 'uses' => 'UserDashboardController@order_details'));
    Route::get('save-for-later', array('as' => 'user.save-for-later', 'uses' => 'UserDashboardController@save_for_later'));
    //Route::get('save-for-later', array('as' => 'user.save-for-later', 'uses' => 'UserDashboardController@save_for_later'));
    Route::post('save_for_later_remove', array('as' => 'user.save_for_later_remove', 'uses' => 'UserDashboardController@save_for_later_remove'));

    Route::get('wishlist', array('as' => 'user.wishlist', 'uses' => 'UserDashboardController@wishlist'));
    Route::post('wishlist_remove', array('as' => 'user.wishlist_remove', 'uses' => 'UserDashboardController@wishlist_remove'));
    
    Route::get('logout', array('as' => 'logout.post', 'uses' => 'LoginController@logout'));
    Route::get('refer-earn', array('as' => 'user.refer-earn', 'uses' => 'UserDashboardController@refer_earn'));
    Route::get('contact-us', array('as' => 'contact-us','uses' => 'UserDashboardController@contact_us'));
    Route::post('save-contact-us', array('as' => 'user.save-contact-us','uses' => 'UserDashboardController@save_contact_us'));
    Route::get('wallet', array('as' => 'user.wallet', 'uses' => 'UserDashboardController@wallet'));




});

Route::group(['middleware'=>['user_check']],function(){
    Route::get('order-review', array('as' => 'order-review', 'uses' => 'CartController@order_review'));
     Route::post('customer-notes', array('as' => 'customer-notes', 'uses' => 'CartController@customer_notes'));
    //delete Patient info Address
    Route::post('deletePatientAddress/{id}', array('as' => 'deletePatientAddress', 'uses' => 'CartController@DeletePatientAddress'));
    Route::get('checkout', array('as' => 'checkout', 'uses' => 'CheckoutController@index'));
    Route::post('delivery_address_check', array('as' => 'delivery_address_check', 'uses' => 'CheckoutController@delivery_address_check'));
    //Check Refrel Code
    Route::post('check_referral_code', array('as' => 'check_referral_code', 'uses' => 'CheckoutController@check_referral_code'));
    Route::post('remove_referrer_code', array('as' => 'remove_referrer_code', 'uses' => 'CheckoutController@remove_referrer_code'));
    Route::post('check_offer_code', array('as' => 'check_offer_code', 'uses' => 'CheckoutController@check_offer_code'));
    Route::post('remove_offer_code', array('as' => 'remove_offer_code', 'uses' => 'CheckoutController@remove_offer_code'));

    //Place Order
    Route::post('place-order', array('as' => 'place-order', 'uses' => 'OrderController@place_orders'));
    Route::post('extra-place-order', array('as' => 'extra-place-order', 'uses' => 'OrderController@extraProductPlaceOrder'));
    Route::post('order-cancel', array('as' => 'order-cancel', 'uses' => 'OrderController@order_cancel'));
});





  Route::post('get-details', array('as' => 'delivery-location.get-details.post', 'uses' => 'Admin\DeliveryLocationController@getDetailsByPinCode'));


 


/*Vendor Login & Register Routes*/
Route::group(['prefix'=>'/vendor','middleware'=>['vendor_check']],function(){
    //Dashboard
    Route::get('my-dashboard', array('as' => 'dashboard', 'uses' => 'Vendor\DashboardController@index'));
    //My Order
    Route::get('orders', array('as' => 'orders', 'uses' => 'Vendor\DashboardController@order'));
    Route::get('accept-orders', array('as' => 'accept-orders', 'uses' => 'Vendor\DashboardController@accepted_orders'));
    Route::get('order-validate/{id}', array('as' => 'order-validate', 'uses' => 'Vendor\DashboardController@order_validate'));
    Route::get('accept-order-detail/{id}', array('as' => 'accept-order-detail', 'uses' => 'Vendor\DashboardController@accept_order_detail'));
    Route::get('profile', array('as' => 'profile', 'uses' => 'Vendor\DashboardController@profile'));
    Route::get('manage-device', array('as' => 'manage-device', 'uses' => 'Vendor\VendorProductController@manage_device'));
    //Add vendor products
    Route::post('add-vendor-product', array('as' => 'add-vendor-product', 'uses' => 'Vendor\VendorProductController@add_vendor_product'));
    Route::post('destroy/{id}', array('as' => 'destroy', 'uses' => 'Vendor\VendorProductController@destroy'));
    
    Route::post('fetchSub_category', array('as' => 'fetchSub_category', 'uses' => 'Vendor\VendorProductController@fetchSub_category'));
    //Subscription 
    Route::get('subscription', array('as' => 'subscription', 'uses' => 'Vendor\DashboardController@subscription'));

    Route::get('subscription-plan', array('as' => 'document.subscription-plan', 'uses' => 'Vendor\DashboardController@subscription_plan'));
  
    //Order Accepted
    Route::post('accepte-orders', array('as' => 'accepte-orders', 'uses' => 'Vendor\DashboardController@accepte_orders'));
    //Order Accepted
    Route::post('decline-orders', array('as' => 'decline-orders', 'uses' => 'Vendor\DashboardController@decline_orders'));
    Route::post('updateVendorOrderProductStatus', array('as' => 'updateVendorOrderProductStatus', 'uses' => 'Vendor\DashboardController@updateVendorOrderProductStatus'));
        Route::get('logoutvendor', array('as' => 'logoutvendor.vendor', 'uses' => 'LoginController@logout_vendor'));
            // Document Upload
    Route::post('document', array('as' => 'vendor.document', 'uses' => 'Vendor\DashboardController@document')); 

    //vendor setting change password and edit deatails
     Route::get('vendor-setting', array('as' => 'vendor.setting', 'uses' => 'Vendor\DashboardController@setting_index')); 

    Route::post('update_profile_vendor', array('as' => 'vendor.update_profile_vendor', 'uses' => 'Vendor\DashboardController@update_profile_vendor'));

     Route::post('change-password', array('as' => 'vendor.change-password', 'uses' => 'Vendor\DashboardController@change_password'));

  // vendor availability 
    Route::post('vendor-availablity', array('as' => 'vendor-availablity', 'uses' => 'Vendor\DashboardController@availablity'));
       /*Notification*/
    Route::get('notification', array('as' => 'vendor.notification', 'uses' => 'Vendor\DashboardController@notification'));
    //Edit profile
    Route::post('edit_profile_vendor', array('as' => 'vendor.edit_profile_vendor', 'uses' => 'Vendor\DashboardController@edit_profile_vendor'));
    //view all notification
    Route::post('view_all/{id}', array('as' => 'vendor.view-all', 'uses' => 'Vendor\DashboardController@view_all'));



});


/*Doctor Login & Register Routes*/
Route::group(['prefix'=>'/doctor','middleware'=>['doctor_check']],function(){
    //
    Route::post('update_profile_doctor', array('as' => 'doctor.update_profile_doctor', 'uses' => 'Doctor\DoctorController@update_profile_user'));
    Route::post('change-password-doctor', array('as' => 'doctor.change-password-doctor', 'uses' => 'Doctor\DoctorController@change_password'));
     Route::post('bank-details', array('as' => 'doctor.bank-details', 'uses' => 'Doctor\DoctorController@bank_details'));

    //Dashboard
    Route::get('setting', array('as' => 'setting', 'uses' => 'Doctor\DoctorController@setting'));
    Route::post('completedConsultation', array('as' => 'completedConsultation', 'uses' => 'Doctor\DoctorController@completedConsultation'));
    //Dashboard
    Route::get('my-dashboard', array('as' => 'dashboard', 'uses' => 'Doctor\DoctorController@index'));
     //My Order
    Route::get('orders', array('as' => 'orders', 'uses' => 'Doctor\DoctorController@order'));
    Route::post('decline-orders', array('as' => 'decline-orders', 'uses' => 'Doctor\DoctorController@decline_orders'));
    //All Orders Accepted
    Route::post('accepte-orders', array('as' => 'accepte-orders', 'uses' => 'Doctor\DoctorController@accepte_orders'));
    //Order Prescription
    Route::get('order-prescription/{id}', array('as' => 'order-prescription', 'uses' => 'Doctor\DoctorController@order_prescription'));
//Update items 
    Route::post('update-product-items', array('as' => 'update-product-items', 'uses' => 'Doctor\DoctorController@update_product_items'));
//Update Dose
    Route::post('update-product-doses', array('as' => 'update-product-doses', 'uses' => 'Doctor\DoctorController@update_product_doses'));
    //Save Order Prescription
    Route::post('save-order-prescription', array('as' => 'save-order-prescription', 'uses' => 'Doctor\DoctorController@save_order_prescription'));
    //LogOut
    Route::get('logoutdoctor', array('as' => 'logoutdoctor.doctor', 'uses' => 'LoginController@logoutdoctor'));
    //Get auto data
    Route::get('autocomplet-search-product', array('as' => 'autocomplet_search', 'uses' => 'Doctor\DoctorController@autocomplet_search'));
    //Here is info
    Route::post('add-medicine-extra', array('as' => 'add-medicine-extra', 'uses' => 'Doctor\DoctorController@add_medicine_extra'));

    Route::post('send-request-to-user', array('as' => 'send-request-to-user', 'uses' => 'Doctor\DoctorController@sendRequestToUser'));
    /*Completed Consutatino*/
   // Route::post('completedConsultation', array('as' => 'completedConsultation', 'uses' => 'Doctor\DoctorController@completedConsultation '));
    //Remove medicens here
    Route::post('remove-product-items', array('as' => 'remove-product-items', 'uses' => 'Doctor\DoctorController@remove_product_items'));
        // Doctor availability 
    Route::post('doctor-availablity', array('as' => 'doctor-availablity', 'uses' => 'Doctor\DoctorController@availablity'));
           /*Notification*/
    Route::get('notification', array('as' => 'notification', 'uses' => 'Doctor\DoctorController@notification'));
        //view all notification
    Route::post('view_all/{id}', array('as' => 'doctor.view-all', 'uses' => 'Doctor\DoctorController@view_all'));

     // Document Upload
    Route::post('document', array('as' => 'doctor.document', 'uses' => 'Doctor\DoctorController@document')); 

});


/*============================== Web Route End =============*/


/*===================================== Admin Routes Section =====================================*/
/*============= Login Route Start =============*/
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', array('as' => 'admin.login', 'uses' => 'Admin\LoginController@index'));
    Route::get('login', array('as' => 'admin.login', 'uses' => 'Admin\LoginController@index'));
    Route::post('login', array('as' => 'admin.login.post', 'uses' => 'Admin\LoginController@doLogin'));
});

    Route::get('logout', array('as' => 'admin.logout.post', 'uses' => 'Admin\LoginController@logout'));

/*============= Dashboard Route Start =============*/
Route::group(['prefix'=>'/admin','middleware'=>['admin.auth']],function(){

Route::get('dashboard', array('as' => 'admin.dashboard', 'uses' => 'Admin\LoginController@dashboard'));


/*============= Profile Route Start =============*/


Route::get('profile', array('as' => 'profile', 'uses' => 'Admin\ProfileController@index'));
Route::post('change-password', array('as' => 'change.password', 'uses' => 'Admin\ProfileController@change_password'));


 //Edit
Route::get('edit/{id}', array('as' => 'user.edit', 'uses' => 'Admin\UserController@edit'));
Route::post('edit/{id}', array('as' => 'user.edit.post', 'uses' => 'Admin\UserController@update'));

/*Role ===========*/
Route::resource('roles', 'Admin\RolesController');
Route::delete('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');


/*============= Category Route Start =============*/
Route::group(array('prefix' => 'category'), function () {
        //View
        Route::get('list', array('as' => 'admin.category.list', 'uses' => 'Admin\CategoryController@index'));
          //sub-category
    Route::get('sub-catgory/{id}', array('as' => 'admin.category.sub_catgory', 'uses' => 'Admin\CategoryController@sub_catgory'));

    Route::get('data/{id}', array('as' => 'admin.category.sub-category.list.data', 'uses' => 'Admin\CategoryController@data_sub_cat'));
         //Insert
        Route::get('add', array('as' => 'admin.category.add', 'uses' => 'Admin\CategoryController@create'));
        Route::post('add', array('as' => 'admin.category.add.post', 'uses' => 'Admin\CategoryController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.category.edit', 'uses' => 'Admin\CategoryController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.category.edit.post', 'uses' => 'Admin\CategoryController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.category.delete.delete-confirm', 'uses' => 'Admin\CategoryController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.category.delete', 'uses' => 'Admin\CategoryController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.category.updatestatus.update-status-confirm', 'uses' => 'Admin\CategoryController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.category.updatestatus', 'uses' => 'Admin\CategoryController@update_status'));
});



/*============= Page Route Start =============*/
Route::group(array('prefix' => 'setting'), function () {
        //View
        Route::get('list', array('as' => 'admin.setting.list', 'uses' => 'Admin\SettingController@index'));

        Route::POST('update-general-setting', array('as' => 'admin.update.general.setting', 'uses' => 'Admin\SettingController@update_general_setting'));

        Route::POST('update-site-setting', array('as' => 'admin.update.site-setting.setting', 'uses' => 'Admin\SettingController@update_site_setting_setting'));

        Route::POST('update-payment-setting', array('as' => 'admin.update.payment-setting.setting', 'uses' => 'Admin\SettingController@update_payment_setting'));

        Route::POST('update-push-notification', array('as' => 'admin.update.push-notification.setting', 'uses' => 'Admin\SettingController@update_push_notification'));

});

/*============= Page Route Start =============*/
Route::group(array('prefix' => 'page'), function () {
        //View
        Route::get('list', array('as' => 'admin.page.list', 'uses' => 'Admin\PageController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.page.add', 'uses' => 'Admin\PageController@create'));
        Route::post('add', array('as' => 'admin.page.add.post', 'uses' => 'Admin\PageController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.page.edit', 'uses' => 'Admin\PageController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.page.edit.post', 'uses' => 'Admin\PageController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.page.delete.delete-confirm', 'uses' => 'Admin\PageController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.page.delete', 'uses' => 'Admin\PageController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.page.updatestatus.update-status-confirm', 'uses' => 'Admin\PageController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.page.updatestatus', 'uses' => 'Admin\PageController@update_status'));
});

/*============= manufacturer Route Start =============*/
Route::group(array('prefix' => 'manufacturer'), function () {
        //View
        Route::get('list', array('as' => 'admin.manufacturer.list', 'uses' => 'Admin\ManufacturerController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.manufacturer.add', 'uses' => 'Admin\ManufacturerController@create'));
        Route::post('add', array('as' => 'admin.manufacturer.add.post', 'uses' => 'Admin\ManufacturerController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.manufacturer.edit', 'uses' => 'Admin\ManufacturerController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.manufacturer.edit.post', 'uses' => 'Admin\ManufacturerController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.manufacturer.delete.delete-confirm', 'uses' => 'Admin\ManufacturerController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.manufacturer.delete', 'uses' => 'Admin\ManufacturerController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.manufacturer.updatestatus.update-status-confirm', 'uses' => 'Admin\ManufacturerController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.manufacturer.updatestatus', 'uses' => 'Admin\ManufacturerController@update_status'));
});


/*============= Banner Route Start =============*/
Route::group(array('prefix' => 'banner'), function () {
        //View
        Route::get('list', array('as' => 'admin.banner.list', 'uses' => 'Admin\BannerController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.banner.add', 'uses' => 'Admin\BannerController@create'));
        Route::post('add', array('as' => 'admin.banner.add.post', 'uses' => 'Admin\BannerController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.banner.edit', 'uses' => 'Admin\BannerController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.banner.edit.post', 'uses' => 'Admin\BannerController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.banner.delete.delete-confirm', 'uses' => 'Admin\BannerController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.banner.delete', 'uses' => 'Admin\BannerController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.banner.updatestatus.update-status-confirm', 'uses' => 'Admin\BannerController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.banner.updatestatus', 'uses' => 'Admin\BannerController@update_status'));
        //Sub cate   
        Route::get('subcat', array('as' => 'admin.subcategory', 'uses' => 'Admin\BannerController@get_sub_category'));
         //Show 
         Route::get('show/{id}', array('as' => 'admin.banner.show', 'uses' => 'Admin\BannerController@show'));


});


/*============= coupon Route Start =============*/
Route::group(array('prefix' => 'coupon'), function () {
        //View
        Route::get('list', array('as' => 'admin.coupon.list', 'uses' => 'Admin\CouponController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.coupon.add', 'uses' => 'Admin\CouponController@create'));
        Route::post('add', array('as' => 'admin.coupon.add.post', 'uses' => 'Admin\CouponController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.coupon.edit', 'uses' => 'Admin\CouponController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.coupon.edit.post', 'uses' => 'Admin\CouponController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.coupon.delete.delete-confirm', 'uses' => 'Admin\CouponController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.coupon.delete', 'uses' => 'Admin\CouponController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.coupon.updatestatus.update-status-confirm', 'uses' => 'Admin\CouponController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.coupon.updatestatus', 'uses' => 'Admin\CouponController@update_status'));
});

/*============= blog Route Start =============*/
Route::group(array('prefix' => 'blog'), function () {
        //View
        Route::get('list', array('as' => 'admin.blog.list', 'uses' => 'Admin\BlogController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.blog.add', 'uses' => 'Admin\BlogController@create'));
        Route::post('add', array('as' => 'admin.blog.add.post', 'uses' => 'Admin\BlogController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.blog.edit', 'uses' => 'Admin\BlogController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.blog.edit.post', 'uses' => 'Admin\BlogController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.blog.delete.delete-confirm', 'uses' => 'Admin\BlogController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.blog.delete', 'uses' => 'Admin\BlogController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.blog.updatestatus.update-status-confirm', 'uses' => 'Admin\BlogController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.blog.updatestatus', 'uses' => 'Admin\BlogController@update_status'));
});

/*============= testimonials Route Start =============*/
Route::group(array('prefix' => 'testimonials'), function () {
        //View
        Route::get('list', array('as' => 'admin.testimonials.list', 'uses' => 'Admin\TestimonialsController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.testimonials.add', 'uses' => 'Admin\TestimonialsController@create'));
        Route::post('add', array('as' => 'admin.testimonials.add.post', 'uses' => 'Admin\TestimonialsController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.testimonials.edit', 'uses' => 'Admin\TestimonialsController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.testimonials.edit.post', 'uses' => 'Admin\TestimonialsController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.testimonials.delete.delete-confirm', 'uses' => 'Admin\TestimonialsController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.testimonials.delete', 'uses' => 'Admin\TestimonialsController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.testimonials.updatestatus.update-status-confirm', 'uses' => 'Admin\TestimonialsController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.testimonials.updatestatus', 'uses' => 'Admin\TestimonialsController@update_status'));
});

/*============= Product Route Start =============*/
Route::group(array('prefix' => 'product'), function () {
        //View
        Route::get('list', array('as' => 'admin.product.list', 'uses' => 'Admin\ProductController@index'));
        Route::get('product-details/{id}', array('as' => 'admin.product.view', 'uses' => 'Admin\ProductController@product_details'));

         //Insert
        Route::get('add', array('as' => 'admin.product.add', 'uses' => 'Admin\ProductController@create'));
        Route::post('add', array('as' => 'admin.product.add.post', 'uses' => 'Admin\ProductController@store'));

        Route::get('import', array('as' => 'admin.product.import', 'uses' => 'Admin\ProductController@import_product_view'));
        
        Route::post('improt-product', array('as' => 'admin.product.import.post', 'uses' => 'Admin\ProductController@import_product'));
        Route::post('store_zip-product', array('as' => 'admin.product.store_zip.post', 'uses' => 'Admin\ProductController@store_zip'));

        Route::get('get-sub-category/{cat_id}', array('as' => 'admin.product.get-subcategory-by-cat', 'uses' => 'Admin\ProductController@sub_category'));




        Route::post('destroy/tag/{tag_id}',  array('as' => 'admin.product.tagdestroy', 'uses' => 'Admin\ProductController@destroytag'));


        //Update
        Route::get('edit/{id}', array('as' => 'admin.product.edit', 'uses' => 'Admin\ProductController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.product.edit.post', 'uses' => 'Admin\ProductController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.product.delete.delete-confirm', 'uses' => 'Admin\ProductController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.product.delete', 'uses' => 'Admin\ProductController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.product.updatestatus.update-status-confirm', 'uses' => 'Admin\ProductController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.product.updatestatus', 'uses' => 'Admin\ProductController@update_status'));

        Route::get('search-product', array('as' => 'admin.product.list.search', 'uses' => 'Admin\ProductController@autocomplet_search'));



});
/*============= Brand Route Start =============*/
Route::group(array('prefix' => 'brand'), function () {
        //View
        Route::get('list', array('as' => 'admin.brand.list', 'uses' => 'Admin\BrandController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.brand.add', 'uses' => 'Admin\BrandController@create'));
        Route::post('add', array('as' => 'admin.brand.add.post', 'uses' => 'Admin\BrandController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.brand.edit', 'uses' => 'Admin\BrandController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.brand.edit.post', 'uses' => 'Admin\BrandController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.brand.delete.delete-confirm', 'uses' => 'Admin\BrandController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.brand.delete', 'uses' => 'Admin\BrandController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.brand.updatestatus.update-status-confirm', 'uses' => 'Admin\BrandController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.brand.updatestatus', 'uses' => 'Admin\BrandController@update_status'));
});

/*============= driver Route Start =============*/
Route::group(array('prefix' => 'driver'), function () {
        //View
        Route::get('list', array('as' => 'admin.driver.list', 'uses' => 'Admin\DriverController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.driver.add', 'uses' => 'Admin\DriverController@create'));
        Route::post('add', array('as' => 'admin.driver.add.post', 'uses' => 'Admin\DriverController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.driver.edit', 'uses' => 'Admin\DriverController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.driver.edit.post', 'uses' => 'Admin\DriverController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.driver.delete.delete-confirm', 'uses' => 'Admin\DriverController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.driver.delete', 'uses' => 'Admin\DriverController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.driver.updatestatus.update-status-confirm', 'uses' => 'Admin\DriverController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.driver.updatestatus', 'uses' => 'Admin\DriverController@update_status'));
            //show 
        Route::get('show/{id}', array('as' => 'admin.driver.show', 'uses' => 'Admin\DriverController@show'));
     
});


/*============= Vendor Route Start =============*/
/*Route::group(array('prefix' => 'vendor'), function () {
        //View
        Route::get('list', array('as' => 'admin.vendor.list', 'uses' => 'Admin\VendorController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.vendor.add', 'uses' => 'Admin\VendorController@create'));
        Route::post('add', array('as' => 'admin.vendor.add.post', 'uses' => 'Admin\VendorController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.vendor.edit', 'uses' => 'Admin\VendorController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.vendor.edit.post', 'uses' => 'Admin\VendorController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.vendor.delete.delete-confirm', 'uses' => 'Admin\VendorController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.vendor.delete', 'uses' => 'Admin\VendorController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.vendor.updatestatus.update-status-confirm', 'uses' => 'Admin\VendorController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.vendor.updatestatus', 'uses' => 'Admin\VendorController@update_status'));
});*/

/*============= Vendor Route Start =============*/
Route::group(array('prefix' => 'vendor'), function () {
        //View
        Route::get('list', array('as' => 'admin.vendor.list', 'uses' => 'Admin\VendorController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.vendor.add', 'uses' => 'Admin\VendorController@create'));
        Route::post('add', array('as' => 'admin.vendor.add.post', 'uses' => 'Admin\VendorController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.vendor.edit', 'uses' => 'Admin\VendorController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.vendor.edit.post', 'uses' => 'Admin\VendorController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.vendor.delete.delete-confirm', 'uses' => 'Admin\VendorController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.vendor.delete', 'uses' => 'Admin\VendorController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.vendor.updatestatus.update-status-confirm', 'uses' => 'Admin\VendorController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.vendor.updatestatus', 'uses' => 'Admin\VendorController@update_status'));
        //Show 
         Route::get('show/{id}', array('as' => 'admin.vendor.show', 'uses' => 'Admin\VendorController@show'));
            //Accepted-order 
         Route::get('accepted-order/{id}', array('as' => 'admin.vendor.accepted-order', 'uses' => 'Admin\VendorController@accepted_orders'));
           //view-product
          Route::get('view-product/{id}', array('as' => 'admin.vendor.view-product', 'uses' => 'Admin\VendorController@product_show'));
          // Vendor Document
          Route::get('document/{id}', array('as' => 'admin.vendor.document', 'uses' => 'Admin\VendorController@document'));
});



/*============= Subscription Meta Route Start =============*/
Route::group(array('prefix' => 'subscriptionmeta'), function () {
        //View
        Route::get('list', array('as' => 'admin.subscriptionmeta.list', 'uses' => 'Admin\SubscriptionMetaController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.subscriptionmeta.add', 'uses' => 'Admin\SubscriptionMetaController@create'));
        Route::post('add', array('as' => 'admin.subscriptionmeta.add.post', 'uses' => 'Admin\SubscriptionMetaController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.subscriptionmeta.edit', 'uses' => 'Admin\SubscriptionMetaController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.subscriptionmeta.edit.post', 'uses' => 'Admin\SubscriptionMetaController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.subscriptionmeta.delete.delete-confirm', 'uses' => 'Admin\SubscriptionMetaController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.subscriptionmeta.delete', 'uses' => 'Admin\SubscriptionMetaController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.subscriptionmeta.updatestatus.update-status-confirm', 'uses' => 'Admin\SubscriptionMetaController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.subscriptionmeta.updatestatus', 'uses' => 'Admin\SubscriptionMetaController@update_status'));
        //Show 
         Route::get('show/{id}', array('as' => 'admin.subscriptionmeta.show', 'uses' => 'Admin\SubscriptionMetaController@show'));
});


/*============= Subscription Faq Route Start =============*/
Route::group(array('prefix' => 'subscriptionfaq'), function () {
        //View
        Route::get('list', array('as' => 'admin.subscriptionfaq.list', 'uses' => 'Admin\SubscriptionFaqController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.subscriptionfaq.add', 'uses' => 'Admin\SubscriptionFaqController@create'));
        Route::post('add', array('as' => 'admin.subscriptionfaq.add.post', 'uses' => 'Admin\SubscriptionFaqController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.subscriptionfaq.edit', 'uses' => 'Admin\SubscriptionFaqController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.subscriptionfaq.edit.post', 'uses' => 'Admin\SubscriptionFaqController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.subscriptionfaq.delete.delete-confirm', 'uses' => 'Admin\SubscriptionFaqController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.subscriptionfaq.delete', 'uses' => 'Admin\SubscriptionFaqController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.subscriptionfaq.updatestatus.update-status-confirm', 'uses' => 'Admin\SubscriptionFaqController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.subscriptionfaq.updatestatus', 'uses' => 'Admin\SubscriptionFaqController@update_status'));
        //Show 
         Route::get('show/{id}', array('as' => 'admin.subscriptionfaq.show', 'uses' => 'Admin\SubscriptionFaqController@show'));
});


/*============= Subscription Plan  Route Start =============*/
Route::group(array('prefix' => 'subscriptionplan'), function () {
        //View
        Route::get('list', array('as' => 'admin.subscriptionplan.list', 'uses' => 'Admin\SubscriptionPlanController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.subscriptionplan.add', 'uses' => 'Admin\SubscriptionPlanController@create'));
        Route::post('add', array('as' => 'admin.subscriptionplan.add.post', 'uses' => 'Admin\SubscriptionPlanController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.subscriptionplan.edit', 'uses' => 'Admin\SubscriptionPlanController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.subscriptionplan.edit.post', 'uses' => 'Admin\SubscriptionPlanController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.subscriptionplan.delete.delete-confirm', 'uses' => 'Admin\SubscriptionPlanController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.subscriptionplan.delete', 'uses' => 'Admin\SubscriptionPlanController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.subscriptionplan.updatestatus.update-status-confirm', 'uses' => 'Admin\SubscriptionPlanController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.subscriptionplan.updatestatus', 'uses' => 'Admin\SubscriptionPlanController@update_status'));
        //Show 
         Route::get('show/{id}', array('as' => 'admin.subscriptionplan.show', 'uses' => 'Admin\SubscriptionPlanController@show'));
});








/*============= Frontcontain Route Start =============*/
Route::group(array('prefix' => 'frontcontain'), function () {
        //View
        Route::get('list', array('as' => 'admin.frontcontain.list', 'uses' => 'Admin\FrontcontainController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.frontcontain.add', 'uses' => 'Admin\FrontcontainController@create'));
        Route::post('add', array('as' => 'admin.frontcontain.add.post', 'uses' => 'Admin\FrontcontainController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.frontcontain.edit', 'uses' => 'Admin\FrontcontainController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.frontcontain.edit.post', 'uses' => 'Admin\FrontcontainController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.frontcontain.delete.delete-confirm', 'uses' => 'Admin\FrontcontainController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.frontcontain.delete', 'uses' => 'Admin\FrontcontainController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.frontcontain.updatestatus.update-status-confirm', 'uses' => 'Admin\FrontcontainController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.frontcontain.updatestatus', 'uses' => 'Admin\FrontcontainController@update_status'));
        //Show 
         Route::get('show/{id}', array('as' => 'admin.frontcontain.show', 'uses' => 'Admin\FrontcontainController@show'));
});


/*============= Refer-earn  Route Start =============*/
Route::group(array('prefix' => 'refer-earn'), function () {
        //View
        Route::get('list', array('as' => 'admin.refer-earn.list', 'uses' => 'Admin\ReferEarnController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.refer-earn.add', 'uses' => 'Admin\ReferEarnController@create'));
        Route::post('add', array('as' => 'admin.refer-earn.add.post', 'uses' => 'Admin\ReferEarnController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.refer-earn.edit', 'uses' => 'Admin\ReferEarnController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.refer-earn.edit.post', 'uses' => 'Admin\ReferEarnController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.refer-earn.delete.delete-confirm', 'uses' => 'Admin\ReferEarnController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.refer-earn.delete', 'uses' => 'Admin\ReferEarnController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.refer-earn.updatestatus.update-status-confirm', 'uses' => 'Admin\ReferEarnController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.refer-earn.updatestatus', 'uses' => 'Admin\ReferEarnController@update_status'));
        //Show 
         Route::get('show/{id}', array('as' => 'admin.refer-earn.show', 'uses' => 'Admin\ReferEarnController@show'));
});



Route::group(array('prefix' => 'faq-product'), function () {
    Route::get('add', array('as' => 'admin.faq-product.add', 'uses' => 'Admin\ProductController@faq_product_add'));
    Route::post('add', array('as' => 'admin.faq-product.add.post', 'uses' => 'Admin\ProductController@faq_save_product'));

});

/*============= doctor Route Start =============*/
Route::group(array('prefix' => 'doctor'), function () {
        //View
        Route::get('list', array('as' => 'admin.doctor.list', 'uses' => 'Admin\DoctorController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.doctor.add', 'uses' => 'Admin\DoctorController@create'));
        Route::post('add', array('as' => 'admin.doctor.add.post', 'uses' => 'Admin\DoctorController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.doctor.edit', 'uses' => 'Admin\DoctorController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.doctor.edit.post', 'uses' => 'Admin\DoctorController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.doctor.delete.delete-confirm', 'uses' => 'Admin\DoctorController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.doctor.delete', 'uses' => 'Admin\DoctorController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.doctor.updatestatus.update-status-confirm', 'uses' => 'Admin\DoctorController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.doctor.updatestatus', 'uses' => 'Admin\DoctorController@update_status'));
        //show
        Route::get('show/{id}', array('as' => 'admin.doctor.show', 'uses' => 'Admin\DoctorController@show')); 

          //Doctor-orders
         Route::get('doctor-order/{id}', array('as' => 'admin.doctor.doctor-orders-list', 'uses' => 'Admin\DoctorController@accepted_orders')); 
             //View item list
        Route::get('view-item/{id}', array('as' => 'admin.doctor.view-item', 'uses' => 'Admin\DoctorController@order_show'));     
});


/*============= Customer Route Start =============*/
Route::group(array('prefix' => 'customer'), function () {
        //View
        Route::get('list', array('as' => 'admin.customer.list', 'uses' => 'Admin\CustomerController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.customer.add', 'uses' => 'Admin\CustomerController@create'));
        Route::post('add', array('as' => 'admin.customer.add.post', 'uses' => 'Admin\CustomerController@store'));
        //Edit
        Route::get('edit/{id}', array('as' => 'admin.customer.edit', 'uses' => 'Admin\CustomerController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.customer.edit.post', 'uses' => 'Admin\CustomerController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.customer.delete.delete-confirm', 'uses' => 'Admin\CustomerController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.customer.delete', 'uses' => 'Admin\CustomerController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.customer.updatestatus.update-status-confirm', 'uses' => 'Admin\CustomerController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.customer.updatestatus', 'uses' => 'Admin\CustomerController@update_status'));
        //show
        Route::get('show/{id}', array('as' => 'admin.customer.show', 'uses' => 'Admin\CustomerController@show'));
          //my-orders
          Route::get('my-order/{id}', array('as' => 'admin.customer.my-orders-list', 'uses' => 'Admin\CustomerController@accepted_orders'));
            //View item list
        Route::get('view/{id}', array('as' => 'admin.customer.view', 'uses' => 'Admin\CustomerController@order_show'));

             //Transaction Wallet History
       Route::get('history/{id}', array('as' => 'admin.customer.history', 'uses' => 'Admin\CustomerController@history'));   
});



/*============= Faq  Route Start =============*/
Route::group(array('prefix' => 'faq'), function () {
        //View
        Route::get('list', array('as' => 'admin.faq.list', 'uses' => 'Admin\FaqController@index')); 
         //Insert
        Route::get('add', array('as' => 'admin.faq.add', 'uses' => 'Admin\FaqController@create'));
        Route::post('add', array('as' => 'admin.faq.add.post', 'uses' => 'Admin\FaqController@store'));
        //Edit
        Route::get('edit/{id}', array('as' => 'admin.faq.edit', 'uses' => 'Admin\FaqController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.faq.edit.post', 'uses' => 'Admin\FaqController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.faq.delete.delete-confirm', 'uses' => 'Admin\FaqController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.faq.delete', 'uses' => 'Admin\FaqController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.faq.updatestatus.update-status-confirm', 'uses' => 'Admin\FaqController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.faq.updatestatus', 'uses' => 'Admin\FaqController@update_status'));
        //show
        Route::get('show/{id}', array('as' => 'admin.faq.show', 'uses' => 'Admin\FaqController@show'));    
});


/*============= Faq Topics Route Start =============*/
Route::group(array('prefix' => 'faqtopics'), function () {
        //View
        Route::get('list', array('as' => 'admin.faqtopics.list', 'uses' => 'Admin\FaqtopicsController@index')); 
         //Insert
        Route::get('add', array('as' => 'admin.faqtopics.add', 'uses' => 'Admin\FaqtopicsController@create'));
        Route::post('add', array('as' => 'admin.faqtopics.add.post', 'uses' => 'Admin\FaqtopicsController@store'));
        //Edit
        Route::get('edit/{id}', array('as' => 'admin.faqtopics.edit', 'uses' => 'Admin\FaqtopicsController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.faqtopics.edit.post', 'uses' => 'Admin\FaqtopicsController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.faqtopics.delete.delete-confirm', 'uses' => 'Admin\FaqtopicsController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.faqtopics.delete', 'uses' => 'Admin\FaqtopicsController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.faqtopics.updatestatus.update-status-confirm', 'uses' => 'Admin\FaqtopicsController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.faqtopics.updatestatus', 'uses' => 'Admin\FaqtopicsController@update_status'));
        //show
        Route::get('show/{id}', array('as' => 'admin.faqtopics.show', 'uses' => 'Admin\FaqtopicsController@show'));    
});



 
/*============= offer Route Start =============*/

Route::group(array('prefix' => 'offer'), function () {
        //Show Offer
        Route::get('list', array('as' => 'admin.offer.list', 'uses' => 'Admin\OfferController@index'));
        Route::get('data', array('as' => 'admin.offer.list.data', 'uses' => 'Admin\OfferController@data'));
        //Insert
        Route::get('add', array('as' => 'admin.offer.add', 'uses' => 'Admin\OfferController@create'));
        Route::post('add', array('as' => 'admin.offer.add.post', 'uses' => 'Admin\OfferController@store'));
        //Edit
        Route::get('edit/{id}', array('as' => 'admin.offer.edit', 'uses' => 'Admin\OfferController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.offer.edit.post', 'uses' => 'Admin\OfferController@update'));

        //Delete
        Route::get('{id}/confirm-delete', array('as' => 'admin.offer.delete.delete-confirm', 'uses' => 'Admin\OfferController@getModalDelete'));
        Route::get('delete/{id}', array('as' => 'admin.offer.delete', 'uses' => 'Admin\OfferController@destroy'));
});


/*============= offer Banner Route Start =============*/
Route::group(array('prefix' => 'offer-banner'), function () {
        //View
        Route::get('list', array('as' => 'admin.offer-banner.list', 'uses' => 'Admin\OfferBannerController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.offer-banner.add', 'uses' => 'Admin\OfferBannerController@create'));
        Route::post('add', array('as' => 'admin.offer-banner.add.post', 'uses' => 'Admin\OfferBannerController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.offer-banner.edit', 'uses' => 'Admin\OfferBannerController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.offer-banner.edit.post', 'uses' => 'Admin\OfferBannerController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.offer-banner.delete.delete-confirm', 'uses' => 'Admin\OfferBannerController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.offer-banner.delete', 'uses' => 'Admin\OfferBannerController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.offer-banner.updatestatus.update-status-confirm', 'uses' => 'Admin\OfferBannerController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.offer-banner.updatestatus', 'uses' => 'Admin\OfferBannerController@update_status'));
        //Show 
         Route::get('show/{id}', array('as' => 'admin.offer-banner.show', 'uses' => 'Admin\OfferBannerController@show'));

         //getSubCategory
          Route::post('getsubcategory', array('as' => 'admin.offer-banner.getsubcategory', 'uses' => 'Admin\OfferBannerController@getsubcategory'));

          //getSubcat
         Route::post('getsubcat', array('as' => 'admin.offer-banner.getsubcat', 'uses' => 'Admin\OfferBannerController@getsubcat'));
});



/*============= Contact-purpose Route Start =============*/
Route::group(array('prefix' => 'contact-purpose'), function () {
        //View
        Route::get('list', array('as' => 'admin.contact-purpose.list', 'uses' => 'Admin\ContactPurposeController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.contact-purpose.add', 'uses' => 'Admin\ContactPurposeController@create'));
        Route::post('add', array('as' => 'admin.contact-purpose.add.post', 'uses' => 'Admin\ContactPurposeController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.contact-purpose.edit', 'uses' => 'Admin\ContactPurposeController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.contact-purpose.edit.post', 'uses' => 'Admin\ContactPurposeController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.contact-purpose.delete.delete-confirm', 'uses' => 'Admin\ContactPurposeController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.contact-purpose.delete', 'uses' => 'Admin\ContactPurposeController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.contact-purpose.updatestatus.update-status-confirm', 'uses' => 'Admin\ContactPurposeController@status_confirm'));

            Route::get('update-status/{status}/{id}', array('as' => 'admin.contact-purpose.updatestatus', 'uses' => 'Admin\ContactPurposeController@update_status'));
       
});



/*============= Reason-return Route Start =============*/
Route::group(array('prefix' => 'reason-return'), function () {
        //View
        Route::get('list', array('as' => 'admin.reason-return.list', 'uses' => 'Admin\ReasonReturnController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.reason-return.add', 'uses' => 'Admin\ReasonReturnController@create'));
        Route::post('add', array('as' => 'admin.reason-return.add.post', 'uses' => 'Admin\ReasonReturnController@store'));
        //Update
        Route::get('edit/{id}', array('as' => 'admin.reason-return.edit', 'uses' => 'Admin\ReasonReturnController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.reason-return.edit.post', 'uses' => 'Admin\ReasonReturnController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.reason-return.delete.delete-confirm', 'uses' => 'Admin\ReasonReturnController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.reason-return.delete', 'uses' => 'Admin\ReasonReturnController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.reason-return.updatestatus.update-status-confirm', 'uses' => 'Admin\ReasonReturnController@status_confirm'));

            Route::get('update-status/{status}/{id}', array('as' => 'admin.reason-return.updatestatus', 'uses' => 'Admin\ReasonReturnController@update_status'));
       
});





/*============= Contact Route Start =============*/

Route::group(array('prefix' => 'contact'), function () {
     //View
    Route::get('list', array('as' => 'admin.contact.list', 'uses' => 'Admin\ContactController@index'));

      Route::post('add', array('as' => 'admin.contact.insert', 'uses' => 'Admin\ContactController@store'));
         
    //Delete
     Route::get('{cat_id}/confirm-delete', array('as' => 'admin.contact.delete.delete-confirm', 'uses' => 'Admin\ContactController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.contact.delete', 'uses' => 'Admin\ContactController@destroy'));
    //Show 
    Route::get('show/{id}', array('as' => 'admin.contact.show', 'uses' => 'Admin\ContactController@show'));
});

/*============= Subscribe Route Start =============*/

Route::group(array('prefix' => 'subscribe'), function () {
     //View
Route::get('list', array('as' => 'admin.subscribe.list', 'uses' => 'Admin\SubscribeController@index'));
         
    //Delete
Route::get('{cat_id}/confirm-delete', array('as' => 'admin.subscribe.delete.delete-confirm', 'uses' => 'Admin\SubscribeController@getModalDelete'));
Route::get('deletedcat/{cat_id}', array('as' => 'admin.subscribe.delete', 'uses' => 'Admin\SubscribeController@destroy'));

});


/*============= Notification Route Start =============*/

Route::group(array('prefix' => 'notification'), function () {

        Route::get('list', array('as' => 'admin.notification.list', 'uses' => 'Admin\NotificationController@index'));
        Route::get('data', array('as' => 'notification.list.data', 'uses' => 'Admin\NotificationController@data'));
        //unavailable
        Route::get('unavailable', array('as' => 'notification.unavailable.list', 'uses' => 'Admin\NotificationController@unavailable'));
        Route::get('unavailabledata', array('as' => 'notification.unavailabledata', 'uses' => 'Admin\NotificationController@unavailableData'));
        //order
        Route::get('order', array('as' => 'admin.notification.order', 'uses' => 'Admin\NotificationController@orderStatus'));

        Route::get('orderStatusData', array('as' => 'admin.notification.orderStatusData', 'uses' => 'Admin\NotificationController@orderStatusData')); 

        //Shopper
        Route::get('shopper', array('as' => 'notification.shopper.list', 'uses' => 'Admin\NotificationController@updateProducts'));       
        Route::get('updateproductData', array('as' => 'notification.updateproductData', 'uses' => 'Admin\NotificationController@updateproductData'));  
        //Driver
        Route::get('address', array('as' => 'notification.address.list', 'uses' => 'Admin\NotificationController@addressUpdate'));       
        Route::get('addressStatusData', array('as' => 'notification.addressStatusData', 'uses' => 'Admin\NotificationController@addressUpdateData'));
        

        Route::get('add', array('as' => 'admin.notification.add', 'uses' => 'Admin\NotificationController@create'));
        Route::post('add', array('as' => 'admin.notification.add.post', 'uses' => 'Admin\NotificationController@store'));

        Route::get('edit/{id}', array('as' => 'notification.edit', 'uses' => 'Admin\NotificationController@edit'));

        Route::post('edit{id}', array('as' => 'notification.edit.post', 'uses' => 'Admin\NotificationController@update'));
        //Delete
        Route::get('{cat_id}/confirm-delete', array('as' => 'notification.delete.delete-confirm', 'uses' => 'Admin\NotificationController@getModalDelete'));
        
        Route::get('deletedcat/{cat_id}', array('as' => 'notification.delete', 'uses' => 'Admin\NotificationController@destroy'));  

        Route::get('note/{id}', array('as' => 'notification.delete', 'uses' => 'Admin\NotificationController@destroy_note'));  
});




/*============= Order Route Start =============*/
Route::group(array('prefix' => 'order'), function () {
        //View
        Route::get('accepted', array('as' => 'admin.order.accepted-orders-list', 'uses' => 'Admin\OrderController@accepted_orders'));
        Route::get('new', array('as' => 'admin.order.list', 'uses' => 'Admin\OrderController@index'));
         //Insert
        Route::get('add', array('as' => 'admin.order.add', 'uses' => 'Admin\OrderController@create'));
        Route::post('add', array('as' => 'admin.order.add.post', 'uses' => 'Admin\OrderController@store'));
        //Return View
        Route::get('return-view/{id}', array('as' => 'admin.order.return-view', 'uses' => 'Admin\OrderController@return_order_details'));
        Route::post('accepte_return_product', array('as' => 'admin.order.accepte_return_product', 'uses' => 'Admin\OrderController@accepte_return_product'));
        //View
        Route::get('view/{id}', array('as' => 'admin.order.view', 'uses' => 'Admin\OrderController@show'));
        Route::get('view/{id}', array('as' => 'admin.order.view', 'uses' => 'Admin\OrderController@show'));
        Route::post('assign-vendor', array('as' => 'admin.order.assign-vendor', 'uses' => 'Admin\OrderController@assign_vendor'));
        Route::get('invoice/{id}', array('as' => 'admin.order.invoice', 'uses' => 'Admin\OrderController@invoice'));
        Route::post('edit/{id}', array('as' => 'admin.order.edit.post', 'uses' => 'Admin\OrderController@update'));
        //Delete
       Route::get('{cat_id}/confirm-delete', array('as' => 'admin.order.delete.delete-confirm', 'uses' => 'Admin\OrderController@getModalDelete'));
        Route::get('deletedcat/{cat_id}', array('as' => 'admin.order.delete', 'uses' => 'Admin\OrderController@destroy'));
        //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.order.updatestatus.update-status-confirm', 'uses' => 'Admin\OrderController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.order.updatestatus', 'uses' => 'Admin\OrderController@update_status'));
                    // Change  status
             Route::get('status-list/{id}', array('as' => 'admin.order.status-list', 'uses' => 'Admin\OrderController@statuslist'));
          //
          Route::any('status', 'Admin\OrderController@changeStatus')->name('admin.order.status');
        Route::any('statuslist/{id}', 'Admin\OrderController@statuslist')->name('order.statuslist');   
});

/*============= Delivery Location Route Start =============*/
Route::group(array('prefix' => 'delivery-location'), function () {
        //Show delivery-location
        Route::get('list', array('as' => 'admin.delivery-location.list', 'uses' => 'Admin\DeliveryLocationController@index'));
        Route::get('data', array('as' => 'admin.delivery-location.list.data', 'uses' => 'Admin\DeliveryLocationController@data'));
        //Insert
        Route::get('add', array('as' => 'admin.delivery-location.add', 'uses' => 'Admin\DeliveryLocationController@create'));
        Route::post('add', array('as' => 'admin.delivery-location.add.post', 'uses' => 'Admin\DeliveryLocationController@store'));
        
        Route::post('get-details', array('as' => 'admin.delivery-location.get-details.post', 'uses' => 'Admin\DeliveryLocationController@getDetailsByPinCode'));
        //Edit
        Route::get('edit/{id}', array('as' => 'admin.delivery-location.edit', 'uses' => 'Admin\DeliveryLocationController@edit'));
        Route::post('edit/{id}', array('as' => 'admin.delivery-location.edit.post', 'uses' => 'Admin\DeliveryLocationController@update'));
        //Delete
        Route::get('{id}/confirm-delete', array('as' => 'admin.delivery-location.delete.delete-confirm', 'uses' => 'Admin\DeliveryLocationController@getModalDelete'));
        Route::get('delete/{id}', array('as' => 'admin.delivery-location.delete', 'uses' => 'Admin\DeliveryLocationController@destroy'));
         //Status Update
        Route::get('update-status-confirm/{status}/{id}', array('as' => 'admin.delivery-location.updatestatus.update-status-confirm', 'uses' => 'Admin\DeliveryLocationController@status_confirm'));
            Route::get('update-status/{status}/{id}', array('as' => 'admin.delivery-location.updatestatus', 'uses' => 'Admin\DeliveryLocationController@update_status'));
});


//Zone Routes
Route::any('admin/zone/datatable', 'Admin\ZoneController@anyData')->name('zone.datatable');
//Route::post('zone/status', 'Admin\ZoneController@changeStatus')->name('admin.zone.status');

Route::any('admin/zone/status', array('as' => 'admin.zone.status', 'uses' => 'Admin\ZoneController@changeStatus'));

Route::any('zone/default', 'Admin\ZoneController@makeDefault')->name('admin.zone.default');
//Route::get('zone/list', 'Admin\ZoneController@index');

Route::get('zone/list', array('as' => 'zone.data', 'uses' => 'Admin\ZoneController@index'));


//Update
Route::get('zone/{id}/edit', array('as' => 'edit/zone', 'uses' => 'Admin\ZoneController@edit'));
Route::post('update/{id}', array('as' => 'update.zone', 'uses' => 'Admin\ZoneController@update'));

//Insert
Route::post('admin/zone-store', 'Admin\ZoneController@store')->name('zone.store');
Route::get('admin/zone/create', 'Admin\ZoneController@create');
//Delete
Route::get('zone/{id}/confirm-delete', array('as' => 'confirm-delete/zone', 'uses' => 'Admin\ZoneController@getModalDelete'));
Route::get('deleted/{id}', array('as' => 'delete/zone', 'uses' => 'Admin\ZoneController@destroy'));
 Route::resource('zone', 'Admin\ZoneController');
//Route::resource('load-slot-zone', 'SlotAdmin\ZoneController');
Route::get('admin/zone-details', 'Admin\ZoneController@getZoneDetailsById')->name('zone-details');
Route::get('load-zone', 'Admin\ZoneController@loadZoneByLat')->name('load-zone');



/*Here is close admin auth section middleware*/
});


Route::get('/clear-cache', function(Request $request) {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
});









/*
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
*/
