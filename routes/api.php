<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

*/


Route::group(['prefix' => '/v1/'], function () {

     /*
    |--------------------------------------------------------------------------
    | Driver  API Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'driver'], function () {
        Route::post('register', 'Api\DriverController@driver_register');
        Route::post('verify-otp', 'Api\DriverController@verify_otp');
        Route::post('resend-otp', 'Api\DriverController@resend_otp');
        Route::post('driver-login', 'Api\DriverController@driver_login');

    });     
    /*
    |--------------------------------------------------------------------------
    | Vendor  API Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'vendor'], function () {
        Route::post('register', 'Api\VendorController@driver_register');
        Route::post('verify-otp', 'Api\VendorController@verify_otp');
        Route::post('resend-otp', 'Api\VendorController@resend_otp');
        Route::post('vendor-login', 'Api\VendorController@vendor_login');

    }); 
    
    /*
    |--------------------------------------------------------------------------
    | Global  API Routes
    |--------------------------------------------------------------------------
    */
    Route::post('register', 'Api\UserController@register');
    Route::post('verify-otp', 'Api\UserController@verify_otp');
    Route::post('resend-otp', 'Api\UserController@resend_otp');
    Route::get('send-otp-test', 'Api\UserController@test_example');
    Route::post('pincode-details', 'Api\UserController@getDetailsByPinCode');

    /*===UtilityController===*/
    Route::get('brand-list', 'Api\UtilityController@getAllBrand');
    Route::get('blog-list', 'Api\UtilityController@getAllBlog');
    Route::get('blog-details/{id}', 'Api\UtilityController@getBlogDetails');
    Route::get('category-list', 'Api\UtilityController@getAllCategory');
    Route::post('banner-list', 'Api\UtilityController@getAllBanner');
    Route::get('faq-list', 'Api\UtilityController@getAllFaq');
    Route::post('coupon-list', 'Api\UtilityController@getAllCoupon');
    Route::post('banner-offer', 'Api\UtilityController@getAllOffer');
    Route::post('offer-slider-image', 'Api\UtilityController@offer_just_for_you_slider_image');

    /*=== ProductController===*/
    Route::post('home-product-list', 'Api\ProductController@home_product_list');


    Route::get('product-list', 'Api\ProductController@product_list');
    Route::get('deal-of-the-day-list', 'Api\ProductController@deal_of_the_day_list');
    Route::get('health-product-list', 'Api\ProductController@deal_of_the_day_list');
    Route::get('most-selling-products', 'Api\ProductController@most_selling_products');
    Route::post('product-details', 'Api\ProductController@product_details');
    Route::post('search-product', 'Api\ProductController@search_product_name');
    Route::get('get-subscription-meta', 'Api\UtilityController@subscription_meta');
    Route::get('get-subscription-faq', 'Api\UtilityController@subscription_faq');
    Route::get('get-legal-info', 'Api\UtilityController@legal_info');
    Route::get('global-setting', 'Api\UtilityController@globalSetting');
    Route::get('get-subscription-plan', 'Api\UtilityController@subscription_plan');
    Route::post('filter-data', 'Api\ProductController@filter_data');

    Route::get('filter-range', 'Api\ProductController@filterRange');
/*
|--------------------------------------------------------------------------
|Authenticated  User API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'auth:api'], function () {         
   //User Profile 
  Route::post('driver/upload-document', 'Api\DriverController@upload_document');

   Route::get('user/profile', 'Api\UserController@details');
   Route::post('user/profile-update', 'Api\UserController@update_profile');
   Route::post('user/send-email', 'Api\UserController@send_email');
   Route::post('user/verify-otp-email', 'Api\UserController@verify_otp_email');
   Route::post('user/update-profile-image', 'Api\UserController@update_profile_image');
   Route::post('user/change-password', 'Api\UserController@change_password');
   //Coustome Info
   Route::post('add-save-for-later', 'Api\UtilityController@save_for_later');
   Route::get('save-for-later-list', 'Api\UtilityController@save_for_later_list');
   Route::post('save-for-later-delete', 'Api\UtilityController@save_for_later_delete');
   Route::post('move-to-cart', 'Api\UtilityController@move_to_cart');
   //Wish Produdct
   Route::post('add-wishlist', 'Api\UtilityController@add_wishlist');
   Route::get('wishlist-list', 'Api\UtilityController@wishlist_list');
   Route::post('wishlist-delete', 'Api\UtilityController@wishlist_delete');
   Route::get('get-all-prescription', 'Api\UtilityController@getAllPrescription');


   //Delivery Addresses
   Route::resource('delivery-addresses', 'Api\DeliveryAddressesController');
   Route::post('update-default-delivery-address', 'Api\DeliveryAddressesController@update_default_deliveryAddress');

   
   //Patient Info Controller
   Route::resource('patient-info', 'Api\PatientInfoController');
   Route::post('update-default-patient-info', 'Api\PatientInfoController@update_default_patient');

   //Payment Methods Conroller
   Route::resource('payment-method-info', 'Api\PaymentmethodsController');

   //Add To cart and list controller
   Route::resource('cart', 'Api\CartController');
   Route::any('clear-cart', 'Api\CartController@clearCart');
   Route::post('cart-add-subscription-plan', 'Api\CartController@add_to_subscription_plan');
   Route::post('remove-plan', 'Api\CartController@remove_plan');
   Route::post('check-referral-code', 'Api\CartController@check_referral_code');
   Route::get('refer-earn', 'Api\CartController@refer_earn');
 
   Route::post('upload-prescription', 'Api\CartController@upload_prescription');
   // Drive Order Route
    Route::get('driver/order-list','Api\DriverController@order_list');
    Route::post('driver/booking-status','Api\DriverController@booking_status');
    Route::post('driver/profile','Api\DriverController@driver_profile');
    Route::post('driver/accept-order','Api\DriverController@accept_driver_orders');
    Route::post('driver/order-details','Api\DriverController@order_details');
    Route::post('driver/update-driver-status','Api\DriverController@update_driver_status');
    Route::post('driver/trip-history','Api\DriverController@trip_history');
    Route::post('driver/add-bank-details','Api\DriverController@add_bank_details');
    Route::post('driver/delivery-address-zone-list','Api\DriverController@delivery_address_zone');
    Route::post('driver/notification','Api\DriverController@driver_notification');
    Route::post('driver/update-login-status','Api\DriverController@update_driver_login_status');
    Route::post('driver/session-time','Api\DriverController@driver_session_time');
    Route::post('driver/update-zone','Api\DriverController@update_driver_zone');
    Route::post('driver/get-away-vendor','Api\DriverController@get_vendor_away_list');
    Route::post('driver/update_driver_order_iteam_status','Api\DriverController@update_driver_order_iteam_status');

    //Vendor Profile
        Route::post('vendor/profile','Api\VendorController@vendor_profile');
        Route::post('vendor/order-list','Api\VendorController@orders_list_by_vendors');
        Route::post('vendor/order-validate-deatils','Api\VendorController@order_validate');
        Route::post('vendor/decline-orders','Api\VendorController@decline_orders');
        Route::post('vendor/notification','Api\VendorController@notification');
        Route::post('vendor/accepte_orders','Api\VendorController@accepte_orders');
        Route::post('vendor/availablity','Api\VendorController@availablity');
        Route::post('vendor/accepted_orders_list','Api\VendorController@accepted_orders_list');
        
   /*=== Order Controller ====*/

   Route::get('user/items-bought','Api\OrderController@items_bought');

   Route::get('user/order-list','Api\OrderController@order_list');

   Route::get('user/past-order-list','Api\OrderController@past_order_list');
   Route::post('user/order-place','Api\OrderController@order_place');
   Route::post('user/reason-return-list','Api\OrderController@reason_return_list');
   Route::post('user/order-details','Api\OrderController@order_details');
   Route::post('user/order-cancel','Api\OrderController@order_cancel');
   Route::post('user/re-order','Api\CartController@reOrder');
   Route::post('user/subscription-plan-order','Api\OrderController@subscription_plan_order');
   Route::post('notification', 'Api\UserController@notification');
   Route::post('admin-notification', 'Api\UserController@admin_notification');
   Route::post('user/check-offer-code','Api\OrderController@check_offer_code');
   /*==== Oder Track ==========*/
   Route::post('user/track-order-vendor','Api\OrderController@track_order_vendors');
   Route::post('user/track-order-user','Api\OrderController@track_order_user');
   Route::post('user/vendor-accepted-products','Api\OrderController@vendor_accepted_products');
   Route::post('user/order-review','Api\OrderController@order_review');

   Route::post('user/return-order-item-list','Api\OrderController@return_order_item_list');
   Route::post('user/return-product','Api\OrderController@return_product');
 
   /*=================== User Wallets ===================*/
  Route::get('user/wallets','Api\UserController@user_wallets');

}); //Close middleware authenticated user api routes



}); 
