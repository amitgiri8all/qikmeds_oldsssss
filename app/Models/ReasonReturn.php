<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ReasonReturn extends Model
{
    use HasFactory,SoftDeletes;

          protected $table = 'reason_return';
 
    protected $fillable = [
        'name',
    ];

}
