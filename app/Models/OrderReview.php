<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderReview extends Model
{
    use HasFactory;

     protected $table = 'order_review';

    protected $fillable = [
        'order_id','rating','description','user_id'
    ];
}
