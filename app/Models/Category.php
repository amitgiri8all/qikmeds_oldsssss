<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use DB;

class Category extends Model
{
    use HasFactory;
        use Sluggable;

    protected $fillable=['category_name','slug','parent_id','status','image','sort_no','is_parent'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'category_name'
            ]
        ];
    }
    
    public static function shiftChild($cat_id){
        return Category::whereIn('id',$cat_id)->update(['is_parent'=>1]);
    }

    public function children() {
    return $this->hasMany(self::class, 'parent_id','id');
}
    
  public function sub_category()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }
 /* 
    public function children()
  {
    return $this->hasMany('App\\Category', 'parent_id');
  }*/

  public function getImageAttribute($value = ""){
        if(!empty($value)){
            return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
   }

   public static function categoryFooter(){
    return DB::table('categories')->select('id','category_name','slug')->where('status','1')->limit(6)->get();
   }

}
