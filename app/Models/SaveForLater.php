<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class SaveForLater extends Model
{
    use HasFactory;
    protected $table = 'save_for_later';
    protected $fillable=['user_id','product_id',];

    public static function chekc_save_for_later($product_id=null){

         $result = DB::table('save_for_later')->where('product_id',$product_id)->where('user_id',Auth::guard('web')->user()->id)->count();
         if($result>0){
              return false;
         }else{
              return true;
         }
    }


    public function Product(){
      return $this->belongsTo('App\Models\Product', 'product_id', 'id');  
    }

       public function ProductImage()
    {
        return $this->belongsTo('App\Models\ProductImage', 'product_id', 'prod_id')->where('set_primary','Yes');
    }

}
