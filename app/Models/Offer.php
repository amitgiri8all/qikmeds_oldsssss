<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model 
{
    use HasFactory;
  
    protected $table = 'offers';

    protected $fillable = [
        'user_id','name','offer_type','offer_value','image','sold_product','to_time','from_time','status',
    ];

 

}
