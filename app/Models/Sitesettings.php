<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sitesettings extends Model
{
    use HasFactory;

          protected $table = 'site_settings';

    protected $fillable = [
        'min_price','max_price','free_delivery_charge','phone','whats_up','facebook','twitter','instagram','linkedin','youtube','currency','created_at','updated_at','express_delivery_charges','express_delivery_time','standard_delivery_charges','standard_delivery_charges','referred_by_amount','referred_by_amount','referral_amount','qikmeds_discount','new_user_qikmeds_discount','delivery_charge','near_by_queue_time','city_queue_time','lock_delivery_times'
    ];
}
