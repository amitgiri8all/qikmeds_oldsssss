<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ReferEarn extends Model
{
    use HasFactory,SoftDeletes;
     
    protected $table = "refer_earn";
    protected $fillable = [
        'title','refer_amount','refer_by_amount','image','description','status',
    ];

     public function getImageAttribute($value = ""){
        //if(!empty($value) && file_exists(asset('storage/app/public/upload/Thumbnail') . "/" .$value)){
        if(!empty($value)){
            return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
   } 

}
