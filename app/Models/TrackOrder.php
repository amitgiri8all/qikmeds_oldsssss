<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackOrder extends Model
{
    use HasFactory;
     protected $table = 'order_track';
 
    protected $fillable = [
        'order_id','order_track_status','date_time'
    ];
}
