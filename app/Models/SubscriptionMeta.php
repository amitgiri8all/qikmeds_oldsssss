<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionMeta extends Model
{
    use HasFactory;

    protected $table = 'subscription_meta';

    protected $fillable = [
        'type','title','description','image','status','price','price_type','subscription_plan_id',
    ];

     public function getImageAttribute($value = ""){
        //if(!empty($value) && file_exists(asset('storage/app/public/upload/Thumbnail') . "/" .$value)){
        if(!empty($value)){
            return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
   } 
}
