<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionFaq extends Model
{
    use HasFactory;

    protected $table = 'subscription_faq';

    protected $fillable = [
        'user_type','question','answer','status'
    ];
}
