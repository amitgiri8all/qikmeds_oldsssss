<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientInfo extends Model
{
    use HasFactory;
    protected $table = 'patient_Info';
    protected $fillable=['user_id','sex','age','mobile_number','patient_name','address','pin_code','dob','prefered_time_call'];
}
