<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Product;

class Manufacturer extends Model
{
      use HasFactory;
 use Sluggable;
    protected $table = 'manufacturer';

      protected $fillable = [
        'id','manufacturer_name','slug','status'   
    ];

    protected $appends = ['ManTot'];
 
     public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'manufacturer_name'
            ]
        ];
    }

    public function Mfr(){
        return $this->belongsTo('App\Models\Product', 'manufacturer_id', 'id');
    }

      public function getManTotAttribute($value=null)
    {
      $data = Product::where('manufacturer_id',$this->id)->count();
       if(!empty($data)){
        return $data; 
       }else{
        return 0;
       } 
    }


 

}
