<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorBankDetails  extends Model
{
    use HasFactory;

    protected $table = 'doctor_bank_details';

    protected $fillable = ['user_id', 'account_holder_name','account_number','ifsc_code'];
}
