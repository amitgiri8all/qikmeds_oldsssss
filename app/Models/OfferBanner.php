<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class OfferBanner extends Model
{
    use HasFactory;
             use Sluggable;

    protected $table = "offer_banner";
     protected $fillable = [
       'title', 'image','sub_title','price','code','minimum_amount','price_type','description','eligibility','how_get_it','condition_offer','term_condition','offer_type'
    ];

     public function getImageAttribute($value = ""){
        //if(!empty($value) && file_exists(asset('storage/app/public/upload/Thumbnail') . "/" .$value)){
        if(!empty($value)){
            return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
   } 
   

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
   
}
