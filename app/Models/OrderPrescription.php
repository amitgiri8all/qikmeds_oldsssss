<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPrescription extends Model
{
    use HasFactory;
     protected $table = 'order_prescription';
    protected $fillable=['order_id','user_id','patient_id','complain','observation','diagnosis','instruction'];
}
