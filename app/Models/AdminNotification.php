<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminNotification extends Model
{
    use HasFactory;

    protected $table = 'admin_notifications';

    protected $fillable = [
        'user_ids', 'id','selection','message_heading', 'image', 'message_url', 'message','link_type','link_ur_type','cat_id','sub_cat_id','vendor_product_id','link'
    ];
    
}
