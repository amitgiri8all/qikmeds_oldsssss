<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserWallet extends Model
{
    use HasFactory,SoftDeletes;
     
    protected $table = "user_wallets";
    protected $fillable = [
        'user_id',
        'transaction_id',
        'amount',
        'description' ,
    ];
}
