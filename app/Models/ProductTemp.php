<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class ProductTemp extends Model
{
    use HasFactory;
 use Sluggable;
 
    //   protected $fillable = [
    //     'id','medicine_name','slug','prescription','type_of_sell','manufacturer_id','salt','mrp','uses',    'alternate_medicines','side_effects','how_to_use','chemical_class','habit_forming',   'therapeutic_class'   
    // ];

     protected $fillable = [
        'id',
        'p_id',
        'medicine_name',
        'mrp',
        'discounted_mrp',
        'igst',
        'hsn',
        'manufacturer_id',
        'composition',
        'packing_type',
        'packaging',
        'schedule',
        'sellable',
        'prescription_required',
        'usage',
        'about_salt',
        'mechanism_of_action',
        'pharmacokinets',
        'onset_of_action',
        'duration_of_action',
        'half_life',
        'side_effects',
        'contra_indications',
        'special_precautions_while_taking',
        'pregnancy_related_Information',
        'product_and_alcohol_interaction',
        'old_age_related_information',
        'breast_feeding_related_information',
        'children_related_information',
        'indications',
        'typical_dosage',
        'storage_requirements',
        'fffects_of_missed_dosage',
        'effects_of_overdose',
        'expert_advice',
        'how_to_use',
        'faqs'   
    ];
 
     public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'medicine_name'
            ]
        ];
    }

   
    public function ProductsImage(){
     return $this->hasMany('App\Models\ProductImageTemp',  'product_id', 'p_id');
    }

    public function s(){
        return $this->morphMany('App\Models\ProductImageTemp', 'image');
    }


}
