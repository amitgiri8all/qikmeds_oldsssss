<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\Order;
use App\Models\OrderItem;
 use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory,HasApiTokens, Notifiable,SoftDeletes;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'first_name',
        'last_name',
        'otp',
        'mobile_number',
        'password',
        'city',
        'state',
        'country',
        'google_id',
        'address',
        'address_location',
        'latitute',
        'longitute',
        'device_type',
        'device_token',
        'device_id',
        'referrer_code',
        'registration_number',
        'pin_code',
        'image',
        'dob',
        'gender',
        'is_subscription','wallet_amount','farm_name','subscription_plan_id','expire_date_subscription',
         'comm_medicine',
        'comm_beauty_pro',
        'comm_device_surgiacl','zone_id','zone_date'
    ];

      protected $appends = ['pastorder','itemsbought','totalsaving'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password',
        'remember_token',
    ];
*/
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function driverditems()
    {
        return $this->hasMany(Driver::class, 'user_id', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

     public function getIsAdminAttribute()
    {
        return $this->roles()->where('id', 1)->exists();
    }

    public function getImageAttribute($value = ""){
        //if(!empty($value) && file_exists(asset('storage/app/public/upload/Thumbnail') . "/" .$value)){
        if(!empty($value)){
            return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
    }

     public function VendorOrderItem()
    {
        return $this->hasOne('App\Models\OrderItem', 'vendor_id', 'id');
    } 


    public function DriverData()
    {
        return $this->hasOne('App\Models\Driver', 'user_id', 'id');
    } 
    public function DriverDataBank()
    {
        return $this->hasOne('App\Models\PaymentMethod', 'user_id', 'id');
    } 
    
    public function SubscriptionPlanUser()
    {
        return $this->hasOne('App\Models\SubscriptionPlan', 'id', 'subscription_plan_id');
    } 



    public function gettoTalsavingAttribute($value=null)
    {
       
      // $data = Order::where('order_status','D')->where('user_id',@Auth::guard('api')->user()->id)->first('discount_amount');
        $data = 500;
       if(!empty($data)){
        return $data; 
       }else{
        return 0;
       } 
    }

     public function getPastOrderAttribute($value=null)
    {
       
      $data = Order::where('order_status','D')->where('user_id',@Auth::guard('api')->user()->id)->count();
       if(!empty($data)){
        return $data; 
       }else{
        return 0;
       } 
    }  
    
    public function getItemsBoughtAttribute($value=null)
    {
       
        $order = Order::select('id')->where('order_status','D')->where('user_id',@Auth::guard('api')->user()->id)->get();
            $data = 0;
            foreach($order as $key=>$val){
              $data += OrderItem::where('order_id',$val->id)->count();
            }
        if(!empty($data)){
          return $data; 
        }else{
          return 0;
        } 
    }  

   

}
