<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use DB;
class Links extends Model
{
    use HasFactory;
    protected $table = 'manager';
    public $timestamps = true;

    public static function getParentMenu()
    {
		return Links::select('id','name','link','parent_id','icon','table_type')
		->where('parent_id','0')
		->where('status','Active')
		->orderBy('order','ASC')
		->get();
	}
	public static function maincatdata(){
		return array();
	}

	public static function getSubMenu($parentid){
		$submenu =  Links::select('id','name','link','parent_id','icon')
		->where('parent_id',$parentid)->where('status','Active')
		->get();
		return $submenu;
	}

	public static function menupermission(){
	// echo Auth()->user()->id; die("ok sure thanks");
		//DB::enableQueryLog(); // Enable query log
		$permission =  Role::select('permissions')
		->where('id',Auth()->guard('admin')->user()->role_id)
		->first();
		$permissionarr = explode(',',$permission->permissions);
		return $permissionarr;
	}


	 

    public static function getpermissionsmenu($permissionId){
		$permitedlinks = Links::select('link')
		->where('parent_id','!=','0')
		->where('status','Active')
		->whereIn('id',$permissionId)
		->get();
		$links = [];
		foreach($permitedlinks as $key=> $value){
			$links[$key] = $value->link;
		}
		return $links;
	}

		public static function getActionMenu($parentid){
		//echo $parentid; die();
//DB::enableQueryLog(); // Enable query log
		$submenu =  DB::table('permissions')->select('id','name','showing_name','manager_id')
		->where('manager_id',$parentid)
		->get();
		//echo "<pre>"; print_r(DB::getQueryLog()); die;
		//echo "<pre>"; print_r($submenu); die;
		return $submenu;
	}

/*	public static function getActionMenu($subid){
		$submenulist =  DB::table('permissions')
  		->select('id','manager_id','name','showing_name','guard_name')
		->where('manager_id',$subid)
		->get();
		return $submenulist;
	}*/

}
