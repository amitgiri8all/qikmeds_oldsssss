<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ManufacturerTmp extends Model
{
     use HasFactory;
 use Sluggable;
 
      protected $fillable = [
        'id','manufacturer_name','slug'   
    ];
 
     public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'manufacturer_name'
            ]
        ];
    }
 

}
