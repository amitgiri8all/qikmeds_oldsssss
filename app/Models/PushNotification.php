<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model 
{
    use HasFactory;
  
    protected $table = 'push_notification_message';

    protected $fillable = [
        'order_accept','order_place','order_cancel','order_return','order_shipped','order_deliverd',
    ];

 

}
