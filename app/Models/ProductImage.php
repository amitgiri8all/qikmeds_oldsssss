<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProductImage extends Model
{
    use HasFactory,SoftDeletes;
     
    protected $table = "product_image";

     protected $fillable = [
        'prod_id', 'image', 'caption','order','set_primary',
    ];


    
   /* public function getImageAttribute($value = ""){
        //if(!empty($value) && file_exists(asset('storage/app/public/upload/qikmeds') . "/" .$value)){
        if(!empty($value)){
            return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
    }*/
}
