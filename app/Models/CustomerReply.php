<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CustomerReply extends Model
{
    use HasFactory,SoftDeletes;
     
    protected $table = "customer_reply";
    protected $fillable = [
       'user_id','complaint_id','reply'
    ];
}
