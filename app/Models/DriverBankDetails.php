<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverBankDetails  extends Model
{
    use HasFactory;

    protected $table = 'driver_bank_details';

    protected $fillable = ['user_id', 'account_holder_name','account_number','ifsc_code'];
}
