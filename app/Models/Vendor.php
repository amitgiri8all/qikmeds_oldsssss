<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;
     
    protected $table = "vendor";
    protected $fillable = [
        'user_id', 'licensed_pharmacist_details', 'authorized_pharmacy_related_documents',
    ];
}
