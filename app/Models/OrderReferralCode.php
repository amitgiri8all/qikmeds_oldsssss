<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderReferralCode extends Model
{
    use HasFactory;

    protected $table = 'order_referral_code';

    protected $fillable = [
        'order_id','form_user_id','to_user_id','referral_code'
    ];
}
