<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Brand extends Model
{
        use Sluggable;

    use HasFactory;

       protected $table = 'brand';

 
    protected $fillable = [
        'brand_name','slug','image','brand_banner','brand_slogan','status'
    ];

    protected $appends = ['brandTot'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'brand_name'
            ]
        ];
    }

    public function getImageAttribute($value = ""){
        if(!empty($value)){
            return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
    }

    public function getbrandTotAttribute($value=null)
    {
      $data = Product::where('brand',$this->id)->count();
       if(!empty($data)){
        return $data; 
       }else{
        return 0;
       } 
    }


}
