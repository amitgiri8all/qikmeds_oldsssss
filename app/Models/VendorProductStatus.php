<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorProductStatus extends Model
{
    use HasFactory;

    protected $table = 'vendor_product_status';

    protected $fillable = [
        'order_id','vendor_id','status'
    ];

}
