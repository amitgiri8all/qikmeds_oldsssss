<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImageTemp extends Model
{
    
    use HasFactory;

    protected $table = "product_image_temps";

     protected $fillable = [
        'product_id','prod_id', 'image', 'caption','order','set_primary',
    ];



    
}
