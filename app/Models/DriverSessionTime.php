<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverSessionTime extends Model
{
    use HasFactory;

    protected $table = 'driver_session_time';

    protected $fillable = ['user_id', 'start_time','end_time','is_online_status'];
}
