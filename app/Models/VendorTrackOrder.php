<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorTrackOrder extends Model
{
    use HasFactory;

    protected $table = 'vendor_track_order';
 
    protected $fillable = [
        'order_id','track_status','date_time','vendor_id'
    ];
}
