<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryAddress extends Model
{
    use HasFactory;

    protected $table = 'delivery_address';

    protected $fillable = [
        'name','mobile_number','pin_code','locality','user_id','address_delivery','city','landmark','alternate_phone','address_type','status','state','latitute','longitute'
    ];
    
}
