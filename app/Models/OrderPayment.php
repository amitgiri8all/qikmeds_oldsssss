<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    use HasFactory;

    protected $table = 'order_payment';

    protected $fillable = [
       'ord_pay_id','order_id','total_amount','payment_method','created_at','updated_at','deleted_at','site_id','ip'
    ];
    
}
