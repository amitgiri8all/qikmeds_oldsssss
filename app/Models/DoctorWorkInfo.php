<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorWorkInfo extends Model
{
    use HasFactory;

     protected $table = 'doctor_work_info';

 
    protected $fillable = [
        'doctor_id','woking_place_name','start_year','end_year'
    ];
}
