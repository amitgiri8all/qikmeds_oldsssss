<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    use HasFactory;

    public function is_role(){
        return $this->belongsTo('App\Models\Role','role_id','id')->select(array('id','name','is_backend'))->where('is_backend','1')->where('slug','!=','admin');
    }
    
    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
