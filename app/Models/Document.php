<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Document extends Model
{
    use HasFactory;
     
    protected $table = "vendor_document";
    protected $fillable = [
         'user_id', 'drug_license', 'gstin','signature','bank','educational_certificate',
    ];
}
