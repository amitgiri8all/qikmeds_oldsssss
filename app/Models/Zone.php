<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Zone extends Model 
{
    use HasFactory;

    protected $table = 'zones';

    protected $fillable = [
        'name','code','point','delivery_charges','package_id','description','status','is_default'
    ];

    /**
     * @param $method
     * @return array
     */
  
    public function getPointsAttribute($value){
        $result=[];
        $value = ltrim($value,'POLYGON((');
        $value = rtrim($value,'))');
        $value = explode(',',$value);
        foreach ($value as $val){
            $val = explode(' ',$val);
            $result[]=['lat'=>(double)$val[0],'lng'=>(double)$val[1]];

        }
        return $result;

    }

    public function setPointAttribute($value)
    {
        $points = json_decode($value,true);
        $polygon="PolygonFromText('POLYGON((";
        foreach ($points as $key=>$point){
            if($key==0){
                $first_point = $point['lat']." ".$point['lng'];
            }
            $polygon.= $point['lat']." ".$point['lng']." , ";

        }
        $polygon.=$first_point." ))')";

        $this->attributes['point'] = DB::raw($polygon);
    }
    



} 