@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

  <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/line-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/slick.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css"/>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Help</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                <div class="col-sm-12">
                    <div id="parentVerticalTab" class="resp-vtabs hor_1" style="display: block; width: 100%; margin: 0px;">
                        <div class="row">
                            <div class="col-sm-3">
                                <ul class="resp-tabs-list hor_1" style="margin-top: 3px;">
                                    <li class="resp-tab-item hor_1 resp-tab-active" aria-controls="hor_1_tab_item-0" role="tab" style="background-color: white; border-color: rgb(193, 193, 193);">Order Enquiry <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-1" role="tab" style="background-color: rgb(245, 245, 245);">Delivery <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-2" role="tab" style="background-color: rgb(245, 245, 245);">Payments <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-3" role="tab" style="background-color: rgb(245, 245, 245);">Returns <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-4" role="tab" style="background-color: rgb(245, 245, 245); border-color: rgb(193, 193, 193);">Subscription <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-5" role="tab" style="background-color: rgb(245, 245, 245);">General Enquiries <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-6" role="tab" style="background-color: rgb(245, 245, 245);">Qikmeds Wallet <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-7" role="tab" style="background-color: rgb(245, 245, 245);">Promotions <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-8" role="tab" style="background-color: rgb(245, 245, 245);">Business Opportunities <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-9" role="tab" style="background-color: rgb(245, 245, 245);">Referrals <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-10" role="tab" style="background-color: rgb(245, 245, 245);">Assisted Ordering <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-11" role="tab" style="background-color: rgb(245, 245, 245);">Prescription Query <i class="arrow ti-angle-right"></i></li>
                                    <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-12" role="tab" style="background-color: rgb(245, 245, 245);">Online Consultation <i class="arrow ti-angle-right"></i></li>
                                </ul>
                            </div>
                        <div class="col-sm-9">
                            <div class="resp-tabs-container hor_1" style="border-color: rgb(193, 193, 193);">
                                <h2 class="resp-accordion hor_1 resp-tab-active" role="tab" aria-controls="hor_1_tab_item-0" style="background-color: white; border-color: rgb(193, 193, 193);"><span class="resp-arrow"></span>Order Enquiry <i class="arrow ti-angle-right"></i></h2><div class="resp-tab-content hor_1 resp-tab-content-active" aria-labelledby="hor_1_tab_item-0" style="display:block">
                                <div class="faq_content">
                                    <h2>Order Enquiry</h2>
                                    <div id="accordion">
                                      <div class="card">
                                        <div class="card-header" id="heading1">
                                            <a href="#accordion" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">How do I cancel my order?</a>
                                        </div>

                                        <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>You can cancel your order by logging into your Netmeds account and clicking on "Order History". Click on the details of the order you wish to cancel and hit the "Cancel" button.</p>
                                            <p>If your order is "In Transit" you must wait until the order reaches you and if you still wish to cancel, simply tell the courier delivery person that you "refuse delivery", and the order will then cancel.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading2">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">How can i track my order?</a>
                                        </div>
                                        <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>Once you login to your Netmeds account, you can check the status of your orders under "Order History". Click on "Details" and you will be able to see the status of your shipment.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading3">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">Items are missing from my order?</a>
                                        </div>
                                        <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>We regret the inconvenience. Please do email us with the following details. We will get back to you soon.<br>
                                            Order ID:<br>
                                            Missing item(s) with quantity:</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading4">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">I want to modify my order?</a>
                                        </div>
                                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>Once your order is confirmed, it cannot be modified. If it is not yet showing as "In Transit" you may cancel it and place a fresh order with your modifications.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading5">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">Items are different from what I ordered?</a>
                                        </div>
                                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>We are sorry. Kindly email us with the following details. We will get back to you shortly.<br>
                                            Order ID: Name of the item(s) you had ordered,<br>
                                            Name of the item(s) received:</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading6">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">How do i get an invoice from order?</a>
                                        </div>
                                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>To get the invoice(s) for your order(s), go to "My Orders." Click on the appropriate order(s) and you will find an option to download the invoice(s) in the PDF format.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading7">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">How can i do online payment?</a>
                                        </div>
                                        <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>You can make an online payment through debit card / credit card / netbanking. We do have various wallet options for payments as well. We can also send you a paylink as well.</p>
                                            <p>Note: You will not be able to use wallet options when you make an online payment using the paylink sent by our executives.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading8">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">When will i receive my order?</a>
                                        </div>
                                        <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>Your order will be delivered within the 'Estimated Delivery Date' indicated when you place your order. You can also check this date by selecting your order from the "My Orders" section. We will keep your updated about delivery dates through mails and SMSes.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading9">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">I have received Damaged products.</a>
                                        </div>
                                        <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
                                          <div class="card-body">
                                            <p>We are sorry. Please do email us with the following details. We will get in touch with you soon.<br>
                                            Order ID:<br>
                                            Damaged product(s) with quantity:</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-1" style="background-color: rgb(245, 245, 245); border-color: rgb(193, 193, 193);"><span class="resp-arrow"></span>Delivery <i class="arrow ti-angle-right"></i></h2><div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-1" style="border-color: rgb(193, 193, 193);">
                                <div class="faq_content">
                                    <h2>Delivery</h2>
                                    <div id="accordion1">
                                      <div class="card">
                                        <div class="card-header" id="heading11">
                                            <a href="#accordion" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">Order status shows 'Delivered' but I have not received my order.</a>
                                        </div>

                                        <div id="collapse11" class="collapse show" aria-labelledby="heading11" data-parent="#accordion1">
                                          <div class="card-body">
                                            <p>We are sorry. Please email the details asked for below and we will get back to you shortly.<br>
                                            Order ID:<br>
                                            Tracking number for the "Delivered" item(s):</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading12">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">Which cities do you operate in?</a>
                                        </div>
                                        <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion1">
                                          <div class="card-body">
                                            <p>We deliver anywhere within India.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading13">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">Can I modify my address after I have placed my order?</a>
                                        </div>
                                        <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion1">
                                          <div class="card-body">
                                            <p>You will not be able to modify your address after the order has been placed. You can cancel the order and place a fresh one with the right address.</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-2" style="background-color: rgb(245, 245, 245); border-color: rgb(193, 193, 193);"><span class="resp-arrow"></span>Payments <i class="arrow ti-angle-right"></i></h2><div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-2" style="border-color: rgb(193, 193, 193);">
                                <div class="faq_content">
                                    <h2>Payments</h2>
                                    <div id="accordion2">
                                      <div class="card">
                                        <div class="card-header" id="heading21">
                                            <a href="#accordion" data-toggle="collapse" data-target="#collapse21" aria-expanded="true" aria-controls="collapse21">What are the payment modes at Qikmeds?</a>
                                        </div>

                                        <div id="collapse21" class="collapse show" aria-labelledby="heading21" data-parent="#accordion2">
                                          <div class="card-body">
                                            <p>We support the following payment options:<br>
                                                1. Credit Card<br>
                                                2. Debit Card<br>
                                                3. Net Banking<br>
                                                4. Digital Wallets<br>
                                                5. Cash on Delivery (COD)<br>
                                                6. UPIs (PhonePe, Mobikwik, Paytm etc.)
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading22">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapse22">When will I get my refund?</a>
                                        </div>
                                        <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#accordion2">
                                          <div class="card-body">
                                            <p>Once the refund has been initiated, the amount is expected to reflect in your account as per the following timelines:<br>
                                                1. Qikmeds Wallet - Same day<br>
                                                2. NEFT - 1-3 working days from the date of refund initiation<br>
                                                3. Online refund - 7-10 working days from the date of refund initiation<br>
                                                4. PhonePe and other wallets - 48 hours from the date of refund initiation<br>
                                                Please note that this timeline depends on your banking partner as well.
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading23">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapse23">How can I make an online payment?</a>
                                        </div>
                                        <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#accordion2">
                                          <div class="card-body">
                                            <p>You can make an online payment through your debit / credit card / Netbanking. We do have various wallet options for payment as well. We can also send you a paylink for making the payment.</p>
                                            <p>Note: You will not be able to use wallet options when you make an online payment using the paylink sent through our executives.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <div class="card-header" id="heading24">
                                            <a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse24" aria-expanded="false" aria-controls="collapse24">Why am I not able to find Amazon Pay when I want to complete my order?</a>
                                        </div>
                                        <div id="collapse24" class="collapse" aria-labelledby="heading24" data-parent="#accordion2">
                                          <div class="card-body">
                                            <p>If there are any sexual wellness products in your order, you will not be able to make the payment through Amazon Pay. You can choose from our other modes of payment in this case.</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12 text-center mt_20"><a href="contact-us.html" class="cmn_btn">Still Need Help</a></div>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 



@stop
@section('footer_scripts')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
 <script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/custom.js')}}"></script>
 
 
@stop
