<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
 use Auth;
class Permission extends Model
{
    use HasFactory;

        protected $table = 'permissions';

        static function checkPermission($routes){ 
 //echo $routes; die();
        $role = Auth()->guard('admin')->user()->role_id;
         //echo $role; die();
         $search = array('.data','.post','.delete-confirm','.search','.changestatus','.update-status-confirm');
        $replace = array('');
        $routes = str_replace($search, $replace, $routes);
         //echo "<pre>"; print_r($routes); die();
        $check = self::where('name',$routes)
                 ->where('role_id',$role)
                 ->join('role_has_permissions','role_has_permissions.permission_id','=','permissions.id')
                 ->count();
        if($check > 0 || $routes=="admin.dashboard" || $routes=="admin/profile" || $routes=="roles.index" || $routes=="roles.edit"|| $routes=="roles.update"){
            return true;
        }else{
            return false;
        }                 
    }
}
