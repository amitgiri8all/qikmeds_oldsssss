<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Blog extends Model
{
    use HasFactory;
       use Sluggable;

      protected $table = 'blog';

    protected $fillable = [
        'title','slug','image','status','description','status'
    ];

    public function getImageAttribute($value = ""){
            if(!empty($value)){
                return asset('storage/app/public/upload/Thumbnail') . "/" .$value;
            }else{
                return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
            }
    }

     public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


}
