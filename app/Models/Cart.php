<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

      protected $table = 'cart';

 
    protected $fillable = [
        'user_id','product_id','qty','type'
    ];



    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    public function ProductImage()
    {
     return $this->hasOne('App\Models\ProductImage',  'product_id', 'product_id')->groupBy('product_id');
    }

    public function SubscribePlan()
    {
        return $this->belongsTo('App\Models\SubscriptionPlan', 'plan_id', 'id');
    }

}
