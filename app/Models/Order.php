<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'order';

    protected $fillable = [
        'state','status','order_code','patient_id','user_id','shipping_description','site_id','discount_amount','discount_coupon_code','sub_total','grand_total','shipping_amount','shipping_tax_amount','tax_amount','total_payed_amount','total_saving','email_status','address_id','order_currency_code','shipping_method','total_item','created_at','updated_at','deleted_at','ip','payment_mode_id','coupon_code','coupon_amount','transaction_id','transaction_status','wallet_payment','online_payment','subscription_plan_amount','sub_paln_id','order_status','invoice_pdf','return_days','delivered_date','offer_price','offer_code','doctor_id','prescription_image','patient_info','delivery_address_info','offer_data','offer_id','offer_status','customer_notes','is_prescription'
    ];

      public function getInvoicePdfAttribute($value = ""){
        //if(!empty($value) && file_exists(asset('storage/app/public/upload/Thumbnail') . "/" .$value)){
        if(!empty($value)){
            return asset('storage/app/public/') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
   } 

      /*public function getPrescriptionImageAttribute($value = ""){
        //if(!empty($value) && file_exists(asset('storage/app/public/upload/Thumbnail') . "/" .$value)){
        if(!empty($value)){
            return asset('storage/app/public/') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
   } */

       public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    } 
    
    public function Driver()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

     public function OrderPayment()
    {
        //return $this->belongsTo('App\Models\OrderPayment', 'id', 'ord_pay_id');
         return $this->hasOne('App\Models\OrderPayment');
    }


    public function UserPrescription()
    {
        
      return $this->hasOne('App\Models\UserPrescription', 'user_id' , 'user_id');
    }

     public function OrderItem()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id', 'id')/*->whereNull('vendor_id')*/;
    }   

    public function OrderItemDriver()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id', 'id')->where('prescription',1);
    }  

     public function OrderItemVendorNull()
    {
        return $this->hasMany('App\Models\OrderItem','order_id','id')->whereNull('vendor_id');
    } 

    public function DeliveryAddress()
    {
        return $this->belongsTo('App\Models\DeliveryAddress', 'address_id', 'id');
    }

     public function ProductOrderItem()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id', 'id');
    }

}
