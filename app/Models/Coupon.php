<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    use HasFactory;
    protected $table = 'coupons';

    protected $fillable = ['code','name','coupon_type','coupon_value','coupon_minimum_value','to_time','from_time','number_of_use','status'];

}
