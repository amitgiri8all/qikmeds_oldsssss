<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqTopics extends Model
{
    use HasFactory;

          protected $table = 'faq_topics';
 
    protected $fillable = [
        'name',
    ];

    public function faqData()
    {
        return $this->hasMany('App\Models\Faq', 'faq_topic_id', 'id');
    }

}
