<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class DeclineProducts extends Model
{
    use HasFactory;
        protected $table = 'decline_order_products';

 
    protected $fillable = [
        'ord_product_item_id','vendor_id','order_id','type','user_id'
    ];


    public static function remove_decline_order_products($ord_product_item_id=null,$vendor_id=null,$order_id=null){

        //echo $ord_product_item_id.'=========>vendor_id======>'.$vendor_id.'====>order_id=====>'.$order_id; die();
         $result = DB::table('decline_order_products')->where('type','vendor')->where('ord_product_item_id',$ord_product_item_id)->where('user_id',$vendor_id)->where('order_id',$order_id)->count();
         //echo $result; die();
         if($result>0){
          return false;
         }else{
          return true;
         }
    }

    public static function remove_order_by_vendor($vendor_id=null,$order_id=null){
      $result = DB::table('decline_order_products')->where('type','vendor')->where('user_id',$vendor_id)->where('order_id',$order_id)->count();
         if($result>0){
          return false;
         }else{
          return true;
         }
    } 

    public static function remove_order_by_doctor($doctore_id=null,$order_id=null){
      $result = DB::table('decline_order_products')->where('type','doctor')->where('user_id',$doctore_id)->where('order_id',$order_id)->count();
         if($result>0){
          return false;
         }else{
          return true;
         }
    }

}


