<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Tag extends Model
{
    use HasFactory;
    use Sluggable;
        protected $table = 'tag';

         public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'tag_name'
            ]
        ];
    }
}
