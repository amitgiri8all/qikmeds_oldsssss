<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\SaveForLater;
use App\Models\Cart;
use App\Models\Wishlist;
use App\Models\Manufacturer;
use Auth;
class Product extends Model
{
   use HasFactory,Sluggable;

  protected $fillable = [
        'id',
        'p_id',
        'medicine_name',
        'mrp',
        'sale_price',
        'discount',
        'igst',
        'hsn',
        'manufacturer_id',
        'composition',
        'packing_type',
        'packaging',
        'schedule',
        'sellable',
        'prescription_required',
        'usage',
        'about_salt',
        'mechanism_of_action',
        'pharmacokinets',
        'onset_of_action',
        'duration_of_action',
        'half_life',
        'side_effects',
        'contra_indications',
        'special_precautions_while_taking',
        'pregnancy_related_Information',
        'product_and_alcohol_interaction',
        'old_age_related_information',
        'breast_feeding_related_information',
        'children_related_information',
        'indications',
        'typical_dosage',
        'storage_requirements',
        'fffects_of_missed_dosage',
        'effects_of_overdose',
        'expert_advice',
        'how_to_use',
        'faqs','deal_of_the_day'   
    ];
 
     public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'medicine_name'
            ]
        ];
    }
    
    protected $appends = ['cart_qty','wishlist'];


    public static function CheckQty($product_id=null)
    {
         $quantity = 0;
        foreach((array) session('cart') as $key => $details){
           if($product_id==$details['id']){ 
             $quantity = $details['quantity'];
           } 
        } 
       return $quantity;
    }

    public function getImageAttribute($value = ""){
        //echo $value; die('okkk');
        if(!empty($value)){
            return asset('storage/app/public/qikmeds') . "/" .$value;
        }else{
            return 'http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg';
        }
    }


    public function getCartqtyAttribute($value=null)
    {
      $data = @Cart::where('product_id',$this->id)->where('user_id',Auth::guard('api')->user()->id)->first()->qty;
       if(!empty($data)){
        return $data; 
       }else{
        return 0;
       } 
    } 
     public function getwishlistAttribute($value=null)
    {
       
      $data = @Wishlist::where('product_id',$this->id)->where('user_id',Auth::guard('api')->user()->id)->first();
       if(!empty($data)){
        return 1; 
       }else{
        return 0;
       } 
    }
 

    
    public function images()
    {
        return $this->morphMany('App\Models\ProductImage', 'image');
    }
    public function image()
    {
        return $this->morphOne('App\Models\ProductImage', 'image');
    }

    public function ProductImage(){
     return $this->hasOne('App\Models\ProductImage',  'product_id', 'id')->groupBy('product_id');
    }

    public function ManufacturerName(){
     return $this->hasOne('App\Models\Manufacturer',  'id', 'manufacturer_id');
    }

   

     
  

 

}
