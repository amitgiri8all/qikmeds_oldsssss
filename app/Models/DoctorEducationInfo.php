<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorEducationInfo extends Model
{
    use HasFactory;

      protected $table = 'doctor_education_info';

 
    protected $fillable = [
        'doctor_id','college_name','subject','start_year','end_year'
    ];
}
