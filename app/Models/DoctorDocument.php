<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class DoctorDocument extends Model
{
    use HasFactory;
     
    protected $table = "doctor_document";
    protected $fillable = [
         'doctor_id', 'doctor_license','educational_certificate'
    ];
}
