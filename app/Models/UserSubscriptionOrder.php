<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSubscriptionOrder extends Model
{
    use HasFactory;

     protected $table = 'user_subscription_order';

    protected $fillable = [
        'user_id','total_amount','plan_id','transaction_id'
    ];

}
