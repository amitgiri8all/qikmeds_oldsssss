<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorProductStatus extends Model
{
    use HasFactory;

    protected $table = 'doctor_product_status';

    protected $fillable = [
        'order_id','doctor_id','status'
    ];

}
