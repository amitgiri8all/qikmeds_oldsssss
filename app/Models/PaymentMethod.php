<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory;

        protected $table = 'payment_method';
        protected $fillable=['card_number','cvc','user_id','expiration_month','bank_name','expiration_year','cardholder_name','image','ifce_code'];
}
