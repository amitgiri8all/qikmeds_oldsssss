<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorSpecializationsInfo extends Model
{
    use HasFactory;

     protected $table = 'doctor_specializations_info';
 
    protected $fillable = [
        'doctor_id','specializations_name'
    ];

    
}
