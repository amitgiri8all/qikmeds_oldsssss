<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class OrderItem extends Model
{
    use HasFactory;

    protected $table = 'order_item';

    use SoftDeletes;

     protected $softDelete = true;

    protected $fillable = [
     'ord_item_id','order_id','product_id','cart_id','vendor_id','doctor_id','product_option','product_sku','product_name','product_description','quantity_invoice','quantity_order','quantity_cancelled','quantity_refund','quantity_shipped','seller_shipment_status','shipment_updated_by','price','base_price','orignal_price','tax_percentage','tax_amount','shipping_cost','row_total','price_include_tax','rowtotal_includetax','admin_commission','commission_amount','transaction_status','seller_id','created_at','updated_at','deleted_at','site_id','ip','morning','night','evening','qty','product_price','data','driver_id','is_return','reason','prescription','advise'
    ];
        protected $dates = ['deleted_at'];


     public function ProductOrderItem()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    static function remove_order_product_by_doctor($user_id=null,$order_id=null){
        //echo $order_id; die();
        $data =  DB::table('order_item')->where('order_id',$order_id)->whereNotNull('deleted_at')->get();
      //  return $data;
       // echo "<pre>"; print_r($data); die();
    }
    

}
