<?php 

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\DeliveryAddress;
use App\Notifications\OrderPlace;
use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\OrderItem;
use App\Models\User;
use App\Models\PatientInfo;
use App\Models\UserWallet;
use App\Models\Product;
use App\Models\Sitesettings ;
use DateTime;
trait HelperTrait
{
	//Create Order Code ====================//
	 public function orderCode() {
          $name = Auth::guard('web')->user()->name;
          $id = Auth::guard('web')->user()->id;
          $txt = rand('000','999');
        return $txt.strtoupper(substr($name, 0, 4)).$id;
    }
      public function orderCodeApi() {
          $name = Auth::guard('api')->user()->name;
          $id = Auth::guard('api')->user()->id;
          $txt = rand('000','999');
        return $txt.strtoupper(substr($name, 0, 4)).$id;
    }
    //Return  patient id ====================//
     public function patient_id() {
         $user_id = Auth::guard('web')->user()->id;
        $patient_id = PatientInfo::where('status',1)->where('user_id',$user_id)->first('id');
         return (!empty( $patient_id->id )) ?  $patient_id->id  : NUll;
     }
          // Patient Info
 public function patient_info() {
         $user_id = Auth::guard('web')->user()->id;
        $patient_id = PatientInfo::where('status',1)->where('user_id',$user_id)->first();
         return (!empty( $patient_id )) ?  $patient_id  : "";
     }
   // Patient Info
 public function patient_info_api() {
         $user_id = Auth::guard('api')->user()->id;
        $patient_id = PatientInfo::where('status',1)->where('user_id',$user_id)->first();
         return (!empty( $patient_id )) ?  $patient_id  : "";
     }

     public function get_delivery_time(){
      $time =  Sitesettings::select('lock_delivery_times')->where('id',1)->first();  
     if($time->lock_delivery_times>date("G:i")){
        $time = date("Y-m-d");
     }else{
        $datetime = new DateTime('tomorrow');
        $time = $datetime->format('Y-m-d');
     }
     return $time;
     }


    public function notifyadmin($add)
   {
    
    $created = Carbon::now()->addMinutes($add);
    $now = Carbon::now();
   // echo $created; die();
    $days = 1440*$created->diff($now)->days;
    $hours = 60*$created->diff($now)->h;
    $minutes = $days + $hours + $created->diff($now)->i;
    //echo $minutes; die();
    $user = 1;
    return "ok send message";
    //$user->notify((new notifyme($user))->delay($minutes));
    //return redirect('/notif');
    }


    public function text_local_send_otp(){
        $OTP = rand(1000,9999);

        $apiKey = urlencode('Mzg2ODMzNGY3MzM5NzkzODRkNDU1MDU1NTg3MTY3NDI=');

        // Message details
        $var= $OTP;

        //10 digit phone number with country code 91
        //$numbers = '9549837077';
        $sender = urlencode('123456');
        $message = rawurlencode("Dear User, your OTP login is ".$var." . This OTP is valid only for 24 hours.");

        $numbers = 9549837077;

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;

    }


    public function CouponAmount($coupon_type,$coupon_value,$sub_total){
        //echo $coupon_type.'====='.$coupon_value.'===='.$sub_total; die();
        if($coupon_type=='percentages'){
           $coupon_code_amount =  $sub_total * $coupon_value/100;
        }else{
           $coupon_code_amount = $coupon_value;
        }
        return $coupon_code_amount;
    }

    public function OfferCodeAmount($banner_type,$offer_value,$sub_total){
        if($banner_type=='percentages'){
           $offer_code_amount =  $sub_total * $offer_value/100;
        }else{
           $offer_code_amount = $offer_value;
        }
        return $offer_code_amount;
    }



    public static function updateCustomerWallet($customer_id,$amount,$transaction_type,$type,$transaction_id,$description,$json_data=null,$order_id=null){
        $user = User::findOrFail($customer_id);
        if(isset($user)){
            if($transaction_type == "CREDIT"){
                $wallet_amount = $user->wallet_amount;
                $wallet_amount += $amount;
                $user->wallet_amount = $wallet_amount;
                $user->save();
            }else if($transaction_type=="DEBIT"){
                $wallet_amount = $user->wallet_amount;
                $wallet_amount -= $amount;
                $user->wallet_amount = $wallet_amount;
                $user->save();
            }

            $wh                     = new UserWallet();
            $wh->user_id            = $customer_id;
            $wh->amount             = $amount;
            $wh->transaction_type   = $transaction_type;
            $wh->type               = $type;
            $wh->transaction_id     = $transaction_id;
            $wh->description        = $description;
            $wh->data               = $json_data;
            $wh->order_id           = $order_id;

            $wh->save();

            return true;
        }else{
            return false;
        }
    } 

    public static function updateCustomerWalletAmt($customer_id,$amount,$transaction_type,$type,$transaction_id,$description,$json_data=null,$order_id=null){
        $user = User::findOrFail($customer_id);
        if(isset($user)){
            $wh                     = new UserWallet();
            $wh->user_id            = $customer_id;
            $wh->amount             = $amount;
            $wh->transaction_type   = $transaction_type;
            $wh->type               = $type;
            $wh->transaction_id     = $transaction_id;
            $wh->description        = $description;
            $wh->data               = $json_data;
            $wh->order_id           = $order_id;

            $wh->save();

            return true;
        }else{
            return false;
        }
    }


      public function outOfStock($Id) {
        try{
            $qty=0;
            $product = Product::select('*')->where('id','=',$Id)->first();
            if(!empty($product)){
                if($product['qty'] > 0){ 
                    $qty = $product->qty; 
                }
            }
            return $qty;
        } catch (\Exception $e) {
            return $e;
        }
        return 0;
    }
   

}

?> 
