<?php
namespace App\Helpers;
use DB;
use Session;
use App\Models\User;
use App\Models\DoctorWorkInfo;
use App\Models\DoctorEducationInfo;
use App\Models\VendorProductStatus;
use App\Models\DoctorSpecializationsInfo;
use App\Models\DeliveryAddress;
use App\Models\Product;
use App\Models\Category;
use App\Models\OrderItem;
use App\Models\Notification;
use App\Models\ImageProduct;
use App\Models\Sitesettings;
use App\Models\ProductImage;
use App\Models\UserPrescription;
use App\Models\Frontcontain;
use App\Models\Page;
use DateTime;
use Auth;
class ProductHelper {

	//Category Management
	public static function category($limit=10){
		$category = Category::select('category_name','id','slug','image')
		->limit($limit)
		->orderBy('id','ASC')
        ->get();
        return $category;
	} 

	//Frontcontain Management
	public static function Frontcontain($type=1){
		$icon =Frontcontain::where('status','!=','0')->where('type',1)->orderBy('id','ASC')->get();
        return $icon;
	} 

	public static function health_products(){

	      $query = Product::select('products.p_id','products.medicine_name','products.slug','products.mrp','products.sale_price','products.discount','product_image.image','categories.category_name')
	      		->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
	            ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
	            ->Join('product_image', function($join)
	           {
	              $join->on('product_image.product_id', '=', 'products.p_id');
	              $join->limit(1);
	           })
	          ->groupBy('product_image.pro_img_id')->limit(10);
	        $productDetail=$query->get();
          return $productDetail;
	}
	//Cart
	 public static function product_session($id){
         $query = Product::select('products.*','product_image.image','categories.category_name','manufacturer.manufacturer_name')
         ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->leftJoin('product_image', function($join)
        {
	        $join->on('product_image.product_id', '=', 'products.p_id');
	        $join->limit(1);
        })
        ->where('products.id',$id)
        ->groupBy('product_image.pro_img_id');
        $data = $query->first();
        return $data;
    }

    /*================================ API Products ==================================== */

    public static function ProductListByType($type){
    	 /*
	        1.Deal of the day
	        2.Health Product
	        3.Most Selling
        */					
	     $query = Product::select('products.*','product_image.image','categories.category_name','manufacturer.manufacturer_name')
	      		->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
	            ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
                ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
	            ->leftJoin('product_image', function($join)
	           {
	            $join->on('product_image.product_id', '=', 'products.p_id');
	            $join->limit(1);
	           })->groupBy('product_image.product_id');
	           if($type==1){
	           	 $query->where('products.deal_of_the_day',1);
	           }
	           if($type==2){
	           	 $query->where('categories.id',3);
	           }
	        $productDetail=$query->limit(10)->get();
            return $productDetail;
	}
    	





}
?>