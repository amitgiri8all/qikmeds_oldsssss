<?php
namespace App\Helpers;
use DB;
use Session;
use App\Models\User;
use App\Models\DoctorWorkInfo;
use App\Models\DoctorEducationInfo;
use App\Models\VendorProductStatus;
use App\Models\DoctorSpecializationsInfo;
use App\Models\DeliveryAddress;
use App\Models\Product;
use App\Models\OrderItem;
use App\Models\Notification;
use App\Models\Sitesettings;
use App\Models\ProductImage;
use App\Models\UserPrescription;
use App\Models\Page;
use DateTime;
use Auth;
class Helper {

	public static function delivery_charege(){
	  try {

	    $delivery_charege = DB::table('delivery_charge')->select('*')->where('id',1)->first();
        return $delivery_charege;
      }
	  catch(\Exception $e) {
	            Session::flash('error', $e->getMessage());
	  }
	  return 0;	
    }

    //Show Card Numebr
	public static function ccMasking($number, $maskingCharacter = '*') {
		return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
	}

	public static function UnreadNotifications($id){
		return  $user= User::find($id);
	}

	public static function UnreadNotificationsUser($id){
	$userNotification = Notification::where([ ['notifiable_id','=',$id],['read_at','=',Null] ])->get();
    return $userNotification;
		//return  $user= Notification::find($id)->whereNull('read_at')->where('',);
	}

     public static function UnreadNotificationsVendor($id){
    $vendorNotification = Notification::where([ ['notifiable_id','=',$id],['read_at','=',Null] ])->get();
    return $vendorNotification;
      
    }

    public static function UnreadNotificationsDoctor($id){
    $doctorNotification = Notification::where([ ['notifiable_id','=',$id],['read_at','=',Null] ])->get();
    return $doctorNotification;
      
    }


	public static function get_vendor_product_status($order_id=null){
     $status = VendorProductStatus::select('status','id')->where('order_id',$order_id)->where('vendor_id',Auth::guard('vendor')->user()->id)->first();
      return $status;
	}
	public static function get_vendor_product_name($vendor_id=null){
       $status = User::select('name','id')->where('id',$vendor_id)->first();
      if($status){
        return $status;	
      } 
	}

	public static function get_doctor_work_info($doctore_id=null){
			$data = DoctorWorkInfo::select('woking_place_name','start_year','end_year')->where('doctor_id',$doctore_id)->get();
			return $data;
	}

	public static function get_doctor_education_info($doctore_id=null){
			$data = DoctorEducationInfo::select('college_name','subject','start_year','end_year')->where('doctor_id',$doctore_id)->get();
			return $data;
	}


	public static function get_specializations_info($doctore_id=null){
			$data = DoctorSpecializationsInfo::select('specializations_name')->where('doctor_id',$doctore_id)->get();
			return $data;
	}

     public static function get_specializations_info_doctor($doctore_id=null){
            $data = DoctorSpecializationsInfo::select('specializations_name')->where('doctor_id',$doctore_id)->get();
            return $data;
    }


	 public static function outOfStock($Id) {
        try {
             $qty=0;
                $product = Product::select('*')->where('id','=',$Id)->first();
                if(!empty($product)){
                    if($product['qty']>0){ $qty = $product->qty; }

                     }
            return $qty;
        } catch (\Exception $e) {
            return $e;
        }
        return 0;
    }

    public static $order_status = ['N' => 'New', 'CF' => 'Confirmed', 'O' => 'Collected', 'S' => 'On the way', 'A' => 'At Doorstep', 'D' => 'Delivered', 'R' => 'Returned', 'C' => 'Canceled', 'UP' => 'Updated', 'U' => 'Unavailable','PA'=>'Panding','PD'=>'Partily done','AC'=>'Accepted','PC'=>'In Progress Consultation','CC'=>'Completed Consultation ','AD'=>'Accept Vendor'];

     public static $order_status_vendor = ['N' => 'New', 'CF' => 'Confirmed', 'O' => 'Collected', 'S' => 'On the way', 'A' => 'At Doorstep', 'D' => 'Delivered', 'R' => 'Returned', 'C' => 'Canceled', 'UP' => 'Updated', 'U' => 'Unavailable','PA'=>'Panding','PD'=>'Partily done','AC'=>'Accepted','PC'=>'In Progress Consultation','CC'=>'Completed Consultation','AD'=>'Accept Vendor'];

      public static $order_status_customer = ['N' => 'New', 'CF' => 'Confirmed', 'O' => 'Collected', 'S' => 'On the way', 'A' => 'At Doorstep', 'D' => 'Delivered', 'R' => 'Returned', 'C' => 'Canceled', 'UP' => 'Updated', 'U' => 'Unavailable','PA'=>'Panding','PD'=>'Partily done','AC'=>'Accepted','PC'=>'In Progress Consultation','CC'=>'Completed Consultation','AD'=>'Accept Vendor'];

     public static $order_status_doctor = ['N' => 'New', 'CF' => 'Confirmed', 'O' => 'Collected', 'S' => 'On the way', 'A' => 'At Doorstep', 'D' => 'Delivered', 'R' => 'Returned', 'C' => 'Canceled', 'UP' => 'Updated', 'U' => 'Unavailable','PA'=>'Panding','PD'=>'Partily done','AC'=>'Accepted','PC'=>'In Progress Consultation','CC'=>'Completed Consultation','AD'=>'Accept Vendor'];



  public static $track_order_user = ['N' => 'Placed', 'CF' => 'Confirmed', 'O' => 'Collected', 'S' => 'On the way', 'A' => 'At Doorstep', 'D' => 'Delivered', 'R' => 'Returned', 'C' => 'Canceled', 'UP' => 'Updated', 'U' => 'Unavailable','PA'=>'Panding','PD'=>'Partily done','AC'=>'Accepted','PC'=>'In Progress Consultation','CC'=>'Completed Consultation','AD'=>'Accept Vendor'];

    public static function GetCurrentPincode(){
		$ip = $_SERVER['REMOTE_ADDR'];
		//$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
		//return $details->postal.' ('.$details->city.' '.$details->region.')';
		//return $details->postal;
		return 302012;
    }
    public static function GetCurrentStateCity(){
		$ip = $_SERVER['REMOTE_ADDR'];
		//$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
		//return $details->city.' ('.$details->region.')';
		return 'Jaipur (Rajsthan)';
    }

    public static function GetAllDeliveryAddress(){
    	if(Auth::guard('web')->user()->id){
		$data = DeliveryAddress::where('status',1)->where('user_id',@Auth::guard('web')->user()->id)->first();
		 return $data;	
		} 	 
    }

        public static function get_doctor_product_accepted(){

     $product_count = OrderItem::where([ ['doctor_id','=',Auth::guard('doctor')->user()->id] ])->count();
        return $product_count;
	}

	public static function get_doctor_available(){
   
    $doctor_available = User::where([ ['id','=',Auth::guard('doctor')->user()->id],['status','=',1]])->first();
            return $doctor_available;
	}

	  public static function get_vendor_product_accepted(){

     $vendor_product_count = OrderItem::where([ ['vendor_id','=',Auth::guard('vendor')->user()->id] ])->count();
            return $vendor_product_count;
	}

	public static function get_vendor_available(){
   
    $vendor_available = User::where([ ['id','=',Auth::guard('vendor')->user()->id],['status','=',1]])->first();
            return $vendor_available;
	}

	

     public function get_delivery_time(){
      $time =  Sitesettings::select('lock_delivery_times')->where('id',1)->first();  
     if($time->lock_delivery_times>date("G:i")){
        $time = date("Y-m-d");
     }else{
        $datetime = new DateTime('tomorrow');
        $time = $datetime->format('Y-m-d');
     }
     return $time;
     }

      public static function get_delivery_days(){
      $time =  Sitesettings::select('lock_delivery_times')->where('id',1)->first();  
     if($time->lock_delivery_times>date("G:i")){
        $time = 'Today';
     }else{
        $datetime = new DateTime('tomorrow');
        $time = 'Tomorrow';
     }
     return $time;
     }

         public static function get_address_user(){
   
    $addresses = DeliveryAddress::where('user_id',@Auth::guard('web')->user()->id)->get();
     return $addresses;
    }
 

      public static function get_delivery_address(){
      $user_id = @Auth::guard('web')->user()->id;
      $delivery_address = DeliveryAddress::where('status',1)->where('user_id',$user_id)->first();
     return $delivery_address;
 }

 public static function product_img($id){
     
     $product_img = ProductImage::where('prod_id',$id)
                       ->where('product_image.set_primary','Yes')
                       ->first();
     return $product_img;
 }


   public static function prescription_img($id){
     $prescription_img = UserPrescription::where('user_id',$id)
                       ->first();
     return $prescription_img;
 }

 public static function our_policy(){
            $data = Page::where('type','ourpolicies')->where('status',1)->get();
            return $data;
         }
         public static function company(){
            $data = Page::where('type','company')
            ->where('show_in_footer',1)
            ->where('status',1)->get();
            return $data;
         }


        public static function driver_name($driver_id) {
           // echo $driver_id; die('okkk');
            if(!empty($driver_id)){
            $name = User::select('id','name')->where('id',$driver_id)->first();         
            return $name->name;   
        }else{
            return 'Not Found';
        }
        }
 public static function doctor_name($docter_id) {
            if(!empty($docter_id)){
            $name = User::select('id','name')->where('id',$docter_id)->first();         
            return $name->name;   
        }else{
            return 'Not Found';
        }
        }
 

   public static function sendNotification ($user_id_array ,$dataArray, $device_type)
    {

        
        //echo "Hi"; die;
        $fields = [];
        $register_id[] = $user_id_array;
        //echo "<pre>"; print_r($register_id); die;
        if ( isset($register_id) && !empty( $register_id ) ) {
            if ( count ( $register_id ) == 1 ) {
                $register_id = $register_id[ 0 ];
            }
           
            //$url = env('FCM_URL');
            $url = 'https://fcm.googleapis.com/fcm/send';
            $server_key = 'AAAAAZepSsQ:APA91bFMrtr6PytTfJX1egtqqlML4lb49HUf9XU6UX6hI5YCiS3Dh9ufsinM6yr6gdldiWhmEaqfSojQtifYHoS9h6S2S12n1_AQ9FX-05WBt-8-QB93QNaMUufliYgwW9_e-dP6YkXG';
           // $server_key = env('FCM_API_KEY');
               
            if ( $device_type == 'I' ) {

                $fields = array
                (
                    //'priority' => "high" ,
                    'notification' => array( "title" => $dataArray['title'] ,"body" => $dataArray['body'] ,"sound" => "mySound" ,'badge' => count ( $dataArray ) ,'vibrate' => 1 ) ,
                    'data' => $dataArray ,
                );

            } else if ( $device_type == 'A' ) {

                $fields = array
                (
                    //'priority' => "high" ,
                    'notification' => array( "title" => $dataArray['title'] ,"body" => $dataArray['body'] ,"sound" => "mySound" ,'badge' => count ( $dataArray ) ,'vibrate' => 1 ) ,
                    'data' => $dataArray
                );
            }
           
                $fields[ 'registration_ids' ] = $register_id;
            $headers = array(
                'Content-Type:application/json' ,
                'Authorization:key=' . $server_key
            );
            $ch = curl_init ();
            curl_setopt ( $ch ,CURLOPT_URL ,$url );
            curl_setopt ( $ch ,CURLOPT_POST ,true );
            curl_setopt ( $ch ,CURLOPT_HTTPHEADER ,$headers );
            curl_setopt ( $ch ,CURLOPT_RETURNTRANSFER ,true );
            curl_setopt ( $ch ,CURLOPT_SSL_VERIFYHOST ,0 );
            curl_setopt ( $ch ,CURLOPT_SSL_VERIFYPEER ,false );
            curl_setopt ( $ch ,CURLOPT_POSTFIELDS ,json_encode ( $fields ) );

            $result = curl_exec ( $ch );
             if ( $result === FALSE ) {
                die( 'FCM Send Error:' . curl_error ( $ch ) );
            }

            curl_close ( $ch );
            return true;
        }

    }



}
?>