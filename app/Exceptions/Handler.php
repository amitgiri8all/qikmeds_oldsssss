<?php

namespace App\Exceptions;

use App\Traits\ResponceTrait;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Exception;
use Illuminate\Support\Facades\Response;

class Handler extends ExceptionHandler
{
    use ResponceTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function(Exception $e, $request) {
            return $this->handleException($request, $e);
        });
    }

    public function handleException($request, Exception $exception){
        if( $request->is('api/*')){
            if($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException){
                $response['code']       = 0;
                $response['error']      = true;
                $response['message']    = $exception->getMessage();
                return response()->json($response, 404);
            }
            
            $response['code']=0;
            $response['error']=true;
            $response['message']=$exception->getMessage();
            $status = 401;

            if ($this->isHttpException($exception)) {
                $status = $exception->getStatusCode();
            }
            return response()->json($response, $status);  
        }else{
           // return $retval = parent::render($request, $exception);
        }

        
    }
}
