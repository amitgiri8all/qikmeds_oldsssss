<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\User;
use App\Models\Tag;
use App\Models\ImageProduct;
use App\Models\ProductTag;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductTemp;
use App\Models\Manufacturer;
use App\Models\ManufacturerTmp;
use App\Traits\ImageTrait;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\ProductFaq;
use App\Helpers\Helper;

use DataTables;
use Session;
use Response;
use DB;
use URL;
use XmlParser;
use Validator;

class ProductController extends Controller
{
       use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  

     public function index(Request $request)
    {

        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Product::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Product::select('count(*) as allcount')->where('medicine_name', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Product::orderBy($columnName,$columnSortOrder)
                ->where('products.medicine_name', 'like', '%' .$searchValue . '%')
                ->select('products.*')
                ->skip($start)
                ->take($rowperpage)
                ->orderBy('id','DESC')
                ->get(); 
                  //echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $edit_url = URL::to("admin/product/edit/$val->id");

                    $delete_url = URL::to("admin/product/$val->id/confirm-delete");
                    $view = URL::to("admin/product/product-details/$val->id");
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      if($val->prescription=='1')
                        { $is_prescription = "<span class='badge badge-success'>Yes</span>";}
                     else{$is_prescription = "<span class='badge badge-danger'>No</span>";};

                     //if(!empty($val->id)){
                      $product_image =Helper::product_img($val->id);
                      // echo "<pre>";print_r($product_image);die;
                      if(!empty($product_image)){
                        $url= asset('storage/app/public/upload/Thumbnail/'.$product_image->image);
                      }
                      
                     //}
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     

                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'medicine_name'=>$val->medicine_name,
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',

                         'prescription'=>$is_prescription,
                        'type_of_sell'=>$val->type_of_sell,
                        'mrp'=>$val->mrp,
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href="'.$edit_url.'"><i class="fa fa-edit"></i> Edit</a>

                        <a class="btn btn-success"  href="'.$view.'"><i class="fa fa-eye"></i> View</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.product.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> '.$status.' </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.product.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    public function product_details($id=null){
      $data = Product::select('*')->where('id',$id)->first();
      $data_manufactoru = Manufacturer::where('id', $data->manufacturer_id)->first();  
      $data_brand = Brand::where('id', $data->brand)->first();

      $data_category = ProductCategory::where('product_id',$id)->first(); 
    
     // echo "<pre>";print_r($data_category);die;
      $data_category_name = Category::where('id',$data_category->category_id)->first();


    
      //for sub category 
      $data_sub_category_name = Category::where('id',$data_category->sub_category)->where('is_parent','0')->first();

      //echo "<pre>"; print_r($data_sub_category_name);die;

     // $product_image_data = ProductImage::where('prod_id',$id)->whereNull('deleted_at')->get();

      return view('backend.product.product-details',compact('data','data_manufactoru','data_brand','data_category_name','data_sub_category_name'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import_product_view()
    {
       return view('backend.product.import');
    }

    public function create()
    {
      $brand_list = Brand::select('id','brand_name')->orderBy('brand_name','asc')->get();
      $manufacturer = Manufacturer::select('id','manufacturer_name')->orderBy('manufacturer_name','asc')->get();
       $allCategories = Category::orderBy('id','DESC')->where('parent_id',0)->get();
       return view('backend.product.create',compact('brand_list','manufacturer','allCategories'));
    }

    public function sub_category(Request $request,$id=null)
    {
        $data = Category::select('id as key','category_name as value')->orderBy('id','DESC')->where('parent_id',$id)->get(['id','category_name']);
        $sub_category = Category::select('id','category_name')->orderBy('id','DESC')->where('parent_id',$id)->get();
        return response()->json(['data' => $sub_category]);
    }
 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request){
         //echo "<pre>";print_r($request->all());die;
       $validator = Validator::make($request->all(), [
            'medicine_name' => 'required',
            'type_of_sell' => 'required',
            'salt' => 'required',
            'therapeutic_class' =>'required',
            'chemical_class'=>'required',
            'manufacturer_id'=>'required',
            'mrp'=>'required',
            'qty'=>'required',
            //'deal_of_the_day'=>'required',
           // 'set_as_home'=>'required',
          //  'is_featured'=>'required',
//'brand'=>'required',
            'habit_forming'=>'required',
            'prescription'=>'required',
            'status'=>'required',
           // 'category_id'=>'required',
          //  'alternate_medicines'=>'required',
            'side_effects'=>'required',
            'how_to_use'=>'required',
            'product_expiry_date'=>'required',
            'description'=>'required',
            'key_benefits'=>'required',
            'directions_for_use'=>'required',
            'safety_information'=>'required',
            'other_information'=>'required',
            'product_highlights'=>'required',

            'image.*' => 'required',
             
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        } else {
           // try {
           //  echo "<pre>"; print_r($request->all()); die('hh');

                $save =  new Product();
                $save->medicine_name       = $request->medicine_name;
                $save->type_of_sell        = $request->type_of_sell;
                $save->salt                = $request->salt;
                $save->therapeutic_class   = $request->therapeutic_class;
                $save->chemical_class      = $request->chemical_class;
                $save->manufacturer_id     = $request->manufacturer_id;
                $save->product_expiry_date = $request->product_expiry_date;
                $save->mrp                 = $request->mrp;
                $save->sale_price          = $request->sale_price;
                $save->discount            = $request->product_discount;
                $save->deal_of_the_day     = $request->deal_of_the_day;
                $save->set_as_home         = $request->set_as_home;
                $save->is_featured         = $request->is_featured;
                $save->brand               = $request->brand;
                $save->habit_forming       = $request->habit_forming;
                $save->prescription       = $request->prescription;
                $save->alternate_medicines = $request->alternate_medicines;
                $save->side_effects        = $request->side_effects;
                $save->how_to_use          = $request->how_to_use;

                $save->description                = $request->description;
                $save->key_benefits               = $request->key_benefits;
                $save->directions_for_use          = $request->directions_for_use;
                $save->safety_information          = $request->safety_information;
                $save->other_information           = $request->other_information;
                $save->product_highlights          = $request->product_highlights;
                $save->qty          = $request->qty;
                $status  = $save->save();
                $last_user_id = $save->id;


                /*Category Info*/
                if(!empty($request->sub_category)){
                   $sub_category    = $request->get('sub_category');
                //echo "<pre>";print_r($request->sub_category);die;
                $dataval = [];
                foreach ($request->sub_category as $key => $value) {
                $dataval['sub_category']        = $sub_category[$key];
                $dataval['product_id']          = $last_user_id;
                $dataval['category_id']         = $request->category_id;
                ProductCategory::create($dataval);
                  }
               }else{
                $dataval['sub_category']     = 0;
                $dataval['product_id']       = $last_user_id;
                $dataval['category_id']      = $request->category_id;
                ProductCategory::create($dataval);
               }
                
              
                /*Save Image Details ============================*/
                $caption     = $request->get('caption');
                $image        = $request->file('image');
                $order         = $request->get('order');
                $set_primary    = $request->get('set_primary');
                //echo "<pre>";print_r($set_primary);die;

                  $data = [];
                   foreach ($request->caption as $key => $value) {
                     //echo "<pre>"; print_r($request->caption );die;
                 
                        $data['caption']      = $caption[$key];
                        $data['image']        = $image[$key];
                        $data['order']        = $order[$key];
                        $data['prod_id']      = $last_user_id;                         
                        $data['set_primary']  = (($set_primary-1) == $key) ? 'Yes' : 'No';
                        //echo "<pre>";print_r($data['set_primary']);
                        if($request->hasFile('image')){
                        $folder_name ='image';
                        $data['image'] = $this->fileUpload($request->file('image')[$key],false,$folder_name);
                         }
                        ProductImage::create($data);
                        // echo "<pre>";print_r($data);die;
                }
                  //  echo "<pre>";print_r($data);die;
            if ($status) {
                    $response['status'] = "success";
                    $response['message'] = "your account has been created successfully.";
                    $response['url']     = route('admin.product.list');
                    return json_encode($response);
                } else {
                    $response['status'] = "error";
                    $response['message'] = "There was an issue account createing the recode. Please try again.";
                    return json_encode($response);
                }
          /*  } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
            }
            return back();*/
        }
      }

      public function aacreate_info(Request $request,$id=null){
            $brand_list = Brand::select('id','brand_name')->orderBy('brand_name','asc')->get();
            $allCategories = [];

            $allCategories = Category::orderBy('id','DESC')->where('parent_id',0)->get();


            foreach($allCategories as $key=>$category) {
                $allCategories[$key]['sub_categorys'] = Category::where('parent_id',$category->id)->get();
            }
$cat_data =  ProductCategory::select(['pro_cat_id','category_id','product_id'])->where('product_id',$id)->get();
            return view('backend.product.create',compact('brand_list','id','allCategories','cat_data'));
      }

      public function create_info_update(Request $request,$id=null){
        $data = $request->all();
        $updateinfo = Product::updateOrCreate(['id' =>$id],$data);
                    $insertedId =$updateinfo->id;
        return redirect('admin/product/add/'.$insertedId)->with('success', trans('You have been successfully create product'));
      }

        public function storetag(Product $product,Request $request)
     {
            $tag_name  =  $request->get('tag_name');
           // echo $tag_name; die();
            $is_tag = Tag::where('tag_name',$tag_name)->first();

            $tag  =  $request->get('id');

            if($tag != ''  ){

            $tagdata = new ProductTag();
            $tagdata->product_id = $product->id;
            $tagdata->tag = $tag;
            $tagdata->save();

            }
            else if(!empty($is_tag))
            {

            $tagdata = new ProductTag();
            $tagdata->product_id = $product->id;
            $tagdata->tag = $is_tag->id;
            $tagdata->save();

            }
            else{

                $tags = new Tag();
                $tags->tag_name = $tag_name;


                 $tags->save();

               $insertedId = $tags->id;
               $protags = new ProductTag();


               $protags->tag =  $insertedId;
               $protags->product_id = $product->id;


                 $protags->save();

              }

            $content['status']="success";
            $content['message']="Tag  Save Successfully.";
            return Response::json($content);


     }

        public function destroytag($id)
    {
            $data= DB::table('product_tag')->where('pro_tag_id',$id)->delete();

 
            $content['status']="success";
            $content['message']="Tag  delete Successfully.";
            return Response::json($content);    
         

    }

    public function tagdata(Product $product)
    {

            $producttags = ProductTag::select(['product_tag.pro_tag_id','tag.tag_name'])
                                       ->join('tag', 'tag.id', '=', 'product_tag.tag')
                                      ->orderBy('product_tag.pro_tag_id', 'desc')->where('product_id', $product->id)->get();

            return View('backend.product.tagdata',compact('producttags'));
    }



  public function   cat_store(Product $product,Request $request)

   {

     $categorys  =  $request->get('categorys');



     //$data= DB::table('product_category')->where('product_id',$product->pro_id)->delete();
        // echo "<pre>";
      //   print_r($categorys);die;
     for($i=0;isset($categorys[$i]);$i++) {

     $pre_data= DB::table('product_category')->where('product_id',$product->id)->where('product_id',$product->id)->where('category_id',$categorys[$i])->first();
    if( $pre_data){

        }
    else{

     $productcat = new ProductCategory();
     $productcat->category_id = $categorys[$i];
     $productcat->product_id  =  $product->id;


     $productcat->save();


        unset($productcat);
     }
    }


      $content['status']="success";
      $content['message']="Category  Save Successfully.";
     return Response::json($content);


  }

    public function cat_remove(Product $product,Request $request)

    {


        $categorys  =  $request->get('categorys');


        $data= DB::table('product_category')->where('product_id',$product->id)->delete();
        for($i=0;isset($categorys[$i]);$i++) {

            $productcat = new ProductCategory();
            $productcat->category_id = $categorys[$i];
            $productcat->product_id  =  $product->id;


            $productcat->save();
            unset($productcat);
        }
            $content['status']="success";
            $content['message']="Category  Remove Successfully.";
            return Response::json($content);


    }


      //Here is the auto search value user name
    public function autocomplet_search(Request $request) {
      //  echo $request->get('query'); die();
        $data = Product::select('id as data', 'medicine_name as value')->Where('medicine_name', 'like', '%' . $request->get('query') . '%')->limit(15)->get();
        
        return response()->json(['suggestions' => $data]);
    }

 

/*File Upload CSV*/

    public function csv_content_parser($content) {
        foreach (explode("\n", $content) as $line) {
          yield str_getcsv($line);
        }
    }

    //store user
    public function import_product(Request $request) {

        //echo "asdf"; die();

        ini_set('memory_limit','512M');
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors();
             return response()->json($validator->messages(), 422);
        } else {
        try { 
            $file1 = $request->file;
            $uploadedFile    = $request->file('file'); 
            $destinationPath = public_path('upload/products/csv');
            $newfilename = date("dmY_His").'.'.$uploadedFile->getClientOriginalExtension();
       if ($uploadedFile->isValid()) {
           $uploadedFile->move($destinationPath, $newfilename);
       }
            $filepath = URL::to('public/upload/products/csv/'.$newfilename);
//            $filename = public_path('upload/products/csv/1.csv');
            $content = file_get_contents($filepath);
            $products = array();
            $header = true;
            foreach ($this->csv_content_parser($content) as $fields) {
                 if($header) { $header = false; continue; }
                 array_push($products, $fields);
            }
      
  // echo "<pre>"; print_r($products); die();
        foreach($products as $key=> $product) {   
            // echo $product[1]; die;         
            $checkexistingproduct = ProductTemp::where('medicine_name',$product[0])->count();
            if($checkexistingproduct == 0){
            $checkexistingbrand = ManufacturerTmp::where('manufacturer_name',@$product[3])->count();
            if($checkexistingbrand == 0){
                $createBrand                = new ManufacturerTmp();
                $createBrand->manufacturer_name    = @$product[3];
                $branddata = $createBrand->save();
            }else{
                $branddata = ManufacturerTmp::where('manufacturer_name',@$product[3])->first();
            }
            $createProduct                      = new ProductTemp();
            $createProduct->medicine_name               = @$product[0];
            $createProduct->prescription                = @$product[1];
            $createProduct->type_of_sell                = @$product[2];
            $createProduct->manufacturer_id             = @$branddata->id;
            $createProduct->salt                        = @$product[4];
            $createProduct->mrp                         = @$product[5];
            $createProduct->uses                        = @$product[6];
            $createProduct->alternate_medicines         = @$product[7];
            $createProduct->side_effects                = @$product[8];
            $createProduct->how_to_use                  = @$product[9];
            $createProduct->chemical_class              = @$product[10];
            $createProduct->habit_forming               = @$product[11];
            $createProduct->therapeutic_class           = @$product[12];
            $produtdata  = $createProduct->save();

         //    $catArr      = explode(' > ',$product[24]);
        
         //    $catid = 0;
         //    $parentidarr = [];
         //    foreach($catArr as $key=> $val){
         //        $val = trim(str_replace('>',' ',$val));
         //        $checkalredyexist = CategoryTemp::select('id')->where('cat_name',$val)->first();
         //        if($checkalredyexist){
         //            $catid                             = $checkalredyexist->id;
         //            $parentidarr[$key]                 = $catid; 
         //            $createProCatRelation              = new ProductCategoryTemp();
         //            $createProCatRelation->product_id  = @$createProduct->id;
         //            $createProCatRelation->category_id = $catid;
         //            $createProCatRelation->save();
         //        }else{
         //            $createCategory                      = new CategoryTemp();
         //            $createCategory->cat_name            = trim(str_replace('>',' ',$val));
         //         //   if($key!=0){
         //                $k = $key-1;
         //                $createCategory->parent_id       = isset($parentidarr[$k]) ? $parentidarr[$k] : 0;
         //          //  }
         //            $data  = $createCategory->save();
         //            $catid = @$createCategory->id;

         //            $createProCatRelation              = new ProductCategoryTemp();
         //            $createProCatRelation->product_id  = @$createProduct->id;
         //            $createProCatRelation->category_id = $catid;
         //            $createProCatRelation->save();
        
         //            $parentidarr[$key] = @$createCategory->id; 
         //        }
         //    }
         //   // echo $product->image_link; die;
         //    if(@getimagesize($product[9])){
         //        $url = $product[9];
         //        $contents = file_get_contents($url);
         //        $name = substr($url, strrpos($url, '/') + 1);
         //        Storage::put('products/'.$name, $contents);
    
         //        $createProCatRelation              = new ProductImageTemp();
         //        $createProCatRelation->product_id  = @$createProduct->id;
         //        $createProCatRelation->image       = $name;
         //        $createProCatRelation->save();                    
         //    }
         // }
            }
        }
            Product::truncate();
            Manufacturer::truncate();
           // ProductImage::truncate();
          //  Category::truncate();
           // Brand::truncate();

            \DB::statement('INSERT products SELECT * FROM product_temps');
             \DB::statement('INSERT manufacturer SELECT * FROM manufacturer_tmps');
         //   \DB::statement('INSERT product_images SELECT * FROM product_image_temps');
         //   \DB::statement('INSERT categories SELECT * FROM category_temps');
         //   \DB::statement('INSERT brands SELECT * FROM brand_temps');

            ProductTemp::truncate();
             ManufacturerTmp::truncate();
         //   ProductImageTemp::truncate();
           // CategoryTemp::truncate();
          //  BrandTemp::truncate();
            //Product::query()->truncate();


            $request->session()->flash('alert-success', 'Product Imported Successfully.');
            return redirect()->route('admin.product.list');

            } catch (ModelNotFoundException $e) {
                $message = 'Failed to add.';
                $request->session()->flash('alert-danger', $message);
                return redirect()->route('admin.product.list');
            }
        }
        $request->session()->flash('alert-success', 'Product Imported Successfully.');
        return redirect()->back();




    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = Product::where('id',$id)->first();

        $category_data = ProductCategory::where('product_id',$data->id)->first();

        /*$sub_category_data = ProductCategory::where([
            ['product_id','=',$category_data->product_id],
            ['category_id','=',$category_data->category_id]
        ])->get();*/

      // echo "<pre>"; print_r($sub_category_data);die;
       // dd($sub_category_data);

        //$image_data = ProductImage::where('prod_id',$data->id)->get();

        $allCategories = Category::orderBy('id','DESC')->where('parent_id',0)->get();
        $image_data = ProductImage::where('prod_id',$id)->get();
        //dd($image_data);
        
        $brand_list = Brand::select('id','brand_name')->orderBy('brand_name','asc')->get();
        $manufacturer = Manufacturer::select('id','manufacturer_name')->orderBy('manufacturer_name','asc')->get();

        return view('backend.product.edit',compact('data','category_data',
           'image_data','manufacturer','brand_list','allCategories'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
             $validator = Validator::make($request->all(), [
                'medicine_name' => 'required',
                'type_of_sell' => 'required',
                'salt' => 'required',
                'therapeutic_class' =>'required',
                'chemical_class'=>'required',
                'manufacturer_id'=>'required',
                'mrp'=>'required',
                 'qty'=>'required',
               // 'deal_of_the_day'=>'required',
                //'set_as_home'=>'required',
                //'is_featured'=>'required',
                //'brand'=>'required',
                'habit_forming'=>'required',
                'status'=>'required',
                //'category_id'=>'required',
                'alternate_medicines'=>'required',
                'side_effects'=>'required',
                'how_to_use'=>'required',
                'description'=>'required',
                'key_benefits'=>'required',
                'directions_for_use'=>'required',
                'safety_information'=>'required',
                'other_information'=>'required',
                'product_highlights'=>'required',
                'product_expiry_date'=>'required',

            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 422);
            } else {
           // try {  
                $product = $request->all();
                $data['medicine_name'] = $request->medicine_name;
                $data['type_of_sell'] = $request->type_of_sell;
                $data['salt'] = $request->salt;
                $data['therapeutic_class'] = $request->therapeutic_class; 
                $data['chemical_class'] = $request->chemical_class;
                $data['manufacturer_id'] = $request->manufacturer_id;
                $data['mrp'] = $request->mrp;
                $data['qty'] = $request->qty;
                $data['sale_price'] = $request->sale_price;
                $data['discount'] =$request->product_discount;
                $data['deal_of_the_day'] = $request->deal_of_the_day;
                $data['set_as_home'] = $request->set_as_home;
                $data['is_featured'] = $request->is_featured;
                $data['product_expiry_date'] = $request->product_expiry_date;
                $data['brand'] = $request->brand;
                $data['habit_forming'] = $request->habit_forming;
                $data['prescription'] = $request->prescription;
                $data['status'] = $request->status;
                $data['alternate_medicines'] = $request->alternate_medicines;
                $data['side_effects'] = $request->side_effects;
                $data['how_to_use'] = $request->how_to_use;
                $data['description'] = $request->description;
                $data['key_benefits'] = $request->key_benefits;
                $data['directions_for_use'] = $request->directions_for_use;
                $data['safety_information'] = $request->safety_information;
                $data['other_information'] = $request->other_information;
                $data['product_highlights'] = $request->product_highlights;
                //echo "<pre>"; print_r($data); die();
                $data = Product::findOrFail($id)->update($data);
                /*delete  Image Details ============================*/
                $data=ProductImage::where('prod_id',$id);
                $data->delete();
                /*Save Image Details ============================*/
                    $caption  = $request->get('caption');
                    $image_array    = $image    = $request->file('image');
                    $new_image_array    = $new_image    = $request->file('new_image');
                    $order    = $request->get('order');
                    $set_primary    = $request->get('set_primary');
                    $old_image    = $request->get('old_image');
                if(!empty($old_image)){
                    end($old_image);
                    $new_key = key($old_image);
                    $new_image_arr = [];
                    $j = $new_key+1;
                    if(!empty($image)){
                        for($i=0;$i<count($image);$i++){
                            $old_arr = $image_array[$i];
                            unset($image_array[$i]);
                            $new_image_arr[$j] = $old_arr;
                            $j++;
                        }
                    }
                }else{
                    $new_image_arr = $image;
                }                    
                $data = [];
                //echo $set_primary; die();
                foreach ($request->caption as $key => $value) {
                    $data['caption']      = $caption[$key];
                    $data['order']        = $order[$key];
                    $data['prod_id']      = $id;
                    $data['set_primary']  = $set_primary;
                    if(!empty($old_image) && array_key_exists($key, $old_image)){
                        $data['image'] = $old_image[$key];
                    }else{
                        $data['image']        = $new_image_arr[$key] ?? '';
                        if($request->hasFile('image')){
                          $folder_name ='image';
                          $data['image'] = $this->fileUpload($new_image_arr[$key],false,$folder_name);
                        }
                    }
                    ProductImage::create($data);
                }
           if ($data){
                    $response['status'] = "success";
                    $response['url'] = route('admin.product.list');
                    $response['message'] = "Product has been update successfully.";
                    request()->session()
                    ->flash('success', 'You have been successfully create Product');
                    return json_encode($response);

           }else{
                  $response['status'] = "error";
                  $response['message'] = "Somthing went wrong.";
            return json_encode($response);
            }
       /* } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
            }*/

        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */

     public function getModalDelete($id = null)
    {
        $model = 'Product';
        $confirm_route = $error = null;
        $confirm_route = route('admin.product.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }


    public function destroy($id)
    {
        $data_product=Product::find($id);
        $status =  $data_product->delete();
       // $data=ProductImage::where('prod_id',$id);
        //$status =  $data->delete();
        if($status){
            request()->session()->flash('success','Product successfully deleted');
                    return redirect()->route('admin.product.list');
        }
        else{
            request()->session()->flash('error','Error while deleting product');
        }
        return redirect()->route('admin.product.list');
    }



/*=========================== FAQ =======================*/

    public function faq_product_add(Request $request){
        $product = Product::all(); 
       return view('backend.product.faq-product',compact('product'));      
    }

    public function faq_save_product(Request $request){
           $validator = Validator::make($request->all() , ['product_id' => 'required']);
        if ($validator->fails())
        {
            return response()->json($validator->messages() , 422);
       }else{
        try{
            /*Save Details Details ============================*/
            $question     = $request->get('question');
            $answer         = $request->get('answer');
             $data=[];
            foreach($request->question as $key => $value){    
                $data['product_id'] = $request->product_id;
                $data['question']     = $question[$key];
                $data['answer']       = $answer[$key];
                ProductFaq::create($data);
            }    

               /// echo $data->id; die();
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.product.list');
                    $response['message'] = "You have been successfully create Faq.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Faq');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.product.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = Product::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.product.list');
     }

}
