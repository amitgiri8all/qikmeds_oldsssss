<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Models\RoleHasPermission;
use App\Models\RoleUser;
use App\Models\Links;
use DB;
use Route;
use Session;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all()->where('is_backend','1');
        return view('backend.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::get()->pluck('name', 'name');
        return view('backend.roles.add', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die();   
        DB::beginTransaction();
      try {  
        $role = Role::create($request->except('manager_permission','action_permission'));
        $managerpermission = $request->get('manager_permission');
        $mainmenupermission = [];
        //echo "<pre>"; print_r($managerpermission); die();
        foreach($managerpermission as $key=> $val){
            $mainmenupermission[] = $key; 
            foreach($val as $value){
            $mainmenupermission[] = $value; 
            }
        }
        $actionmenupermission = [];
        $actionpermission = $request->get('action_permission');
        foreach($actionpermission as $key=> $val){
            $mainmenupermission[] = $key; 
            foreach($val as $value){
            $actionmenupermission[] = $value; 
            }
        }
       // echo "<pre>"; print_r($mainmenupermission); die();
        $mainmenupermission = implode(',',$mainmenupermission);
        Role::where('id',$role->id)->update(['permissions'=>$mainmenupermission]);
         foreach($actionmenupermission as $val){
            $addactionpermission = new RoleHasPermission();
            $addactionpermission->permission_id = $val;
            $addactionpermission->role_id = $role->id;
            $addactionpermission->save();
        }
        DB::commit();
        } catch (\Exception $e) {
            Session::flash('error',$e->getMessage());
             return redirect()->route('roles.index');


        }
        Session::flash('success', 'You have been successfully create roles');
        return redirect()->route('roles.index');
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Role $role)
    {
/*        echo "<pre>"; print_r($Role);  die();
        $role->load('permissions');
        echo "<pre>"; print_r($role); die();
*/        return view('backend.roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function edit(Role $role)
    {   
        $permissions = Permission::get()->pluck('name', 'name');
        $actions    = RoleHasPermission::where('role_id',$role->id)->select('permission_id')->get();
        $actionarr  = [];
        foreach($actions as $key=> $val){
            $actionarr[$key] =  $val->permission_id;
        }
        return view('backend.roles.edit', compact('role','actionarr', 'permissions'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        DB::beginTransaction();
      try {  
        $managerpermission = $request->get('manager_permission');
        $mainmenupermission = [];
        foreach($managerpermission as $key=> $val){
            $mainmenupermission[] = $key; 
            foreach($val as $value){
            $mainmenupermission[] = $value; 
            }
        }

        $actionmenupermission = [];
        $actionpermission = $request->get('action_permission');
        foreach($actionpermission as $key=> $val){
            $mainmenupermission[] = $key; 
            foreach($val as $value){
            $actionmenupermission[] = $value; 
            }
        }
        $mainmenupermission = implode(',',$mainmenupermission);
        Role::where('id',$role->id)->update(['permissions'=>$mainmenupermission]);
        RoleHasPermission::where('role_id',$role->id)->delete();
         foreach($actionmenupermission as $val){
            $addactionpermission = new RoleHasPermission();
            $addactionpermission->permission_id = $val;
            $addactionpermission->role_id = $role->id;
            $addactionpermission->save();
        }
        DB::commit();
        } catch (\Exception $e) {
            Session::flash('error',$e->getMessage());
        }
        Session::flash('success', 'You have been successfully Update roles');
        return redirect()->route('roles.index');
    }
   
    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        RoleHasPermission::where('role_id',$role->id)->delete();
        $role->delete();
        Session::flash('success', 'You have been successfully deleted roles');
        return redirect()->route('roles.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        Role::whereIn('id', request('ids'))->delete();
        return response()->noContent();
    }

}