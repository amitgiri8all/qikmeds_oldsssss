<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\User;
use App\Traits\ImageTrait;
use DataTables;
use Session;
use Response;
use DB;
use URL;

class CategoryController extends Controller
{
    //Upload Image Function Traits
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Category::select('count(*) as allcount')->where('is_parent','1')
                 ->where('parent_id','0')
                //->where('status','1')
                ->count();
                $totalRecordswithFilter = Category::select('count(*) as allcount')
                ->where('category_name', 'like', '%' .$searchValue . '%')
                ->where('is_parent','1')
                ->where('parent_id','0')
                //->where('status','1')
                ->count();
                // Fetch records
                $records = Category::orderBy($columnName,$columnSortOrder)
                ->where('categories.category_name', 'like', '%' .$searchValue . '%')->where('is_parent','1')
                ->where('parent_id','0')
                //->where('status','1')
                ->select('categories.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                 //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/category/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/category/edit/$val->id");
                    $sub_category_url = URL::to("admin/category/sub-catgory/$val->id");
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                     if($val->is_parent==1){ $is_parent = "<span class='badge badge-success'>Yes</span>";}else{$is_parent = "<span class='badge badge-danger'>No</span>";};
                      if(!empty($val->image)){
                    $url= $val->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'category_name'=>$val->category_name,
                        'image'=>'<img src="'.$url.'" border="0" width="100" class="img-rounded" align="center" />',
                        'is_parent'=>$is_parent,
                        'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>/*'
                               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal'.$val->id.'">
                                  View Sub Category
                                </button>


                                <div class="modal fade" id="exampleModal'.$val->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">View Sub Category</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">

                                                 <table class="table table-bordered">
                                                      <thead>
                                                        <tr>
                                                          <th scope="col">#</th>
                                                          <th scope="col">Name</th>
                                                          <th scope="col">Created At</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr>
                                                          <th scope="row">1</th>
                                                          <td>Mark</td>
                                                          <td>10 August,2021</td>
                                                        </tr>
                                                        </tr>
                                                      </tbody>
                                                </table>

                                        
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
*/
                      
                      ' <a href="'.$sub_category_url.'" class="btn btn-success">View Sub  categories</a>
                        <a class="btn btn-success" href="'.$edit_url.'"><i class="fa fa-edit"></i> Edit</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.category.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> '.$status.' </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.category.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $categories = Category::select('categories.*')->with('sub_category')->where('parent_id',0)->get();
      return view('backend.category.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    // Load List Page View ----------- 
    public function sub_catgory($id=null){
   //   echo $id; die();
      return view('backend.category.sub_catgory_list',compact('id'));
    }


      public function data_sub_cat(Request $request,$id=null)
    {
/*         try{
*/            if ($request->ajax()) {
    //echo "ok"; die();
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
 
                ## Custom Field value
                $order_status = $request->get('order_status');
                // Total records
                $totalRecords = Category::select('*')->where('parent_id','=',$id);
                //echo $totalRecords; die();
                  $totalRecordswithFilter = Category::select('count(*) as allcount')->where('parent_id','=',$id)
                    ->Where('category_name', 'like', '%' .$searchValue . '%')

               
                 /*
                if($searchValue!=''){ 
                $order->whereHas('User', function($q) use ($searchValue) {
                    $q->where('name', 'like', '%' . $searchValue . '%');
                   });
                }
              
                if($order_status!=''){ 
                $order->where('order_status',$order_status);
                }
                //echo "<pre>";print_r($request->get('to_date'));die;
                if ($request->get('from_date') and !empty($request->from_date)) {
                  $order->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                  $order->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }*/
                ->count();
               // Fetch records
               $records = Category::select('*')->where('parent_id','=',$id)
                 ->select('*')
                 ->skip($start)
                ->take($rowperpage)
                ->orderBy('id','DESC')
                ->Where('category_name', 'like', '%' .$searchValue . '%')

              /*  if($order_status!=''){ 
                  $records->where('order_status',$order_status);
                }
                  
                if ($request->get('from_date') and !empty($request->from_date)) {
                $records->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                $records->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }*/
                ->get();
               // echo "<pre>";print_r($records);die;

                $data_arr = [];
                foreach($records as $key=> $val){

                  //  $delete_url = URL::to("banner/$val->id/confirm-delete");
                   // $status_url = URL::to("admin/order/statuslist/$val->id");

                  //  $view_url = URL::to("admin/order/view/$val->id");
                   // $view_invoice = URL::to("admin/order/invoice/$val->id");
                     /*if($val->shipping_amount<=0){
                        $shipping_amount = "Free";
                    }else{ 
                        $shipping_amount = '₹ '.$val->shipping_amount;
                    }*/

                    /*if(array_key_exists($val->order_status, Helper::$order_status)) {
                         $order_status_name =  Helper::$order_status[$val->order_status];
                     } else {
                       echo  "";
                     }*/
                  /*  if($val->status=='new'){
                      $status = '<span class="badge badge-danger">'.ucwords($val->status).'</span>';
                    }else{
                      $status = '<span class="badge badge-info">'.ucwords($val->status).'</span>';
                    } */ 
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'category_name'=>ucwords($val->category_name),
                        'created_at'=> date('d F,Y', strtotime($val->created_at)),
                    /*    'payment_method'=>isset($val->OrderPayment->payment_method)?$val->OrderPayment->payment_method:'--',*/
                      //  'payment_method'=>'Cash',
                        /*'sub_total'=>'₹ '.$val->sub_total,
                        'shipping_amount'=>$shipping_amount,
                        'total_payed_amount'=>'₹ '.$val->total_payed_amount,
                        'discount_coupon_code'=>isset($val->discount_coupon_code)?'₹ '.$val->discount_coupon_code:'==',

                        'status'=>
                     '<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm" onclick="popupchangestatus('.$val->id.',\''.$val->order_status.'\')">Change Status</button>
                       '
                      ,
                        'orderstatus'=>Helper::$order_status[$val->order_status],
                        'created_at'=> date('d F,Y', strtotime($val->created_at)),
                         'action'=>'
                        <a title="Order Details View" class="btn btn-success" style="margin:3px;" href="'.$view_url.'">Order Details <i class="fa fa-info-circle"></i></a>
                        <a title="Invoice"  class="btn btn-info" href="'.$view_invoice.'">View Invoice <i class="fa fa-file"></i></a>'*/
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" =>  $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
              
            return view('backend.category.sub_category_list',compact('id'));
       
      /*  } catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    } 



    public function store(Request $request){
        try {
        $data= $request->all();
        if($request->hasFile('image')){
            $folder_name ='category';
            $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }
        $status = Category::create($data);
        if($status){
            return redirect('admin/category/list')->with('success', trans('You have been successfully create category'));
            request()->session()->flash('success','You have been successfully create category');
        }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }
        return back();
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $data = Category::where('id',$id)->first();
     $categories = Category::select('categories.*')->with('sub_category')->where('parent_id',0)->get();
     return view('backend.category.edit',compact('data','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {   
  //echo "<pre>"; print_r($request->all()); die();
      $data = $request->all();
      if($request->get('is_parent')){
        $data['is_parent']='1';
      }else{
        $data['is_parent']='0';
      }
        if($request->hasFile('image')){
            $folder_name ='category';
            $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }
      $category =Category::findOrFail($id)->update($data);
       if($category){
            return redirect('admin/category/list')->with('success', trans('You have been successfully update category'));
         }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Category';
        $confirm_route = $error = null;
        $confirm_route = route('admin.category.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       // echo $id; die();
        $category=Category::find($id);
        $child_cat_id=Category::where('parent_id',$id)->pluck('id');
        $status=$category->delete();
        // echo $status; die();
        if($status){
            if(count($child_cat_id)>0){
                Category::shiftChild($child_cat_id);
            }
            request()->session()->flash('success','Category successfully deleted');
                    return redirect()->route('admin.category.list');
        }
        else{
            request()->session()->flash('error','Error while deleting category');
        }
        return redirect()->route('admin.category.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.category.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = Category::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.category.list');
     }
}
