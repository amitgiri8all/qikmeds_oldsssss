<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Contact;
use App\Models\Page;
use App\Models\SubscriptionPlan;
use App\Models\AdminNotification;
use App\Models\Setting;
use Validator;
use Redirect;
use Auth;
use Session;    
use DateTime;
use DB;
use Mail;
use Hash;
use URL;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      return view('backend.login.index');
    }

     public function doLogin(Request $request){
        $rules = array(
            'email'    => 'required',
            'password' => 'required'
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
        return Redirect::to('/admin')
            ->withErrors($validator)
            ->withInput($request->except('password'));
        } else {
       // Auth::logoutOtherDevices($request->get('password'));
        $userdata = array(
            'email'     => $request->get('email'),
            'password'  => $request->get('password')
        );
        if (Auth::guard('admin')->attempt($userdata)) {
            if(Auth::guard('admin')->user()->status != '0'){
                Auth::guard('admin')->user()->save();
                Session::flash('success','You have been successfully login.');  
 
                return Redirect::intended('/admin/dashboard');    
            }else{
                Session::flash('error','Your account is not enabled. please contact your administrator to access away.');           
                return Redirect::to('/admin');
            }
        } else {
            Session::flash('error','These credentials do not match our records.');
             return Redirect::to('/admin');
        }
      } 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(){

       $users = User::where('role','user')->get()->count();

       $drivers = User::where('role','driver')->get()->count();
       $doctors = User::where('role','doctor')->get()->count();
       $vendors = User::where('role','vendor')->get()->count();
       $premium_plans = SubscriptionPlan::get()->count();
       $settings = Setting::get()->count();

        $category = Category::where([
                        ['parent_id','=', 0],
                        ['status','=', 1],
                        ['deleted_at','=',NULL]
                   ])->get()->count();

        $product = Product::where([
                        ['status','=', 1],
                        ['deleted_at','=',NULL]
                   ])->get()->count();

        $coupon = Coupon::where([ 
                   ])->get()->count();

         $order = Order::where([
                        ['deleted_at','=',NULL]
                   ])->get()->count();

         $contact = Contact::where([
                        ['deleted_at','=',NULL]
                   ])->get()->count();

         $page = Page::where([
                        ['deleted_at','=',NULL],['status','=','1']
                   ])->get()->count();

         $admin_notifications = AdminNotification::get()->count();

       //echo "<pre>";print_r($contact);die;
       //dd($users); 
       return view('backend.dashboard', compact('users','category','product','coupon','order','contact','page','drivers','doctors','vendors','premium_plans','admin_notifications','settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Logout user admin ----------------------
    public function logout(Request $request) {
      Auth::guard('admin')->logout();
      return redirect('/admin/login');
    }
}
