<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminNotification;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\Notification;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;
use DataTables;
//use Yajra\DataTables\DataTables;

class NotificationController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = AdminNotification::select('count(*) as allcount')->count();
                $totalRecordswithFilter = AdminNotification::select('count(*) as allcount')
                //->where('role','vendor')
               ->Where('message_heading', 'like', '%' .$searchValue . '%')
                ->orWhere('message', 'like', '%' .$searchValue . '%')
              /*  ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = AdminNotification::orderBy($columnName,$columnSortOrder)
                //->where('role','vendor')
                 ->Where('message_heading', 'like', '%' .$searchValue . '%')
                 ->orWhere('message', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('admin_notifications.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $view_url = URL::to("admin/notification/show/$val->id");
                    $id =  $val->id;
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                   if(!empty($val->image)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$val->image);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }   
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'message_heading'=>$val->message_heading,
                        'message'=>$val->message,
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                         <a href="#" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal_'.$id.'"><i class="fa fa-eye"  data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Show Details"></i> Show Details<div class="ripple-container"></div></a>
                  <div class="modal fade col-sm-12" id="myModal_'.$id.'" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                     <div class="modal-dialog">
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close resetModal" data-dismiss="modal" aria-hidden="true">×</button>
                              <h4 class="modal-title" id="myModalLabel">Send Notification Details</h4>
                           </div>
                           <div class="modal-body">
                              <div class="table-responsive">
                                 <table class="table table-bordered">
                                    <tbody>
                                       <tr>
                                          <th width="30%"><label>Id</label></th>
                                          <td width="70%">'.$val->id.' </td>
                                       </tr> 
                                      
                                       </tr> 
                                       <tr>
                                          <th width="30%"><label>Message heading</label></th>
                                          <td width="70%">'.$val->message_heading.'</td>
                                       </tr>  <tr>
                                          <th width="30%"><label>Message</label></th>
                                          <td width="70%">'.$val->message.'</td>
                                       </tr>
                                       <tr>
                                          <th width="30%"><label>Image</label></th>
                                          <td width="70%"> <img width="100" src="'.$url.'" class="thumb-image"></td>
                                       </tr>  
                                       <tr>
                                          <th width="30%"><label>Created On</label></th>
                                          <td width="70%">'.date("Y-M-d",strtotime($val->created_at)).'</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
            '

                        
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.notification.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }


    public function orderStatus()
    {
        /*$user= $this->note->whereIn('type',['App\Notifications\OrderStatus','App\Notifications\AllOrderStatus'])->update(['read_at'=> now()]);
        if ($this->user->can('orderStatusNotification', Notification::class)) {
            return abort(403,'not able to access');
        }*/
        return view('backend.notification.order-status');
    }


     public function orderStatusData(Request $request)
    {
       /* $notifications =  $this->note->selectRaw('*,id as id1')->whereIn('type',['App\Notifications\OrderStatus','App\Notifications\AllOrderStatus'])->orderBy('created_at','DESC')->get();*/
       $notifications = Notification::get();
        $start = 1;
       // echo "<pre>";print_r($notifications[0]['type']);die;
       // echo "<pre>";print_r($notifications);die;
        return DataTables::of($notifications)
            ->addColumn('Slno',function ($notifications) use(&$start)  {
                return  $start++;
            })
        

            ->addColumn('heading',function ($notifications){
                $data = json_decode($notifications->data,true);
                return $data['message'];

            })
            ->addColumn('order_code',function ($notifications){
                $data = json_decode($notifications->data,true);
                return $data['order_code'];

            })
            ->addColumn('sendor',function ($notifications){
                //if($notifications->type == 'App\Notifications\AllOrderStatus' ){
                    $data = json_decode($notifications->data,true);
                    return isset($data['sender']) ? $data['sender'] : '';
                //}
                

            })
            
            ->addColumn('action',function ($notifications){
                $data = json_decode($notifications->data,true);
                /*return '<a href="'.route('order.view',$data['order_id']).'"  class="btn btn-success">Direct to order</a><button type="button" onclick="deleteRow(\''.$notifications->id1.'\')" class="btn btn-danger">Delete</button>';*/
            })
            ->rawColumns(['action'])
            ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('role','user')->get();
        return view('backend.notification.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // echo "<pre>"; print_r($request->all()); die();
          try {

        $input = request()->except(['_token']);

       $user_ids = implode(',', $input['user_ids']);   
       //echo "<pre>"; print_r($user_ids); die();
      //  $this->notification->create($input);

        $save =  new AdminNotification();
        if($request->hasFile('image')){
        $folder_name ='notification';
        $save['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
    }
       // $save->link_type = $request->link_type;
        $save->message_heading = $request->message_heading;
        $save->message = $request->message;
        $save->user_ids = $user_ids;
        $save->save();

         return redirect('admin/notification/list')->with('success', trans('Notification Sent Successful.'));

        } catch (\Exception $e) {
                Session::flash('error',$e->getMessage());
        }
      return back();
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = User::where('id',$id)->first();
      // $data_vendor = Vendor::where('user_id',$id)->first();
       return view('backend.vendor.show',compact('data'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = User::where('id',$id)->first();
        //$data_image = Vendor::where('user_id',$id)->first();
        return view('backend.vendor.edit',compact('data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all() , ['first_name' => 'required','last_name'=>'required', 'email' => 'required|email', 'address_name' => 'required','city' => 'required', 'state' => 'required', 'country' => 'required','mobile_number' => 'required', 'pin_code' => 'required','image'=>'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        { 
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $vendor = $request->all();

       if($request->hasFile('image')){
         $folder_name ='image';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['email'] = $request->email;
        $data['address'] = $request->address_name; 
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['country'] = $request->country;
        $data['mobile_number'] =$request->mobile_number;
        $data['pin_code'] = $request->pin_code;

        $data =User::findOrFail($id)->update($data);


     /*  if ($request->hasFile('licensed_pharmacist_details'))
           {
            $folder_name = 'licensed_pharmacist_details';
            $licensed_pharmacist_details = $this->fileUpload($request->file('licensed_pharmacist_details') , false, $folder_name);
            }

        if ($request->hasFile('authorized_pharmacy_related_documents'))
        {
            $folder_name = 'authorized_pharmacy_related_documents';
            $authorized_pharmacy_related_documents = $this->fileUpload($request->file('authorized_pharmacy_related_documents') , false, $folder_name);
        }*/


        //echo $licensed_pharmacist_details.'===='.$id.'====='.$authorized_pharmacy_related_documents; die();
       // $data= DB::table('vendor')->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details,'authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents])->where('user_id ',$id);
        /* if(!empty($licensed_pharmacist_details) && !empty($authorized_pharmacy_related_documents)){
             $data = Vendor::where("user_id", $id)->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details,'authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents]);
         }elseif(!empty($licensed_pharmacist_details)){
             $data = Vendor::where("user_id", $id)->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details]);

         }elseif(!empty($authorized_pharmacy_related_documents)){
             $data = Vendor::where("user_id", $id)->update(['authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents]);
         }*/

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.vendor.list');
                    $response['message'] = "Vendor has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Vendor');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/

      }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Vendor';
        $confirm_route = $error = null;
        $confirm_route = route('admin.vendor.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
      // $data=User::find($id);
       //$data->delete();
      // $data=Vendor::where('user_id',$id);
      // $status =  $data->delete();
        if($status){
            request()->session()->flash('success','vendor successfully deleted');
                    return redirect()->route('admin.vendor.list');
        }
        else{
            request()->session()->flash('error','Error while deleting vendor');
        }
        return redirect()->route('admin.vendor.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.vendor.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = User::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.vendor.list');
     }
}

