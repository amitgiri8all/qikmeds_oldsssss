<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Frontcontain;
use Illuminate\Http\Request;
use Validator;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;
class FrontcontainController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Frontcontain::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Frontcontain::select('count(*) as allcount')
                //->where('role','vendor')
               ->Where('title', 'like', '%' .$searchValue . '%')
                /* ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = Frontcontain::orderBy($columnName,$columnSortOrder)
                //->where('role','vendor')
                 ->Where('title', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('front_content.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    //$delete_url = URL::to("admin/frontcontain/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/frontcontain/edit/$val->id");
                    $view_url = URL::to("admin/frontcontain/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                   if(!empty($val->image_icon)){
                    $url= $val->image_icon;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }   
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'title'=>$val->title,
                        'short_description'=>$val->short_description,
                       
                        'image_icon'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-primary" href="'.$edit_url.'"><i class="fa fa-edit"></i>Edit </a>
                        <a class="btn btn-info" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                        
                         

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.frontcontain.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.frontcontain.index');
       }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.frontcontain.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['title' => 'required','short_description'=>'required','image_icon' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
            // DB::beginTransaction();
            try
            {
                    //echo "<pre>"; print_r($request->all()); die();

                $front = new Frontcontain();

                if($request->hasFile('image_icon')){
                $folder_name ='image';
                $front['image_icon'] = $this->fileUpload($request->file('image_icon'),false,$folder_name);
                }
                $front->title = $request->title;
                $front->short_description = $request->short_description;
               
                $data = $front->save();
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.frontcontain.list');
                    $response['message'] = "Frontcontain has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Frontcontain');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = Frontcontain::where('id',$id)->first();
      // $data_vendor = Vendor::where('user_id',$id)->first();
       return view('backend.frontcontain.show',compact('data'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = Frontcontain::where('id',$id)->first();
        //$data_image = Vendor::where('user_id',$id)->first();
        return view('backend.frontcontain.edit',compact('data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all() , ['title' => 'required','short_description'=>'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {  
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $front = $request->all();

       if($request->hasFile('image_icon')){
         $folder_name ='image';
         $data['image_icon'] = $this->fileUpload($request->file('image_icon'),false,$folder_name);
        }

        $data['title'] = $request->title;
        $data['short_description'] = $request->short_description;
      

        $data =Frontcontain::findOrFail($id)->update($data);


     /*  if ($request->hasFile('licensed_pharmacist_details'))
           {
            $folder_name = 'licensed_pharmacist_details';
            $licensed_pharmacist_details = $this->fileUpload($request->file('licensed_pharmacist_details') , false, $folder_name);
            }

        if ($request->hasFile('authorized_pharmacy_related_documents'))
        {
            $folder_name = 'authorized_pharmacy_related_documents';
            $authorized_pharmacy_related_documents = $this->fileUpload($request->file('authorized_pharmacy_related_documents') , false, $folder_name);
        }*/


        //echo $licensed_pharmacist_details.'===='.$id.'====='.$authorized_pharmacy_related_documents; die();
       // $data= DB::table('vendor')->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details,'authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents])->where('user_id ',$id);
        /* if(!empty($licensed_pharmacist_details) && !empty($authorized_pharmacy_related_documents)){
             $data = Vendor::where("user_id", $id)->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details,'authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents]);
         }elseif(!empty($licensed_pharmacist_details)){
             $data = Vendor::where("user_id", $id)->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details]);

         }elseif(!empty($authorized_pharmacy_related_documents)){
             $data = Vendor::where("user_id", $id)->update(['authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents]);
         }*/

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.frontcontain.list');
                    $response['message'] = "Frontcontain has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Frontcontain');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/

      }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Frontcontain';
        $confirm_route = $error = null;
        $confirm_route = route('admin.frontcontain.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=Frontcontain::find($id);
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','Frontcontain successfully deleted');
                    return redirect()->route('admin.frontcontain.list');
        }
        else{
            request()->session()->flash('error','Error while deleting frontcontain');
        }
        return redirect()->route('admin.frontcontain.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.frontcontain.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = Frontcontain::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.frontcontain.list');
     }
}

