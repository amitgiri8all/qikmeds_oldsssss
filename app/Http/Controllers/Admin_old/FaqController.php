<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FaqTopics;
use Illuminate\Http\Request;
use Validator;
use App\Models\Faq;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;
class FaqController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Faq::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Faq::select('count(*) as allcount')
                ->Where('question', 'like', '%' .$searchValue . '%')
                /*->where('role','vendor')
                ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = Faq::orderBy($columnName,$columnSortOrder)
                 ->Where('question', 'like', '%' .$searchValue . '%')
               // ->where('role','vendor')
                // ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('faq.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/faq/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/faq/edit/$val->id");
                    $view_url = URL::to("admin/faq/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        //'faq_topic_id'=>$val->faq_topic_id,
                        'question'=>$val->question,
                        'answer'=>$val->answer,
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href="'.$edit_url.'"><i class="fa fa-edit">Edit</i> </a>
                        <a class="btn btn-success" href="'.$view_url.'"><i class="fa fa-eye">View</i> </a>
                        
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.faq.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.faq.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faq_topics = DB::table('faq_topics')->get();
        //echo "<pre>";print_r($faq_topics);die;
        return view('backend.faq.create',compact('faq_topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['topic_id' => 'required','question'=>'required', 'answer' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
            // DB::beginTransaction();
            try
            {
             //echo "<pre>"; print_r($request->all()); die();

                $faq = new Faq();

                $faq->faq_topic_id = $request->topic_id;
                $faq->question = $request->question;
                $faq->answer = $request->answer;
                $data = $faq->save();

               /// echo $data->id; die();
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.faq.list');
                    $response['message'] = "Faq has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Faq');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = Faq::where('id',$id)->first();
       $topics = Faq::join('faq_topics', 'faq.faq_topic_id', '=', 'faq_topics.id')
                ->where('faq.id',$id)->first(['faq_topics.*']);
       //echo "<pre>";print_r($topics);die;        
      // $data_vendor = Vendor::where('user_id',$id)->first();
       return view('backend.faq.show',compact('data','topics'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = Faq::where('id',$id)->first();
        $faq_topics  = DB::table('faq_topics')->get();
        //$data_image = Vendor::where('user_id',$id)->first();
        return view('backend.faq.edit',compact('data','faq_topics'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator = Validator::make($request->all() , ['topic_id' => 'required','question'=>'required', 'answer' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        { 
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $faq = $request->all();

        $data['faq_topic_id'] = $request->topic_id;
        $data['question'] = $request->question;
        $data['answer'] = $request->answer;

        $data =Faq::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.faq.list');
                    $response['message'] = "Faq has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Faq');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
       }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Faq';
        $confirm_route = $error = null;
        $confirm_route = route('admin.faq.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=Faq::find($id);
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','faq successfully deleted');
                    return redirect()->route('admin.faq.list');
        }
        else{
            request()->session()->flash('error','Error while deleting faq');
        }
        return redirect()->route('admin.faq.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.faq.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = Faq::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.faq.list');
     }
}

