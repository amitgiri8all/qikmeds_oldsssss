<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;
 use App\Models\User;
 use App\Models\Manufacturer;
use App\Traits\ImageTrait;
use DataTables;
use Session;
use Response;
use DB;
use URL;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Manufacturer::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Manufacturer::select('count(*) as allcount')->where('manufacturer_name', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Manufacturer::orderBy($columnName,$columnSortOrder)
                ->where('manufacturer.manufacturer_name', 'like', '%' .$searchValue . '%')
                ->select('manufacturer.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                 $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/manufacturer/$val->id/confirm-delete");
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }

                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'manufacturer_name'=>$val->manufacturer_name,
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href=""><i class="fa fa-edit"></i> Edit</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.manufacturer.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status"></i> '.$status.' </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.manufacturer.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
