<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Models\User;
use App\Traits\ImageTrait;
use DataTables;
use Session;
use Response;
use DB;
use URL;

class BlogController extends Controller
{
      use ImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page

                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');

                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value

                // Total records
                $totalRecords = Blog::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Blog::select('count(*) as allcount')->where('title', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Blog::orderBy($columnName,$columnSortOrder)
                ->where('blog.title', 'like', '%' .$searchValue . '%')
                ->select('blog.*')
                ->skip($start)
                ->take($rowperpage)
                ->get();
                //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/blog/$val->id/confirm-delete");
                     if($val->status==1){
                        $btnclass = "btn-success";
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                    if(!empty($val->image)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$val->image);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                    $data_arr[] = array(
                        'id'=>$val->id,
                        'title'=>$val->title,
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href=""><i class="fa fa-edit"></i> Edit</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.blog.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> '.$status.' </a>
                        '
                    );
                 // }
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );

                 echo json_encode($response); die;

            }

            return view('backend.blog.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request){
            try {
            $data= $request->all();
              if($request->hasFile('image')){
                $folder_name ='blog';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }
            $status = Blog::create($data);
            if($status){
                return redirect('admin/blog/list')->with('success', trans('You have been successfully create blog'));
                request()->session()->flash('success','You have been successfully create blog');
            }
            else{
                request()->session()->flash('error','Error occurred, Please try again!');
            }
            } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
            }
            return back();
      }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Blog';
        $confirm_route = $error = null;
        $confirm_route = route('admin.blog.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
         $data=Blog::find($id);
        $status=$data->delete();
        if($status){
            request()->session()->flash('success','Blog successfully deleted');
                    return redirect()->route('admin.blog.list');
        }
        else{
            request()->session()->flash('error','Error while deleting category');
        }
        return redirect()->route('admin.blog.list');
    }

     //Here is the call status model view page ------
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';};
       $route_link = route('admin.blog.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';};
       $status = Blog::where('id',$id)->update(['status'=>$status]);

       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.blog.list');
     }
}
