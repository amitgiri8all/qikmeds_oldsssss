<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\OfferRequest;
use App\Models\Offer;
 use App\Models\User;
 use DataTables;
use Session;
use App\Traits\ImageTrait;
use DB;
use URL;
class OfferController extends Controller
{

    use ImageTrait;
    // Define All Properties -----------
    protected $offer;
    protected $user;
    protected $method;
    // Define Constructer Methode ----------- 
    function __construct(Request $request, Offer $offer, User $user)
    {
        // Initialize the object and its properties by assigning value(models)----------- 
        $this->offer = $offer;
        $this->user = $user;
        $this->method = $request->method();
     }

        public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Offer::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Offer::select('count(*) as allcount')->where('name', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Offer::orderBy($columnName,$columnSortOrder)
                ->where('offers.name', 'like', '%' .$searchValue . '%')
                ->select('offers.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                 $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/offer/$val->id/confirm-delete");
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }

                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'name'=>$val->name,
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href=""><i class="fa fa-edit"></i> Edit</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>                       '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.offer.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    //Pass posts data to view and load datatable view -----------
    public function data()
    {
        $offer = $this->offer->get();
        return DataTables::of($offer)->addColumn('created_at', function ($offer){
           return date('Y/',strtotime($offer->created_at));
        })
        ->addIndexColumn()
        ->addColumn('actions', '
            @if(App\Models\Permission::checkPermission("offer.edit"))
                <a class="btn btn-info" href="{{URL::to("offer/edit/$id")}}"><i class="fa fa-edit"></i>Edit
                </a>
            @endif
            @if(App\Models\Permission::checkPermission("offer.delete"))
                <a data-toggle="modal" data-target="#delete_confirm" href="{{URL::to("offer/$id/confirm-delete")}}" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i>
                 Delete
                 </a>
            @endif
            ')
        ->rawColumns(['actions'])
        ->make(true);
    }

    //Load insert form view ----------
    public function create()
    {
      return view('backend.offer.add');
    }

    //Insert post data in table ---------
    public function store(OfferRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('image'))
        {
            $this->fileUpload($request->file('image'));
            $input['image'] = $this->fileUpload($request->file('image'));
        }
        try
        {
            $this->offer->create($input);
            return redirect('admin/offer/list')->with('success', trans('Offer Create Successfully.'));

            //Session::flash('success', 'Offer create successful');
        }
        catch(\Exception $e)
        {
            Session::flash('error', $e->getMessage());
        }
        return back();
    }

    //Get post data by id  and load edit form----------
    public function edit($id = null)
    {
      $data = $this->offer->withoutGlobalScope(StatusScope::class)->findOrFail($id);
      return view('backend.offer.edit', compact('data'));
    }
    
    //update post data by id ----------------
    public function update(OfferRequest $request, $id = null)
    {
        $input = request()->except(['_token']);
        try
        {
            $this->offer->withoutGlobalScope(StatusScope::class)->findOrFail($id)->update($input);
            //.Session::flash('success', 'Offer Update Successfully');
            return redirect('admin/offer/list')->with('success', trans('Offer Update Successfully.'));

        }
        catch(\Exception $e)
        {
            Session::flash('error', $e->getMessage());
        }
        return back();
    }

    //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Offer';
        $confirm_route = $error = null;
        $confirm_route = route('offer.delete', $id);
        return View('admin/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

     //Here is the part for delete data
     public function destroy($id)
     {
        $page = $this->offer->withoutGlobalScope(StatusScope::class)->findOrFail($id);
        $page->delete();
        $page->deleteTranslations();
        if($page){
         return redirect('offer/list')->with('success', trans('Offer Delete Successfully.'));
        }else{
         return redirect('offer/list')->with('error', trans('There was an issue deleting the recode. Please try again.'));
        }
     }


}

