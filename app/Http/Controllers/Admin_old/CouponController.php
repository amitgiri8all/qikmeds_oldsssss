<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Http\Requests\CouponRequest;
use App\Models\User;
use DataTables;
use Session;
use Response;
use DB;
use URL;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Coupon::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Coupon::select('count(*) as allcount')->where('name', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Coupon::orderBy($columnName,$columnSortOrder)
                ->where('coupons.name', 'like', '%' .$searchValue . '%')
                ->select('coupons.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/coupon/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/coupon/edit/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                    if($val->coupon_type=='percentages'){ $coupon_type = "<span class='badge badge-success'>percentages</span>";}else{$coupon_type = "<span class='badge badge-danger'>Amount</span>";};
                    
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'name'=>$val->name,
                        'code'=>$val->code,
                        'coupon_type'=>$val->coupon_type,
                        'coupon_value'=>$val->coupon_value,
                        'start'=>date('d F,Y',strtotime($val->from_time)),
                        'end'=>date('d F,Y',strtotime($val->to_time)),
                        'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href="'.$edit_url.'"><i class="fa fa-edit"></i> Edit</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.coupon.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> '.$status.' </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.coupon.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function store(CouponRequest $request){
            try {
            $data= $request->all();
            $status = Coupon::create($data);
            if($status){
                return redirect('admin/coupon/list')->with('success', trans('You have been successfully create coupon'));
                request()->session()->flash('success','You have been successfully create coupon');
            }
            else{
                request()->session()->flash('error','Error occurred, Please try again!');
            }
            } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
            }
            return back();
      }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon,$id)
    {
      $data = Coupon::where('id',$id)->first();  
      return view('backend.coupon.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {
      try {   
       $data = $request->all();
      $data =Coupon::findOrFail($id)->update($data);
       if($data){
            return redirect('admin/coupon/list')->with('success', trans('You have been successfully update coupon'));
         }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }
        return back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Coupon';
        $confirm_route = $error = null;
        $confirm_route = route('admin.coupon.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       // echo $id; die();
        $data=Coupon::find($id);
         $status=$data->delete();
    // echo $status; die();
        if($status){
             
            request()->session()->flash('success','Coupon successfully deleted');
                    return redirect()->route('admin.coupon.list');
        }
        else{
            request()->session()->flash('error','Error while deleting category');
        }
        return redirect()->route('admin.coupon.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.coupon.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = Coupon::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.coupon.list');
     }

}
