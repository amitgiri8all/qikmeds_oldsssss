<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Hash;
use Session;
class ProfileController extends Controller
{

	public function index(){
    // echo url()->full(); die();

    //dd($user);
    return view('backend.profile.index');
	//return view('backend.profile.index',compact('user'));
	}

	public function change_password(Request $request)
    {
        $request->validate([
            'password' => ['required'],
            'password_confirm' => ['same:password'],
        ]);
        User::find(Auth::guard('admin')->user()->id)->update(['password'=> Hash::make($request->password)]);
        Session::flash('success',' Password change successfully.');
         return back();
    }


}
