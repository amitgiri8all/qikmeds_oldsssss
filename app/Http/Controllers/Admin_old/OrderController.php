<?php

namespace App\Http\Controllers\Admin;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\TrackOrder;
use App\Models\OrderItem;
use App\Models\VendorProductStatus;
use App\Models\Sitesettings;
use App\Models\VendorTrackOrder;
use Carbon\Carbon;
use Validator;
use Response;
use Session;
use DB;
use URL;
use Auth;
 class OrderController extends Controller
{

    //Define All Properties -----------
    protected $order;
    protected $user;
    protected $orderPayment;
    //Define Constructer Methode -----------
    function __construct(Request $request, Order $order, OrderPayment $orderPayment,User $user) {
        //Initialize the object and its properties by assigning value(models)-----------
        $this->order = $order;
        $this->user = $user;
        $this->orderPayment = $orderPayment;
        $this->method = $request->method();
    }

     public function index(Request $request)
    {
/*         try{
*/            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
 
                ## Custom Field value
                $order_status = $request->get('order_status');
                // Total records
                $totalRecords = $this->order->select('count(*) as allcount')->where('status','!=','accepted')->count();
                //echo $totalRecords; die();
                 $order= $this->order->select('count(*) as allcount','order.order_id')->with(['User','OrderPayment','DeliveryAddress','OrderItem'])->where('status','!=','accepted');
                if($searchValue!=''){ 
                $order->whereHas('User', function($q) use ($searchValue) {
                    $q->where('name', 'like', '%' . $searchValue . '%');
                   });
                }
              
                if($order_status!=''){ 
                $order->where('order_status',$order_status);
                }
                //echo "<pre>";print_r($request->get('to_date'));die;
                if ($request->get('from_date') and !empty($request->from_date)) {
                  $order->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                  $order->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }
                $totalRecordswithFilter=$order->count();
               // Fetch records
               $records = $this->order-> 
                   with(['User','OrderPayment','DeliveryAddress','OrderItem'])
                 ->select('*')
                 ->skip($start)
                ->take($rowperpage)
                ->where('status','!=','accepted')
                ->whereHas('User', function($q) use ($searchValue) {
                    $q->where('name', 'like', '%' . $searchValue . '%');
                   })
                 ->orderBy('id','DESC');
                if($order_status!=''){ 
                  $records->where('order_status',$order_status);
                }
                  
                if ($request->get('from_date') and !empty($request->from_date)) {
                $records->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                $records->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }
                $records=$records->get();
               // echo "<pre>";print_r($records[0]->order_status);die;

                $data_arr = [];
                foreach($records as $key=> $val){

                    $delete_url = URL::to("banner/$val->id/confirm-delete");
                    $status_url = URL::to("admin/order/statuslist/$val->id");

                    $view_url = URL::to("admin/order/view/$val->id");
                    $view_invoice = URL::to("admin/order/invoice/$val->id");
                     if($val->shipping_amount<=0){
                        $shipping_amount = "Free";
                    }else{ 
                        $shipping_amount = '<span>₹'.$val->shipping_amount.'</span>';
                    }

                    /*if(array_key_exists($val->order_status, Helper::$order_status)) {
                         $order_status_name =  Helper::$order_status[$val->order_status];
                     } else {
                       echo  "";
                     }*/

                    if($val->order_status =='R'){
                      $view_url_return = URL::to("admin/order/return-view/$val->id");
                      $button_return = '<a title="Order Returned" class="btn btn-danger" style="margin:3px;" href="'.$view_url_return.'" >Order Returned <i class="fa fa-eye"></i></a>';
                    }else{
                      $button_return = '';
                    }  

                    $data_arr[] = array( 
                        'order_id'=>$val->id,
                        'user_name'=>ucwords($val->user->name),
                        'payment_method'=>isset($val->OrderPayment->payment_method)?$val->OrderPayment->payment_method:'--',
                      //  'payment_method'=>'Cash',
                        'sub_total'=>'<span>₹'.$val->sub_total.'</span>', 
                        'shipping_amount'=>$shipping_amount,
                        'total_payed_amount'=>'<span>₹'.$val->total_payed_amount.'</span>',
                        'discount_coupon_code'=>isset($val->discount_coupon_code)?'<span>₹'.$val->discount_coupon_code.'</span>':'==',
                        'status'=>
                     '<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm" onclick="popupchangestatus('.$val->id.',\''.$val->order_status.'\')">Change Status</button>
                       '
                      ,
                        'orderstatus'=>Helper::$order_status[$val->order_status],
                        'created_at'=> date('d F,Y', strtotime($val->created_at)),
                         'action'=>' 
                        <a title="Order Details View" class="btn btn-success" style="margin:3px;" href="'.$view_url.'">Order Details <i class="fa fa-info-circle"></i></a>
                        '.$button_return.'
                        <a title="Invoice"  class="btn btn-info" href="'.$view_invoice.'">View Invoice <i class="fa fa-file"></i></a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" =>  $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.order.index');
       
      /*  } catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    } 
     public function accepted_orders(Request $request)
    {
/*         try{
*/            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
 
                ## Custom Field value
                $order_status = $request->get('order_status');
                // Total records
                $totalRecords = $this->order->select('count(*) as allcount')->where('status','accepted')->count();
                //echo $totalRecords; die();
                 $order= $this->order->select('count(*) as allcount','order.order_id')->with(['User','OrderPayment','DeliveryAddress','OrderItem']);
                if($searchValue!=''){ 
                $order->whereHas('User', function($q) use ($searchValue) {
                    $q->where('name', 'like', '%' . $searchValue . '%');
                   });
                }
              
                if($order_status!=''){ 
                $order->where('status',$order_status);
                }
                if ($request->get('from_date') and !empty($request->from_date)) {
                  $order->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                  $order->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }
                $totalRecordswithFilter=$order->count();
               // Fetch records
               $records = $this->order-> 
                   with(['User','OrderPayment','DeliveryAddress','OrderItem'])
                 ->select('*')
                 ->skip($start)
                ->take($rowperpage)
                ->where('status','accepted')
                ->whereHas('User', function($q) use ($searchValue) {
                    $q->where('name', 'like', '%' . $searchValue . '%');
                   })->orderBy('order_id','DESC');
                if($order_status!=''){ 
                  $records->where('status',$order_status);
                }
                  
                if ($request->get('from_date') and !empty($request->from_date)) {
                $records->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                $records->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }
                $records=$records->get();

                $data_arr = [];
                foreach($records as $key=> $val){
                    $delete_url = URL::to("banner/$val->id/confirm-delete");
                    $view_url = URL::to("admin/order/view/$val->order_id");
                    $view_invoice = URL::to("admin/order/invoice/$val->order_id");
                     if($val->shipping_amount<=0){
                        $shipping_amount = "Free";
                    }else{ 
                        $shipping_amount = '₹'.$val->shipping_amount;
                    }
                    $status = '<span class="badge badge-success">'.ucwords($val->status).'</span>';

                    $data_arr[] = array( 
                        'order_id'=>$val->order_id,
                        'user_name'=>ucwords($val->user->name),
                        'payment_method'=>ucwords($val->orderpayment->payment_method),
                        'sub_total'=>'₹'.$val->sub_total,
                        'shipping_amount'=>$shipping_amount,
                        'total_payed_amount'=>$val->total_payed_amount,
                        'discount_coupon_code'=>isset($val->discount_coupon_code)?$val->discount_coupon_code:'==',
                        'status'=>$status,
                        'created_at'=> date('d F,Y', strtotime($val->created_at)),
                         'action'=>'
                        <a title="Order Details View" class="btn btn-success" href="'.$view_url.'"><i class="fa fa-info-circle"></i></a>
                        <a title="Invoice"  class="btn btn-info" href="'.$view_invoice.'"><i class="fa fa-file"></i></a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" =>  $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;    
            }
            return view('backend.order.accepte-orders');
       
      /*  } catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    }


  /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
     $vendors = User::where('role','vendor')->whereNotNull('name')->get();   
     $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$id)->first();
      return view('backend.order.order-details',compact('order','vendors'));
    }

    public function invoice($id=null){
      $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$id)->first();
      return view('backend.order.invoice',compact('order')); 
    }

    public function assign_vendorf(Request $request){
      $ord_item_id = $request->get('ord_item_id');
        $status = OrderItem::whereIn('ord_item_id',$ord_item_id)
        ->update(array('vendor_id' =>$request->vendor_id));
         // echo $status; die();
        if($status){
                   $updateOrderStatus = OrderItem::where('order_id', $request->order_id)->whereNull('vendor_id')->count();
                    if($updateOrderStatus>0){
                     $status ="PD";
                    }else{
                       $status ="AC";
                    }
    $track = new VendorTrackOrder();
    $track->order_id =  $request->order_id;
    $track->vendor_id = $request->vendor_id;
    $track->date_time = Carbon::now();
    $track->track_status = 'Order in Progress';
    $track->save();
                    Order::where('id', $request->order_id)
                      ->update(array('order_status' => $status));

                        $status = new VendorProductStatus();
                        $status->order_id = $request->order_id;
                        $status->vendor_id =$request->vendor_id;
                        $status->save();



                        $response['status'] = "success";
                        $response['loading'] =  'true';
                        $response['message'] = "You has been created successfully accepte order.";
                        return json_encode($response);

          }else{
              $response['status'] = "error";
              $response['message'] = "There was an issue account createing the recode. Please try again.";
              return json_encode($response);
           }  

      // return $getItemsIds;

        /**/
    }

      /**Order Accepted**/
    public function assign_vendor(Request $request){
      $vendor_id = $request->get('vendor_id');
      $order_id = $request->get('order_id');
      $ord_item_id = $request->get('ord_item_id');
      if(!empty($ord_item_id)){
       // \DB::enableQueryLog();
        $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
          ->update(array('vendor_id' => $vendor_id));
      }else{
        $get_all_order_item = OrderItem::select('ord_item_id')->whereNull('vendor_id')->where('order_id',$order_id)->get();
         $ord_item_id = $get_all_order_item->pluck('ord_item_id');
            $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
          ->update(array('vendor_id' => $vendor_id));
      } 
      if($status){
        $updateOrderStatus = OrderItem::where('order_id', $order_id)->whereNull('vendor_id')->count();
            if($updateOrderStatus>0){
                    $statusV ="PD";
                    }else{
                       $statusV ="AC";
                    }
                    $track = new VendorTrackOrder();
                    $track->order_id =  $order_id;
                    $track->vendor_id = $vendor_id;
                    $track->date_time = Carbon::now();
                    $track->track_status = 'Order in Progress';
                    $track->save();
                    Order::where('id', $request->order_id)
                      ->update(array('order_status' =>$statusV));

        $status = new VendorProductStatus();
        $status->order_id = $order_id;
        $status->vendor_id = $vendor_id;
        $status->save();

        $response['status'] = "success";
        $response['loading'] =  'true';
        $response['message'] = "You has been created successfully accepte order.";
        return json_encode($response);
      }else{
          $response['status'] = "error";
          $response['message'] = "There was an issue account createing the recode. Please try again.";
          return json_encode($response);
       }  
    }


    public function statuslist($id)
    {
       // echo $id; die();
     // $orders = $this->orderstatusnew->with(["ProductOrder",'User'])->where('order_id', '=',$id)->orderBy('order_id', 'DESC')->get();
     // echo "<pre>"; print_r($orders); die();
      return view('admin/order/statuslist');
    } 


    public function changeStatus(Request $request){
         $order= $this->order->findOrFail($request->id);
         $status =$request->status;
         //echo $status; die();
         $checked = TrackOrder::where('order_id',$request->id)->where('order_track_status',$status)->count();
         //echo $checked; die();
         if($checked>0){
            echo "ha";  
         }else{
            $track = new TrackOrder();
            $track->order_id           = $request->id;
            $track->order_track_status = $status;
            $track->date_time          = Carbon::now();
            $track->save();
        }    
        // echo $status; die();
         /*====================*/
        // echo $AppSetting->return_days; die();
        if($status=='D'){ 
         $AppSetting = Sitesettings::where('id',1)->first();
         $current_date = date('Y-m-d H:i:s');
         $this->order->where('id',$request->id)->update(['return_days'=>$AppSetting->return_days,'delivered_date'=>$current_date]);
        } 
         /*====================*/
         $update = Helper::$order_status[$status];
         $data = $order->fill(['order_status'=>$status])->save();
         if($request->ajax()){
                if($data){
                    return response()->json([
                        'status' => true,
                        'message' => 'update'
                    ],200);
                }else{
                    return response()->json([
                        'status' => false,
                        'data' =>$order,
                        'message' => 'some thing is wrong.'
                    ],400);
                }
         }
 } 


  public function return_order_details(Request $request,$id=null){
    $vendors = User::where('role','vendor')->get();   
    $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('order_status','R')->where('id',$id)->first();
    //echo "<pre>"; print_r($order); die('okk');  
    return view('backend.order.return-order-details',compact('order','vendors'));

  }

  public function accepte_return_product(Request $request){   
   //echo "<pre>"; print_r($request->all()); die();
    //echo $request->order_id.'==='.$request->order_item_id; die();
      $update = OrderItem::where('order_id',$request->order_id)->where('ord_item_id',$request->order_item_id)->update(['is_return'=>0]);
    // echo $update; die();
      if ($update>0){
        $response['status'] = "success";
        $response['message'] = "Successfully added New Card.";
        return Response::json($response);
      }else{
        $response['status'] = "error";
        $response['message'] = "Something went wrong ";
        return Response::json($response);
      }
  }
 

}
