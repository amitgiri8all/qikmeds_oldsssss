<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Notifications\OrderPlace;
use App\Notifications\AllOrderStatus;
use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use App\Models\UserSubscriptionOrder;
use App\Models\SubscriptionPlan;
use App\Models\OrderReferralCode;
use App\Models\Coupon;
use App\Models\OfferBanner;
use App\Traits\HelperTrait;
use DB;
use Notification;   
use Auth;
use Session;
use PDF;
use Storage;
class OrderController extends Controller
{
    use HelperTrait;

    protected $order;
    protected $deliveryAddress;
    protected $orderPayment;

    public function __construct(Request $request,Order $order,DeliveryAddress $deliveryAddress,OrderPayment $orderPayment,OrderItem $orderItem,OrderReferralCode $orderReferralCode,OfferBanner $offer_banner){
     $this->order           = $order;
     $this->deliveryAddress = $deliveryAddress;
     $this->orderPayment    = $orderPayment;
     $this->orderItem               = $orderItem;
     $this->orderReferralCode       = $orderReferralCode;
     $this->offer_banner       = $offer_banner;
      //$this->orderPayment    = $orderPayment;
    }
    
    //Save Order Recods
    public function place_orders(Request $request){
   //  try {   
     //echo "<pre>"; print_r($request->all()); die();      
      $payment_method = isset($request->payment_method)?$request->payment_method:'';
     // echo $payment_method; die();
     if(empty($payment_method)){ 
        $json['success'] = false;
        $json['status']  = 'error';
        $json['message']  = 'Please select payment method';
        echo json_encode($json); 
      }else{
        $cart = session('cart');
        //echo "<pre>"; print_r($cart); die('okkk');
        $subscriptionplan =SubscriptionPlan::where('plan_type','user')->where('id',Session::get('plan_id'))->first();
          if(!empty($cart) && empty($subscriptionplan)){
               // echo 'ok fine'; die();
               $total = 0 ;
               $is_prescription = 0;
               $ab =false;
               foreach((array) session('cart') as $id => $details){

                    $price= str_replace('₹', '',$details['price']);
                    $total += (int) $price * (int) $details['quantity'];
                    
                    if((!$ab) && ($details['prescription']=='1')){
                        $ab = true;
                        $is_prescription = 1;    
                    }
               }
              //  echo $is_prescription.'====='.$ab; die();

               $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
               $qikmeds_discount = $site_setting->qikmeds_discount;
               $delivery = $site_setting->delivery_charge;
              //Check delivery calculation 
               if($site_setting->min_price > $total){
                $delivery_charge = $delivery;
               }else{
                $delivery_charge =0;
               }
              // echo $delivery_charge; die();
              //Get Total Cart item
              $total_item =count((array) session('cart'));
              //Get Delivery Addresss
              $user_id = Auth::guard('web')->user()->id;
              $address = $this->deliveryAddress->select('id','user_id')->where('user_id',$user_id)->where('status',1)->first();
              $address = $this->deliveryAddress->where('user_id',$user_id)->where('status',1)->first();

              $promo_val = Session::get('promo_val');
              $total_payed_amount = $total-$qikmeds_discount+$delivery_charge-$promo_val;
              $total_amt = $total+$delivery_charge-$site_setting->qikmeds_discount-$promo_val; 
              $total_saving = $site_setting->qikmeds_discount+$promo_val;

              //echo $total_amt.'==========='.$total_saving; die();
                $order_code =  $this->orderCode();
                $patient_id =  $this->patient_id();
                $patient_info =  $this->patient_info();

              $input = $request->all();
              //Here is is_prescription products

              if($is_prescription==1){
                   $input['order_status']               = 'N'; 
                  $input['is_prescription']            = 'Y';
              }else{  
                  $input['order_status']               = 'CC'; 
                  $input['is_prescription']            = 'N';
              }
              $input['sub_total']            = $total;
              $input['shipping_amount']      = $delivery_charge;
              $input['discount_amount']      = $qikmeds_discount;
              $input['total_item']           = $total_item;
              $input['user_id']              = $user_id;
              $input['address_id']           = $address->id;
              $input['total_payed_amount']   = $total_amt;
              $input['discount_coupon_code']   = $promo_val;
              $input['order_code']   = $order_code;
              $input['patient_id']   = $patient_id;
              $input['patient_info']   = $patient_info;
              

              $input['delivery_address_info']   = $address;
              //Create Orders  
              //Offer Code Calcuation code here ============================/
              /*========== Offer Banner =======*/
                if(!empty(Session::get('offer_id_aapyed'))){
                 $offerid = Session::get('offer_id_aapyed');
                  $offdata = OfferBanner::where('id',$offerid)->first();   
                  if($offdata->offer_type!='discount'){
                      $input['offer_status'] = 'N'; 
                  }else{
                        $input['offer_status'] = 'Y'; 
                  }
                  $input['offer_code'] = $offdata->code; 
                  $input['offer_data'] = $offdata;
                  $input['offer_id'] = $offdata->id;
                  $input['offer_price'] = $offdata->price;
                } 
                //echo "<pre>"; print_r($data); die();
                /*===========             =======*/    
              // Coupan Code Calcuation ==================
              $coupan_id = Session::get('coupan_id'); 
              //echo $coupan_id; die();
              //GET VALUE
              $coupondata = Coupon::select('id','code','name','coupon_type','coupon_value')->where('id',$coupan_id )->first();

               $input['coupon_amount'] = isset($coupondata->coupon_value)?$coupondata->coupon_value:null;
              $input['coupon_code']   = isset($coupondata->code)?$coupondata->code:null;

              //Referral code Calcuation =========================
              $order_referral_code = Session::get('referrer_code');
               if(!empty($order_referral_code)){
                    $check_ref_code =  User::where('referrer_code',$order_referral_code)->where('id','!=',Auth::guard('web')->user()->id)->where('role','user')->first();
                     if(!empty($check_ref_code)){
                        $form_user_id =Auth::guard('web')->user()->id;     
                        $to_user_id = $check_ref_code->id;     
                        $referrer_code = $check_ref_code->referrer_code;   
                    }   
               } 
              /*=============================================*/
               $customer_data =  Session::get('customer_note');
              $input['customer_notes']   = $customer_data;
              // echo $customer_data; die('BCA');

            // echo "<pre>"; print_r($input); die('giri');
              $order = $this->order->create($input);
               $this->invoice_create($order->id);

              $orderPayment['order_id']       =$order->id; 
              $orderPayment['payment_method'] =$request->payment_method; 
               $orderPayment['total_amount']   =$total_amt; 
              //Create order payment methode      
               $this->orderPayment->create($orderPayment);
             if(isset($order_referral_code)){  
                $value['order_id'] = $order->id;
                $value['form_user_id'] = $form_user_id;
                $value['to_user_id'] = $to_user_id;
                $value['referral_code'] = $referrer_code;
                $this->orderReferralCode->create($value);
             }

              //Create Order item --------------------
              $orderItem = [];
              foreach((array) session('cart') as $id => $details){
                    $product_info =   $this->getProdcutDetails($details['id']);
                    $orderItem =  new orderItem();
                    $orderItem->product_id = $details['id'];
                    $orderItem->data = $product_info;
                    $orderItem->price = $details['price'];
                    $orderItem->prescription = $details['prescription'];
                    $orderItem->product_price = $details['price'];
                    $orderItem->product_name = $details['name'];
                    $orderItem->quantity_order = $details['quantity'];
                    $orderItem->order_id = $order->id;
                    $orderItem->save();
                }
            Session::forget('total_saving');
            Session::forget('total_amt');
            Session::forget('promo_val');
            Session::forget('cart');
            Session::forget('customer_note');
                
            $user = User::all();
            //Notification::send($user,new OrderPlace('New order placed'));
              /*send notification to customer*/

    $user_id_array1 = User::where('id',Auth::guard('web')->user()->id)->select('id','device_type','device_token','name')->get();
    // echo "<pre>"; print_r($user_id_array1); die();
    $userData = User::where('id', '=', Auth::guard('web')->user()->id )->select('device_token')->get();
    $user_id_array = collect($userData)->pluck('device_token');
    $senderName = $user_id_array1[0]->name;
    $message    = '#'.$order->order_code.trans('order.new_order');
    $type       = 'new order';
    $order->user->notify(new AllOrderStatus($order,$senderName,$message,$type));
            $json['message'] = 'Your order successfully placed...';
            $json['status']  = 'success';
            $json['redirect_url']  = route('home');
            $json['url']     = route('home');
            echo json_encode($json);  

        }elseif(!empty($subscriptionplan) && !empty($cart)){
            //echo "sadfasdf"; die();
            $this->UserSubscriptionOrder($subscriptionplan->sale_price,$subscriptionplan->id);
                $is_prescription = 0;
               $ab =false;
               foreach((array) session('cart') as $id => $details){

                    $price= str_replace('₹', '',$details['price']);
                    $total += (int) $price * (int) $details['quantity'];
                    
                    if((!$ab) && ($details['prescription']=='1')){
                        $ab = true;
                        $is_prescription = 1;    
                    }
               }

               $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
               $qikmeds_discount = $site_setting->qikmeds_discount;
               $delivery = $site_setting->delivery_charge;

               //Check delivery calculation 
               if($site_setting->min_price > $total){
                $delivery_charge = $delivery;
               }else{
                $delivery_charge =0;
               }

              // echo $delivery_charge; die();
              //Get Total Cart item
              $total_item =count((array) session('cart'));
              //Get Delivery Addresss
              $user_id = Auth::guard('web')->user()->id;
              $address = $this->deliveryAddress->select('id','user_id')->where('user_id',$user_id)->where('status',1)->first();

              $promo_val = Session::get('promo_val');
              //$total_amt = Session::get('total_amt');
             // $total_saving = Session::get('total_saving');
              $total_payed_amount = $total-$qikmeds_discount+$delivery_charge-$promo_val;

                $total_amt = $total+$delivery_charge-$site_setting->qikmeds_discount-$promo_val; 
                $total_saving = $site_setting->qikmeds_discount+$promo_val;

              //echo $total_amt.'==========='.$total_saving; die();
                $order_code =  $this->orderCode();
                $patient_id =  10;
              $customer_data =  Session::get('customer_note');
               //echo $customer_data;die ('amit');
              // echo $patient_id; die();
              $input = $request->all();
               if($is_prescription==1){
                   $input['order_status']               = 'N'; 
                  $input['is_prescription']            = 'Y';
              }else{  
                  $input['order_status']               = 'CC'; 
                  $input['is_prescription']            = 'N';
              }
              $input['sub_total']            = $total;
              $input['shipping_amount']      = $delivery_charge;
              $input['discount_amount']      = $qikmeds_discount;
              $input['total_item']           = $total_item;
              $input['user_id']              = $user_id;
              $input['address_id']           = $address->id;
              $input['total_payed_amount']   = $total_amt;
              $input['discount_coupon_code']   = $promo_val;
              $input['order_code']   = $order_code;
              $input['patient_id']   = isset($patient_id)?$patient_id:0;
              $input['customer_notes'] =  $customer_data;
              //Create Orders  
           // echo "<pre>"; print_r($input); die();
              $order = $this->order->create($input);

              $orderPayment['order_id']       =$order->id; 
              $orderPayment['payment_method'] =$request->payment_method; 
              $orderPayment['total_amount']   =$total_amt; 
              //Create order payment methode
              $this->orderPayment->create($orderPayment);
              //Create Order item --------------------
              $orderItem = [];
              foreach((array) session('cart') as $id => $details){
                    $orderItem =  new orderItem();
                    $orderItem->product_id = $details['id'];
                    $orderItem->price = $details['price'];
                    $orderItem->prescription = $details['prescription'];
                    $orderItem->product_name = $details['name'];
                    $orderItem->quantity_order = $details['quantity'];
                    $orderItem->order_id = $order->id;
                    $orderItem->save();
                }
                Session::forget('total_saving');
                Session::forget('total_amt');
                Session::forget('promo_val');
                Session::forget('cart');
                Session::forget('plan_id');
                Session::forget('customer_note');
                session()->forget('referrer_code'); 
                $user = User::all();
            /*send notification to customer*/

    $user_id_array1 = User::where('id',Auth::guard('web')->user()->id)->select('id','device_type','device_token','name')->get();
    // echo "<pre>"; print_r($user_id_array1); die();
    $userData = User::where('id', '=', Auth::guard('web')->user()->id )->select('device_token')->get();
    $user_id_array = collect($userData)->pluck('device_token');
    $senderName = $user_id_array1[0]->name;
    $message    = '#'.$order->order_code.trans('order.new_order');
    $type       = 'new order';
    $order->user->notify(new AllOrderStatus($order,$senderName,$message,$type));
           // Notification::send($user,new OrderPlace('New order placed'));
            $json['message'] = 'Your order successfully placed...plan is activate';
            $json['status']  = 'success';
            $json['redirect_url']  = route('home');
            $json['url']     = route('home');

            echo json_encode($json); 
          }elseif(!empty($subscriptionplan) && empty($cart)){
          //  echo "dsafasd"; die();
                        Session::forget('total_saving');
                        Session::forget('total_amt');
                        Session::forget('promo_val');
                        Session::forget('cart');
                        Session::forget('plan_id');
                        $this->UserSubscriptionOrder($subscriptionplan->sale_price,$subscriptionplan->id);
                        $json['message'] = 'Your Subscription successfully...';
                        $json['status']  = 'success';
                        $json['redirect_url']  = route('home');
                        $json['url']     = route('home');

                         echo json_encode($json); 
          } else{
                        $json['message'] = 'Order Not placed';
                        $json['status']  = 'error';
                        $json['redirect_url']  = route('home');
                        $json['url']     = route('home');
                         echo json_encode($json); 
          } 

      /*   } catch (\Exception $e) {
            Session::flash('error',$e->getMessage());
        }   */     
      }

    }


    /*



    */



// Place Order Close \\
    public function invoice_create($order_id){
        $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$order_id)->first();

        //return view('frontend.invoice',compact('order'));
        //echo "<pre>"; print_r($order); die();
        view()->share('order',$order);
        $pdf = PDF::loadView('frontend.invoice')->setOptions(['defaultFont' => 'sans-serif']);
       // return $pdf->Download('amit.pdf');
        $content = $pdf->download()->getOriginalContent();
        $extension = 'pdf';
        $fileName           =   time().'-invoice.'.$extension;
        $folderName         =   strtoupper(date('M'). date('Y'))."/";
        $name = $fileName;
        //echo $name; die();
        Storage::put('public/invoice/'.$name.'',$content);
        $save = 'invoice/'.$name;
       // echo $save; die();
        //echo "success"; die();
        $this->order->where('id',$order_id)->update(['invoice_pdf'=>$save]);

    } 

     public static function getProdcutDetails($id){
         $query = Product::select('products.*','product_image.image','categories.category_name','manufacturer.manufacturer_name')
         ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->leftJoin('product_image', function($join){
            $join->on('product_image.product_id', '=', 'products.p_id');
            $join->limit(1);
        })
        ->where('products.id',$id)
        ->groupBy('product_image.pro_img_id');
        $data = $query->first();
        return $data;
    }

    public function extraProductPlaceOrder(Request $request){
    // try { 

            $order_id = decrypt($request->order_id);
            $product_id = $request->product_id;

            $query = Product::select('brand.brand_name','products.id','products.medicine_name','products.slug','products.mrp','products.sale_price','products.brand','products.prescription','products.type_of_sell','products.salt','products.is_featured','product_image.image','product_image.caption')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
               ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
                ->leftJoin('brand','brand.id','=','products.brand')
               ->leftJoin('product_image', function($join)
              {
                $join->on('product_image.product_id', '=', 'products.id');

              })
              ->where('products.status',1)
              ->where('products.id',decrypt($product_id))
              ->orderBy('products.id','DESC');

                $details=$query->first();
                //echo $details['mrp']; die();  
                $orderItem =  new orderItem();
                $orderItem->product_id = $details['id'];
                $orderItem->price = $details['mrp'];
                $orderItem->product_name = $details['name'];
                $orderItem->quantity_order = 1;
                $orderItem->order_id = $order_id;
                $orderItem->save();

                $user = User::all();
                Notification::send($user,new OrderPlace('New order placed'));
                
                $json['success'] = 'Your order successfully placed...';
                $json['status']  = 'success';
                $json['redirect_url']  = route('home');
                $json['url']     = route('home');
                echo json_encode($json);      
        /*} catch (\Exception $e) {
            Session::flash('error',$e->getMessage());
        }  */ 

    }

    public function UserSubscriptionOrder($total_amount,$plan_id){
       $save = new UserSubscriptionOrder();
      $save->user_id = Auth::guard('web')->user()->id;  
      $save->total_amount = $total_amount;  
      $save->paln_id     = $plan_id;
      $save->save(); 
      User::where('id',Auth::guard('web')->user()->id)->update(['is_subscription'=>1]); 
    }


     public function order_cancel(Request $request){
        //echo 'sadfasd'; die();
        $order_id = decrypt($request->order_id);
      //echo $order_id; die();
        try {
             $status = 'C';
            $data = $this->order->where('id',$order_id)->where('user_id',Auth::guard('web')->user()->id)->update(['order_status'=>'C']);

           if($data>0){ 
            $res['status'] ='success';
             $res['message'] = "Order Cancelled Sucsessfully";
        }else{
            $res['status'] ='error';
             $res['message'] = "Order Not Cancelled";
        }
            return response()->json($res);
        } catch (\Exception $e) {
             return $this->clientErrorResponse($e);
        }
    }  

}
