<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\RegistrationSuccessful;
use Illuminate\Support\Str;
use App\Models\User;
use Session;
use Validator;
use Response;
use Auth;
use Redirect;
use Hash;
 class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if(Auth::check()) {
        return Redirect('home');
       }
      return view('frontend.login-register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function sendOtp(Request $request)
    {
       $validator = Validator::make($request->all(), [
                        'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                    ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{

        $ckeckephoneexist = User::select('mobile_number')->where('role','!=','admin')->where('mobile_number',$request->get('mobile_number'))->first();
            if($ckeckephoneexist){
                if($request->get('type')=='otp'){
                    Session::put('otp', 'otp');
                }else{
                    Session::put('password', 'password');
                }
               //  echo "mobile_number exit in database"; die();
                $otp   = '123456';
                $mobile_number   = $request->get('mobile_number');
                $otptime   = '1:01';
                Session::put('otp_form_show', 'otp_form_show');
                Session::put('mobile_number', $mobile_number);
                $response['status'] = "success";
                $response['otp_form_show'] = true;
               if($request->get('type')=='otp'){
                $response['message'] = "Otp send successfully.";
                }else{
                $response['status'] = "error";
                 $response['message'] = "Please enter'your password";
                }

                return json_encode($response);

            }else{
                $otp   = '123456';
                $otptime   = '1:01';
                $phone = $request->get('mobile_number');
                $current_date_time = date('Y-m-d H:i:s');
                Session::put('otptime', $otptime);
                Session::put('otp_number', $otp);
                Session::put('current_date_time', $current_date_time);
                Session::put('register_form_show', 'register_form_show');
                Session::put('phone', $phone);
                $response['status'] = "success";
                $response['register_form'] = true;
                $response['message'] = "Otp send successfully.";
                return json_encode($response);
            }
        }

    }

    public function login(Request $request){
      //     echo "<pre>"; print_r($request->all()); die;
      if($request->get('logintype')!='password'){
        $opt_num = '123456';
       // echo $opt_num; die();
         User::where('mobile_number',$request->get('mobileno'))->update(['otp' => $opt_num]);
            // User::where('mobile_number',$request->get('mobileno')->update($request->only(['otp', $opt_num])).
            //User::where('mobile_number', 2)->update(['customer_id' => 1,'answer'=>2]);
        $enterotp = $request->get('otp1').$request->get('otp2').$request->get('otp3').$request->get('otp4').$request->get('otp5').$request->get('otp6');
        if ($request->get('otp1')=='') {
            $response['status'] = "error";
            $response['message'] = "field is required.";
            return json_encode($response);
        }else{
            $userdata = array(
             'mobile_number'     => $request->get('mobileno'),
            );
            //echo $request->get('mobileno').'==='.$enterotp; die();
            $user = User::where('mobile_number',$request->get('mobileno'))->where('otp',$enterotp)->first();
           // return "<pre>"; print_r($user); die();
            //echo $user->role; die();
            /*if($user->role=='user'){
                $guard = 'web';
                $url = route('home');
            }else{
                $guard = 'vendor';
                $url = url('vendor/my-dashboard');
            }*/

            if ($user->role=='user') {
                $guard = 'web';
                $url = route('home'); 
            }elseif($user->role=='vendor' && $user->status==1){
                $guard = 'vendor';
                $url = url('vendor/orders');
            }else{
                $guard = 'doctor';
                $url = url('doctor/orders');
            }
         
            if (Auth::guard($guard)->loginUsingId($user->id)) {
                if(Auth::guard($guard)->user()->status != '0'){
                    Auth::guard($guard)->user()->save();
                    $response['status'] = "success";
                    $response['message'] = "Login successfully.";
                    $response['url']     = $url;
                    return json_encode($response);
                 }else{
                    $response['status'] = "error";
                    $response['message'] = "Your account is not enabled. please contact your administrator to access away.";
                    return json_encode($response);
                }
            } else {
                $response['status'] = "error";
                $response['message'] = "OTP does not match.";
                return json_encode($response);
            }



        }

      }else{
            $validator = Validator::make($request->all(), [
                'password' => 'required',
            ]);
      }
      if ($validator->fails()){
               return response()->json($validator->messages(), 422);
      }else{
        if($request->get('logintype')=='password'){
                $userdata = array(
                 'mobile_number'     => $request->get('mobileno'),
                 'password'  => $request->get('password')
                );
        }else{
            $userdata = array(
             'mobile_number'     => $request->get('mobileno'),
             'otp'  => $enterotp
            );
        }
                $pass = Hash::make($request->get('password'));
            //echo $pass; die();
            $user = User::where('mobile_number',$request->get('mobileno'))->first();
           // echo $user->role; die();    
         if ($user->role=='user') {
                $guard = 'web';
                $url = route('home'); 
            }elseif($user->role=='vendor'){
                $guard = 'vendor';
                $url = url('vendor/my-dashboard');
            }else{
                $guard = 'doctor';
                $url = url('doctor/my-dashboard');
            }
        if (Auth::guard( $guard)->attempt($userdata)) {
            if(Auth::guard( $guard)->user()->status != '0'){
                Auth::guard( $guard)->user()->save();
                $response['status'] = "success";
                $response['message'] = "Login successfully.";
                $response['url']     = $url;
                return json_encode($response);
             }else{
                $response['status'] = "error";
                $response['message'] = "Your account is not enabled. please contact your administrator to access away.";
                return json_encode($response);
            }
        } else {
            $response['status'] = "error";
            $response['message'] = "These credentials do not match our records.";
            return json_encode($response);
        }

      }

      return json_encode($response);
      //echo "<pre>"; print_r($request->all()); die;
      //if($request->get('logintype')){
      //}
        //$otp = $request->get('otp1').$request->get('otp2').$request->get('otp3').$request->get('otp4').$request->get('otp5').$request->get('otp6');
        //echo $otp; die;
    }

    public function doRegister(Request $request)
    {
     //   echo "asdfsa"; die();
         $validator = Validator::make($request->all(), [
/*            'email' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
*/            'password' => 'required_with:password|same:password|min:6',
/*            'password_confirmation' => 'required_with:password|same:password|min:6'
*/        ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{
            $otp        = Session::get('otp_number');
            $phone      = Session::get('phone');
            $expiretime = Session::get('current_date_time');
            $enterotp   = 123456;
            $to_time    = strtotime($expiretime);
            $from_time  = strtotime(date('Y-m-d H:i:s'));
            $differenceInSeconds = $from_time - $to_time;
              //echo; die();
          //  if($differenceInSeconds <= 60){
                if($enterotp == $otp){
                    $save                      = new User();
                    $save->email               = $request->get('email');
                    $save->name                = $request->get('first_name');
                    $save->mobile_number       = $phone;
                    $save->otp                 = $otp;
                    $save->first_name          = $request->get('first_name');
                    $save->last_name           = $request->get('last_name');
                    $save->referrer_code       =Str::random(8);
                    $save->password            = 'null';
                    $data = $save->save();
                   if($data >0) {
                $userdata = array(
                     'mobile_number'     =>  $phone,
                     'otp'  => 123456,
                    );
            $user = User::where('mobile_number',$save->mobile_number)->where('otp',$save->otp)->first();
            if (Auth::guard('web')->loginUsingId($user->id)) {

                                 if(Auth::guard('web')->user()->status != '0'){
                                    Auth::guard('web')->user()->save();
                                    $response['status'] = "success";
                                    $response['message'] = "Login successfully.";
                                    $response['url']     = route('home');
                                    return json_encode($response);
                                 }else{
                                    $response['status'] = "error";
                                    $response['message'] = "Your account is not enabled. please contact your administrator to access away.";
                                    return json_encode($response);
                                }
                            } else {
                                

                                $response['status'] = "error";
                                $response['message'] = "These credentials do not match our records.";
                                return json_encode($response);
                            }

                    Session::forget('register_form_show');
                    Session::forget('otp_form_show');
                    Session::forget('mobile_number');
                    Session::forget('password');
                    Session::forget('otp');
                    $response['status'] = "success";
                    $response['message'] = "User has been registered successfully.";
                    // New code to send the notification upon registration
                    return notify(new RegistrationSuccessful());
                    


                   }
                }else{
                    $response['status'] = "error";
                    $response['message'] = "OTP does not match.";
                }

       /* }else{
            $response['status'] = "error";
            $response['message'] = "OTP has been expired.";
        }*/
        return json_encode($response);

            return json_encode($response);

        }
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storessssa(Request $request)
    {
      //  echo "<pre>"; print_r($request->all()); die();
        $mobile_number = $request->mobile_number;
        if($mobile_number=='') {
            //echo "ok s"; die();
            $json['mobile_number'] = 'Please enter your mobile number.';
            $formStatus = false;
        }else{
        $ckeckephoneexist = User::select('mobile_number')->where('role','!=','admin')->where('mobile_number',$request->get('mobile_number'))->first();
        //echo $ckeckephoneexist; die();
        if ($ckeckephoneexist) {
            $json['mobile_number_exit'] = 'mobile_number_exit';
            $formStatus = false;
        }else{
            $current_date_time = date('Y-m-d H:i:s');
            $otp = '123456';
            Session::put('otp_number', $otp);
            Session::put('current_date_time', $current_date_time);

/*              $save                      = new User();
                $save->mobile_number       = $request->get('mobile_number');
                $save->otp       = '123456';
                $save->save();*/

                $json['mobile_number_save'] = 'Please fill this from';
                $formStatus = false;
        }
 }

        echo json_encode($json);
    }


    public function sign_up(Request $request){
       return($request->all());
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_sessions()
    {
        Session::forget('register_form_show');
        Session::forget('otp_form_show');
        Session::forget('mobile_number');
        Session::forget('password');
        Session::forget('otp');

        $json['success'] = 'forget session';
        $json['redirect_url'] = route('account.login');
        $formStatus = true;

       echo json_encode($json);
      //echo "account.session-forgot.post"; die();
    }

    //Logout user  ----------------------
    public function logout(Request $request) {
     Auth::guard('web')->logout();
      return redirect('home');
    }
    //Logout user  ----------------------
    public function logout_vendor(Request $request) {
     Auth::guard('vendor')->logout();
    return redirect('home');
    }
       //Logout user  ----------------------
    public function logoutdoctor(Request $request) {
       Auth::guard('doctor')->logout();
      return redirect('home');
    }
}
