<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\User;
use App\Models\Tag;
use App\Models\ImageProduct;
use App\Models\Manufacturer;
use App\Models\ProductTag;
use App\Models\ProductCategory;
use App\Models\DeliveryAddress;
use App\Models\PatientInfo;
use App\Models\OrderItem;
use App\Traits\ImageTrait;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\ProductImage;
use App\Models\ProductFaq;
use App\Models\Product;
use App\Models\Wishlist;
use Auth;
use DB;
use View;
class ProductController extends Controller
{
    public function product_list(Request $request,$cat_slug=null,$sub_cate_slug=null){
        


      $allCategories =  Category::select('category_name','id','slug')->limit(10)->orderBy('id','ASC')
      ->get();
      
     foreach($allCategories as $key=>$category) {
            $allCategories[$key]['sub_categorys'] = Category::where('parent_id',$category->id)->get();
     }
      $search_cate = is_null($sub_cate_slug) ? $cat_slug : $sub_cate_slug;
      $categoryids = Category::select('id','parent_id','slug')->where('slug',$search_cate)->first();
      $manufacturer = Manufacturer::select('manufacturer_name','id','slug')->get();

       


   $query = Product::select('products.*','product_image.image')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
          ->Join('product_image', function($join)
        {
          $join->on('product_image.product_id', '=', 'products.p_id');

        });

        if (isset($categoryids->parent_id) && ($categoryids->parent_id != '0')) {
         $query->where('product_category.category_id',$categoryids->parent_id ?? "" );
         $query->where('product_category.category_id',$categoryids->id ?? "" );
        }else{
          $query->where('product_category.category_id',$categoryids->id ?? "" );
        }
        
          $productDetail=$query->groupBy('product_image.pro_img_id')->paginate(15);

        if($cat_slug == "all"){
         $query = Product::select('products.*','product_image.image')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
           ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
            ->Join('product_image', function($join){
              $join->on('product_image.product_id', '=', 'products.p_id');
           });
          $productDetail=$query->groupBy('product_image.pro_img_id')->paginate(15);
        }

     $out_of_stock = Product::where('in_stock',0)->count();

     return view('frontend.product-list',compact('allCategories','productDetail','manufacturer','out_of_stock'));
    }

    public function product_ajax(Request $request){
      $sort = $request->get('sort');
      $manufacturer_id = $request->get('manufacturer');
        $query = Product::select('products.*','product_image.image')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->Join('product_image', function($join)
        {
          $join->on('product_image.product_id', '=', 'products.p_id');
         })
        ->where('products.status',1);
        
        if($manufacturer_id>0){
          $query->whereIn('products.manufacturer_id',$manufacturer_id);
        }

        if($sort=="rel")
        {
          $query->orderBy('products.mrp','ASC');
        }

        //Average rating     
        if($sort==1){
            $query->orderBy('products.id','DESC');
        }

        //low to hight
        if($sort==2){
           if(!empty('products.sale_price')){
            $query->orderBy('products.sale_price','ASC');
           }
           else{
            $query->orderBy('products.mrp','ASC');
           }
        }

        //hight to low
        if($sort==3){
          if(!empty('products.sale_price')){
            $query->orderBy('products.sale_price','DESC');
          }else{
            $query->orderBy('products.mrp','DESC');
          }
        }

        //Discount
        if($sort==4){
            
            $query->where('products.discount','!=','')->orderBy('products.discount','DESC');
        }
         
          $productDetail=$query->groupBy('product_image.pro_img_id')->paginate(15);

        //echo "<pre>";print_r($productDetail);die;

      //return view('frontend.product-ajax',compact('productDetail'));
        $view = View::make('frontend.product-ajax',compact('productDetail'))->render();

      return json_encode(array('content'=>$view));
    }

    public function brand_search(Request $request){
      //echo "<pre>"; print_r($request->all()); die(); 
      $search = $request->brand_name;
      $brand = Brand::select('brand_name','id','slug')->where('brand_name','LIKE',"%{$search}%")->get();
      $view = View::make('frontend.brand-ajax',compact('brand'))->render();
      return json_encode(array('content'=>$view));

    }

    public function manufacturers_search(Request $request){
      $search = $request->manufacturers_name;
      $manufacturer = Manufacturer::select('manufacturer_name','id','slug')->where('manufacturer_name','LIKE',"%{$search}%")->get();
      $view = View::make('frontend.manufacturer-ajax',compact('manufacturer'))->render();
      return json_encode(array('content'=>$view));
    }


    public function product_details(Request $request,$pro_slug=null){

        $query = Product::select('products.*','product_image.image','categories.category_name','categories.id as category_id','manufacturer.manufacturer_name')
            ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
              ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
              ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
              ->Join('product_image', function($join)
             {
                $join->on('product_image.product_id', '=', 'products.p_id');
             })
            ->where('products.slug',$pro_slug)  
            ->groupBy('product_image.pro_img_id');
          $productDetail=$query->first(); 
         // echo "<pre>"; print_r($productDetail); die();

          $query = Product::select('products.*','product_image.image','categories.category_name','manufacturer.manufacturer_name','product_image.pro_img_id')
            ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
              ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
              ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
              ->Join('product_image', function($join)
             {
                $join->on('product_image.product_id', '=', 'products.p_id');
             })
            ->where('products.slug','!=',$productDetail->slug)
            ->where('product_category.category_id',$productDetail->category_id)
            ->groupBy('product_image.pro_img_id')
            ->limit(5);

          $related_products=$query->get();
        // echo "<pre>"; print_r($related_products); die();


        /* $query = Product::select('products.*','product_image.image')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('brand','brand.id','=','products.brand')
         ->leftJoin('product_image', function($join)
        {
          $join->on('product_image.prod_id', '=', 'products.id');

        })
        ->where('products.status',1)
        ->where('categories.id',$productDetail->id)
        ->where('products.slug','!=',$productDetail->slug)
        ->where('product_category.category_id',$productDetail->id)
        ->limit(5)
        ->groupBy('product_image.prod_id');
        $related_products=$query->get();*/


       //$product_images_data =Product::with('productData')->get(); 
       $product_details =Product::where('slug',$pro_slug)->first();

       $product_images_data = ProductImage::where('product_id',$product_details->p_id)->get();
      // echo "<pre>"; print_r($product_images_data);die;
       $product_image_set_primary = ProductImage::where('product_id',$product_details->p_id)->whereNull('deleted_at')->first();

       $faqdataproduct = ProductFaq::where('product_id',$productDetail->id)->get();
 
        return view('frontend.product-details',compact('productDetail','related_products','product_images_data','product_image_set_primary','faqdataproduct'));
    }

    /*===Add To wish list===*/
     public function add_wishlistsss(Request $request){
      $user_id = Auth::guard('web')->user()->id;
      $product_id = $request->get('product_id');
      $count = Wishlist::where('product_id',$product_id)->count();
       if($count>0){
        $wishlist = new Wishlist();
        $wishlist->user_id = $user_id;
        $wishlist->product_id = $product_id;
        $save = $wishlist->save();
        if($save){
          $response['status'] = "error";
          $response['message'] = "Add to wishlist";
        }else{
          $response['status'] = "error";
          $response['message'] = "Somthing went wrong.";
        }
       }else{
       // echo "die"; die();
       }
          return json_encode($response);
    }



    /* ==== Save for later */
    public  function add_wishlist(Request $request){
     /// dd($request->all());
        $count=Wishlist::where('user_id',Auth::guard('web')->user()->id)->where('product_id',$request->get('product_id'))->count();
        if($count>0){
                $SaveForLater = Wishlist::where('user_id',Auth::guard('web')->user()->id)->where('product_id',$request->get('product_id'));
                $SaveForLater->forceDelete();
                return(json_encode(array(
                'success' => false,
                'message' => "Remove to wish list successfully.",
                'status' => "success"
                
                ))); 
            }
            else
            {
                $save = new Wishlist();
                $save->user_id = Auth::guard('web')->user()->id;
                $save->product_id = $request->get('product_id');
                $status = $save->save();
                return(json_encode(
                array(
                'success' => true,
                'message' => "Added to wish list successfully.",
                'status' => "success"
                
                ))); 
            } 
    }

    /*Product added extra by doctor*/

    public function NewProductOrderAddByDoctor(Request $request,$product_id=null,$user_id=null,$order_id=null){
        $order_id = $request->order_id; 
        $user = User::where('id',decrypt($user_id))->first();
        $guard = 'web';
        
        Auth::guard('web')->login($user);
        
        $user_id = Auth::guard('web')->user()->id;
        
        $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
        $delivery_address = DeliveryAddress::where('status',1)->where('user_id',$user_id)->first();
        $addresses = DeliveryAddress::where('user_id',Auth::guard('web')->user()->id)->get();
        $patient = PatientInfo::where('user_id',Auth::guard('web')->user()->id)->get();
        $patient_address = PatientInfo::where('status',1)->where('user_id',$user_id)->first();
        
        $query = Product::select('brand.brand_name','products.id','products.medicine_name','products.slug','products.mrp','products.sale_price','products.brand','products.prescription','products.type_of_sell','products.salt','products.is_featured','product_image.image','product_image.caption')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
          ->leftJoin('brand','brand.id','=','products.brand')
         ->leftJoin('product_image', function($join)
        {
          $join->on('product_image.product_id', '=', 'products.p_id');
        })
        ->where('products.status',1)
        ->where('products.id',decrypt($product_id))
        ->orderBy('products.id','DESC');

        $productDetail=$query->first();

        return view('frontend.new-product-add-by-doctor',compact('productDetail','site_setting','delivery_address','patient','addresses','patient_address','order_id'));
    }
    public function extra_product_payment(Request $request,$order_id=null){
        $id = decrypt($order_id);
        $productDetail = OrderItem::where('order_id',$id)->where('added_by','doctor')->get();
        //$total_amt = $productDetail->mrp;
        $total_amt= 0;
        foreach($productDetail as $key=>$value){
           $total_amt += $value->ProductOrderItem->mrp;
        }    
        //echo $total_amt; die();
        $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
        return view('frontend.extra-product-payment',compact('productDetail','site_setting','order_id','total_amt'));
        
    }


    





}
