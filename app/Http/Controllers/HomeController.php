<?php

namespace App\Http\Controllers;

use App\Models\Home;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\User;
use App\Models\Tag;
use App\Models\ImageProduct;
use App\Models\ProductTag;
use App\Models\ProductCategory;
use App\Models\Newsletter;
use App\Traits\ImageTrait;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Testimonials;
use App\Models\Blog;
use App\Models\Page;
use App\Models\Banner;
use App\Models\OfferBanner;
use App\Models\Frontcontain;
use App\Models\FaqTopics;
use App\Helpers\Helper;
use App\Helpers\ProductHelper;
use Response;
use Cache;
use Validator;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function send_notification_olds(Request $request)
    {
        $firebaseToken = User::where('id',377)->whereNotNull('device_token')->pluck('device_token')->all();
         // return $firebaseToken;  
        $SERVER_API_KEY = 'AAAAAZepSsQ:APA91bFMrtr6PytTfJX1egtqqlML4lb49HUf9XU6UX6hI5YCiS3Dh9ufsinM6yr6gdldiWhmEaqfSojQtifYHoS9h6S2S12n1_AQ9FX-05WBt-8-QB93QNaMUufliYgwW9_e-dP6YkXG';
        //return $SERVER_API_KEY;
        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => 'amit giri',
                "body" => 'okokok',  
            ]
        ];
        $dataString = json_encode($data);
      
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
      
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                 
        $result = curl_exec($ch);

        echo "hello"; echo '<pre>';print_r($result);
            if ( $result === FALSE ) {
                die( 'FCM Send Error:' . curl_error ( $ch ) );
            }
            curl_close ( $ch );
            return true;
    
        return back()->with('success', 'Notification send successfully.');
    }


    public function send_notification(Request $request){
             //echo "okkkkkkkkkkkkkkkkkkk"; die();
            /*send notification to customer*/
            $user_id_array1 = User::where('id',377)->select('id','device_type','device_token','name')->get();
            $userData = User::where('id',377)->select('device_token')->get();
            $user_id_array = collect($userData)->pluck('device_token');
            $dataArray = [];
            $dataArray['type'] = 'Order';
            $dataArray['product_type'] = 'New';
            $dataArray['title'] = 'New Order';
            $dataArray['body'] = trans('order.order_confirmed');
            $device_type = $user_id_array1[0]->device_type;
            Helper::sendNotification($user_id_array ,$dataArray, $device_type);
    }   


    public function index()
    {


      $testimonials = Testimonials::where('status','1')->orderBy('id','DESC')->get();
      $blog = Blog::where('status','1')->orderBy('id','DESC')->get();
      $banner = Banner::where('status','!=','0')->orderBy('id','DESC')->get();
      $offer_banner = OfferBanner::orderBy('id','ASC')->where('status','1')->get();
      
      return view('frontend.index',compact('testimonials','blog','banner','offer_banner'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function show(Home $home)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function edit(Home $home)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Home $home)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Home  $home
     * @return \Illuminate\Http\Response
     */
    public function destroy(Home $home)
    {
        //
    }


    public function newsletter(Request $request){

        $validator = Validator::make($request->all(), [
                        'email' => 'required|email|unique:newsletter,email',
                    ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{
      $input['email'] = $request->get('email');


      $email= new Newsletter();
      $email->email=$request->get('email');
      $newsemail= $input['email'];

      if($email->save())
       {
        $response['status'] = "success";
        $response['message'] = "Thanks for subscribing to our newsletter";
         return json_encode($response);

       }else{
        $response['status'] = "error";
        $response['message'] = "There was an issue updateing the general setting. Please try again.";
         return json_encode($response);

       }
      }

     }

  /*Search*/
 public function autoCompleteAjax(Request $request)
    {
        $search=  $request->term;
/*        $posts = Product::with('ProductImage','ManufacturerName')
                 ->where('medicine_name','LIKE',"{$search}%")
                 ->where('status',1)
                ->orderBy('created_at','DESC')->limit(5)->get();
*/
$query = Product::select('products.*','product_image.image','categories.category_name','manufacturer.manufacturer_name')
         ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->leftJoin('product_image', function($join)
        {
                  $join->on('product_image.product_id', '=', 'products.p_id');

        })
        ->where('products.medicine_name','LIKE',"{$search}%")
        ->groupBy('product_image.product_id');
       $posts  = $query->limit(5)->get();
       


//     echo "<pre>";print_r($posts);die;               
    //  echo $posts->ProductImage->image; die();
        if(!$posts->isEmpty())
        {
            $new_row=[];    
            foreach($posts as $post)
            {
                $new_row['product_id']= encrypt($post->id);
                $new_row['title']= $post->medicine_name;
                $new_row['sale_price']= $post->sale_price??'';
                $new_row['mrp']= $post->mrp??'';
                $new_row['discount']= $post->discount??'';
                $new_row['manufacturer_name']= $post->manufacturer_name ?? '';
                $new_row['image']=$post->image;
                $new_row['url']= url('product-details/'.$post->slug);
                $row_set[] = $new_row; //build an array
            }
        }
        echo json_encode($row_set); 
    }
    //Page Managemnet ===================
    public function staticpages($slug=NULL)
    {
        cache::flush();
        $pageTitle = $slug;
        $getPageData =  Page::select('title','description','slug')
         ->where('slug',$slug)->where('status',1)
         ->first();     
    
    //    echo "<pre>"; print_r($getPageData); die('d');
        return view('frontend.staticpage',compact('pageTitle','getPageData'));
        
    }
    
    /*=====================*/

     public function faq(Request $request){
        $faq_topics =FaqTopics::with('faqData')->get(); 
        return view('frontend.faq',compact('faq_topics'));  
    }

     public function blog_details(Request $request,$slug=null){
        $blog_details = Blog::where('slug',$slug)->first();
       return view('frontend.blog-details',compact('blog_details'));  
    }


    public function offer_just_list(Request $request){
      $offer_banner = OfferBanner::orderBy('id','ASC')->where('status','1')->get();
     return view('frontend.offer-just-for-list',compact('offer_banner'));  
    } 

    public function offer_just_details(Request $request,$slug=NULL){
      $offer_banner = OfferBanner::orderBy('id','ASC')->where('slug',$slug)->where('status','1')->first();
     return view('frontend.offer-just-for-you-details',compact('offer_banner'));  
    }

     public function legal_information(){
        $legal_information_data = Page::where('slug','terms-and-conditions')->where('status','1')->first();
       return view('frontend.legal-information',compact('legal_information_data'));  
    }


    



    
}
