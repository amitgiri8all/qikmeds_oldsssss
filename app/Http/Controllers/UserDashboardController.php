<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Models\User;
use App\Models\Page;
use App\Models\SaveForLater;
use App\Models\Product;
use App\Models\Order;
use App\Models\Contact;
use App\Models\OrderPayment;
use App\Models\SubscriptionMeta;
use App\Models\SubscriptionFaq;
use App\Models\PatientInfo;
use App\Models\ContactPurpose;
use App\Models\SubscriptionPlan;
use App\Models\UserPrescription;
use App\Models\OrderItem;
use App\Models\ReferEarn;
use App\Models\PaymentMethod;
use App\Models\AdminNotification;
use App\Models\Notification;
use App\Models\Wishlist;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use App\Traits\ImageTrait;
use Validator;
use Response;
use Session;
use DB;
use URL;
use Auth;
use Crypt;
use Hash;
class UserDashboardController extends Controller
{
    use ImageTrait;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('frontend.user.index');
    }

    public function update_profile_user(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all() , ['name' => 'required', 'mobile_number' => 'required|string|digits:10', 'email' => 'required',]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
             $data = request()->except(['_token']);
             $user_id = Auth::guard('web')->user()->id; 
             $user_data = DB::table('users')->select('*')->where('id',$user_id)->first();
             $doc_image = $user_data->image;  
              if($request->hasFile('image')){
                $folder_name ='user';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }else{
                $data['image'] = $doc_image;
              }
             //  image
             $status = User::where('id',$user_id)->update($data);

                if ($status)
                {
                    $response['status'] = "success";
                    $response['message'] = "User update Successfully.";
                    return Response::json($response);
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Something went wrong";
                    return Response::json($response);
                }
        }

    }

    public function addresses(Request $request){
       // echo ; die();
       $addresses = DeliveryAddress::where('user_id',Auth::guard('web')->user()->id)->get(); 
      // return $addresses;
       return view('frontend.user.addresses',compact('addresses'));
    }

    /*** Add new delivery Addresss ***/
    public function add_delivery_address(Request $request)
    {
        return view('frontend.user.add-new-address');
    }
    // DeliveryAddress
    public function save_delivery_address(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all() , ['name' => 'required', 'mobile_number' => 'required|string|digits:10', 'pin_code' => 'required', 'locality' => 'required', 'address_delivery' => 'required',]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
             $user_id = Auth::guard('web')->user()->id;  
             $data['user_id'] = $user_id;    
              // echo "<pre>"; print_r($data); die();
             $status = DeliveryAddress::create($data);
                if ($status)
                {
                    $response['status'] = "success";
                    $response['message'] = "Delivery Address  added Successfully.";
                    $response['url'] = url('user/addresses');
                    return Response::json($response);
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Something went wrong.";
                    return Response::json($response);
                }
        }

    }

    /*Selected Address*/
    public function selecte_address(Request $request){
     $user_id = Auth::guard('web')->user()->id;  
     $selected_id = $request->selected_id;  
        DeliveryAddress::where('user_id',$user_id)->update(['status'=>0]);
        DeliveryAddress::where('id',$selected_id)->update(['status'=>1]);

        $update_location =DeliveryAddress::where('user_id',$user_id)->where('status',1)->first();
        $lat = $update_location->latitute;
        $long = $update_location->longitute; 
        User::where('id',$user_id)->update(['latitute'=>$lat,'longitute'=>$long]);
        
        $response['status'] = "success";
        $response['message'] = "Delivery Address  update Successfully.";
        return Response::json($response);
     } 


    /*User Ordered List*/
    public function order_list(Request $request){
      $orders = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('user_id',Auth::guard('web')->user()->id)
      ->orderBy('id','DESC')
      ->get();
       return view('frontend.user.orders',compact('orders'));
    }

    public function order_details(Request $request,$id=null){
      //  echo $id; die('id');
     $id = decrypt($id);
    // echo $id; die();
     $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$id)->first(); 

     $removeproduct = OrderItem::where('order_id',$id)->onlyTrashed()->get();

     $newproductadded = OrderItem::where('order_id',$id)->where('added_by','doctor')->get();
    // echo "<pre>"; print_r($order); die('========');
    // echo $newproductadded->product_id; die();

     return view('frontend.user.order-detail',compact('order','removeproduct','newproductadded'));
    }  


     public function patient_manage(){

        return view('frontend.user.patient-manage');
     }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment_method()
    {
        $user_id = Auth::guard('web')->user()->id;  
        $payment_methods =PaymentMethod::all()->where('user_id',$user_id);
        return view('frontend.user.payment-method',compact('payment_methods'));
    }
    public function payment_method_save(Request $request)
    {
        try {       
           $data = $request->all();
             if($request->hasFile('image')){
                $folder_name ='card';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }
            $validator = Validator::make($request->all() ,[
                'bank_name' => 'required',
                'card_number' => ['required', 'unique:payment_method,card_number', new CardNumber],
                'expiration_year' => ['required', new CardExpirationYear($request->get('expiration_month'))],
                'expiration_month' => ['required', new CardExpirationMonth($request->get('expiration_year'))],
                'cvc' => ['required', new CardCvc($request->get('card_number'))]
            ]);
            if ($validator->fails()){
                return response()->json($validator->messages() , 422);
            }else{
                  $user_id = Auth::guard('web')->user()->id;  
                  $data['user_id'] = $user_id;    
                  $status = PaymentMethod::create($data);
                  if ($status){
                        $response['status'] = "success";
                        $response['url'] = route('user.payment-method');
                        $response['message'] = "Successfully added New Card.";
                        return Response::json($response);
                      // return redirect()->route('user.payment-method');
                      //return view('user.payment-method');

                  }else{
                        $response['status'] = "error";
                        $response['message'] = "Something went wrong ";
                        return Response::json($response);
                  }
            }
        } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }
    }


    public function payment_edit(Request $request, $id){
        
        $validator = Validator::make($request->all() , [

                'bank_name' => 'required',
                'card_number' => ['required', 'unique:payment_method,card_number', new CardNumber],
                'expiration_year' => ['required', new CardExpirationYear($request->get('expiration_month'))],
                'expiration_month' => ['required', new CardExpirationMonth($request->get('expiration_year'))],
                'cvc' => ['required', new CardCvc($request->get('card_number'))]
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        { 
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $payment_data = $request->all();

       if($request->hasFile('image')){
                $folder_name ='card';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }

        $data['bank_name'] = $request->bank_name;
        $data['card_number'] = $request->card_number;
        $data['expiration_year'] = $request->expiration_year;
        $data['expiration_month'] = $request->expiration_month; 
        $data['cvc'] = $request->cvc;
        $data['cardholder_name'] = $request->cardholder_name;
   
        $data =PaymentMethod::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('user.payment-method');
                    $response['message'] = "Payment card has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully Updated Payment Card');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/

      }
       

    }

    //Save For Later =============================//
    public function save_for_later()
    {
        try {        
        $product = SaveForLater::where(['user_id'=>Auth::guard('web')->user()->id])
         ->with(['Product','ProductImage'])
/*         ->with(['Manufacturer.Mfr'])
*/         ->get();
     //  echo "<pre>"; print_r($product); die();
        } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }
      return view('frontend.user.save-later',compact('product'));
    }

    public function save_for_later_remove(Request $request){
/*          try {   
*/          $status =SaveForLater::where('id',$request->id)->delete();  
            if ($status){
            $response['status'] = "success";
            $response['message'] = "Product successfully remove";
            return Response::json($response);

            }else{
            $response['status'] = "error";
            $response['message'] = "Something went wrong ";
            return Response::json($response);
            }

         /* } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }*/
    }


      public function refer_earn(Request $request){
      $data = ReferEarn::where('id','1')->where('status','1')->first();  
      return view('frontend.user.refer-earn',compact('data'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data = DeliveryAddress::where('id',$id)->first();
       //echo "<pre>"; print_r($data); die();
       return view('frontend.user.edit-new-address',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //echo "<pre>"; print_r($request->all()); die();
         $validator = Validator::make($request->all() , [
            /*'first_name' => 'required','last_name'=>'required', 'email' => 'required|email', 'address_name' => 'required','city' => 'required', 'state' => 'required', 'country' => 'required','mobile_number' => 'required', 'pin_code' => 'required','image'=>'required',*/
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        { 
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $address = $request->all();

        $data['name'] = $request->name;
        $data['mobile_number'] = $request->mobile_number;
        $data['pin_code'] = $request->pin_code;
        $data['locality'] = $request->locality; 
        $data['address_delivery'] = $request->address_delivery;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['landmark'] =$request->landmark;
        $data['alternate_phone'] = $request->alternate_phone;
        $data['address_type'] = $request->address_type;
        $data['latitute'] = $request->latitute;
        $data['longitute'] = $request->longitute;

        $data =DeliveryAddress::findOrFail($id)->update($data);


    
       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('user.addresses');
                    $response['message'] = "Address has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully Update Address');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/

      }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }

    // DeliveryAddress
    public function save_patient_info(Request $request)
    {

        $data = $request->all();
        //return $data;
        $validator = Validator::make($request->all() , ['patient_name' => 'required', 'mobile_number' => 'required|string|digits:10|unique:patient_Info,mobile_number', 'sex' => 'required', 'age' => 'required','save_patient_info'=>'requirede']);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
             $user_id = Auth::guard('web')->user()->id;  
             $data['user_id'] = $user_id;    
               //  echo "<pre>"; print_r($data); die();
             $status = PatientInfo::create($data);
                if ($status)
                {
                    $response['status'] = "success";
                    $response['message'] = "Patient Info create added Successfully.";
                    $response['url'] = url()->previous();
                    return Response::json($response);
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Something went wrong.";
                    return Response::json($response);
                }
        }

    }
    /*Selected Address*/
    public function selecte_patient(Request $request){
     $user_id = Auth::guard('web')->user()->id;  
     $selected_id = $request->selected_id;  
  //   echo $selected_id; die();
        PatientInfo::where('user_id',$user_id)->update(['status'=>0]);
        PatientInfo::where('id',$selected_id)->update(['status'=>1]);
        $response['status'] = "success";
        $response['message'] = "Patient Info update Successfully.";
        return Response::json($response);
     }

     /*Contact Us*/

     public function contact_us(){
      $contactpurpose = ContactPurpose::where('status',1)->get();   
      return view('frontend.user.contact-us',compact('contactpurpose'));
     }


     public function save_contact_us(Request $request)
    {

        $data = $request->all();
        //return $data;
        $validator = Validator::make($request->all() , ['purpose_of_contact' => 'required','message' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {

           // echo $request->get('inlineRadioOptions'); die('');
         if($request->inlineRadioOptions=='email'){ 
          $data['email'] =  Auth::guard('web')->user()->email;  
         }else{
          $data['mobile'] =  Auth::guard('web')->user()->mobile_number;  
         }   

       //  return $data;
         
         $user_id = Auth::guard('web')->user()->id;  
         $data['user_id'] = $user_id;    
         $data['contact_purpose_id'] = $request->purpose_of_contact;    
         $data['message'] = $request->message;
        // echo "<pre>"; print_r($data); die();    
         $status = Contact::create($data);
            if ($status)
            {
                $response['status'] = "success";
                $response['message'] = "Contact us create added Successfully.";
                return Response::json($response);
            }
            else
            {
                $response['status'] = "error";
                $response['message'] = "Something went wrong.";
                return Response::json($response);
            }
        }

    }

    //
    public function DeleteDeliveryAddress(Request $request,$id){
       DeliveryAddress::find($id)->delete($id);
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }


    public function Delete($id){ 
       PaymentMethod::find($id)->delete($id);
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }

    public function prescription_delete($id)
    {
       if(!empty($id)){ 
       $user_prescription = UserPrescription::where('user_id',$id)->delete();
       if ($user_prescription){
            $response['status'] = "success";
            $response['message'] = "Successfully added New Card.";
            return Response::json($response);

      }else{
            $response['status'] = "error";
            $response['message'] = "Please insert image! ";
            return Response::json($response);
      }

       }


    }

    //Change password
     public function change_password(Request $request)
    {
        $input = $request->all();
        $userid = Auth::guard('web')->user()->id;
       if($request->type==1){ 
        $rules = array(
            'type' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            return response()
                ->json($validator->messages() , 422);
        } else {
            try {

                if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                    $arr = array("status" => "error", "message" => "Check your old password.", "data" => array());
                } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                    $arr = array("status" => "error", "message" => "Please enter a password which is not similar then current password.", "data" => array());
                } else {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("status" => "success", "message" => "Password updated successfully.");
                }


            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("status" => "error", "message" => $msg);
            }
        }
     }else{
        $rules = array(
            'type' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
           return response()
                ->json($validator->messages() , 422);
        } else {
            try {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("status" => "success", "message" => "Password updated successfully.");
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("status" => 'success', "message" => $msg);
            }
        } 
     }   
        return \Response::json($arr);
    }


    /*========= User Prescription ===========*/
    public function user_prescription(Request $request){
     $userprescription = UserPrescription::where('user_id',Auth::guard('web')->user()->id)->first();   
     //echo "<pre>"; print_r($userprescription); die();
     return view('frontend.user.user-prescription',compact('userprescription'));
    }


    public function upload_prescription(Request $request){
     $userprescription = UserPrescription::where('user_id',Auth::guard('web')->user()->id)->first();   
     return view('frontend.upload-prescription',compact('userprescription'));
    }

     public function upload_prescription_save(Request $request){
        //echo "okkk"; die();
        try {
            $user = Auth::guard('api')->user();
            $validator = Validator::make($request->all(), [
              'image' => 'image|mimes:jpeg,png,jpg',
            ]);
            if ($validator->fails()) {
                return response()
                ->json($validator->messages() , 422);
            }

            $count = UserPrescription::where('user_id',Auth::guard('web')->user()->id)->count();
            //echo $count; die();
            $input = $request->all();
            if ($request->hasFile('image')) {
              $folder_name = 'prescription';
              $input['image'] = $this->fileUpload($request->file('image'), false, $folder_name);
            }
            $image = $input['image'];
            if($count>0){
               UserPrescription::where('user_id',Auth::guard('web')->user()->id)->update(array('image'=>$image));
                $response['status'] = "success";
                $response['message'] = "Prescription added Successfully.";
                $response['url'] = route('user.prescription');
                return Response::json($response);
            }else{
                $save = new UserPrescription();
                $save->user_id = Auth::guard('web')->user()->id;
                $save->image = $image;
                $save->save(); 
           
                $response['status'] = "success";
                $response['message'] = "Prescription added Successfully.";
                $response['url'] = route('user.prescription');
                return Response::json($response);
              }

        }catch(\Exception $e){
         return $e;
       }    
    }


    public function subscription (Request $request){
      $subscription_faq = SubscriptionFaq::where('status','!=','0')->get();
      $subscription_meta = SubscriptionMeta::where('status','>','0')->get();
      return view('frontend.user.subscription',compact('subscription_faq','subscription_meta'));
    }

    public function subscription_plan(Request $request){
      $subscription_faq = SubscriptionFaq::where('status','!=','0')->get();
      $subscription_meta = SubscriptionMeta::where('status','!=','0')->get();
      $subscription_plan = SubscriptionPlan::where('status','!=','0')->where('plan_type','user')->get();
      return view('frontend.user.subscription-plan',compact('subscription_faq','subscription_meta','subscription_plan'));
    }


     /*Contact Us*/

     public function Wallet(){
       return view('frontend.user.wallet');
     }

       public function notification(Request $request){
       //$order_notification = Order::get();
       $user_id = Auth::guard('web')->user()->id; 
      // echo $user_id;die; 
      // $order_notification = AdminNotification::whereIn('user_ids',$user_id)
       //->get();
        $order_notification = AdminNotification::select(['id','message_heading','image', 'message_url', 'message','created_at','link_type','link_url_type','link','cat_id','sub_cat_id','vendor_product_id'])->orderBy('id','DESC')->whereRaw('FIND_IN_SET('.$user_id.', user_ids)')->get();

         $alert_notification = Notification::orderBy('id','DESC')->where('notifiable_id', $user_id)->get();

       return view('frontend.user.notification-order',compact('order_notification','alert_notification'));
    }

          public function view_all($id){ 
       
         //echo $id; die;
         $current_date = date('Y-m-d H:i:s');

        Notification::where('notifiable_id',$id)->update(['read_at'=>$current_date]);
       return    response()->json([
            'success' => 'Data Updated successfully!'
        ]);
    }

     public function legal_information(){
        $legal_information_data = Page::where('slug','terms-and-conditions')->where('status','1')->first();
        //dd($legal_information_data);
       return view('frontend.user.legal-information',compact('legal_information_data'));  
    }



     public function wishlist()
    {
        /*try {  */      
        $product_wishlist = Wishlist::where(['user_id'=>Auth::guard('web')->user()->id])
        ->with(['Product','ProductImage'])
         ->get();

        //echo "<pre>"; print_r($product_wishlist); die();
      /*  } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }*/
      return view('frontend.user.wishlist',compact('product_wishlist'));
    }

       public function wishlist_remove(Request $request){
/*          try {   
*/          $status =Wishlist::where('id',$request->id)->delete();  
            if ($status){
            $response['status'] = "success";
            $response['message'] = "Product successfully remove";
            return Response::json($response);

            }else{
            $response['status'] = "error";
            $response['message'] = "Something went wrong ";
            return Response::json($response);
            }

         /* } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }*/
    }





}

