<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\UserWallet;
use App\Models\UserPrescription;
use App\Models\Order;
use App\Models\OrderItem;
use App\Traits\ImageTrait;

use response;
use Session;
use DB;
use Hash;
use URL;


class CustomerController extends Controller
{

    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*try {*/
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page

                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');

                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value

                // Total records
                $totalRecords = User::select('count(*) as allcount')->count();
                $totalRecordswithFilter = User::select('count(*) as allcount')
                    ->where('role', 'user');
                if($searchValue!=''){ 
                $totalRecordswithFilter->where('users.first_name', 'like', '%' .$searchValue . '%');
                }
                $totalRecordswithFilter = $totalRecordswithFilter->count();
                // Fetch records
                $records = User::orderBy($columnName, $columnSortOrder)
                    ->where('role', 'user')
                    ->select('users.*')
                    ->skip($start)
                    ->take($rowperpage);
                     if($searchValue!=''){ 
                     $records->where('users.first_name', 'like', '%' .$searchValue . '%');
                     }
                $records=$records->get(); 
                //echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach ($records as $key => $val) {
                    //foreach($val->sub_category as $sub_category){
                    $edit_url = URL::to("admin/customer/edit/$val->id");
                    $delete_url = URL::to("admin/customer/$val->id/confirm-delete");
                    $view_url = URL::to("admin/customer/show/$val->id");
                    $view_history = URL::to("admin/customer/history/$val->id");
                    $accepted_order_url = URL::to("admin/customer/my-order/$val->id");
                   // $view_prescription = URL::to("admin/customer/prescription/$val->id");
                    if ($val->status == 1) {
                        $btnclass = "btn-success";
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    } else {
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }

                     if(!empty($val->image)){
                     $url= $val->image;
                     }else{
                     $url= asset('/public/assets/images/no-image.jpg');
                     } 

                
                    $data_arr[] = array(
                        'id' => $val->id,
                        'name'=>$val->name,
                        'email' => $val->email,
                        'mobile_number' => $val->mobile_number,
                        'dob' => $val->dob,
                        'gender' => $val->gender,
                        'created_at' =>date('d F,y',strtotime($val->created_at)),
                         'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                        'action' => '<a class="btn btn-info" style="margin: 1px;" href="' . $edit_url . '"><i class="fa fa-edit">Edit</i> </a>
                        <a class="btn btn-primary" style="margin: 3px;" href="' . $view_url . '"><i class="fa fa-eye"></i>View </a>

                         <a data-toggle="modal" data-target="#delete_confirm" href="' . $delete_url . '" class="delval btn btn-xs btn-danger"  title="Delete" style="margin: 1px;"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="' . route('admin.customer.updatestatus.update-status-confirm', [$val->status, $val->id]) . '" class="delval btn btn-xs ' . $btnclass . '"  title="Status" style="margin: 1px;"><i class="fa ' . $faicon . '" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Status">'.$status.'</i> 
                          </a>
                           <a class="btn btn-warning" style="margin: 1px;" href="'.$accepted_order_url.'"><i class="fa fa-first-order"></i>My Order </a>
                         <a class="btn btn-primary" style="margin: 1px;background:black;" href="' . $view_history . '"><i class="fa fa-history"></i>Wallet History </a>

                        
                          '
                    );
                   
                    // }  
                } //Close foreach loop
                $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                );

                echo json_encode($response);
                die;
            }

            return view('backend.customer.index');
       /* } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }*/
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        return view('backend.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { {

          // echo "<pre>";print_r($request->all());die;
            //  DB::beginTransaction();
            //  try{   

            $validator = Validator::make($request->all(), [

                'email' => 'unique:users,email',
                'password' => 'required|confirmed|min:6',
                'dob' => 'required',
                //'gender' => 'required',
                'mobile_number' => 'required|unique:users,mobile_number',
                'password_confirmation'=>'required',
                'image'=>'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 422);
            } else {
                /*Save Customer Info ============================*/
                $save =  new User();
                if($request->hasFile('image')){
                $folder_name ='image';
                $save['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
                }
                $save->name = $request->first_name . ' ' . $request->last_name;
                $save->first_name = $request->first_name;
                $save->last_name = $request->last_name;
                $save->email = $request->email;
                $save->mobile_number = $request->mobile_number;
                $save->dob      = $request->dob;
                $save->gender      = $request->gender;
                $save->password      = Hash::make($request->password);
                $save->role = 'user';
                $status  = $save->save();
              //echo  $last_user_id = $save->id; die;
                
                if ($status) {
                    $response['status'] = "success";
                    $response['message'] = "your account has been created successfully.";
                    $response['url']     = route('admin.customer.list');
                    return json_encode($response);
                } else {
                    $response['status'] = "error";
                    $response['message'] = "There was an issue account createing the recode. Please try again.";
                    return json_encode($response);
                }
            }
            //DB::commit();   
            return json_encode($response);
            /* }catch(\Exception $e)
        {
          // DB::rollback(); 
           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::where('id', $id)->first();
        //echo "<pre>"; print_r($data);die;
        $order_data = Order::where('user_id',$id)->get();
        //echo "<pre>"; print_r($order_data);die;
        $user_prescription = UserPrescription::where('user_id',$id)->first();
        return view('backend.customer.show', compact('data','order_data','user_prescription'));
    }


    public function history($id){
        $wallet_history = UserWallet::where('user_id',$id)
        ->orderBy('id','DESC')
        ->get();
        return view('backend.customer.wallet_list', compact('wallet_history'));
    }

 /*   public function prescription($id =null){
        $user_prescription = UserPrescription::where('user_id',$id)->first();
        //echo "<pre>";print_r($user_prescription);die;
        return View('backend.customer.prescription', compact('user_prescription'));

    }*/


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   

        $data = User::where('id', $id)->first();
       $user_prescription = UserPrescription::where('user_id',$id)->first();

        //echo "<pre>"; print_r($data);die;
        return view('backend.customer.edit', compact('data','user_prescription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

         $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'dob' => 'required',
                //'gender' => 'required',
                'mobile_number' => 'required',
                //'image'=>'required',
                 'password' => 'required|confirmed|min:6',
                'password_confirmation'=>'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 422);
            } else {
     // echo "<pre>"; print_r($request->all()); die();
     // try {  
       $customer = $request->all();

         if($request->hasFile('image')){
         $folder_name ='image';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['name'] = $request->first_name . ' ' . $request->last_name;
        $data['email'] = $request->email;
        $data['mobile_number'] =$request->mobile_number;
        $data['dob'] =$request->dob;
        $data['gender'] =$request->gender;
        $data['password']= Hash::make($request->password);
     
        $data =User::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.customer.list');
                    $response['message'] = "Customer has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create customer');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/

      }
       
    }


    //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Customer';
        $confirm_route = $error = null;
        $confirm_route = route('admin.customer.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

     public function prescription($id)
    {

        $model = 'prescription';
        $confirm_route = $error = null;
         $user_prescription = UserPrescription::where('user_id',$id)->first();
        //$confirm_route = route('admin.customer.delete', $id);
        return View('backend.customer.prescription', compact('error', 'model','user_prescription'));
    }

    public function destroy($id)
    {
        $data = User::find($id);
        $status =  $data->forceDelete();

        if ($status) {
            request()->session()->flash('success', 'Customer successfully deleted');
            return redirect()->route('admin.customer.list');
        } else {
            request()->session()->flash('error', 'Error while deleting vendor');
        }
        return redirect()->route('admin.customer.list');
    }

    //Here is the call status model view page ------ 
    public function status_confirm($status, $id)
    {
        if ($status == '1') {
            $status = 'Inactive';
        } else {
            $status = 'Active';
        };
        $route_link = route('admin.customer.updatestatus', [$status, $id]);
        return view('backend/layouts/active-inactive', compact('route_link', 'status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status, $id)
    {
        if ($status == 'Active') {
            $status = '1';
        } else {
            $status = '0';
        };
        $status = User::where('id', $id)->update(['status' => $status]);

        if ($status) {
            Session::flash('success', 'Status Updated Successfully.');
        } else {
            Session::flash('error', 'Somthing went wrong.');
        }
        return Redirect()->route('admin.customer.list');
    }


    public function old_accepted_order($id){

        $customer_data = Order::with('User','OrderPayment','OrderItem')->orderBy('id', 'DESC')->where('user_id',$id)->get();

       return view('backend.customer.my-order',compact('customer_data'));

    }

    public function accepted_orders(Request $request,$id)
    { 
         
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
 
                ## Custom Field value
                //$order_status = $request->get('order_status');
                // Total records
                $totalRecords = Order::select('count(*) as allcount')->count();
               // echo $totalRecords; die();
                 $order= Order::select('count(*) as allcount','order.order_id')->with(['User','OrderPayment','OrderItem'])->where('user_id',$id)
                 ->Where('order_code', 'like', '%' .$searchValue . '%');
               /* if($searchValue!=''){ 
                $order->whereHas('User', function($q) use ($searchValue) {
                    $q->where('name', 'like', '%' . $searchValue . '%');
                   });
                }*/
              
               /* if($order_status!=''){ 
                $order->where('status',$order_status);
                }
                if ($request->get('from_date') and !empty($request->from_date)) {
                  $order->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                  $order->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }*/
                $totalRecordswithFilter=$order->count();
               // Fetch records
               $records = Order:: 
                   with(['User','OrderPayment','OrderItem'])
                   ->Where('order_code', 'like', '%' .$searchValue . '%')
                 ->select('*')
                 ->skip($start)
                ->take($rowperpage);
                /*->where('status','accepted')
                ->whereHas('User', function($q) use ($searchValue) {
                    $q->where('name', 'like', '%' . $searchValue . '%');
                   })->orderBy('order_id','DESC');
                if($order_status!=''){ 
                  $records->where('status',$order_status);
               }*/
                  
                /*if ($request->get('from_date') and !empty($request->from_date)) {
                $records->whereDate('created_at','>=',$request->from_date." 00:00:00");
                }
                if ($request->get('to_date') and !empty($request->to_date)){
                $records->whereDate('created_at','<=',$request->to_date." 23:59:59");
                }*/
                $records=$records->where('user_id',$id)->orderBy('id','DESC')->get();
                //echo "<pre>"; print_r($records);die;

                $data_arr = [];
                foreach($records as $key=> $val){
                   // $delete_url = URL::to("banner/$val->id/confirm-delete");
                    $view_url = URL::to("admin/customer/view/$val->id");
                   // $view_invoice = URL::to("admin/order/invoice/$val->order_id");
                     if($val->shipping_amount<=0){
                        $shipping_amount = "Free";
                    }else{ 
                        $shipping_amount = '₹'.$val->shipping_amount;
                    }
                    /*$status = '<span class="badge badge-success">'.ucwords($val->status).'</span>';*/

                    $data_arr[] = array( 
                        'order_code'=>$val->order_code,
                        'user_name'=>ucwords($val->user->name),
                        'payment_method'=>ucwords($val->orderpayment->payment_method ?? ''),
                        'sub_total'=>'₹'.$val->sub_total,
                        'shipping_amount'=>$shipping_amount,
                        'total_payed_amount'=>$val->total_payed_amount,
                        'discount_coupon_code'=>isset($val->discount_coupon_code)?$val->discount_coupon_code:'==',
                        'status'=>Helper::$order_status_customer[$val->order_status],
                        'created_at'=> date('d F,Y', strtotime($val->created_at)),
                         'action'=>'<a title="View Details " class="btn btn-info" href="'.$view_url.'">View Item </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" =>  $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
              
             $my_order_id = $id;
            return view('backend.customer.my-orders',compact('my_order_id'));
       
        } catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }


    public function order_show($id=null)
    {
      //$vendors = User::where('role','vendor')->get();
     $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$id)->first();
      return view('backend.customer.order-details',compact('order'));
    }
    
}
