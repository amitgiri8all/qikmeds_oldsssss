<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SubscriptionPlan;
use Illuminate\Http\Request;
use Validator;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;

class SubscriptionPlanController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = SubscriptionPlan::select('count(*) as allcount')->count();
                $totalRecordswithFilter = SubscriptionPlan::select('count(*) as allcount')
               // ->where('role','vendor')
                ->Where('plan_type', 'like', '%' .$searchValue . '%')
                ->orWhere('plan_name', 'like', '%' .$searchValue . '%')
               /* ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = SubscriptionPlan::orderBy($columnName,$columnSortOrder)
                //->where('role','vendor')
                 ->Where('subscription_plan.plan_type', 'like', '%' .$searchValue . '%')
                 ->orWhere('subscription_plan.plan_name', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('subscription_plan.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/subscriptionplan/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/subscriptionplan/edit/$val->id");
                    $view_url = URL::to("admin/subscriptionplan/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                   if(!empty($val->image)){
                    $url= $val->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }   
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'plan_type'=>$val->plan_type,
                        'plan_name'=>$val->plan_name,
                        'duration'=>$val->duration,
                        'price'=>$val->price,
                        'sale_price'=>$val->sale_price,
                        'discount'=>$val->discount,
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" href="'.$edit_url.'"><i class="fa fa-edit"></i>Edit </a>
                        <a class="btn btn-primary" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                        
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.subscriptionplan.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.subscriptionplan.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.subscriptionplan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['plan_type' => 'required','plan_name'=>'required','duration' => 'required','price' => 'required', 'sale_price' => 'required','image' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
            // DB::beginTransaction();
            try
            {
                    //echo "<pre>"; print_r($request->all()); die();

                $subscriptionplan = new SubscriptionPlan();

                if($request->hasFile('image')){
                $folder_name ='image';
                $subscriptionplan['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
                }
                $subscriptionplan->plan_type = $request->plan_type;
                $subscriptionplan->plan_name = $request->plan_name;
                $subscriptionplan->duration = $request->duration;
                $subscriptionplan->price = $request->price;
                $subscriptionplan->sale_price = $request->sale_price;
                $subscriptionplan->discount = $request->product_discount;
               
                $data = $subscriptionplan->save();
               
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.subscriptionplan.list');
                    $response['message'] = "subscriptionplan has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create subscription plan');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = SubscriptionPlan::where('id',$id)->first();
      // $data_vendor = Vendor::where('user_id',$id)->first();
       return view('backend.subscriptionplan.show',compact('data'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = SubscriptionPlan::where('id',$id)->first();
        //$data_image = Vendor::where('user_id',$id)->first();
        return view('backend.subscriptionplan.edit',compact('data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator = Validator::make($request->all() , ['plan_type' => 'required','plan_name'=>'required','duration' => 'required','price' => 'required', 'sale_price' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {   
     // echo "<pre>"; print_r($request->all()); die();
     // try {  
       $Subscriptionmeta = $request->all();

       if($request->hasFile('image')){
         $folder_name ='image';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['plan_type'] = $request->plan_type;
        $data['plan_name'] = $request->plan_name;
        $data['duration'] = $request->duration;
        $data['price'] = $request->price; 
        $data['sale_price'] = $request->sale_price;
        $data['discount'] = $request->product_discount;

        $data =SubscriptionPlan::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.subscriptionplan.list');
                    $response['message'] = "Subscription plan has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Subscription plan');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
      }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Subscription-plan';
        $confirm_route = $error = null;
        $confirm_route = route('admin.subscriptionplan.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=SubscriptionPlan::find($id);
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','subscription plan successfully deleted');
                    return redirect()->route('admin.subscriptionplan.list');
        }
        else{
            request()->session()->flash('error','Error while deleting Subscription Plan');
        }
        return redirect()->route('admin.subscriptionplan.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.subscriptionplan.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = SubscriptionPlan::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.subscriptionplan.list');
     }
}

