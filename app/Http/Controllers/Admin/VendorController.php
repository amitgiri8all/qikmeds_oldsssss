<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Models\Document;
use App\Models\OrderItem;
use App\Models\Order;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;
class VendorController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        /*try{*/
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = User::select('count(*) as allcount')->where('role','vendor')->count();

                $totalRecordswithFilter = User::select('count(*) as allcount')
                ->where('role','vendor');
               
                if($searchValue!=''){ 
                $totalRecordswithFilter->where('users.first_name', 'like', '%' .$searchValue . '%');
                }
                $totalRecordswithFilter=$totalRecordswithFilter->count();
                // Fetch records
               // DB::enableQueryLog(); // Enable query log
    
                $records = User::orderBy($columnName,$columnSortOrder)
                ->where('role','vendor')
                ->select('users.*')
                ->skip($start)
                ->take($rowperpage);

                if($searchValue!=''){ 
                $records->where('users.first_name', 'like', '%' .$searchValue . '%');
                }
                $records=$records->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/vendor/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/vendor/edit/$val->id");
                    $view_url = URL::to("admin/vendor/show/$val->id");
                    $vendor_document = URL::to("admin/vendor/document/$val->id");
                    $accepted_order_url = URL::to("admin/vendor/accepted-order/$val->id");
                    

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                   if(!empty($val->image)){
                    $url= $val->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }   
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'first_name'=>$val->first_name,
                        'last_name'=>$val->last_name,
                        'email'=>$val->email,
                        'address'=>$val->address,
                        'city'=>$val->city,
                        'state'=>$val->state,
                        'country'=>$val->country,
                        'mobile_number'=>$val->mobile_number,
                        'pin_code'=>$val->pin_code,
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" href="'.$edit_url.'"><i class="fa fa-edit"></i>Edit </a>
                        <a class="btn btn-primary" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                        
                        
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.vendor.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>
                        <a class="btn btn-dark" href="'.$vendor_document.'"><i class="fa fa-eye"></i>Document </a>
                         <a class="btn btn-warning" href="'.$accepted_order_url.'"><i class="fa fa-first-order"></i>Acccepted Order </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.vendor.index');
        /*}
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['first_name' => 'required','last_name'=>'required', 'email' => 'required|email|unique:users', 'address_name' => 'required','city' => 'required', 'state' => 'required', 'country' => 'required','mobile_number' => 'required|unique:users,mobile_number', 'pin_code' => 'required','password' => 'required|confirmed|min:6','password_confirmation'=>'required','image'=>'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
            // DB::beginTransaction();
            try
            {
                    //echo "<pre>"; print_r($request->all()); die();

                $vendor = new User();

                if($request->hasFile('image')){
                $folder_name ='image';
                $vendor['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
                }
                $vendor->name = $request->first_name.' '.$request->last_name;
                $vendor->first_name = $request->first_name;
                $vendor->last_name = $request->last_name;
                $vendor->email = $request->email;
                $vendor->address = $request->address_name;
                $vendor->city = $request->city;
                $vendor->state = $request->state;
                $vendor->country = $request->country;
                $vendor->mobile_number = $request->mobile_number;
                $vendor->pin_code = $request->pin_code;
                $vendor->password = Hash::make($request->password);
                $vendor->role = 'vendor';
                $data = $vendor->save();
                //echo $vendor->id; die();
                /*Image Uploads*/
                /*if ($request->hasFile('licensed_pharmacist_details'))
                {
                    $folder_name = 'licensed_pharmacist_details';
                    $licensed_pharmacist_details = $this->fileUpload($request->file('licensed_pharmacist_details') , false, $folder_name);
                }

                if ($request->hasFile('authorized_pharmacy_related_documents'))
                {
                    $folder_name = 'authorized_pharmacy_related_documents';
                    $authorized_pharmacy_related_documents = $this->fileUpload($request->file('authorized_pharmacy_related_documents') , false, $folder_name);
                }*/

                /*Image Uploads*/
               /* $vendordetils = new Vendor();
                $vendordetils->licensed_pharmacist_details = $licensed_pharmacist_details;
                 $vendordetils->authorized_pharmacy_related_documents =$authorized_pharmacy_related_documents;
                $vendordetils->user_id = $vendor->id;
                $data = $vendordetils->save();*/
               /// echo $data->id; die();
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.vendor.list');
                    $response['message'] = "Vendor has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Vendor');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = User::where('id',$id)->first();
      // $data_vendor = Vendor::where('user_id',$id)->first();
       return view('backend.vendor.show',compact('data'));
        
    }

    public function document($id){
        
        $vendor_documents = Document::where('user_id',$id)->first();
         return view('backend.vendor.vendor-document',compact('vendor_documents'));
    }

   /* public function accepted_order($id)
    {
         $accepted_order_data = OrderItem::select('order_item.order_id','users.name','order.sub_total','order.shipping_amount','order.discount_coupon_code','order.total_payed_amount','order_payment.payment_method','order_item.seller_shipment_status','order_item.product_name','order_item.product_price','order_item.quantity_order','order.created_at','product_image.image')
          
          ->leftJoin('users', 'users.id', '=', 'order_item.vendor_id')

          ->leftJoin('order', 'order.id', '=', 'order_item.order_id')

          ->leftJoin('order_payment', 'order_payment.order_id', '=', 'order_item.order_id')
         
         ->leftJoin('product_image', function($join)
        {
          $join->on('product_image.prod_id', '=', 'order_item.product_id');

        })
        ->where('order_item.vendor_id',$id)
        ->whereNull('order_item.deleted_at')
        ->get();                               

        $accepted_order_data =  OrderItem::where([ ['vendor_id',$id]])
        ->whereNull('deleted_at')
        ->get();
       return view('backend.vendor.accepted-order',compact('accepted_order_data'));

    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */




    public function edit($id)
    {   
        $data = User::where('id',$id)->first();
        //$data_image = Vendor::where('user_id',$id)->first();
        return view('backend.vendor.edit',compact('data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all() , ['first_name' => 'required','last_name'=>'required', 'email' => 'required|email', 'address_name' => 'required','city' => 'required', 'state' => 'required', 'country' => 'required','mobile_number' => 'required', 'pin_code' => 'required',  'password' => 'required|confirmed|min:6',
                'password_confirmation'=>'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        { 
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $vendor = $request->all();

       if($request->hasFile('image')){
         $folder_name ='image';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['email'] = $request->email;
        $data['address'] = $request->address_name; 
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['country'] = $request->country;
        $data['mobile_number'] =$request->mobile_number;
        $data['pin_code'] = $request->pin_code;
        $data['password']= Hash::make($request->password);

        $data =User::findOrFail($id)->update($data);


     /*  if ($request->hasFile('licensed_pharmacist_details'))
           {
            $folder_name = 'licensed_pharmacist_details';
            $licensed_pharmacist_details = $this->fileUpload($request->file('licensed_pharmacist_details') , false, $folder_name);
            }

        if ($request->hasFile('authorized_pharmacy_related_documents'))
        {
            $folder_name = 'authorized_pharmacy_related_documents';
            $authorized_pharmacy_related_documents = $this->fileUpload($request->file('authorized_pharmacy_related_documents') , false, $folder_name);
        }*/


        //echo $licensed_pharmacist_details.'===='.$id.'====='.$authorized_pharmacy_related_documents; die();
       // $data= DB::table('vendor')->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details,'authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents])->where('user_id ',$id);
        /* if(!empty($licensed_pharmacist_details) && !empty($authorized_pharmacy_related_documents)){
             $data = Vendor::where("user_id", $id)->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details,'authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents]);
         }elseif(!empty($licensed_pharmacist_details)){
             $data = Vendor::where("user_id", $id)->update(['licensed_pharmacist_details'=>$licensed_pharmacist_details]);

         }elseif(!empty($authorized_pharmacy_related_documents)){
             $data = Vendor::where("user_id", $id)->update(['authorized_pharmacy_related_documents'=>$authorized_pharmacy_related_documents]);
         }*/

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.vendor.list');
                    $response['message'] = "Vendor has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Vendor');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/

      }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Vendor';
        $confirm_route = $error = null;
        $confirm_route = route('admin.vendor.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=User::find($id);
       //$data->delete();
      // $data=Vendor::where('user_id',$id);
       $status =  $data->forceDelete();
        if($status){
            request()->session()->flash('success','vendor successfully deleted');
                    return redirect()->route('admin.vendor.list');
        }
        else{
            request()->session()->flash('error','Error while deleting vendor');
        }
        return redirect()->route('admin.vendor.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.vendor.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = User::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.vendor.list');
     }

      public function accepted_orders(Request $request,$id)
    { 
        //echo $id; die;
         
/*         try{
*/            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
 
                ## Custom Field value
                //$order_status = $request->get('order_status');
                // Total records
                $totalRecords = Order::select('count(*) as allcount')->count();
               // echo $totalRecords; die();
                $order= Order::select('count(*) as allcount','order.order_id')->with(['User','OrderItem']);
                if($id!=''){ 
                $order->whereHas('OrderItem', function($q) use ($id) {
                    $q->where('vendor_id', $id);
                   });
                }
        
                $totalRecordswithFilter=$order->count();
               // Fetch records
               $records = Order:: 
                   with(['User','OrderItem'])
                  ->skip($start)
                  ->take($rowperpage);
              
                $records=$records
                   ->whereHas('OrderItem', function($q) use ($id) {
                    $q->where('vendor_id', $id);
                   })->get();
               // echo "<pre>"; print_r($records);die;

                $data_arr = [];
                foreach($records   as $key=> $val){
                      $view_product_url = URL::to("admin/vendor/view-product/$val->id");
                     if($val->shipping_amount<=0){
                        $shipping_amount = "Free";
                    }else{ 
                        $shipping_amount = '₹'.$val->shipping_amount;
                    }
                    
                   // $sub_total_data = json_decode($val->order,true);
                   // echo "<pre>"; print_r($sub_total_data);die;
                   // $arraySingle = call_user_func_array('array_merge', $sub_total_data);
                   
                  //  $status =  
                    //App\Helpers\Helper::order_status_vendor(ucwords($val->status));

                    $data_arr[] = array( 
                        'order_id'=>$val->id,
                        'user_name'=>ucwords($val->user->name),
                        'payment_method'=>ucwords($val->orderpayment->payment_method ?? ''),
                        'sub_total'=>'₹'.$val->sub_total,
                        'shipping_amount'=>$shipping_amount,
                        'total_payed_amount'=>$val->total_payed_amount,
                        'discount_coupon_code'=>isset($val->discount_coupon_code)?$val->discount_coupon_code:'==',
                        'status'=>Helper::$order_status_vendor[$val->order_status],
                        'created_at'=> date('d F,Y', strtotime($val->created_at)),

                         'action'=>'<a title="View Details " class="btn btn-info" href="'.$view_product_url.'">View Product </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" =>  $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
              
             $my_order_id = $id;
            return view('backend.vendor.accepted-order',compact('my_order_id'));
       
      /*  } catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    }


    public function product_show($id=null)
    {
     $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$id)->first();
      return view('backend.vendor.product-show',compact('order'));
    }
}

