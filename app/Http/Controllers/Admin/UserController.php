<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use App\Helpers\Helper;
use App\Models\Role;
use App\Models\Category;
use App\Models\Vehicle;
use App\Models\UserWallet;
use App\Traits\ImageTrait;
use App\Helpers\datehelper;
use DataTables;
use Session;
use Hash;
use DB;
class UserController extends Controller {
    use ImageTrait;
    // Define All Properties -----------
    protected $user;
    protected $method;
    protected $order;
    protected $vehicle;
    // Define Constructer Methode -----------
  /*  function __construct(Request $request, User $user,   Order $order,) {
        // Initialize the object and its properties by assigning value(models)-----------
        $this->user        = $user;
 
        $this->order    = $order;
    }*/
    // Load List Page View -----------
    public function index() {
       // $zone     = $this->zone->get();  
        //return view('admin.user.list',compact('zone'));
    }
    //Pass posts data to view and load datatable view -----------
    public function data(Request $request) {
        //echo $request->zone_id; die();
        $user = $this->user->select('id', 'name', 'image', 'logo','zone_id', 'email', 'status', 'user_type', 'phone_number', 'created_at', 'last_login')->where('user_type', '!=', 'user')->where('role', '!=', 'admin');
        if ($request->has('email') and !empty($request->email)) {
            $user->where('id', $request->email);
        }
        if ($request->has('user_type') and !empty($request->user_type)) {
            $user->where('user_type', $request->user_type);
        }if ($request->has('zone_id') and !empty($request->zone_id)) {
            $user->where('zone_id', $request->zone_id);
        }
        $user = $user->get();
        return DataTables::of($user)->addIndexColumn()->addColumn('created_at', function ($user) {
            return date($this->date, strtotime($user->created_at));
        })->addColumn('last_login', function ($user) {
            if (!empty($user->last_login)) {
                return date('d-m-Y H:i:s', strtotime($user->last_login));
            } else {
                return '-';
            }
        })->addColumn('user_type', function ($user) {
            if (!empty($user->user_type) && $user->user_type == 'vendor') {
                $url = isset($user->logo) ? $user->logo : '';
                return '<img src="' . \URL::asset('storage/app/public/upload/Thumbnail/' . $url . '"') . '" border="0" width="50" style="border-radius: 89px;position: absolute;">
            <img src="' . \URL::asset('public/assets/images/vendor2.jpeg') . '" border="0" width="150" />';
            } else {
                return '<img src="' . \URL::asset('public/assets/images/driver.png') . '" border="0" width="150" />';
            }
        })->addColumn('image', function ($user) {
            $url = isset($user->image) ? $user->image : '';
            return '<img src="' . \URL::asset('storage/app/public/upload/Thumbnail/' . $url . '"') . '" border="0" width="100" />';
        })->addColumn('actions', '@if(App\Models\Permission::checkPermission("user.edit"))<a class="btn btn-info" href="{{URL::to("user/edit/$id")}}"><i class="fa fa-edit"></i>Edit</a>@endif
          @if(App\Models\Permission::checkPermission("user.delete"))
          <a data-toggle="modal" data-target="#delete_confirm" href="{{URL::to("user/$id/confirm-delete")}}" class="delval btn btn-xs btn-danger"  title="Delete user"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete user"></i> Delete user </a>@endif
          <a data-toggle="modal" data-target="#delete_confirm" href="{{route(\'admin.customer.updatestatus.update-status-confirm\',[$status,$id])}}" class="delval btn btn-xs @if($status==1)btn-success @else btn-danger @endif"  title="Delete"><i class="fa @if($status==1)fa-toggle-on @else fa-toggle-off @endif" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> @if($status==1)Active @else Inactive @endif </a>
            <br />
            <a class="btn btn-warning" href="{{URL::to("vendor-product/list?vendor=$id")}}"><i class="fa fa-eye"></i>View Vendor Product</a>')->rawColumns(['actions', 'last_login', 'user_type', 'image'])->make(true);
    }
    //Load insert form view and pass data all access level on add page----------
    public function create() {
        $vehicle = $this->vehicle->get();
        $accessLevel = $this->accessLevel->get();
        $roles = Role::select('id', 'name')->get();
        $zone = $this->zone->with(['translations'])->get();
        $category = Category::with(['translations'])->get();
        return view('admin.user.add', compact('accessLevel', 'category', 'zone', 'roles','vehicle'));
    }

    //Insert post data in table ---------
    public function store(Request $request) {
    // echo "<pre>"; print_r($request->all()); die();


if($request->user_type!='driver'){
    $validated = $request->validate(['user_type' => 'required', 'email' => 'required|email', 'password' => 'required', 'password_confirmation' => 'required_with:password|same:password|min:6', 'zone_id' => 'required','open_time' => 'required', 'phone_number' => 'required','close_time' => 'required','name' => 'required','address' => 'required','address_location' => 'required'
     ]);
}else{
   $validated = $request->validate(['user_type' => 'required', 'email' => 'required|email|unique:users', 'password' => 'required', 'password_confirmation' => 'required_with:password|same:password|min:6','phone_number' => 'required'
     ]);
}
 

        $input = request()->except(['_token']);
        $input['password'] = Hash::make($request->password);
        if ($request->hasFile('upload_image')) {
            $folder_name = 'user';
            $input['image'] = $this->fileUpload($request->file('upload_image'), false, $folder_name);
        }
        if ($request->hasFile('logo_image')) {
            $folder_name = 'vendor-logo';
            $input['logo'] = $this->fileUpload($request->file('logo_image'), false, $folder_name);
        } 
        if ($request->hasFile('license_image')) {
            $folder_name = 'license-image';
            $input['license_image'] = $this->fileUpload($request->file('license_image'), false, $folder_name);
        }
        if(!empty($input['zone_id'])){
          $input['zone_id'] = $input['zone_id'];
        }
        if(!empty($input['address_location'])){
         $input['address_location'] = $input['address_location'];   
        }
            if(!empty($input['restaurant_review_number'])){
         $input['restaurant_review_number'] = $input['restaurant_review_number'];   
        }
         if(!empty($input['offer_tag'])){
         $input['offer_tag'] = $input['offer_tag'];   
        }   
         if(!empty($input['restaurant_review'])){
         $input['restaurant_review'] = $input['restaurant_review'];   
        }
        
         try {
            //echo "<pre>"; print_r($input); die();
            $this->user->create($input);
            //Session::flash('success', ' You have successfully create user.');
            return redirect('user/list')->with('success', trans('You have successfully create user.'));

        }
        catch(\Exception $e) {
            Session::flash('error', $e->getMessage());
        }
        return back();
    }
    //Get post data by id  and load edit form and pass data all category on edit page----------
    public function edit($id = null) {
       // $category = Category::all();
       // $vehicle = $this->vehicle->get();
        //$userData = $this->user->withoutGlobalScope(StatusScope::class)->findOrFail($id);
        $userData    = User::where('id',$id)->first();
        
        //$accessLevel = Role::select('id', 'name')->get();
       // $zone = $this->zone->get();
       //echo "<pre>"; print_r($userData->zone_id); die();
        return view('admin.user.edit', compact('userData'));
    }
    //Update post data by id ----------------
    public function update(Request $request, $id) {

    //echo "<pre>"; print_r($request->all()); die();

  /*     
    if($request->user_type!='driver'){
        $validated = $request->validate(['user_type' => 'required', 'email' => 'required|email','zone_id' => 'required','open_time' => 'required', 'phone_number' => 'required','close_time' => 'required','name' => 'required','address' => 'required','address_location' => 'required'
         ]);
    }else{
       $validated = $request->validate(['user_type' => 'required', 'email' => 'required|email','phone_number' => 'required'
         ]);
    }


        $inputs = request()->except(['_token']);
        if ($request->hasFile('upload_image')) {
            $folder_name = 'user';
            $inputs['image'] = $this->fileUpload($request->file('upload_image'), false, $folder_name);
        }
        if ($request->hasFile('logo_image')) {
            $folder_name = 'vendor-logo';
            $inputs['logo'] = $this->fileUpload($request->file('logo_image'), false, $folder_name);
        }
        if ($request->hasFile('license_image')) {
            $folder_name = 'license-image';
            $inputs['license_image'] = $this->fileUpload($request->file('license_image'), false, $folder_name);
        }

        if(!empty($inputs['zone_id'])){
          $inputs['zone_id'] = $inputs['zone_id'];
        }
         if(!empty($input['address_location'])){
         $input['address_location'] = $input['address_location'];   
        }  
          if(!empty($input['restaurant_review_number'])){
         $input['restaurant_review_number'] = $input['restaurant_review_number'];   
        }
         if(!empty($input['offer_tag'])){
         $input['offer_tag'] = $input['offer_tag'];   
        }   
         if(!empty($input['restaurant_review'])){
         $input['restaurant_review'] = $input['restaurant_review'];   
        }
        */


       if($request->hasFile('upload_image')){
         $folder_name ='profile';
         $data['image'] = $this->fileUpload($request->file('upload_image'),false,$folder_name);
        }

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['mobile_number'] = $request->phone_number;
        $data['dob'] = $request->dob; 
        $data['address'] = $request->address;
      

        $data =User::findOrFail($id)->update($data);
        if($data){
            request()->session()->flash('success','Admin successfully Updated');
                    return redirect()->route('profile');
        }
        else{
            request()->session()->flash('error','Error while deleting vendor');
        }
        return redirect()->route('profile');

       /* try {
          //  echo $request->hasFile('license_image'); die();
            //echo "<pre>"; print_r($inputs); die('ok');
            $user = $this->user->withoutGlobalScope(StatusScope::class)->findOrFail($id)->update($inputs);
            //Session::flash('success', 'You have successfully update user');
            
            return redirect('user/list')->with('success', trans('You have successfully update user.'));

        }
        catch(\Exception $e) {
            Session::flash('error', $e->getMessage());
        }
        return back();*/
    }
    //Here is the call delete model view page ------
    public function getModalDelete($id = null) {
        $model = 'User Model';
        $confirm_route = $error = null;
        $confirm_route = route('user.delete', $id);
        return View('admin/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }
    //Here is the part for delete data
    public function destroy($id) {
        $page = $this->user->withoutGlobalScope(StatusScope::class)->findOrFail($id);
        $page->delete();
         if ($page) {
            return redirect('user/list')->with('success', trans('User Delete Successfully.'));
        } else {
            return redirect('user/list')->with('error', trans('There was an issue deleting the recode. Please try again.'));
        }
    }
    //Here is the auto search value user name
    public function autocomplet_search(Request $request) {
        $data = User::select('id as data', 'email as value')->Where('email', 'like', '%' . $request->get('query') . '%')->where('role', '!=', 'admin')->where('user_type', '!=', 'user')->limit(5)->get();
        return response()->json(['suggestions' => $data]);
    }
    // Load List Page View -----------
    public function customer_list() {
        return view('admin.user.customer_list');
    }
    //Pass posts data to view and load datatable view -----------
    public function customer_data() {
    $type = 'user';
    $user = \DB::table('users')
    ->select('users.id', 'users.name','users.created_at','users.status','users.offer_tag','users.email', 'users.user_type', 'users.address', 'users.category_id', 'users.phone_number',DB::raw('SUM(ib_points) AS total_points'))
    ->leftJoin('ib_points', 'users.id', '=', 'ib_points.user_id')
    ->where('users.user_type', $type)->orderBy('users.id', 'DESC')
    ->groupBy('users.id')
    ->get();
        return DataTables::of($user)->addIndexColumn()
        ->addColumn('total_points', function ($user) {
            return isset($user->total_points)?$user->total_points:'0.00';

        })
        
        ->addColumn('created_at', function ($user) {
            return date($this->date, strtotime($user->created_at));

        })
        ->addColumn('actions', ' 
          <a data-toggle="modal" data-target="#delete_confirm" href="{{route(\'admin.customer.updatestatus.update-status-confirm\',[$status,$id])}}" class="delval btn btn-xs @if($status==1)btn-success @else btn-danger @endif"  title="Delete"><i class="fa @if($status==1)fa-toggle-on @else fa-toggle-off @endif" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> @if($status==1)Active @else Inactive @endif </a>

          <a href="{{URL::to("customer/wallet/$id")}}" class="btn btn-primary btn-xs">Wallet History</a>')->rawColumns(['actions'])->make(true);
    }
    // Load List Page View -----------
    public function wallet_list($id) {
         //$mywallet = $this->order->with(['User'])->select('*')->where('user_id',$id)->orderBy('id','DESC')->get();
          $user_wallet = UserWallet::select('*')->where('user_id',$id)->orderBy('id','DESC')->get();
         // echo "<pre>"; print_r($user_wallet); die();
        return view('admin.user.wallet_list', compact('id'));
    }
    // Load List Page View -----------
    public function wallet_data($id) {
       // $user = $this->order->with(['User'])->select('*')->where('user_id', $id)->orderBy('id', 'DESC')->get();
         $user = UserWallet::select('*')->where('user_id',$id)->orderBy('id','DESC')->get();
        return DataTables::of($user)->addIndexColumn()->addColumn('created_at', function ($user) {
            return date($this->date, strtotime($user->created_at));
        })->make(true);
    }
    public function not_permission() {
        return view('errors.not-permission');
    }
    //Here is the call status model view page ------
    public function status_confirm($status, $id) {
        if ($status == '1') {
            $status = 'Inactive';
        } else {
            $status = 'Active';
        };
        $route_link = route('admin.customer.updatestatus', [$status, $id]);
        return view('admin/layouts/active-inactive', compact('route_link', 'status'));
    }
    //Here is udpate status  Active And Inactive
    public function update_status($status, $id) {
        if ($status == 'Active') {
            $status = '1';
        } else {
            $status = '0';
        };
        $status = User::where('id', $id)->update(['status' => $status]);
        if ($status) {
            Session::flash('success', 'Status Updated Successfully.');
        } else {
            Session::flash('error', 'Somthing went wrong.');
        }
        return Redirect()->back();
    }  

    //Here is udpate status  Active And Inactive ========================
    public function check_zone_user(Request $request) {
        $zone_id =  $request->zone_id;
        $get_zone_ids = $this->user->where('zone_id',$zone_id)->count();
        if($get_zone_ids >10){
         $json['message']="You have already 10 vendor registered this zone,please select other zone";
         $json['form']=true;
         $json['select']=true;
         $status = true;
        }else{
         $json['message']="";
         $json['form']=false;
         $json['select']=false;
         $status = true;
     }
        return json_encode($json);
    }
}
