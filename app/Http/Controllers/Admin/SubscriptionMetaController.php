<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SubscriptionMeta;
use App\Models\SubscriptionPlan;
use Illuminate\Http\Request;
use Validator;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;

class SubscriptionMetaController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = SubscriptionMeta::select('count(*) as allcount')->count();
                $totalRecordswithFilter = SubscriptionMeta::select('count(*) as allcount')
               // ->where('role','vendor')
                ->Where('title', 'like', '%' .$searchValue . '%')
                ->orWhere('type', 'like', '%' .$searchValue . '%')
                /*->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = SubscriptionMeta::orderBy($columnName,$columnSortOrder)
                //->where('role','vendor')
                 ->Where('title', 'like', '%' .$searchValue . '%')
                 ->orWhere('type', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('subscription_meta.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/subscriptionmeta/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/subscriptionmeta/edit/$val->id");
                    $view_url = URL::to("admin/subscriptionmeta/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                   if(!empty($val->image)){
                    $url= $val->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }   
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'type'=>$val->type,
                        'title'=>$val->title,
                        'description'=>$val->description,
                        'price'=>$val->price,
                        'price_type'=>$val->price_type,
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" href="'.$edit_url.'"><i class="fa fa-edit"></i>Edit </a>
                        <a class="btn btn-primary" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                        
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.subscriptionmeta.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.subscriptionmeta.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subscription_plan = SubscriptionPlan::all();
        return view('backend.subscriptionmeta.create',compact('subscription_plan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['type' => 'required','title'=>'required','price' => 'required','description' => 'required', 'image' => 'required','price_type' => 'required','subscription_plan_id' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
            // DB::beginTransaction();
            try
            {
                    //echo "<pre>"; print_r($request->all()); die();

                $subscriptionmeta = new SubscriptionMeta();

                if($request->hasFile('image')){
                $folder_name ='image';
                $subscriptionmeta['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
                }
                $subscriptionmeta->subscription_plan_id = $request->subscription_plan_id;
                $subscriptionmeta->type = $request->type;
                $subscriptionmeta->title = $request->title;
                $subscriptionmeta->description = $request->description;
                $subscriptionmeta->price = $request->price;
                $subscriptionmeta->price_type = $request->price_type;
                $data = $subscriptionmeta->save();
               
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.subscriptionmeta.list');
                    $response['message'] = "subscriptionmeta has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create subscriptionmeta');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = SubscriptionMeta::where('id',$id)->first();
       $subscription_show = SubscriptionPlan::where('id',$data->subscription_plan_id)->first();
       //dd($subscription_show);
       return view('backend.subscriptionmeta.show',compact('data','subscription_show'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = SubscriptionMeta::where('id',$id)->first();
        $subscription_plan_data = SubscriptionPlan::all();
        return view('backend.subscriptionmeta.edit',compact('data','subscription_plan_data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all() , ['type' => 'required','title'=>'required','price' => 'required','description' => 'required', 'price_type' => 'required','subscription_plan_id' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $Subscriptionmeta = $request->all();

       if($request->hasFile('image')){
         $folder_name ='image';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['subscription_plan_id'] = $request->subscription_plan_id;
        $data['type'] = $request->type;
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['price'] = $request->price; 
        $data['price_type'] = $request->price_type;
      
        $data =SubscriptionMeta::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.subscriptionmeta.list');
                    $response['message'] = "Subscription meta has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Subscription meta');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Subscriptionmeta';
        $confirm_route = $error = null;
        $confirm_route = route('admin.subscriptionmeta.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=SubscriptionMeta::find($id);
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','subscription meta successfully deleted');
                    return redirect()->route('admin.subscriptionmeta.list');
        }
        else{
            request()->session()->flash('error','Error while deleting vendor');
        }
        return redirect()->route('admin.subscriptionmeta.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.subscriptionmeta.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = SubscriptionMeta::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.subscriptionmeta.list');
     }
}

