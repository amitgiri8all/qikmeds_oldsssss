<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Illuminate\Http\Request;
use Validator;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;
class SubscribeController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
       // try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Newsletter::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Newsletter::select('count(*) as allcount')
                ->Where('email', 'like', '%' .$searchValue . '%')
                //->where('role','vendor')
                //->Where('mobile', 'like', '%' .$searchValue . '%')
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = Newsletter::orderBy($columnName,$columnSortOrder)
                  ->Where('email', 'like', '%' .$searchValue . '%')
                 // ->orWhere('email', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('newsletter.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
                // echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                   // $view_url = URL::to("admin/contact/show/$val->id");
                    $delete_url = URL::to("admin/subscribe/$val->news_id/confirm-delete");

                    $data_arr[] = array( 
                        'news_id'=>$val->news_id,
                        'email'=>$val->email,
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>
                     </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.subscribe.index');
   /*     }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */

    public function getModalDelete($id = null)
    {
        $model = 'Newsletter';
        $confirm_route = $error = null;
        $confirm_route = route('admin.subscribe.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
        //echo $id;die;
       $data=Newsletter::find($id);
       //$data->delete();
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','Subscribe successfully deleted');
                    return redirect()->route('admin.subscribe.list');
        }
        else{
            request()->session()->flash('error','Error while deleting subscribe');
        }
        return redirect()->route('admin.subscribe.list');
    }



}

