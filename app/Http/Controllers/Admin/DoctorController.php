<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Models\Doctor;
use App\Models\DoctorWorkInfo;
use App\Models\DoctorEducationInfo;
use App\Models\DoctorSpecializationsInfo;
use App\Models\Order;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;


class DoctorController extends Controller
{

    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page

                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');

                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value

                // Total records
                $totalRecords = User::select('count(*) as allcount')->count();
                $totalRecordswithFilter = User::select('count(*) as allcount')
                    ->where('role', 'doctor')
                    ->where('users.name', 'like', '%' .$searchValue . '%')
               /* ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                    ->count();
                // Fetch records
                $records = User::orderBy($columnName, $columnSortOrder)
                    ->where('role', 'doctor')
                   ->where('users.name', 'like', '%' .$searchValue . '%')
                /* ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                    ->select('users.*')
                    ->skip($start)
                    ->take($rowperpage)
                    ->get();
                //echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach ($records as $key => $val) {
                    //foreach($val->sub_category as $sub_category){
                    $edit_url = URL::to("admin/doctor/edit/$val->id");
                    $delete_url = URL::to("admin/doctor/$val->id/confirm-delete");
                    $view_url = URL::to("admin/doctor/show/$val->id");
                    $accepted_order_url = URL::to("admin/doctor/doctor-order/$val->id");
                    if ($val->status == 1) {
                        $btnclass = "btn-success";
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    } else {
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }

                    if (!empty($val->image)) {
                        $url =  $val->image;
                    } else {
                        $url = asset('/public/assets/images/no-image.jpg');
                    }
                    $data_arr[] = array(
                        'id' => $val->id,
                        'first_name' => $val->first_name,
                        'last_name' => $val->last_name,
                        'email' => $val->email,
                        'mobile_number' => $val->mobile_number,
                        'registration_number' => $val->registration_number,
                        'image' => '<img src="' . $url . '" border="0" width="100" height="100" class="img-rounded" align="center" />',
                        'created_at' => date('d F,Y', strtotime($val->created_at)),
                        'action' => '
                        <a class="btn btn-info" style="margin:1px;" href="' . $edit_url . '"><i class="fa fa-edit">Edit</i> </a>
                        <a class="btn btn-primary" style="margin:1px;" href="' . $view_url . '"><i class="fa fa-eye"></i>View </a>

                         <a data-toggle="modal" data-target="#delete_confirm" href="' . $delete_url . '" class="delval btn btn-xs btn-danger"  title="Delete" style="margin:1px;"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i> </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="' . route('admin.doctor.updatestatus.update-status-confirm', [$val->status, $val->id]) . '" class="delval btn btn-xs ' . $btnclass . '"  title="Delete" style="margin:1px;"><i class="fa ' . $faicon . '" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">'.$status.'</i> </a>
                        <a class="btn btn-warning" style="margin:1px;" href="'.$accepted_order_url.'"><i class="fa fa-first-order"></i> My Orders </a>
                        '
                    );
                    // }  
                } //Close foreach loop
                $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                );

                echo json_encode($response);
                die;
            }

            return view('backend.doctor.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.doctor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { {
            //  DB::beginTransaction();
            //  try{   

           
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'pin_code' => 'required',
                'email' => 'required|unique:users,email',
                'mobile_number' => 'required|unique:users,mobile_number',
                'registration_number' => 'required',
                'experience' => 'required',
                'about_info' => 'required',
               /* 'college[]' => 'required',
                'subject[]' => 'required',
                'woking_place_name[]' => 'required',
                'specializations[]' => 'required',*/
                'image' => 'required',
                'signature' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 422);
            } else {
                /*Save Doctor Info ============================*/
                if ($request->hasFile('image')) {
                    $folder_name = 'doctor-profile';
                    $doctorprofile = $this->fileUpload($request->file('image'), false, $folder_name);
                }
                if ($request->hasFile('signature')) {
                    $folder_name    = 'signatureimage';
                    $signatureimage = $this->fileUpload($request->file('signature'), false, $folder_name);
                }
                $save =  new User();
                $save->name = $request->first_name . ' ' . $request->last_name;
                $save->first_name = $request->first_name;
                $save->last_name = $request->last_name;
                $save->email = $request->email;
                $save->mobile_number = $request->mobile_number;
                $save->pin_code      = $request->pin_code;
                $save->degree_name      = $request->degree_name;
                $save->registration_number      = $request->registration_number;
                $save->image                   = $doctorprofile;
                $save->role = 'doctor';
                $status  = $save->save();
                $last_user_id = $save->id;
                /*Doctore Info*/
                $data['signature_image'] = $signatureimage;
                $data['about_info']      = $request->about_info;
                $data['experience'] =      $request->experience;
                $data['user_id']         = $last_user_id;
                Doctor::create($data);
                /*Save Doctor Eduction Details ============================*/
                $subject    = $request->get('subject');
                $college    = $request->get('college');
                $start_year = $request->get('start_year_edu');
                $end_year   = $request->get('end_year_edu');
                $data = [];
                foreach ($request->college as $key => $value) {
                    $data['subject']      = $subject[$key];
                    $data['college_name'] = $college[$key];
                    $data['start_year']   = $start_year[$key];
                    $data['end_year']     = $end_year[$key];
                    $data['doctor_id']    = $last_user_id;
                    DoctorEducationInfo::create($data);
                }
                /*Save Work & Experience ============================*/

                $woking_place_name    = $request->get('woking_place_name');
                $start_year_work = $request->get('start_year_work');
                $end_year_work   = $request->get('end_year_work');
                $datainfo = [];
                foreach ($request->woking_place_name as $key => $value) {
                    $datainfo['woking_place_name']      = $woking_place_name[$key];
                    $datainfo['start_year']            = $start_year_work[$key];
                    $datainfo['end_year']               = $end_year_work[$key];
                    $datainfo['doctor_id']              = $last_user_id;
                    DoctorWorkInfo::create($datainfo);
                }
                /*specializations Info ============================*/

                $specializations    = $request->get('specializations');
                //echo "<pre>";print_r($request->specializations);die;
                $dataval = [];
                foreach ($request->specializations as $key => $value) {
                    $dataval['specializations_name']     = $specializations[$key];
                    $dataval['doctor_id']              = $last_user_id;
                    DoctorSpecializationsInfo::create($dataval);
                }
                if ($status) {
                    $response['status'] = "success";
                    $response['message'] = "your account has been created successfully.";
                    $response['url']     = route('admin.doctor.list');
                    return json_encode($response);
                } else {
                    $response['status'] = "error";
                    $response['message'] = "There was an issue account createing the recode. Please try again.";
                    return json_encode($response);
                }
            }
            //DB::commit();   
            return json_encode($response);
            /* }catch(\Exception $e)
        {
          // DB::rollback(); 
           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::where('id', $id)->first();
        //echo "<pre>"; print_r($data);die;

        $data_doctor  = Doctor::where('user_id', $id)->first();
        //echo "<pre>"; print_r($data_doctor);die;
        $data_doctor_education_info = DoctorEducationInfo::where('doctor_id', $id)->get();
        //echo "<pre>"; print_r($data_doctor_education_info);die;
        $data_doctor_specializations_info = DoctorSpecializationsInfo::where('doctor_id', $id)->get();
        //echo "<pre>"; print_r($data_doctor_specializations_info);die;
        $data_doctor_work_info = DoctorWorkInfo::where('doctor_id', $id)->get();
        //echo "<pre>"; print_r($data_doctor_work_info);die;
        return view('backend.doctor.show', compact('data', 'data_doctor', 'data_doctor_education_info', 'data_doctor_specializations_info', 'data_doctor_work_info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id', $id)->first();
        //echo "<pre>"; print_r($data);die;

        $data_doctor  = Doctor::where('user_id', $id)->first();
        //echo "<pre>"; print_r($data_doctor);die;
        $data_doctor_education_info = DoctorEducationInfo::where('doctor_id', $id)->get();
        //echo "<pre>"; print_r($data_doctor_education_info);die;
        $data_doctor_specializations_info = DoctorSpecializationsInfo::where('doctor_id', $id)->get();
        //echo "<pre>"; print_r($data_doctor_specializations_info);die;
        $data_doctor_work_info = DoctorWorkInfo::where('doctor_id', $id)->get();
        //echo "<pre>"; print_r($data_doctor_work_info);die;

        return view('backend.doctor.edit', compact('data', 'data_doctor', 'data_doctor_education_info', 'data_doctor_specializations_info', 'data_doctor_work_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        
        // echo "<pre>"; print_r($request->all()); die('hh');
        //$data = $request->all();die;
        // {
        //  DB::beginTransaction();
        //  try{   

          $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'pin_code' => 'required',
                'email' => 'required|email',
                'mobile_number' => 'required',
                'registration_number' => 'required',
                'experience' => 'required',
                'about_info' => 'required',
                //'college[]' => 'required',
                //'subject[]' => 'required',
                //'woking_place_name[]' => 'required',
                //'specializations[]' => 'required',
                //'image' => 'required',
                //'signature' => 'required',
                'password' => 'required|confirmed|min:6',
                'password_confirmation'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        } else {
            /*Save Doctor Info ============================*/
            if ($request->hasFile('image')) {
                $folder_name = 'doctor-profile';
                $save['image'] = $this->fileUpload($request->file('image'), false, $folder_name);
            }

            $save['name'] = $request->first_name . ' ' . $request->last_name;
            $save['first_name'] = $request->first_name;
            $save['last_name'] = $request->last_name;
            $save['email'] = $request->email;
            $save['mobile_number'] = $request->mobile_number;
            $save['pin_code']      = $request->pin_code;
            $save['degree_name']      = $request->degree_name;
            $save['registration_number']      = $request->registration_number;
            $data['password']= Hash::make($request->password);
            // $save['image']                   = $doctorprofile;
            $save = User::findOrFail($id)->update($save);

            /*Doctore Info*/

            if ($request->hasFile('signature')) {
                $folder_name    = 'signatureimage';
                $signature_image = $this->fileUpload($request->file('signature'), false, $folder_name);
            }

            $about_info      = $request->about_info;
            $experience =      $request->experience;

            if (!empty($signature_image) && !empty($about_info) && !empty($experience)) {
                $save = Doctor::where("user_id", $id)->update(['signature_image' => $signature_image, 'about_info' => $about_info, 'experience' => $experience]);
            } elseif (!empty($signature_image)) {
                $save = Doctor::where("user_id", $id)->update(['signature_image' => $signature_image]);
            } elseif (!empty($about_info)) {
                $save = Doctor::where("user_id", $id)->update(['about_info' => $about_info]);
            } elseif (!empty($experience)) {
                $save = Doctor::where("user_id", $id)->update(['experience' => $experience]);
            }


            /*Save Doctor Eduction Details ============================*/
            //  $id_edu =DoctorEducationInfo::where('doctor_id',$id)->get();

            // print_r($request->get('education_id'));die;
            $education_id = $request->get('education_id');
            $subject    = $request->get('subject'); //store all column value of subject 
            $college    = $request->get('college');
            $start_year = $request->get('start_year_edu');
            $end_year   = $request->get('end_year_edu');

            //print_r($subject);die;

            // echo "<pre>"; print_r($request->college);
            // echo "<pre>"; print_r($subject[$key]);die;

            $data = [];
            foreach ($request->college as $key => $value) {
                if (!empty($education_id[$key])) {
                    // echo "<pre>";print_r($value);die;
                    $data['subject']      = $subject[$key];  //subject k row
                    $data['college_name'] = $college[$key];
                    $data['start_year']   = $start_year[$key];
                    $data['end_year']     = $end_year[$key];
                    $id_education         = $education_id[$key];
                    //$data['doctor_id']    = $last_user_id;

                    // DoctorEducationInfo::create($data);
                    // echo "<pre>"; print_r($request->college);die;
                    // echo "<pre>";print_r($request->college);die;
                    $save = DoctorEducationInfo::where("id", $id_education)->update(['subject' => $data['subject'], 'college_name' => $data['college_name'], 'start_year' => $data['start_year'], 'end_year' => $data['end_year']]);
                } else {
                    $data = [];
                    // foreach ($request->college as $key => $value) {

                    //     if (empty($education_id[$key])) {
                            $data['subject']      = $subject[$key];
                            $data['college_name'] = $college[$key];
                            $data['start_year']   = $start_year[$key];
                            $data['end_year']     = $end_year[$key];
                            $data['doctor_id']    = $id;
                            DoctorEducationInfo::create($data);
                    //     }
                    // }
                }
            }



            // echo "<pre>";print_r($save);die;
            // DoctorEducationInfo::create($save);  

            /*Save Work & Experience ============================*/
            $working_id          = $request->get('work_id');
            // echo "<pre>"; print_r($working_id);die;
            $woking_place_name    = $request->get('woking_place_name');
            $start_year_work = $request->get('start_year_work');
            $end_year_work   = $request->get('end_year_work');
            $datainfo = [];
            foreach ($request->woking_place_name as $key => $value) {
                if (!empty($working_id[$key])) {
                    $datainfo['woking_place_name']      = $woking_place_name[$key];
                    $datainfo['start_year']            = $start_year_work[$key];
                    $datainfo['end_year']               = $end_year_work[$key];
                    // echo "<pre>"; print_r($working_id[$key]);die;
                    $working_ids                         = $working_id[$key];
                    //$datainfo['doctor_id']              = $last_user_id;

                    $save = DoctorWorkInfo::where("id", $working_ids)->update(['woking_place_name' => $datainfo['woking_place_name'], 'start_year' => $datainfo['start_year'], 'end_year' => $datainfo['end_year']]);
                } else {
                    $datainfo = [];
                    // foreach ($request->woking_place_name as $key => $value) {
                    //     if (empty($working_id[$key])) {
                            $datainfo['woking_place_name']      = $woking_place_name[$key];
                            $datainfo['start_year']            = $start_year_work[$key];
                            $datainfo['end_year']               = $end_year_work[$key];
                            $datainfo['doctor_id']              = $id;
                            DoctorWorkInfo::create($datainfo);
                    //     }
                    // }
                }
            }

            //echo "<pre>"; print_r($save); die;
           
            /*specializations Info ============================*/
            $specializations_id = $request->get('spec_id'); 
            $specializations    = $request->get('specializations');
            //echo "<pre>";print_r($request->specializations);die;
            $dataval = [];
            foreach ($request->specializations as $key => $value) {
                if(!empty($specializations_id[$key])){
                $dataval['specializations_name']     = $specializations[$key];
                $special_id                          = $specializations_id[$key];
                //$dataval['doctor_id']              = $last_user_id;
                //DoctorSpecializationsInfo::create($dataval);
                $save = DoctorSpecializationsInfo::where("id", $special_id)->update(['specializations_name' => $dataval['specializations_name']]);
                //echo "<pre>";print_r($save);die; 
                }else{
                    $dataval = [];
                    // foreach ($request->specializations as $key => $value) {
                        // if(empty($specializations_id[$key])){
                        $dataval['specializations_name']     = $specializations[$key];
                        $dataval['doctor_id']              = $id;
                        DoctorSpecializationsInfo::create($dataval);
                        // }
                    // }

                }
            }


            if ($save) {
                $response['save'] = "success";
                $response['message'] = "your account has been updated successfully.";
                $response['url']     = route('admin.doctor.list');
                return json_encode($response);
            } else {
                $response['save'] = "error";
                $response['message'] = "There was an issue account createing the record. Please try again.";
                return json_encode($response);
            }
            // }
            //DB::commit();   
            return json_encode($response);
            /* }catch(\Exception $e)
        {
          // DB::rollback(); 
           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        }
    }

    //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Doctor';
        $confirm_route = $error = null;
        $confirm_route = route('admin.doctor.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
        $data = User::find($id);
        $data->forceDelete();
        $data = Doctor::where('user_id', $id);
        $data->forceDelete();
        $data  = DoctorEducationInfo::where('doctor_id', $id);
        $data->forceDelete();
        $data  = DoctorSpecializationsInfo::where('doctor_id', $id);
        $data->forceDelete();
        $data  = DoctorWorkInfo::where('doctor_id', $id);
        $status =  $data->forceDelete();

        if ($status) {
            request()->session()->flash('success', 'doctor successfully deleted');
            return redirect()->route('admin.doctor.list');
        } else {
            request()->session()->flash('error', 'Error while deleting vendor');
        }
        return redirect()->route('admin.doctor.list');
    }

    //Here is the call status model view page ------ 
    public function status_confirm($status, $id)
    {
        if ($status == '1') {
            $status = 'Inactive';
        } else {
            $status = 'Active';
        };
        $route_link = route('admin.doctor.updatestatus', [$status, $id]);
        return view('backend/layouts/active-inactive', compact('route_link', 'status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status, $id)
    {
        if ($status == 'Active') {
            $status = '1';
        } else {
            $status = '0';
        };
        $status = User::where('id', $id)->update(['status' => $status]);

        if ($status) {
            Session::flash('success', 'Status Updated Successfully.');
        } else {
            Session::flash('error', 'Somthing went wrong.');
        }
        return Redirect()->route('admin.doctor.list');
    }

  public function accepted_orders(Request $request,$id)
    { 
         
/*         try{
*/            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
 
                ## Custom Field value
                //$order_status = $request->get('order_status');
                // Total records
                $totalRecords = Order::select('count(*) as allcount')->count();
               // echo $totalRecords; die();
                 $order= Order::select('count(*) as allcount','order.order_id')->with(['User','OrderPayment','OrderItem'])->where('doctor_id',$id);
               
                $totalRecordswithFilter=$order->count();
               // Fetch records
               $records = Order:: 
                   with(['User','OrderPayment','OrderItem'])
                 ->select('*')
                 ->skip($start)
                ->take($rowperpage)
                ->whereHas('OrderItem', function($q) use ($id) {
                $q->where('doctor_id',$id);
               });
                $records=$records->get();
                //echo "<pre>"; print_r($records);die;

                $data_arr = [];
                foreach($records as $key=> $val){
                   // $delete_url = URL::to("banner/$val->id/confirm-delete");
                    $Item_view_url = URL::to("admin/doctor/view-item/$val->id");
                   // $view_invoice = URL::to("admin/order/invoice/$val->order_id");
                     if($val->shipping_amount<=0){
                        $shipping_amount = "Free";
                    }else{ 
                        $shipping_amount = '₹'.$val->shipping_amount;
                    }
                    /*$status = '<span class="badge badge-success">'.ucwords($val->status).'</span>';*/

                    $data_arr[] = array( 
                        'order_code'=>$val->order_code,
                        'user_name'=>ucwords($val->user->name),
                        'payment_method'=>ucwords($val->orderpayment->payment_method ?? ''),
                        'sub_total'=>'₹'.$val->sub_total,
                        'shipping_amount'=>$shipping_amount,
                        'total_payed_amount'=>$val->total_payed_amount,
                        'discount_coupon_code'=>isset($val->discount_coupon_code)?$val->discount_coupon_code:'==',
                        'status'=>Helper::$order_status_doctor[$val->order_status],
                        'created_at'=> date('d F,Y', strtotime($val->created_at)),
                         'action'=>'<a title="View Details " class="btn btn-info" href="'.$Item_view_url.'">View Item </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" =>  $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
              
             $my_order_id = $id;
            return view('backend.doctor.doctor-order',compact('my_order_id'));
       
      /*  } catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    }

    public function order_show($id=null)
    {
     $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$id)->first();
      return view('backend.doctor.order-details',compact('order'));
    }


}
