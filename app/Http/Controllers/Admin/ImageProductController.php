<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Redirect;
use View;
use App\Models\Product;
use App\Models\ImageProduct;
use Illuminate\Support\Str;

use Storage;
use Datatables;
use File;
use Response;
use DB;

class ImageProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       public function save(Request $request,Product $product)
    {
            if ($file = $request->file('image'))
            {
                $fileName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $folderName = '/product/'.$product->id.'/';
                $safeName = Str::random(10) . '.' . $extension;
                Storage::disk('uploads')->putFileAs($folderName, $file,$safeName);
                $product_image = $safeName;
            }

        if($request->get('order')!=""){
         $order = $request->get('order');
        }
        else{
            $maxOrder = ImageProduct::select('order')->orderBy('pro_img_id','DESC')->first();
            if(count($maxOrder)!=0){
                $order = $maxOrder->order+1;
            }
            else{
                $order = 1;
            }
        }
        if($request->get('set_primary')=="Yes"){
            $removePrimaryStatus =  DB::table('product_image')->where('product_id',$product->id)->update(['set_primary'=>'No']);
                $set_primary = 'Yes';
            }
        
            else{
                $removePrimaryStatus =  DB::table('product_image')->where('product_id',$product->id)->count();
                if($removePrimaryStatus==0){
                    $set_primary    = 'Yes';
                }
                else{
                    $set_primary    = 'No';
                }
            }

        $imagedata = new ImageProduct();
        $imagedata->product_id = $product->id;
        $imagedata->image      = $product_image;
        $imagedata->order      = $order; 
        $imagedata->set_primary= $set_primary; 
        $imagedata->save();
        $failure= false;
        $success_message ="Image Uploaded succesfully";

        if($request->ajax())
            {
                if($failure)
                {

                    $data['success']=false;
                    $data['message']=$errorMsg;
                }
                else
                {
                    $data['status']='success';
                    $data['success']=true;
                    $data['resetform']=true;
                    $data['message'] = $success_message;
                    $data['callback_type'] = 'Image data show';

                }
                $data['ajaxPageCallBack'] = true;
                $data['slideToThisForm'] = false;
                echo json_encode($data);die;
            }
    }

    public function savecaptionProductIMG(Request $request)
     {
    // echo "savecaptionProductIMG"; die();
      //  echo "<pre>"; print_r($request->all()); die();
        $caption = $request->get('caption');
        $id = $request->get('id');
     $image_detail= DB::table('product_image')->select('pro_img_id','set_primary','image','caption','product_id')->where('pro_img_id',$id)->first();
    $updatecaption =  ImageProduct::select('pro_img_id','image','caption','product_id')->where('pro_img_id',$id)
    ->update(['caption'=>$caption]);

    if($updatecaption){

     $data['status']='success';
     $data['success']=true;
      $data['redirect']=false;
     $data['message']="Update Caption Successfully.";

    // return Response::json($content);
     echo json_encode($data);die;

     }

     }

 public function setprimaryProductIMG(Request $request)
     {
       // echo "setprimaryProductIMG"; die();
        $id =$request->get('id');
     $image_detail= DB::table('product_image')->select('pro_img_id','set_primary','image','caption','product_id')->where('pro_img_id',$id)->first();
    $removePrimaryStatus =  DB::table('product_image')->where('product_id',$image_detail->product_id)->update(['set_primary'=>'No']);

    $updatePrimaryStatus =  ImageProduct::select('pro_img_id','image','caption','product_id')->where('pro_img_id',$id)
    ->update(['set_primary'=>'Yes']);

    if($updatePrimaryStatus){

     $data['status']='success';
     $data['success']=true;
      $data['redirect']=false;
     $data['message']="Image Set Primary Successfully.";

    // return Response::json($content);
     echo json_encode($data);die;

     }

     }



    public function imagedata(Product $product)
   {

       $productimage = ImageProduct::select(['pro_img_id','image','set_primary','caption','product_id'])->orderBy('pro_img_id', 'desc')->where('product_id', $product->id)->get();


       return View('backend.product.productimages',compact('productimage'));
       }

        public function destroyProductIMG($id)
     {

     $image_detail= DB::table('product_image')->select('pro_img_id','image','caption','product_id')->where('pro_img_id',$id)->first();


    $product_id     =  $image_detail->product_id;
    $product_image  =            $image_detail->image;
    $folderName     = '/uploads/product/'. $product_id;
    $my =   public_path() . $folderName .'/'.  $product_image;

    if (File::exists($my )) {
        File::delete($my );
        }

    $delete_image =  DB::table('product_image')->select('pro_img_id','image','caption','product_id')->where('pro_img_id',$id)->delete();

    if($delete_image){

     $data['status']='success';
     $data['success']=true;
      $data['redirect']=false;
     $data['message']="Image deleted Successfully.";
     echo json_encode($data);die;

     }

     }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
