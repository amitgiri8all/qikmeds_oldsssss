<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;
 use App\Models\User;
use App\Traits\ImageTrait;
use DataTables;
use Session;
use Response;
use DB;
use URL;
class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Page::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Page::select('count(*) as allcount')->where('title', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Page::orderBy($columnName,$columnSortOrder)
                ->where('page.title', 'like', '%' .$searchValue . '%')
                ->select('page.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/page/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/page/edit/$val->id");
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                     if($val->show_in_footer==1){ $show_in_footer = "<span class='badge badge-success'>Yes</span>";}else{$show_in_footer = "<span class='badge badge-danger'>No</span>";};
                      
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'title'=>$val->title,
                        'description'=>mb_strimwidth($val->description, 0, 97, '...'),
                        'show_in_footer'=>$show_in_footer,
                        'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" style="margin: 1px;" href="'.$edit_url.'"><i class="fa fa-edit"></i> Edit</a>

                          <a data-toggle="modal" style="margin:2px;" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.page.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> '.$status.' </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.page.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {
        $data= $request->all();
        $data['title'] = $request->name;
        $status = Page::create($data);
        if($status){
            return redirect('admin/page/list')->with('success', trans('You have been successfully create page'));
            request()->session()->flash('success','You have been successfully create page');
        }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Page::where('id',$id)->first();  
      return view('backend.page.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try {   
       $data = $request->all();
      if($request->get('show_in_footer')){
        $data['show_in_footer']='1';
      }else{
        $data['show_in_footer']='0';
      }
      $data =Page::findOrFail($id)->update($data);
       if($data){
            return redirect('admin/page/list')->with('success', trans('You have been successfully update page'));
         }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
     public function getModalDelete($id = null)
    {
        $model = 'Page';
        $confirm_route = $error = null;
        $confirm_route = route('admin.page.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
        $page=Page::find($id);
        $status=$page->delete();
        if($status){
        request()->session()->flash('success','page successfully deleted');
         return redirect()->route('admin.page.list');
        }
        else{
        request()->session()->flash('error','Error while deleting page');
        }
        return redirect()->route('admin.page.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.page.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       //echo $status; die();
       $status = Page::where('id',$id)->update(['status'=>$status]);
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.page.list');
     }
}
