<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\FaqTopics;
use App\Traits\ImageTrait;

use response;
use Session;
use DB;
use Hash;
use URL;


class FaqtopicsController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page

                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');

                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value

                // Total records
                $totalRecords = FaqTopics::select('count(*) as allcount')->count();
                $totalRecordswithFilter = FaqTopics::select('count(*) as allcount')
                ->where('name', 'like', '%' .$searchValue . '%')
                ->count();
                // Fetch records
                $records = FaqTopics::orderBy($columnName, $columnSortOrder)
                ->where('name', 'like', '%' .$searchValue . '%')
                    ->select('faq_topics.*')
                    ->skip($start)
                    ->take($rowperpage)
                    ->get();
                //echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach ($records as $key => $val) {
                    //foreach($val->sub_category as $sub_category){
                    $edit_url = URL::to("admin/faqtopics/edit/$val->id");
                    $delete_url = URL::to("admin/faqtopics/$val->id/confirm-delete");
                    $view_url = URL::to("admin/faqtopics/show/$val->id");
                    if ($val->status == 1) {
                        $btnclass = "btn-success";
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    } else {
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
 
                    $data_arr[] = array(
                        'id' => $val->id,
                        'name' => $val->name,
                        'created_at' =>date('d F,y',strtotime($val->created_at)),

                        'action' => '<a class="btn btn-info" href="' . $edit_url . '"><i class="fa fa-edit"></i>Edit</a>
                        

                         <a data-toggle="modal" data-target="#delete_confirm" href="' . $delete_url . '" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>
                         

                        
                          </a>'
                    );
                    // }  
                } //Close foreach loop
                $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                );

                echo json_encode($response);
                die;
            }

            return view('backend.faqtopics.index');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            Session::flash('error', $msg);
            return redirect()->back()->withInput();
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        return view('backend.faqtopics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { {

           //echo "<pre>";print_r($request->all());die;
            //  DB::beginTransaction();
            //  try{   

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 422);
            } else {
                /*Save topic Info ============================*/
                $save =  new FaqTopics();
                $save->name = $request->name;
                $status  = $save->save();
              //echo  $last_user_id = $save->id; die;
                
                if ($status) {
                    $response['status'] = "success";
                    $response['message'] = "your Topic name has been created successfully.";
                    $response['url']     = route('admin.faqtopics.list');
                    return json_encode($response);
                } else {
                    $response['status'] = "error";
                    $response['message'] = "There was an issue account createing the recode. Please try again.";
                    return json_encode($response);
                }
            }
            //DB::commit();   
            return json_encode($response);
            /* }catch(\Exception $e)
        {
          // DB::rollback(); 
           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = FaqTopics::where('id', $id)->first();
        //echo "<pre>"; print_r($data);die;
        //$order_data = Order::where('user_id',$id)->get();
        //echo "<pre>"; print_r($order_data);die;
        return view('backend.faqtopics.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   

        $data = FaqTopics::where('id', $id)->first();
        //echo "<pre>"; print_r($data);die;
        return view('backend.faqtopics.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
            'name' => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        } else {
     // echo "<pre>"; print_r($request->all()); die();
     // try {  
       $topic = $request->all();

        $data['name'] = $request->name;

        $data =FaqTopics::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.faqtopics.list');
                    $response['message'] = "Faq Topics has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully update Faq Topics');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
      }
       
    }


    //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Faqtopics';
        $confirm_route = $error = null;
        $confirm_route = route('admin.faqtopics.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
        $data = FaqTopics::find($id);
        $status =  $data->delete();

        if ($status) {
            request()->session()->flash('success', 'Faq Topics successfully deleted');
            return redirect()->route('admin.faqtopics.list');
        } else {
            request()->session()->flash('error', 'Error while deleting topic');
        }
        return redirect()->route('admin.faqtopics.list');
    }

    //Here is the call status model view page ------ 
    public function status_confirm($status, $id)
    {
        if ($status == '1') {
            $status = 'Inactive';
        } else {
            $status = 'Active';
        };
        $route_link = route('admin.faqtopics.updatestatus', [$status, $id]);
        return view('backend/layouts/active-inactive', compact('route_link', 'status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status, $id)
    {
        if ($status == 'Active') {
            $status = '1';
        } else {
            $status = '0';
        };
        $status = FaqTopics::where('id', $id)->update(['status' => $status]);

        if ($status) {
            Session::flash('success', 'Status Updated Successfully.');
        } else {
            Session::flash('error', 'Somthing went wrong.');
        }
        return Redirect()->route('admin.faqtopics.list');
    }
}
