<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\GeneralRequest;
use App\Http\Requests\PaymentRequest;
use DB;
use Redirect;
use Validator;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try { 
         $setting = DB::table('settings')->select('*')->where('id',1)->first();   
         $site = DB::table('site_settings')->select('*')->where('id',1)->first();   
         $payments = DB::table('payments')->select('*')->where('id',1)->first();   
         $push_notification = DB::table('push_notification_message')->select('*')->where('id',1)->first();   
        } catch(QueryException $ex){ 
            dd($ex->getMessage()); 
        }
       return view('backend.setting.create',compact('setting','site','payments','push_notification'));
    }

      //Update All General Settings ----------- 
    public function update_general_setting(Request $request){

       //echo "<pre>"; print_r($request->all()); die();
         $data = request()->except(['_token']);
         $setting = DB::table('settings')->select('*')->where('id',1)->first();   
         $image = $setting->app_logo;
        //echo $image; die();
    $validator = Validator::make($request->all(), [
            'app_name' => 'required',
            'app_env' => 'required',
            'app_debug' => 'required',
            'app_log_level' => 'required',
            'app_url' => 'required||url',
            'mail_driver' => 'required',
            'mail_host' => 'required',
            'mail_username' => 'required',
            'mail_password' => 'required',
            'mail_encryption' => 'required',
            'mail_from_address' => 'required',
            'mail_from_name' => 'required',
            'app_url_android' => 'required|url',
            'app_url_ios' => 'required|url',
            'under_maintenance' => 'required',
            'timezone' => 'required',
            'pagination_limit' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'currency' => 'required',
            //'app_logo' => 'required',
        ]); 
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{      
         if($request->hasFile('app_logo')){
             $image = $request->file('app_logo');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $request->file('app_logo')->storeAs(
                'public/upload/image/', $imageName
            );
            $data['app_logo']=$imageName;
         } else{
            $data['app_logo']=$image;
         }   
        // echo "<pre>"; print_r($data); die();
         $result = DB::table('settings')->where('id',1)->update($data);
         if($result)
         {  

            $response['status'] = "success";
           // $response['message'] = "General Setting Update Successfully.";

            return redirect()->to('/route')->with('success', 'Updated successfully!');
            

         } else {
            $response['status'] = "error";
            $response['message'] = "There was an issue updateing the general setting. Please try again.";
            return redirect()->to('/route');
          }  
       }   
         return json_encode($response);


    }


    //Update All Site Settings ----------- 
    public function update_site_setting_setting(Request $request){
            $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'whats_up' => 'required',
            'facebook' => 'required',
            'twitter' => 'required',
            'instagram' => 'required',
            'linkedin' => 'required',
            'youtube' => 'required',
            'near_by_queue_time' => 'required',
            'city_queue_time' => 'required',
            ]);         
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{     
           $data = request()->except(['_token']);
         $result = DB::table('site_settings')->where('id',1)->update($data);
         if($result)
         {
            $response['status'] = "success";
            //$response['message'] = "Site Setting Update Successfully";

            return redirect()->to('/route')->with('success', 'Updated successfully!');
         } else {
            $response['status'] = "error";
            $response['message'] = "There was an issue updateing the general setting. Please try again.";
            return redirect()->to('/route');
         }  
       }  
                return json_encode($response);

    }


    //Update All Payment Settings ----------- 
    public function update_payment_setting(Request $request){
          $validator = Validator::make($request->all(), [
            'active_payment_page' => 'required',
            'cash_on_delivery' => 'required',
            'wallet' => 'required',
            'credit_card' => 'required',
            'paypal' => 'required',
            'stripe_secret_key' => 'required',
            'stripe_public_key' => 'required',
            'paypal_account_email' => 'required||email',
            'paypal_currency' => 'required',
            ]);         
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{     
            $data = request()->except(['_token']);
            $result = DB::table('payments')->where('id',1)->update($data);
              if($result)
             {
                $response['status'] = "success";
                $response['message'] = "Site Setting Update Successfully";
                return redirect()->to('/route')->with('success', 'Updated successfully!');;
             } else {
                $response['status'] = "error";
                $response['message'] = "There was an issue updateing the general setting. Please try again.";
                return redirect()->to('/route');
             }  
       }  
        return json_encode($response);

    }

     
    public function update_push_notification(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'order_accept' => 'required',
            'order_place' => 'required',
            'order_cancel' => 'required',
            'order_return' => 'required',
            'order_shipped' => 'required',
            'order_deliverd' => 'required'
           
            ]);         
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{     
            $data = request()->except(['_token']);
            $result = DB::table('push_notification_message')->where('id',1)->update($data);
              if($result)
             {
                $response['status'] = "success";
                $response['message'] = "Push Notification Message Update Successfully";
                return redirect()->to('/route')->with('success', 'Updated successfully!');;
             } else {
                $response['status'] = "error";
                $response['message'] = "There was an issue updateing the general setting. Please try again.";
                return redirect()->to('/route');
             }  
       }  
        return json_encode($response);
    }
    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
