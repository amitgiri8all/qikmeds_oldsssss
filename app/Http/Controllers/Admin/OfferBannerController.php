<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OfferBanner;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\Product;
use Illuminate\Http\Request;
use Validator;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;

class OfferBannerController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = OfferBanner::select('count(*) as allcount')->count();
                $totalRecordswithFilter = OfferBanner::select('count(*) as allcount')
                ->Where('title', 'like', '%' .$searchValue . '%')
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = OfferBanner::orderBy($columnName,$columnSortOrder)
                 ->Where('title', 'like', '%' .$searchValue . '%')
                ->select('offer_banner.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
             // echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/offer-banner/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/offer-banner/edit/$val->id");
                     $view_url = URL::to("admin/offer-banner/show/$val->id");
                   // $view_url = URL::to("admin/offer-banner/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                   if(!empty($val->image)){
                    $url=$val->image;
                   }   
                    $data_arr[] = array( 
                        'id'=>$val->id,
                       
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'title'=>$val->title,
                         'sub_title'=>$val->sub_title,
                         'description'=>substr($val->description,0, 50),
                         'eligibility'=> substr($val->eligibility,0, 50),
                         'how_get_it'=>substr($val->how_get_it,0, 50),
                         'condition_offer'=>substr($val->condition_offer,0, 50),
                         'term_condition'=>substr($val->term_condition,0, 50),
                         'code'=>$val->code,
                         'offer_type'=>$val->offer_type,
                         'minimum_amount'=>$val->minimum_amount,
                         'price'=>$val->price,
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" href="'.$edit_url.'"><i class="fa fa-edit"></i>Edit </a>
                        <a class="btn btn-primary" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.offer-banner.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.offerbanner.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category_id = DB::table('categories')->where('parent_id','=',0)->get();
       // echo "<pre>";print_r($category_id);die;
        return view('backend.offerbanner.create',compact('category_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all() , ['title' => 'required','sub_title'=>'required','image'=>'required','price'=>'required','code'=>'required|max:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/','description'=>'required','eligibility'=>'required','how_get_it'=>'required','condition_offer'=>'required','term_condition'=>'required','offer_type'=>'required','minimum_amount'=>'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
                 $offer_banner = new OfferBanner();

                if($request->hasFile('image')){
                $folder_name ='image';
                $offer_banner['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
                }
                $offer_banner->title = $request->title;
                $offer_banner->sub_title = $request->sub_title;
                $offer_banner->price = $request->price;
                $offer_banner->offer_type = $request->offer_type;
                $offer_banner->minimum_amount = $request->minimum_amount;
                $offer_banner->code = $request->code;
                $offer_banner->description = $request->description;
                $offer_banner->eligibility = $request->eligibility;
                $offer_banner->how_get_it = $request->how_get_it;
                $offer_banner->condition_offer = $request->condition_offer;
                $offer_banner->term_condition = $request->term_condition;
                $data = $offer_banner->save();
               
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.offer-banner.list');
                    $response['message'] = "Offer Banner has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Offer banner');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = OfferBanner::where('id',$id)->first();
      // $data_vendor = Vendor::where('user_id',$id)->first();
      return view('backend.offerbanner.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = OfferBanner::where('id',$id)->first();
        $category_id = DB::table('categories')->where('parent_id','=',0)->get();
        return view('backend.offerbanner.edit',compact('data','category_id'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['title' => 'required','sub_title'=>'required','price'=>'required','code'=>'required|max:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/','description'=>'required','eligibility'=>'required','how_get_it'=>'required','condition_offer'=>'required','term_condition'=>'required','offer_type'=>'required','minimum_amount'=>'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
     // try {  
       $offerbanner = $request->all();
       //echo "<pre>"; print_r($offerbanner); die();
       if($request->hasFile('image')){
         $folder_name ='image';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['title'] = $request->title;
        $data['sub_title'] = $request->sub_title;
        $data['price'] = $request->price;
        $data['offer_type'] = $request->offer_type;
        $data['minimum_amount'] = $request->minimum_amount;
        $data['code'] = $request->code;
        $data['description'] = $request->description;
        $data['eligibility'] = $request->eligibility;
        $data['how_get_it'] = $request->how_get_it;
        $data['condition_offer'] = $request->condition_offer;
        $data['term_condition'] = $request->term_condition;


        /*if($request->link_type == 'external'){
            $data['link_type'] = $request->link_type;
            $data['link_url'] = $request->link_url;

         }elseif($request->link_type == 'internal'){
            $data['link_type'] = $request->link_type;
            $data['link_url'] = '';
            $data['cat_id'] = $request->cat_id;
            $data['sub_cat_id'] = $request->sub_cat_id;
            $data['product'] = $request->vendor_product_id;
                }*/
      
        $data =offerbanner::findOrFail($id)->update($data);

       if ($data)
                {
                $response['status'] = "success";
                $response['url'] = route('admin.offer-banner.list');
                $response['message'] = "OfferBanner has been update successfully.";
                request()->session()
                    ->flash('success', 'You have been successfully create OfferBanner');
                return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);
                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
       }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'offer-banner';
        $confirm_route = $error = null;
        $confirm_route = route('admin.offer-banner.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=OfferBanner::find($id);
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','offer-banner successfully deleted');
                    return redirect()->route('admin.offer-banner.list');
        }
        else{
            request()->session()->flash('error','Error while deleting offer-banner');
        }
        return redirect()->route('admin.offer-banner.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.offer-banner.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = OfferBanner::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.offer-banner.list');
     }

     //getSubCategory

     public function getsubcategory(Request $request){
          $catId = $request->input('id');
          //Fetch  Subcategories data from categories 
        $category= Category::where('parent_id',$catId)->whereNull('deleted_at')->get();
        $categoryData = $category->pluck('category_name','id');
        if(count($category) == 1){
            $subCatId = $category[0]->id; 
        }else{
            $subCatId =   $catId;
        }
         
        if (isset($categoryData) && !empty($categoryData)) {
            return response()->json(['status' => 'true', 'data' => $categoryData]);

        }else{
            return response()->json(['status' => 'false', 'data' => []]);
        }

     }

      public function getsubcat(Request $request){
        $catId = $request->input('id');
          //Fetch  Subcategories data from product categories 
        $category= ProductCategory::where('category_id',$catId)->whereNull('deleted_at')->first();
        //dd($category);
        $category= Product::where('id',$category->pro_cat_id)->whereNull('deleted_at')->get();
        $categoryData = $category->pluck('medicine_name','id');

        if(count($category) == 1){
            $subCatId = $category[0]->id; 
        }else{
            $subCatId =   $catId;
        }
         
        if (isset($categoryData) && !empty($categoryData)) {
            return response()->json(['status' => 'true', 'data' => $categoryData]);

        }else{
            return response()->json(['status' => 'false', 'data' => []]);
        }

     }



}

