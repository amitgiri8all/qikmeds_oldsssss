<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ReferEarn;
use Illuminate\Http\Request;
use Validator;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;
class ReferEarnController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
       // try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = ReferEarn::select('count(*) as allcount')->count();
                $totalRecordswithFilter = ReferEarn::select('count(*) as allcount')
                //->where('role','vendor')
                ->Where('title', 'like', '%' .$searchValue . '%')
               /* ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = ReferEarn::orderBy($columnName,$columnSortOrder)
                ->Where('title', 'like', '%' .$searchValue . '%')

                //->where('role','vendor')
                // ->orWhere('users.name', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('refer_earn.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/refer-earn/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/refer-earn/edit/$val->id");
                    $view_url = URL::to("admin/refer-earn/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }

                  if(!empty($val->image)){
                    $url= $val->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }   
                      
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'title'=>$val->title,
                        'refer_amount'=>$val->refer_amount,
                        'refer_by_amount'=>$val->refer_by_amount,
                        'description'=>$val->description,
                        'image'=>'<img src="'.$url.'" border="0" width="100" height="100" class="img-rounded" align="center" />',
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" href="'.$edit_url.'"><i class="fa fa-edit"></i>Edit </a>
                        <a class="btn btn-primary" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                        
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.refer-earn.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.referearn.index');
       /* }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.referearn.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['title' => 'required','refer_amount'=>'required','description' => 'required','refer_by_amount' =>'required', 'image'=>'required' ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
            // DB::beginTransaction();
            try
            {
                //echo "<pre>"; print_r($request->all()); die();
                $referearn = new ReferEarn();

                if($request->hasFile('image')){
                $folder_name ='image';
                $referearn['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
                }

                $referearn->title = $request->title;
                $referearn->refer_amount = $request->refer_amount;
                $referearn->refer_by_amount = $request->refer_by_amount;
                $referearn->description = $request->description;
                $data = $referearn->save();
              
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.refer-earn.list');
                    $response['message'] = "ReferEarn has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create ReferEarn');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = ReferEarn::where('id',$id)->first();
       return view('backend.referearn.show',compact('data'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = ReferEarn::where('id',$id)->first();
        return view('backend.referearn.edit',compact('data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all() , ['title' => 'required','refer_amount'=>'required','description' => 'required','refer_by_amount' =>'required']);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
        //$data = $request->all();

       if($request->hasFile('image')){
         $folder_name ='image';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['title'] = $request->title;
        $data['refer_amount'] = $request->refer_amount;
        $data['refer_by_amount'] = $request->refer_by_amount;
        $data['description'] = $request->description; 
      
        $data =ReferEarn::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.refer-earn.list');
                    $response['message'] = "Refer earn has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Refer earn');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
      }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Refer-earn';
        $confirm_route = $error = null;
        $confirm_route = route('admin.refer-earn.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=ReferEarn::find($id);
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','Refer earn successfully deleted');
                    return redirect()->route('admin.refer-earn.list');
        }
        else{
            request()->session()->flash('error','Error while deleting vendor');
        }
        return redirect()->route('admin.refer-earn.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.refer-earn.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = ReferEarn::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.refer-earn.list');
     }
}

