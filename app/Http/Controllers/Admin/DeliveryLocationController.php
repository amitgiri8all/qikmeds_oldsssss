<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DeliveryLocation;
use App\Http\Requests\DeliveryLocationRequest;
use DataTables;
use Session;
use Response;
use DB;
use URL;
class DeliveryLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index(Request $request)
    {
        //try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = DeliveryLocation::select('count(*) as allcount','brand.*')->count();
                $totalRecordswithFilter = DeliveryLocation::select('count(*) as allcount')->where('pin_code', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = DeliveryLocation::orderBy($columnName,$columnSortOrder)
                ->where('pin_code', 'like', '%' .$searchValue . '%')
                ->select('*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                 //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/delivery-location/$val->id/confirm-delete");
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                     
                      
                    $data_arr[] = array( 
                        'id'=>$val->id,
                         'pin_code'=>$val->pin_code,
                        'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href=""><i class="fa fa-edit"></i> Edit</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.delivery-location.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> '.$status.' </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
             return view('backend.deliverylocation.index');
        /*}
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
        //
    }
    public function indddex()
    {
    return view('backend.deliverylocation.index');
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $geocode = file_get_contents("http://localhost:3000/products");
        $data_decode = json_decode($geocode,true);

        return $data_decode;



        return view('backend.deliverylocation.create');
    }

    public function store(DeliveryLocationRequest $request){
         try {
        $data= $request->all();
         $status = DeliveryLocation::create($data);
        if($status){
            return redirect('admin/delivery-location/list')->with('success', trans('You have been successfully created delivery location '));
            request()->session()->flash('success','You have been successfully create delivery location');
        }
        else{
            request()->session()->flash('error','Error occurred, Please try again!');
        }
        } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }
        return back();
  }
    public function getDetailsByPinCode(Request $request){
       // try {
        $pin_code = $request->get('pin_code');
        $geocode = file_get_contents("https://api.postalpincode.in/pincode/".$pin_code);
        $data_decode = json_decode($geocode,true);
       //    echo "<pre>"; print_r($data_decode); die();
        //$state = $geocode->PostOffice[0]->State;
        //echo $state; die;
        $name = array();
        $state = '';
        $city = '';
        foreach ($data_decode[0]['PostOffice'] as $key=>$value) {
          $name[] = $value['Name'];
          $state = $value['State'];
          $city = $value['District'];
        }
        $data['AllState'] = $name;
        $data['state'] = $state;
        $data['city'] = $city;
        //echo $state; die();
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function stosre(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Delivery Location';
        $confirm_route = $error = null;
        $confirm_route = route('admin.delivery-location.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       // echo $id; die();
        $data=DeliveryLocation::find($id);
         $status=$data->delete();
        // echo $status; die();
        if($status){
            request()->session()->flash('success','Delivery location successfully deleted');
            return redirect()->route('admin.delivery-location.list');
        }
        else{
            request()->session()->flash('error','Error while deleting brand');
        }
        return redirect()->route('admin.delivery-location.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.delivery-location.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = DeliveryLocation::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.delivery-location.list');
     }
}

