<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Requests\BannerRequest;
use App\Models\User;
use App\Models\Category;
use App\Traits\ImageTrait;
use DataTables;
use Session;
use Response;
use DB;
use URL;
use Validator;



class BannerController extends Controller
{
 
 use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Banner::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Banner::select('count(*) as allcount')->where('title', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Banner::orderBy($columnName,$columnSortOrder)
                ->where('banner.title', 'like', '%' .$searchValue . '%')
                ->select('banner.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/banner/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/banner/edit/$val->id");
                    $view_url = URL::to("admin/banner/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                    /* if($val->link_type=='external'){ $show = "<span class='badge badge-success'>External</span>";}else{$show = "<span class='badge badge-danger'>Internal</span>";}*/

                    if (!empty($val->image)) {
                        $url = $val->image;
                    } else {
                        $url = $val->image;
                    }

                  /* if(!empty($val->image)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$val->image);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   } */  
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'title'=>$val->title,
                         'image' => '<img src="' . $url . '" border="0" width="150" height="120" class="img-rounded" align="center" />',
                        'link_type'=>$val->link_type,
                        'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" style="margin:1px;"  href="'.$edit_url.'"><i class="fa fa-edit"></i> Edit</a>

                        <a class="btn btn-primary" style="margin:1px;" href="' . $view_url . '"><i class="fa fa-eye"></i>View </a>

                         <a data-toggle="modal" style="margin:1px;" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.banner.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete" style="margin:1px;"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> '.$status.' </a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.banner.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
     $category = Category::select('id','category_name','parent_id')->where('parent_id',0)->get();   
     return view('backend.banner.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


      public function store(Request $request){
       

         $validator = Validator::make($request->all(), [
                        'title'     => 'required',
                        'link_type' => 'required',
                        'image'     => 'required',
                        'banner_type' => 'required',
                        'status' => 'required',
                    ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }

         else {

         //    try {
            $data= $request->all();

              if($request->hasFile('image')){
                $folder_name ='banner';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }

          // echo "<pre>"; print_r($data); die();
            $status = Banner::create($data);
            if ($status) {
                    $response['status'] = "success";
                    $response['message'] = "your banner has been created successfully.";
                    $response['url']     = route('admin.banner.list');
                    return json_encode($response);
                } else {
                    $response['status'] = "error";
                    $response['message'] = "There was an issue banner createing the recode. Please try again.";
                    return json_encode($response);
                }
           /* } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
            }*/
           // return back();
           // return redirect()->route('admin.banner.list');

        }
      }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $data = Banner::where('id', $id)->first();
        //echo "<pre>"; print_r($data);die;
        return view('backend.banner.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data =Banner::where('id',$id)->first();
        //echo "<pre>";print_r($data);die;
        return view('backend.banner.edit',compact('data'));  
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
                        'title'     => 'required',
                        'link_type' => 'required',
                        //'image'     => 'required',
                        'banner_type' => 'required',
                        'status' => 'required',
                    ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }

         else {
      
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $banner = $request->all();

       if($request->hasFile('image')){
         $folder_name ='banner';
         $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
        }

        $data['banner_type'] = $request->banner_type;
        $data['title'] = $request->title;
        $data['link_type'] = $request->link_type;
        $data['status'] = $request->status; 
        $data['city'] = $request->city;


        $data =Banner::findOrFail($id)->update($data);


       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.banner.list');
                    $response['message'] = "Banner has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Banner');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
     }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Banner';
        $confirm_route = $error = null;
        $confirm_route = route('admin.banner.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       // echo $id; die();
        $data=Banner::find($id);
         $status=$data->delete();
      // echo $status; die();
        if($status){
             
            request()->session()->flash('success','Banner successfully deleted');
                    return redirect()->route('admin.banner.list');
        }
        else{
            request()->session()->flash('error','Error while deleting category');
        }
        return redirect()->route('admin.banne.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.banner.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = Banner::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.banner.list');
     }


    public function get_sub_category(Request $request){
      $subCategories = Category::select('id','category_name','parent_id')->where('parent_id',$request->get('cat_id'))->pluck("category_name","id");
      return response()->json($subCategories);  
    }
}
