<?php

namespace App\Http\Controllers\Admin;


use App\Scopes\StatusScope;
use App\Models\User;
use App\Models\Zone;
use App\Models\ZoneTranslation;
use App\Http\Requests\ZoneRequest;
use App;
Use DB;
use App\Models\DeliveryLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\datehelper;
use Session;
use DataTables;
use Response;
use URL;
class ZoneController extends Controller
{

    protected $zone;
    protected $user;
    protected $method;
    function __construct(Request $request,Zone $zone,User $user)
    {
        $this->zone     = $zone;
        $this->user     = $user;
        $this->method   = $request->method();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $zones = [];
    //     $zones=$this->zone->selectRaw(' AsText(point) as points')->get();
    //     return view('backend.zone.index')->with('zones',$zones);
    // }


    public function index(Request $request)
    {

       // try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Zone::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Zone::select('count(*) as allcount')->where('name', 'like', '%' .$searchValue . '%')->count();
                // Fetch records
                $records = Zone::orderBy($columnName,$columnSortOrder)
                ->where('zones.name', 'like', '%' .$searchValue . '%')
                ->select('zones.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/zone/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/zone/$val->id/edit");
                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                    $status = $val->status;
                    $id = $val->id;
                     if($val->show_in_footer==1){ $show_in_footer = "<span class='badge badge-success'>Yes</span>";}else{$show_in_footer = "<span class='badge badge-danger'>No</span>";};
                      
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'name'=>$val->name,
                        'description'=>mb_strimwidth($val->description, 0, 97, '...'),
                        'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-success" href="'.$edit_url.'"><i class="fa fa-edit"></i> Edit</a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete"></i> Delete </a>

                        <a class="javascript:void(0);"><button type="button" data-status-value='.$status.' data-status-id='.$id.' class="btn '.($status==1 ? 'btn-success':'btn-warning').' status">'.($status==1 ? 'Active':'Inactive').'</button></a>
                        '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
    
            }
    
            return view('backend.zone.index');
        // }
        // catch(\Exception $e)
        // {

        //    $msg = $e->getMessage();
        //    Session::flash('error', $msg);
        //    return redirect()->back()->withInput();
        // }
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // 
    public function create()
    {
       // echo "ok"; die();
        $zones = [];
        $zones=$this->zone->selectRaw(' AsText(point) as points')->withoutGlobalScope(StatusScope::class)->get();
        return view('backend.zone.add')->with('zones',$zones);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ZoneRequest $request)
    {  
	    $inputs=  request()->except(['_token']);
	    $inputs['code']=uniqid('z');
        try {
	        $this->zone->create($inputs);
	        Session::flash('success','Zone create successful');
	    } catch (\Exception $e) {
	        Session::flash('error',$e->getMessage());
	    }
	  return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {       
        $zone=$this->zone->selectRaw('*, AsText(point) as points')->withoutGlobalScope(StatusScope::class)->findOrFail($id);
        $zones=$this->zone->selectRaw(' AsText(point) as points')->withoutGlobalScope(StatusScope::class)->where('id','!=',$id)->get();
        return view('backend.zone.edit')->with('zone',$zone)->with('zones',$zones);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
      
            try {
                $this->model->withoutGlobalScope(StatusScope::class)->FindOrFail($id)->update($input);
                $responce=response()->json([
                    'status' => true,
                    'message' => 'update'
                ],200);
            } catch (\Exception $e) {
                $responce=response()->json([
                    'status' => false,
                    'message' => $e->getMessage()
                ],200);
            }
     
            $this->zone->withoutGlobalScope(StatusScope::class)->FindOrFail($id)->update($input);
            return redirect('admin/zone/list')->with('success','Zone Update Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function getModalDelete($id = null)
    {
         $model = 'Zones Manager';
        $confirm_route = $error = null;
        $confirm_route = route('delete/zone', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }
    public function destroy($id)
    {

        $flight = $this->zone->findOrFail($id);
        $flight->delete();
        if($flight){
          return redirect('admin/zone/list')->with('success', trans('Zone Delete Successfully'));
        }else{
          return redirect('admin/zone/list')->with('error', trans('something went wrong'));
        }


    }

    /**
     * @return mixed
     */
    

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(Request $request){
        $inputs=  request()->except(['_token']);
        if($request->status_value==1){
            $status='0';
        }else{
            $status='1';
        }

        $user= $this->zone->findOrFail($request->id)->update(['status'=>$status]);
       if($user){
        return Response::json(array('success' => true,'status' => "success",'data' => $user)); 
       }else{
        return Response::json(array('success' => false,'status' => "error",'data' => $user)); 
       }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeDefault(Request $request){

        $this->zone->where(['is_default'=>'1'])->update(['is_default'=>'0']);
        $user = $this->zone->findOrFail($request->id)->update(['is_default'=>'1']);
        if($user){
            return Response::json(array('success' => true,'status' => "success",'data' => $user)); 
        }else{
            return Response::json(array('success' => false,'status' => "error",'data' => $user)); 
        }
    }

    public function getZoneDetailsById(Request $request)
    {

        $vendor= $this->user->whereRaw('FIND_IN_SET('.$request->id.', zone_id) ')->where(['user_type'=>'vendor'])->select(['name','id'])->first();
        $shoper= $this->user->whereRaw('FIND_IN_SET('.$request->id.', zone_id) ')->where(['user_type'=>'shoper'])->select(['name','id'])->first();
        $driver= $this->user->whereRaw('FIND_IN_SET('.$request->id.', zone_id) ')->where(['user_type'=>'driver'])->select(['name','id'])->first();

        return response()->json([
            'status' => true,
            'message' => 'update',
            'data'=>['driver'=>$driver,'shoper'=>$shoper,'vendor'=>$vendor]
        ],200);
    }

    public function isPointInPolygon($latitude, $longitude, $latitude_array, $longitude_array) {
    $size = count($longitude_array);
    $flag1 = false;
    $k = $size - 1;
    $j = 0;
    while ($j < $size) {
        $flag = false;
        $flag2 = false;
        $flag3 = false;
        if ($latitude_array[$j] > $latitude) {
            $flag2 = true;
        } else {
            $flag2 = false;
        }
        if ($latitude_array[$k] > $latitude) {
            $flag3 = true;
        } else {
            $flag3 = false;
        }
        $flag = $flag1;
        if ($flag2 != $flag3) {
            $flag = $flag1;
            if ($longitude < (($longitude_array[$k] - $longitude_array[$j]) * ($latitude - $latitude_array[$j])) / ($latitude_array[$k] - $latitude_array[$j]) +
                $longitude_array[$j]) {
                if (!$flag1) {
                    $flag = true;
                } else {
                    $flag = false;
                }
            }
        }
        $k = $j;
        $j++;
        $flag1 = $flag;
    }
    return $flag1;
}
     public function loadZoneByLat(Request $request)
    {
        $deliveryLocation = DeliveryLocation::findOrFail($request->id);
        $lat = $deliveryLocation->lat;
        $lng = $deliveryLocation->lng;
        //return $lat.'-----'. $lng;
        $zone_id =  0;
        $vendor = 0;
        $shoper = 0;
        $driver = 0;
        $delivery_charges = 0;
        $zone = $this->getZoneData($lat, $lng);
        //return $zone;
        //return $zone['zone_id'];

        //$zone = Zone::whereRaw('CONTAINS(point, point('.$lat.','.$lng.'))')->first();
        //return $zone;
        if(isset($zone)){
           // $zone_id =  $zone->id;
            $zone_id =  $zone['zone_id'];
            $delivery_charges =  $zone['delivery_charges'];
            $vendor= $this->user->whereRaw('FIND_IN_SET('.$zone_id.', zone_id) ')->where(['user_type'=>'vendor'])->select(['name','id'])->get();
            $shoper= $this->user->whereRaw('FIND_IN_SET('.$zone_id.', zone_id) ')->where(['user_type'=>'shoper'])->select(['name','id'])->first();
            $driver= $this->user->whereRaw('FIND_IN_SET('.$zone_id.', zone_id) ')->where(['user_type'=>'driver'])->select(['name','id'])->first();
            }
        return response()->json([
            'status' => true,
            'message' => 'update',
            'data'=>['driver'=>$driver,'shoper'=>$shoper,'vendor'=>$vendor,'zone_id'=>$zone_id,'delivery_charges'=>$delivery_charges]
        ],200);
    }
     public function getZoneData($lat, $lng)
    {
        $zone_id = '';
        $zoneArray = [];
        $zArray = [];
        $fArray = [];
        $finalArray = [];
      
        $zonedata = DB::table('zones')->select('id',DB::raw("ST_AsGeoJSON(point) as json"),'delivery_charges' )->where('deleted_at',null)->where('status','=','1')->get();
      
            $json_arr = json_decode($zonedata, true);
            foreach ($json_arr as $zvalue) {
                $zone_id=$zvalue['id'];
                $delivery_charges=$zvalue['delivery_charges'];
                $json=json_decode($zvalue['json']);
                $coordinates=$json->coordinates;
                $new_coordinates=$coordinates[0];
                $lat_array=array();
                $lng_array=array();
                foreach($new_coordinates as $new_coordinates_value){
                    $lat_array[]=$new_coordinates_value[0];
                    $lng_array[]=$new_coordinates_value[1];

                }
           
            $is_exist = $this->isPointInPolygon($lat, $lng,$lat_array,$lng_array);
           
            if($is_exist){
                $zData = ZoneTranslation::where('zone_id', $zone_id)->where('locale', App::getLocale())->first();
                $data['match_in_zone'] = true;
                $data['zone_id'] = $zone_id;
                $data['zone_name'] = $zData->name;
                $data['delivery_charges'] = $delivery_charges;
                return $data;
            }

            }
            
            $zone_id_default = 0;
            
            $zData = ZoneTranslation::where('zone_id', $zone_id_default)->where('locale', App::getLocale())->first();
            $data['match_in_zone'] = false;
            $data['zone_id'] = $zone_id_default;
            $data['delivery_charges'] = 0;
            return $data;
       
       

       
    }



}