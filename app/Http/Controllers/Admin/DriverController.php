<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\DriverSessionTime;
use App\Models\DriverBankDetails;
use App\Traits\ImageTrait;
use DataTables;
use Session;
use Response;
use DB;
use URL;
use Validator;
use Hash;

 class DriverController extends Controller
{
      use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page

                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');

                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value

                // Total records
                $totalRecords = User::select('count(*) as allcount')->where('role','driver')->count();
                $totalRecordswithFilter = User::select('count(*) as allcount')
                ->where('role','driver');
                /*->orwhere('name', 'like', '%' .$searchValue . '%')
                ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                if($searchValue!=''){ 
                $totalRecordswithFilter->where('users.name', 'like', '%' .$searchValue . '%');
                }
                $totalRecordswithFilter=$totalRecordswithFilter->count();
                // Fetch records
                $records = User::where('role','driver')
               /* ->orwhere('users.name', 'like', '%' .$searchValue . '%')
                ->orWhere('users.email', 'like', '%' .$searchValue . '%')
                ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->select('users.*')->orderBy('id','desc')
                ->skip($start)
                ->take($rowperpage);
                if($searchValue!=''){ 
                $records->where('users.name', 'like', '%' .$searchValue . '%');
                }
                $records=$records->get(); 

                //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/driver/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/driver/edit/$val->id");
                    $view_url = URL::to("admin/driver/show/$val->id");
                     if($val->status==1){
                        $btnclass = "btn-success";
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }

                  

                    $data_arr[] = array(
                        'id'=>$val->id,
                        'name'=>$val->name,
                        'email'=>$val->email,
                        'mobile_number'=>$val->mobile_number,
                        
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" style="margin:1px;" href="'.$edit_url.'"><i class="fa fa-edit">Edit</i> </a>
                        <a class="btn btn-primary" style="margin:1px;" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete" style="margin:1px;"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.driver.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="Delete" style="margin:1px;" ><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i></a>
                        '
                    );
                 // }
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );

                 echo json_encode($response); die;

            }

            return view('backend.driver.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     return view('backend.driver.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // echo "<pre>"; print_r($request->all()); die('hh');
       $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation'=>'required',
            'mobile_number' => 'required|unique:users,mobile_number',
            'aadhar_card' => 'required',
            'pan_card' => 'required',
            'account_holder_name' => 'required',
            'account_number' => 'required|confirmed|min:6',
            'account_number_confirmation'=>'required',
            'ifsc_code' => 'required',
            'insurance' => 'required',
            'vehicle_registration_certificate' => 'required',
            'plate_number' => 'required',
             'driving_license' => 'required',
            'address_name' => 'required',
        ],       
         [
            'mobile_number.required' => 'Mobile number is required',
            'vehicle_registration_certificate.required'=>'Registration is required'
        ]
    );
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{
          // DB::beginTransaction();
       /* try{*/
             
            $driver = new User();
      
            $driver->email              = $request->email;
            $driver->name               = $request->name;
            $driver->password           = Hash::make($request->password);
            $driver->mobile_number      = $request->mobile_number;
            $driver->address            = $request->address_name;
            //$driver->address_location   = $request->address;
            //$driver->latitute           = $request->lat;
           // $driver->longitute          = $request->lng;
            $driver->role               = 'driver';
            $driver->save();


            $driverBankDetails = new DriverBankDetails;

            $driverBankDetails->account_holder_name = $request->account_holder_name;
            $driverBankDetails->account_number = $request->account_number;
            $driverBankDetails->ifsc_code = $request->ifsc_code;
            $driverBankDetails->user_id = $driver->id;
            $driverBankDetails->save();




            /*Image Uploads*/
            if($request->hasFile('aadhar_card')){
                $folder_name ='aadhar_card';
                $aadhar_card = $this->fileUpload($request->file('aadhar_card'),false,$folder_name);
            }
            if($request->hasFile('pan_card')){
                $folder_name ='pan_card';
                $pan_card = $this->fileUpload($request->file('pan_card'),false,$folder_name);
            }
            if($request->hasFile('driving_license')){
                $folder_name ='driving_license';
                $driving_license = $this->fileUpload($request->file('driving_license'),false,$folder_name);
            }
               if($request->hasFile('plate_number')){
                $folder_name ='plate_number';
                $plate_number = $this->fileUpload($request->file('plate_number'),false,$folder_name);
            }
            if($request->hasFile('vehicle_registration_certificate')){
                $folder_name ='vehicle_registration_certificate';
                $vehicle_registration_certificate = $this->fileUpload($request->file('vehicle_registration_certificate'),false,$folder_name);
            }

               if($request->hasFile('insurance')){
                $folder_name ='insurance';
                $insurance = $this->fileUpload($request->file('insurance'),false,$folder_name);
            }
            
            /*Image Uploads*/
            $driverdetils             = new Driver();
            $driverdetils->aadhar_card = $aadhar_card;
            $driverdetils->pan_card = $pan_card;
            $driverdetils->driving_license = $driving_license;
            $driverdetils->plate_number = $plate_number;
            $driverdetils->vehicle_registration_certificate = 
            $vehicle_registration_certificate;
            $driverdetils->insurance = $insurance;
            $driverdetils->user_id = $driver->id;
            $data = $driverdetils->save();
            if($data){
            $response['status'] = "success";
            $response['url']     = route('admin.driver.list');
            $response['message'] = "Driver has been registered successfully.";
            request()->session()->flash('success','You have been successfully create driver');
            }else{
                $response['status'] = "error";
                 $response['message'] = "Somthing went wrong.";
            }
          /* } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
              // DB::rollBack();
            }*/

         }
        return json_encode($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::where('id',$id)->first();
        $data_driver = Driver::where('user_id',$id)->first();
        $driver_bank_data =  DB::table('driver_bank_details')->where('user_id',$id)->first();
        $driver_session_data = DriverSessionTime::where('user_id',$id)->first();
        return view('backend.driver.show',compact('data','data_driver','driver_bank_data','driver_session_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id',$id)->first();
        $data_driver = Driver::where('user_id',$id)->first();
        $data_driver_bank =DriverBankDetails::where('user_id',$id)->first(); 
        //echo "<pre>"; print_r($data_driver);die;
        return view('backend.driver.edit',compact('data','data_driver','data_driver_bank'));
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     //  echo "<pre>";print_r($request->all());die;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'mobile_number' => 'required',
            'address_name' => 'required',
            'account_holder_name' => 'required',
            'account_number' => 'required',
            'ifsc_code' => 'required',
            'password' => 'required|confirmed|min:6',
            'password_confirmation'=>'required',

          
        ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{
       // echo "<pre>"; print_r($request->all()); die('hh');
        $data = $request->all();

      /*  if($request->hasFile('image')){
            $folder_name ='image';

            $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
           }
*/
        $data['name'] = $request->name;
        //$data['country'] = $request->country;
       // $data['city'] = $request->city;
        $data['email'] = $request->email;
        $data['mobile_number'] =$request->mobile_number;
        $data['address'] = $request->address_name;
        $data['password']= Hash::make($request->password);

        $data =User::findOrFail($id)->update($data);

        $datas['account_holder_name'] = $request->account_holder_name;
        $datas['account_number'] = $request->account_number;
        $datas['ifsc_code'] = $request->ifsc_code;

        $data = DriverBankDetails::where('user_id',$id)->update($datas); 


       if($request->hasFile('aadhar_card')){
                $folder_name ='aadhar_card';
                $aadhar_card = $this->fileUpload($request->file('aadhar_card'),false,$folder_name);
            }
            if($request->hasFile('pan_card')){
                $folder_name ='pan_card';
                $pan_card = $this->fileUpload($request->file('pan_card'),false,$folder_name);
            }
            if($request->hasFile('vehicle_registration_certificate')){
                $folder_name ='vehicle_registration_certificate';
                $vehicle_registration_certificate = $this->fileUpload($request->file('vehicle_registration_certificate'),false,$folder_name);
            }
            if($request->hasFile('driving_license')){
                $folder_name ='driving_license';
                $driving_license = $this->fileUpload($request->file('driving_license'),false,$folder_name);
            }

              if($request->hasFile('plate_number')){
                $folder_name ='plate_number';
                $plate_number = $this->fileUpload($request->file('plate_number'),false,$folder_name);
            }

              if($request->hasFile('insurance')){
                $folder_name ='insurance';
                $insurance = $this->fileUpload($request->file('insurance'),false,$folder_name);
            }



     

        if(!empty($aadhar_card) && !empty($pan_card) && !empty($vehicle_registration_certificate) && !empty($driving_license)
         && !empty($plate_number)  && !empty($insurance)){

            $data = Driver::where("user_id", $id)->update(['aadhar_card'=>$aadhar_card,'pan_card'=>$pan_card,'vehicle_registration_certificate'=>$vehicle_registration_certificate,'driving_license' => $driving_license,'plate_number' => $plate_number,'insurance' => $insurance]);   
        }
        elseif(!empty($aadhar_card)){
        $data = Driver::where("user_id", $id)->update(['aadhar_card'=>$aadhar_card]);
        }

        elseif(!empty($pan_card)){
        $data = Driver::where("user_id", $id)->update(['pan_card'=>$pan_card]);
        }

        elseif(!empty($vehicle_registration_certificate)){
        $data = Driver::where("user_id", $id)->update(['vehicle_registration_certificate'=>$vehicle_registration_certificate]);
        }

        elseif(!empty($driving_license)){
        $data = Driver::where("user_id", $id)->update(['driving_license'=>$driving_license]);
        }

        elseif(!empty($plate_number)){
          $data = Driver::where("user_id",$id)->update(['plate_number'=>$plate_number]);
        }
        elseif(!empty($insurance)){
          $data = Driver::where("user_id",$id)->update(['insurance'=>$insurance]);

        }
     
       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.driver.list');
                    $response['message'] = "Driver has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully Updated Driver');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }

              }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'Driver';
        $confirm_route = $error = null;
        $confirm_route = route('admin.driver.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {   
        //echo $id; die();
        $data=User::find($id);
        $data->delete();
        $data=Driver::where('user_id',$id);
        $status =  $data->delete();
            request()->session()->flash('success','Driver successfully deleted');
            return redirect()->route('admin.driver.list');
        
        return redirect()->route('admin.driver.list');
    }

     //Here is the call status model view page ------
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';};
       $route_link = route('admin.driver.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';};
       $status = User::where('id',$id)->update(['status'=>$status]);

       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.driver.list');
     }
}
