<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SubscriptionFaq;
use Illuminate\Http\Request;
use Validator;
use App\Traits\ImageTrait;
use response;
use Session;
use DB;
use Hash;
use URL;
class SubscriptionFaqController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = SubscriptionFaq::select('count(*) as allcount')->count();
                $totalRecordswithFilter = SubscriptionFaq::select('count(*) as allcount')
               // ->where('role','vendor')
                ->Where('user_type', 'like', '%' .$searchValue . '%')
                ->orWhere('question', 'like', '%' .$searchValue . '%')
                /*->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                ->orWhere('users.country', 'like', '%' .$searchValue . '%')*/
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = SubscriptionFaq::orderBy($columnName,$columnSortOrder)
                //->where('role','vendor')
                 ->Where('user_type', 'like', '%' .$searchValue . '%')
                 ->orWhere('question', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('subscription_faq.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
               //  echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $delete_url = URL::to("admin/subscriptionfaq/$val->id/confirm-delete");
                    $edit_url = URL::to("admin/subscriptionfaq/edit/$val->id");
                    $view_url = URL::to("admin/subscriptionfaq/show/$val->id");

                     if($val->status==1){
                        $btnclass = "btn-success"; 
                        $status = "Active";
                        $faicon = "fa-toggle-on";
                    }else{ 
                        $btnclass = "btn-danger";
                        $status = "Inactive";
                        $faicon = "fa-toggle-off";
                    }
                      
                   if(!empty($val->image)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$val->image);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }   
                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'user_type'=>$val->user_type,
                        'question'=>$val->question,
                        'answer'=>$val->answer,
                        'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                        <a class="btn btn-info" href="'.$edit_url.'"><i class="fa fa-edit"></i>Edit </a>
                        <a class="btn btn-primary" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                        
                         <a data-toggle="modal" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>  </a>

                        <a data-toggle="modal" data-target="#delete_confirm" href="'.route('admin.subscriptionfaq.updatestatus.update-status-confirm',[$val->status,$val->id]).'" class="delval btn btn-xs '.$btnclass.'"  title="status"><i class="fa '.$faicon.'" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="status">'.$status.'</i> </a>'
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.subscriptionfaq.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.subscriptionfaq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // echo "<pre>"; print_r($request->all()); die();
        $validator = Validator::make($request->all() , ['user_type' => 'required','question'=>'required','answer' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
            // DB::beginTransaction();
            try
            {
                 //echo "<pre>"; print_r($request->all()); die();
                $subscriptionfaq = new SubscriptionFaq();

                $subscriptionfaq->user_type = $request->user_type;
                $subscriptionfaq->question = $request->question;
                $subscriptionfaq->answer = $request->answer;
                $data = $subscriptionfaq->save();
               
                if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.subscriptionfaq.list');
                    $response['message'] = "Subscription faq has been registered successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Subscription faq');
                    return json_encode($response);
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);
                }
            }
            catch(\Exception $e)
            {
                Session::flash('error', $e->getMessage());
                // DB::rollBack();
                
            }

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = SubscriptionFaq::where('id',$id)->first();
      // $data_vendor = Vendor::where('user_id',$id)->first();
       return view('backend.subscriptionfaq.show',compact('data'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = SubscriptionFaq::where('id',$id)->first();
        //$data_image = Vendor::where('user_id',$id)->first();
        return view('backend.subscriptionfaq.edit',compact('data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //echo "<pre>"; print_r($request->all()); die();
     // try {  
       $subscriptionfaq = $request->all();
       $validator = Validator::make($request->all() , ['user_type' => 'required','question'=>'required','answer' => 'required',
             ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else{
      
        $data['user_type'] = $request->user_type;
        $data['question'] = $request->question;
        $data['answer'] = $request->answer;
  

        $data =SubscriptionFaq::findOrFail($id)->update($data);

       if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('admin.subscriptionfaq.list');
                    $response['message'] = "Subscription faq has been update successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create Subscription faq');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }
       /* } catch (\Exception $e) {
          Session::flash('error',$e->getMessage());
        }*/
      }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
     //Here is the call delete model view page ------
    public function getModalDelete($id = null)
    {
        $model = 'SubscriptionFaq';
        $confirm_route = $error = null;
        $confirm_route = route('admin.subscriptionfaq.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=SubscriptionFaq::find($id);
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','Subscription faq successfully deleted');
                    return redirect()->route('admin.subscriptionfaq.list');
        }
        else{
            request()->session()->flash('error','Error while deleting subscription faq');
        }
        return redirect()->route('admin.subscriptionfaq.list');
    }

     //Here is the call status model view page ------ 
    public function status_confirm($status,$id){
       if($status=='1'){ $status='Inactive'; }else{ $status='Active';}; 
       $route_link = route('admin.subscriptionfaq.updatestatus',[$status,$id]);
       return view('backend/layouts/active-inactive',compact('route_link','status'));
    }

    //Here is udpate status  Active And Inactive
    public function update_status($status,$id){
       if($status=='Active'){ $status='1'; }else{ $status='0';}; 
       $status = SubscriptionFaq::where('id',$id)->update(['status'=>$status]);
      
       if($status){
          Session::flash('success','Status Updated Successfully.');
       }else{
          Session::flash('error','Somthing went wrong.');
       }
      return Redirect()->route('admin.subscriptionfaq.list');
     }
}

