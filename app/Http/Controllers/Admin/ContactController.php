<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\ContactPurpose;
use App\Models\CustomerReply;
use App\Traits\ImageTrait;
use Illuminate\Support\Facades\Mail;
use response;
use Session;
use DB;
use Hash;
use URL;
class ContactController extends Controller
{
    use ImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function index(Request $request)
    {
        try{
            if ($request->ajax()) {
                ## Read value
                $draw = $request->get('draw');
                $start = $request->get("start");
                $rowperpage = $request->get("length"); // Rows display per page
                
                $columnIndex_arr = $request->get('order');
                $columnName_arr = $request->get('columns');
                $order_arr = $request->get('order');
                $search_arr = $request->get('search');
                
                $columnIndex = $columnIndex_arr[0]['column']; // Column index
                $columnName = $columnName_arr[$columnIndex]['data']; // Column name
                $columnSortOrder = $order_arr[0]['dir']; // asc or desc
                $searchValue = $search_arr['value']; // Search value
                
                // Total records
                $totalRecords = Contact::select('count(*) as allcount')->count();
                $totalRecordswithFilter = Contact::select('count(*) as allcount')
                //->where('role','vendor')
                ->Where('mobile', 'like', '%' .$searchValue . '%')
                ->orWhere('email', 'like', '%' .$searchValue . '%')
                ->count();
                // Fetch records
           // DB::enableQueryLog(); // Enable query log
    
                $records = Contact::orderBy($columnName,$columnSortOrder)
                  ->Where('mobile', 'like', '%' .$searchValue . '%')
                  ->orWhere('email', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.mobile_number', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.city', 'like', '%' .$searchValue . '%')
                // ->orWhere('users.country', 'like', '%' .$searchValue . '%')
                ->select('contact.*')
                ->skip($start)
                ->take($rowperpage)
                ->get(); 
            //  echo "<pre>"; print_r(DB::getQueryLog()); die();
             //    echo "<pre>"; print_r($records); die();
                $data_arr = [];
                foreach($records as $key=> $val){
                 //foreach($val->sub_category as $sub_category){
                    $view_url = URL::to("admin/contact/show/$val->id");
                    $delete_url = URL::to("admin/contact/$val->id/confirm-delete");

                    //For Reply button
                    $url   = route('admin.contact.insert');
                    $token = csrf_field(); 

                    $data_arr[] = array( 
                        'id'=>$val->id,
                        'mobile'=>$val->mobile,
                        'email'=>$val->email,
                        'message'=>$val->message,
                         'created_at'=>date('d F,Y',strtotime($val->created_at)),
                        'action'=>'
                       
                        <a class="btn btn-primary" style="margin:1px;" href="'.$view_url.'"><i class="fa fa-eye"></i>View </a>
                        
                         <a data-toggle="modal" style="margin:1px;" data-target="#delete_confirm" href="'.$delete_url.'" class="delval btn btn-xs btn-danger"  title="Delete"><i class="fa fa-trash" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Delete">Delete</i>
                         </a>
                             
                        <a href="#" style="margin:1px;" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal'.$val->id.'"><i class="fa fa-reply"  data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Reply"></i> Reply<div class="ripple-container"></div></a>

                          <div class="modal fade col-sm-12" id="myModal'.$val->id.'" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                          <div class="modal-dialog">
                          <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close resetModal" data-dismiss="modal" aria-hidden="true">×</button>
                              <h4 class="modal-title" id="myModalLabel">Customer Complaint Reply</h4>
                           </div>
                           <div class="modal-body">
                            
                              <form class="form ajaxform" action="'.$url.'"  id=customer-complaint-form" method="post">
                                '.$token.'
                              <div class="table-responsive">
                                 <table class="table table-bordered">
                                    <tbody>
                                    <input type="hidden" name="user_id" value="'.$val->user_id.'" />
                                    <input type="hidden" name="complaint_id" value="'.$val->id.'" />
                                     
                                      
                                       <tr>
                                          <th width="30%""><label>Eamil</label></th>
                                          <td width="70%""><input type="email" id="email" name="email" class="form-control"  value="'.$val->email.'" readonly style="width: 100%;""></td>
                                       </tr>
                                       <tr>
                                          <th width="30%"><label>Message</label></th>
                                          <td width="70%"><textarea id="reply" name="reply" class="form-control" style="width: 100%; margin-bottom: 34px;"></textarea>
                                        <br/>
                                       <center>
                                       <i class="la la-check-square-o"></i>
                                          <button type="submit" class="btn btn-xs btn-warning status">Send</margin-bottom: 34px;>
                                       </center>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                              </form>
                           </div>

                        </div>
                     </div>
                  </div>
                     '
                    );
                 // }  
                }//Close foreach loop
                 $response = array(
                    "draw" => intval($draw),
                    "iTotalRecords" => $totalRecords,
                    "iTotalDisplayRecords" => $totalRecordswithFilter,
                    "aaData" => $data_arr
                 );
                 
                 echo json_encode($response); die;
            }
    
            return view('backend.contact.index');
        }
        catch(\Exception $e)
        {

           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = Contact::where('id',$id)->first();
       $data_contact = ContactPurpose::where('id',$data->contact_purpose_id)->first();
       return view('backend.contact.show',compact('data','data_contact'));
    }


    public function getModalDelete($id = null)
    {
        $model = 'Contact';
        $confirm_route = $error = null;
        $confirm_route = route('admin.contact.delete', $id);
        return View('backend/layouts/delete_modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function destroy($id)
    {
       $data=Contact::find($id);
       //$data->delete();
       $status =  $data->delete();
        if($status){
            request()->session()->flash('success','contact successfully deleted');
                    return redirect()->route('admin.contact.list');
        }
        else{
            request()->session()->flash('error','Error while deleting contact');
        }
        return redirect()->route('admin.contact.list');
    }


     //save
    public function store(Request $request){
       // echo "<pre>";print_r($request->all()); die;
      $reply = $request->get('reply');
      if($reply==!''){
      $insert                = new CustomerReply();
      $insert->user_id       = $request->get('user_id');
      $insert->complaint_id  = $request->get('complaint_id');
      $insert->reply         = $reply;
      $save = $insert->save();
      $userdata = User::select('email', 'name')->where('id',$request->get('user_id'))->first();
      //For Mail code ..
      if($save){
        $data = $userdata;
         Mail::send('emails.complaint', compact('data','reply'), function ($m) use ($userdata)  {   
        $m->from('info@qikmeds.com', 'complaint');
        $m->to($userdata->email, 'complaint')->subject('Responding to a complaint with a proposed solution');
        }); 
    }

     //End mail for code ..
      $request->session()->flash('success','Reply  Successfully sent..!');
        $response['status'] = "success";
        $response['message'] = "Reply  Successfully sent..!";
        //$response['reload'] = route('admin.contact.list');
        return redirect()->route('admin.contact.list');
        
      }else{
        $response['status'] = "error";
        $response['message'] = "Reply Filde is required";
      }
      return json_encode($response);
    }






}

