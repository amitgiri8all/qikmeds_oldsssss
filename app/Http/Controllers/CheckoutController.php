<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Models\SubscriptionPlan;
use App\Models\ReferEarn;
use App\Models\User;
use App\Models\Order;
use App\Models\OfferBanner;
use Auth;
use Session;
use DB;

class CheckoutController extends Controller
{
    //Checkout Page
     public function index()
    {
        $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
        $subscriptionplan =SubscriptionPlan::where('plan_type','user')->where('id',Session::get('plan_id'))->first();
        $check_first_order = Order::where('user_id',Auth::guard('web')->user()->id)->count();
       // echo $check_first_order; die();
        return view('frontend.checkout',compact('site_setting','subscriptionplan','check_first_order'));
    } 

    public function delivery_address_check()
    {
      $user_id = Auth::guard('web')->user()->id;
      $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
      $delivery_address = DeliveryAddress::where('status',1)->where('user_id',$user_id)->first();
      if (!empty($delivery_address)) {
       $json['redirect_url']  = route('checkout');
       echo json_encode($json);      
      }else{
       $json['message'] = 'Delivery address required from customer.';
       $json['status']  = 'error';
       echo json_encode($json);      
       //return redirect('user/addresses');
      }
    }


     public function check_referral_code(Request $request){
      session()->forget('referrer_code');
    //  echo Auth::guard('web')->user()->id; die();
     // echo "<pre>"; print_r($request->all()); die();
        if(!empty($request->referrer_code)){
         ///  DB::enableQueryLog(); 
        //  echo $request->referrer_code; die();
          $check_ref_code = User::where('referrer_code',$request->referrer_code)->where('id','!=',Auth::guard('web')->user()->id)->where('role','user')->first();
        // echo "<pre>"; print_r(DB::getQueryLog()); die(); 
         //  echo $check_ref_code; die();
          if(!empty($check_ref_code)){
              $referEarn = ReferEarn::where('id','1')->first();
              session()->put('referrer_code',$check_ref_code->referrer_code);
              $res['error'] =true;
              $res['status'] ='success';
              $res['message'] = "Referrer code match. You earn Rs .".$referEarn->refer_amount."";
              return response()->json($res);
          }else{
              $res['error'] =false;
              $res['status'] ='error';
              $res['message'] = "This referral code is not valid! Check and try again.";
              return response()->json($res);
          }
      }else{
              $res['error'] =false;
              $res['status'] ='error';
              $res['message'] = "Referral code is required!";
              return response()->json($res);
      }    
    }

    /*===========*/
    public function remove_referrer_code(Request $request){
      session()->forget('referrer_code'); 
      $res['error'] =true;
      $res['status'] ='success';
      $res['message'] = "Remove Referrer Code";
      return response()->json($res);
    }

    /*===========*/
    public function remove_offer_code(Request $request){
      session()->forget('offer_code'); 
      session()->forget('offer_code_discount'); 
      session()->forget('offer_cashback'); 
      session()->forget('offer_id_aapyed'); 
      $res['error'] =true;
      $res['status'] ='success';
      $res['message'] = "Remove Offer Code";
      return response()->json($res);
    }

    /*******************
     Offer Code Chekc Calcution       
    *******************/ 
    public function check_offer_code(Request $request){
     // echo "<pre>"; print_r($request->all()); die();
      if(!empty($request->offer_code)){
       $total_amt = Session::get('total_amt');
      // echo $total_amt; die('======');
       $check_offer_code = OfferBanner::where('code',$request->offer_code)->first();
        if(empty($check_offer_code)){
              $res['status'] ='error';
              $res['code'] =0;
              $res['message'] = "This offer code is not valid! Check and try again";
          }elseif($check_offer_code->minimum_amount >=$request->sum_total_amount_val){
              $res['error'] =false;
              $res['status'] ='error';
              $res['message'] = "Sorry, this offer code is only applicable on Total Amount value above Rs.".$check_offer_code->minimum_amount ." Please add more items to avail this offer";
          }else{
                
            if($check_offer_code->offer_type!='discount'){
            $offer_value = 0;
            $offer_cashback = $check_offer_code->price;
           }else{
           session()->put('offer_code_discount',$check_offer_code->price);
            $offer_value = $check_offer_code->price;
            $offer_cashback = 0;
           }
            session()->put('offer_code',$request->offer_code);
            session()->put('offer_cashback',$offer_cashback);
            session()->put('offer_id_aapyed',$check_offer_code->id);
            
              $res['error'] =false;
              $res['status'] ='success';
              $res['message'] = "Offer code applied successfully.";
          }
       }else{
        $res['error'] =false;
        $res['status'] ='error';
        $res['message'] = "Offer code is required!";

       }   
            return response()->json($res);
     }  


}
