<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Models\PatientInfo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\SaveForLater;
use App\Models\Cart;
use App\Models\User;
use App\Models\SubscriptionPlan;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Blog;
use Session;

class CartController extends Controller
{



     /*Here is call Construct Method*/
    function __construct(Request $request,User $user,DeliveryAddress $deliveryAddress,Brand $brand,Blog $blog,Category $category,Banner $banner,Product $product,Cart $cart){
        $this->method           =$request->method();
        $this->user             = $user;
        $this->brand            = $brand;
        $this->blog             = $blog;
        $this->deliveryAddress  = $deliveryAddress;
        $this->banner           = $banner;
        $this->category         = $category;
        $this->product          = $product;
        $this->cart             = $cart;
        $this->userId           = Auth::guard('web')->user()?Auth::guard('web')->user()->id:null;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    //    echo Session::get('plan_id');  die();
        $subscriptionplan =SubscriptionPlan::where('plan_type','user')->where('id',Session::get('plan_id'))->first();
       // echo "<pre>"; print_r($subscriptionplan); die();
    //    DB::enableQueryLog();
        $coupon_code = Coupon::select('id','code','name','coupon_type','coupon_value','to_time','from_time','number_of_use','status')
       ->where('status','1')->orderBy('id','DESC')->where('to_time', '>=', date('Y-m-d'))->get();

       
        $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();

     //  echo print_r(DB::getQueryLog()); die();
       //echo "<pre>"; print_r($coupon_code); die();
       $query = Product::select('products.*','product_image.image','product_image.caption','manufacturer.manufacturer_name',)
         ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->leftJoin('product_image', function($join)
        {
          $join->on('product_image.product_id', '=', 'products.p_id');
        })

        ->where('products.status',1)
        ->limit(5)
       ->groupBy('product_image.product_id');
        $productDetail=$query->get();
      if(Auth::guard('web')->user()){  
        $product = SaveForLater::where(['user_id'=>Auth::guard('web')->user()->id])
         ->with(['Product','Product.ProductImage'])
         ->get();
      }else{
        $product = [];
      }   

      return view('frontend.cart',compact('productDetail','coupon_code','site_setting','product','subscriptionplan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function addToCart(Request $request)
    {
        $id = $request->product_id;
        $product = Product::findOrFail($id);
        if(empty($product->sale_price)){
         $price = $product->mrp;
        }else{
         $price = $product->sale_price;
        }
        $cart = session()->get('cart', []);
        $cart[$id] = [
            "id" => $product->id,
            "name" => $product->medicine_name,
            "prescription" => $product->prescription,
            "quantity" => 1,
            "price" => $price,
            "image" => $product->image
        ];

        //if($cart==null) { 
        if(Auth::guard('web')->user()){
            $input = $request->all();
            $input['user_id'] = Auth::guard('web')->user()->id;    
            $input['product_id'] = $product->id;    
            $input['qty'] = 1;    
            $this->cart->create($input);
        }    
       //  }  
        session()->put('cart', $cart);
        return json_encode(array('status'=>'success'));
    }

     public function ReorderaddToCart(Request $request)
    {
      //  echo "sadfasd"; die();
        $id = $request->product_ids;
        //echo $id; die();  
        $values=explode(',',$id);

        foreach($values as $id)
    {
        $product = Product::where('id',$id)->first();
        if(empty($product->sale_price)){
         $price = @$product->mrp;
        }else{
         $price = @$product->sale_price;
        }
        $cart = session()->get('cart', []);
        $cart[$id] = [
            "id" => @$product->id,
            "name" => @$product->medicine_name,
            "prescription" => @$product->prescription,
            "quantity" => 1,
            "price" => @$price,
            "image" => @$product->image
        ];
       // if($cart==null) { 
            if(Auth::guard('web')->user()){
                $input = $request->all();
                $input['user_id'] = Auth::guard('web')->user()->id;    
                @$input['product_id'] = $product->id;    
                $input['qty'] = 1;    
                $this->cart->create($input);
            }    
      //  }  
        session()->put('cart', $cart);
       }  
       $url = url('cart');
        return json_encode(array('status'=>'success', 'url' => $url));
    }



    /**
     * Write code on Method
     *
     * @return response()
     */
    public function update_cart(Request $request)
    {
       if($request->quantity==0){
          $cart = session()->get('cart');
            if(isset($cart[$request->product_id])) {
                unset($cart[$request->product_id]);
                session()->put('cart', $cart);
            }
            return json_encode(array('status'=>'success'));
         }
         if(Auth::guard('web')->user()){
          // Cart::update(['qty'=>$request->quantity])->where('user_id',Auth::guard('web')->user()->id)->where('product_id',$request->product_id);
           Cart::where('product_id',$request->product_id)->where('user_id',Auth::guard('web')->user()->id)->update(['qty' => $request->quantity]);

        } 

         if($request->product_id && $request->quantity){
             $cart = session()->get('cart');
            $cart[$request->product_id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
        return json_encode(array('status'=>'success'));
        }

    }

     /**
     * Write code on Method
     *
     * @return response()
     */
    public function remove(Request $request)
    {
       // echo Auth::guard('web')->user()->id; die();
       // echo "sadfas"; die();
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
                if(Auth::guard('web')->user()){
                  DB::table('cart')->where('product_id',$request->id)->where('user_id',Auth::guard('web')->user()->id)->delete();
                } 
            }  
            return json_encode(array('status'=>'success'));
        }
    } 

    public function remove_plan(Request $request)
    {//echo $request->plan_id; die();
        if($request->plan_id) {
         Session::forget('plan_id');
         return json_encode(array('status'=>'success'));
        }
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function clear_cart(Request $request)
    {

         Session::forget('cart');
         Session::forget('plan_id');
         if(Auth::guard('web')->user()){
                 //   echo $request->id; die();
                  DB::table('cart')->where('user_id',Auth::guard('web')->user()->id)->delete();
         }
         return json_encode(array('status'=>'success'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function order_review(Request $request){
      $subscriptionplan =SubscriptionPlan::where('plan_type','user')->where('id',Session::get('plan_id'))->first();
      $user_id = Auth::guard('web')->user()->id;
      $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
      $delivery_address = DeliveryAddress::where('status',1)->where('user_id',$user_id)->first();
      $addresses = DeliveryAddress::where('user_id',Auth::guard('web')->user()->id)->get();
      $patient = PatientInfo::where('user_id',Auth::guard('web')->user()->id)->get();
      $patient_address = PatientInfo::where('status',1)->where('user_id',$user_id)->first();
      $subscriptionplan =SubscriptionPlan::where('plan_type','user')->where('id',Session::get('plan_id'))->first();
      return view('frontend.order-review',compact('site_setting','delivery_address','patient','addresses','subscriptionplan','patient_address','subscriptionplan'));
    }

    public function promocode_calculation(Request $request){
        Session::forget('total_saving');
        Session::forget('total_amt');
        Session::forget('promo_val');
        Session::forget('coupan_code');
        Session::forget('coupan_id');
        Session::forget('coupan_name');
        Session::forget('coupon_type');
        Session::forget('total_saving');
      $total = 0 ;
       foreach((array) session('cart') as $id => $details){
            $price= str_replace('₹', '',$details['price']);
            $total += (int) $price * (int) $details['quantity'];
       }
      //GET VALUE
      $get_coupan = Coupon::select('id','code','name','coupon_type','coupon_value','coupon_minimum_value')->where('id',$request->coupon_id)->first();

           //echo "<pre>";print_r($get_coupan->coupon_minimum_value);die;
        /*
              if($get_coupan->coupon_minimum_value > $total )
              {

              }*/



      //Get Coupan Value
      if($get_coupan->coupon_type=='percentages'){
       // echo "percentages".$total.'=='.$get_coupan->coupon_value; die();
        $promo_val = $total * $get_coupan->coupon_value/100;
        //$promo_val = $total-$val;
      }else{
          $promo_val = $get_coupan->coupon_value;
      }
      //Site Setting
       $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
       
       $qikmeds_discount = $site_setting->qikmeds_discount;

       $delivery = $site_setting->delivery_charge;
       //Check delivery calculation
       if($site_setting->min_price > $total){
         $delivery_charge = $delivery;
       }else{
         $delivery_charge =0;
       }

       $total_saving = $qikmeds_discount+$promo_val;
       //Total Sub value and saving
       $total_amt = $total-$total_saving;
       //Set here all session value;
       Session::put('coupan_id', $get_coupan->id);
       Session::put('coupan_name', $get_coupan->name);

       Session::put('coupan_code', $get_coupan->code);
       Session::put('coupon_type', $get_coupan->coupon_type);

       Session::put('total_saving', $total_saving);

       Session::put('total_amt', $total_amt);

       Session::put('promo_val', $promo_val);

      // echo $promo_val; die();

       $json['success'] = 'Promo Code Apply successfully..!!';
       $json['status'] = 'success';
       $json['total_amt'] = '₹'.$total_amt;
       $json['total_saving'] = '₹'.$total_saving;
       $json['promo_val'] = '₹'.$promo_val;
       $json['coupan_id'] = $get_coupan->id;
       echo json_encode($json);
    }

    /* ==== Save for later */
    public  function save_for_later(Request $request){
     /// dd($request->all());
        $count=SaveForLater::where('user_id',Auth::guard('web')->user()->id)->where('product_id',$request->get('product_id'))->count();
        if($count>0){
                $SaveForLater = SaveForLater::where('user_id',Auth::guard('web')->user()->id)->where('product_id',$request->get('product_id'));
                $SaveForLater->forceDelete();
                    $id = $request->get('product_id');
                    $cart = session()->get('cart');
                    if(isset($cart[$id ])) {
                    unset($cart[$id ]);
                    session()->put('cart', $cart);
                        if(Auth::guard('web')->user()->id){
                            DB::table('cart')->where('product_id',$id )->where('user_id',Auth::guard('web')->user()->id)->delete();
                        } 
                    }  
                return(json_encode(array(
                'success' => false,
                'status' => "success"
                
                ))); 
            }
            else
            {
                $id = $request->get('product_id');
                $cart = session()->get('cart');
                if(isset($cart[$id ])) {
                unset($cart[$id ]);
                session()->put('cart', $cart);
                if(Auth::guard('web')->user()->id){
                DB::table('cart')->where('product_id',$id )->where('user_id',Auth::guard('web')->user()->id)->delete();
                } 
                }  


                $save = new SaveForLater();
                $save->user_id = Auth::guard('web')->user()->id;
                $save->product_id = $request->get('product_id');
                $status = $save->save();

                return(json_encode(
                array(
                'success' => true,
                'status' => "success"
                
                ))); 
            } 
    }


    public function add_subscription_plan(Request $request){
       if($request->plan_id!=''){ 
            Session::forget('plan_id');
            Session::put('plan_id', $request->plan_id);
             if(session('cart')){
               return(json_encode(array('success' => true,'status' => "success",'message'=>'Successfully add in your cart','redirect_url'=>route('cart')))); 
             }else{
                  $json['redirect_url'] = route('checkout');
                return(json_encode(array('success' => true,'status' => "success",'message'=>'Successfully By Now','redirect_url'=>route('checkout')))); 
             }
        }else{
            return(json_encode(array('error' => true,'status' => "validation"))); 
        }   
       
    }

       public function DeletePatientAddress($id){ 
       PatientInfo::find($id)->delete($id);
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }


    public function customer_notes(Request $request){
        $customer_notes_data  = $request->get('customr_note');
        Session::put('customer_note', $customer_notes_data);
        
    }

    public function coupon_minimum_value(){
        echo "d";die;
    }


}
