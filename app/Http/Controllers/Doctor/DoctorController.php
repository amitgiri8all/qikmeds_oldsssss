<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Notifications\OrderPlace;
use App\Notifications\AllOrderStatus;
use App\Models\User;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Order;
use App\Models\Doctor;
use App\Models\PatientInfo;
use App\Models\DeclineProducts;
use App\Models\OrderPayment;
use App\Models\UserPrescription;
use App\Models\PaymentMethod;
use App\Models\VendorProductStatus;
use App\Models\OrderPrescription;
use App\Models\DoctorProductStatus;
use App\Models\DoctorBankDetails;
use App\Models\DoctorDocument;
use App\Models\Notification;
use App\Models\DoctorSpecializationsInfo;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Helpers\Helper;
use Carbon\Carbon;
use Validator;
use Response;
use Session;
use DB;
use URL;
use Auth;
use Crypt;
use Mail;
use Hash;
//use Notification;
class DoctorController extends Controller
{
    use HelperTrait;
        use ImageTrait;


   
    public function index(){
      //$doctor_id = Auth::guard('doctor')->user()->id;
     // $product_count = OrderItem::where([ ['doctor_id','=',$doctor_id] ])->count();

      //$delivery_online = User::where([ ['id','=',$doctor_id],['status','=',1]])->first();
      //dd($delivery_online);

      $doctor_id = Auth::guard('doctor')->user()->id;
      $bank_details = DoctorBankDetails::where('user_id',$doctor_id)->first();
      $doctor_documents = DoctorDocument::where('doctor_id',$doctor_id)->first();
      $doctor_education_info = DoctorSpecializationsInfo::where('doctor_id',$doctor_id)->get();

      return view('frontend.doctor.dashboard',compact('bank_details','doctor_documents','doctor_education_info'));
    }

    public function document(Request $request){

    $doctor_id = Auth::guard('doctor')->user()->id;
    $document_data = DoctorDocument::where('doctor_id', $doctor_id)->first();
    if (!empty($document_data)) {
      $document_id =  $document_data->doctor_id;
    }

    if (empty($document_id)) {

      $validator = Validator::make($request->all(), [
        // 'doctor_license' => 'required',

      ]);

      if ($validator->fails()) {
        return response()->json($validator->messages(), 422);
      } else {
        // DB::beginTransaction();
        // try{

        $document = new DoctorDocument();
        //Insert value in doctor_id
        $document->doctor_id  = Auth::guard('doctor')->user()->id;

        /*Image Uploads*/
        //Inser for twice image in database code..
        if($request->hasFile('doctor_license') && $request->hasFile('educational_certificate') ){
          $folder_name = 'doctor_license';
          $doctor_license = $this->fileUpload($request->file('doctor_license'), false, $folder_name);

          $folder_name = 'educational_certificate';
          $educational_certificate = $this->fileUpload($request->file('educational_certificate'), false, $folder_name);

          

          $document->doctor_license   = $doctor_license;
          $document->educational_certificate   = $educational_certificate;
          
          //Insert only one field data 
        } if ($request->hasFile('doctor_license')) {
          $folder_name = 'doctor_license';
          $doctor_license = $this->fileUpload($request->file('doctor_license'), false, $folder_name);
          $document->doctor_license   = $doctor_license;
           //Insert only one field data 
        } if ($request->hasFile('educational_certificate')) {
          $folder_name = 'educational_certificate';
          $educational_certificate = $this->fileUpload($request->file('educational_certificate'), false, $folder_name);
          $document->educational_certificate   = $educational_certificate;
        } 

        $document->save();

        return redirect()->back()->with('status', 'Inserted Successfully');
        /*} catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
              // DB::rollBack();
            }*/
      }
    } else {
      //for Update data
      
      if ($request->hasFile('doctor_license')) {
        $folder_name = 'doctor_license';
        $doctor_license = $this->fileUpload($request->file('doctor_license'), false, $folder_name);
      }
      if ($request->hasFile('educational_certificate')) {
        $folder_name = 'educational_certificate';
        $educational_certificate = $this->fileUpload($request->file('educational_certificate'), false, $folder_name);
      }

      if (!empty($doctor_license) && !empty($educational_certificate)) {

        $data = DoctorDocument::where("doctor_id", $document_id)->update(['doctor_license' => $doctor_license, 'educational_certificate' => $educational_certificate]);
      } if (!empty($doctor_license)) {
        $data = DoctorDocument::where("doctor_id", $document_id)->update(['doctor_license' => $doctor_license]);
      } if(!empty($educational_certificate)){
        $data = DoctorDocument::where("doctor_id",$document_id)->update(['educational_certificate' => $educational_certificate]);
      }

      return redirect()->back()->with('status', 'Updated Successfully');
    }
    }

    public function order(){
      $doctor_id = Auth::guard('doctor')->user()->id;
      $near_by_queue_time = 4000;
      $doctor_orders = Order::with('User','OrderPayment','DeliveryAddress','OrderItem','UserPrescription')
       ->whereHas('OrderItem', function($q) {   
        $q->whereNull('doctor_id');
      })->where('created_at', '>', Carbon::now()->subMinutes($near_by_queue_time)->toDateTimeString())->orderBy('id','desc')->where(['order_status'=>'N','is_prescription'=>'Y']) 
       ->get();
     
      $accepted_doctor_orders  = Order::with(['User','OrderPayment','DeliveryAddress','OrderItem','UserPrescription'])
       ->whereHas('OrderItem', function($q) use ($doctor_id) {
                    $q->where('doctor_id',$doctor_id);
       });
      $accepted_doctor_orders = $accepted_doctor_orders->orderBy('id','desc')->get();
      
      $completed_consultation_order   = Order::with(['User','OrderPayment','DeliveryAddress','OrderItem','UserPrescription'])->orderBy('id','desc')->where('order_status','CC')
       ->whereHas('OrderItem', function($q) use ($doctor_id) {
                    $q->where('doctor_id',$doctor_id);
       });
      $completed_consultation_order = $completed_consultation_order->orderBy('id','desc')->get();
      //echo "<pre>"; print_r($doctor_orders); die;
     return view('frontend.doctor.order',compact('doctor_orders','accepted_doctor_orders','completed_consultation_order')); 
    }

      /**Order decline**/
    public function decline_orders(Request $request){
      $doctor_id = Auth::guard('doctor')->user()->id;
      $order_id = $request->get('order_id');
      $ord_item_id = $request->get('ord_item_id');
      $get_all_order_item = OrderItem::select('ord_item_id')->where('order_id',$order_id)->get();
      $orderItem= [];
      foreach($get_all_order_item as $key=>$val){
        $orderItem =  new DeclineProducts();
        $orderItem->ord_product_item_id = $val->ord_item_id;
        $orderItem->order_id = $order_id;
        $orderItem->user_id = $doctor_id ;
        $orderItem->type = 'doctor';
        $orderItem->save();
      }
      if($orderItem){
        $response['status'] = "success";
        $response['loading'] =  'true';
        $response['message'] = "You  has been created successfully decline order.";
        return json_encode($response);
      }else{
        $response['status'] = "error";
        $response['message'] = "There was an issue account createing the recode. Please try again.";
        return json_encode($response);
      }  
    }  


    /**Order Accepted**/
    public function accepte_orders(Request $request){
      $doctor_id = Auth::guard('doctor')->user()->id;
      $order_id = $request->get('order_id');
      $ord_item_id = $request->get('ord_item_id');
      $order_data = Order::where('id',$order_id)->first();

      /*send notification to customer*/
      
      $user_id_array = User::where('id',$order_data->user_id)->select('id','device_type','device_token','name')->get();
      $user_id_array1 = User::where('id',$order_data->user_id)->select('id','device_type','device_token','name')->get();
      $userData = User::where('id', '=', $order_data->user_id )->select('device_token')->get();
      $user_id_array = collect($userData)->pluck('device_token');
     // $user_id_array = collect($userData)->pluck('device_token');
      $senderName = $user_id_array1[0]->name;
      $message    = '#'.$order_data->order_code.trans('order.Order Accepted');
      $type       = 'Order Accepted';
      $order_data->user->notify(new AllOrderStatus($order_data,$senderName,$message,$type));
     
      /**/
      $dataArray = [];
      $dataArray['type'] = 'Order';
      $dataArray['product_type'] = 'Order Accepted';
      $dataArray['title'] = ' Accepted';
      $dataArray['body'] = trans('Order Accepted successfully');
      $device_type = $user_id_array1[0]->device_type;

     Helper::sendNotification($user_id_array ,$dataArray, $device_type);
    /*send notification to customer*/

      //Vendor Product Status 
      $status = new DoctorProductStatus();
      $status->order_id = $order_id;
      $status->doctor_id = $doctor_id;
      $status->save();

      if(!empty($ord_item_id)){
        $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
          ->update(array('doctor_id' => $doctor_id));

        $get_all_unchecked_ids = OrderItem::select('ord_item_id')->whereNull('doctor_id')->where('order_id',$order_id)->get();
        if(!empty($get_all_unchecked_ids)){
           foreach($get_all_unchecked_ids as $key=>$val){
            $orderItem =  new DeclineProducts();
            $orderItem->ord_product_item_id = $val->ord_item_id;
            $orderItem->order_id = $order_id;
            $orderItem->user_id = $doctor_id;
            $orderItem->type = 'vendor';
            $orderItem->save();
          }
        }  
      }else{
       
        $get_all_order_item = OrderItem::select('ord_item_id')->whereNull('doctor_id')->where('order_id',$order_id)->get();
        // echo "<pre>";print_r($get_all_order_item);die;
         $ord_item_id = $get_all_order_item->pluck('ord_item_id');
            $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
          ->update(array('doctor_id' => $doctor_id));

      } 

       $url = route('order-prescription',encrypt($order_id));


      if($status){
        $response['status'] = "success";
        $response['loading'] =  'true';
      //  $response['url'] =  route('order-prescription',$order_id);
        $response['url']     = $url;
        $response['message'] = "You has been created successfully accepte order.";
       return json_encode($response);
      }else{
          $response['status'] = "error";
          $response['message'] = "There was an issue account createing the recode. Please try again.";
         return json_encode($response);
       }  
    }

//ALTER TABLE `order_item` ADD `doctor_id` INT(11) NULL DEFAULT NULL AFTER `vendor_id`;

    public function order_prescription(Request $request,$id=null){
      $doctor_id = Auth::guard('doctor')->user()->id;
      $id  = decrypt($id);
      $val = 1;
      $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItemDriver')
      ->where('id',$id)->first();  


      //echo "<pre>"; print_r($order); die();
      $patient_address = PatientInfo::where('status',1)->where('id',$order->patient_id)->first();
      $orderprescription=OrderPrescription::where('user_id',$order->user_id)->where('order_id',$id)->where('patient_id',$order->patient_id)->first();
      $doctorInfo = Doctor::where('user_id',$doctor_id)->first();
      //return $doctorInfo;
      return view('frontend.doctor.order-prescription',compact('order','patient_address','orderprescription','doctorInfo')); 
    }
    
    public function save_order_prescription(Request $request){
      $resp = array();
      $rules = array('complain' => 'required', 'observation' => 'required', 'diagnosis'=>'required', 'instruction'=>'required');
      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails())
      {
          $resp['status'] = 'validation';
          $resp['errors'] = $validator->getMessageBag()->toArray();
          return response()->json($resp);
      }

      $data = $request->all();
      $order_id = decrypt($request->order_id);
      $user_id  = decrypt($request->user_id);
      $patient_id  = decrypt($request->patient_id);
      $data['order_id'] =$order_id;
      $data['user_id'] = $user_id;
      $data['patient_id'] = $patient_id;

       $count=OrderPrescription::where('user_id',$user_id)->where('order_id',$order_id)->where('patient_id',$patient_id)->count();
      ///  echo $count; die();
  if($count==0){ 
              $status = OrderPrescription::create($data);
              if($status){
              $response['status'] = "success";
              $response['loading'] =  'true';
              $response['message'] = "You has been created successfully.";
              return json_encode($response);
              }else{
                $response['status'] = "error";
                $response['message'] = "There was an issue account createing the recode. Please try again.";
               return json_encode($response);
              }  
       }else{
     
       $status = OrderPrescription::where('user_id',$user_id)->where('order_id',$order_id)->where('patient_id',$patient_id)->update(["complain" =>$request->complain,"observation" =>$request->observation,"diagnosis" =>$request->diagnosis,"instruction" =>$request->instruction]);
     //  echo "<pre>"; print_r(DB::getQueryLog()); die();
        if($status){
        $response['status'] = "success";
        $response['loading'] =  'true';
        $response['message'] = "You has been updated successfully.";
        return json_encode($response);
        }else{
        $response['status'] = "error";
        $response['message'] = "There was an issue account createing the recode. Please try again.";
        return json_encode($response);
        }  

        }
    }
     
    public function update_product_items(Request $request){
      //echo "<pre>"; print_r($request->all()); die();
      //echo $request->advise.'===='.$request->order_item_id; die();
      $order_item_id = decrypt($request->order_item_id); 
       $status = OrderItem::where('ord_item_id',$order_item_id)->update(['advise' => $request->advise]);
       if($status){
          $response['status'] = "success";
          $response['message'] = "You has been updated successfully.";
        }else{
          $response['status'] = "error";
          $response['message'] = "There was an issue account createing the recode. Please try again.";
        } 
        return json_encode($response);
    }
    //Here is the auto search value user name
    public function autocomplet_search(Request $request) 
    {
      //echo $request->get('query'); die();
       // \DB::enableQueryLog(); 
        $data = Product::select('id as data', 'medicine_name as value','image')->Where('medicine_name', 'like', '%' . $request->get('query') . '%')->limit(200)->get();
        //print_r(DB::getQueryLog()); die();
        return response()->json(['suggestions' => $data]);
    }


    public function update_product_doses(Request $request){

     // echo "<pre>"; print_r($request->all()); die();
     $order_item_id = decrypt($request->order_item_id); 
     //echo $order_item_id; die();
     if($request->type=='morning'){
       $status = OrderItem::where('ord_item_id',$order_item_id)->update(['morning' => 'm']);
     }elseif ($request->type=='night') {
       $status = OrderItem::where('ord_item_id',$order_item_id)->update(['night' => 'n']);
     }else{
       $status = OrderItem::where('ord_item_id',$order_item_id)->update(['evening' => 'e']);
     }

     if($status){
          $response['status'] = "success";
          $response['message'] = "You has been updated successfully.";
        }else{
          $response['status'] = "error";
          $response['message'] = "There was an issue account createing the recode. Please try again.";
        } 
        return json_encode($response);
    }

    public function add_medicine_extra(Request $request){
    //  try {
             //echo "<pre>"; print_r($request->all()); die();
             $orders  =   Order::where('id',decrypt($request->id))->first();
            // echo "<pre>"; print_r($orders); die();
             $product =  Product::where('id',decrypt($request->product_id_add))->first();
             $userdata = User::where('id',$orders->user_id)->first(); 


             /*Add medicine extra in product*/
              $order_id = decrypt($request->id);
              $product_id = $request->product_id_add;


            $query = Product::select('brand.brand_name','products.id','products.medicine_name','products.slug','products.mrp','products.sale_price','products.brand','products.prescription','products.type_of_sell','products.salt','products.is_featured','product_image.image','product_image.caption')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
               ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
                ->leftJoin('brand','brand.id','=','products.brand')
               ->leftJoin('product_image', function($join)
              {
                $join->on('product_image.product_id', '=', 'products.id');

              })
              ->where('products.status',1)
              ->where('products.id',decrypt($product_id))
              ->orderBy('products.id','DESC');

                $details=$query->first();
                //echo $details['mrp']; die();  
                $orderItem =  new orderItem();
                $orderItem->product_id = $details['id'];
                $orderItem->price = $details['mrp'];
                $orderItem->product_name = $details['name'];
                $orderItem->quantity_order = 1;
                $orderItem->added_by = 'doctor';
                $orderItem->order_id = $order_id;
                $orderItem->save();

                $user = User::all();
                Notification::send($user,new OrderPlace('New Product added by doctor'));


             /*Add medicine extra in product*/

              /*
               $user = array(
                  'user_id'=>$userdata->id,
                  'name'=>$userdata->name,
                  'product_id'=>$product->id,
                  'order_id'=>$request->order_id
               );
               // return $user['name'];
                // Send the activation code through email
               $send =  Mail::send('emails.welcome', compact('user'), function ($m) use ($user) {
                  $m->to('amit.giri@brsoftech.org', $user['name']);
                   $m->subject('Dear '. $user['name']. 'New Product Added By Doctor');
                });
               */

              
              $response['status'] = "success";
              $response['message'] = "You has been successfully New Product added.";
              return json_encode($response);
        /*  }
          catch(\Exception $e) {
              Session::flash('error', $e->getMessage());
          }*/
     
     }

     public function sendRequestToUser(Request $request){
             //echo "ok"; die();
             //echo "<pre>"; print_r($request->all()); die();
             $orders  =   Order::where('id',decrypt($request->order_id))->first();
             $userdata = User::where('id',$orders->user_id)->first(); 
              
              $user = array(
                  'user_id'=>$userdata->id,
                  'name'=>$userdata->name,
                  'order_id'=>$request->order_id
               );
              // Send the activation code through email
              $send =  Mail::send('emails.welcome', compact('user'), function ($m) use ($user) {
                  $m->to('amit.giri@brsoftech.org', $user['name']);
                   $m->subject('Dear '. $user['name']. 'New Product Added By Doctor');
              });
                
              $response['status'] = "success";
              $response['message'] = "You has been successfully send request to user.";
              return json_encode($response);
       
     
     }

     public function remove_product_items(Request $request){
            try {

            $id = decrypt($request->order_item_id);
            $post = OrderItem::where('ord_item_id',$id);
            $status = $post->delete();

            if($status){
              $response['status'] = "success";
              $response['message'] = "You has been successfully deleted this product.";
              return json_encode($response);
            }
            else{

              $response['status'] = "error";
              $response['message'] = "Error while deleting category";
              return json_encode($response);
            }
       } catch(\Exception $e) {
              Session::flash('error', $e->getMessage());
          }
     }


     public function setting(Request $request){
      return view('frontend.doctor.setting');
     }


     public function update_profile_user(Request $request){

        $data = $request->all();

       $doctor_id = Auth::guard('doctor')->user()->id;
       $doctor_data = DB::table('users')->select('*')->where('id',$doctor_id)->first();
       $doc_image = $doctor_data->image; 

        $validator = Validator::make($request->all() ,[
               'name' => 'required', 
               'mobile_number' => 'required|string|digits:10',
              // 'image' => 'required|mimes:png,jpeg,gif',
               'email' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
             $data = request()->except(['_token']);
             $user_id = Auth::guard('doctor')->user()->id;  
              if($request->hasFile('image')){
                $folder_name ='user';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }else{
                $data['image'] = $doc_image;
              }
             //  image
             $status = User::where('id',$user_id)->update($data);

                if ($status)
                {
                    $response['status'] = "success";
                    $response['message'] = "Doctor update Successfully.";
                    $response['url'] = route('setting');
                    return Response::json($response);
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Something went wrong";
                    return Response::json($response);
                }
        }

    }

    //Change password
     public function change_password(Request $request)
    {
        $input = $request->all();
        $userid = Auth::guard('doctor')->user()->id;
       if($request->type==1){ 
        $rules = array(
            'type' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            return response()
                ->json($validator->messages() , 422);
        } else {
            try {

                if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                    $arr = array("status" => "error", "message" => "Check your old password.", "data" => array());
                } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                    $arr = array("status" => "error", "message" => "Please enter a password which is not similar then current password.", "data" => array());
                } else {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("status" => "success", "message" => "Password updated successfully.");
                }


            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("status" => "error", "message" => $msg);
            }
        }
     }else{
        $rules = array(
            'type' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
           return response()
                ->json($validator->messages() , 422);
        } else {
            try {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("status" => "success", "message" => "Password updated successfully.");
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("status" => 'success', "message" => $msg);
            }
        } 
     }   
        return \Response::json($arr);
    }


    public function availablity(Request $request){
      //echo "HI"; die;
      $availablity_status = $request->get('data');
      //echo $availablity_status;die;

      $doctor_id = Auth::guard('doctor')->user()->id;

      if($availablity_status=='Yes'){ $status="No"; }else{ $status='Yes';}; 

       $status = User::where('id',$doctor_id)->update(['is_online'=>$status]);
      
       // if($status){
       //    Session::flash('success','Doctor availablity Updated Successfully.');
       // }else{
       //    Session::flash('error','Somthing went wrong.');
       // }
     // return Redirect()->route('admin.vendor.list');

       if(!empty($status)){
                $response['status'] = "success";
                $response['message'] = "Availablity has changed.";
                return json_encode($response);
          }else{
          $response['status'] = "error";
           $response['message'] = "Somthing went wrong.";
           //return json_encode($response);
          }


    }


    /*
     Completed Consultation By Doctor
    */

    public function completedConsultation(Request $request){
     $order_id = decrypt($request->order_id);
    // echo $order_id; die();
     $status =  Order::where('id',$order_id)->update(['order_status'=>'CC']);
      if ($status)
        {
             $url = route('orders');

            $response['status'] = "success";
            $response['url'] = $url;
            $response['message'] = "Doctor Completed Consultation  Successfully.";
            return Response::json($response);
        }
        else
        {
            $response['status'] = "error";
            $response['message'] = "Something went wrong";
            return Response::json($response);
        }
    }

     public function notification(Request $request){
       //$order_notification = Order::get();
       $doctor_id = Auth::guard('doctor')->user()->id; 

         $alert_notification = Notification::orderBy('id','DESC')->where('notifiable_id', $doctor_id)->get();

       return view('frontend.doctor.notification',compact('alert_notification'));
    }


       public function view_all($id){ 
       
         //echo $id; die;
         $current_date = date('Y-m-d H:i:s');

        Notification::where('notifiable_id',$id)->update(['read_at'=>$current_date]);
       return    response()->json([
            'success' => 'Data Updated successfully!'
        ]);
    }


    

    public function bank_details(Request $request){

       $data = $request->all();
       //echo "<pre>";print_r($data);die;
       $doctor_id = Auth::guard('doctor')->user()->id;
       $doctor_data = DB::table('doctor_bank_details')->select('*')->where('id',$doctor_id)->first();

        $validator = Validator::make($request->all() ,[
               'account_holder_name' => 'required', 
               'account_number' => 'required|confirmed|min:6',
               'account_number_confirmation'=>'required',
               'ifsc_code' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
             $data = request()->except(['_token','account_number_confirmation']);
             $user_id = Auth::guard('doctor')->user()->id;

             $bank_details_id = DoctorBankDetails::where('user_id',$user_id)->first();
             if(!empty($bank_details_id)){
             $status = DoctorBankDetails::where('user_id',$user_id)->update($data);
                if ($status)
                {
                    $response['status'] = "success";
                    $response['message'] = "Doctor update Successfully.";
                    $response['url'] = route('setting');
                    return Response::json($response);
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Something went wrong";
                    return Response::json($response);
                }
              }
              else{

                $bank = new DoctorBankDetails();
                
                $bank->account_holder_name = $request->account_holder_name;
                $bank->account_number = $request->account_number;
                $bank->ifsc_code = $request->ifsc_code;
                $bank->user_id = $doctor_id;
               
                $data = $bank->save();

             

               if ($data)
                {
                    $response['status'] = "success";
                    $response['url'] = route('setting');
                    $response['message'] = " create successfully.";
                    request()->session()
                        ->flash('success', 'You have been successfully create doctor');
                    return json_encode($response);

                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Somthing went wrong.";
                    return json_encode($response);

                }

              }
        }

    }





}
