<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Models\User;
use App\Models\OrderItem;
use App\Models\DoctorWorkInfo;
use App\Models\Order;
use App\Models\DoctorEducationInfo;
use App\Models\Doctor;
use App\Models\DeclineProducts;
use App\Models\OrderPayment;
use App\Models\DoctorSpecializationsInfo;
use App\Models\PaymentMethod;
use App\Models\VendorProductStatus;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use Carbon\Carbon;
use Validator;
use Response;
use Session;
use DB;
use URL;
use Auth;
use Crypt;

class DoctorRegisterController extends Controller
{
 use ImageTrait;
/*== Doctor Register View Page===*/
    public function register_view(){
      return view('frontend.doctor.doctor-register');  
    } 

    /*==Do Register Post==*/
    public function do_register(Request $request){
   //  DB::beginTransaction();
   //  try{   

            $validator = Validator::make($request->all(), [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'pin_code' => 'required',
                        'email' => 'unique:users,email',
                        'mobile_number' => 'unique:users,mobile_number',
                    ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{
/*Save Doctor Info ============================*/
                if($request->hasFile('image')){
                 $folder_name ='doctor-profile';
                 $doctorprofile = $this->fileUpload($request->file('image'),false,$folder_name);
                } 
                if($request->hasFile('image')){
                 $folder_name    ='signatureimage';
                 $signatureimage = $this->fileUpload($request->file('signature'),false,$folder_name);
                }
                $save =  new User ();
                $save->name = $request->first_name.' '.$request->last_name;
                $save->first_name = $request->first_name;
                $save->last_name = $request->last_name;
                $save->email = $request->email;
                $save->mobile_number = $request->mobile_number;
                $save->pin_code      = $request->pin_code;
                $save->registration_number      = $request->registration_number;
                $save->degree_name      = $request->degree_name;
                $save->image                   = $doctorprofile;
                $save->role = 'doctor';
                $status  = $save->save();
                $last_user_id = $save->id;
/*Doctore Info*/ 
                $data['signature_image'] = $signatureimage;
                $data['about_info']      = $request->about_info;
                $data['user_id']         = $last_user_id;
                Doctor::create($data);
/*Save Doctor Eduction Details ============================*/
                $subject    = $request->get('subject');
                $college    = $request->get('college');
                $start_year = $request->get('start_year_edu');
                $end_year   = $request->get('end_year_edu');
                $data = [];
                foreach ($request->college as $key => $value) {
                    $data['subject']      = $subject[$key];
                    $data['college_name'] = $college[$key];
                    $data['start_year']   = $start_year[$key];
                    $data['end_year']     = $end_year[$key];
                    $data['doctor_id']    = $last_user_id;
                    DoctorEducationInfo::create($data);
                }
/*Save Work & Experience ============================*/
           
                $woking_place_name    = $request->get('woking_place_name');
                 $start_year_work = $request->get('start_year_work');
                $end_year_work   = $request->get('end_year_work');
                $datainfo = [];
                foreach ($request->woking_place_name as $key => $value) {
                    $datainfo['woking_place_name']      = $woking_place_name[$key];
                     $datainfo['start_year']            = $start_year_work[$key];
                    $datainfo['end_year']               = $end_year_work[$key];
                    $datainfo['doctor_id']              = $last_user_id;
                    DoctorWorkInfo::create($datainfo);
                }
/*specializations Info ============================*/
           
                $specializations    = $request->get('specializations');
                $dataval = [];
                foreach ($request->specializations as $key => $value) {
                    $dataval['specializations_name']               = $specializations[$key];
                    $dataval['doctor_id']              = $last_user_id;
                    DoctorSpecializationsInfo::create($dataval);
                }
            if($status){
                $response['status'] = "success";
                 $response['message'] = "your account has been created successfully.";
                //$response['url']     = route('user.profile');
                return json_encode($response);
            }else{
                $response['status'] = "error";
                $response['message'] = "There was an issue account createing the recode. Please try again.";
                return json_encode($response);
            }
         }
       //DB::commit();   
      return json_encode($response);
      /* }catch(\Exception $e)
        {
          // DB::rollback(); 
           $msg = $e->getMessage();
           Session::flash('error', $msg);
           return redirect()->back()->withInput();
        }*/
    }   
}


