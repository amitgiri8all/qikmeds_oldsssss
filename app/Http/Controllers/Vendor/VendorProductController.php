<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\User;
use App\Models\Tag;
use App\Models\ImageProduct;
use App\Models\ProductTag;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductTemp;
use App\Models\Manufacturer;
use App\Models\ManufacturerTmp;
use App\Models\Product;
use App\Traits\ImageTrait;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use DataTables;
use Session;
use Response;
use DB;
use URL;
use XmlParser;
use Validator;
use Auth;
class VendorProductController extends Controller
{
     use ImageTrait;


  public function manage_device()
  {
   // echo Auth::guard('vendor')->user()->id; die();
    $manufacturer = Manufacturer::select('id','manufacturer_name')->orderBy('manufacturer_name','asc')->get();
    $allCategories = Category::orderBy('id','DESC')->where('parent_id','0')->where('vendor_cat','1')->get();

     $categories_data = Category::orderBy('id','ASC')->where('parent_id',0)->where('status','1')->get();

    $devices = Product::select('brand.brand_name','products.id','products.vendor_id','products.product_highlights','products.description','products.medicine_name','products.slug','products.mrp','products.qty','products.discount','products.sale_price','products.brand','products.prescription','products.type_of_sell','products.salt','products.is_featured','product_image.image','product_image.caption','manufacturer.manufacturer_name','categories.slug','product_category.category_id')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
        ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
        ->leftJoin('brand','brand.id','=','products.brand')
        ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->leftJoin('product_image', function($join)
        {
          $join->on('product_image.prod_id', '=', 'products.id');
        })
        ->where('products.status',1)
        ->where('categories.slug','devices')
        ->where('products.vendor_id',Auth::guard('vendor')->user()->id)
        ->groupBy('product_image.prod_id');
        $devices = $devices->get();
       // echo "<pre>"; print_r($devices); die();
        $surgicals = Product::select('brand.brand_name','products.id','products.vendor_id','products.product_highlights','products.description','products.medicine_name','products.slug','products.mrp','products.qty','products.discount','products.sale_price','products.brand','products.prescription','products.type_of_sell','products.salt','products.is_featured','product_image.image','product_image.caption','manufacturer.manufacturer_name','categories.slug','categories.category_name')->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('brand','brand.id','=','products.brand')
       ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')

         ->leftJoin('product_image', function($join)
        {
          $join->on('product_image.prod_id', '=', 'products.id');
        })
         ->where('categories.slug','surgical')
        ->where('products.status',1)
        ->where('products.vendor_id',Auth::guard('vendor')->user()->id)

        ->groupBy('product_image.prod_id');
        $surgicals = $surgicals->get();
    return view('frontend.vendor.manage_device',compact('manufacturer','allCategories','devices','surgicals','categories_data'));
  }

  public function fetchSub_category(Request $request)
    {
        $data['subcategory'] = Category::where("parent_id",$request->category_id)->where('parent_id','!=',0)->get(["category_name", "id"]);
        return response()->json($data);
    }


    public function add_vendor_product(Request $request){
        $request->validate([
            'product_name' => 'required',
            'manufacturer_id'=>'required',
            'price'=>'required',
            'qty'=>'required',
            'category'=>'required',
            'description'=>'required',
            'image' => 'required',
        ]);
       //Save Product
     // echo "<pre>"; print_r($request->all()); die();
        try {
         
            $save =  new Product();
            $save->medicine_name       = $request->product_name;
            $save->manufacturer_id     = $request->manufacturer_id;
            $save->mrp                 = $request->price;
            $save->sale_price          = $request->sale_price;
            $save->discount            = $request->discount;
            $save->description         = $request->description;
            $save->qty                 = $request->qty;
            $save->vendor_id                 = Auth::guard('vendor')->user()->id;
            $status  = $save->save();
            $last_user_id = $save->id; 

            //Category added
            $dataval['sub_category']     = $request->category_sub;
            $dataval['product_id']       = $last_user_id;
            $dataval['category_id']      = $request->category;
             ProductCategory::create($dataval);
            /*Save Image Details ============================*/

             if($request->hasFile('image')){
                $folder_name ='image';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }
              $data['prod_id'] = $last_user_id;
             ProductImage::create($data);
         if ($status) {
                    $response['status'] = "success";
                    $response['message'] = "your product has been created successfully.";
                    $response['url']     = route('manage-device');
                    return json_encode($response);
                } else {
                    $response['status'] = "error";
                    $response['message'] = "There was an issue account createing the recode. Please try again.";
                    return json_encode($response);
                } 
          } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
            }             
    }


    public function destroy($id){
     //echo $id; die();
    Product::where('id',$id)->delete();
    ProductCategory::where('product_id')->delete();
    ProductImage::where('prod_id')->delete();
    return response()->json([
        'success' => 'Record deleted successfully!'
    ]);
}

}
