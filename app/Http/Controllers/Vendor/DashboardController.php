<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Models\User;
use App\Models\Document;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Product;
use App\Models\Notification;
use App\Models\DeclineProducts;
use App\Models\OrderPayment;
use App\Models\VendorTrackOrder;
use App\Models\PaymentMethod;
use App\Models\VendorProductStatus;
use App\Models\SubscriptionMeta;
use App\Models\SubscriptionFaq;
use App\Models\SubscriptionPlan;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use Carbon\Carbon;
use Validator;
use Response;
use Session;
use DB;
use URL;
use Auth;
use Crypt;
use Hash;

class DashboardController extends Controller
{
  use HelperTrait;
  use ImageTrait;
  public function index()
  {
    $vendor_id = Auth::guard('vendor')->user()->id;

    $site = DB::table('site_settings')->where('id', 1)->first();
    $near_by_queue_time = date('H', strtotime($site->near_by_queue_time));

    $total_active_orders = Order::with('User', 'OrderPayment', 'DeliveryAddress', 'OrderItem')
      ->whereHas('OrderItem', function ($q) {
        $q->whereNull('vendor_id');
      })->where('created_at', '<', Carbon::now()->subMinutes($near_by_queue_time)->toDateTimeString())
       ->where('order_status','CC')->orWhere('order_status','PD')->orderBy('id','desc')->count();

    //$total_accepte_orders = OrderItem::where('vendor_id', $vendor_id)->where('seller_shipment_status','Received')->count();
       
    $total_accepte_orders = VendorProductStatus::where('status','fulfilled')->where('vendor_id',$vendor_id)->count();
    $totalRejectedOrders = DeclineProducts::where('type', 'vendor')->where('user_id', $vendor_id)->count();
    return view('frontend.vendor.dashboard', compact('total_accepte_orders', 'totalRejectedOrders','total_active_orders'));
  }


  /** Show Order for Vendor near by  **/
  public function order()
  {

    $site = DB::table('site_settings')->where('id', 1)->first();
    $near_by_queue_time = date('H', strtotime($site->near_by_queue_time));
    $city_queue_time    = date('H', strtotime($site->city_queue_time));
    $notifications_time = $city_queue_time + $near_by_queue_time;

    $vendor_orders = Order::with('User', 'OrderPayment', 'DeliveryAddress', 'OrderItem')
      ->whereHas('OrderItem', function ($q) {
        $q->whereNull('vendor_id');
      })->where('created_at', '>', Carbon::now()->subMinutes($near_by_queue_time)->toDateTimeString())
      ->where('order_status','CC')->orWhere('order_status','PD')->orderBy('id','desc')->get();

    $vendor_orders_city_quee = Order::with('User', 'OrderPayment', 'DeliveryAddress', 'OrderItem')
      ->whereHas('OrderItem', function ($q) {
        $q->whereNull('vendor_id');
      })->where('created_at', '<', Carbon::now()->subMinutes($near_by_queue_time)->toDateTimeString())
       ->where('order_status','CC')->orWhere('order_status','PD')->orderBy('id','desc')->get();



    return view('frontend.vendor.order', compact('vendor_orders', 'vendor_orders_city_quee'));
  }


   /** Show Accpted Orders for Vendor near by  **/
  public function accepted_orders(Request $request)
  {
     //echo "<pre>";print_r($request->all());die;
    //echo $request->get('start');die();
    //$start = date('Y-m-d', strtotime($request->get('start')));
    //$end   = date('Y-m-d', strtotime($request->get('end')));

    $start = $request->get('start');
    $end   = $request->get('end');
     //echo $start.'===='.$end; die;
    $vendor_id = Auth::guard('vendor')->user()->id;
    $vendor_orders = Order::with(['User', 'OrderPayment', 'DeliveryAddress', 'OrderItem'])
      ->whereHas('OrderItem', function ($q) use ($vendor_id) {
        $q->where('vendor_id', $vendor_id);
      });
    //check data if date between two dates
    /*if (!empty($request->get('start')) && !empty($request->get('end'))) {
      $vendor_orders->where('updated_at', '>=', $start)
                           ->where('updated_at', '<=', $end);
     }*/

     if (!empty($request->get('start')) && !empty($request->get('end'))) {
        $vendor_orders->whereBetween('created_at',[$start,$end]);
      }

    // if (!empty($request->get('start')) && !empty($request->get('end'))) {
    // $vendor_orders->whereDate('created_at',$request->get('start'));
    // }
    // if ($request->get('end') and !empty($request->get('end'))){
    // $vendor_orders->whereDate('created_at',$request->get('end'));
    // }

    $vendor_orders = $vendor_orders->get();
    //echo "<pre>";print_r(count($vendor_orders));die;
    return view('frontend.vendor.accept-orders', compact('vendor_orders'));
  }

  /** Order Validate Details Page**/
  public function order_validate(Request $request, $id = null)
  {
    $id = Crypt::decrypt($id);
    $order = Order::with('User', 'OrderPayment', 'DeliveryAddress', 'OrderItemVendorNull')
      ->where('id', $id)->first();
      
    return view('frontend.vendor.order_validate', compact('order'));
  }
  /** Accepted Order Details Page**/
  public function accept_order_detail(Request $request, $id = null)
  {
    $id = Crypt::decrypt($id);
    $vendor_id = Auth::guard('vendor')->user()->id;
    //echo $vendor_id; die();
    $vendor_orders = Order::with(['User', 'OrderPayment', 'DeliveryAddress', 'OrderItem' => function ($q) use ($vendor_id) {
      $q->where(['vendor_id' => $vendor_id]);
    }]);
    /*->whereHas('OrderItem', function($q) use ($vendor_id) {
                return $q->where('vendor_id',$vendor_id);
       });*/
    $order = $vendor_orders->where('id', $id)->first();
    //dd($order);
    //echo "<pre>"; print_r($order); die();
    return view('frontend.vendor.accept-order-detail', compact('order'));
  }

  /**Order Accepted**/
  public function accepte_orders(Request $request)
  {
    //echo "sdfas"; die();
    $vendor_id = Auth::guard('vendor')->user()->id;
    $order_id = $request->get('order_id');
    $ord_item_id = $request->get('ord_item_id');
    //Vendor Product Status 
    $status = new VendorProductStatus();
    $status->order_id = $order_id;
    $status->vendor_id = $vendor_id;
    $status->save();

    $track = new VendorTrackOrder();
    $track->order_id = $order_id;
    $track->vendor_id = $vendor_id;
    $track->date_time = Carbon::now();
    $track->track_status = 'Order in Progress';
    $track->save();

    if (!empty($ord_item_id)) {
      $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
        ->update(array('vendor_id' => $vendor_id));

      $get_all_unchecked_ids = OrderItem::select('ord_item_id')->whereNull('vendor_id')->where('order_id', $order_id)->get();
      if (!empty($get_all_unchecked_ids)) {
        foreach ($get_all_unchecked_ids as $key => $val) {
          $orderItem =  new DeclineProducts();
          $orderItem->ord_product_item_id = $val->ord_item_id;
          $orderItem->order_id = $order_id;
          $orderItem->user_id = $vendor_id;
          $orderItem->type = 'vendor';
          $orderItem->save();
        }
      }
    } else {
      $get_all_order_item = OrderItem::select('ord_item_id')->whereNull('vendor_id')->where('order_id', $order_id)->get();
      $ord_item_id = $get_all_order_item->pluck('ord_item_id');
      $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
        ->update(array('vendor_id' => $vendor_id));
    }
    if ($status) {
      $updateOrderStatus = OrderItem::where('order_id', $order_id)->whereNull('vendor_id')->count();
      if ($updateOrderStatus > 0) {
        $status = "PD";
      } else {
        $status = "AC";
      }
      Order::where('id', $order_id)
        ->update(array('order_status' => $status));
      $response['status'] = "success";
      $response['url'] =  URL('/vendor/accept-order-detail/'.encrypt($order_id));
      $response['loading'] =  'true';
      $response['message'] = "You has been created successfully accepte order.";
      return json_encode($response);
    } else {
      $response['status'] = "error";
      $response['message'] = "There was an issue account createing the recode. Please try again.";
      return json_encode($response);
    }
  }

  /**Order decline**/
  public function decline_orders(Request $request)
  {
    //return $request->all(); 
    $vendor_id = Auth::guard('vendor')->user()->id;
    $order_id = $request->get('order_id');
    $ord_item_id = $request->get('ord_item_id');
    //echo "<pre>"; print_r($ord_item_id); die;
    if (!empty($ord_item_id)) {
      $orderItem = [];
      foreach ($ord_item_id as $key => $val) {
        $orderItem =  new DeclineProducts();
        $orderItem->ord_product_item_id = $val;
        $orderItem->order_id = $order_id;
        $orderItem->type = 'vendor';
        $orderItem->user_id = $vendor_id;
        $orderItem->save();
      }
      if ($orderItem) {
        $response['status'] = "success";
        $response['loading'] =  'true';
        $response['message'] = "You  has been created successfully decline order.";
        return json_encode($response);
      } else {
        $response['status'] = "error";
        $response['message'] = "There was an issue account createing the recode. Please try again.";
        return json_encode($response);
      }
    } else {
        //echo "sadfasd"; die;
        $get_all_order_item = OrderItem::select('ord_item_id')->whereNull('vendor_id')->where('order_id', $order_id)->get();
        // $ord_item_id = $get_all_order_item->pluck('ord_item_id');
        $orderItem = [];
        //echo "<pre>"; print_r($get_all_order_item); die();
        foreach ($get_all_order_item as $key => $val) {
          $orderItem =  new DeclineProducts();
          $orderItem->ord_product_item_id = $val->ord_item_id;
          $orderItem->order_id = $order_id;
          $orderItem->type = 'vendor'; 
          $orderItem->user_id = $vendor_id;
          $orderItem->save();
        }
        if ($orderItem) {
          $response['status'] = "success";
          $response['loading'] =  'true';
          $response['message'] = "You  has been created successfully decline order.";
          return json_encode($response);
        } else {
          $response['status'] = "error";
          $response['message'] = "There was an issue account createing the recode. Please try again.";
          return json_encode($response);
        }
    }
  }

  public function updateVendorOrderProductStatus(Request $request)
  {
    $id = $request->id;
    $status = VendorProductStatus::find($id);
    $status = $status->status;
    if ($status == 'accept') {
      $status = 'fulfilled';
    } else {
      $status = 'accept';
    };
    $status = VendorProductStatus::where('id', $id)->update(['status' => $status]);
    if ($status) {
      $response['status'] = "success";
      $response['loading'] =  'true';
      $response['message'] = "You  has been updated successfully order.";
    } else {
      $response['status'] = "error";
      $response['message'] = "There was an issue account createing the recode. Please try again.";
    }
    return json_encode($response);
  }

  public function profile()
  {

    $vendor_id = Auth::guard('vendor')->user()->id;
    $profile_data = User::where('id', $vendor_id)->first();
    $doc_data = Document::where('user_id', $vendor_id)->first();

    // echo "<pre>"; print_r($profile_data);die;
    //dd($profile_data);
    return view('frontend.vendor.profile', compact('profile_data', 'doc_data'));
  }


  public function subscription()
  {
    $subscription_meta = SubscriptionMeta::where([ ['type','=','vendor'],['status','!=','0'] ])->get();
    $subscription_faq = SubscriptionFaq::where([ ['user_type','=','vendor'], ['status','!=','0'] ])->get();

    return view('frontend.vendor.subscription',compact('subscription_meta','subscription_faq'));
  }

  public function subscription_plan(Request $request){
     $subscription_meta = SubscriptionMeta::where([ ['type','=','vendor'],['status','!=','0'] ])->get();

    $subscription_faq = SubscriptionFaq::where([ ['user_type','=','vendor'], ['status','!=','0'] ])->get();

      $subscription_plan_data = SubscriptionPlan::where([ ['plan_type','=','vendor'],['status','!=','0'] ])->get();

      return view('frontend.vendor.subscription-plan',compact('subscription_faq','subscription_meta','subscription_plan_data'));
    }


  public function document(Request $request)
  {
    $vendor_id = Auth::guard('vendor')->user()->id;
    $document_data = Document::where('user_id', $vendor_id)->first();
    if (!empty($document_data)) {
      $document_id =  $document_data->user_id;
    }

    if (empty($document_id)) {

      $validator = Validator::make($request->all(), [
        // 'drug_license' => 'required',
        // 'gstin' => 'required',
        // 'signature' => 'required',
        // 'bank' => 'required',
        // 'educational_certificate' => 'required',

      ]);

      if ($validator->fails()) {
        return response()->json($validator->messages(), 422);
      } else {
        // DB::beginTransaction();
        // try{

          
        $document = new Document();
        $document->user_id        = Auth::guard('vendor')->user()->id;

        /*Image Uploads*/
        if($request->hasFile('drug_license') && $request->hasFile('gstin') && $request->hasFile('signature') && $request->hasFile('bank') && $request->hasFile('educational_certificate')){
          $folder_name = 'drug_license';
          $drug_license = $this->fileUpload($request->file('drug_license'), false, $folder_name);

          $folder_name = 'gstin';
          $gstin = $this->fileUpload($request->file('gstin'), false, $folder_name);

          $folder_name = 'signature';
          $signature = $this->fileUpload($request->file('signature'), false, $folder_name);

          $folder_name = 'bank';
          $bank = $this->fileUpload($request->file('bank'), false, $folder_name);

          $folder_name = 'educational_certificate';
          $educational_certificate = $this->fileUpload($request->file('educational_certificate'), false, $folder_name);

          $document->drug_license   = $drug_license;
          $document->gstin   = $gstin;
          $document->signature   = $signature;
          $document->bank   = $bank;
          $document->educational_certificate   = $educational_certificate;
        } if ($request->hasFile('drug_license')) {
          $folder_name = 'drug_license';
          $drug_license = $this->fileUpload($request->file('drug_license'), false, $folder_name);
          $document->drug_license   = $drug_license;
        } if ($request->hasFile('gstin')) {
          $folder_name = 'gstin';
          $gstin = $this->fileUpload($request->file('gstin'), false, $folder_name);
          $document->gstin   = $gstin;
        } if ($request->hasFile('signature')) {
          $folder_name = 'signature';
          $signature = $this->fileUpload($request->file('signature'), false, $folder_name);
          $document->signature   = $signature;

        } if ($request->hasFile('bank')) {
          $folder_name = 'bank';
          $bank = $this->fileUpload($request->file('bank'), false, $folder_name);
          $document->bank   = $bank;

        } if ($request->hasFile('educational_certificate')) {
          $folder_name = 'educational_certificate';
          $educational_certificate = $this->fileUpload($request->file('educational_certificate'), false, $folder_name);
          $document->educational_certificate   = $educational_certificate;

        }


        /*Image Uploads*/
        /* if($request->hasFile('drug_license')){
                $folder_name ='drug_license';
                $document['drug_license'] = $this->fileUpload($request->file('drug_license'),false,$folder_name);
            }

            if($request->hasFile('gstin')){
                $folder_name ='gstin';
                $document['gstin'] = $this->fileUpload($request->file('gstin'),false,$folder_name);
            }
            if($request->hasFile('signature')){
                $folder_name ='signature';
                $document['signature'] = $this->fileUpload($request->file('signature'),false,$folder_name);
            }
            if($request->hasFile('bank')){
                $folder_name ='bank';
                $document['bank'] = $this->fileUpload($request->file('bank'),false,$folder_name);
            }

             if($request->hasFile('educational_certificate')){
                $folder_name ='educational_certificate';
                $document['educational_certificate'] = $this->fileUpload($request->file('educational_certificate'),false,$folder_name);
            }
             */

       



        // Document::create($document);
        $document->save();

        return redirect()->back()->with('status', 'Inserted Successfully');
        /*} catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
              // DB::rollBack();
            }*/
      }
    } else {

      
      if ($request->hasFile('drug_license')) {
        $folder_name = 'drug_license';
        $drug_license = $this->fileUpload($request->file('drug_license'), false, $folder_name);
      }
      if ($request->hasFile('gstin')) {
        $folder_name = 'gstin';
        $gstin = $this->fileUpload($request->file('gstin'), false, $folder_name);
      }
      if ($request->hasFile('signature')) {
        $folder_name = 'signature';
        $signature = $this->fileUpload($request->file('signature'), false, $folder_name);
      }
      if ($request->hasFile('bank')) {
        $folder_name = 'bank';
        $bank = $this->fileUpload($request->file('bank'), false, $folder_name);
      }
      if ($request->hasFile('educational_certificate')) {
        $folder_name = 'educational_certificate';
        $educational_certificate = $this->fileUpload($request->file('educational_certificate'), false, $folder_name);
      }

      if (!empty($drug_license) && !empty($gstin) && !empty($signature) && !empty($bank) && !empty($educational_certificate)) {

        $data = Document::where("user_id", $document_id)->update(['drug_license' => $drug_license, 'gstin' => $gstin, 'signature' => $signature, 'bank' => $bank, 'educational_certificate' => $educational_certificate]);
      } if (!empty($drug_license)) {
        $data = Document::where("user_id", $document_id)->update(['drug_license' => $drug_license]);
      } if (!empty($gstin)) {
        $data = Document::where("user_id", $document_id)->update(['gstin' => $gstin]);
      } if (!empty($signature)) {
        $data = Document::where("user_id", $document_id)->update(['signature' => $signature]);
      } if (!empty($bank)) {
        $data = Document::where("user_id", $document_id)->update(['bank' => $bank]);
      } if (!empty($educational_certificate)) {
        $data = Document::where("user_id", $document_id)->update(['educational_certificate' => $educational_certificate]);
      }

      return redirect()->back()->with('status', 'Inserted Successfully');
    }
  }

  public function setting_index(){
    return view('frontend.vendor.setting-index');
  }

  public function update_profile_vendor(Request $request){
    //echo "<pre>"; print_r($request->all()); die();
       $data = $request->all();

       $vendor_id = Auth::guard('vendor')->user()->id;
       $vendor_data = DB::table('users')->select('*')->where('id',$vendor_id)->first();
       $ven_image = $vendor_data->image;   

        $validator = Validator::make($request->all() , ['name' => 'required', 'mobile_number' => 'required|string|digits:10','email' => 'required',]);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
             $data = request()->except(['_token']);
             $user_id = Auth::guard('vendor')->user()->id;  
              if($request->hasFile('image')){
                $folder_name ='user';
                $data['image'] = $this->fileUpload($request->file('image'),false,$folder_name);
              }
              else{
                $data['image'] = $ven_image;
              }
             //  image
             $status = User::where('id',$user_id)->update($data);

                if ($status)
                {
                    $response['status'] = "success";
                    $response['message'] = "Vendor update Successfully.";
                    $response['url'] = route('vendor.setting');
                    return Response::json($response);
                   // return redirect()->back()->with('status', 'Vendor update Successfully');
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Something went wrong";
                    return Response::json($response);
                }
        }


  }


    //Change password
     public function change_password(Request $request)
    { 
       //echo "<pre>"; print_r($request->all());die;
        $input = $request->all();
        $userid = Auth::guard('vendor')->user()->id;
       if($request->type==1){ 
        $rules = array(
            'type' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            return response()
                ->json($validator->messages() , 422);
        } else {
            try {

                if ((Hash::check(request('old_password'), Auth::guard('vendor')->user()->password)) == false) {
                    $arr = array("status" => "error", "message" => "Check your old password.", "data" => array());
                } else if ((Hash::check(request('new_password'), Auth::guard('vendor')->user()->password)) == true) {
                    $arr = array("status" => "error", "message" => "Please enter a password which is not similar then current password.", "data" => array());
                } else {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("status" => "success", "message" => "Password updated successfully.");
                }


            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("status" => "error", "message" => $msg);
            }
        }
     }else{
        $rules = array(
            'type' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
           return response()
                ->json($validator->messages() , 422);
        } else {
            try {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("status" => "success", "message" => "Password updated successfully.");
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("status" => 'success', "message" => $msg);
            }
        } 
     }   
        return \Response::json($arr);
    }


    public function availablity(Request $request){
       $availablity_status = $request->get('data');
 
      $vendor_id = Auth::guard('vendor')->user()->id;

      if($availablity_status=='Yes'){ $status="No"; }else{ $status='Yes';}; 

       $status = User::where('id',$vendor_id)->update(['is_online'=>$status]);
       if(!empty($status)){
                $response['status'] = "success";
                $response['message'] = "Availablity has changed.";
                return json_encode($response);
          }else{
          $response['status'] = "error";
           $response['message'] = "Somthing went wrong.";
          }
    }


     public function notification(Request $request){
        $vendor_id = Auth::guard('vendor')->user()->id; 

         $alert_notification = Notification::orderBy('id','DESC')->where('notifiable_id', $vendor_id)->get();

       return view('frontend.vendor.notification',compact('alert_notification'));
    }


     public function edit_profile_vendor(Request $request){
        
        //echo "<pre>";print_r($request->all());die;   
        $data = $request->all();

       $validator = Validator::make($request->all() , ['registration_number'=>'required|min:6']);
        if ($validator->fails())
        {
            return response()
                ->json($validator->messages() , 422);
        }
        else
        {
             $data = request()->except(['_token']);
             $vendor_id = Auth::guard('vendor')->user()->id;  

            $data['name'] = $request->name;
            $data['farm_name'] = $request->farm_name;
            $data['address'] = $request->address;
            $data['pin_code'] = $request->pin_code; 
            $data['registration_number'] = $request->registration_number;
            $data['comm_medicine'] = $request->comm_medicine;
            $data['comm_beauty_pro'] = $request->comm_beauty_pro;
            $data['comm_device_surgiacl'] = $request->comm_device_surgiacl;
             
             
             $status = User::where('id',$vendor_id)->update($data);

                if ($status)
                {
                    $response['status'] = "success";
                    $response['message'] = "Vendor update Successfully.";
                    $response['url'] = route('vendor.update_profile_vendor');
                    return redirect()->back()->with('status', 'Vendor update Successfully');
                    return Response::json($response);
                }
                else
                {
                    $response['status'] = "error";
                    $response['message'] = "Something went wrong";
                     return redirect()->back()->with('status', 'Vendor update Successfully');
                    // $response['url'] = route('vendor.profile');

                    return Response::json($response);
                }
       }

    }


      public function view_all($id){ 
       
         //echo $id; die;
         $current_date = date('Y-m-d H:i:s');

        Notification::where('notifiable_id',$id)->update(['read_at'=>$current_date]);
       return    response()->json([
            'success' => 'Data Updated successfully!'
        ]);
    }




}
