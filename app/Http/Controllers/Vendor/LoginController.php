<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Traits\ImageTrait;
use Validator;
use Session;
 use Response;
use Auth;
use Redirect;
use Hash;
class LoginController extends Controller
{
      use ImageTrait;

      // Define All Properties -----------
    protected $user;
    protected $method;
    // Define Constructer Methode ----------- 
    function __construct(Request $request,User $user){
        // Initialize the object and its properties by assigning value(models)----------- 
         $this->user             = $user;
        $this->method           = $request->method();
 
    }

    /*== Register View Page===*/
    public function register(){
      return view('frontend.vendor.vendor-register');  
    }

    /*==Do Register Post==*/
    public function do_register(Request $request){

            $validator = Validator::make($request->all(), [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'unique:users,email',
                        'mobile_number' => 'unique:users,mobile_number',
                        'password' => 'required',
                        'confirm-password' => 'required_with:password|same:password',
                        'city' => 'required',
                        'state' => 'required',
                        'country' => 'required',
                        'farm_name' => 'required',
                        'pin_code' => 'required',
                        'address' => 'required',
                        'image' => 'required',
                    ]);
        if ($validator->fails()){
               return response()->json($validator->messages(), 422);
         }else{
            
                if($request->hasFile('image')){
                 $folder_name ='vendor';
                 $image = $this->fileUpload($request->file('image'),false,$folder_name);
                }
                $save =  new User ();
                $save->name = $request->first_name.' '.$request->last_name;
                $save->first_name = $request->first_name;
                $save->last_name = $request->last_name;
                $save->email = $request->email;
                $save->mobile_number = $request->mobile_number;
                $save->city = $request->city;
                $save->state = $request->state;
                $save->country = $request->country;
                $save->address = $request->address;
                $save->pin_code = $request->pin_code;
                $save->farm_name = $request->farm_name;
                $save->address_location = $request->address_location;
                $save->latitute = $request->latitute;
                $save->longitute = $request->longitute;
                $save->password = Hash::make($request->password);
                $save->role = 'vendor';
                $save->image = $image;
            
            $status  = $save->save();
            if($status){
                $response['status'] = "success";
                $response['loading'] =  'true';
                $response['url'] =  route('account.login');
                $response['message'] = "your account has been created successfully.";

                //$response['url']     = route('user.profile');
                return json_encode($response);
            }else{
                $response['status'] = "error";
                $response['message'] = "There was an issue account createing the recode. Please try again.";
                return json_encode($response);
            }
         }
      return json_encode($response);
    }
}
