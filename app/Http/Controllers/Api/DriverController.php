<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UserWallet;
use App\Models\Driver;
use App\Models\DeclineProducts;
use App\Models\AdminNotification;
use App\Models\Notification;
use App\Models\Zone;
use App\Models\DriverSessionTime;
use App\Models\PaymentMethod;
use App\Notifications\AllOrderStatus;
use App\Notifications\OrderAccept;
use Illuminate\Support\Str;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Validator;
use Paginator;

use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB; 
use App;
use Session;
use Mail;

class DriverController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;
    /*Here is call Construct Method*/
    function __construct(User $user,UserWallet $userWallet,Driver  $driver,Zone $zone){
        $this->user = $user;
        $this->driver = $driver;
        $this->userWallet = $userWallet;
        $this->zone     = $zone;
    }


    //Driver Register =============
    public function driver_register(Request $request){
        //Here is put validation form fildes ===============
        $validator = Validator::make($request->all(), [
            'device_type'=>'required|in:A,I',
            'name' => 'required',
            'device_id' => 'required',
            'address' => 'required',
            'mobile_number' => 'required|max:10|unique:users,deleted_at,NULL',
            'email' => 'required|email|unique:users,deleted_at,NULL',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        try { 
            $otp = '123456';
            $user_temp = new UserTemp();
            $user_temp->mobile_number = $request->mobile_number;    
            $user_temp->otp           = $otp;
            $user_temp->name          = $request->name;
            $user_temp->device_type   = $request->device_type;
            $user_temp->device_id     = $request->device_id;
            $user_temp->address       = $request->address;
            $user_temp->email       = $request->email;
            $user_temp->save();
            $response = [
                'code' => 200,
                'otp' => $otp,
                'user_type' => 'new',
                'message' => 'OTP sent successfully.'
            ];
            return response()->json($response, 200); 
        } catch (\Exception $e) {
            return $e;
        } 
          
    }

    
    // Driver Verify Opt
    public function verify_otp(Request $request) {
         $response = array();
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'expire' => 'required',
            'type'=>'required|in:1,2',
            'otp' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        if($request->expire!=1){
                $response['code'] = 1;
                $response['message'] = 'OTP expired, Please try again !';
        }else{
            if($request->type==2){
                    $verifyOtp          =   UserTemp::where('mobile_number',$request->mobile_number)->first();
                    if(empty($verifyOtp)){
                        $response["code"]           =   0; 
                        $response["message"]        =   trans("Invalid details");
                        $response["data"]           =   (object)array();
                        return response()->json($response,203);
                    }else{

                        if ($verifyOtp->otp == $request->otp) {
                            $getuser          =   User::select('id','mobile_number')->where('role','driver')->where('mobile_number',$request->mobile_number)->first(); 
                              if(empty($getuser)){
                                $user_temp                = new User();
                                $user_temp->mobile_number = $verifyOtp->mobile_number;    
                                $user_temp->device_type   = $verifyOtp->device_type;
                                $user_temp->device_id     = $verifyOtp->device_id;
                                $user_temp->email         = $verifyOtp->email;
                                $user_temp->address       = $verifyOtp->address;
                                $user_temp->name       = $verifyOtp->name;
                                $user_temp->status       = '0';
                                $user_temp->otp           = null;
                                $user_temp->role           = 'driver';
                                $user_temp->is_mobile_number_verifed = 1;
                                $user_temp->save();
                                 UserTemp::where('mobile_number', $request->mobile_number)->delete();
                           }
                           UserTemp::where('mobile_number',$request->mobile_number)->where('otp',$request->otp)->update(array('otp_verify'=>1));
                           $user_details          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 

                            $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                            $response["code"]          =   200; 
                            $response["message"]       =   trans("Your mobile number has been verified successfully.");
                            $response["data"]          =   $user_details;
                            $response['data']['token'] = $refreshed;
                            return response()->json($response,200);
                        }else{
                            $response["code"]         =   0; 
                            $response["message"]        =   trans("Invalid OTP.");
                            $response["data"]           =   (object)array();
                            return response()->json($response,203);
                        }
                    }
            }else{
                //Driver verify otp 
                    $verifyOtp          =   User::where('mobile_number',$request->mobile_number)->first();
                    if(empty($verifyOtp)){
                        $response["code"]           =   0; 
                        $response["message"]        =   trans("Invalid details");
                        $response["data"]           =   (object)array();
                        return response()->json($response,203);
                    }else{
                        if ($verifyOtp->otp == $request->otp) {
                            $getuser          =   User::select('id','mobile_number')->where('role','driver')->where('mobile_number',$request->mobile_number)->first(); 

                           $user_details          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 
                            $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                            $response["code"]          =   200; 
                            $response["message"]       =   trans("Your mobile number has been verified successfully.");
                            $response["data"]          =   $user_details;
                            $response['data']['token'] = $refreshed;
                            return response()->json($response,200);
                        }else{
                            $response["code"]         =   0; 
                            $response["message"]        =   trans("Invalid OTP.");
                            $response["data"]           =   (object)array();
                            return response()->json($response,203);
                        }
                    }    
            }         
        }
        return response()->json($response, 201);
    }

    // Resend OTP ===================
    public function resend_otp(Request $request) {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'resend_type' => 'required'
        ]);
        if ($validator->fails()){
            $response = [
                'code' => 0,
                'error' => true,
                'message' => $validator->errors()->first(),
            ];
            
            return response()->json($response, 200);
        }
        /* 1=Register OTP, 2=Login OTP */
        if ($request->resend_type==1) {
            //echo "1"; die();
            $otp_details = DB::table('user_temp')->where('mobile_number', $request->mobile_number)->first();
            $otp = $otp_details->otp;
           //print_r($otp_details);die;
            if (isset($otp_details) && !empty($otp_details)) {
                if (isset($otp_details->otp) && !empty($otp_details->otp)) {

                  // $otp = rand(1000, 9999);
                 $otp = '123456';
                    DB::table('user_temp')->where('mobile_number',$request->mobile_number)->update(array('otp'=>$otp));
                    $dataotp = DB::table('user_temp')->select('mobile_number','otp')->where('mobile_number', $request->mobile_number)->first();
                    //return $dataotp;
                    $response_data['message'] = 'OTP sent successfully.';
                    $response_data['code'] = 200;
                    $response_data['data'] =$dataotp;
                } else {
                    $response_data['code'] = 1;
                    $response_data['message'] = 'OTP not send.';
                }
            } else {
                $response_data['code'] = 1;
                $response_data['message'] = 'OTP not Match.';
            }
        } else if ($request->resend_type==2) {
            //echo "ok sure";; die();
            $otp_details = DB::table('users')->where('role','driver')->where('mobile_number', $request->mobile_number)->first();
           // echo "<pre>"; print_r($otp_details); die();
            if (isset($otp_details) && !empty($otp_details)) {
              //  echo "sdafasd"; die();
                    $otp = '123456';
                    User::where('mobile_number',$request->mobile_number)->where('role','driver')->update(array('otp'=>$otp));
                    $dataotp = User::where('mobile_number', $request->mobile_number)->where('role','driver')->first();
                    $response_data['message'] = 'OTP sent successfully....';
                    $response_data['code'] = 200;
                    $response_data['data'] =$dataotp;

            } else {
                $response_data['code'] = 1;
                $response_data['message'] = 'OTP not Match.';
            }
        } else {
            $response_data['code'] = 1;
            $response_data['message'] = 'Invalid selection.';
        }
        
        $response_data['error'] = false;
        return response()->json($response_data, 200);
    }

    //Driver login =============
    public function driver_login(Request $request){
        //Here is put validation form fildes ===============
        $validator = Validator::make($request->all(), [
            'device_type'=>'required|in:A,I',
            'device_id' => 'required',
            'mobile_number' => 'required|max:10',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        try { 
          //  DB::enableQueryLog(); 
            $check_user = User::where('mobile_number',$request->mobile_number)->where('role','=','driver')->count();
           // echo "<pre>"; print_r(DB::getQueryLog()); die();

          //  echo "<pre>"; print_r($check_user); die('okkk');
           // return $check_user;
            //echo "==".$check_user; die();

           if($check_user>0){
                $loginuser = User::where('mobile_number',$request->mobile_number)->where('role','driver')->first();
             // if($loginuser->status != 0){
                $otp = '123456';
                User::where('mobile_number',$request->mobile_number)->where('role','driver')->update(array('otp'=>$otp));
                $dataotp = User::where('mobile_number', $request->mobile_number)->first();
                 $dataotp['zone_data'] = json_decode($dataotp->zone_data);
                $response['message'] = 'OTP sent successfully.';
                $response['code'] = 200;
                $response['data'] =$dataotp;
                return response()->json($response, 200); 
               // }else{
               //  $response['message'] = 'Your account has not been activated.';
               //  $response['code'] = 0;
               //  return response()->json($response, 200); 
               // } 
           }else{
            $response['message'] = 'mobile number is not exit.';
            $response['code'] = 0;
           return response()->json($response); 
           }

        } catch (\Exception $e) {
            return $e;
        } 
    }

     //Driver Profile
        public function driver_profile(Request $request){
         $user_id = Auth::guard('api')->user()->id;
        //  $user_id =1;
          $data = User::with('DriverData','DriverDataBank')->where('id',$user_id)->first();
          $is_doc = Driver::where('user_id',$user_id)->count();
          $data['id_document'] = $is_doc;
            if(!empty($data->zone_data)){
              $data['zone_data'] = json_decode($data->zone_data);
             }else{
               $data['zone_data'] = [];
             }        
               return $this->showResponse($data,'');
        } 


      public function upload_document(Request $request){
        try {

            $user_id = Auth::guard('api')->user()->id;
            $validator = Validator::make($request->all(), [
                'aadhar_card'   =>  'required|image|mimes:jpeg,png,jpg',
                'pan_card'   =>  'required|image|mimes:jpeg,png,jpg',
                'driving_license'   =>  'required|image|mimes:jpeg,png,jpg',
                'plate_number'   =>  'required|image|mimes:jpeg,png,jpg',
                'insurance'   =>  'required|image|mimes:jpeg,png,jpg',
                'vehicle_registration_certificate'   =>  'required|image|mimes:jpeg,png,jpg',
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $input = $request->all();

            if ($request->hasFile('aadhar_card')) {
              $folder_name = 'aadhar-card';
              $input['aadhar_card'] = $this->fileUpload($request->file('aadhar_card'), false, $folder_name);
            }

            if ($request->hasFile('pan_card')) {
              $folder_name = 'pan_card';
              $input['pan_card'] = $this->fileUpload($request->file('pan_card'), false, $folder_name);
            }  

            if ($request->hasFile('driving_license')) {
              $folder_name = 'driving_license';
              $input['driving_license'] = $this->fileUpload($request->file('driving_license'), false, $folder_name);
            }

            if ($request->hasFile('plate_number')) {
              $folder_name = 'plate_number';
              $input['plate_number'] = $this->fileUpload($request->file('plate_number'), false, $folder_name);
            }

            if ($request->hasFile('insurance')) {
              $folder_name = 'insurance';
              $input['insurance'] = $this->fileUpload($request->file('insurance'), false, $folder_name);
            } 

            if ($request->hasFile('vehicle_registration_certificate')) {
              $folder_name = 'vehicle_registration_certificate';
              $input['vehicle_registration_certificate'] = $this->fileUpload($request->file('vehicle_registration_certificate'), false, $folder_name);
            }
            
            $pan_card = $input['pan_card'];
            $aadhar_card = $input['aadhar_card'];
            $driving_license = $input['driving_license'];
            $plate_number = $input['plate_number'];
            $insurance = $input['insurance'];
            $vehicle_registration_certificate = $input['vehicle_registration_certificate'];

            $check_user = $this->driver->where('user_id',$user_id)->count();
            //echo "===".$check_user; die();
            //echo "===".; die(); 
            if($check_user>0){
                Driver::where('user_id',$user_id)->update(['pan_card'=>$pan_card,'aadhar_card'=>$aadhar_card,'driving_license'=>$driving_license,'plate_number'=>$plate_number,'insurance'=>$insurance,'vehicle_registration_certificate'=>$vehicle_registration_certificate]);
                $response = [
                    'code' => 200,
                    'message' => 'Document Upload successfully.'
                ];
                return response()->json($response, 200); 
            }else{
              $save = new Driver();
              $save->user_id = $user_id;
              $save->aadhar_card = $pan_card;
              $save->pan_card    = $aadhar_card;
              $save->driving_license    = $driving_license;
              $save->plate_number    = $plate_number;
              $save->insurance    = $insurance;
              $save->vehicle_registration_certificate    = $vehicle_registration_certificate;
              $save->save();
              $response = [
                'code' => 200,
                'message' => 'Document Upload successfully.'
              ];
              return response()->json($response, 200); 
            }
        }catch(\Exception $e){
         return $e;
       }    
    }

  //Driver Order list //
        public function order_list()
        {
         try {
              $user_id = Auth::guard('api')->user()->id;
              $remove_ids_order = DeclineProducts::select('order_id')->where('user_id',$user_id)->where('type','driver')->pluck('order_id')->toArray();  
              $data = Order::with('User','OrderPayment','OrderItem')->whereNotIn('id',$remove_ids_order)->where('order_status','AC')->orderBy('id', 'DESC')->get();
              foreach ($data as $key => $value) {
                  $data[$key]['patient_info'] = json_decode($value->patient_info);
                  $data[$key]['delivery_address_info'] = json_decode($value->delivery_address_info);
                  $data[$key]['offer_data'] = json_decode($value->offer_data);
                  $data[$key]['user']['zone_data'] = json_decode($value->user->zone_data);
              }
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data); 

    } 



    //Driver Order Status accept / reject

    public function booking_status(Request $request){

            $user_id = Auth::guard('api')->user()->id;
            $driver_id = Auth::guard('api')->user()->id;
           // echo $user_id; die();    
            $validator = Validator::make($request->all(), [
                'order_id'   =>  'required',
                'status'   =>  'required|in:0,1',  //Status = 1 accept 2 = Reject 
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $order_id = $request->order_id;

            if($request->status==1){
                    $status = OrderItem::where('order_id', $order_id)
                    ->update(array('driver_id' => $user_id));
                    $status = Order::where('id', $order_id)
                    ->update(array('order_status' =>'AD'));
                    

                /*send notification to customer*/
                  $order = Order::where('id',$request->order_id)->first();  
                  $user_id_array1 = User::where('id',$driver_id)->select('id','device_type','device_token','name')->get();
                  $userData = User::where('id', '=',$driver_id)->select('device_token')->get();
                  $user_id_array = collect($userData)->pluck('device_token');
                  $senderName = $user_id_array1[0]->name;
                  $message    = '#'.$order->order_code.trans('Accepted order');
                  $type       = 'Accepted order';
                 // $order->user->notify(new OrderAccept($order,$senderName,$message,$type,$user_id));
                /*$saveinfo =  [
                    'status' => $order->order_status,
                    'message' => $message,
                    'type' => $type,
                    'sender' => $senderName,
                    'order_code' => $order->order_code,
                    'order_id' => $order->id,
                    'created_at' => now(),
                    ];*/
                  //echo "<pre>"; print_r($saveinfo); die();  

                  /*$save = new Notification();
                  $save->id = '03c7469e-46fb-44ba-a63d-8714700ed7'.rand(10,100); 
                  $save->data =  json_encode($saveinfo, true);
                  $save->type = 'App\Notifications\OrderAccept'; 
                  $save->notifiable_type = 'App\Models\User'; 
                  $save->notifiable_id = $user_id;
                  $save->save();*/
                /*=================================*/
                $response = [
                  'code' => 200,
                  'message' => 'You has been created successfully accepte order.'
                ];
                return response()->json($response, 200); 
            }else{
                $ord_item_id = OrderItem::select('ord_item_id')->where('order_id',$order_id)->get();
                $orderItem = [];
                foreach ($ord_item_id as $key => $val) {
                   // echo $val->ord_item_id; die();
                    $orderItem =  new DeclineProducts();
                    $orderItem->ord_product_item_id = $val->ord_item_id;
                    $orderItem->order_id = $order_id;
                    $orderItem->type = 'driver';
                    $orderItem->user_id = Auth::guard('api')->user()->id;
                    $orderItem->save();
                }
                if ($orderItem) {
                  $response['code'] = 200;
                  $response['message'] = "You  has been created successfully decline order.";
                return json_encode($response);
                } else {
                  $response['code'] = 200;
                  $response['message'] = "There was an issue account createing the recode. Please try again.";
                return json_encode($response);
                }
            }
    }

    public function accept_driver_orders(Request $request){
     try {
         $driver_id = Auth::guard('api')->user()->id;
        //  echo $driver_id; die();

        $accepted_doctor_orders  = Order::with(['User','OrderPayment','OrderItem'])
          ->whereHas('OrderItem', function($q) use ($driver_id) {
          $q->where('driver_id',$driver_id);
        });
        $data = $accepted_doctor_orders->get();
       // echo "<pre>"; print_r($data); die();
        foreach ($data as $key => $value) {
               //$zone_data = isset($value->user->zone_data)?$value->user->zone_data:'';
                //echo $zone_data
            $data[$key]['patient_info'] = json_decode($value->patient_info);
            $data[$key]['delivery_address_info'] = json_decode($value->delivery_address_info);
            $data[$key]['offer_data'] = json_decode($value->offer_data);
            $data[$key]['user']['zone_data'] = json_decode($value->user->zone_data);

        }
        return $this->showResponse($data); 
       //Vendor ------------------
      } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
      }

    }

    //Order Details
     public function order_details(Request $request){
       $user_id = Auth::guard('api')->user()->id;
            $validator = Validator::make($request->all(), ['order_id'   =>  'required',]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
         try {
            $driver_id = Auth::guard('api')->user()->id;
            $order_id = $request->order_id;
            $accepted_doctor_orders  = Order::with(['User','OrderPayment','OrderItem'])->where('id',$order_id)
              ->whereHas('OrderItem', function($q) use ($driver_id) {
              $q->where('driver_id',$driver_id);
            });
            $data = $accepted_doctor_orders->first();
            $data['patient_info'] = json_decode($data->patient_info,true);
            $data['delivery_address_info'] = json_decode($data->delivery_address_info,true);
            $data['offer_data'] = json_decode($data->offer_data,true);
            $data['user']['zone_data'] = json_decode($data->user->zone_data);


            $vendor_ids = OrderItem::select('vendor_id')->where('order_id',$order_id)->where('driver_id',$driver_id)->pluck('vendor_id')->toArray(); 
           // return $vendor_ids;>
            $vendor_info = User::whereIn('id',$vendor_ids)->get();
                //echo "<pre>"; print_r($vendor_info[0]->zone_data); die();
           // return $vendor_info->zone_data; 
            $data['vendor_info'] = $vendor_info;
            $data['vendor_info'][0]['zone_data'] = json_decode($vendor_info[0]->zone_data);
      } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
      }
        return $this->showResponse($data);    
             
     }

     public function update_driver_status(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'order_id'   =>  'required',
                'status'   =>  'required|in:picked,unpicked',  //Status = 1 accept 2 = Reject 
                'vendor_id'   =>  'required',  //Status = 1 accept 2 = Reject 
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $order_id = $request->order_id;
            $vendor_id = $request->vendor_id;
            $status = $request->status;
            $status= OrderItem::where('vendor_id',$vendor_id)->where('order_id',$order_id)->update(['driver_status'=>$status]);
             $response = [
                'code' => 200,
                'message' => 'Order picked',
              ];
            return response()->json($response, 200); 

        } catch (\Exception $e) {
             return $this->clientErrorResponse($e);
        }
     }
     
    public function trip_history(Request $request){
       // echo "ok die"; die();
          try{
            //echo Carbon::now()->subMonth()->month; die();
                $validator = Validator::make($request->all(), [
                    'type'   =>  'required|in:1,2,3',  //Type = 1 today 2 = Week  3 Months 
                ]);
                if ($validator->fails()) {
                    return $this->validationErrorResponse($validator);
                }
            $type = $request->type;

            $driver_id = Auth::guard('api')->user()->id;
            $accepted_doctor_orders  = Order::with(['User','OrderPayment','OrderItem'])->where('order_status','D')
              ->whereHas('OrderItem', function($q) use ($driver_id) {
              $q->where('driver_id',$driver_id);
            });
            
            if($type==1){
                $accepted_doctor_orders->whereDate('created_at',date('Y-m-d'));
            }elseif($type==2){
                $accepted_doctor_orders->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
            }else{
                $accepted_doctor_orders->whereMonth('created_at', '=', Carbon::now()->subMonth()->month);
            }  
            $data = $accepted_doctor_orders->get();

            foreach ($data as $key => $value) {
                $data[$key]['patient_info'] = json_decode($value->patient_info);
                $data[$key]['delivery_address_info'] = json_decode($value->delivery_address_info);
                $data[$key]['offer_data'] = json_decode($value->offer_data);
                $data[$key]['user']['zone_data'] = json_decode($value->user->zone_data);

            }
            $datacount =  count($data);
            //return $data;
           if($datacount>0){
          //  echo "ok"; die();
               $response = [
                'code' => 200,
                'data' => $data,
                'message' => 'Trip history',
              ];
           }else{
           // echo "not data"; die();
                $response = [
                 'code' => 0,
                 'message' => 'No data found.',
                ];
           }
           return response()->json($response, 200); 
          //  return $this->showResponse($data); 

          } catch (\Exception $e) {
                 return $this->clientErrorResponse($e);
            }
         }

//Add bank account deatils
         
        public function add_bank_details(Request $request){
         try {   
            $user_id = Auth::guard('api')->user()->id;  
            $validator = Validator::make($request->all() ,[
             'bank_name' => 'required',
             'ifce_code' => 'required',
             'card_number' => 'required', 'unique:payment_method,card_number',
             'card_number_confirmation' => 'required_with:card_number|same:card_number'
            ]);

            if ($validator->fails()) {
             return $this->validationErrorResponse($validator);
            }  
            
            PaymentMethod::where('user_id',$user_id)->delete();

            $data = $request->all();
            $data['user_id'] = $user_id;    
            $status = PaymentMethod::create($data);

            $response["code"]          =   200; 
            $response["message"]       =   trans("Your bank account has been successfully added.");
            return response()->json($response,200);

          } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
          }

    }

    public function delivery_address_zone(Request $request){
        try{
            $validator = Validator::make($request->all() ,[
             'search' => 'required',
            ]);
            if ($validator->fails()) {
                 return $this->validationErrorResponse($validator);
            }  
            $name = $request->search;
            $data = DB::table('zones')->select('id','name','description','status')->where('name', 'LIKE', "%$name%")->get();
            $datacount =  count($data);
            if($datacount>0){
                   $response = [
                    'code' => 200,
                    'data' => $data,
                    'message' => 'Delivery address info',
                  ];
            }else{
                    $response = [
                     'code' => 0,
                     'message' => 'No data found.',
                    ];
            }
            return response()->json($response, 200); 
           }catch (\Exception $e) {
              return $this->clientErrorResponse($e);
            }
         }


  public function driver_notification(request $request)
    {
         $a = [];
        $userId = Auth::guard('api')->user()->id;
            $notifications = Auth::guard('api')->user()->notifications(['data'])->orderBy('created_at','DESC')->get();
            $notifications->transform(function($item, $key) 
            {
                $data = $item->data;
                $data['title']= "Notification";
                $data['type']= "Order Notifications";
                $data['decription']= $data['message'];
                $data['date']= $item['created_at']->format('d-m-Y');
               $data['from_timestamp'] = date('h:i A',strtotime($item['created_at']));
                $item->data = $data;
                return $item->data;
            });
            $notifications = $notifications->toArray();
            $merged = collect($notifications);
        return $this->showResponse($merged);
    }

    public function update_driver_login_status(Request $request){
         try {   
            $user_id = Auth::guard('api')->user()->id; 
            //echo "==".$user_id; die(); 
            $validator = Validator::make($request->all() ,[
             'status' => 'required|in:Yes,No',
            ]);
            if ($validator->fails()) {
             return $this->validationErrorResponse($validator);
            }  
            $status = $request->status;

            if($status=='Yes'){
              $save = new DriverSessionTime ();
              $save->start_time = date('Y-m-d H:i:s');
              $save->user_id = $user_id;
              $save->save();  
              DriverSessionTime::where('id',$save->id)->update(['is_online_status'=>$status]);
            }else{
              //Time Date  
              $info = DriverSessionTime::where('is_online_status','Yes')->where('user_id',$user_id)->first();
             // return $info;
            //  echo $info->start_time; die();


                 $t2 = strtotime($info->start_time);
                 $t1 = strtotime(date('Y-m-d H:i:s'));
               // echo "==".$info->start_time.'===='.date('Y-m-d H:i:s'); die();
                 $diff = $t1 - $t2;
                $hours = $diff / ( 60 * 60 );
                $hours = round($hours,2);


               // echo "hours".$hours; die();

              $update = DriverSessionTime::where('is_online_status','Yes')->where('user_id',$user_id)->update(['is_online_status'=>$status,'end_time'=>date('Y-m-d H:i:s'),'total_housrs'=>$hours]);
            }
           
            User::where('id',$user_id)->update(['is_online'=>$status]);

            $response["code"]          =   200; 
            $response["message"]       =   trans("Update successfully.");
            return response()->json($response,200);
          } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
          }

    }


    public function driver_session_time(Request $request){
       try {   
         //Date == 0 than show today data only
            $user_id = Auth::guard('api')->user()->id; 
            $validator = Validator::make($request->all() ,[
             'date' => 'required',
            ]);
            if ($validator->fails()) {
             return $this->validationErrorResponse($validator);
            }  
            $date = $request->date;
              $date_info = Carbon::parse($request->date);
            //  echo $date_info; die();
            //echo strtotime('Y-m-d',$date); die();
            $data = DriverSessionTime::where('user_id',$user_id);
            if($date==0){
              $data->whereDate('created_at',date('Y-m-d'));  
            }else{
              $data->whereDate('created_at',$date_info);  
            }
            $data = $data->get();
            $total_housrs = 0;
            foreach ($data as $key => $value) {
              $total_housrs+=$value->total_housrs;  
            }

            if(!empty($data)){
            $response["code"]          =   200; 
            $response["total_housrs"]  =   round($total_housrs,2); 
            $response["data"]          =   $data; 
            $response["message"]       =   trans("Driver Session Time List");
           }else{
             $response["code"]          =   200; 
             $response["message"]       =   'Not found data';
                 
           } 
            return response()->json($response,200);
          } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
          }

    }


    public function update_driver_zone(Request $request){
        try{
          $user_id = Auth::guard('api')->user()->id; 
          $info = $request->zone_data;
            $resultarr = array();
            foreach ($info as $key => $value) {
                $resultarr[] = $value['id'];
            }
            //Zone
            $zone_date_info =  implode(',', $resultarr);
            $infodata = json_encode($info);

          $update = User::where('id',$user_id)->update(['zone_id'=>$zone_date_info,'zone_data'=>$infodata]);

          $response["code"]          =   200; 
          $response["message"]       =   trans("Update successfully");
          return response()->json($response,200);
         } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
          }
    }

    // Trip history driver 
    public function get_vendor_away_list(Request $request){
     try{
          $user_id = Auth::guard('api')->user()->id; 
          $validator = Validator::make($request->all() ,[
            'order_id' => 'required',
          ]);
          if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
          }  
          $get_user_delevery_id = Order::select('id','delivery_address_info')->where('id',$request->order_id)->first();
        //return $get_user_delevery_id->delivery_address_info;
        
        $info = json_decode($get_user_delevery_id->delivery_address_info);
        //echo "<pre>"; print_r($info); die();
        $latitude  = isset($info->latitute)?$info->latitute:'28.5742986';
        $longitude =  isset($info->longitute)?$info->longitute:'77.0336434';
        
        $accepted_vendor_orders = OrderItem::select('vendor_id')->where('driver_status','Unpicked')->where('order_id',$request->order_id)->pluck('vendor_id');
//return $accepted_vendor_orders ;
        if(count($accepted_vendor_orders) != 0){
            $accepted_vendor_orders = OrderItem::select('vendor_id')->where('driver_status','Unpicked')->where('order_id',$request->order_id)->pluck('vendor_id');
        }else{
             $accepted_vendor_orders = Order::select('user_id')->where('id',$request->order_id)->pluck('user_id');
        }

        //echo $accepted_vendor_orders;die();
        
        $vendor_info = User::select('latitute','longitute')->whereIn('id',$accepted_vendor_orders)->get();
        /*=======================================*/
         
        $shops          =       User::select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                * cos(radians(latitute)) * cos(radians(longitute) - radians(" . $longitude . "))
                                + sin(radians(" .$latitude. ")) * sin(radians(latitute))) AS distance"));
        $shops          =       $shops->whereIn('id',$accepted_vendor_orders);
        $shops          =       $shops->orderBy('distance', 'DESC');

        $shops          =       $shops->first();
       // echo "<pre>"; print_r( $shops ); die();
        $shops['zone_data'] =json_decode(isset($shops->zone_data)?$shops->zone_data:''); 
       // return $shops;

           if(!empty($shops)){ 

                $response = [
                    'error' => false,
                    'code' => 200,
                    'data' => $shops,
                    'message' => 'Order details',
                ];
            }else{
                $response = [
                    'error' => false,
                    'code' => 0,
                    'data' => $shops,
                    'message' => 'Vendor zone data not found',
                ];
            }    
        return response()->json($response);

        //return $this->showResponse($shops);


      }catch (\Exception $e) {
        return $this->clientErrorResponse($e);
      }
    }

    public function update_driver_order_iteam_status(Request $request){
       try {
          $user_id = Auth::guard('api')->user()->id; 
          $validator = Validator::make($request->all() ,[
            'order_id' => 'required',
            'user_id' => 'required',
            'order_status' => 'required'
          ]);
          if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
          } 
          $update = Order::where('id',$request->order_id)->where('user_id',$request->user_id)->update(['order_status'=>$request->order_status]);
          $response["code"]          =   200; 
          $response["message"]       =   trans("Update successfully");
          return response()->json($response,200);

       }catch (\Exception $e) {
        return $this->clientErrorResponse($e);
      }
    } 
 

}
