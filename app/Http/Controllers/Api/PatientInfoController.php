<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\PatientInfo;
use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
class PatientInfoController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;

    /*Here is call Construct Method*/
    function __construct(Request $request,User $user,PatientInfo $patientInfo){
        $this->method           =$request->method();
        $this->user             =$user;
        $this->patientInfo      =$patientInfo;
        $this->userId           =Auth::guard('api')->user()?Auth::guard('api')->user()->id:null;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try {
        $patientInfo = $this->patientInfo->where('user_id','=',$this->userId)->orderBy('updated_at','desc')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($patientInfo,'Updated default patient info');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();
         $validator = Validator::make($request->all() , ['pin_code' => 'required', 'patient_name' => 'required', 'mobile_number' => 'required|digits:10', 'dob' => 'required','sex' => 'required|in:female,male,other']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
         try {    
             $input['user_id'] = $this->userId;    
             $data =   $this->patientInfo->create($input);
             return $this->showResponse($data,'Patient info added');

         } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
   {
         $validator = Validator::make($request->all() , ['patient_name' => 'required', 'mobile_number' => 'required|digits:10', 'age' => 'required','sex' => 'required|in:female,male,other']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
         try {    
                $patientInfo =  $this->patientInfo->findOrFail($id);
                $input = $request->all();
                $patientInfo->fill($input)->save();
                return $this->showResponse($patientInfo,'Updated patient info .');
         } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        try {
            $del = $this->patientInfo->where('id',$id)->where('user_id',$this->userId)->firstOrFail();
            $del->delete();
            return $this->deletedResponse('Patient deleted.');
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
    }

    /*Update Default Patent Info*/

     public function update_default_patient(Request $request)
   {
         $validator = Validator::make($request->all() ,['id' => 'required']);
         if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
         }
         try {    
                PatientInfo::where('user_id',$this->userId)->update(['status'=>0]);
               $patientInfo=PatientInfo::where('id',$request->id)->update(['status'=>1]);

                return $this->showResponse($patientInfo,'Updated default patient info');
         } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }
}
