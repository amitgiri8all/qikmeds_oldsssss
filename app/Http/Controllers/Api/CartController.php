<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Helpers\Helper;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Brand;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\DeliveryAddress;
use App\Models\Blog;
use App\Models\SaveForLater;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Product;
use App\Models\Cart;
use App\Models\UserPrescription;
use App\Models\SubscriptionPlan;
use App\Models\ReferEarn;
use Illuminate\Support\Str;
use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;

use Image;
use Carbon\Carbon;
use Hash;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
use Mail;
class CartController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;

    /*Here is call Construct Method*/
    function __construct(Request $request,User $user,DeliveryAddress $deliveryAddress,Brand $brand,Blog $blog,Category $category,Banner $banner,Product $product,Cart $cart){
        $this->method           =$request->method();
        $this->user             = $user;
        $this->brand            = $brand;
        $this->blog             = $blog;
        $this->deliveryAddress  = $deliveryAddress;
        $this->banner           = $banner;
        $this->category         = $category;
        $this->product          = $product;
        $this->cart             = $cart;
        $this->userId           = Auth::guard('api')->user()?Auth::guard('api')->user()->id:null;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //  echo $this->userId; die();
        try {
            
            return $cartRec = Cart::with(['Product.ProductImage'])->where('user_id',$this->userId)->where('type','product')->get();
          //  echo "<pre>"; print_r($cartRec); die();
            $cart_details = Cart::where('user_id',$this->userId)->where('type','plan')->first();
         // echo "<pre>"; print_r($cart_details); die();

           if(!empty($cart_details)){ 
             $subscriptionplan = SubscriptionPlan::where('id',$cart_details->plan_id)->first();
             $subscriptionplan_price =  $subscriptionplan->sale_price; 
           } else{
              $subscriptionplan_price = 0;  
              $subscriptionplan = (object)[];   
           } 
             $total = 0;
             $result= [];
             $prescription = 0;
             $total_qty = 0;
             foreach($cartRec as $key=>$rec){
                $total_qty += $rec['qty'];
                //echo $total_qty;    
                if(!empty($rec['Product']['sale_price'])){
                 $total = $total + ((int) $rec['Product']['sale_price'] * $rec['qty'] );
                }else{
                 $total = $total + ( (int) $rec['Product']['mrp'] * $rec['qty'] );
                }

                if($rec['Product']['prescription']=='Yes'){
                 $prescription  = true;   
                }else{
                 $prescription  = false;     
                }
                $rec['Product']['mrp'] = number_format((float)$rec['Product']['mrp'], 2, '.', '');
                $rec['Product']['sale_price'] = $rec['Product']['sale_price'];
                $rec['Product']['discount'] = $rec['Product']['discount'];

             //echo $rec['ProductImage']['image']; die();
           
                if(!empty($rec['ProductImage'])){
                 $cartRec[$key]['Product']['image']=isset($rec['ProductImage']['image'])?$rec['ProductImage']['image']:'';
                }else{
                 $cartRec[$key]['Product']['image'] = isset($rec['ProductImage']['image'])?$rec['ProductImage']['image']:'';
                }

                $result[]= $rec;
             }
             //die();
            // echo "<pre>";  print_r($result); die();
            $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
          //  echo $site_setting->min_price; die();
            if($site_setting->free_delivery_charge > $total) {
                $delivery_charege= $site_setting->delivery_charge; 
            }else{ 
                $delivery_charege= 0;
            }
          //  echo  $delivery_charege; die();

            if(!empty($result) && !empty($subscriptionplan)){
                $charge  = $delivery_charege;
            }elseif(empty($result) && !empty($subscriptionplan)){
                $charge  = 0;
            }else{
                $charge  = 0;
            }

            if(!empty($result)){
                $code = 200;
                $prescription_val = $prescription;
            }else{
                $code =0;
                $prescription_val = false;
            }
            $check_user_first_order = Order::where('user_id',$this->userId)->count();
            if($check_user_first_order>0){
             $user_order = false;   
            }else{
                $user_order = true;
            }
           // echo $total_qty; die();
            $delivery_time = $this->get_delivery_time();
            $response = [
                'error'=>false,
                'code' => $code,
                'user_order' => $user_order,
                'data' => $result,
                'cart_count' =>$total_qty,
                'total' =>$total,
                'prescription' =>$prescription_val,
                'qikmeds_discount' =>$site_setting->qikmeds_discount,
                'delivery_charge' =>$charge,
                'delivery_estimate_time' =>$delivery_time,
                'total_saving' =>$site_setting->qikmeds_discount,
                'plan_data' =>$subscriptionplan,
                'total_price' => $total+$charge-$site_setting->qikmeds_discount+$subscriptionplan_price,
                'currency' =>'₹',
                'message'=>'cart list',
            ];

            return response()->json($response, 200);

        } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            //echo $this->userId; die();
        $input = $request->all();
         $validator = Validator::make($request->all() , ['product_id' => 'required','qty' => 'required|integer|min:1']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
      //  try {
            $message ='';

           $cart= $this->cart->where(['user_id'=>$this->userId,'product_id'=>$request->product_id])->first();
                $qty = Helper::outOfStock($request->product_id);
               /* if($qty < $request->qty){
                    $out_of_stock_responce['message'] = trans('Product Out of Stock');
                   // if($request->action == 'add'){
                      return $this->outOfStockResponse($out_of_stock_responce);
                   // }
                }*/
          if($cart==null) { 
            $input = $request->all();
            $input['user_id'] = $this->userId;    
            $data =   $this->cart->create($input);
            $message = 'Added to your cart';
         }   
             $cartRec = Cart::with(['Product'])->where('user_id',$this->userId)->where('type','product')->get();
             //return $cartRec;
           // echo "<pre>"; print_r($cartRec); die('hjhh');

             $total = 0;
             $result= [];
             foreach($cartRec as $key=>$rec){
                if(!empty($rec['Product']['sale_price'])){
                 $total = $total + ((int) $rec['Product']['sale_price'] * $rec['qty'] );
                
                }else{
                 $total = $total + ( (int) $rec['Product']['mrp'] * $rec['qty'] );
                }
                $result[]= $rec;
             }

            $site_setting = DB::table('site_settings')->select('*')->where('id',1)->first();
            if($site_setting->min_price > $total) { $delivery_charege= $site_setting->delivery_charge; }else{ $delivery_charege= 0;}
          //  echo "<pre>"; print_r($result); die('hjhh');
            $response = [
                'error'=>false,
                'code' => 200,
                'data' => $result,
                'cart_count' =>count($result),
                'total' =>$total,
                'qikmeds_discount' =>$site_setting->qikmeds_discount,
                'delivery_charge' =>$delivery_charege,
                'total_saving' =>$site_setting->qikmeds_discount,
                'total_price' => $total+$delivery_charege-$site_setting->qikmeds_discount,
                'currency' =>'₹',
                'message'=>$message,
            ];

            return response()->json($response, 200);

      /*  } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
               // echo "asdfasd".$this->userId; die();

        $validator = Validator::make($request->all(),[
            'qty' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponce($validator);
        }else{

            try {
                    $cart = $this->cart->where('product_id',$id)->where('type','product')->where('user_id',$this->userId)->first();
                    $qty = Helper::outOfStock($cart->product_id);

                    /*if($qty < $request->qty){
                        $out_of_stock_responce['message'] =trans('Product Out of Stock');
                        return $this->outOfStockResponse($out_of_stock_responce);
                    }*/
                    $cart->update($request->only('qty'));
         $data=Cart::where('product_id',$id)->where('qty',0);
         $status=$data->delete('Updated in your cart');
            } catch (\Exception $e) {
                return $this->clientErrorResponse($e);
            }
            return $this->showResponse($cart,'Updated cart');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $cart = $this->cart->where('id',$id)->where('user_id',$this->userId)->firstOrFail();
            $cart->delete();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
       return $this->deletedResponse('Removed product');
     } 

     public function remove_plan(Request $request)
    {
       // echo "sadfasd"; die();
        $validator = Validator::make($request->all(),[
            'plan_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponce($validator);
        }
        try {
            $id = $request->plan_id;
            $cart = $this->cart->where('plan_id',$id)->where('type','plan')->where('user_id',$this->userId)->firstOrFail();
            $cart->delete();
           return $this->deletedResponse();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
     }
/**
     * Clear cart info the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

      public function clearCart()
    {

        try {
            $cart = $this->cart->where('user_id',$this->userId);
            $cart->delete();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->deletedResponse(trans('clear cart'));

    }


    /*public function upload_prescription(Request $request){
       $validator = Validator::make($request->all(),[
            'image' => 'image|mimes:jpeg,png,jpg',
        ]);

        if ($validator->fails()) {
            return $this->validationErrorResponce($validator);
        }
    }*/

    public function upload_prescription(Request $request){
        try {
            $user = Auth::guard('api')->user();
            $validator = Validator::make($request->all(), [
              'image' => 'image|mimes:jpeg,png,jpg',
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }

            $count = UserPrescription::where('user_id',$this->userId)->count();
            //echo $count; die();
            $input = $request->all();
            if ($request->hasFile('image')) {
              $folder_name = 'prescription';
              $input['image'] = $this->fileUpload($request->file('image'), false, $folder_name);
            }
            $image = $input['image'];
            if($count>0){
               UserPrescription::where('user_id',$this->userId)->update(array('image'=>$image));
            }else{
                $save = new UserPrescription();
                $save->user_id = $this->userId;
                $save->image = $image;
                $save->save(); 
            } 

            $userData  = UserPrescription::where('user_id',$this->userId)->first();


            return $this->showResponse($userData,'Prescription Uploaded.','200');
        }catch(\Exception $e){
         return $e;
       }    
    }


    public function add_to_subscription_plan(Request $request){
         try {
            $user = Auth::guard('api')->user();
            $validator = Validator::make($request->all(), [
              'plan_id' => 'required',
              'type' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $count = Cart::where('user_id',$this->userId)->whereNotNull('plan_id')->count();
            if($count>0){
                $updateplan  = Cart::where('user_id',$this->userId)->update(['plan_id'=>$request->plan_id]);
                $userData  = SubscriptionPlan::where('id',$request->plan_id)->first();
                return $this->showResponse($userData,'Plan updateed .','200');

            }else{    
                $save = new Cart();
                $save->user_id = $this->userId;
                $save->plan_id = $request->plan_id;
                $save->type = 'plan';
                $save->qty = 1;
                $save->save(); 
            $userData  = SubscriptionPlan::where('id',$request->plan_id)->first();
            return $this->showResponse($userData,'Plan added .','200');
           } 
        }catch(\Exception $e){
         return $e;
       }    
    }

    public function refer_earn(Request $request){
     try{
        $data = ReferEarn::where('id','1')->first();
        return $this->showResponse($data);
     }catch(\Exception $e){
         return $e;
     }     
    }

    public function check_referral_code(Request $request){
        $validator = Validator::make($request->all(), [
          'referral_code' => 'required',
         ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        $check_ref_code = User::where('referrer_code',$request->referral_code)->where('id','!=',$this->userId)->first();
      //  echo $check_ref_code; die();
        if(!empty($check_ref_code)){
            $referEarn = ReferEarn::where('id','1')->first();
            $res['error'] =false;
            $res['code'] =200;
            $res['message'] = "Referrer code match. You earn Rs .".$referEarn->refer_amount."";
            return response()->json($res);
        }else{
            $res['error'] =false;
            $res['code'] =0;
            $res['message'] = "Wrong referral code entered! Check and try again.";
            return response()->json($res);
        }
    }


    
    public function reOrder(Request $request){
       // echo "sadfas"; die();
        $ProductOrderItemArray=[];
        $OrderItemArray=[];
        $input_request = [];
        $vendorid = Order::select('id')->where(['id'=>$request->order_id])->first();
                
        $order =  OrderItem::select('product_id','quantity_order')->where(['order_id'=>$request->order_id])->get();

        if(count($order) > 0){
            foreach ($order as $key => $value) {
                $OrderItemArray[$value->product_id] = $value->quantity_order;
            } 
        }

        $oldcart = $this->cart->where('user_id',Auth::guard('api')->user()->id);
        $oldcart->delete();
        
        foreach ($OrderItemArray as $vendorProductId => $productQty) {
            $qty = $this->outOfStock($vendorProductId);
            if($qty > 0){
                $input_request['product_id']= $vendorProductId;
                 $input_request['user_id']=Auth::guard('api')->user()->id;
                if($qty > $productQty || $qty == $productQty ){
                    $input_request['qty']= $productQty;
                }
                if($qty < $productQty){
                     $input_request['qty']= $productQty;
                }
                $cart =   $this->cart->create($input_request);

            }
           
        }
        if(!empty($cart)){
            $res['error'] =false;
            $res['code'] =200;
            $res['message'] = "Reorder successful";
            return response()->json($res);
        }else{
            $res['error'] =false;
            $res['code'] =0;
            $res['message'] = "";
            return response()->json($res);
        }

     }


}
