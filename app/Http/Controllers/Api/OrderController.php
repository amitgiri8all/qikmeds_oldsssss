<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\AllOrderStatus;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Brand;
use App\Models\DeliveryAddress;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Banner;
use App\Models\VendorTrackOrder;
use App\Models\TrackOrder;
use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\VendorProductStatus;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Sitesettings;
use App\Models\OrderItem;
use App\Models\Coupon;
use App\Models\OrderReferralCode;
use App\Models\SubscriptionPlan;
use App\Models\UserSubscriptionOrder;
use App\Models\OfferBanner;
use App\Models\UserPrescription;
use App\Models\OrderReview;
use Illuminate\Support\Str;
/*=================*/
 use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
use Mail;
use PDF;
use File;


class OrderController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;
    /*Here is call Construct Method*/
    function __construct(Request $request,User $user,DeliveryAddress $deliveryAddress,Brand $brand,Blog $blog,Category $category,Banner $banner,Product $product,Order $order,OrderItem $orderItem,Cart $cart,Coupon $coupon,OrderPayment $orderPayment,OrderReferralCode $orderReferralCode,SubscriptionPlan $subscriptionPlan,UserSubscriptionOrder $userSubscriptionOrder,OfferBanner $offerBanner,UserPrescription $userprescription){
        $this->method           =$request->method();
        $this->user             = $user;
        $this->brand            = $brand;
        $this->blog             = $blog;
        $this->deliveryAddress  = $deliveryAddress;
        $this->banner           = $banner;
        $this->category         = $category;
        $this->product          = $product;
        $this->order            = $order;
        $this->orderItem        = $orderItem;
        $this->cart             = $cart;
        $this->coupon           = $coupon;
        $this->orderPayment          = $orderPayment;
        $this->orderReferralCode     = $orderReferralCode;
        $this->subscriptionPlan      = $subscriptionPlan;
        $this->userSubscriptionOrder = $userSubscriptionOrder;
        $this->offerBanner           = $offerBanner;
        $this->userprescription      = $userprescription;
        $this->userId           = Auth::guard('api')->user()?Auth::guard('api')->user()->id:null;

    }
    // 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function items_bought()
    {
        try {
              $data = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('order_status','D')->orderBy('id', 'DESC')->where('user_id',$this->userId)->get();
            foreach($data as $key=>$value){
                $data[$key]['mrp'] = number_format((float)$value->mrp, 2, '.', '');
                $data[$key]['sale_price'] = number_format((float)$value->sale_price, 2, '.', '');
                $data[$key]['discount'] = number_format((float)$value->discount, 2, '.', '');
            }

        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data); 

    }    

    public function order_list()
    {
        try {
              $data = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->orderBy('id', 'DESC')->where('user_id',$this->userId)->get();
          foreach($data as $key=>$value){
            $data[$key]['mrp'] = number_format((float)$value->mrp, 2, '.', '');
            $data[$key]['sale_price'] = number_format((float)$value->sale_price, 2, '.', '');
            $data[$key]['discount'] = number_format((float)$value->discount, 2, '.', '');
        }


        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data); 

    } 


 public function past_order_list()
    {

          try {
              $data = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('order_status','D')->orderBy('id', 'DESC')->where('user_id',$this->userId)->get();

            foreach($data as $key=>$value){
             $data[$key]['mrp'] = number_format((float)$value->mrp, 2, '.', '');
             $data[$key]['sale_price'] = number_format((float)$value->sale_price, 2, '.', '');
             $data[$key]['discount'] = number_format((float)$value->discount, 2, '.', '');
            }

        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data); 

    }    
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function order_details(Request $request)
    {
       // echo $this->userId; die();
        $validator = Validator::make($request->all(), [
          'order_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        try {
              $order_id = $request->order_id;
              $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')
             // ->where('user_id',$this->userId)
              ->where('id',$order_id)->first(); 

            $order->mrp = number_format((float)$order->mrp, 2, '.', '');
            $order->sale_price = number_format((float)$order->sale_price, 2, '.', '');
            $order->discount = number_format((float)$order->discount, 2, '.', '');

            $order['delivery_address_info'] = json_decode($order->delivery_address_info);
            $order['patient_info'] = json_decode($order->patient_info);

           // echo "<pre>"; print_r($order); die();
            $ProductOrderItemArray=[];
            foreach ($order['OrderItem'] as $key=>$ProductOrderItem){
                 $mrp =  json_decode($ProductOrderItem['data']);
                //echo $ProductOrderItem['data']->brand_name; die(); 

              $vendor = $this->user->where('role','vendor')->where('id',$ProductOrderItem->vendor_id)->first(); 
              $ProductOrderItem['data']             = json_decode($ProductOrderItem['data']);

                $ProductOrderItem['data']->mrp  = number_format((float)$mrp->mrp, 2, '.', '');
                $ProductOrderItem['data']->sale_price  = number_format((float)$mrp->sale_price, 2, '.', '');
                $ProductOrderItem['data']->discount  = number_format((float)$mrp->discount, 2, '.', '');

              $ProductOrderItem['vendor_firm_name'] = isset($vendor->farm_name)?$vendor->farm_name:'No Found Farm Name';
              $ProductOrderItem['vendor_details']   =  $vendor;
              $ProductOrderItemArray[]              = $ProductOrderItem;
            }  
          $val = OrderReview::where('order_id',$request->order_id)->where('user_id',$this->userId)->count();
          $order['order_rating'] = $val;
          return $this->showResponse($order); 
 

        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function order_place(Request $request)
    { 
      //  echo $this->userId; die();
        $validator = Validator::make($request->all(), [
          'address_id' => 'required',
          'payment_mode_id' => 'required',
          //'patient_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
         //Get Cart Details BY login user 
         $cartRec = $this->cart->with(['Product','ProductImage'])->where('type','product')->where('user_id',$this->userId)->get();
           $cartplan = $this->cart->with(['SubscribePlan'])->where('type','plan')->where('user_id',$this->userId)->get();
      
            $result =[];
            $error=0;
            if($cartRec->count()=='0' && $cartplan->count()=='0'){
                $res['error'] =true;
                $res['code'] =1;
                $res['message'] = "No Product in cart for this address.";
                return response()->json($res);
            }elseif ($cartRec->count()=='0' && $cartplan->count()!='0') {
                //echo "only subscription"; die();
                    $validator = Validator::make($request->all(), [
                        'plan_id' => 'required',
                        'transaction_id' => 'required',
                        ]);
                        if ($validator->fails()) {
                          return $this->validationErrorResponse($validator);
                        }
                        $plan_data = SubscriptionPlan::where('id',$request->plan_id)->first();

                        if(empty($plan_data)){
                            $res['error'] =true;
                            $res['code'] =1;
                            $res['message'] = "No found any subscription plan.";
                            return response()->json($res);
                        }else{

                            $plan_data = SubscriptionPlan::where('id',$request->plan_id)->first();

                            $future_timestamp = strtotime($plan_data->duration);

                            $data = date('Y-m-d', $future_timestamp);

                            $subscriptionPlanAmt = isset($plan_data->sale_price)?$plan_data->sale_price:$plan_data->price;

                            User::where('id',Auth::guard('api')->user()->id)->update(['is_subscription'=>'1','subscription_plan_id'=>$request->plan_id,'expire_date_subscription'=>$data]);  

                            $sub['user_id'] = Auth::guard('api')->user()->id;

                            $sub['total_amount'] = $subscriptionPlanAmt;

                            $sub['plan_id'] =$request->plan_id;

                            $sub['transaction_id'] =$request->transaction_id;

                            $this->userSubscriptionOrder->create($sub);

                            Cart::where('user_id',$this->userId)->delete();

                            return $this->showResponse($plan_data,'Order placed'); 
                        }
            }else{ 
                               // echo "subscription with product"; die();
               /*==================  Product And Subscription =====================*/ 
               try {
                 //   echo "<pre>"; print_r($cartRec); die();
                    $is_prescription = 0;
                    $ab =false;
                   foreach ($cartRec as $key=>$Rec) {
 
                        if((!$ab) && ($Rec['Product']['prescription']=='1')){
                            $ab = true;
                            $is_prescription = 1;    
                        }


                      $AppSetting = Sitesettings::where('id',1)->first();
                      $trmpArray = [];
                        $product_id = $Rec['Product']['id'];

                         // echo $rec['ProductImage']['image']; die();
                        if(!empty($Rec['ProductImage'])){
                         $cartRec[$key]['Product']['image']=$Rec['ProductImage']['image'];
                        }else{
                         $cartRec[$key]['Product']['image'] = @$Rec['ProductImage']['image'];
                        }
                        if(!empty($Rec['Product']['sale_price'])){
                           $saleprice =  $Rec['Product']['sale_price'] *$Rec['qty'];
                        }else{
                            $saleprice = $Rec['Product']['mrp'] * $Rec['qty'];;
                        }
                       // echo $saleprice; die();
                        //echo $product_id; die();
                        //if($Rec['qty'] <= $Rec['Product']['qty']) {
                            $trmpArray['prescription'] = $Rec['Product']['prescription'];
                            $trmpArray['product_price'] = $Rec['Product']['mrp'];
                            
                            $trmpArray['price'] = $Rec['Product']['mrp'] * $Rec['qty'];
                            $trmpArray['product_name'] = $Rec['Product']['medicine_name'];
                            $trmpArray['sales_price'] = $saleprice;
                            $trmpArray['base_price'] = isset($Rec['Product']['sale_price'])?$Rec['Product']['sale_price']:$Rec['Product']['mrp'] * $Rec['qty'];
                            $trmpArray['quantity_order'] = $Rec['qty'];
                            $trmpArray['product_id'] = $Rec['Product']['id'];
                            $dataProduct =  $this->getProdcutDetails($Rec['Product']['id']);
                          $trmpArray['data']=$dataProduct;
                          $trmpArray['status'] = 1;
                        /* } else {

                          $trmpArray['total'] = 0;
                          $trmpArray['offer_total'] = 0;
                          $trmpArray['message'] = $Rec['Product']['medicine_name'] . trans('site.out_of_stock').''.trans('site.max_qty').''.$Rec['qty'];
                          $trmpArray['status'] = 0;
                          $error = 1;
                        }*/
                        $result[] = $trmpArray;
                    }
                } catch (\Exception $e) {
                  return $this->notFoundResponse($e);
                        }
                if($error){
                    return $this->outOfStockResponse(collect($result)->where('status','=',0)->first());
                }else{  
        //DB::beginTransaction();
             try {
                //echo "<pre>"; print_r($result); die();
                $input = [];
                /*actual total price  without value*/
                $sub_total = collect($result)->sum('price');
                /*actual total  without value*/
                $sales_price = collect($result)->sum('sales_price'); 
                $discount_product_price = $sub_total-$sales_price;
              //  echo $sales_price.'==='.$sub_total; die('ssssss');
                /*****************
                  Prescription Image  
                ******************/
                  $pre = $this->userprescription->where('user_id',Auth::guard('api')->user()->id)->first();
                  if(!empty($pre)){
                    $input['prescription_image'] =$pre->image;
                  }else{
                    $input['prescription_image'] =null;
                  }
             $address = $this->deliveryAddress->where('user_id',$this->userId)->where('status',1)->first();
             $patient_info =  $this->patient_info_api();
             $delivery_time = $this->get_delivery_time();
            // echo $delivery_time; die('locked');
                /*****************
                  Plan Ammount Calcutaion  
                ******************/  
                if(!empty($request->plan_id)){
               //   echo "ok"; die();
                   $plan_data = $this->subscriptionPlan->where('id',$request->plan_id)->first();
                   $subscriptionPlanAmt = isset($plan_data->sale_price)?$plan_data->sale_price:$plan_data->price;
                   $input['subscription_plan_amount'] =$subscriptionPlanAmt;
                   $input['sub_paln_id'] =$request->plan_id;
                   User::where('id',Auth::guard('api')->user()->id)->update(['subscription_plan_id'=>$request->plan_id]);
                }else{
                    $input['subscription_plan_amount'] =null;
                    $input['sub_paln_id'] =null;
                    $subscriptionPlanAmt =0;
                } 
            //  echo $subscriptionPlanAmt; die();
                 /*****************
                  Basic User Info  
                ******************/   
                 if($is_prescription=='Yes'){
                    $input['is_prescription']         = 'Y';
                    $input['order_status']            = 'N';
                 }else{
                     $input['is_prescription']         = 'N';
                     $input['order_status']            = 'CC';
                 } 

                $input['delivered_date']         = $delivery_time;
               
                $input['user_id']         = Auth::guard('api')->user()->id;
                $input['patient_id']      = $request->patient_id;
                $input['address_id']      = $request->address_id;
                $input['payment_mode_id'] = $request->payment_mode_id;
                
                $input['order_code']      = $this->orderCodeApi();
                $input['patient_info']   = $patient_info;
                $input['delivery_address_info']   = $address;

                /*****************
                  Delivery Charge Ammount Calcutaion  
                ******************/ 
               if(empty($request->plan_id)){
                    if($sub_total<$AppSetting->min_price){
                      $shipping_amount  = $AppSetting->delivery_charge;
                    }else{
                     $shipping_amount   = '0';
                    } 
                }else{
                    $shipping_amount  = '0';
                } 
                $input['shipping_amount']  = $shipping_amount;
                /*****************
                  Offer Code Ammount Calcutaion  
                ******************/
                $offer_code_amount_net =0;
                if(isset($request->offer_code)){
                    $offerbannerdata = $this->offerBanner->where('code',$request->offer_code)->where('status','1')->first();
                    if(!empty($offerbannerdata)){
                      if($offerbannerdata->offer_type!='cashback'){  
                           $offer_code_amount =  $this->OfferCodeAmount($offerbannerdata->price_type,$offerbannerdata->price,$sub_total);
                           $offer_code_amount_net = $sub_total -$offer_code_amount;
                           $input['offer_price'] = $offer_code_amount_net;
                           $input['offer_code']   = $offerbannerdata->code;
                      }else{
                           $offer_code_amount_net = 0;   
                      } 
                    }else{
                           $offer_code_amount_net = 0;   
                    }
                }

                // echo $offer_code_amount_net; die();

                  /*****************
                  Coupon Code Ammount Calcutaion  
                ******************/
                  //echo $request->coupon_id; die();
                  $coupon_code_amount_net =0;
                if(isset($request->coupon_id)){
                  $coupondata = $this->coupon->where('id',$request->coupon_id)->where('status','1')->first();

                    if(!empty($coupondata)){
                      //  echo $sub_total; die()
                       $coupon_code_amount =  $this->CouponAmount($coupondata->coupon_type,$coupondata->coupon_value,$sales_price);
                      // echo $coupon_code_amount; die();
                       $sales_price_amt  = round($sales_price);
                       //echo $sales_price_amt; die();
                       $coupon_code_amount_net = $sales_price -$coupon_code_amount;
                       $input['coupon_amount'] = $coupon_code_amount;
                       $input['coupon_code']   = $coupondata->code;
                    }else{
                     $coupon_code_amount_net = 0;   
                    }
                 }
                
              // echo $coupon_code_amount_net; die('okkkk');

                 /*****************
                  Payment Mode Calcutaion  
                ******************/ 
                $wallet_flag =0;
                if($request->payment_mode_id == 2){
                            $input['transaction_id']      = $request->transaction_id;
                          //  $input['charge_id']           = $request->charge_id;
                            $input['online_payment']      =  $request->online_payment;
                            $input['transaction_status']  = '1';
                }else if($request->payment_mode_id == 3){
                    if($request->wallet_payment <= Auth::guard('api')->user()->wallet_amount){
                      $input['transaction_id']      = null;
                     // $input['charge_id'] = null;
                      $input['transaction_status']  = '1';
                      $input['wallet_payment']      =  $request->wallet_payment;
                      $wallet_flag = 1;
                    }else{
                      $res = ['error'=>true,'code'=>5,'message'=>'Insufficient wallet amount i.e.'.Auth::guard('api')->user()->wallet_amount.' Rs'];
                      return response()->json($res);
                    }
                }else{
                    $input['transaction_id'] = null;
                  //  $input['charge_id']      = null;
                    $input['transaction_status'] = '0';
                }

              /*****************
              Referral Code   
             ******************/
               // order_referral_code
               if(isset($request->referral_code)){
                    $check_ref_code = User::where('referrer_code',$request->referral_code)->where('id','!=',Auth::guard('api')->user()->id)->first();
                     if(!empty($check_ref_code)){
                        $form_user_id =Auth::guard('api')->user()->id;     
                        $to_user_id = $check_ref_code->id;     
                        $referrer_code = $check_ref_code->referrer_code;   
                    }else{
                        $res['error'] =false;
                        $res['code'] =0;
                        $res['message'] = "Wrong referral code entered! Check and try again.";
                        return response()->json($res);
                    }   
               }

                /*****************
                  Onilne Payment AND Wallet Payment Calcutaion  
                ******************/ 
              $payment_mode_id =  explode(',',$request->payment_mode_id);
              if(count($payment_mode_id) > 1){
                $credit_bal = $request->wallet_payment + $request->online_payment;

                if((string)$credit_bal !== (string)$input['sub_total']){
                  $res = ['error'=>true,'code'=>4,'message'=>'Please select amount properly...'];
                  return response()->json($res);
                }
                if($payment_mode_id[0] == 2 || $payment_mode_id[1] == 2){
                  $input['transaction_id']      = $request->transaction_id;
                 // $input['charge_id']           = $request->charge_id;
                  $input['transaction_status']  = '1';
                  $input['online_payment']      = $request->online_payment;
                }else if($payment_mode_id[0] == 3 || $payment_mode_id[1] == 3){
                  if($request->wallet_payment <= Auth::guard('api')->user()->wallet_amount){
                    $input['transaction_id']      = null;
                    $input['transaction_status']  = '1';
                    $input['wallet_payment']      =  $request->wallet_payment;
                    
                    $wallet_flag = 1;
                  }else{
                    $res = ['error'=>true,'code'=>5,'message'=>'insufficient wallet amount i.e.'.Auth::guard('api')->user()->wallet_amount.' Rs'];
                    return response()->json($res);
                  }
                }
              } 

              if($request->payment_mode_id==2){
                $mode = 'Online Payment';
              }elseif($request->payment_mode_id==3){
                $mode = 'Wallet Payment';
              }else{
                $mode = 'Cash On Dellivery';
              }

               /*****************
                  Order Ammount Calcutaion  
                ******************/   
//       echo $sales_price.'======'.$shipping_amount.'======'.$coupon_code_amount_net.'====='.$subscriptionPlanAmt; die();

                $input['sub_total']               = $coupon_code_amount_net-$AppSetting->qikmeds_discount;
                $input['total_payed_amount']      = $coupon_code_amount_net+$shipping_amount;
                $input['total_saving']            = 0;
                $input['discount_amount']         = $AppSetting->qikmeds_discount;

    
     //echo "<pre>"; print_r($input); die();

              $order = $this->order->create($input);
                 //User Subscription Order
               if(!empty($order->sub_paln_id) && $subscriptionPlanAmt>0){

                    $plan_data = SubscriptionPlan::where('id',$order->sub_paln_id)->first();
                    $future_timestamp = strtotime($plan_data->duration);
                    $date_sub = date('Y-m-d', $future_timestamp);  

                   User::where('id',Auth::guard('api')->user()->id)->update(['is_subscription'=>'1','subscription_plan_id'=>$order->sub_paln_id,'expire_date_subscription'=>$date_sub]);   
                  $sub['user_id'] = Auth::guard('api')->user()->id;
                  $sub['total_amount'] = $subscriptionPlanAmt;
                  $sub['plan_id'] =$order->sub_paln_id;
                  $this->userSubscriptionOrder->create($sub);

               } 
              //Order Refrel Code
            if(isset($request->referral_code)){  
              $value['order_id'] = $order->id;
              $value['form_user_id'] = $form_user_id;
              $value['to_user_id'] = $to_user_id;
              $value['referral_code'] = $referrer_code;
              $this->orderReferralCode->create($value);
            }
               //echo $mode; die();
              //Order Payment Created
              $data['order_id'] = $order->id;  
              $data['payment_method'] = $mode;  
              $data['total_amount'] = $order->total_payed_amount;  

              /*========================*/



              $this->orderPayment->create($data);
              $order->ProductOrderItem()->createMany($result);
              /*====================== Track Order =============*/
              /*****************
              Create Order   
             ******************/ 
             $this->invoice_create($order->id);

            /*****************
              Wallet Payment Update  
             ******************/ 
            if($wallet_flag == 1){
                if($request->wallet_payment > 0){
                    $customer_id = Auth::guard('api')->user()->id;
                    $transaction_type = 'DEBIT';
                    $transaction_id = rand('000000','999999');
                    $type = "wallet_payment";
                    $amount = $request->wallet_payment;
                   // $amount = $input['offer_total'];
                    $description = "This is a order payment entry for order code - ".$order->order_code;
                    $json_data = json_encode(['order_id'=>$order->id,'order_code'=>$order->order_code]);
                    $order_id = $order->id;
                    $this->updateCustomerWallet($customer_id,$amount,$transaction_type,$type,$transaction_id,$description,$json_data,$order_id);
                 }
            }

            if($request->payment_mode_id == 2){
                $customer_id = Auth::guard('api')->user()->id;
                $transaction_type = 'DEBIT';
                $transaction_id = rand('000000','999999');
                $type = "online_payment";
                $amount = $request->online_payment;
                $description = "This is a order payment entry for order code - ".$order->order_code;
                $json_data = json_encode(['order_id'=>$order->id,'order_code'=>$order->order_code]);
                $order_id = $order->id;
                $this->updateCustomerWalletAmt($customer_id,$amount,$transaction_type,$type,$transaction_id,$description,$json_data,$order_id);
            } 
             /*****************
              Clear Cart Table    
             ******************/
            Cart::where('user_id',$this->userId)->delete();

             
             $data  = $this->orderDetails($order->id);
               $track = new TrackOrder();
              $track->order_id           = $order->id;
              $track->order_track_status = 'N';
              $track->date_time          = Carbon::now();
              $track->save();

 
              
              /*send notification to customer*/
              $user_id_array = User::where('id',Auth::guard('api')->user()->id)->select('id','device_type','device_token','name')->get();
              $user_id_array1 = User::where('id',Auth::guard('api')->user()->id)->select('id','device_type','device_token','name')->get();
             // echo "<pre>"; print_r($user_id_array1); die();
              $userData = User::where('id', '=', Auth::guard('api')->user()->id )->select('device_token')->get();
              $user_id_array = collect($userData)->pluck('device_token');
              $user_id_array = collect($userData)->pluck('device_token');
              $senderName = $user_id_array1[0]->name;
              $message    = '#'.$order->order_code.trans('order.new_order');
              $type       = 'new order';
              $order->user->notify(new AllOrderStatus($order,$senderName,$message,$type));
              /**/
                $dataArray = [];
                $dataArray['type'] = 'Order';
                $dataArray['product_type'] = 'New';
                $dataArray['title'] = 'New Order';
                $dataArray['body'] = trans('Order placed successfully');
                $device_type = $user_id_array1[0]->device_type;

               Helper::sendNotification($user_id_array ,$dataArray, $device_type);
 
             return $this->showResponse($data,'Order placed'); 
  

             }catch (\Exception $e) {
                return $e;
            //DB::rollBack();
              return $this->clientErrorResponse($e);
            }  

        }   
      }   
    }


// Place Order Close \\
    public function invoice_create($order_id){
        //echo $order_id; die();
        $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$order_id)->first();

   //  return view('frontend.invoice',compact('order'));
        //echo "<pre>"; print_r($order); die();
        view()->share('order',$order);
        $pdf = PDF::loadView('frontend.invoice')->setOptions(['defaultFont' => 'sans-serif']);
       // return $pdf->Download('amit.pdf');
        $content = $pdf->download()->getOriginalContent();
        $extension = 'pdf';
        $fileName           =   time().'-invoice.'.$extension;
        $folderName         =   strtoupper(date('M'). date('Y'))."/";
        $name = $fileName;
        //echo $name; die();
        Storage::put('public/invoice/'.$name.'',$content);
        $save = 'invoice/'.$name;
      // echo $save; die();
        //echo "success"; die();
        $this->order->where('id',$order_id)->update(['invoice_pdf'=>$save]);

    } 
 

    public function subscription_plan_order(Request $request){
        $validator = Validator::make($request->all(), [
        'plan_id' => 'required',
        'transaction_id' => 'required',
        ]);
        if ($validator->fails()) {
          return $this->validationErrorResponse($validator);
        }
        $plan_data = SubscriptionPlan::where('id',$request->plan_id)->first();

        if(empty($plan_data)){
            $res['error'] =true;
            $res['code'] =1;
            $res['message'] = "No found any subscription plan.";
            return response()->json($res);
        }else{

            $future_timestamp = strtotime($plan_data->duration);
            $data = date('Y-m-d', $future_timestamp);    

            $subscriptionPlanAmt = isset($plan_data->sale_price)?$plan_data->sale_price:$plan_data->price;
            User::where('id',Auth::guard('api')->user()->id)->update(['is_subscription'=>'1','subscription_plan_id'=>$request->plan_id,'expire_date_subscription'=>$data]);  
            $sub['user_id'] = Auth::guard('api')->user()->id;
            $sub['total_amount'] = $subscriptionPlanAmt;
            $sub['plan_id'] =$request->plan_id;
            $sub['transaction_id'] =$request->transaction_id;
            $this->userSubscriptionOrder->create($sub);
            Cart::where('user_id',$this->userId)->delete();
            return $this->showResponse($plan_data); 
        }
     }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function orderDetails($id)
    {
       // echo "okokk"; die();
     $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')->where('id',$id)->first(); 
     return $order;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reason_return_list()
    {

        try {
              $data = DB::table('reason_return')->whereNull('deleted_at')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data); 

    } 


    public function order_cancel(Request $request){
        $validator = Validator::make($request->all(), [
          'order_id' => 'required',
        ]);
        if ($validator->fails()) {
          return $this->validationErrorResponse($validator);
        }
        $order_id = $request->order_id;
       // echo $order_id; die();
        try {
             $status = 'C';
            $data = $this->order->where('id',$order_id)->where('user_id',$this->userId)->update(['order_status'=>'C']);
           // echo $data; die();
           if($data>0){ 
            $res['error'] =false;
            $res['code'] =200;
            $res['message'] = "Order Cancelled";
        }else{
            $res['error'] =false;
            $res['code'] =200;
            $res['message'] = "Error";
        }
            return response()->json($res);
        } catch (\Exception $e) {
             return $this->clientErrorResponse($e);
        }
    }  



    /*******************
     Offer Code Chekc Calcution       
    *******************/ 
    public function check_offer_code(Request $request){
       // echo $this->userId; die();
     $validator = Validator::make($request->all(), [
       'offer_code' => 'required',
       'paid_amount' => 'required',
     ]);
     if ($validator->fails()) {
       return $this->validationErrorResponse($validator);
     }
     $check_offer_code = $this->offerBanner->where('code',$request->offer_code)->first();
      if(empty($check_offer_code)){
            $res['error'] =false;
            $res['code'] =0;
            $res['message'] = "Wrong offer code entered! Check and try again.";
        }elseif($check_offer_code->minimum_amount >= $request->paid_amount){
            $res['error'] =false;
            $res['code'] =0;
            $res['message'] = "Sorry, this offer code is only applicable on Cart value above Rs.".$check_offer_code->minimum_amount ." Please add more items to avail this offer";
        }else{
            $res['error'] =false;
            $res['code'] =200;
            $res['message'] = "Offer code applied.";
        }
            return response()->json($res);
     }  

     /*===========Track Order ==============*/

     public function track_order_vendors(Request $request){
       try{
            $validator = Validator::make($request->all(), [
              'vendor_id' => 'required',
              'order_id' => 'required',
            ]);
            if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
            }
            $vendor_id = $request->get('vendor_id'); 
            $order_id = $request->get('order_id');
            /*Order ============*/
            $order = Order::where('id',$order_id)->first();
            $total = OrderItem::where('order_id',$order_id)->where('vendor_id',$vendor_id)->count('product_id');
            $new = ['track_status'=>'Order Place','date_time'=>$order->created_at ];
            $data = VendorTrackOrder::select('track_status','date_time')->where('order_id',$order_id)->where('vendor_id',$vendor_id)->get()->toArray(); 
            $newData = array_merge(array($new),$data);
         //   $newData['total_product']['total_product'] = $total;
          //  return $this->showResponse(array_values($newData),'Track Order'); 

            if(empty($newData)){
                $res['error'] =false;
                $res['code'] =0;
                $res['message'] = "Not Found data";
             }else{
                $res['error'] =false;
                $res['code'] =200;
                $res['total_items'] =$total;
                $res['data'] =$newData;
                $res['message'] = "Track Order.";
            }
        return response()->json($res);
        }catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
     }

     public function track_order_user(Request $request){
       try{
            $validator = Validator::make($request->all(), [
              'order_id' => 'required',
            ]);
            if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
            }

            $order_id = $request->get('order_id');
            $order = Order::where('id',$order_id)->first();
            $data = TrackOrder::select('order_track_status','date_time')->where('order_id',$order_id)->get(); 
            //Track Order =================//
            $arr = [];
            foreach($data as $key=>$value){ 
               $arr[$key]['order_track_status'] = 'Order is '. Helper::$track_order_user[$value->order_track_status];
               $arr[$key]['date_time'] = $value->date_time; 
            }   
           // $data['order_track_status'] = 'Order is '; 
            return $this->showResponse($arr,'Track Order'); 
            
        }catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
     }
     public function vendor_accepted_products(Request $request){
       try{
            $validator = Validator::make($request->all(), [
              'order_id' => 'required',
              'vendor_id' => 'required',
            ]);
            if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
            }

            $order_id = $request->get('order_id');
            $vendor_id = $request->get('vendor_id');
            $vendor_name = User::where('id',$vendor_id)->first('farm_name');
            $order = OrderItem::where('order_id',$order_id)->where('vendor_id',$vendor_id)->get('data');
            $newarr = [];
            foreach($order as $key=>$value){
              $newarr[]= json_decode($value->data);
            }


 if(empty($newarr)){
                $res['error'] =false;
                $res['code'] =0;
                $res['message'] = "Not Found data";
             }else{
                $res['error'] =false;
                $res['code'] =200;
                $res['vendor_name'] =$vendor_name->farm_name;
                $res['data'] =$newarr;
                $res['message'] = "Vendor Product.";
            }
        return response()->json($res);

             
        }catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
     }


     public function getProdcutDetails($id){
         $query = Product::select('products.*','product_image.image','product_image.caption','manufacturer.manufacturer_name','products.qty','products.discount','products.uses')
             ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
             ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
             ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
              ->Join('product_image', function($join)
            {
              $join->on('product_image.product_id', '=', 'products.id');
            }) 
            ->where('products.id',$id)
             ->where('product_image.set_primary','Yes')->groupBy('product_image.prod_id');
            $data =$query->first();
            return $data;
     }





      public function order_review(Request $request){
       try{
      //  echo "asdfas"; die();
            $validator = Validator::make($request->all(), [
              'order_id' => 'required',
              'rating' => 'required',
            ]);
            if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
            }

            $order_id = $request->get('order_id');
            $rating = $request->get('rating');
            $description = $request->get('description');
            /*======================= OrderReview  ==========================*/
            $save = new OrderReview();
            $save->order_id      = $order_id;
            $save->rating        = $rating;
            $save->user_id       = $this->userId;
            $save->description   = $description;
           $data =  $save->save();

        if($data<0){
            $res['error'] =false;
            $res['code'] =0;
            $res['message'] = "Not Found data";
         }else{
            $res['error'] =false;
            $res['code'] =200;
            $res['message'] = "Thank you for review!";
        }
        return response()->json($res);

             
        }catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
     }

    // Retun Product APIS
     public function return_order_item_list(Request $request){
          $validator = Validator::make($request->all(), [
              'order_id' => 'required',                                                                                       
            ]);
            if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
            }
         try{
          $order_id = $request->order_id;
              $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItem')
             // ->where('user_id',$this->userId)
              ->where('id',$order_id)->first(); 
              $order['delivery_address_info'] = json_decode($order->delivery_address_info);
              $order['patient_info'] = json_decode($order->patient_info);
              $order['delivery_time_info'] = Carbon::parse($order->created_at)->diffForHumans();
            $ProductOrderItemArray=[];      
            foreach ($order['OrderItem'] as $ProductOrderItem){
              $vendor = $this->user->where('role','vendor')->where('id',$ProductOrderItem->vendor_id)->first(); 
              $ProductOrderItem['data']             = json_decode($ProductOrderItem['data']);
              $ProductOrderItem['vendor_firm_name'] = isset($vendor->farm_name)?$vendor->farm_name:'No Found Farm Name';
              $ProductOrderItem['vendor_details']   =  $vendor;
              $ProductOrderItemArray[]              = $ProductOrderItem;
            } 
          $val = OrderReview::where('order_id',$request->order_id)->where('user_id',$this->userId)->count();
          $order['order_rating'] = $val;
          return $this->showResponse($order); 
         }catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }  
     }


      public function return_product(Request $request){
        $data = $request->product_return;
          $data_update ='';
          foreach ($data as $key => $value) {
             $data_update = OrderItem::where('order_id',$value['order_id'])->where('ord_item_id',$value['order_item_id'])->update(['is_return'=>1,'quantity_cancelled'=>$value['qty'],'reason'=>$value['reason']]); 
             Order::where('id',$value['order_id'])->update(['order_status'=>'R']);
          }
          $res['error'] = false;
          $res['code'] = 200;
          $res['message'] = "Sucsessfully Update";

        return response()->json($res);


      }
 
}
