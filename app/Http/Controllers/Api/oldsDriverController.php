<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UserWallet;
use App\Models\Driver;
use App\Models\DeclineProducts;
use App\Models\AdminNotification;
use Illuminate\Support\Str;
use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB; 
use App;
use Session;
use Mail;
class DriverController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;
    /*Here is call Construct Method*/
    function __construct(User $user,UserWallet $userWallet,Driver  $driver){
        $this->user = $user;
        $this->driver = $driver;
        $this->userWallet = $userWallet;
    }

    //Driver Register =============
    public function driver_register(Request $request){
        //Here is put validation form fildes ===============
        $validator = Validator::make($request->all(), [
            'device_type'=>'required|in:A,I',
            'name' => 'required',
            'device_id' => 'required',
            'address' => 'required',
            'mobile_number' => 'required|max:10|unique:users',
            'email' => 'required|email|unique:users',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        try { 
            $otp = '123456';
            $user_temp = new UserTemp();
            $user_temp->mobile_number = $request->mobile_number;    
            $user_temp->otp           = $otp;
            $user_temp->name          = $request->name;
            $user_temp->device_type   = $request->device_type;
            $user_temp->device_id     = $request->device_id;
            $user_temp->address       = $request->address;
            $user_temp->email       = $request->email;
            $user_temp->save();
            $response = [
                'code' => 200,
                'otp' => $otp,
                'user_type' => 'new',
                'message' => 'OTP sent successfully.'
            ];
            return response()->json($response, 200); 
        } catch (\Exception $e) {
            return $e;
        } 
          
    }

    
    // Driver Verify Opt
    public function verify_otp(Request $request) {
         $response = array();
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'expire' => 'required',
            'type'=>'required|in:1,2',
            'otp' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        if($request->expire!=1){
                $response['code'] = 1;
                $response['message'] = 'OTP expired, Please try again !';
        }else{
            if($request->type==2){
                    $verifyOtp          =   UserTemp::where('mobile_number',$request->mobile_number)->first();
                    if(empty($verifyOtp)){
                        $response["code"]           =   0; 
                        $response["message"]        =   trans("Invalid details");
                        $response["data"]           =   (object)array();
                        return response()->json($response,203);
                    }else{
                        if ($verifyOtp->otp == $request->otp) {
                            $getuser          =   User::select('id','mobile_number')->where('role','driver')->where('mobile_number',$request->mobile_number)->first(); 
                              if(empty($getuser)){
                                $user_temp                = new User();
                                $user_temp->mobile_number = $verifyOtp->mobile_number;    
                                $user_temp->device_type   = $verifyOtp->device_type;
                                $user_temp->device_id     = $verifyOtp->device_id;
                                $user_temp->email         = $verifyOtp->email;
                                $user_temp->address       = $verifyOtp->address;
                                $user_temp->name       = $verifyOtp->name;
                                $user_temp->status       = '0';
                                $user_temp->otp           = null;
                                $user_temp->role           = 'driver';
                                $user_temp->is_mobile_number_verifed = 1;
                                $user_temp->save();
                                 UserTemp::where('mobile_number', $request->mobile_number)->delete();
                           }
                           UserTemp::where('mobile_number',$request->mobile_number)->where('otp',$request->otp)->update(array('otp_verify'=>1));
                           $user_details          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 

                            $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                            $response["code"]          =   200; 
                            $response["message"]       =   trans("Your mobile number has been verified successfully.");
                            $response["data"]          =   $user_details;
                            $response['data']['token'] = $refreshed;
                            return response()->json($response,200);
                        }else{
                            $response["code"]         =   0; 
                            $response["message"]        =   trans("Invalid OTP.");
                            $response["data"]           =   (object)array();
                            return response()->json($response,203);
                        }
                    }
            }else{
                //Driver verify otp 
                    $verifyOtp          =   User::where('mobile_number',$request->mobile_number)->first();
                    if(empty($verifyOtp)){
                        $response["code"]           =   0; 
                        $response["message"]        =   trans("Invalid details");
                        $response["data"]           =   (object)array();
                        return response()->json($response,203);
                    }else{
                        if ($verifyOtp->otp == $request->otp) {
                            $getuser          =   User::select('id','mobile_number')->where('role','driver')->where('mobile_number',$request->mobile_number)->first(); 

                           $user_details          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 
                            $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                            $response["code"]          =   200; 
                            $response["message"]       =   trans("Your mobile number has been verified successfully.");
                            $response["data"]          =   $user_details;
                            $response['data']['token'] = $refreshed;
                            return response()->json($response,200);
                        }else{
                            $response["code"]         =   0; 
                            $response["message"]        =   trans("Invalid OTP.");
                            $response["data"]           =   (object)array();
                            return response()->json($response,203);
                        }
                    }    
            }         
        }
        return response()->json($response, 201);
    }

    // Resend OTP ===================
    public function resend_otp(Request $request) {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'resend_type' => 'required'
        ]);
        if ($validator->fails()){
            $response = [
                'code' => 0,
                'error' => true,
                'message' => $validator->errors()->first(),
            ];
            
            return response()->json($response, 200);
        }
        /* 1=Register OTP, 2=Login OTP */
        if ($request->resend_type==1) {
            //echo "1"; die();
            $otp_details = DB::table('user_temp')->where('mobile_number', $request->mobile_number)->first();
            $otp = $otp_details->otp;
           //print_r($otp_details);die;
            if (isset($otp_details) && !empty($otp_details)) {
                if (isset($otp_details->otp) && !empty($otp_details->otp)) {

                  // $otp = rand(1000, 9999);
                 $otp = '123456';
                    DB::table('user_temp')->where('mobile_number',$request->mobile_number)->update(array('otp'=>$otp));
                    $dataotp = DB::table('user_temp')->select('mobile_number','otp')->where('mobile_number', $request->mobile_number)->first();
                    //return $dataotp;
                    $response_data['message'] = 'OTP sent successfully.';
                    $response_data['code'] = 200;
                    $response_data['data'] =$dataotp;
                } else {
                    $response_data['code'] = 1;
                    $response_data['message'] = 'OTP not send.';
                }
            } else {
                $response_data['code'] = 1;
                $response_data['message'] = 'OTP not Match.';
            }
        } else if ($request->resend_type==2) {
            //echo "ok sure";; die();
            $otp_details = DB::table('users')->where('role','driver')->where('mobile_number', $request->mobile_number)->first();
           // echo "<pre>"; print_r($otp_details); die();
            if (isset($otp_details) && !empty($otp_details)) {
              //  echo "sdafasd"; die();
                    $otp = '123456';
                    User::where('mobile_number',$request->mobile_number)->where('role','driver')->update(array('otp'=>$otp));
                    $dataotp = User::where('mobile_number', $request->mobile_number)->where('role','driver')->first();
                    $response_data['message'] = 'OTP sent successfully....';
                    $response_data['code'] = 200;
                    $response_data['data'] =$dataotp;

            } else {
                $response_data['code'] = 1;
                $response_data['message'] = 'OTP not Match.';
            }
        } else {
            $response_data['code'] = 1;
            $response_data['message'] = 'Invalid selection.';
        }
        
        $response_data['error'] = false;
        return response()->json($response_data, 200);
    }

    //Driver login =============
    public function driver_login(Request $request){
        //Here is put validation form fildes ===============
        $validator = Validator::make($request->all(), [
            'device_type'=>'required|in:A,I',
            'device_id' => 'required',
            'mobile_number' => 'required|max:10',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        try { 
            $check_user = User::where('mobile_number',$request->mobile_number)->where('role','driver')->count();

           if($check_user>0){
                $loginuser = User::where('mobile_number',$request->mobile_number)->where('role','driver')->first();
              if($loginuser->status != 0){
                $otp = '123456';
                User::where('mobile_number',$request->mobile_number)->where('role','driver')->update(array('otp'=>$otp));
                $dataotp = User::where('mobile_number', $request->mobile_number)->first();
                $response['message'] = 'OTP sent successfully.';
                $response['code'] = 200;
                $response['data'] =$dataotp;
                return response()->json($response, 200); 
               }else{
                $response['message'] = 'Your account has not been activated.';
                $response['code'] = 200;
                return response()->json($response, 200); 
               } 
           }else{
            $response['message'] = 'mobile number is not exit.';
            $response['code'] = 0;
           return response()->json($response); 
           }

        } catch (\Exception $e) {
            return $e;
        } 
    }

     //Driver Profile
        public function driver_profile(Request $request){
          $user_id = Auth::guard('api')->user()->id;
          $data = User::with('DriverData')->where('id',$user_id)->first();
          $is_doc = Driver::where('user_id',$user_id)->count();
          $data['id_document'] = $is_doc;
          return $this->showResponse($data,'');
        } 


      public function upload_document(Request $request){
        try {

            $user_id = Auth::guard('api')->user()->id;
            $validator = Validator::make($request->all(), [
                'aadhar_card'   =>  'required|image|mimes:jpeg,png,jpg',
                'pan_card'   =>  'required|image|mimes:jpeg,png,jpg',
                'driving_license'   =>  'required|image|mimes:jpeg,png,jpg',
                'plate_number'   =>  'required|image|mimes:jpeg,png,jpg',
                'insurance'   =>  'required|image|mimes:jpeg,png,jpg',
                'vehicle_registration_certificate'   =>  'required|image|mimes:jpeg,png,jpg',
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $input = $request->all();

            if ($request->hasFile('aadhar_card')) {
              $folder_name = 'aadhar-card';
              $input['aadhar_card'] = $this->fileUpload($request->file('aadhar_card'), false, $folder_name);
            }

            if ($request->hasFile('pan_card')) {
              $folder_name = 'pan_card';
              $input['pan_card'] = $this->fileUpload($request->file('pan_card'), false, $folder_name);
            }  

            if ($request->hasFile('driving_license')) {
              $folder_name = 'driving_license';
              $input['driving_license'] = $this->fileUpload($request->file('driving_license'), false, $folder_name);
            }

            if ($request->hasFile('plate_number')) {
              $folder_name = 'plate_number';
              $input['plate_number'] = $this->fileUpload($request->file('plate_number'), false, $folder_name);
            }

            if ($request->hasFile('insurance')) {
              $folder_name = 'insurance';
              $input['insurance'] = $this->fileUpload($request->file('insurance'), false, $folder_name);
            } 

            if ($request->hasFile('vehicle_registration_certificate')) {
              $folder_name = 'vehicle_registration_certificate';
              $input['vehicle_registration_certificate'] = $this->fileUpload($request->file('vehicle_registration_certificate'), false, $folder_name);
            }
            
            $pan_card = $input['pan_card'];
            $aadhar_card = $input['aadhar_card'];
            $driving_license = $input['driving_license'];
            $plate_number = $input['plate_number'];
            $insurance = $input['insurance'];
            $vehicle_registration_certificate = $input['vehicle_registration_certificate'];

            $check_user = $this->driver->where('user_id',$user_id)->count();
            //echo "===".$check_user; die();
            //echo "===".; die(); 
            if($check_user>0){
                Driver::where('user_id',$user_id)->update(['pan_card'=>$pan_card,'aadhar_card'=>$aadhar_card,'driving_license'=>$driving_license,'plate_number'=>$plate_number,'insurance'=>$insurance,'vehicle_registration_certificate'=>$vehicle_registration_certificate]);
                $response = [
                    'code' => 200,
                    'message' => 'Document Upload successfully.'
                ];
                return response()->json($response, 200); 
            }else{
              $save = new Driver();
              $save->user_id = $user_id;
              $save->aadhar_card = $pan_card;
              $save->pan_card    = $aadhar_card;
              $save->driving_license    = $driving_license;
              $save->plate_number    = $plate_number;
              $save->insurance    = $insurance;
              $save->vehicle_registration_certificate    = $vehicle_registration_certificate;
              $save->save();
              $response = [
                'code' => 200,
                'message' => 'Document Upload successfully.'
              ];
              return response()->json($response, 200); 
            }
        }catch(\Exception $e){
         return $e;
       }    
    }

  //Driver Order list //
        public function order_list()
        {

         // echo "ok thanks"; die();
         try {
              $user_id = Auth::guard('api')->user()->id;
              //echo "string".$user_id; die();
              $remove_ids_order = DeclineProducts::select('order_id')->where('user_id',$user_id)->where('type','driver')->pluck('order_id')->toArray();  
             // return $remove_ids_order;
              $data = Order::with('User','OrderPayment','OrderItem')->whereNotIn('id',$remove_ids_order)->where('order_status','AC')->orderBy('id', 'DESC')->get();

                foreach ($data as $key => $value) {
                  $data[$key]['patient_info'] = json_decode($value->patient_info);
                  $data[$key]['delivery_address_info'] = json_decode($value->delivery_address_info);
                  $data[$key]['offer_data'] = json_decode($value->offer_data);
                }
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data); 

    } 



    //Driver Order Status accept / reject

    public function booking_status(Request $request){

            $user_id = Auth::guard('api')->user()->id;
            //echo $user_id; die();    
            $validator = Validator::make($request->all(), [
                'order_id'   =>  'required',
                'status'   =>  'required|in:0,1',  //Status = 1 accept 2 = Reject 
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $order_id = $request->order_id;

            if($request->status==1){
                    $status = OrderItem::where('order_id', $order_id)
                    ->update(array('driver_id' => $user_id));
                    $status = Order::where('id', $order_id)
                    ->update(array('order_status' =>'AD'));
                    
                    $response = [
                    'code' => 200,
                    'message' => 'You has been created successfully accepte order.'
                    ];
                    return response()->json($response, 200); 
            }else{
                $ord_item_id = OrderItem::select('ord_item_id')->where('order_id',$order_id)->get();
                $orderItem = [];
                foreach ($ord_item_id as $key => $val) {
                   // echo $val->ord_item_id; die();
                    $orderItem =  new DeclineProducts();
                    $orderItem->ord_product_item_id = $val->ord_item_id;
                    $orderItem->order_id = $order_id;
                    $orderItem->type = 'driver';
                    $orderItem->user_id = Auth::guard('api')->user()->id;
                    $orderItem->save();
                }
                if ($orderItem) {
                  $response['code'] = 200;
                  $response['message'] = "You  has been created successfully decline order.";
                return json_encode($response);
                } else {
                  $response['code'] = 200;
                  $response['message'] = "There was an issue account createing the recode. Please try again.";
                return json_encode($response);
                }
            }
   
    }

    public function accept_driver_orders(Request $request){
     try {
         $driver_id = Auth::guard('api')->user()->id;
        $accepted_doctor_orders  = Order::with(['User','OrderPayment','OrderItem'])
          ->whereHas('OrderItem', function($q) use ($driver_id) {
          $q->where('driver_id',$driver_id);
        });
        $data = $accepted_doctor_orders->get();
        foreach ($data as $key => $value) {
            $data[$key]['patient_info'] = json_decode($value->patient_info);
            $data[$key]['delivery_address_info'] = json_decode($value->delivery_address_info);
            $data[$key]['offer_data'] = json_decode($value->offer_data);
        }
       //Vendor ------------------
      } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
      }
        return $this->showResponse($data); 

    }

    //Order Details
     public function order_details(Request $request){
       $user_id = Auth::guard('api')->user()->id;
            $validator = Validator::make($request->all(), ['order_id'   =>  'required',]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
         try {
            $driver_id = Auth::guard('api')->user()->id;
            $order_id = $request->order_id;
            $accepted_doctor_orders  = Order::with(['User','OrderPayment','OrderItem'])->where('id',$order_id)
              ->whereHas('OrderItem', function($q) use ($driver_id) {
              $q->where('driver_id',$driver_id);
            });
            $data = $accepted_doctor_orders->first();
            $data['patient_info'] = json_decode($data->patient_info,true);
            $data['delivery_address_info'] = json_decode($data->delivery_address_info,true);
            $data['offer_data'] = json_decode($data->offer_data,true);

            $vendor_ids = OrderItem::select('vendor_id')->where('order_id',$order_id)->where('driver_id',$driver_id)->pluck('vendor_id')->toArray(); 
           // return $vendor_ids;>
            $data['vendor_info'] = User::whereIn('id',$vendor_ids)->get();

      } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
      }
        return $this->showResponse($data);    
             
     }

     public function update_driver_status(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'order_id'   =>  'required',
                'status'   =>  'required|in:picked,unpicked',  //Status = 1 accept 2 = Reject 
                'vendor_id'   =>  'required',  //Status = 1 accept 2 = Reject 
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $order_id = $request->order_id;
            $vendor_id = $request->vendor_id;
            $status = $request->status;
            $status= OrderItem::where('vendor_id',$vendor_id)->where('order_id',$order_id)->update(['driver_status'=>$status]);
             $response = [
                'code' => 200,
                'message' => 'Order picked',
              ];
            return response()->json($response, 200); 

        } catch (\Exception $e) {
             return $this->clientErrorResponse($e);
        }
     }

     public function trip_history(Request $request){
      try{
      	//echo Carbon::now()->subMonth()->month; die();
 			$validator = Validator::make($request->all(), [
                'type'   =>  'required|in:1,2,3',  //Type = 1 today 2 = Week  3 Months 
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
        $type = $request->type;

       	$driver_id = Auth::guard('api')->user()->id;
        
        $accepted_doctor_orders  = Order::with(['User','OrderPayment','OrderItem'])->where('order_status','D')
          ->whereHas('OrderItem', function($q) use ($driver_id) {
          $q->where('driver_id',$driver_id);
        });
        if($type==1){
        	$accepted_doctor_orders->whereDate('created_at',$date);
        }elseif($type==2){
        	$accepted_doctor_orders->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        }else{
        	$accepted_doctor_orders->whereMonth('created_at', '=', Carbon::now()->subMonth()->month);
        }  
        $data = $accepted_doctor_orders->get();

        //echo "<pre>"; print_r($data); die();
        foreach ($data as $key => $value) {
            $data[$key]['patient_info'] = json_decode($value->patient_info);
            $data[$key]['delivery_address_info'] = json_decode($value->delivery_address_info);
            $data[$key]['offer_data'] = json_decode($value->offer_data);
        }
        return $this->showResponse($data); 
      } catch (\Exception $e) {
             return $this->clientErrorResponse($e);
        }
     }




}
