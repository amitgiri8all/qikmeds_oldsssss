<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UserWallet;
use App\Models\Driver;
use App\Models\DeclineProducts;
use App\Models\AdminNotification;
use App\Models\Notification;
use App\Models\Zone;
use App\Models\VendorProductStatus;
use App\Models\VendorTrackOrder;
use App\Models\DriverSessionTime;
use App\Models\PaymentMethod;
use App\Notifications\AllOrderStatus;
use App\Notifications\OrderAccept;
use Illuminate\Support\Str;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Validator;
use Paginator;

use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB; 
use App;
use Session;
use Mail;

class VendorController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;
    /*Here is call Construct Method*/
    function __construct(User $user,UserWallet $userWallet,Driver  $driver,Zone $zone){
        $this->user     = $user;
        $this->driver   = $driver;
        $this->userWallet = $userWallet;
        $this->zone       = $zone;
    }

    //Driver login =============
    public function vendor_login(Request $request){
        //Here is put validation form fildes ===============
        $validator = Validator::make($request->all(), [
            'device_type'=>'required|in:A,I',
            'device_id' => 'required',
            'mobile_number' => 'required|max:10',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        try { 
          //  DB::enableQueryLog(); 
           // echo "===".$request->mobile_number;
            $check_user = User::where('mobile_number',$request->mobile_number)->where('role','=','vendor')->count();
           if($check_user>0){
                $loginuser = User::where('mobile_number',$request->mobile_number)->where('role','vendor')->first();
             //if($loginuser->status != 0){
                $otp = '123456';
                User::where('mobile_number',$request->mobile_number)->where('role','vendor')->update(array('otp'=>$otp));
                $dataotp = User::where('mobile_number', $request->mobile_number)->first();
                 $dataotp['zone_data'] = json_decode($dataotp->zone_data);
                $response['message'] = 'OTP sent successfully.';
                $response['code'] = 200;
                $response['data'] =$dataotp;
                return response()->json($response, 200); 
              /* }else{
                $response['message'] = 'Your account has not been activated.';
                $response['code'] = 0;
                return response()->json($response, 200); 
               } */
           }else{
            $response['message'] = 'mobile number is not exit.';
            $response['code'] = 0;
           return response()->json($response); 
           }

        } catch (\Exception $e) {
            return $e;
        } 
    }
   // Driver Verify Opt
    public function verify_otp(Request $request) {

       // echo "ok"; die();
         $response = array();
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'expire' => 'required',
            'otp' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        if($request->expire!=1){
                $response['code'] = 1;
                $response['message'] = 'OTP expired, Please try again !';
        }else{
                //Driver verify otp 
                    $verifyOtp          =   User::where('mobile_number',$request->mobile_number)->first();
                    if(empty($verifyOtp)){
                        $response["code"]           =   0; 
                        $response["message"]        =   trans("Invalid details");
                        $response["data"]           =   (object)array();
                        return response()->json($response,203);
                    }else{
                        if ($verifyOtp->otp == $request->otp) {
                            $getuser          =   User::select('id','mobile_number')->where('role','vendor')->where('mobile_number',$request->mobile_number)->first(); 

                           $user_details          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 
                            $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                            $response["code"]          =   200; 
                            $response["message"]       =   trans("Your mobile number has been verified successfully.");
                            $response["data"]          =   $user_details;
                            $response['data']['token'] = $refreshed;
                            return response()->json($response,200);
                        }else{
                            $response["code"]         =   0; 
                            $response["message"]        =   trans("Invalid OTP.");
                            $response["data"]           =   (object)array();
                            return response()->json($response,203);
                        }
                    }    
                     
        }
        return response()->json($response, 201);
    }

    // Resend OTP ===================
    public function resend_otp(Request $request) {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
        ]);
        if ($validator->fails()){
            $response = [
                'code' => 0,
                'error' => true,
                'message' => $validator->errors()->first(),
            ];
            
            return response()->json($response, 200);
        }
        /* 1=Register OTP, 2=Login OTP */
         
            //echo "ok sure";; die();
            $otp_details = DB::table('users')->where('role','vendor')->where('mobile_number', $request->mobile_number)->first();
           // echo "<pre>"; print_r($otp_details); die();
            if (isset($otp_details) && !empty($otp_details)) {
              //  echo "sdafasd"; die();
                    $otp = '123456';
                    User::where('mobile_number',$request->mobile_number)->where('role','vendor')->update(array('otp'=>$otp));
                    $dataotp = User::where('mobile_number', $request->mobile_number)->where('role','vendor')->first();
                    $response_data['message'] = 'OTP sent successfully....';
                    $response_data['code'] = 200;
                    $response_data['data'] =$dataotp;

            } else {
                $response_data['code'] = 1;
                $response_data['message'] = 'OTP not Match.';
            }
         
        $response_data['error'] = false;
        return response()->json($response_data, 200);
    }


     //Driver Profile
        public function vendor_profile(Request $request){
         $user_id = Auth::guard('api')->user()->id;
        //  $user_id =1;
          $data = User::with('DriverData','DriverDataBank')->where('id',$user_id)->first();
          return $this->showResponse($data,'');
        } 


         /** Show Order for Vendor near by  **/
  public function orders_list_by_vendors()
  {

    $site = DB::table('site_settings')->where('id', 1)->first();
    $near_by_queue_time = date('H', strtotime($site->near_by_queue_time));
    $city_queue_time    = date('H', strtotime($site->city_queue_time));
    $notifications_time = $city_queue_time + $near_by_queue_time;

    $vendor_orders = Order::with('User', 'OrderPayment', 'DeliveryAddress', 'OrderItem')
      ->whereHas('OrderItem', function ($q) {
        $q->whereNull('vendor_id');
      })->where('created_at', '>', Carbon::now()->subMinutes($near_by_queue_time)->toDateTimeString())
      ->where('order_status','CC')->orWhere('order_status','PD')->orderBy('id','desc')->get();

    $vendor_orders_city_quee = Order::with('User', 'OrderPayment', 'DeliveryAddress', 'OrderItem')
      ->whereHas('OrderItem', function ($q) {
        $q->whereNull('vendor_id');
      })->where('created_at', '<', Carbon::now()->subMinutes($near_by_queue_time)->toDateTimeString())
       ->where('order_status','CC')->orWhere('order_status','PD')->orderBy('id','desc')->get();

       foreach($vendor_orders as $key=>$value){
            foreach($value->orderItem as $keys=>$val){
               $vendor_orders[$key]['orderItem'][$keys]['data'] = json_decode($val->data);
            }
       }

       foreach($vendor_orders_city_quee as $key=>$value){
            foreach($value->orderItem as $keys=>$val){
               $vendor_orders_city_quee[$key]['orderItem'][$keys]['data'] = json_decode($val->data);
            }
       }

   $data ['vendor_orders_near_by'] = $vendor_orders;
   $data ['vendor_orders_city_quee'] = $vendor_orders_city_quee;
    return $this->showResponse($data,'');

   }

   /**Order Validate Details Page**/
  public function order_validate(Request $request)
  {
    $id = $request->order_id;
    $order = Order::with('User','OrderPayment','DeliveryAddress','OrderItemVendorNull')->where('id', $id)->first();

    $order['OrderItemVendorNull'][0]['data'] = json_decode($order->OrderItemVendorNull[0]['data']);
    return $this->showResponse($order,'');
  }

   /**Order decline**/
  public function decline_orders(Request $request){
    $vendor_id = Auth::guard('api')->user()->id;
    $order_id = $request->get('order_id');

    $get_all_order_item = OrderItem::select('ord_item_id')->whereNull('vendor_id')->where('order_id', $order_id)->get();
         $orderItem = [];
         foreach ($get_all_order_item as $key => $val) {
          $orderItem =  new DeclineProducts();
          $orderItem->ord_product_item_id = $val->ord_item_id;
          $orderItem->order_id = $order_id;
          $orderItem->type = 'vendor'; 
          $orderItem->user_id = $vendor_id;
          $orderItem->save();
        }
        if ($orderItem) {
          $response['error'] = false;
          $response['code'] = 200;
          $response['message'] = "You  has been created successfully decline order.";
          return json_encode($response);
        } else {
          $response['error'] = true;
          $response['code'] = 0;
          $response['message'] = "There was an issue account createing the recode. Please try again.";
          return json_encode($response);
        }
  }

   /**Order Accepted**/
  public function accepte_orders(Request $request)
  {
    //echo "sdfas"; die();
    $vendor_id = Auth::guard('api')->user()->id;
    $order_id = $request->get('order_id');
    $ord_item_id_arr = $request->get('ord_item_id');
    //Vendor Product Status 
    $status = new VendorProductStatus();
    $status->order_id = $order_id;
    $status->vendor_id = $vendor_id;
    $status->save();

    $track = new VendorTrackOrder();
    $track->order_id = $order_id;
    $track->vendor_id = $vendor_id;
    $track->date_time = Carbon::now();
    $track->track_status = 'Order in Progress';
    $track->save();

    ///Comma seprate value
    $ord_item_id = explode(',',$ord_item_id_arr);
    //echo "<pre>"; print_r($ord_item_id); die();
    if (!empty($ord_item_id)) {
      $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
        ->update(array('vendor_id' => $vendor_id));

      $get_all_unchecked_ids = OrderItem::select('ord_item_id')->whereNull('vendor_id')->where('order_id', $order_id)->get();
      if (!empty($get_all_unchecked_ids)) {
        foreach ($get_all_unchecked_ids as $key => $val) {
          $orderItem =  new DeclineProducts();
          $orderItem->ord_product_item_id = $val->ord_item_id;
          $orderItem->order_id = $order_id;
          $orderItem->user_id = $vendor_id;
          $orderItem->type = 'vendor';
          $orderItem->save();
        }
      }
    } else {
      $get_all_order_item = OrderItem::select('ord_item_id')->whereNull('vendor_id')->where('order_id', $order_id)->get();
      $ord_item_id = $get_all_order_item->pluck('ord_item_id');
      $status = OrderItem::whereIn('ord_item_id', $ord_item_id)
        ->update(array('vendor_id' => $vendor_id));
    }
    if ($status) {
      $updateOrderStatus = OrderItem::where('order_id', $order_id)->whereNull('vendor_id')->count();
      if ($updateOrderStatus > 0) {
        $status = "PD";
      } else {
        $status = "AC";
      }
      Order::where('id', $order_id)
        ->update(array('order_status' => $status));
            $response['error'] = false;
            $response['code'] = 200;
            $response['message'] = "You has been created successfully accepte order.";
            return json_encode($response);
    } else {
        $response['error'] = true;
        $response['code'] = 0;
        $response['message'] = "There was an issue account createing the recode. Please try again.";
      return json_encode($response);
    }
  }


     public function notification(Request $request){
        $vendor_id = Auth::guard('api')->user()->id; 

         $alert_notification = Notification::orderBy('id','DESC')->where('notifiable_id', $vendor_id)->get();

          return $this->showResponse($alert_notification,'');
    }

    public function availablity(Request $request){
       $availablity_status = $request->get('status');
 
      $vendor_id = Auth::guard('api')->user()->id;
     // echo $vendor_id; die();
      if($availablity_status=='Yes'){ $status="Yes"; }else{ $status='No';}; 

       $status = User::where('id',$vendor_id)->update(['is_online'=>$status]);
       if(!empty($status)){
                $response['error'] = false;
                $response['code'] = 200;
                $response['message'] = "Availablity has changed.";
                return json_encode($response);
          }else{
            $response['error'] = true;
            $response['code'] = 0;
            $response['message'] = "There was an issue account createing the recode. Please try again.";
          }
    }


    /** Show Accpted Orders for Vendor near by  **/
  public function accepted_orders_list(Request $request)
  {
    //echo $request->get('start');die();
    $start = date('Y-m-d', strtotime($request->get('start')));
    $end   = date('Y-m-d', strtotime($request->get('end')));
  //  echo $start.'===='.$end; die;
    $vendor_id = Auth::guard('api')->user()->id;
    $vendor_orders = Order::with(['User', 'OrderPayment', 'DeliveryAddress', 'OrderItem'])
      ->whereHas('OrderItem', function ($q) use ($vendor_id) {
        $q->where('vendor_id', $vendor_id);
      });
    //check data if date between two dates
    if (!empty($request->get('start')) && !empty($request->get('end'))) {
      $vendor_orders->where('updated_at', '>=', $start)
                           ->where('updated_at', '<=', $end);
    }

    $vendor_orders = $vendor_orders->get();
      foreach($vendor_orders as $key=>$value){
            foreach($value->orderItem as $keys=>$val){
               $vendor_orders[$key]['orderItem'][$keys]['data'] = json_decode($val->data);
            }
       }

        if(!empty($vendor_orders)){
                $response['error'] = false;
                $response['code'] = 200;
                $response['data'] = $vendor_orders;
                $response['message'] = "Vendor orders accepted.";
                return json_encode($response);
          }else{
            $response['error'] = true;
            $response['code'] = 0;
            $response['message'] = "There was an issue account createing the recode. Please try again.";
          }
  }



}

