<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UserWallet;
use App\Models\AdminNotification;
use Illuminate\Support\Str;
use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
use Mail;
class UserController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;
    /*Here is call Construct Method*/
    function __construct(User $user,UserWallet $userWallet){
        $this->user = $user;
        $this->userWallet = $userWallet;
    }

     public function register(Request $request){
        //Here is put validation form fildes ===============
         $validator = Validator::make($request->all(), [
            'device_type'=>'required|in:A,I',
            'login_type' => 'required',
            'mobile_number' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        //Here is Store All Information a variable ========
        $input = $request->all();
        
        $check_user  = $this->user->withTrashed()->where(['mobile_number'=>$request->mobile_number])->first();
        //Here is chekc User Exit or Not 
        if($check_user){
         //  return $this->text_local_send_otp();   
            //Login type ==1 login with password
            if($request->login_type==1){
                $validator = Validator::make($request->all(), [
                  'password' => 'required',
                ]);
                if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
                }
             //Here is chekc user password Exit or Not 
            $check_user_pass  = $this->user->withTrashed()->where(['mobile_number'=>$request->mobile_number])->first();
                if(!empty($check_user_pass->password)){
                    $userdata = array(
                     'mobile_number'     => $request->mobile_number,
                     'password'  => $request->password
                    );
                    $guard = 'web';
                    //Loign User
                    if (Auth::guard($guard)->attempt($userdata)) {
                        if(Auth::guard( $guard)->user()->status != '0'){
                         Auth::guard($guard)->user()->save();
                         User::where('mobile_number',$request->mobile_number)->update(array('device_type'=>$request->device_type,'device_id'=>$request->device_id,'device_token'=>$request->device_token));
                         $user_details = User::where('mobile_number',$request->mobile_number)->first();
                         $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                         $response['code'] = 200;
                         $response['data'] = $user_details;
                         $response['data']['token'] = $refreshed;
                         $response['message'] = "Login successfully.";
                         return response()->json($response, 200);
                        }else{
                         $response['code']    = 0;
                         $response['message'] = "Your account is not enabled. please contact your administrator to access away.";
                         return response()->json($response, 200);  
                        }
                    } else {
                        $response['code'] = 0;
                        $response['message'] = "Invalid password. Please try again";
                        return response()->json($response, 200);  
                    }
                }else{
                    $response = [
                            'code' => 404,
                            'message' => 'No password is set with this mobile No. Please login with OTP.',
                    ];
                    return response()->json($response, 200);  
                }
            }else{

                // Session::put('name','Amit');
                $otp = 123456;
                User::where('mobile_number',$request->mobile_number)->update(array('otp'=>$otp,'device_type'=>$request->device_type,'device_id'=>$request->device_id,'device_token'=>$request->device_token));
                $response = [
                        'code' => 200,
                        'otp' => $otp,
                        'user_type' => 'old',
                        'message' => 'Please verify your mobile no by entering the OTP sent to you.'
                ];
                return response()->json($response, 200);
            }
        }else{

            $otp = 123456;
            $user_temp = new UserTemp();
            $user_temp->mobile_number = $request->mobile_number;    
            $user_temp->otp           = $otp;
            $user_temp->device_type   = $request->device_type;
            $user_temp->device_id     = $request->device_id;
            $user_temp->device_token  = $request->device_token;
            $user_temp->referrer_code =Str::random(8);
             $user_temp->save();
            $response = [
                'code' => 200,
                'otp' => $otp,
                'user_type' => 'new',
                'message' => 'OTP sent successfully.'
            ];
            return response()->json($response, 200);        
        }
    }
 
     public function verify_otp(Request $request) {
         $response = array();
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'expire' => 'required',
            'otp' => 'required',
            'verify_type' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        if($request->expire!=1){
                $response['code'] = 1;
                $response['message'] = 'OTP expired, Please try again !';
        }else{
            if($request->verify_type=='new'){

                $verifyOtp          =   UserTemp::where('mobile_number',$request->mobile_number)->first();
              //  echo "<pre>"; print_r($verifyOtp); die();    
                if(empty($verifyOtp)){
                    $response["code"]           =   0; 
                    $response["message"]        =   trans("Invalid details");
                    $response["data"]           =   (object)array();
                    return response()->json($response,203);
                }else{
                    if ($verifyOtp->otp == $request->otp) {
                        $getuser          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 
                          if(empty($getuser)){
                            $user_temp                = new User();
                            $user_temp->mobile_number = $verifyOtp->mobile_number;    
                            $user_temp->device_type   = $verifyOtp->device_type;
                            $user_temp->device_id     = $verifyOtp->device_id;
                            $user_temp->device_token  = $verifyOtp->device_token;
                            $user_temp->referrer_code = $verifyOtp->referrer_code;
                            $user_temp->otp           = null;
                            $user_temp->is_mobile_number_verifed = 1;
                            $user_temp->save();
                             UserTemp::where('mobile_number', $request->mobile_number)->delete();
                       }
                       
                        UserTemp::where('mobile_number',$request->mobile_number)->where('otp',$request->otp)->update(array('otp_verify'=>1));
                       $user_details          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 

                        $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                        $response["code"]          =   200; 
                        $response["message"]       =   trans("Your mobile number has been verified.");
                        $response["data"]          =   $user_details;
                        $response['data']['token'] = $refreshed;
                        return response()->json($response,200);
                    }else{
                        $response["code"]         =   0; 
                        $response["message"]        =   trans("Invalid OTP.");
                        $response["data"]           =   (object)array();
                        return response()->json($response,203);
                    }
                } 
            }else if($request->verify_type=='old'){
               // echo "ok fine"; die();
               $verifyOtpOldUser          =   User::where('mobile_number',$request->mobile_number)->first();    
               if(!empty($verifyOtpOldUser)){
                    if ($verifyOtpOldUser->otp == $request->otp) {
                     //  echo "ok die()"; die();
                            User::where('mobile_number',$request->mobile_number)->update(array(
                                'otp_verify'=>1,
                                'otp'=>null,
                                'device_type'=> $request->device_type,
                                'device_id'=> $request->device_id,
                                'device_token'=> $request->device_token
                            ));
                           $user_details          =   User::select('id','mobile_number')->where('mobile_number',$request->mobile_number)->first(); 

                            $refreshed                 =  $user_details->createToken('Qikmeds')->accessToken;
                            $response["code"]          =   200; 
                            $response["message"]       =   trans("Your mobile number has been verified.");
                            $response["data"]          =   $user_details;
                            $response['data']['token'] = $refreshed;
                            return response()->json($response,200);
                    }else{
                            $response["code"]         =   0; 
                            $response["message"]        =   trans("Invalid OTP.");
                            $response["data"]           =   (object)array();
                            return response()->json($response,203);
                    } 
                }else{
                    $response["code"]         =   0; 
                    $response["message"]        =   trans("verify type is wrong.");
                    $response["data"]           =   (object)array();
                    return response()->json($response,203);
                }       
            }else{
                $response['code'] = 1;
                $response['message'] = 'Invalid selection.Please try again.';
            }              
        }
        return response()->json($response, 201);
    }



    public function test_example(Request $request){
        // Account details
        $apiKey = urlencode('Mzg2ODMzNGY3MzM5NzkzODRkNDU1MDU1NTg3MTY3NDI=');
        // Message details
        $numbers = urlencode('919549837077'); //Mobile number on which you want to send message
        $sender = urlencode('600010');
        $message = rawurlencode('123456 is your One-Time Password, valid for 10 minutes only. Please do not share your OTP with anyone. Thank you for shopping at QikMeds.com');

        // Prepare data for POST request
        $data = 'apikey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        //echo $data; die();
        // Send the GET request with cURL
        $ch = curl_init('https://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        echo $response;

    }



    public function resend_otp(Request $request) {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'resend_type' => 'required'
        ]);
        if ($validator->fails()){
            $response = [
                'code' => 0,
                'error' => true,
                'message' => $validator->errors()->first(),
            ];
            
            return response()->json($response, 200);
        }
        /* 1=Register OTP, 2=Login OTP */
        if ($request->resend_type==1) {
            //echo "1"; die();
            $otp_details = DB::table('user_temp')->where('mobile_number', $request->mobile_number)->first();
            $otp = $otp_details->otp;
           //print_r($otp_details);die;
            if (isset($otp_details) && !empty($otp_details)) {
                if (isset($otp_details->otp) && !empty($otp_details->otp)) {

                  // $otp = rand(1000, 9999);
                 $otp = 123456;
                    DB::table('user_temp')->where('mobile_number',$request->mobile_number)->update(array('otp'=>$otp));
                    $dataotp = DB::table('user_temp')->select('mobile_number','otp')->where('mobile_number', $request->mobile_number)->first();
                    //return $dataotp;
                    $response_data['message'] = 'OTP sent.';
                    $response_data['code'] = 200;
                    $response_data['data'] =$dataotp;
                } else {
                    $response_data['code'] = 1;
                    $response_data['message'] = 'OTP not send.';
                }
            } else {
                $response_data['code'] = 1;
                $response_data['message'] = 'OTP not Match.';
            }
        } else if ($request->resend_type==2) {
                   //     echo "2"; die();
            $otp_details = DB::table('users')->where('mobile_number', $request->mobile_number)->first();
           // return $otp_details; die();
            if (isset($otp_details) && !empty($otp_details)) {
                    $otp = 123456;
                    User::where('mobile_number',$request->mobile_number)->update(array('otp'=>$otp));
                    $dataotp = User::where('mobile_number', $request->mobile_number)->first();
                    $response_data['message'] = 'OTP sent.';
                    $response_data['code'] = 200;
                    $response_data['data'] =$dataotp;

            } else {
                $response_data['code'] = 1;
                $response_data['message'] = 'OTP not Match.';
            }
        } else {
            $response_data['code'] = 1;
            $response_data['message'] = 'Invalid selection.';
        }
        
        $response_data['error'] = false;
        return response()->json($response_data, 200);
    }

     /**
     * details api
     * @return \Illuminate\Http\Response

     */

    public function details(){
         $user = $this->user->with(['SubscriptionPlanUser'])->where('id',Auth::guard('api')->user()->id)->first();
         return $this->showResponse($user,'User Profile info',200);
        
    }

      /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_profiles(Request $request){
        try {
            $user = Auth::guard('api')->user();
            $validator = Validator::make($request->all(), [
                'name'   =>  'sometimes|required',
                'email' => 'sometimes|email|unique:users,email,'.$user->id,
                //'email' => 'sometimes|email|unique:users,email,'.$user->id,
                //'phone_number' => 'sometimes|phone_number|unique:users,phone_number,'.$user->id,
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $userData  = $this->user->FindOrFail($user->id);
            $input = $request->all();
            $input['gender'] = $request->gender; 
            echo "<pre>"; print_r($input); die;
            $userData->update($input);



            $userData['token'] = $userData->createToken('Qikmeds')->accessToken;
            return $this->showResponse($userData,200,200);
        } catch (\Exception $e) {
            //return $e;die();
            return $this->unauthenticatedResponse();
        }

    }

      public function update_profile(Request $request){
        try {
            $user = Auth::guard('api')->user();
            $validator = Validator::make($request->all(), [
                'name'   =>  'sometimes|required',
                //'email' => 'required|unique:users,email',
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $userData  = $this->user->FindOrFail($user->id);
             User::where('id',$user->id)->update(array('name'=>$request->name,'email'=>$request->email,'dob'=>$request->dob,'gender'=>$request->gender));
             return $this->showResponse($userData,'Profile updated.',200);
        }catch(\Exception $e){
         return $e;
       }    
    }

      public function send_email(Request $request){
        try {
            $user = Auth::guard('api')->user();
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users,email',
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }  
          //  echo $user->id.'==='.$user->name; die();
            //$otp = rand(100000,999999);
            $otp = 123456;
            User::where('id',Auth::guard('api')->user()->id)->update(array('otp'=>$otp));
            $user = array(
             'user_id'=>$user->id,
             'name'=>$user->name,
             'otp'=>$otp
            );
            // Send the activation code through email */
            // $send =  Mail::send('emails.email-verification', compact('user'), function ($m) use ($user) {
            //   $m->to('amit.giri@brsoftech.org', $user['name']);
            //    $m->subject('Dear '. $user['name']. ' Confirm Your Email Address');
            // });
            $user_details = User::select('mobile_number','otp')->where('id',Auth::guard('api')->user()->id)->first();
            $response['code'] = 200;
            $response['data'] = $user_details;
            $response['data']['email'] = $request->email;
            $response['message'] = 'OTP sent.';
           return response()->json($response, 200);
           // return $this->showResponse($user,200,200);
        }catch(\Exception $e){
         return $e;
       }    
    }

 
      /*
       Verify otp Email =====================  
      */  
      public function verify_otp_email(Request $request) {
        $response = array();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'expire' => 'required',
            'otp' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        if($request->expire!=1){
                $response['code'] = 1;
                $response['message'] = 'OTP expired, Please try again !';
        }else{
          
               $verifyOtpUser  =User::where('id',Auth::guard('api')->user()->id)->first();    
             //  return $verifyOtpUser;
               if(!empty($verifyOtpUser)){
                    if ($verifyOtpUser->otp == $request->otp) {
                            User::where('id',Auth::guard('api')->user()->id)->update(array('is_email_verifed'=>1,'otp'=>null,'email'=>$request->email));
                            $user_details          =   User::where('email',$request->email)->first(); 
                            $response["code"]          =   200; 
                            $response["message"]       =   trans("Your email has been verified.");
                            $response["data"]          =   $user_details;
                            return response()->json($response,200);
                    }else{
                            $response["code"]         =   0; 
                            $response["message"]        =   trans("Invalid OTP.");
                            $response["data"]           =   (object)array();
                            return response()->json($response,203);
                    } 
                }else{
                    $response["code"]         =   0; 
                    $response["message"]        =   trans("Somthing went wrong..Please try again");
                    $response["data"]           =   (object)array();
                    return response()->json($response,203);
                }       
        }
        return response()->json($response,'', 201);
    }



      public function update_profile_image(Request $request){
        try {
            $user = Auth::guard('api')->user();
            $validator = Validator::make($request->all(), [
                'image'   =>  'required|image|mimes:jpeg,png,jpg',
            ]);
            if ($validator->fails()) {
                return $this->validationErrorResponse($validator);
            }
            $input = $request->all();
            if ($request->hasFile('image')) {
              $folder_name = 'user-profile';
              $input['image'] = $this->fileUpload($request->file('image'), false, $folder_name);
            }
            $image = $input['image'];
            User::where('id',$user->id)->update(array('image'=>$image));
            $userData  = $this->user->FindOrFail($user->id);
            return $this->showResponse($userData,'Profile image updateed.','200');
        }catch(\Exception $e){
         return $e;
       }    
    }


    public function change_password(Request $request)
    {
        $input = $request->all();
        $userid = Auth::guard('api')->user()->id;
       if($request->type==1){ 
        $rules = array(
            'type' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            $arr = array("code" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {

                if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                    $arr = array("code" => 400, "message" => "Your old password was entered incorrectly", "data" => array());
                } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                    $arr = array("status" => 400, "message" => "Please enter a password which is not similar then current password.", "data" => array());
                } else {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $userData  = $this->user->FindOrFail($userid);
                    $arr = array("code" => 200, "message" => "Password updated.", "data" => $userData);
                }


            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("code" => 400, "message" => $msg, "data" => array());
            }
        }
     }else{
        $rules = array(
            'type' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            $arr = array("code" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            try {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $userData  = $this->user->FindOrFail($userid);
                    $arr = array("code" => 200, "message" => "Password updated.", "data" => $userData);
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array("code" => 400, "message" => $msg, "data" => array());
            }
        } 
     }   
        return \Response::json($arr);
    }



     public function getDetailsByPinCode(Request $request){
        $validator = Validator::make($request->all(), [
            'pin_code'   =>  'required|digits:6',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        $pin_code = $request->get('pin_code');
       
        $geocode = file_get_contents("https://api.postalpincode.in/pincode/".$pin_code);
        $data_decode = json_decode($geocode,true);
        $name = array();
        $state = '';
        $city = '';
      if (isset($data_decode[0]['PostOffice']) && count($data_decode[0]['PostOffice']) > 0){
        foreach ($data_decode[0]['PostOffice'] as $key=>$value) {
          $name[] = $value['Name'];
          $state = $value['State'];
          $city = $value['District'];
        }
         $data['state'] = $state;
        $data['city'] = $city;
       return $this->showResponse($data,'',200);
       }else{
        $response = [
            'error' => false,
            'code' => 0,
            'message' => (!empty($message) ? $message : trans('No records found')),
        ];
                        return json_encode($response);

 
      }  
    }



public function forgotpassword(Request $request){
try{ 

    if($request->has('mobile_number')){
      $mobile_number = $request->mobile_number;
      $user = $this->user->where('mobile_number',$mobile_number)->where('mobile_number',$mobile_number)->first();
    }
    if(!empty($user)){
    $email = $user->email;
    $token =  $user->createToken('MyApp')->accessToken;
    $to_name = $user->name;;
    $to_email = $email;
    $data = array('name'=>$to_name, "tokan" =>  $token, "email" =>  $to_email);
        try{
            $user_password = rand(100000,999999);
            $password = bcrypt($user_password);
            $user->password = $password;
            $user->save();
            $info = ['name'=>$user->name,'user_password'=>$user_password];
            $response = Mail::send("emails.mail", $info, function($message) use ($to_name, $to_email) {
            $message->from("info@tet.com","I bazar Grocery Delivery");
            $message->to($to_email, $to_name)->subject("Reset Password Notification");
        });
        if(count(Mail::failures()) > 0){
            $errors = 'Failed to send password reset email, please try again for '.$email.'.';
        }
    }catch(\Exception $e){return $e;}
        $result['error'] =false;
        $result['code'] = 0;
        $result['data'] = $response;
        $result['message']= "Mail sent to your Email address ".$data['email'];
    }else{
        $result['error'] =true;
        $result['code'] = 1;
        $result['data'] = [];
        $result['message']= "This data is not in our records";    
    } 
    return response()->json($result);
  }catch(\Exception $e){
    return $e;
  }
}



  public function notification(request $request)
    {
         $a = [];
        $validator = Validator::make($request->all(), [
            'type'=>'required|in:alerts',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        $userId = Auth::guard('api')->user()->id;
        if($request->type == 'alerts'){
            $notifications = Auth::guard('api')->user()->notifications(['data'])->orderBy('created_at','DESC')->get();
            $notifications->transform(function($item, $key)
            {
                $data = $item->data;
                $data['title']= "Notification";
                $data['type']= "Order Notifications";
                $data['decription']= $data['message'];
                $data['date']= $item['created_at']->format('d-m-Y');
               $data['from_timestamp'] = date('h:i A',strtotime($item['created_at']));
                $item->data = $data;
                return $item->data;
            });
            $notifications = $notifications->toArray();
            //usort($notifications, array($this,'sortByNamea'));
            $merged = collect($notifications);
        }
        return $this->showResponse($merged);
    }

  public function admin_notification(request $request)
    {
         $a = [];
        $validator = Validator::make($request->all(), [
            'type'=>'required|in:notifications',
        ]);
        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }
        $userId = Auth::guard('api')->user()->id;
        if($request->type == 'notifications'){
               $adminNotifications = AdminNotification::select(['id','message_heading','image', 'message_url', 'message','created_at','link_type','link_url_type','link','cat_id','sub_cat_id','vendor_product_id'])->orderBy('id','DESC')->whereRaw('FIND_IN_SET('.$userId.', user_ids)')->get();
            /* $n = $notifications->collect();
            $an = $adminNotifications->collect();*/
            //$merged = $adminNotifications->merge($notifications);
         //   return $adminNotifications; die();
            $adminNotifications = $adminNotifications->transform(function($item, $key){
               
                $url= asset('storage/app/public/upload/Thumbnail/'.$item->image);
               
                $adata['id']= $item->id;
                $adata['title']= $item->message_heading;
                $adata['decription']=  $item->message;
                $adata['type']=  "Promotional Notifications";
                $adata['message_url']=  $item->message_url;
                $adata['date']= $item['created_at']->format('d-m-Y');
                $adata['from_timestamp'] = date('h:i A',strtotime($item['created_at']));
                $adata['image']= $url;
                return $adata;
            });
            $adminNotifications = $adminNotifications->toArray();
           // usort($adminNotifications, array($this,'sortByNamea'));
            $merged = collect($adminNotifications);
           //$merged =  usort($merged, array($this,'sortByName')) ;
        }
        return $this->showResponse($merged);
    }

 public function user_wallets(Request $request){
      try {
        $user = 80;
        $userData  = $this->userWallet->where('user_id',$user)->get();
           if(!empty($userData)){
                $response = [
                    'code' => 200,
                    'error' => false,
                    'wallet_balence' => 500,
                    'data' => $userData,
                ];
                return response()->json($response, 200);
            }else{
                $response = [
                    'code' => 0,
                    'error' => false,
                    'wallet_balence' => 0,
                    'message' => 'Not data yet.!',
                ];
                return response()->json($response, 200);
            }     
       }catch(\Exception $e){
          return $e;
       }    
    }
    


}
