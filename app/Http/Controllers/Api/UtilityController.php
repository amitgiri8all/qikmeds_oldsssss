<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Brand;
use App\Models\Page;
use App\Models\DeliveryAddress;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Faq;
use App\Models\FaqTopics;
use App\Models\Coupon;
use App\Models\SaveForLater;
use App\Models\SubscriptionPlan;
use App\Models\SubscriptionMeta;
use App\Models\Wishlist;
use App\Models\SubscriptionFaq;
use Illuminate\Support\Str;
use App\Models\Cart;
use App\Models\UserPrescription;
use App\Models\OfferBanner;
use App\Models\SiteSetting;
use App\Models\Socialmedia;
use App\Models\AppSetting;

use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
use Mail;

class UtilityController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;
    /*Here is call Construct Method*/
    function __construct(Request $request,User $user,DeliveryAddress $deliveryAddress,Brand $brand,Blog $blog,Category $category,Banner $banner,Faq $faq,FaqTopics $faqTopics,Coupon $coupon,Cart $cart,UserPrescription $userPrescription,OfferBanner $offerBanner){
        $this->method           =$request->method();
        $this->user             = $user;
        $this->brand            = $brand;
        $this->blog             = $blog;
        $this->deliveryAddress  = $deliveryAddress;
        $this->banner           = $banner;
        $this->category         = $category;
        $this->faq              = $faq;
        $this->faqTopics        = $faqTopics;
        $this->coupon           = $coupon;
        $this->cart             = $cart;
        $this->userPrescription = $userPrescription;
        $this->offerBanner      = $offerBanner;
        $this->userId           = Auth::guard('api')->user()?Auth::guard('api')->user()->id:null;

    }

    /*Get All Brand List ===========*/
    public function getAllBrand(Request $request){
      try {
        $data = $this->brand->where('status',1)->orderBy('updated_at','desc')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Blog Info');  
    }
    /*Get All Blog List ===========*/
    public function getAllBlog(Request $request){
      try {
        $data = $this->blog->where('status',1)->orderBy('id','desc')->limit(5)->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data);  
    }
    /*Get All Blog Details ===========*/
    public function getBlogDetails(Request $request,$id){
      try {
        $data = $this->blog->where('status',1)->where('id',$id)->first();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Blog Info');  
    }
    /*Get All Category List ===========*/
    public function getAllCategory(Request $request){
      try {
        $data = Category::select('category_name','id','slug','image')
        ->limit(10)
        ->orderBy('id','ASC')
        ->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data);  
    }

    /*Get All Banner List ===========*/
    public function getAllBanner(Request $request){
      try {
        $type = $request->type;
        $data = $this->banner->where('banner_type',$type)->where('status','!=','0')->orderBy('updated_at','asc')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data);  
    }

    /*Get All FAQ List ===========*/
    public function getAllFaq(Request $request){
      try {
        $data = $this->faqTopics->with(['faqData'])->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Faq Info');  
    }

    /*Get All Coupon List ===========*/
    public function getAllCoupon(Request $request){
      try {
        $data = $this->coupon->where('status','1')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Coupon Info');  
    } 
    
    /*Get All Coupon List ===========*/
    public function getAllOffer(Request $request){
      try {
        $data = $this->offerBanner->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Offer Info');  
    } 


    /* ==== Insert save for later info */
    public  function save_for_later(Request $request){
     /// dd($request->all())
        $validator = Validator::make($request->all() , ['product_id' => 'required']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         };
      try { 
        $count=SaveForLater::where('user_id',$this->userId)->where('product_id',$request->get('product_id'))->count();
       // echo $count; die;
        if($count>0){
           // echo "asdfasd"; die();
                $SaveForLater = SaveForLater::where('user_id',$this->userId)->where('product_id',$request->get('product_id'));
                $SaveForLater->forceDelete();
                 return $this->showResponse($SaveForLater,'Product added for save for later');  
               
         }else{
        //    echo "ok fine"; die();
                //DB::table('cart')->where('user_id',$this->userId)->where('product_id',$request->get('product_id'))->delete();
                $cart = Cart::where('user_id',$this->userId)->where('product_id',$request->get('product_id'));
                $cart->forceDelete();   
                //Save for later
                $save = new SaveForLater();
                $save->user_id = $this->userId;
                $save->product_id = $request->get('product_id');
                $status = $save->save();
                return $this->showResponse($status,'Product added!');  
            } 
         } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
           
    }

  /*========  List save for later ====*/
   public function save_for_later_list()
    {
      try {        
        $product = SaveForLater::where(['user_id'=>$this->userId])
         ->with(['Product','ProductImage'])
         ->get();
         foreach ($product as $key=>$Rec) {
            if(!empty($Rec['ProductImage'])){
             $product[$key]['Product']['image']=$Rec['ProductImage']['image'];
            }else{
             $product[$key]['Product']['image'] = @$Rec['ProductImage']['image'];
            }
         } 
          return $this->showResponse($product,'Save For Later Info');  
        } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }
     } 

 /*========  Remove save for later ====*/
   public function save_for_later_delete(Request $request)
    {
        try {        
          $status =SaveForLater::where('id',$request->id)->where('user_id',$this->userId)->delete();  
          return $this->deletedResponse('Removed Save For Later Product');
        } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }
     }



     /*========================= Wish List =========================*/
     /*===Add To wish list===*/
      public  function add_wishlist(Request $request){

         $validator = Validator::make($request->all() , ['product_id' => 'required']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
        $count=Wishlist::where('user_id',$this->userId)->where('product_id',$request->get('product_id'))->count();
        if($count>0){
                $Wishlist = Wishlist::where('user_id',$this->userId)->where('product_id',$request->get('product_id'));
                $Wishlist->forceDelete();
               return $this->showResponse($Wishlist,'Removed  wishlist');
            }
            else
            {
                $save = new Wishlist();
                $save->user_id = $this->userId;
                $save->product_id = $request->get('product_id');
                $status = $save->save();
                return $this->showResponse($status,'Added  wishlist');  
            } 
    }//wishlist_list


     /*========  List Wish List ====*/
   public function wishlist_list()
    {
        try {        
        $data = Wishlist::where(['user_id'=>$this->userId])
         ->with(['Product','ProductImage'])
         ->get();
         foreach ($data as $key=>$Rec) {
            if(!empty($Rec['ProductImage'])){
             $data[$key]['Product']['image']=$Rec['ProductImage']['image'];
            }else{
             $data[$key]['Product']['image'] = @$Rec['ProductImage']['image'];
            }
         }   
          return $this->showResponse($data,'Wish List');  
        } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }
     } 

     /*========  Remove wish list ====*/
   public function wishlist_delete(Request $request)
   {
        try {        
          $status =Wishlist::where('id',$request->id)->where('user_id',$this->userId)->delete();  
          return $this->deletedResponse('Remove wishlist');
        } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }
     }

 /*========  Remove wish list ====*/
   public function move_to_cart(Request $request)
   {
         $validator = Validator::make($request->all() , ['product_id' => 'required']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
        try {        
          $count = SaveForLater::where('product_id',$request->product_id)->where('user_id',$this->userId)->count();
          if($count>0){ 
               $status =SaveForLater::where('product_id',$request->product_id)->where('user_id',$this->userId)->delete();  
               /*===== Product Save to cart ====*/
               $save = new Cart();
               $save->user_id = $this->userId;
               $save->product_id = $request->product_id;
               $save->qty = 1;
               $save->save();
                $response = [
                 'code' => 200,
                 'error' => false,
                 'message' => 'Product move to cart',
                ];
                
          }else{
                $response = [
                 'code' => 200,
                 'error' => false,
                 'message' => 'not Found data',
                ];
          }
          return json_encode($response);
        } catch (\Exception $e) {
              Session::flash('error',$e->getMessage());
               DB::rollBack();
        }
     }


     /*========Offer Just For You*/

     public function offer_just_for_you_slider_image(Request $request){
        try {
        $data = $this->offerBanner->where('status','1')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data); 
     }


 
    public function getAllPrescription(Request $request){
      try {
        $data = $this->userPrescription->where('user_id',$this->userId)->first();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Prescription Info');  
    } 

    // Subscription Plan
    public function subscription_plan(){
        try {
        $data =SubscriptionPlan::where('plan_type','user')->where('status','!=','0')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Subscription Plan Info');  
    }
    // Subscription meta 
    public function subscription_meta(){
        try {
        $data =SubscriptionMeta::where('type','user')->where('status','!=','0')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data);  
    }
    // Subscription faq 
    public function subscription_faq(){
        try {
        $data =SubscriptionFaq::where('user_type','user')->where('status','!=','0')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Subscription Faq');  
    } 

    // Legal Info 
    public function legal_info(){
        try {
        $data =Page::where('slug','terms-and-conditions')->where('status','!=','0')->first();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Subscription Faq');  
    }


    /*============== Globa Settings ================*/
    public function globalSetting()
    {
        $data  = DB::table('site_settings')->select('dont_have_prescription','phone','whats_up')->where('id','1')->first();
         return $this->listResponse($data);
    }




 








}
