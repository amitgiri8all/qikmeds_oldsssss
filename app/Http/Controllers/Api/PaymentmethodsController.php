<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\PatientInfo;
use App\Models\PaymentMethod;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
class PaymentmethodsController extends Controller
{
    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;

    /*Here is call Construct Method*/
    function __construct(Request $request,User $user,PatientInfo $patientInfo,PaymentMethod $paymentMethod){
        $this->method           =$request->method();
        $this->user             =$user;
        $this->paymentMethod    =$paymentMethod;
        $this->patientInfo      =$patientInfo;
        $this->userId           =Auth::guard('api')->user()?Auth::guard('api')->user()->id:null;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try {
        $data = $this->paymentMethod->where('user_id','=',$this->userId)->orderBy('updated_at','desc')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($data,'Payment Method');
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();
        $validator = Validator::make($request->all() ,[
                'card_number' => ['required',new CardNumber],
                'expiration_year' => ['required', new CardExpirationYear($request->get('expiration_month'))],
                'expiration_month' => ['required', new CardExpirationMonth($request->get('expiration_year'))],
                'cvc' => ['required', new CardCvc($request->get('card_number'))]
            ]);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
         try {    
             $input['user_id'] = $this->userId;    
             $data =   $this->paymentMethod->create($input);
             return $this->showResponse($data,'Payment method successfully added');

         } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        try {
            $del = $this->paymentMethod->where('id',$id)->where('user_id',$this->userId)->firstOrFail();
            $del->delete();
            return $this->deletedResponse('Deleted your patient info successfully');
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
    }
}
