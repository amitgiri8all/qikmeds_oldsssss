<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserTemp;
use App\Models\Brand;
use App\Models\DeliveryAddress;
use App\Models\Manufacturer;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductFaq;
use Illuminate\Support\Str;
use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use App\Helpers\ProductHelper;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
use Mail;
class ProductController extends Controller
{

    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;
    /*Here is call Construct Method*/
    function __construct(Request $request,User $user,DeliveryAddress $deliveryAddress,Brand $brand,Blog $blog,Category $category,Banner $banner,Product $product){
        $this->method           =$request->method();
        $this->user             = $user;
        $this->brand            = $brand;
        $this->blog             = $blog;
        $this->deliveryAddress  = $deliveryAddress;
        $this->banner           = $banner;
        $this->category         = $category;
        $this->product          = $product;
        $this->userId           = Auth::guard('api')->user()?Auth::guard('api')->user()->id:null;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home_product_list(Request $request){
        /*
        1.Deal of the day
        2.Health Product
        3.Most Selling
        */
        $type = $request->type;
        try {  
         $data = ProductHelper::ProductListByType($type);
          if($type==1){
            $date = date('Y-m-d 12:00');
            $newdate = date("Y-m-d 12:00",strtotime ( '+1 day' , strtotime ( $date ) )) ;
            $yourdate = $newdate;
            $stamp = strtotime($yourdate); // get unix timestamp
            $time_in_ms = $stamp*1000;
          }else{
            $time_in_ms = 00;
          }  
          if(!empty($data)){
                $response = [
                    'code' => 200,
                    'error' => false,
                    'deal_date' => $time_in_ms,
                    'type' => $type,
                    'data' => $data,
                ];
                return response()->json($response, 200);
            }else{
                $response = [
                    'code' => 0,
                    'error' => false,
                    'remaining_time' => 0,
                    'message' => 'Not data yet.!',
                ];
                return response()->json($response, 200);
            }   


        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
    }
 
    public function product_list(Request $request)
    {
	    //echo "<pre>"; print_r($request->all()); die();
     // try {
		/*
		Sort. number formart
		Sort = o not filter
		sort = 1  Popularity
		sort = 2  low to hight
		sort = 3  hight to low
		sort = 4  Discount         
		*/
            
            $category_id = $request->get('category_id');
            $brand_id = $request->get('brand_id');
            $sort = $request->get('sort');

            $query = Product::select('products.*','categories.category_name','manufacturer.manufacturer_name','product_image.image')
            ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
             ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
            ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
            ->Join('product_image', function($join)
            {
                  $join->on('product_image.product_id', '=', 'products.p_id');
            });
            
            if(!empty($category_id>0)){
             $query->where('categories.id',$category_id);
            }
            // Brand id
            if(!empty($brand_id>0)){
             $query->where('brand.id',$brand_id);
            }
            
			//Popularity     
            if($sort==1){
                $query->orderBy('products.id','DESC');
            }

            //low to hight
            if($sort==2){

                $query->orderBy('products.sale_price','ASC');
            }

            //hight to low
            if($sort==3){
                $query->orderBy('products.sale_price','DESC');
            }

            //Discount
            if($sort==4){
                
                $query->where('products.discount','!=','');
            }
            $query->groupBy('product_image.pro_img_id');

            $data=$query->paginate(10);
/*
            foreach($data as $key=>$value){
                   $data[$key]['mrp'] = number_format((float)$value->mrp, 2, '.', '');
                   $data[$key]['sale_price'] = number_format((float)$value->sale_price, 2, '.', '');
                   $data[$key]['discount'] = number_format((float)$value->discount, 2, '.', '');
            }*/
        

/*
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
*/
        return $this->showResponse($data);  

    }

    /**
     * Show the form for product details a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function filterRange(Request $request){

        $category_id = $request->category_id;
        $max_price = Product::join('product_category',function($query) use($category_id){
            $query->on('products.id','=','product_category.product_id');
            $query->where('product_category.category_id',$category_id);
        })->max('products.mrp');

        $price_range_array = [];
        $price_range_array[0]['min'] = 0;
        $price_range_array[0]['max'] = round($max_price/4);
        $price_range_array[1]['min'] = round(($max_price/2) / 2);
        $price_range_array[1]['max'] = round($max_price/2);
        $price_range_array[2]['min'] = round($max_price/2);
        $price_range_array[2]['max'] = round($max_price-($max_price/4));
        $price_range_array[3]['min'] = round($max_price-($max_price/4));
        $price_range_array[3]['max'] = round($max_price);

        $coupon_discount = [];
        foreach (range(0,9) as $key => $value) {
            if ($key == 0) {
               $coupon_discount[] = ['min'=> (string) 0 ,'max' => (string) 10];
            }else{
                $coupon_discount[] = ['min'=>$key.'0','max' =>  ++$value.'0'];
            }
        }



        $data['price_range_filter'] = $price_range_array;
        $data['coupon_discount'] = $coupon_discount;

        return $this->showResponse($data); 
    }

    public function product_details(Request $request)
   {
    $product_id = $request->get('product_id');
      try {
  

$query = Product::select('products.*','product_image.image','categories.category_name','manufacturer.manufacturer_name')
         ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
         ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->leftJoin('product_image', function($join)
        {
            $join->on('product_image.product_id', '=', 'products.p_id');
            $join->limit(1);
        })
        ->where('products.id',$product_id)
        ->groupBy('product_image.pro_img_id');

        $productDetail=$query->first();

         $query = Product::select('products.*','product_image.image','product_image.caption','manufacturer.manufacturer_name','categories.id')
         ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
         ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
          ->leftJoin('manufacturer','manufacturer.id','=','products.manufacturer_id')
         ->leftJoin('product_image', function($join)
        {
          $join->on('product_image.prod_id', '=', 'products.id');
        })
        ->where('products.status',1)
        ->where('categories.id',$productDetail->id)
        ->where('products.id','!=',$product_id)
        ->limit(5)
        ->groupBy('product_image.product_id');
        $related_products=$query->get();

        $productDetail['related_products'] = $related_products;
        $faq = ProductFaq::where('product_id',$product_id)->get();

        $productDetail['share_url'] = url('/product-details/'.$productDetail->slug);

        } catch (\Exception $e) {
          return $this->clientErrorResponse($e);
        }
        return $this->showResponse($productDetail,'Products Details...');  

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search_product_name(Request $request)
    {
            $search  =  $request->search;
            $category_id  =  $request->category_id;
          $query = Product::select('products.*','product_image.image','product_image.caption','product_category.category_id','products.discount','products.status')
                ->leftJoin('product_category', 'product_category.product_id', '=', 'products.id')
                ->leftJoin('categories', 'categories.id', '=', 'product_category.category_id')
                ->leftJoin('product_image', function($join)
                {
                  $join->on('product_image.product_id', '=', 'products.p_id');
                })
                ->orWhere('products.medicine_name','LIKE',"%{$search}%")->orWhere('product_category.category_id',$category_id)
                ->where('products.status',1)->limit(5)
                 ->groupBy('product_image.product_id');
                $posts=$query->get();
                //echo "<pre>"; print_r($posts); die();
     
            if(!$posts->isEmpty())
            {
                $data=[];    
                foreach($posts as $post){
                   $data[] = $post;
                }
          
        return $this->showResponse($data); 
          }else{
            $response = [
              'code' => 0,
              'error' => true,
              'message' => 'No Data not found',
            ];
            return response()->json($response, 404);
          } 
   }
   
//    Filter data
	public function filter_data(Request $request){

		 $data['availability'] = ['Exclude out of stock']; 
		 $data['manufacturers'] = Manufacturer::select('id','manufacturer_name')->where('status',1)->get();
		 $data['brand'] = Brand::select('id','brand_name')->where('status',1)->get();
		 $data['price'] = ['0-500','500-1000','10000-1500','1500-2000','2000-2500','2500-3000','3000-5000']; 
		 $data['discount'] = ['0-20','20-40','40-60','60-80','80-100'];
		 //$discount = ['0-20','20-40','40-60','60-80','80-100'];
		 //$data = ['id'=>1,'name'=>'Price Range','data'=>$discount];
        return $this->showResponse($data,'filter data'); 
	}

}
