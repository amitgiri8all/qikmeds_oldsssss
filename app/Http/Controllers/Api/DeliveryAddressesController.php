<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ResponceTrait;
use App\Traits\ImageTrait;
use App\Traits\HelperTrait;
use App\Models\User;
use App\Models\DeliveryAddress;
use Validator;
use Paginator;
use Collection;
use Auth;
use Storage;
use Image;
use Carbon\Carbon;
use Hash;
use App\Helpers\Helper;
use DateTime;
use DateInterval;
use DB;
use App;
use Session;
class DeliveryAddressesController extends Controller
{

    use ResponceTrait;
    use ImageTrait;
    use HelperTrait;

    /*Here is call Construct Method*/
    function __construct(Request $request,User $user,DeliveryAddress $deliveryAddress){
        $this->method           =$request->method();
        $this->user             = $user;
        $this->deliveryAddress = $deliveryAddress;
        $this->userId           = Auth::guard('api')->user()?Auth::guard('api')->user()->id:null;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try {
        $deliveryAddress = $this->deliveryAddress->where('user_id','=',$this->userId)->orderBy('updated_at','desc')->get();
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
        return $this->showResponse($deliveryAddress);
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();
         $validator = Validator::make($request->all() , ['name' => 'required', 'mobile_number' => 'required|digits:10', 'pin_code' => 'required|string|digits:6', 'address_delivery' => 'required','address_type' => 'required|in:work,home,other','latitute'=>'required','longitute'=>'required']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
         try {    
             $input['user_id'] = $this->userId;    
             $data =   $this->deliveryAddress->create($input);
             return $this->showResponse($data,'Delivery address added');

         } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
   {
         $validator = Validator::make($request->all() , ['name' => 'required', 'mobile_number' => 'required', 'pin_code' => 'required|string|digits:6', 'address_delivery' => 'required','address_type' => 'required|in:work,home,other']);
         if ($validator->fails()) {
              return $this->validationErrorResponse($validator);
         }
         try {    
                $deliveryAddress =  $this->deliveryAddress->findOrFail($id);
                $input = $request->all();
                $deliveryAddress->fill($input)->save();
                return $this->showResponse($deliveryAddress,'Delivery address Updated');
         } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        try {
            $del = $this->deliveryAddress->where('id',$id)->where('user_id',$this->userId)->firstOrFail();
            $del->delete();
            return $this->deletedResponse('Delivery address deleted');
        } catch (\Exception $e) {
            return $this->clientErrorResponse($e);
        }
    }

      /*Update Default Patent Info*/

     public function update_default_deliveryAddress(Request $request)
   {
         $validator = Validator::make($request->all() ,['id' => 'required']);
         if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
         }
         try {    
                DeliveryAddress::where('user_id',$this->userId)->update(['status'=>0]);
               $deliveryAddress=DeliveryAddress::where('id',$request->id)->update(['status'=>1]);

                return $this->showResponse($deliveryAddress,'Updated  default delivery address.');
         } catch (\Exception $e) {
         return $this->clientErrorResponse($e);
        }
    }
}
