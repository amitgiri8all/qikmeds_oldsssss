<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'app_name' => 'required',
            'app_env' => 'required',
            'app_debug' => 'required',
            'app_log_level' => 'required',
            'app_url' => 'required||url',
            'mail_driver' => 'required',
            'mail_host' => 'required',
            'mail_username' => 'required',
            'mail_password' => 'required',
            'mail_encryption' => 'required',
            'mail_from_address' => 'required',
            'mail_from_name' => 'required',
            'app_url_android' => 'required|url',
            'app_url_ios' => 'required|url',
            'under_maintenance' => 'required',
            'timezone' => 'required',
            'pagination_limit' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'currency' => 'required',
            'address' => 'required',
        ];
    }
}
