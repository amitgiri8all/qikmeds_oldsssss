<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required',
            'whats_up' => 'required',
            'facebook' => 'required||url',
            'twitter' => 'required|url',
            'instagram' => 'required||url',
            'linkedin' => 'required|url',
            'youtube' => 'required||url',
          ];
    }
}
