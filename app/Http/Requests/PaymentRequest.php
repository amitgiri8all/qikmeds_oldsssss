<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'active_payment_page' => 'required',
            'cash_on_delivery' => 'required',
            'wallet' => 'required',
            'credit_card' => 'required',
            'paypal' => 'required',
            'stripe_secret_key' => 'required',
            'stripe_public_key' => 'required',
            'paypal_account_email' => 'required||email',
            'paypal_currency' => 'required',
        ];
    }
}
