<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*public function handle(Request $request, Closure $next)
    {
        if($request->user()->role=='user'){
            return $next($request);
        }
        else{
            return redirect('/home');
        }
    }*/

    public function handle($request, Closure $next, $guard = null)
    {
        $auth=Auth::guard('web');
        if (!$auth->check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
