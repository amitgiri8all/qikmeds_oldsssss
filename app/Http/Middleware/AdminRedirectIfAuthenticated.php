<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminRedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
          if ($guard == 'admin' && Auth::guard($guard)->check())
        {
           /*  $currentroute = \Request::route()->getName();
            if (!Permission::checkPermission($currentroute) && Auth::guard($guard)->user()->id != 1)
            {
                echo "<center><h1>Not a permission..</h1></center>";
                die;
            }*/
        }

        return $next($request);
    }
}
