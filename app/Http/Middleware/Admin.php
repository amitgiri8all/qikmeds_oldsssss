<?php
namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Permission;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
 
    public function handle($request, Closure $next, $guard = 'admin')
    {
           //  echo 'Guard name Add==>'.$guard; die();
        if ($guard == 'admin' && Auth::guard($guard)->check())
        {
              /*$currentroute = \Request::route()->getName();
              //echo $currentroute; die();
            if (!Permission::checkPermission($currentroute) && Auth::guard($guard)->user()->id != 1)
            {
                echo "<center><h1>Not a permission..</h1></center>";
                die;
            }else{
                 return redirect('/admin');
            }*/
        }

        return $next($request);
    }
}

