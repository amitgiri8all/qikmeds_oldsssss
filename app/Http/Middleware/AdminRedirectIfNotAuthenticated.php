<?php

namespace App\Http\Middleware;

use Closure;
 use Auth;
use App\Models\Permission;
class AdminRedirectIfNotAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        if (!Auth::guard('admin')->check()) {
            return redirect('/admin');
        }else{
            /*$currentroute = \Request::route()->getName();
            if (!Permission::checkPermission($currentroute) && Auth::guard($guard)->user()->id == 1)
            {
                echo "<center><h1>Not a permission..</h1></center>";
                die;
            }*/
                    return $next($request);

        }    
    }
}
