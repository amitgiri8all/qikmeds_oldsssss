function increaseValue(product_id) {
  var value = parseInt($("#number"+product_id).val(), 10);
  value = isNaN(value) ? 0 : value;
  value++;
  $('#number'+product_id).val(value);
}

function decreaseValue(product_id) {
  var value = parseInt($('#number'+product_id).val(), 10);
  if(value=='1'){
    location.reload();
  }
  value = isNaN(value) ? 0 : value;
  value < 1 ? value = 1 : '';
  value--;
  $('#number'+product_id).val(value);
 
}


function decreaseValues(product_id) {
  var value = parseInt($('#number'+product_id).val(), 10);
   if(value=='1'){
  }
  value = isNaN(value) ? 0 : value;
  value < 1 ? value = 1 : '';
  value--;
  $('#number'+product_id).val(value);  
}
        
 $(document).on('click', '.addToCart', function() {
        product_id = $(this).attr("data-id");
        $(".btnCart"+product_id).css("display", "none");
      //  $("#number"+product_id).css("display", "block");
         event.preventDefault();
          $.ajax({
            url: addtocart_route,
            type: "POST",
            data: {'_token': token,product_id:product_id},
            dataType: 'json',
            success: function(response) {
                    $("#cartcount").load(document.URL + " #cartcount");
                    $("#cartdel").load(document.URL + " #cartdel");
                    $("#total_cart_value").load(document.URL + " #total_cart_value");
                    $("#tot").load(document.URL + " #tot");
                    $("#total_sav").load(document.URL + " #total_sav");
                    $("#sum_total_amount").load(document.URL + " #sum_total_amount");
                    $("#charge").load(document.URL + " #charge");
                    $("#free").load(document.URL + " #free");
                    $("#ref"+product_id).load(document.URL + " #ref"+product_id);
                    toastr[response.status]("Product successfully Add in your cart", "Cart");
             }
            });
}); 


//$('#add_to_cart').submit(function( event ) {
$(document).on('click', '.update_cart', function() {

           product_id= $(this).attr("data-id");
         var quantity =$('#number'+product_id).val();

         event.preventDefault();
          $.ajax({
            url: update_cart,
            type: "POST",
            data: {'_token': token,product_id:product_id,quantity:quantity},
            dataType: 'json',
            success: function(response) {
                      $("#total_cart_value").load(document.URL + " #total_cart_value");
                      $("#tot").load(document.URL + " #tot");
                      $("#total_sav").load(document.URL + " #total_sav");
                    $("#sum_total_amount").load(document.URL + " #sum_total_amount");
                    $("#charge").load(document.URL + " #charge");
                    $("#free").load(document.URL + " #free");

                    toastr[response.status]("Updated in your cart", "Cart");
            }
            });
});


 $(document).on('click', '.clear_cart', function() {
           event.preventDefault();
          $.ajax({
            url: clear_cart,
            type: "POST",
            data: {'_token': token},
            dataType: 'json',
            success: function(response) {
                    toastr[response.status]("Cart Clar successfully", "Cart");
                    window.location.reload();
            }
            });
});


 /*===============*/
$(document).on('click', '.update_address', function() {
            selected_id= $(this).attr("data-id");
         event.preventDefault();
          $.ajax({
            url:  select_address,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
               // toastr[response.status]("Updated in your cart", "Cart");
                // $("#select_address").load(document.URL + " #select_address");
                  window.location.reload();
            }
            });
 });

$(document).on('click', '.not_login', function() {
 //alert('ok')
    toastr['warning']("Please Login first !", "Login");
     window.setTimeout( function(){
                 window.location = redirect_url_login;
     }, 3000 );
});


 $(document).on('click', '.promo_code', function() {
     var coupon_id =  $(this).find('.coupon_id').val();
     $("#r-"+coupon_id).attr('checked', 'checked');

     $(this).is(':checked');
      //alert(coupon_id);
          event.preventDefault();
          $.ajax({
            url: promocode_calculation,
            type: "POST",
            data: {'_token': token,coupon_id:coupon_id},
            dataType: 'json',
            success: function(response) {
               // alert(response.total_saving)
                $('#promo_dis').text(response.promo_val);
                $('.total_saving').text(response.total_saving);
                $('#sum_total_amount').text(response.total_amt);
                toastr[response.status]("Promo Code Apply successfully..!!", "Promo");
                $('input[name=radio]:checked', '.promo_code').val()
            }
            });
});


 $(document).on('click', '.save_for_later', function() {
     var product_id =  $(this).attr("data-id");
        // alert(product_id)
          event.preventDefault();   
          $.ajax({
            url: save_for_later,
            type: "POST",
            data: {'_token': token,product_id:product_id},
            dataType: 'json',
            success: function(response) {
                toastr[response.status](response.message, "Save For Later");
                $("#cartdel").load(document.URL + " #cartdel");
               }    
            });
});


/*=========Remove Form remove_plan*/
 $(document).on('click', '.remove_plan', function() {
         plan_id = $(this).attr("data-id");
          event.preventDefault();
          $.ajax({
            url: remove_plan,
            type: "POST",
            data: {'_token': token,plan_id:plan_id},
            dataType: 'json',
            success: function(response) {
                    $("#cartcount").load(document.URL + " #cartcount");
                    $("#total_cart_value").load(document.URL + " #total_cart_value");
                    $("#tot").load(document.URL + " #tot");

                    $("#cartdel").load(document.URL + " #cartdel");
                    $("#total_sav").load(document.URL + " #total_sav");
                    $("#sum_total_amount").load(document.URL + " #sum_total_amount");
                    $("#charge").load(document.URL + " #charge");
                    $("#free").load(document.URL + " #free");

                    toastr[response.status]("Plan removed successfully", "Cart");
            }
            });
});




/*=========Remove Form Car*/
 $(document).on('click', '.remove-from-cart', function() {
         id = $(this).attr("data-id");
          event.preventDefault();
          $.ajax({
            url: remove_cart,
            type: "POST",
            data: {'_token': token,id:id},
            dataType: 'json',
            success: function(response) {
                      $("#cartcount").load(document.URL + " #cartcount");
                 $("#total_cart_value").load(document.URL + " #total_cart_value");
                    $("#tot").load(document.URL + " #tot");

                    $("#cartdel").load(document.URL + " #cartdel");
        $("#total_sav").load(document.URL + " #total_sav");
        $("#sum_total_amount").load(document.URL + " #sum_total_amount");
        $("#charge").load(document.URL + " #charge");
        $("#free").load(document.URL + " #free");

                    toastr[response.status]("Product removed successfully", "Cart");
            }
            });
});

/*========== Add To Wish List JS=========*/
$(document).on('click', '.add_to_wishlist', function() {
            event.preventDefault();
                product_id = $(this).attr("data-id");
           $.ajax({
            url: wish_list,
            type: "POST",
            data: {'_token': token,product_id:product_id},
            dataType: 'json',
            success: function(response) {
             toastr[response.status](response.message, "Wish List");
               $("#heart"+product_id).load(document.URL + " #heart"+product_id);

             }
            });
});


  


