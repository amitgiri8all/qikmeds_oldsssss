$('.single-item').slick({
  infinite: true,
  speed: 800,
  infinite: true,
  autoplay: true,
  fade: true,
  prevArrow: $('.prev'),
  nextArrow: $('.next'),
  dots: false,
  arrows: true,
  cssEase: 'linear'
});

$('.autoplay').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  arrows: true,
  autoplay: true,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.autoplay-1').slick({
  slidesToShow: 7,
  slidesToScroll: 1,
  arrows: true,
  autoplay: false,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});

$('.autoplay-2').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  autoplay: false,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev wht'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next wht'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.autoplay-3').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  arrows: true,
  autoplay: true,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.autoplay-4').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  arrows: true,
  autoplay: false,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev wht'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next wht'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.autoplay-5').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  autoplay: false,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.autoplay-6').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  autoplay: true,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


$('.autoplay-7').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  autoplay: true,
  infinite: true,
  prevArrow: "<button type='button' class='slick-prev'><i class='las la-arrow-left'></i></button>",
  nextArrow: "<button type='button' class='slick-next'><i class='las la-arrow-right'></i></button>",
  speed:1000,
  useTransform: true,
  autoplaySpeed: 2200,
  responsive: [
    {
      breakpoint: 1140,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1

      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

 $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  useTransform: true,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  useTransform: true,
  dots: false,
  arrows: true,
  prevArrow: "<button type='button' class='slick-prev'><i class='ti ti-angle-up'></i></button>",
  nextArrow: "<button type='button' class='slick-next'><i class='ti ti-angle-down'></i></button>",
  vertical:true,
  focusOnSelect: true
});

jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function() {
  var spinner = jQuery(this),
	input = spinner.find('input[type="number"]'),
	btnUp = spinner.find('.quantity-up'),
	btnDown = spinner.find('.quantity-down'),
	min = input.attr('min'),
	max = input.attr('max');

  btnUp.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue >= max) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue + 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

  btnDown.click(function() {
	var oldValue = parseFloat(input.val());
	if (oldValue <= min) {
	  var newVal = oldValue;
	} else {
	  var newVal = oldValue - 1;
	}
	spinner.find("input").val(newVal);
	spinner.find("input").trigger("change");
  });

});


/* (function($){
  $(window).on("load",function(){
    $(".content").mCustomScrollbar();
  });
})(jQuery); */

/*$(document).ready(function(){
	// Add minus icon for collapse element which is open by default
	$(".collapse.show").each(function(){
		$(this).prev(".card-header").find(".ti").addClass("ti-angle-down").removeClass("ti-angle-right");
	});

	// Toggle plus minus icon on show hide of collapse element
	$(".collapse").on('show.bs.collapse', function(){
		$(this).prev(".card-header").find(".ti").removeClass("ti-angle-right").addClass("ti-angle-down");
	}).on('hide.bs.collapse', function(){
		$(this).prev(".card-header").find(".ti").removeClass("ti-angle-down").addClass("ti-angle-right");
	});
});

$('.mobile-filter').click(function() {
  $('.mobile-category-show').slideToggle(500);
});



// Set the date we're counting down to
var countDownDate = new Date("September 1, 2021 23:59:59").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
  window.onload = function what(){

  document.getElementById("demo").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
};
  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
*/
// custom select

function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.listing li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}

DropDown.prototype = {
    initEvents: function () {
        var obj = this;
        obj.dd.on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).toggleClass('active');
        });
        obj.opts.on('click', function () {
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
            opt.siblings().removeClass('selected');
            opt.filter(':contains("' + obj.val + '")').addClass('selected');
        }).change();
    },
    getValue: function () {
        return this.val;
    },
    getIndex: function () {
        return this.index;
    }
};

$(function () {
    // create new variable for each menu
    var dd1 = new DropDown($('#select-box'));
    // var dd2 = new DropDown($('#other-gases'));
    $(document).click(function () {
        // close menu on document click
        $('.custom-slct').removeClass('active');
    });
});


/*$(document).ready(function() {
        //Horizontal Tab
        $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

		//Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
*/
/*$(function () {
	$('.input-number').customNumber();
});*/


$( function(){
	$('.js-enhance').on('click', function () {
		console.log('enhance');
		$( document ).trigger( "enhance" );
	});

	$( document ).trigger( "enhance" );
});
