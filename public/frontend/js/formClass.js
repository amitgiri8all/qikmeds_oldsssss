$(document).ready(function () {
//$(".ajax_form").submit(function(event){
    $(document).on("submit", ".ajax_form", function (event) {	
		 $(':input[type="submit"]').prop('disabled', true);
        var posturl = $(this).attr('action');
        var callbackFunction = $(this).attr('data-callback_function');
        if (callbackFunction) {
            if (callbackForm() == false) {
                return false;
            }
        }
        var formid = '#' + $(this).attr('id');
        $(this).ajaxSubmit({
            url: posturl,
            dataType: 'json',
            beforeSend: function () {
			$('.formmessage').remove();
                $('#wait-div').show();
            },
            success: function (response) {
                $(':input[type="submit"]').prop('disabled', true);
              $("input[type=submit]").html('Processing');
                $('#wait-div').hide();
                if (response.messageNot) {
                    $(formid).find('.alert').removeClass('alert-success').removeClass('alert-danger').fadeOut(100);
                }
                else {
                    $(formid).find('.alert').removeClass('alert-success').removeClass('alert-danger').fadeIn(200);

                    if (response.status=="success") {
                        $(formid).find('.alert').fadeIn();
                        $(formid).find('.alert').addClass('alert-success').children('.ajax_message').html(response.success_msg);
                    }
                    else {
                        $(formid).find('.alert').fadeIn();
                        $(formid).find('.alert').addClass('alert-danger').children('.ajax_message').html(response.error_msg);
                    }
                }

                if (response.resetform)
                    $(formid).resetForm();
                
                if (response.register_form){
                    $('#frm_res').css({'display':'block'});
                    $('#frm_login').css({'display':'none'});
                     window.location.reload();
                }
                if (response.otp_form_show){
                    $('#mobile_number').attr('readonly', 'readonly');
                    $('.login_with_otp_box').css({'display':'block'});
                     window.location.reload();
                }
                if(response.status == 'error'){
                    show_messege_toast("danger", response.message, "Error !!");
                    return false;
                } 
                if(response.status == 'errorotp'){
                    show_messege_toast("warning", response.message, "OTP");
                    return false;
                }
                if(response.status == 'success'){
                    show_messege_toast("success", response.message, "Register");
                 } 
                  if(response.loading == 'true'){
                   setTimeout(function(){
                     $("#overlay").fadeOut(5000);
                      window.location = response.url;
                    },3000);
                 }
                if (response.url)
                    window.location.href = response.url;

                if (response.parentUrl)
                    window.top.location.href = response.parentUrl;

                if (response.selfReload)
                    window.location.reload();

                if (response.slideToThisDiv)
                    slideToDiv(response.divId);

                if (response.slideToTop)
                    slideToTop();

                if (response.slideToThisForm)
                    slideToElement(formid);

                if (response.ajaxPageCallBack) {
                    response.formid = formid;
                    ajaxPageCallBack(response);
                }
                if (response.ajaxPageCallBackData) {
                    response.formid = formid;
                    ajaxPageCallBackData(response);
                }
                if (response.hideModel) {
                    setTimeout(function () {
                        $('.modal').modal('hide');
                    }, 20000);
                }
                if (response.popup) {
                    parent.$.fancybox.update();
                }
                setTimeout(function () {
                    $(formid).find('.ajax_report').fadeOut(1000);
                    if (response.popup) {
                        parent.$.fancybox.update();
                    }
                }, 7000);
                setTimeout(function () {
                    if (response.popup) {
                        parent.$.fancybox.update();
                    }
                }, 81000);
                
                if(response.status=='success'){
                    $(formid)[0].reset();
                    // window.location.reload();

				}
                if(response.redirect=='yes'){
					window.location.href=response.redirectUrl;
				}
            },
            error: function(response){
				$(':input[type="submit"]').prop('disabled', false);
				var data = response.responseJSON;
                       $.each(data, function( key, value ) {
                      console.log(key + " => " + value);
                      var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
                       $('.ajax_form').find('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);
                 });
		}

        });
        return false;
    });
});
$(document).ready(function (e) {
    $(document).on("click", ".alert .close", function (event) {
        $(this).closest(".ajax_report").hide();
    });
});
function slideToElement(element) {
    $("html, body").animate({scrollTop: $(element).offset().top - 150}, 1000);
}
function slideToDiv(element) {
    $("html, body").animate({scrollTop: $(element).offset().top - 50}, 1000);
}
function slideToTop() {
    $("html, body").animate({scrollTop: 50}, 1000);
}

function show_messege_toast(type, message, title) {
    if(title == ''){ title = 'Information';}
    switch (type) {
        case "success":
            toastr.success(message, title, {timeOut: 1000});
            break;
        case "warning":
            toastr.warning(message, title, {timeOut: 1000});
            break;
        case "error":
            toastr.error(message, title, {timeOut: 5000});
            break;
        case "info":
            toastr.info(message, title, {timeOut: 10000});
            break;
        case "clear":
            toastr.clear();
            break;
        default:
            toastr.info(message, 'Information', {timeOut: 1000});
    }
}

function ajaxPageCallBack(response)
{
    var CallBackRequest	=	response.CallBackRequest;

    if(CallBackRequest == 'CheckBalance')
    {
        if(response.success){
            var data = response.data;
            var html ='<dt>Total Balance</dt><dd>$'+data.amount+'</dd><dt>Remain Balance</dt><dd>$'+data.remain_amount+'</dd><dt>Giftcard Expiry</dt><dd>'+data.expiry_date+'</dd>';

            $('.show-result').find('dl').html(html);
        }
        else{
            $('.show-result').find('dl').html('');
        }
    }
}
