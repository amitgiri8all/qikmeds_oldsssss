$(document).ready(function(){
$('#contact_us').submit(function( event ) {

     event.preventDefault();

     $('.btnsbmt').html('Processing');
     $('.btnsbmt').attr('disabled','disabled')
        $.ajax({
        url: contact_route,
        type: "POST",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend:function(){

            $('.formmessage').remove();

            }
        ,
        success: function(response) {
                   $('.btnsbmt').html('Submit');
                    $('.btnsbmt').removeAttr('disabled');
                     $(".btnsbmt").css("background-color","green");
                    toastr[response.status]("Sucessfully Submit", "Notifications");
                    $('#contact_us')[0].reset();

        },
        error: function(response){
                $('.btnsbmt').html('Submit');
                    $('.btnsbmt').removeAttr('disabled');
                    $('.btnsbmt').addClass('btn-success');

                   // $(".btnsbmt").css("background-color", "#d0d1e2");
            var data = response.responseJSON;
                       $.each(data, function( key, value ) {
                      console.log(key + " => " + value);
                      var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
                      $('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);

                 });
        }

        });

});

$('#customDesign').submit(function( event ) {

     event.preventDefault();

     $('.btnsbmt').html('Processing');
     $('.btnsbmt').attr('disabled','disabled')
        $.ajax({
        url: urlcustomDesign,
        type: "POST",
		data:new FormData($("#customDesign")[0]),
      //  data: $(this).serialize(),
        dataType: 'json',
        beforeSend:function(){
            $('.formmessage').remove();
		},
        success: function(response) {
			$('.btnsbmt').html('Submit');
			$('.btnsbmt').removeAttr('disabled');
			$(".btnsbmt").css("background-color","green");
             $("#customDesign").find('.alert .ajax_message').html("Your Information Sent Successfully.").show();
            $("#customDesign").find('.alert').removeClass('alert-danger').addClass('alert-success').show();
             setTimeout(function(){ $("#customDesign").find('.alert').fadeOut(); }, 5000);
			//toastr[response.status]("Sucessfully Submit", "Notifications");
			$('#customDesign')[0].reset();
			//alert('Form submited successfully');
			setTimeout(function(){window.location.href="/"; }, 6000);
			// window.location.href="/";
        },
        error: function(response){
			$('.btnsbmt').html('Submit');
			$('.btnsbmt').removeAttr('disabled');
			$('.btnsbmt').addClass('btn-success');
			var html='';
			var data = response.responseJSON;
			$.each(data, function( key, value ) {
			  console.log(key + " => " + value);
				html+= "<p>"+value+"</p>";
			});
             
             $("#customDesign").find('.alert .ajax_message').html(html).show();
             $("#customDesign").find('.alert').addClass('alert-danger').removeClass('alert-success').show();
             setTimeout(function(){ $("#customDesign").find('.alert').fadeOut(); }, 5000);
        }

	});

});


$('#register').submit(function( event ) {

     event.preventDefault();

     $('.regi').html('Processing');
     $('.regi').attr('disabled','disabled')
        $.ajax({
        url: register,
        type: "POST",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend:function(){

            $('.formmessage').remove();

            }
        ,
        success: function(response) {
                   if(response.status=="success"){
                     
                   $('.register').html('<div class="alert alert-success fade in alert-dismissable">'+response.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>')
                   $('.regi').html('Submit');
                    $('.regi').removeAttr('disabled');
                     $(".regi").css("background-color","green");
                    toastr[response.status]("Sucessfully Register", "Notifications");
                    $('#register')[0].reset();
                    $('#ModalSignup').modal('hide');
                   } 

        },
        error: function(response){
                $('.regi').html('Submit');
                    $('.regi').removeAttr('disabled');
                    $('.regi').addClass('btn-success');
                     var data = response.responseJSON;
                       $.each(data, function( key, value ) {
                      console.log(key + " => " + value);
                      var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
                      $('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);

                 });
       
        }

        });

});


$('#login').submit(function( event ) {

     event.preventDefault();
    var loginurl=$(this).attr('action');
    
    

     $('.signup').html('Processing');
     $('.signup').attr('disabled','disabled')
        $.ajax({
        url: loginurl,
        type: "POST",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend:function(){

            $('.formmessage').remove();

            }
        ,
        success: function(response) {
                 if(response.status=='success'){  
                   $('.signup').html('Submit');
                    $('.signup').removeAttr('disabled');
                     $(".signup").css("background-color","green");
                    toastr[response.status]("Sucessfully Login", "Notifications");
                  
                   $('.succes').html('<div class="alert alert-success fade in alert-dismissable">'+response.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>')
                   $('#login')[0].reset();
                   
                   window.location.href=response.redirecturl; 
                    
                  
                   }else{
					     
					      $('.error').show();
					      $('.signup').removeAttr('disabled');
                      $('.error').html('<div class="alert alert-danger fade in alert-dismissable">'+response.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>')
                     
                      $('.error').fadeOut(20000);  
					   
					   } 

        },
        error: function(response){
                $('.signup').html('Submit');
                    $('.signup').removeAttr('disabled');
                    $('.signup').addClass('btn-success');
                     var data = response.responseJSON;
                       $.each(data, function( key, value ) {
                      console.log(key + " => " + value);
                      var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
                      $('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);

                 });
       
        }

        });

});


$('#emailforgetform').submit(function( event ) {
    
	
	 event.preventDefault();
	 
	 	$('.emailbtn').html('Processing');
        $('.emailbtn').attr('disabled','disabled') 
		$.ajax({
		url : $(this).attr('action'),
		type: "POST",
		data: $('#emailforgetform').serialize(),
		dataType: 'json',
		 beforeSend:function(){
		
			$('.formmessage').remove();
			
			}
			,	
		success: function(response) {
			
		 $('.emailbtn').html('Submit');
                 $('.emailbtn').removeAttr('disabled');
		
		if(response.status== "error"){
			
			 $('#error_message').html(
				  "<div class='alert alert-danger alert-dismissable margin5'>"+
					"<button type='button' class='close' data-dismiss='alert' aria-hidden=true'>&times;</button>"+
					"<strong >Email incorrect</strong>"+ 
				   "</div>"
				   );
			
			}
		if(response.status== "success"){
				
				 $('#error_message').html(
				  "<div class='alert alert-success alert-dismissable margin5'>"+
					"<button type='button' class='close' data-dismiss='alert' aria-hidden=true'>&times;</button>"+
					"<strong >Your Password Reset Successfully . Please Check Your Mail</strong>"+ 
				   "</div>"
				   );
			}
		
		},
		error: function(response) {
			 $('.emailbtn').html('Submit');
                    $('.emailbtn').removeAttr('disabled');
                    
                     var data = response.responseJSON;
                       $.each(data, function( key, value ) {
                      console.log(key + " => " + value);
                      var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
                      $('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);
   
                 });
                 
			
			
			
		}
		});
});



});

function swapStyleSheet(sheet){
document.getElementById('pagestyle').setAttribute('href', sheet);
}

$(document).ready(function(){
	
	$('[data-toggle="tooltip"]').tooltip();


});
 $(document).ready(function(){
	 $('#offerclose').click(function(){
	 $('#offer').addClass("out");
		 });
	 });

$('#tracklogin').click(function(){
		$('#redirect_value').val('order');
});
$('#normallogin').click(function(){
		$('#redirect_value').val('');
});
$(window).on("scroll", function() {
    if($(window).scrollTop() > 300) {
        $(".header").addClass("active");

    } else {
 
       $(".header").removeClass("active");
    }
});
$('body').on('hidden.bs.modal', '.modal', function () {
  	$(this).removeData('bs.modal');
});

function openModel(url,position)
{
	$.get(url, function(data){
		$('#'+position).find('.modal-content').html(data);
		return false;
	});
}
function check_forgot_option(el)
{
      var value=    el;

    if( value == 'otp_mobile'){


           $('.mobile_no_input').show();
            $('.email_input').hide();

		}
		else if( value == 'tmp_pass_email'){
           $('.email_input').show();
             $('.mobile_no_input').hide();

		}


}


$(document).on("click", ".forget-pas-pop", function (event) {
    $('.login_div').hide();
    $('.login_forgot_option').show();
  });

  $(document).on("click", ".back_login_forgot_option", function (event) {


    $('.login_forgot_option').hide();
      $('.login_div').show();
  });
  $('#SimilarProductSlider').owlCarousel({
    items:5,
       loop:true,
    autoplay:true,
	  navigation: true,
        navigationText: [
          "<i class='fa fa-angle-left'></i> ",
          " <i class='fa fa-angle-right'></i> "
          ]


});

$('.owl-carousel').owlCarousel({
    items:5,
       loop:true,
    autoplay:true,
	  navigation: true,
        navigationText: [
          "<i class='fa fa-angle-left'></i> ",
          " <i class='fa fa-angle-right'></i> "
          ]


});

