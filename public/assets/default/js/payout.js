$(function(){
$('#first').click();
});

function data(ref,tableid,tab3=null)
{
	 var route = $(ref).attr('data-url');
	 var id = $(ref).attr('href');
	 if(tab3==null)
	 {
	
		 var table = $('#'+tableid).DataTable({
                processing: true,
                serverSide: true,
                bDestroy:true,
                aaSorting:[],
                ajax: route,
                
                columns: [
					
						{ data: 'spt_id', name: 'spt_id' },
						{ data: 'tran_date', name: 'tran_date' },
						{ data: 'action', name: 'action',render:function(data,type,row,meta){ return displaycrdr(data,type,row,meta)} },
						{ data: 'description', name: 'description' },
						{ data: 'amount', name: 'amount' },
						
                ]
            });
		}
		else
		{
			 var table = $('#'+tableid).DataTable({
                processing: true,
                serverSide: true,
                bDestroy:true,
                aaSorting:[],
                ajax: route,
                
                columns: [
					
						{ data: 'spt_id', name: 'spt_id' },
						{ data: 'amount', name: 'amount',render:function(data,type,row,meta){ return displaybal(data,type,row,meta)} },
						{ data: 'amount', name: 'amount',render:function(data,type,row,meta){ return displaybal1(data,type,row,meta)} },
						{ data: 'tran_date', name: 'tran_date'},
						{ data: 'current_balance', name: 'current_balance' },
						
						
                ]
            });
			
		}
	
}

function displaycrdr(data,type,row,meta)
{
	if(data=='Add')
	return 'CR';
	if(data=='Remove')
	return 'DR';
}
function displaybal(data,type,row,meta)
{
	if(row.action=='Add')
	return data;
	else
	return '';
	
}
function displaybal1(data,type,row,meta)
{
	if(row.action=='Remove')
	return data;
	else
	return '';
	
}
