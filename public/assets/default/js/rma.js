$(document).ready(function() {
});

function getComments(req_id) {
  var url = comment_url
  $.ajax({
    url: url,
    type: "GET",
    data: {
      "r": req_id,
    },
    dataType: 'json',
    success: function(data) {
      $('.comment_lists').html(data.comments);
    }
  });
}

function ajaxPageCallBack(data) {
  if (data.callback_type == "replace_comment") {
    getComments(re_id);
  }
}
$('body').on('hidden.bs.modal', '.modal', function() {
  $(this).removeData('bs.modal');
});