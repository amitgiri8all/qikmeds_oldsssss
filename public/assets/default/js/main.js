$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            var values = target.attr('data-inner');
            if(values==undefined){
				$(this).find(".btn-select-input").val(value);
			}else{
				$(this).find(".btn-select-input").val(values);
			}
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});




$('.owl-carousel').owlCarousel({
    loop:true,
	 autoplay:true,
	 autoPlay : 5000,
	  navigation: true,
        navigationText: [
          "<i class='fa fa-angle-left'></i> ",
          " <i class='fa fa-angle-right'></i> "
          ],
    margin:10,
    responsiveClass:true,
    
      items : 4,
      itemsDesktop : [1199,3],

      itemsDesktopSmall : [979,3],
})

$('.owl-carousel2').owlCarousel({
    loop:true,
	 autoplay:true,
	 autoPlay : 5000,
	  navigation: false,
	     items : 1,
 transitionStyle : "fadeUp",
     margin:10,
	 lazyLoad: true,

}) 
$('#newsletters').on('submit',function(e){
   var error="";
	$('#btnsbmt').html("Processing");
	$('#btnsbmt').addClass("disabled");
    e.preventDefault(e);
        $.ajax({
        type:"POST",
        url:newsletter,
        data:$(this).serialize(),
        dataType: 'json',
        success: function(data){
			$('#btnsbmt').html("Subscribe to our Newsletter");
			$('#btnsbmt').removeClass("disabled");
		 $("#newsletters")[0].reset();
            console.log(data);
            $('#newsemail').html("<span class='alert alert-success alert-dismissable margin5 col-sm-12' id='newsemail'>Email Successfully Submited</span>");
            $('#email').val('');
        },
        error: function(data){
			$('#btnsbmt').html("Subscribe to our Newsletter");
			$('#btnsbmt').removeClass("disabled");
			var data = data.responseJSON;
                       $.each(data, function( key, value ) {
                                error+=data[key]+"<br>";
                                $('#newsemail').html("<span class='alert alert-danger alert-dismissable margin5 col-sm-12' id='newsemail'>"+error+"</span>");
                 });
          }
    })
    });



      function random(owlSelector){
        owlSelector.children().sort(function(){
            return Math.round(Math.random()) - 0.5;
        }).each(function(){
          $(this).appendTo(owlSelector);
        });
      }
     
      $(".owl-carousel3").owlCarousel({
		    autoPlay: 4000, //Set AutoPlay to 3 seconds
 
      items : 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [979,2],
        navigation: false,
        navigationText: [
          "<i class='fa fa-angle-left'></i> ",
          " <i class='fa fa-angle-right'></i> "
          ],
        beforeInit : function(elem){
          //Parameter elem pointing to $("#owl-demo")
          random(elem);
        }
     
      });
      
      
      $(window).on("scroll", function() {
    if($(window).scrollTop() > 45) {
        $(".header").addClass("active");

    } else {
 
       $(".header").removeClass("active");
    }
});
		$(function() {
		
				$("#slider").slider({
					range: "min",
					min: 10,
					max: 100,
					value: 80			
				});
				$("#slider1").slider({
					range: true,
					values: [17, 83]
				});	
				
				$( "#slider2" ).slider({
					range: "min",
					value: 140,
					min: 1,
					max: 800,
					slide: function(event, ui) {
						$("#amount").val("$" + ui.value);
					}
				});
				
				$("#amount").val( 
					"$" + $("#slider2").slider("value") 
				);
				
				$("#slider3").slider({
					range: "max",
					min: 1,
					max: 10,
					value: 2,
					slide: function(event, ui) {
						$("#bedrooms").val(ui.value);
					}
				});
				
				$("#bedrooms").val( 
					$("#slider3").slider("value") 
				);
				
				
				//var select = $("#guests");
//				var guestnumber = $("#slider4").slider({
//					min: 1,
//					max: 10,
//					range: "min",
//					value: select[0].selectedIndex + 1,
//					slide: function(event, ui) {
//						select[0].selectedIndex = ui.value - 1;
//					}
//				});
				
				$("#guests").change(function() {
					guestnumber.slider("value", this.selectedIndex + 1);
				});
	
				$("#slider5").slider({
					value:100,
					min: 0,
					max: 500,
					step: 50,
					range: "min",
					slide: function(event, ui) {
						$("#donation").val(ui.value);
					}
				});
				
				$("#donation").val($("#slider5").slider("value"));
				$("#donation").blur(function() {
						$("#slider5").slider("value", $(this).val());
				});
				
				$("#slider6").slider({
					orientation: "vertical",
					range: "min",
					min: 0,
					max: 100,
					value: 60,
					slide: function(event, ui) {
						$("#volume").val(ui.value);
					}
				});
				
				$("#volume").val( 
					$("#slider6").slider("value") 
				);
				
				 $("#slider7").slider({
					orientation: "vertical",
					range: true,
					values: [27, 67],
					slide: function(event, ui) {
						$("#sales").val("$" + ui.values[0] + " - $" + ui.values[1]);
					}
				});
				$("#sales").val( "$" + $("#slider7").slider("values", 0) + " - $" + $("#slider7").slider("values", 1));
				
				$("#eq > .sliderv-wrapper").each(function() {
					var value = parseInt($(this).text(), 10);
						$(this).empty().slider({
						value: value,
						range: "min",
						animate: true,
						orientation: "vertical"
					});
				});	
				
				$("#eq2 > .sliderv-wrapper").each(function() {
					var value = parseInt($(this).text(), 10);
						$(this).empty().slider({
						value: value,
						range: "min",
						animate: true,
						orientation: "vertical"
					});
				});				
				
				
				$('#slider8').slider({
					range: true,
					values: [500, 1500],
					min: 10,
					max: 2000,
					step: 10,
					slide: function(event, ui) { 
						$("#budget").val("$" + ui.values[0] + " - $" + ui.values[1]);
					}			
				});
				
				$("#budget").val("$" + $("#slider8").slider("values", 0) + " - $" + $("#slider8").slider("values", 1));
				
				var initialYear = 1980;
				var yearTooltip = function(event, ui) {
					var curYear = ui.value || initialYear
					var yeartip = '<span class="slider-tip">' + curYear + '</span>';
					$(this).find('.ui-slider-handle').html(yeartip);
				}
				
				$("#slider10").slider({
					value: initialYear,
					range: "min",
					min: 1950,
					max: 2020,
					step: 1,
					create: yearTooltip,
					slide: yearTooltip
				});	
				
			$("#slider-range").slider({
				range: true,
				min: 0,
				max: 500,
				values: [100, 380],
				slide: function(event, ui) {
					$("#amounts").val("$" + ui.values[0] + " - $" + ui.values[1]);
				}
			});
			
			$("#amounts").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
			
	
			$("#slider11").slider({
				min: 100,
				max: 1000,
				step: 10,
				values: [300, 700],
				range: true,
				slide: function(event, ui) {
					for (var i = 0; i < ui.values.length; ++i) {
						$(".sliderValue[data-index=" + i + "]").val(ui.values[i]);
					}
				}
			});
			
			$(".sliderValue").change(function() {
					var $this = $(this);
					$("#slider11").slider("values", $this.data("index"), $this.val());
			});
		
		
            var valtooltip = function(sliderObj, ui){
                val1            = '<span class="slider-tip">'+ sliderObj.slider("values", 0) +'</span>';
                val2            = '<span class="slider-tip">'+ sliderObj.slider("values", 1) +'</span>';
                sliderObj.find('.ui-slider-handle:first').html(val1);
                sliderObj.find('.ui-slider-handle:last').html(val2);		                 
            };

            $( "#slider-range2" ).slider({
              range: true,
              min: 0,
              max: 500,
              values: [180, 400],              
			  create:function(event,ui){
					valtooltip($(this),ui);                    
              },
              slide: function( event, ui ) {
					valtooltip($(this),ui);
					$( "#amountx" ).val("$" + ui.values[0] + " - $" + ui.values[1]); 
              }, 
              stop:function(event,ui){
					valtooltip($(this),ui);             
              }			               

            });
			
			$( "#amountx" ).val("$" + $("#slider-range2").slider("values", 0) + " - $" + $("#slider-range2").slider("values", 1));
			
			var initialSpark = 60;
			var sparkTooltip = function(event, ui) {
				var curSpark = ui.value  || initialSpark
				var sparktip = '<span class="slider-tip">' + curSpark + '</span>';
				$(this).find('.ui-slider-handle').html(sparktip);
			}			
			
			$("#slider9").slider({
				orientation: "vertical",
				range: "min",
				min: 1,
				max: 100,
				step: 1,
				value: initialSpark,
				create: sparkTooltip,
				slide: sparkTooltip				
			});
			

		});				
 
});
