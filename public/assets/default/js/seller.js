
/*ok*/
$(function() {

 load_unseen_notification();
 load_unseen_announcement();

	//*************************** Seller login form  **********************************************************************///
$('.seller_login_form').submit(function( event ) {

	 event.preventDefault();

	  $('.btnsbmt-login').html('Processing');
	 $('.btnsbmt-login').attr('disabled','disabled');
		$.ajax({
		url : $(this).attr('action'),
		type: "POST",
		data: $(this).serialize(),
		dataType: 'json',
		beforeSend:function(){

			$('.formmessage').remove();

			}
        ,
		success: function(response) {


			 $('.btnsbmt-login').html('Submit');
			 $('.btnsbmt-login').removeAttr('disabled');


			if(response.message){

					 $('#login_message').show();
			          $('#login_message').html("<div id='login_message'  class='alert alert-danger'>"+response.message+"</div>");
					  $('#login_message').fadeIn('slow').delay(6000).hide(0);

					}

			if(response.redirect_url){
				 $('#login_message').show();
			     $('#login_message').html("<div id='login_message'  class='alert alert-info'>Login Successfully</div>");
				 $('#login_message').fadeIn('slow').delay(6000).hide(0);
				  window.location.href =response.redirect_url;
				}

			if(response.messageBag){


				 $('#login_message').show();
			     $('#login_message').html("<div id='login_message'  class='alert alert-danger'>"+response.messageBag+"</div>");
				 $('#login_message').fadeIn('slow').delay(6000).hide(0);
				}

			//alert('success')




		},
		error: function(response){

					if(response.message){
					//alert(response.message)
					 $('#login_message').show();
						$('#login_message').html(response.message);


					}

			 $('.btnsbmt-login').html('Submit');
			        $('.btnsbmt-login').removeAttr('disabled');

			var data = response.responseJSON;
                       $.each(data, function( key, value ) {
                      console.log(key + " => " + value);
                      var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
                      $('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);

                 });
		}

		});



});



	$('.change_password_seller').submit(function( event ) {

	 event.preventDefault();


	 $('.btn-chnahe-user-pass').html('Processing');
	 $('.btn-chnahe-user-pass').attr('disabled','disabled');

		$.ajax({
		url : $(this).attr('action'),
		type: "POST",
		data: $(this).serialize(),
		dataType: 'json',
		beforeSend:function(){

			$('.formmessage').remove();

			}
        ,
		success: function(response) {



				if(response.message){
					 $('.change_password_seller')[0].reset();
					$('#up_pass_user').show();

						$('#up_pass_user').html(response.message);
					    $('#up_pass_user').fadeIn('slow').delay(6000).hide(0);


					}
			 $('.btn-chnahe-user-pass').html('Save');
			        $('.btn-chnahe-user-pass').removeAttr('disabled');
				if(response.status=="success"){

						toastr[response.status]("Password Updated Successfully", "Notifications");
						 $('.change_password_seller')[0].reset();

			        if(response.redirect_url){

                         setTimeout(function(){
								window.location.href=response.redirect_url;
							}, 4000);
						}
					}





		},
		error: function(response){

			     $('.btn-chnahe-user-pass').html('Save');
			        $('.btn-chnahe-user-pass').removeAttr('disabled');
			var data = response.responseJSON;
                       $.each(data, function( key, value ) {
                      console.log(key + " => " + value);
                      var msg = '<p class="error formmessage" for="'+key+'"  style="color:red">'+value+'</p>';
                      $('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);

                 });
		}

		});



});








$(".metaform").on("submit", function(event) {
	$('.submitbtn_meta').html('Processing');
	$('.submitbtn_meta').removeClass('btn-success');
	$('.submitbtn_meta').addClass('btn-warning');
	$('.submitbtn_meta').attr('disabled', 'disabled')
	$.ajax({
		url: $(this).attr('action'),
		type: "POST",
		data: $(this).serialize(),
		dataType: 'json',
		success: function(data) {
			// log data to the console so we can see
			toastr[data.status](data.message, "Notifications");
			$('.submitbtn_meta').removeClass('btn-warning');
			$('.submitbtn_meta').addClass('btn-success');
			$('.submitbtn_meta').html('Save');
			$('.submitbtn_meta').removeAttr('disabled');
			$('.submitbtn_meta').removeAttr('disabled');
		}
	});
	return false;
	// stop the form from submitting and refreshing
	event.preventDefault();
});




$('.tabbable-line li a').click(function() {
	var tabpage = $(this).attr('href');
	if (tabpage == "#Image ") {
		displayimage();
	}

	if (tabpage == "#Customoption ") {
		displayattributedata();
	}

});


 $('.gift').on('change',function(){
		var value=$(this).is(':checked');
		if(value==true)
		var route=search_route;
		else
		var route=rsearch_route;

		$.ajax({
		  url: route+'/'+product_id,
		  type: "POST",
		  data: $('#filter_form').serialize(),
		  dataType: "json",
		  success: function(data) {
			 toastr[data.status](data.message, "Notifications");
			 }

		});
	});





});

 var counter = 1;
 var limit = 8;
 var my = 1;


  $(document).on("submit", "#bulk_form", function (event) {
	  
	  event.preventDefault();
	  var url =$(this).attr('action');
	   var product =$(this).attr('data-pro');
	   var from = $('#qty_range_from').val();
	   var to = $('#qty_range_to').val();
	   if(from<=to)
	   {
			$('.btnsbmt').val('Processing');
			$('.btnsbmt').attr('disabled','disabled');

			$.ajax({
					url:  url,
					type: "post",
					dataType: 'json',
					data: $(this).serialize() + "&product_id=" + product,
					success: function(data){
						$('.btnsbmt').val('Save');
				setTimeout(function(){
                    $('#table2').load(document.URL +  ' #table2');
                    //~ location.reload();
                },1000);
						$('.btnsbmt').removeAttr('disabled');

						if(data.status=="error")
						{
											$('#error_msg').fadeIn();
											$('#error_msg').html(
											"<div class='alert alert-danger alert-dismissable margin5'>"+
											"<button type='button' class='close' data-dismiss='alert' aria-hidden=true'>&times;</button>"+
											"<strong >"+data.message+"</strong>"+
											"</div>"
											).fadeOut(10000);
						}
						else
						{
							$('#bulk_form')[0].reset();
							$('#qty_range_from').attr('min',data.minqtyForNext+1);
							$('#qty_range_to').attr('min',data.minqtyForNext+1);
							toastr[data.status](data.message, "Notifications");
							bulkdata();
							if(data.count>=5)
							{
								$('#bulk_from_div').hide();
								$('#message').fadeIn();
											$('#message').html(
											"<div class='alert alert-info alert-dismissable margin5'>"+
											"<button type='button' class='close' data-dismiss='alert' aria-hidden=true'>&times;</button>"+
											"<strong >Now You Can Not Add Quantity Range</strong>"+
											"</div>"
											).fadeOut(10000);
							}

						}


					},
					error: function(response){
									alert(response);
					}

			  });
		  }
		  else
		  {
			  $('#error_msg').fadeIn();
											$('#error_msg').html(
											"<div class='alert alert-danger alert-dismissable margin5'>"+
											"<button type='button' class='close' data-dismiss='alert' aria-hidden=true'>&times;</button>"+
											"<strong >Qty Range from should Be lessthan Qty Range to !!</strong>"+
											"</div>"
											).fadeOut(10000);

		  }
   });


$(document).on("click", "#delete_pro_img", function(event) {
     var id = $(this).data('id');
     var imgurlp = imagedestroy + '/' + id;
     $.ajax({
         url: imgurlp,
         type: "post",
         dataType: 'json',
         data: {
             '_token': $('input[name=_token]').val(),
         },
         success: function(data) {
             toastr[data.status](data.message, "Notifications");
             displayimage();
         }
     });
 });
/*
$(document).on("click", ".setPrimary", function(event) {
     var id = $(this).data('id');
       $.ajax({
           url: 'quikmeds/admin/product/savecaption',
         type: "post",
         dataType: 'json',
         data: {
             '_token': $('input[name=_token]').val(),
             'id':id
         },
         success: function(data) {
             toastr[data.status](data.message, "Notifications");
             displayimage();
         }
     });
 });*/

$(document).on("click", ".savecaption", function(event) {
	var caption = $(this).closest("td").find("input[name=caption]").val();
     var id = $(this).data('id');
 //  alert(caption+'===='+id)
     

     $.ajax({
           url: 'quikmeds/admin/product/savecaption',
         type: "post",
         dataType: 'json',
         data: {
             '_token': $('input[name=_token]').val(),
             'caption': caption,
             'id':id

         },
         success: function(data) {
             toastr[data.status](data.message, "Notifications");
             displayimage();
         }
     });
 });


 $(document).on("click", ".setPrimary", function(event) {
      var id = $(this).data('id');
      

     $.ajax({
           url: 'quikmeds/admin/product/setprimary',
         type: "post",
         dataType: 'json',
         data: {
             '_token': $('input[name=_token]').val(),
              'id':id

         },
         success: function(data) {
             toastr[data.status](data.message, "Notifications");
             displayimage();
         }
     });
 });


 
$(document).on("click", ".edit_Attribute", function(event) {
    var id = $(this).data('id');
    counter = 1;
    var url = edit_attr_url + '/' + id;
    $.ajax({
        url: url,
        type: "post",
        dataType: 'json',
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(response) {
            var data = response;
            var attribute_id = data.attribute.atrb_id;
            $('.custom-form').each(function() {
                this.reset();
            });

              $('.custom-form-edit').each(function() {
                this.reset();
            });
            $('#attribute_name').val(data.attribute.attribute_name);
            $('#attribute_type').val(data.attribute.attribute_type);
            $('#mandatory').val(data.attribute.mandatory);
            for (var i = 0; i < 40; i++) {
                $('#removerow').click();
                counter = 1;
            }
            $.each(data.attribute_values, function(index, value) {
                if (index == (data.attribute_values.length - 1)) {} else {
                    $('#addrow').click();
                }
            });
            $.each(data.attribute_values, function(index, value) {
                $('#v' + index).val(data.attribute_values[index].attribute_value);
                $('#p' + index).val(data.attribute_values[index].price);
                $('#o' + index).val(data.attribute_values[index].order_no);
            });
            $('#saverecord').hide();
            $('#updaterecord').show();
            $('#form_type').val('edit');
            $('.custom-form').removeClass('custom-form').addClass('custom-form-edit');
            $('#update').empty();
            $('#update').append("<button id='updateatt' class='btn btn-primary btn-block btn-md btn-responsive' data-id=" + attribute_id + " >Update Record</button>");
            displayattributedata();
        }
    });
});

$(document).on("submit", ".custom-form", function(event) {
	event.preventDefault();
	$.ajax({
		url: attribute_form_submit,
		type: "post",
		dataType: 'JSON',
		beforeSend: function() {
			$('.formmessage').remove();
		},
		data: $(this).serialize(),
		success: function(data) {
			$('.custom-form').each(function() {
				this.reset();
			});
			for (var i = 0; i < 40; i++) {
				$('#removerow').click();
				counter = 1;
			}
			displayattributedata();
			toastr[data.status](data.message, "Notifications");
		},
		error: function(response) {
			var data = response.responseJSON;
			$.each(data, function(key, value) {
				var result = key.split('.');
				var index = key;
				if (result[0] == 'store') {
					result.shift();
					index = 'store[' + result.join("][") + ']';
					value = value[0].replace("store." + result[0] + ".", "");
				}
				var msg = '<p class="error formmessage" for="' + index + '"  style="color:red">' + value + '</p>';
				$('input[name="' + index + '"], select[name="' + index + '"],textarea[name="' + index + '"]').addClass('inputTxtError').after(msg);
			});
		}
	});
	return false;
});



 $(document).on("click", ".delete_Attribute", function(event) {
    var id = $(this).data('id');
    counter = 1;
    var url = delete_attr_url + '/' + id;
    $.ajax({
        url: url,
        type: "post",
        dataType: 'json',
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(response) {
			  toastr[response.status](response.message, "Notifications");
            displayattributedata();
        }
    });
});

$(document).on("click", ".delete_attr_val", function(event) {
    var id = $(this).data('id');
    counter = 1;
    var url = delete_attr_val_url + '/' + id;
    $.ajax({
        url: url,
        type: "post",
        dataType: 'json',
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(response) {
			  toastr[response.status](response.message, "Notifications");
            displayattributedata();
        }
    });
});

$(document).on("submit", ".custom-form-edit", function(event) {

    event.preventDefault();
    var id = $('#updateatt').data('id');
    var url = update_atr_url + '/' + id;

    $.ajax({
        url: url,
        type: "post",
        dataType: 'JSON',
        beforeSend: function() {
            $('.formmessage').remove();
        },
        data: $(this).serialize(),
        success: function(data) {

            $('.custom-form-edit').each(function() {
                this.reset();
            });

            for (var i = 0; i < 40; i++) {
                $('#removerow').click();
                counter = 1;
            }
             $('#saverecord').show();
					   $('#update').empty();
              $('.custom-form-edit').removeClass('custom-form-edit').addClass('custom-form');


            displayattributedata();
            toastr[data.status](data.message, "Notifications");

        },
        error: function(response) {

            var data = response.responseJSON;
            $.each(data, function(key, value) {
                var result = key.split('.');
                var index = key;

                if (result[0] == 'store') {
                    result.shift();
                    index = 'store[' + result.join("][") + ']';
                    value = value[0].replace("store." + result[0] + ".", "");
                }

                var msg = '<p class="error formmessage" for="' + index + '"  style="color:red">' + value + '</p>';
                $('input[name="' + index + '"], select[name="' + index + '"],textarea[name="' + index + '"]').addClass('inputTxtError').after(msg);
            });

        }


    });

    return false;

});


function addInput(divName) {
    var newdiv = document.createElement('div');
    newdiv.innerHTML = " <div class='row' id=" + counter + "><div class='col-sm-3'> Attributes Value :<input type='text'  class='form-control' id='v" + counter + "' name='store[" + counter + "][attribute_value]' >  </div>  <div class='col-sm-3'> Price :<input type='number'  step='0.01' min='0'  id='p" + counter + "'  class='form-control' name='store[" + counter + "][price]' >  </div>    <div class='col-sm-3'> Order No :<input type='number'  id='o" + counter + "'  min='1' max='15'   class='form-control' name='store[" + counter + "][order_no]' >  </div><div class='col-sm-3'> <br> <input type='button' id='removerow' value='Remove  Row' onClick='removeDiv(" + counter + ");'> </div> </div>";
    document.getElementById(divName).appendChild(newdiv);
    counter++;
}

function removeDiv(divId) {
    $("#" + divId).remove();
    var counter = 1;
}

function displayimage() {
   $.ajax({
	   url: imagedataurl,
	   type: "get",
	   dataType: 'html',
	   success: function(s) {
		   $('.imagerecord').html(s);
	   }
   });
}

function displayattributedata() {
    $.ajax({
        url: displayattributeurl,
        type: "get",
        dataType: 'html',
        success: function(s) {
            $('.displayrecord').html(s);
        }
    });
}

function ajaxPageCallBack(data) {
    if (data.success) {
        if (data.callback_type == 'Image data show') {
            displayimage();
        }
    }
}



$(document).on("click", "input[name = 'category']", function(event) {
	$('#catloading-image').show();
	var catname = $(this).attr('data-name');
	var form_name = $(this).closest('form').attr('id');
	var formno = $(this).attr('data-form');
	if(form_name=="main_cat")
	{
		$('#sub_cat2')[0].reset();
		$('#subcat_div2').hide();
	}
	if(form_name!="sub_cat2")
	{
		$.ajax({
			url: url,
			type: "post",
			dataType: 'JSON',

			data: $('#'+form_name).serialize(),
			success: function(data) {

				$('#subcat_div'+formno).show(catname);
				$('#cat'+formno).html(catname);
				$('#child'+formno).html(data.content);
			},
			complete: function(){
			$('#catloading-image').hide();
			},



		});
	}


});

$(document).on("submit", "#search_pro", function(event) {
	 event.preventDefault();
	 $('#loading-image').show();
		var cat_id = $('#search').attr('data-cat');

		$.ajax({
			url: search_pro,
			type: "post",
			dataType: 'JSON',

			data: $(this).serialize()+ "&cat_id=" + cat_id,
			success: function(data) {
				$('#product_list').html(data.content);


			},
			complete: function(){
			$('#loading-image').hide();
			},

		});

});


$(document).on("click", "#next", function(event) {

	var value = $('[name="category"]:last:checked');
	var chkArray = [];
	$.each($("input[name='category']:checked"), function(){
		chkArray.push($(this).val());
	});

    var lastEl = chkArray[chkArray.length-1];
    $('#search').attr('data-cat',lastEl);
    $('#category_panel').fadeOut(500);
    $('#smart_search').fadeIn(1000).scrollTop()+100;


});


$('#brand').change(function(){
	if($(this).val()!="")
	{
		$('#brand_name').prop('required',false);
		$('#brand_coll').removeClass('in');
		$('#brand_name').val('');
	}
});
$('#an_brand').click(function(){

	$('#brand').hide();
	$('#brand_name').prop('required',true);
});
$('#brand_div').click(function(){

		$('#brand').show();
		$('#brand_coll').removeClass('in');
		$('#brand_name').val('');
		$('#brand_name').prop('required',false);
});


$(document).on("submit", "#search_pro_sku", function(event) {
	 event.preventDefault();
	 $('#loading-image').show();

		$.ajax({
			url: search_pro,
			type: "post",
			dataType: 'JSON',

			data: $(this).serialize(),
			success: function(data) {
				$('#product_list').html(data.content);


			},
			complete: function(){
			$('#loading-image').hide();
			},

		});

});
$(document).on("submit", "#search_pro_cat", function(event) {
	 event.preventDefault();
	 $('#loading-image1').show();

		$.ajax({
			url: search_pro,
			type: "post",
			dataType: 'JSON',

			data: $(this).serialize(),
			success: function(data) {
				$('#product_list1').html(data.content);


			},
			complete: function(){
			$('#loading-image1').hide();
			},

		});

});
$('#next1').click(function(){

	  $('#list_panel').fadeOut('slow');
	   $('#list_panel2').fadeIn('slow');

	 });
 $('#cat_id').change(function(){
	var cat_id = $(this).val();
	$('#search_cpro').show();
	$('#search').attr('data-cat',cat_id);
 });



$(document).on('click', '.continue', function(){

	var pro_id = $(this).attr('data-id');
		$('.continue').html('Processing');
		$('.continue').attr('disabled','disabled');
      $.ajax({
		url: session_route,
		data:{'_token': $('input[name=_token]').val(),'pro_id':pro_id},
		type: "POST",
		dataType: 'json',
		success: function(response) {
			$('.continue').html('Continue');
			$('.continue').removeAttr('disabled');

             window.location.href=oldpro_route;

		}
		});


	});

$(document).on("submit", "#old_form", function(event) {

	 event.preventDefault();
	  $('.btnsmt').val('Processing');
	 $('.btnsmt').attr('disabled','disabled');
	 var route =$(this).attr('action');

		$.ajax({
			url: route,
			type: "post",
			dataType: 'JSON',
			beforeSend:function(){

			$('.formmessage').remove();

			}
        ,

			data: $(this).serialize(),
			success: function(data) {
					$('.btnsmt').val('Submit');
					$('.btnsmt').removeAttr('disabled');

				toastr[data.status]("Successfull Add", "Notifications");
				window.location.href=prolist;

			},
			complete: function(){
			$('#loading-image').hide();
			},
			error: function(response){


				$('.btnsmt').val('Submit');
				$('.btnsmt').removeAttr('disabled');

				var data = response.responseJSON;
				$.each(data, function( key, value ) {
				console.log(key + " => " + value);
				var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
				$('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);

				});
			}

		});

});
$('#newsletter').on('submit',function(e){
		$('#btnsmt').html('Processing');
		$('#btnsmt').attr('disabled','disabled');

    e.preventDefault(e);

        $.ajax({

        type:"POST",
        url:newsletter,
        data:$(this).serialize(),
        dataType: 'json',
        success: function(data){
			$('#btnsmt').html('Submit');
			$('#btnsmt').removeAttr('disabled');
			toastr[data.status]("Sucessfully Updated", "Notifications");
        },

		})
    });

	function filechange()
	{
		var posturl=$('#file_upload').attr('action');

			$('#file_upload').ajaxSubmit({
			url: posturl,
			dataType: 'json',
			beforeSend: function(){
				$('.formmessage').remove();
			},
			success: function(data){
				toastr[data.status]("Sucessfully Updated", "Notifications");
			},
			error:function(response){


			}
		});

		}

$(document).on("input", "#shop_name", function(event) {
	var shop_name = $(this).val();
		shop_name = shop_name.replace(/ /g, "");
		$('#shop_url').text('Shop URL: '+shop_name+'.'+site_url);

});

$(document).on("click", "#my_form input:radio", function(event) {


			$.ajax({
			url: route,
			 type:"POST",
			dataType: 'json',
			beforeSend: function(){
				$('.formmessage').remove();
			},
			data: $('#my_form').serialize(),
			success: function(data){
				toastr[data.status](data.message, "Notifications");
			},
			error:function(response){


			}
		});

	});

//****************************Seller Bulk Product**********************************//
$(document).on("click", ".logo_radio1", function (event) {
   var value = $(this).val();
	if(value=='Yes')
	{
		$('#bulk_data').show();
	}
	else
	{
		$('#bulk_data').hide();
	}

	 $.ajax({

        type:"POST",
        url:set_bulk_order,
        data:{'bulk_orders':value,'_token': $('input[name=_token]').val()},
        dataType: 'json',
        success: function(data){
			toastr[data.status]("Sucessfully Set", "Notifications");
        },

	})
});


$(document).on("click", ".general_form .logo_radio", function (event) {
   var value = $(this).val();
	if(value=='No')
	{
		$('#shipping_cost_div').show();
		$('#shipping_cost').prop('required',true);
	}
	else
	{
		$('#shipping_cost_div').hide();
		$('#shipping_cost').prop('required',false);
	}
});
 $(document).on("input", ".general_form .price", function (event) {

	if($('#price').val()!="")
	var price = $('#price').val();
	if($('#sale_price').val()!="")
	var sale_price = $('#sale_price').val();
	if(typeof price!='undefined' && typeof sale_price!='undefined');
	{
		var discount = Math.round(((price-sale_price)/price)*100);
		$('#discount').val(discount+'%');
	}

 });
 $(document).on("change", ".discount", function (event) {
	var dis = $(this).val();
	var unitprice = product_sale_price-((product_sale_price*dis)/100);
	$('.applicable_price').val(unitprice);

 });

function bulkdata()
{
 $(function() {
            var table = $('#table1').DataTable({
                processing: true,
                serverSide: true,
                bDestroy:true,
                aaSorting:[],
                ajax: bulk_data,

                columns: [

						{ data: 'qty_range_from', name: 'qty_range_from' },
						{ data: 'qty_range_to', name: 'qty_range_to' },
						{ data: 'discount', name: 'discount' },
						{ data: 'applicable_price', name: 'applicable_price' },
						{ data: 'shipping_cost', name: 'shipping_cost' },


                    { data: 'actions', name: 'actions', orderable: true, searchable: true }
                ]
            });

        });




	}
function editbulkData(bulk_ord_id,dis,appprice,shippingprice)
        {

			   BootstrapDialog.show({
				title: 'Edit Bulk Info',
				 message: function (dialogItself) {
				var $form = $('<form ></form>');
				var $titleDrop = $('<input type="number" value='+dis+' placeholde="Addidition Discount" name="discount" min="1" step="0.01" class="form-control"/><br>');
				var $titleDrop1 = $('<input type="number" value='+shippingprice+' placeholde="Shipping Cost"  name="shipping_cost" min="1" step="0.01" class="form-control"/><br>');
				dialogItself.setData('field-title-drop', $titleDrop);
				dialogItself.setData('field-title-drop1', $titleDrop1);   // Put it in dialog data's container then you can get it easier by using dialog.getData() later.
				$form.append('<label>Addidition Discount</label>').append($titleDrop).append('<label>Shipping Cost</label>').append($titleDrop1);

				return $form;
				},

				buttons: [{
					label: 'Edit',
					action: function(dialogItself) {
						var dis = dialogItself.getData('field-title-drop').val();
						var shippingcost = dialogItself.getData('field-title-drop1').val();

						$.ajax({
							url:add_bulk_info+'/'+bulk_ord_id,
							type: "POST",
							data: {'_token': $('input[name=_token]').val(),'sale_price' :product_sale_price,'discount':dis,'shipping_cost':shippingcost},
							dataType: 'json',
							success: function(response) {
							toastr[response.status](response.message, "Notifications");

								dialogItself.close();
								bulkdata();
							},

							});

					}
				}]
			});
		}
//****************************End Seller Bulk Product**********************************//




//*******************Start Notification and Anncoments *****************************//

 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:'',
   method:"GET",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('.noti-list').html(data.notification);

    if(data.unseen_notification > 0)
    {
     $('.count').html(data.unseen_notification);
    }
   }
  });
 }


 $(document).on('click', '.noti-tg', function(){
  $('.count').html('');
  load_unseen_notification('yes');
 });


 function load_unseen_announcement(view = '')
 {
  $.ajax({
   url:'',
   method:"GET",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('.anc-list').html(data.notification);

    if(data.unseen_notification > 0)
    {
     $('.anc-count').html(data.unseen_notification);
    }
   }
  });
 }



 $(document).on('click', '.anc-tg', function(){
  $('.anc-count').html('');
  load_unseen_announcement('yes');
 });



 //~ setInterval(function(){
  //~ load_unseen_notification();;
   //~ load_unseen_announcement();;
 //~ }, 5000);


function changePlacehoder(val)
{

	if(val =='order'){

	$('.search_input').attr('placeholder','Enter Invoice Id or Item Name Ordered');
	$('.serach_select').val('order');

	}

	if(val =='item'){

	$('.search_input').attr('placeholder','Enter SKU or Product Title');
	$('.serach_select').val('item');
	}
}


function search_redirect()
{
var search_selct = $('.serach_select').val();

if(search_selct == 'order'){

window.location = "{{ route('seller.orders') }}";

	}
	if(search_selct == 'item')
	{

window.location = "{{ route('productlist') }}";

		}
	}





 $(document).on('click', '.item-add-row', function(){
	
		var i= ($('.vari_row').length); alert(i);
		var markup ='<tr class="vari_row"><td><input type="text" class="form-control" name="item_specification['+i+'][name]"/></td>'+
		'<td><input type="text" class="form-control" name="item_specification['+i+'][value]" /></td>'+
		'<td><button type="button" class="item-delete-row"><i class="fa fa-trash"></i></button></td></tr>';
		$("#item_spci tbody .rowclass").before(markup);
    

});
  $(document).on('click', '.item-delete-row', function(){
                    $(this).parents("tr").remove();
                    	
		 });


$(document).on("click", ".product_listingp", function (event) {
	
   var value = $(this).val();
	if(value=='inquiry')
	{
		$('#manage_stock_div').hide();
		$('#bulk_order_div').hide();
		$('#bulk_hr').hide();
		$('#manage_stock_hr').hide();
		$('#product_type_div').hide();
		$('#pt_hr').hide();
		
	}
	else
	{
			$('#manage_stock_div').show();
			$('#bulk_order_div').show();
			$('#bulk_hr').show();
			$('#manage_stock_hr').show();
			$('#pt_hr').show();
			$('#product_type_div').show();
	}
});


$(document).ready(function(){	

   var value =  	$('input[name=listing_purpose]:checked').val();
	if(value=='inquiry')
	{
		$('#manage_stock_div').hide();
		$('#bulk_order_div').hide();
		$('#bulk_hr').hide();
		$('#manage_stock_hr').hide();
		$('#product_type_div').hide();
		$('#pt_hr').hide();
		
	}
	else
	{
			$('#manage_stock_div').show();
			$('#bulk_order_div').show();
			$('#bulk_hr').show();
			$('#manage_stock_hr').show();
			$('#pt_hr').show();
			$('#product_type_div').show();
	}
});
