$(document).ready(function() {
	$(document).on("submit", ".ajaxform", function (event) {
	var posturl=$(this).attr('action');
	var callbackFunction=$(this).attr('data-callback_function');
	if(callbackFunction)
	{
		if(callbackForm(callbackFunction) == false)
		{
			return false;
		}
	}
	var btn_txt;
	var formid = $(this).attr('id');
	if(formid)
	var formid = '#'+formid;
	else
	var formid = ".ajaxform";

	$(this).ajaxSubmit({
			url: posturl,
			dataType: 'json',
			beforeSend: function(){
				$(formid).find('.alert').removeClass('alert-success').removeClass('alert-danger').removeClass('alert-info');
				$(formid).find('.alert').addClass('alert-info').children('.ajax_message').html('<p><strong>Please wait! </strong>Your action is in proccess...</p>');
				btn_txt = $(formid).find("input[type=submit]").html();
				if(!isset(btn_txt))
				{
					btn_txt = $(formid).find("button[type=submit]").html();
				}

				$(formid).find("input[type=submit]").attr("disabled", "disabled");
				$(formid).find("button[type=submit]").attr("disabled", "disabled");
				$(formid).find("input[type=submit]").html('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Please wait');
				$(formid).find("button[type=submit]").html('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Please wait');
				$(formid).find('.alert').fadeIn();
			},
			success: function(response){

				$(formid).find("input[type=submit]").removeAttr("disabled");
				$(formid).find("button[type=submit]").removeAttr("disabled");
				$(formid).find("input[type=submit]").html(btn_txt);
				$(formid).find("button[type=submit]").html(btn_txt);

				$('#wait-div').hide();
				$(formid).find('.alert').removeClass('alert-success').removeClass('alert-danger').removeClass('alert-info');

				if(response.message)
				{
					if(response.messageNot)
					{
						$(formid).find('.alert').fadeOut(100);
					}
					else
					{
						$(formid).find('.alert').fadeIn(200);

						if(response.success)
						{
							$(formid).find('.alert').fadeIn();
							$(formid).find('.alert').addClass('alert-success').children('.ajax_message').html(response.message);
						}
						else
						{
							$(formid).find('.alert').fadeIn();
							$(formid).find('.alert').addClass('alert-danger').children('.ajax_message').html(response.message);
						}
					}
				}
				else
				{
					$(formid).find('.alert').fadeOut(100);
				}

				if(response.resetform)
				$(formid).resetForm();

				if(response.url)
				window.location.href=response.url;

				if(response.parentUrl)
				window.top.location.href = response.parentUrl;

				if(response.selfReload)
				window.location.reload();

				if(response.slideToThisDiv)
				slideToDiv(response.divId);

				if(response.slideToTop)
				slideToTop();

				if(response.scrollToThisForm)
				slideToElement(formid);
				if(response.ajaxPageCallBack)
				{
                                        response.formid = formid;
					ajaxPageCallBack(response);
				}
				if(response.ajaxPageCallBackData)
				{
					response.formid = formid;
					ajaxPageCallBackData(response);
				}
				if(response.hideModel)
				{
					setTimeout(function()
					{
						$('.modal').modal('hide');
					}, 2000);
				}
				setTimeout(function() {
					$(formid).find('.ajax_report').fadeOut(1000);
				}, 7000);
			},
			error:function(response){alert( 'Connection error')}
		});
	return false;
	});

	$(document).on("click", ".alert .close", function (event) {
		$(this).closest(".ajax_report").hide();
		$(this).closest(".alert").hide();
	});
});

function slideToElement(element,position)
{
	if(position == '')
	{
		position	=	150;
	}
	$("html, body").animate({scrollTop: $(element).offset().top }, 1000);
}
function slideToDiv(element)
{
	$("html, body").animate({scrollTop: $(element).offset().top-50 }, 1000);
}
function slideToTop()
{
	$("html, body").animate({scrollTop: 50}, 1000);
}

function isset(variable)
{
	if(typeof(variable) != "undefined" && variable !== null)
	{
		return true;
	}
	else
	{
		return false;
	}
}
