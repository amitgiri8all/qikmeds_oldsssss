var pagefrom;
var dType	=	'json';
var	rType	=	"POST";
var availableTags = [];
    

var	checkoutUrl	=	siteurl+'checkout';
$(function(){

	$(document).on("click", ".top-cart-content .products_list li .close_product", function (event) {
		performcartoption(this,'remove');
	});

	$(document).on("click", ".cartTable.maincart .CartProduct .delete a", function (event) {
		performcartoption(this,'remove_item');
	});

	$(document).on("change", ".cartTable.maincart .CartProduct .quantity", function (event) {
		$('#update_cart').submit();
	});

	$(document).on("click", ".modal-content .continue-shop", function (event) {
		modal_id =	$(this).closest('.modal').attr('id');
		$('#'+modal_id).modal('hide');
	});

	$(document).on("change", ".upload-img", function (event) {
		$(this).closest('.image-containor').find('#waiting-div').show();
		$(this).closest('form').submit();
	});

	$(document).on("click", "a.buy_now", function (event) {

		$(this).closest('div').find('input[name="action"]').remove();
		$(this).closest('div').append('<input type="hidden" name="action" value="buy_now">');
		//$(this).closest('.image-containor').find('#waiting-div').show();
		$(this).closest('form').submit();
	});

});

/* Show Success Or Error message in Bootstrap */
function show_messege_toast(message,type)
{
	if(type == '')
	type = "info";
	_toastr(message,"top-right",type,false);
}

/* Show Success Or Error message in Bootstrap */
function show_messege(message,type)
{
	if(type == 'error')
	var color =  '#d9534f';
	else if(type == 'success')
	var color =  '#5cb85c';
	else
	var color =  '#5bc0de';

	var dialog = new BootstrapDialog({
		message: function(dialogRef){
			var $message = $('<div>'+message+'</div>');

			return $message;
		},
		closable: true
	});
	dialog.realize();
	dialog.getModalHeader().hide();
	dialog.getModalFooter().hide();
	dialog.getModalBody().css('background-color', color);
	dialog.getModalBody().css('border-radius', '5px');
	dialog.getModalBody().css('color', '#fff');
	dialog.getModalBody().css('text-align', 'center');
	dialog.getModalContent().css('background-color', 'transparent');
	dialog.getModalDialog().css('background', 'transparent');
	dialog.open();
	setTimeout(function(){
		dialog.close();
	}, 4000);
}

function show_modal(message,type,p_info,model_id)
{
	var html= '';
	html+= '<div class="modal-body"><div class="add-cart-popup">';
	html+= '<div class="alert alert-'+type+'">'+message+'</div>';
    html+= '<div class="media"><div class="media-left"><img alt="'+p_info.name+'" src="'+p_info.image+'" width="80" class="media-object"></div>';
    html+= '<div class="media-body"><h4><a href="'+p_info.url+'">'+p_info.name+'</a></h4><p><span>Price: <strong><i class="fa fa-rupee"></i>'+p_info.price+'</strong></span> <span>Qty.: <strong>'+p_info.qty+'</strong></span></p>';
    html+= '</div></div>';
    html+= '<div class="graybuttons clearfix continue-shop"> <a href="javascript:void(0);" class="btn btn-black pull-left">Continue Shopping</a> <a href="'+checkoutUrl+'" class="btn btn-black pull-right">Go to shopping cart</a> </div>';
    html+= '</div></div>';

	if(model_id == '')
	model_id = 'modal-regular';

	$('#'+model_id).find('.modal-content.my-model').html(html);
	$('#'+model_id).modal('show');
}

function ajaxPageCallBack(response)
{
	var CallBackRequest	=	response.CallBackRequest;

	if(CallBackRequest == 'add_cart')
	{
		var page = ["header"];
		getcartinfo(page);
	}

	if(CallBackRequest == 'remove')
	{
		var page = ["header"];
		getcartinfo(page);
	}

	if(CallBackRequest == 'update_cart')
	{
		var page = ["header","detail"];
		getcartinfo(page);
	}

    if(CallBackRequest == 'reset_link')
	{
		var url = siteurl+'reset-link?email='+response.email;
        $('#reset_link').show();
        $('#reset_link a').attr('href',url);
	}

	if(CallBackRequest == 'write_review')
	{
		$('#review').find('.comment-container').append(response.content);
		$('#rrid').hide();
	}

	if(CallBackRequest == 'change_profile_image')
	{
		var target = $(response.formid);
		target.closest('.image-containor').find('#waiting-div').hide();

		if(response.success)
		{
			show_messege_toast(response.message,'success');
			$("#comp_logo").attr('src',response.imgsrc);
		}
		else
		{
			show_messege_toast(response.message,'error');
		}
	}
}

function getcartinfo(page)
{
	var posturl=siteurl+'cart/getinfo';
	$.ajax({
	url: posturl,
	dataType: 'json',
	type: "GET",
	data: {page:page},
	beforeSend: function(){
		$('#wait-div').show();
	},
	success: function(data){
		$('#wait-div').hide();
		$.each(page, function(key, value) {
			if(value == 'header')
			{
				$('.top_cart').find('.title').html(data.total_item+' item(s) - ');
				$('.top_cart').find('.cart-total-full').html('PKR '+data.total_amount);
				$('.top-cart-content').html(data.cupper);
			}
			if(value == 'detail')
			{
				if(data.cart_empty)
				{
					$('.cart-page .cart-inner').html(data.cart_left);
				}
				else
				{
					$('table.cartTable tbody').html(data.cart_left);
					$('.cart_totals table tbody').html(data.cart_right);
				}
			}
		});
	},
	error: function (data) {
			return false;
		}
	});
}

function performcartoption($this,task)
{
	var proceed = false;
	var page 	= ["header"];

	if(task=='remove')
	{
		var id 		= $($this).closest('li').attr('data-item');
		var data	= { rowid: id,task:task};
		if(pagefrom == 'cart'){
			page		= ["header","detail"];
		}
		proceed		= true;
	}

	if(task=='remove_item')
	{
		var id 		= $($this).closest('td').find('input[type=hidden]').val();
		var data 	= { rowid: id,task:'remove'};
		page		= ["header","detail"];
		proceed		= true;
	}
	//console.log(task+':'+id);

	if(proceed)
	{
		var posturl=siteurl+'cart';
		$.ajax({
		url: posturl,
		dataType: 'json',
		type: "POST",
		data: data,
		beforeSend: function(){
			ajax_action('ajaxcart');
		},
		success: function(data){
			ajax_action_unset('ajaxcart');
			getcartinfo(page);

			if(data.message)
			{
				if(data.success)
				var type = 'success';
				else
				var type = 'error';
				show_modal(data.message,type,data.p_info,'modal-small');
			}
		},
		error: function (data) {
				return false;
			}
		});
	}
}

function ajax_action(type)
{
	if(type == '')
	type = 'form';
	var loading_img	=	$('body .wrapper #ajaxconfig_info').find('div img').attr('src');

	$('body').append('<div id="wraper_ajax"></div>');
	//$('body').css('overflow','hidden');

	//if(type == 'ajaxcart')
	$('body').find('#wraper_ajax').html('<div class="loadding loadding_'+type+'"><img src="'+loading_img+'"></div>');
}
function ajax_action_unset(type)
{
	//$('body').css('overflow','');
	$('body').find('#wraper_ajax').remove();
}

$('body').on('hidden.bs.modal', '.modal', function () {
  	$(this).removeData('bs.modal');
});

function openModel(url,position)
{
	$('#'+position).modal('show');
	$('#'+position).find('.modal-content').html('<div style="text-align:center;"><img src="'+templateassets+'images/spin.svg" alt="" class="loading">Please wait...</div>');
	$.get(url, function(data){
		$('#'+position).find('.modal-content').html(data);
		return false;
	});
}


function ajax_call(call_by,$this,posturl,data_type,req_type,data,OtherPar) {

	if(sessionId)
	{
		$.ajax({
		url: posturl,
		dataType: data_type,
		type: req_type,
		data: data,
		beforeSend: function(){
			ajax_action();
		},
		success: function(data){
			ajax_action_unset();
			if(data.success)
			var type = 'success';
			else
			var type = 'error';
			show_messege_toast(data.message,type);
			if(call_by == 'wishlist')
			{
				if(OtherPar[0] == 'add')
				{
					if(OtherPar[1] == 'detail')
					{
						$('.product-right.actions').find('a[rel=wishlist]').remove();
					}
					if(OtherPar[1] == 'list')
					{
						$($this).html('<i class="fa fa-star fill"></i><br>Added to Wishlist');
						//$('.product-right.actions').find('a[rel=wishlist]').remove();
					}
				}
				if(OtherPar[0] == 'remove')
				{
					$($this).closest('tr').fadeTo("slow",0.5, function(){
						$(this).remove();
					});

					var rows = $('.cartContent table.cartTable tbody').find('tr').length;
					if(rows == 0)
					{
						//window.location.reload();
					}
				}
			}

		},
		error: function (data) {
				return false;
			}
		});
	}
	else
	{
		$( "#login" ).trigger( "click" );
	}
}

function flyToElement(flyer, flyingTo) {
    var $func = $(this);
    var divider = 3;
    var flyerClone = $(flyer).clone();
    $(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 1000});
    $('body').append($(flyerClone));
    var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
    var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;

    $(flyerClone).animate({
        opacity: 0.4,
        left: gotoX,
        top: gotoY,
        width: $(flyer).width()/divider,
        height: $(flyer).height()/divider
    }, 700,
    function () {
        $(flyingTo).fadeOut('fast', function () {
            $(flyingTo).fadeIn('fast', function () {
                $(flyerClone).fadeOut('fast', function () {
                    $(flyerClone).remove();
                });
            });
        });
    });
}

function showSpecification(categoryID){
	$('#editshowdata').hide();
	//var categoryID = $(this).val();

	if(categoryID){
		$.ajax({
			type:'POST',
			url:siteurl+'get-attribute/',
			data:{'country_id' : categoryID},
			dataType:'json',

			success:function(data){
				if(data.content)
				$('#showdata').show();
				else
				$('#showdata').hide();
				$('#speci').html(data.content);
			}
		});
	}
}

var category;
var sub_category;
var catObject	= new Object();

$(document).on('click','#choose-cat, #choose-cat-btn',function(){
	chooseCategory();
});

function chooseCategory(){
	openModel(siteurl+'ajax/create_product/categories/','modal-large');
	catObject		= new Object();
	catObject.first	= new Object();
	catObject.scnd	= new Object();
	catObject.thrd	= new Object();
}
$(function()
{
	 $( "#keyword" ).autocomplete({
	  source: siteurl+"keysearch/autocomplete",
	  minLength: 1,
	  scroll: false,
	  select: function(event, ui) {
		
	  	$('#keyword').val(ui.item.value);
	  }
	});
});
  

$(document).on('click','.categories-type #icons-placeholder a.parent-category',function(){
	category 			= $(this).attr('data-category');
	var category_name	= $(this).find('p').text();

	$('#category-0').removeClass('hidden');
	$('#categoryTitleForIcons').html(category_name).hide();
	$('.categories-type .icongrid').html('');

	$('#category-0 .overview').find('a.icon-list').removeClass('selected');
	$('#category-0 .overview').find('a.icon-list.category-'+category).addClass('selected');

	$('#category-'+category).show();
	$('#empty-cateogry').removeClass('hidden');
	catObject.first.name	= category_name;
	catObject.first.id		= category;
	//catObject.first.image 	= $('#cat-'+catObject.first.id).find('span.caticon img').attr('src');
});
$(document).on('click','.firstcolumn a.icon-list',function(event){
	event.preventDefault();
	category 		= $(this).attr('data-category');
	category_name 	= $(this).find('span.inlblk').text();

	$('.hiddencat').hide();
	$('#category-'+category).show();
	$('#categoryTitleForIcons').html(category_name).hide();
	$('#category-0 .overview').find('a.icon-list').removeClass('selected');
	$('#category-0 .overview').find('a.icon-list.category-'+category).addClass('selected');

	catObject.first.name	= category_name;
	catObject.first.id		= category;
	//catObject.first.image	= $('#cat-'+catObject.first.id).find('span.caticon img').attr('src');
});
$(document).on('click','.hiddencat a.icon-list',function(event){
	event.preventDefault();
	sub_category 	= $(this).attr('data-category');
	category_name 	= $(this).find('span.inlblk').text();

	lastcolumn = $(this).closest('.hiddencat').hasClass('lastcolumn');
	$('.hiddencat.lastcolumn').hide();
	if($('#category-'+sub_category).length > 0){
		$('#empty-cateogry').hide();
		$('#category-'+sub_category).show();
		$('#category-'+category+' .overview').find('a.icon-list').removeClass('selected');
		$('#category-'+category+' .overview').find('a.icon-list.category-'+category).addClass('selected');

		if(!lastcolumn){
			catObject.scnd.name	= category_name;
			catObject.scnd.id	= sub_category;
		}

	}else{

		if(lastcolumn){
			catObject.thrd.name	= category_name;
			catObject.thrd.id	= sub_category;
		}
		if(!lastcolumn){
			catObject.scnd.name	= category_name;
			catObject.scnd.id	= sub_category;
		}

		update_category();
		showSpecification(sub_category);
      
		$('#modal-large').modal('hide');
		$('#empty-cateogry').show();
	}
});


function update_category(){

	//console.log(catObject);
	//$('#catgory-breadcrumb-text').html('');

	$('#category-breadcrumb-container').html('<ol class="breadcrumb" id="catgory-breadcrumb-text" style="display:inline-block;"></ol><input type="hidden" class="gui-input" name="category_id" id="category_id" value=""><a id="choose-cat-btn" href="javascript:" class="btn btn-success btn-sm" title="Select category"> Change</a> <i class="fa fa-check-circle" aria-hidden="true" style="font-size:20px; margin-left:10px; color:orange; vertical-align:middle;"></i><div class="clr"></div>');
	//<img src="" height="50">

	$("#category_id").val(sub_category);
	//alert(catObject.first.image);
	//$('#category-breadcrumb-container').find('img').attr('src',catObject.first.image);
	$.each(catObject, function(key, value) {
		if(value.name){
			$('#catgory-breadcrumb-text').append('<li>'+value.name+'</li>');
		}
	});
}

function box(type,value)
{
	this.type=value;
}
