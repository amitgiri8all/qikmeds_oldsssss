$(function(){

	$(document).on("click", ".field-edit .itemSelector .action-btn", function (event) {
		var act_mode = $(this).attr('data-mode');
		if(act_mode == 'open-form')
		{
			generateform($(this).closest('.itemSelector'));
		}
		else if(act_mode == 'save-form')
		{
			saveform($(this).closest('.itemSelector'));
		}
	});
	$(document).on("change", ".upload-img", function (event) {
		$(this).closest('.image-containor').find('#waiting-div').show();
		$(this).closest('form').submit();
	});

});

function generateform(target)
{
	target.find('.action-btn').attr('data-mode','save-form');

	target.find('.itemContainer .text-box').hide();
	target.find('.itemContainer .form-box').show();
	target.find('.action-btn').removeClass('glyphicon-plus').addClass('glyphicon-ok-sign');
}

function saveform(target)
{
	target.find('.itemContainer form').submit();
}

function ajaxPageCallBack(response)
{
	var CallBackRequest	=	response.CallBackRequest;

	if(CallBackRequest == 'update_description' || CallBackRequest == 'update_skills' || CallBackRequest =='update_moto'  ||  CallBackRequest =='add_mobile_no' || CallBackRequest =='add_dob' || CallBackRequest =='add_gender' || CallBackRequest == 'change_password' || CallBackRequest=='add_timezone'  || CallBackRequest=='update_category' )
	{
		if(response.success)
		{
			var target = $(response.formid).closest('.itemSelector');
			if(CallBackRequest == 'update_description')
                        {

                            var myString = response.content.personal_description;
                            
                            target.find('.itemContainer .text-box').html(myString.nl2br()).fadeIn();
			
                        }
                        if(CallBackRequest == 'update_skills')
			target.find('.itemContainer .text-box').html(response.content.skills).fadeIn();

                        if(CallBackRequest == 'update_moto')
			target.find('.itemContainer .text-box').html(response.content.moto).fadeIn();

                        if(CallBackRequest == 'add_mobile_no')
			target.find('.itemContainer .text-box').html(response.content.phone).fadeIn();

                        if(CallBackRequest == 'add_dob')
			target.find('.itemContainer .text-box').html(response.content.dob).fadeIn();

                        if(CallBackRequest == 'add_gender')
			target.find('.itemContainer .text-box').html(response.content.gender).fadeIn();
                      

                       if(CallBackRequest == 'add_timezone')
			target.find('.itemContainer .text-box').html(response.content.time_zone).fadeIn();
                               


                        if(CallBackRequest == 'update_category')
                        {
                           //alert(response.content.coach_type);
                            //console.log(response.content.coach_type);

                         

			target.find('.itemContainer .text-box').html(response.content.category_name).fadeIn();
                        } 





			target.find('.itemContainer .text-box').attr('data-mode','update');
			target.find('.itemContainer .form-box').hide();
			target.find('.action-btn').attr('data-mode','open-form');
			target.find('.action-btn').removeClass('glyphicon-plus').removeClass('glyphicon-ok-sign').addClass('glyphicon-pencil');
		}
	}

	if(CallBackRequest == 'change_profile_image')
	{
		var target = $(response.formid);
		target.closest('.image-containor').find('#waiting-div').hide();

		if(response.success)
		{
			show_messege(response.message,'success');
			$("#comp_logo").attr('src',response.imgsrc);
		}
		else
		{
			show_messege(response.message,'error');
		}
	}
}

/* Show Success Or Error message in Bootstrap */
function show_messege(message,type)
{
	if(type == '')
	type = "info";
	_toastr(message,"top-right",type,false);
}

String.prototype.nl2br = function()
{
    return this.replace(/\n/g, "<br />");
}



