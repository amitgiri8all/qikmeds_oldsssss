window.onload = function () {

var dataPoints1 = [];

	$.each(tabData, function(key, value) {
            var xtime  	  = new Date(value.x);
            yValue1        = parseFloat(value.y);
            dataPoints1.push({
                x: xtime,
                y: yValue1
            });
        });
        
	var options = {
		height:200,
		animationEnabled: true,
		axisX:{
		title: "Date",
		valueFormatString: "DD/MMM/YYYY",
		
		},
		axisY:{
		title: "Sale",
		},
		data: [
		{
			type: "column",
			dataPoints:dataPoints1
			
		}
		]
	};

	var chart=$("#chartContainer").CanvasJSChart(options);
	
}
