
//******************* Pricing And Inventory*****************************//


 $(document).on('click', '.add-row', function(){

	 if($('.vari_row').length==2)
		$('.rowclass').hide();
		var markup ='<tr class="vari_row"><td><input  required name="attribute_name[]" type="text" class="form-control"></td>'+
		'<td><input type="text" value=""  name="attri_values[]" class="tags" /></td>'+
		'<td><button type="button" class="delete-row"><i class="fa fa-trash"></i></button></td></tr>';
		$("#quo_table tbody .rowclass").before(markup);
      $('.tags').tagsinput('refresh');

});
  $(document).on('click', '.delete-row', function(){
                    $(this).parents("tr").remove();
                    	if($('.vari_row').length<3)
						$('.rowclass').show();
						  setCombinationRequest();
		 });

$(document).ready(function() {
	$('.tags').tagsinput();
    $(document).on('itemAdded', '.tags', function(item, tag){
        $('.items').html('');
		var tags = $('.tags').tagsinput('items');
        setCombinationRequest();
    });
});
 $(document).on('itemRemoved', '.tags', function(item, tag){
	 var ref= $(this);
        $('.items').html('');
		var tags = $('.tags').tagsinput('items');
        setCombinationRequest();
    });

 function setCombinationRequest()
{
	$.ajax({
		url:  combi_route,
		type: "POST",
		dataType: 'json',
		data: $('#attr_form').serialize(),
		success: function(data){

			$('#set').html(data.set_table);
	}
	});

}
 $(document).on('submit', '#attr_form', function(event){
	  event.preventDefault();


	   BootstrapDialog.confirm('Are you sure want to save beacuse after save attribute can not be changed', function(result){
            if(result) {
				$('#attr_form').ajaxSubmit({
						url:  attr_route,
						type: "POST",
						dataType: 'json',
						data:{'product_id' : product_id},
						beforeSend:function(){
							$('.formmessage').remove();
								$('#attr_form').find("input[type=submit]").html('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Please wait');
								$('#attr_form').find("input[type=submit]").attr("disabled", "disabled");
						},
						success: function(data){
								$('#attr_form').find("input[type=submit]").removeAttr("disabled");
								$('#attr_form').find("input[type=submit]").val('Submit');
								$('#quo_table').hide();
									$('#edit_set').html(data.set_table);


					},
					error: function(response){
						$('#attr_form').find("input[type=submit]").removeAttr("disabled");
								$('#attr_form').find("input[type=submit]").val('Submit');
					var data = response.responseJSON;
							   $.each(data, function( key, value ) {
								  console.log(key + " => " + value);
								  var msg = '<label class="error formmessage" for="'+key+'"  style="color:red">'+value+'</label>';
								  $('input[name="' + key + '"], select[name="' + key + '"],textarea[name="' + key + '"]').addClass('inputTxtError').after(msg);

							 });
					}
					});

			}else {

            }
        });

   });
//*******************End Pricing And Inventory*****************************//
