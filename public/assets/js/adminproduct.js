function setProductCommission(pro_id,pro_comm)
        {
			  if(pro_comm!=null)
			  {
					var setcomm =pro_comm;
					var label="<label>Edit Product Specific Commission: </label>";
			  }
			  else
			   {
					var setcomm ='';
					var label="<label>Add Product Specific Commission: </label>";
			   }
			 
			  	
				BootstrapDialog.show({
				title: 'Define Admin Commission',
				message: function (dialogItself) {
				var $form = $('<form ></form>');
				var $titleDrop = $('<input required type="number" placeholde="Enter Commission" value='+setcomm+' name="product_commission" min="0" max="99" step="0.01" class="form-control"/>');
				dialogItself.setData('field-title-drop', $titleDrop);   // Put it in dialog data's container then you can get it easier by using dialog.getData() later.
				$form.append(label).append($titleDrop).append('<span><strong>Hint:</strong> Please note this will override Admin Default Commission or Category Specific commission for this particular product<span>');

				return $form;
				},
				
				buttons: [{
					label: 'Add',
					type:'submit',
					cssClass: 'btn-primary',
					//~ hotkey: 13, // Enter.
					action: function(dialogItself) {
						var comm = dialogItself.getData('field-title-drop').val()
						if(comm>=0 && comm<=100)
						{
							$.ajax({
								url:set_commroute+'/'+pro_id,
								type: "POST",
								data: {'_token': $('input[name=_token]').val(),'pro_id':pro_id,'product_commission':comm},
								dataType: 'json',
								success: function(response) {
								toastr[response.status](response.message, "Notifications");
								
									dialogItself.close();
									data();
								},

								});
						}
						else
						{
							alert("Please Enter Commission 0 to 100 %");
						}
						
				
					}
				}]
			});
		}
        
		function setasApprove(pro_id)
		{
				BootstrapDialog.show({
					title: 'Confirm Box',
					message: 'Are You Sure Want To Approve This Product',
					buttons: [{
						  label: 'Close',
					action: function(dialogItself){
						dialogItself.close();
					}
					}, {
					label: 'Approve',
					action: function(dialogItself) {
							$.ajax({
							url:approval_route+'/'+pro_id,
							type: "POST",
							data: {'_token': $('input[name=_token]').val()},
							dataType: 'json',
							success: function(response) {
							toastr[response.status]("Successfully Approved", "Notifications");
								data();
							},

							});
							dialogItself.close();
					}
					}]
				});
				
		}
		
		function data()
		{
			var table = $('#table1').DataTable({
											processing: true,
											serverSide: true,
											bDestroy: true,
											"order": [[ 0, "desc" ]],

											ajax: data_route,
											columns: [
											{ data: 'pro_id', name: 'pro_id' },
											
											{ data: 'image', name: 'image' ,render:function (data,type,row,meta){ return displayimage(data,type,row,meta)} },
											{ data: 'title', name: 'title' },
											
											{ data: 'sale_price', name: 'sale_price',render:function(data,type,row,meta){ return rupeesign(data,type,row,meta)} },
											//{ data: 'product_commission', name: 'product_commission',render:function (data,type,row,meta){ return commission(data,type,row,meta)} },
											//{ data: 'company_name', name: 'company_name',render:function(data,type,row,meta){ return sellerName(data,type,row,meta)} },
											{ data: 'add_date', name: 'add_date' },
											{ data: 'actions', name: 'actions', orderable: true, searchable: true }
											]
											});
											table.on( 'draw', function () {
											$('.livicon').each(function(){
											$(this).updateLivicon();
											});
											} );

		}
		
		//~ function commission(data,type,row,meta)
		//~ {
			//~ if(data!=0.00 && data!=null )
			//~ var comm = data+'%'+'<br>'+'Type: Product Specific';
			//~ else if(row.admin_comm_applicable!=0.00 && row.admin_comm_applicable!=null )
			//~ var comm =  row.admin_comm_applicable+'%'+'<br>'+'Type: Category Specific';
			//~ else
			//~ var comm = admin_comm+'%'+'<br>'+'Type: Admin Default ';
			
			//~ return comm;
		//~ }
		
		function rupeesign(data,type,row,meta)
		{
				return 'Rs.'+data;
		
		} 
		
	
