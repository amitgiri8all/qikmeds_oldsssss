//Data Table JS
// $(function() {
//     var table = $('#table1').DataTable({
//         processing: true,
//         serverSide: true,
//         bDestroy: true,

//         ajax: datatbleurl,
//         columns: [{
//                 data: 'id',
//                 name: 'id'
//             },
//             {
//                 data: 'name',
//                 name: 'name'
//             },
//             {
//                 data: 'code',
//                 name: 'code'
//             },
//             {
//                 data: 'description',
//                 name: 'description'
//             },
//             {
//                 data: 'created_at',
//                 name: 'created_at'
//             },
//             {
//                 data: 'action',
//                 name: 'action'
//             }
//         ]
//     });
// });
// table.on('draw', function() {
//     $('.livicon').each(function() {
//         $(this).updateLivicon();
//     });
// });

//Google Shape Js for Map
window.shape = null;

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 25.14901470,
            lng: 75.83543170
        },
        zoom: 5
    });
    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [ /*'marker', 'circle','polyline', 'rectangle',*/ 'polygon']
        },
        markerOptions: {
            icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
        },
        circleOptions: {
            fillColor: '#ffff00',
            fillOpacity: 1,
            strokeWeight: 5,
            clickable: false,
            editable: true,
            zIndex: 1
        }
    });
$zones ='';
    var zones = $zones;
    for (var i in zones) {
        var bermudaTriangle_all = new google.maps.Polygon({
            paths: zones[i]['points'],
            strokeColor: '#2aff20',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#2529ff',
            fillOpacity: 0.35
        });
        bermudaTriangle_all.setMap(map);
    }

    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
        bermudaTriangle.setMap(null);
        if (shape != null) {
            shape.overlay.setMap(null);
        }
        if (event.type == 'polygon') {
            shape = event;
            var radius = event.overlay.getPath().getArray();

            //console.log(JSON.stringify(radius));
            $("[name=point]").val(JSON.stringify(radius));
        }
    });
}


