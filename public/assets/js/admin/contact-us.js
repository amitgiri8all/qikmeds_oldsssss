$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'message',
                name: 'message'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
    table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});
});
