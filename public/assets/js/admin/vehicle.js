$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'registration_number',
                name: 'registration_number'
            },
            {
                data: 'vehicle_number',
                name: 'vehicle_number'
            }, 
             {
                data: 'vehicle_type',
                name: 'vehicle_type'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
    table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});
});
