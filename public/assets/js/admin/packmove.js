$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'phone_number',
                name: 'phone_number'
            }, 
            {
                data: 'from_address',
                name: 'from_address'
            },
            {
                data: 'to_address',
                name: 'to_address'
            }, 
             {
                data: 'comment',
                name: 'comment'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
});
table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});