$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'from_time',
                name: 'from_time'
            },
            {
                data: 'to_time',
                name: 'to_time'
            },
            {
                data: 'lock_time',
                name: 'lock_time'
            },
            {
                data: 'total_order',
                name: 'total_order'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
});
table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});