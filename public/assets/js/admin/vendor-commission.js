$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
         columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            { data: 'user.name', name: 'user.name' }, 
            { data: 'percent', name: 'percent' },
           
            { data: 'created_at', name: 'created_at' },
            { data: 'actions', name: 'actions',orderable: false, searchable: false }
        ]
    });
table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});
});
