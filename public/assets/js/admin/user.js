$("#search_btn").click(function() {
    var email = $('#email_id').val();
    var user_type = $('#user_type').val();
    $('#table1').DataTable().ajax.url(datatbleurl + "?email=" + email + "&user_type=" + user_type).load();
});
$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        language: {
            serverSide: true,
            bDestroy: true,
            scrollX: true,
/*            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
*/        },
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'image',
                name: 'image'
            },  
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'user_type',
                name: 'user_type'
            },
            {
                data: 'phone_number',
                name: 'phone_number'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'last_login',
                name: 'last_login'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });

    table.on('draw', function() {
        $('.livicon').each(function() {
            $(this).updateLivicon();
        });
    });

});
$(document).ready(function() {
    // For email Name
    $('#autocomplete-email-name').autocomplete({
        serviceUrl: datatbleurl2,
        params: {},
        autoSelectFirst: false,
        deferRequestBy: 200,
        noCache: true,
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
        },
        onSearchStart: function() {
            $('.auto_complete_loader').show();
        },
        onSearchComplete: function() {
            $('.auto_complete_loader').hide();
        },
        onSelect: function(suggestion) {
            $('#email_id').val(suggestion.data);
        },
        onHint: function(hint) {
            $('#autocomplete-email-name-x').val(hint);

        },
        onInvalidateSelection: function() {
            console.log('You selected: none');
        }
    });
    // search key up value clear
    $("#autocomplete-email-name").keyup(function() {
        $('#email_id').val('');
    });

});