$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'user.name',
                name: 'user.name'
            },
             {
                data: 'shop.name',
                name: 'shop.name'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'message',
                name: 'message'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
});
/*table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});*/