$("#search_data").click(function() {
    var link_type   = $('#link_type').val();
    var banner_type = $('#banner_type').val();
    $('#table1').DataTable().ajax.url(datatbleurl + "?link_type=" + link_type+ "&banner_type=" + banner_type ).load();
});
$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'image',
                name: 'image'
            },
            {
                data: 'banner_type',
                name: 'banner_type',
                orderable: false,
                searchable: false
            },
            {
                data: 'link_type',
                name: 'link_type'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }
        ]
    });
    
    table.on('draw', function() {
        $('.livicon').each(function() {
            $(this).updateLivicon();
        });
    });
});