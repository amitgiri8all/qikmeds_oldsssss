$("#search_category").click(function() {
   // var category_id = $('#category_id').val();
    //alert(category_id)
    $('#table1').DataTable().ajax.url("data?category_id" + category_id).load();
});



$("#search_vendor_category").click(function() {
    var category_id = $('#category_id').val();
    $('#table1').DataTable().ajax.url(datatbleurl + "?category_id=" + category_id).load();
});

$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'name',
                name: 'name'
            },   {
                data: 'user.name',
                name: 'user.name',
            },
            {
                data: 'image',
                name: 'image'
            },
            {
                data: 'sort_no',
                name: 'sort_no'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });

    table.on('draw', function() {
        $('.livicon').each(function() {
            $(this).updateLivicon();
        });
    });
});
$(document).ready(function() {
    // For player page
    $('#autocomplete-category-name').autocomplete({
        serviceUrl: datatbleurl2,
        params: {},
        autoSelectFirst: false,
        deferRequestBy: 200,
        noCache: true,
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            //console.log(suggestion)
            //alert(suggestion)
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
        },
        onSearchStart: function() {
            //alert('COMPLET')
            $('.auto_complete_loader').show();
        },
        onSearchComplete: function() {
            //alert('COMPLET')
            $('.auto_complete_loader').hide();
        },
        onSelect: function(suggestion) {
            //alert(suggestion.data)
            $('#category_id').val(suggestion.data);
        },
        onHint: function(hint) {
            //alert(hint)
            $('#autocomplete-category-name-x').val(hint);

        },
        onInvalidateSelection: function() {
            console.log('You selected: none');
        }
    });

    // search key up value clear
    $("#autocomplete-category-name").keyup(function() {
        //$('#category_id').val('');

    });

});