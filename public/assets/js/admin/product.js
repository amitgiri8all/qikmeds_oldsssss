$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'image',
                name: 'image'
            },
            {
                data: 'sku_code',
                name: 'sku_code'
            },
           /* {
                data: 'measurement_value',
                name: 'measurement_value'
            },  
            {
                data: 'measurement_class',
                name: 'measurement_class'
            },*/
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
});
table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});