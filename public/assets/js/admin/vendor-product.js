$("#search_data").click(function() {
    var user_id = $('#user_id').val();
    var status = $('#status').val();
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    $('#table1').DataTable().ajax.url(datatbleurl + "?user_id=" + user_id + "&status=" + status + "&from_date=" + from_date + "&to_date" + to_date).load();
});
$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'product.name',
                name: 'product_id',
            },   
            {
                data: 'user.name',
                name: 'user.name',
            },
            {
                data: 'price',
                name: 'price'
            },
            {
                data: 'qty',
                name: 'qty'
            },
            {
                data: 'offer.name',
                name: 'offer.name',
                orderable: false,
                searchable: false
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ],
         "columnDefs": [{
    "targets": '_all',
    "defaultContent": ""
}],
    });

    table.on('draw', function() {
        $('.livicon').each(function() {
            $(this).updateLivicon();
        });
    });
});