
/*============================================================*/
$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        language: {
            serverSide: true,
            bDestroy: true,
            scrollX: true,
/*            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
*/        },
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'phone_number',
                name: 'phone_number'
            },
            {
                data: 'user_type',
                name: 'user_type'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
             {
                data: 'actions',
                name: 'actions'
            },
            
        ]
    });

    table.on('draw', function() {
        $('.livicon').each(function() {
            $(this).updateLivicon();
        });
    });

});
