$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'offer_type',
                name: 'offer_type'
            },
            {
                data: 'offer_value',
                name: 'offer_value'
            },
            {
                data: 'from_time',
                name: 'from_time'
            },
            {
                data: 'to_time',
                name: 'to_time'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
    table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});
});
