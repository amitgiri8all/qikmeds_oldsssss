$(function() {
    var table = $('#table1').DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        scrollX: true,
        ajax: datatbleurl,
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'code',
                name: 'code'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'coupon_type',
                name: 'coupon_type'
            },
            {
                data: 'coupon_value',
                name: 'coupon_value'
            },
            {
                data: 'to_time',
                name: 'to_time'
            },
            {
                data: 'from_time',
                name: 'from_time'
            },
            {
                data: 'number_of_use',
                name: 'number_of_use'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'actions',
                name: 'actions'
            },
        ]
    });
});
table.on('draw', function() {
    $('.livicon').each(function() {
        $(this).updateLivicon();
    });
});