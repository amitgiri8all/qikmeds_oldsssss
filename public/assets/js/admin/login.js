// JavaScript Document
jQuery(function($) {

	$("#btn_submit").click(function(e) {
		e.preventDefault();
		var email = $("#email").val();

		$.ajax({
			url: url,
			type: 'post',
			data: {
				'_token': token,
				email: email
			},
			dataType: 'json',
			beforeSend: function() {
				$("#btn_submit").html('Validating...');
				$("#frm_login").find("button[type=submit]").attr("disabled", "disabled");

			},
			success: function(json) {
				console.log(json)

				// csrf token update
				if (json['email']) {
					$(".help-block").html('<strong>' + json['email'] + '</strong>');
					$("#btn_submit").html('Send');
					$("#frm_login").find("button[type=submit]").removeAttr("disabled");

				}

				if (json['faild']) {
					$(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> ' + json['faild'] + '</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();
					$('.help-block').hide();
					$("#btn_submit").html('Send');
					$("#frm_login").find("button[type=submit]").removeAttr("disabled");

					return false;
				}

				if (json['success']) {
					$(".main_error").html('<strong><i class="fa fa-check-circle"></i> ' + json['success'] + '</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-danger').addClass('alert-success').show();
					window.setTimeout(function() {
						window.location.href = json['redirect_url'];
					}, 3000);
				}

			},
			error: function(json) {
				$(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> Something Went Wrong Please Contact to administrator!</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();
			}
		});

	});
});



function start_loader() {
	$('#admin_loader').fadeIn('fast');
}

function stop_loader_form() {
	$('#admin_loader').fadeOut('fast');
}
$('body').on('click', '.close', function() {
	$(".main_error").html('').removeClass('alert-danger').hide();
	stop_loader_form();
});