//Allow only numeric values (numbers) in Textbox
$(document).ready(function() {
  $(".num").bind("keypress", function (e) {
    var keyCode = e.which ? e.which : e.keyCode
    if (!(keyCode >= 48 && keyCode <= 57)) {
      return false;
    }
  });
});
 $(document).ready(function(){
        $("#access_submit").attr('disabled','disabled');
    });
    $("[name=access_level_id]").change(function () {
        if($(this).val()== ''){
            $("#access_submit_div").hide();
            $("#data_continner").html('');
            $("#access_submit").attr('disabled','disabled');
            return false;
        }else{
            $("#access_submit_div").show();
            
             $("#access_submit").removeAttr('disabled');
        }
        $.ajax({
        url: route,
        type: 'POST',
        data: {
            '_token': token,access_level_id:$(this).val(),
        },
        dataType: "json",
        success: function(data) {
              $("#data_continner").html(data.data.html);
        },
        error: function(data) {
            alert('error')
        },
       
        });
    });

//Update Zone Status Js
$(document).ready(function(){
    $(document).on('click', '.status', function()
    {
        var id        = $(this).attr('data-status-id');
        var status_value   = $(this).attr('data-status-value');
        var that            = $(this);
        $.ajax({
            url         : statusurl,
            type        : 'POST',
            data        : {'_token': token,id:id,status_value:status_value},
            dataType    : 'json',
      beforeSend: function ()
      {
        $(that).text('Updating...');
      },
            success     : function(data) 
            {
              $('#table1').DataTable().ajax.reload();
              toastr[data.status]("Update Succesfully", "Zone Status");
            
            },
            error       : function(response)
            {
                 console.log(response);
            }
        });
    });  
//Update as default value in zone table
    $(document).on('click', '.is_default', function()
    {
        var id        = $(this).attr('data-id');
        var value   = $(this).attr('data-value');
        var that            = $(this);
        $.ajax({
            url         : defaulturl,
            type        : 'POST',
            data        : {'_token': token,id:id,value:value},
            dataType    : 'json',
      beforeSend: function ()
      {
        $(that).text('Updating...');
      },
            success     : function(data) 
            {
            $('#table1').DataTable().ajax.reload();
              toastr[data.status]("Update Succesfully", "Zone Default");
          },
            error       : function(response)
            {
                 console.log(response);
            }
        });
    });
}); 

$("#user_type").change(function(){
    if($(this).val()=='vendor'){
        $(".vendor").show();
    }else{
        $(".vendor").hide();
    }
});
// Image preview befor upload ================================
$("#upload_image").on('change', function()
{
  var upload_image_holder = $("#upload_image_holder");
  upload_image_holder.empty();
  upload_image_holder.hide();
  // Create object of FileReader
  var reader = new FileReader();
  reader.onload = function(e) {
    $("<img />", { 
    "src": e.target.result,
    "class": "thumb-image"
    }).appendTo(upload_image_holder);
  }
  upload_image_holder.show();
  reader.readAsDataURL($(this)[0].files[0]);
  
  var filename = $('#upload_image').val();
  if (filename.substring(3,11) == 'fakepath') {
    filename = filename.substring(12);
  } 
  $(".upload").html(filename);
    $('.img_padding_bottom').show();

});

// Image preview befor logo ================================
$("#logo_image").on('change', function()
{
  var logo_image_holder = $("#logo_image_holder");
  logo_image_holder.empty();
  logo_image_holder.hide();
  // Create object of FileReader
  var reader = new FileReader();
  reader.onload = function(e) {
    $("<img />", { 
    "src": e.target.result,
    "class": "thumb-image"
    }).appendTo(logo_image_holder);
  }
  logo_image_holder.show();
  reader.readAsDataURL($(this)[0].files[0]);
  
  var filename = $('#logo_image').val();
  if (filename.substring(3,11) == 'fakepath') {
    filename = filename.substring(12);
  } 
  $(".logo").html(filename);
    $('.img_padding_bottom').show();
});
//Open Close Search Tab
$(".triger").on("click", function(e){
    $(".search-form").slideToggle();
    if($(this).hasClass('expanded')){$('.triger').removeClass("expanded");}
    else{$('.triger').addClass("expanded");}
});
//Date Picker =================
$(document).ready(function () {
  $('#from_date').datetimepicker();
  $('#to_date').datetimepicker();
  
    $(".from_date").change(function (selected){
      alert("ok");
        var startDate = new Date($("#from_date").val());
        var endDate =  new Date($("#to_date").val());
        if (endDate < startDate){
            alert('End date can not be less than start date');
            $("#to_date").val('');
        }                 
    });
    $("#from_date").change(function (selected){
       alert("oksss");
        var startDate = new Date($("#from_date").val());
                alert(startDate)
        var endDate =  new Date($("#to_date").val());
        if (endDate < startDate){
            alert('End date can not be less than start date');
            $("#to_date").val('');
        }                 
    });
  
  $('#datetimepicker1').datetimepicker({
     format: 'YYYY-MM-DD HH:mm:ss',
     useCurrent:'day'
  });

  $('#datetimepicker2').datetimepicker({
     format: 'YYYY-MM-DD HH:mm:ss',
     useCurrent:'day'
  });
  
  //only date
  $('#datetimepicker_provider1').datetimepicker({
     format: 'YYYY-MM-DD',
     useCurrent:'day'
  });
});

/*
$(function () {
    $("#from_date").datetimepicker({
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#to_date").datetimepicker("option", "minDate", dt);
        }
    });
    $("#to_date").datetimepicker({
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#from_date").datetimepicker("option", "maxDate", dt);
        }
    });
});*/

  
  // Image preview befor license ================================
$("#license_image").on('change', function()
{
  var license_image_holder = $("#license_image_holder");
  license_image_holder.empty();
  license_image_holder.hide();
  // Create object of FileReader
  var reader = new FileReader();
  reader.onload = function(e) {
    $("<img />", { 
    "src": e.target.result,
    "class": "thumb-image"
    }).appendTo(license_image_holder);
  }
  license_image_holder.show();
  reader.readAsDataURL($(this)[0].files[0]);
  
  var filename = $('#license_image').val();
  if (filename.substring(3,11) == 'fakepath') {
    filename = filename.substring(12);
  } 
  $(".license").html(filename);
    $('.img_padding_bottom').show();
});


function start_loader()
{
   $('#admin_loader').fadeIn('fast');
}

function stop_loader_form()
{
   $('#admin_loader').fadeOut('fast');
}
