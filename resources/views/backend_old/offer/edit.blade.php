@extends('admin/layouts/default') @section('title') Offer Manager::CRM @stop @section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen" />
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<style type="text/css">
  .form-horizontal .form-group {
  margin-right: 0px !important;
   margin-left: 0px !important;
}
</style>
@stop {{-- Content --}} @section('content')
<section class="content-header">
  <h1>
    Offer Manager
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="/"> <i class="livicon" data-name="home" data-size="14" data-color="#000"></i> Offer Manager </a>
    </li>
    <li>Offer Manager</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
              data-hc="white"></i>
            Edit Offer
          </h3>
             <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{route('offer.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
        </div>
        <div class="panel-body">
        <!--<form method="post" action="{{ route('offer.edit.post',$data->id) }}" id="example-form" enctype="multipart/form-data">-->
      {!! Form::model($data,['route' => ['offer.edit.post',$data->id],'method'=>'post','class'=>'form-horizontal form-label-left validation','enctype'=>'multipart/form-data','id'=>'example-form']) !!}
            @csrf
            <!-- Access Level Status -->
            <div class="form-group">
              <label for="text">Offer Type </label>
              <div class="input-group col-xs-12">
                <select class="form-control" name="offer_type" id="type">
                  <option value="">Please Offer Type</option>
                  <option value="percentages" @if($data->offer_type=='percentages') {{ 'selected' }} @endif>Percentages (%)</option>
                  <option value="amount" @if($data->offer_type=='amount') {{ 'selected' }} @endif>Amount (00.0)</option>
                </select>
                {!! $errors->first('offer_type', '<span class="help-block">:message</span>') !!} 
              </div>
            </div>
            <!-- Offer Value -->
            <div class="form-group">
              <label for="text">Offer Value</label>
              <div class="input-group col-xs-12">
                <input type="text" name="offer_value" value="@if(isset($data->offer_value)){{ $data->offer_value}}@else{!! old('offer_value')  !!}@endif" class="form-control num" placeholder="Enter Offer Vlaue"> {!! $errors->first('offer_value', '<span class="help-block">:message</span>') !!} 
              </div>
            </div>
            <!-- From Time -->
            <div class="form-group">
              <label for="text">Start Date</label>
              <div class="input-group col-xs-12">
                <input type="date" value="@if(isset($data->from_time)){{ $data->from_time}}@else{!! old('from_time')  !!}@endif" name="from_time" class="form-control" placeholder="Select data"> {!! $errors->first('from_time', '<span class="help-block">:message</span>') !!} 
              </div>
            </div>
            <!-- To Time -->
            <div class="form-group">
              <label for="text">End Date</label>
              <div class="input-group col-xs-12">
                <input type="date" value="@if(isset($data->to_time)){{ $data->to_time}}@else{!! old('to_time') !!}@endif" name="to_time" class="form-control" placeholder="Select to time"> {!! $errors->first('to_time', '<span class="help-block">:message</span>') !!} 
              </div>
            </div>
            @foreach(config('translatable.locales') as $locale)
            <!-- Name -->
            <div class="form-group">
              <label for="text">Name {{$locale}}*</label>
              <div class="input-group col-xs-12">
              <!--   <input type="text" @if($locale=='ar' ) dir='rtl' @else dir='ltr' @endif class="form-control" lang="{{$locale}}" value="{{$data->name}}" name="name:{{$locale}}" placeholder="Enter Offer Name"> {!! $errors->first('name:'.$locale, '<span class="help-block">:message</span>') !!}  -->
              {!!  Form::text('name:'.$locale, null, array('placeholder' => 'Name','class' => 'form-control col-md-7 col-xs-12',  'dir'=>($locale=="ar" ? 'rtl':'ltr'), 'lang'=>$locale )) !!}
              {!! $errors->first('name:'.$locale, '<span class="help-block">:message</span>') !!}
              </div>
            </div>
            @endforeach
            <!-- Offer Image -->
            <div class="form-group">
              <label for="text">Image</label>
              <div class="input-group col-xs-12">
                <input type="file" class="form-control" value="{{old('image')}}" name="image" placeholder="Enter Offer Image"> {!! $errors->first('image', '<span class="help-block">:message</span>') !!} </span>
              </div>
            </div>
            <!-- Status -->
            <div class="form-group">
              <label for="text">Status</label>
              <div class="input-group col-xs-12">
                <select class="form-control" name="status" id="type">
                  <option value="">Please Status Type</option>
                      <option value="1" @if($data->status=='1') {{ 'selected' }} @endif>Active</option>
                      <option value="0" @if($data->status=='0') {{ 'selected' }} @endif>Inactive</option>
                </select>
                {!! $errors->first('status', '<span class="help-block">:message</span>') !!} 
              </div>
            </div>
            <div class="col-md-12 mar-10">
              <div class="col-xs-4 col-md-4"></div>
              <div class="col-xs-4 col-md-2">
                <button type="submit" class="btn btn-primary btn-block btn-md btn-responsive"> @lang('SAVE') </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- row-->
</section>
@stop @section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/ckeditor/js/config.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/editor.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/timezones.full.js') }}" type="text/javascript"></script>@stop