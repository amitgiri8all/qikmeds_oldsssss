@extends('backend/layouts/default')
@section('title')
Banner Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Banner Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Edit new Banner</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                      Edit Banner
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.banner.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
            <form method="post" class="ajax_form" id="banner" enctype="multipart/form-data">
                @csrf
                   <!-- Banner Status -->
                    <div class="form-group">
                     <label for="text">Banner Type</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="banner_type" id="type" value="{{$data->banner_type}}">
                             <option value="">Please Banner Type</option>
                             <option @if(($data->banner_type) == 'home') selected @endif  value="home">Banner Home</option>
                             <option @if(($data->banner_type)== 'ads') selected @endif value="ads">Banner Ads</option>
                          </select>
                         {!! $errors->first('banner_type', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>  

                   <!-- Banner title -->
                <div class="form-group">
                     <label for="text">Title</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="title" placeholder="banner title" value="@if(isset($data->title)){!! $data->title !!}@else{!! old('title') !!}@endif">
                        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- Banner Image -->
                   <div class="form-group">
                     <label for="text">Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" value="{{old('image')}}" name="image">
                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                     </div>
                       @php
                    if(!empty($data->image)){
                    $url= $data->image;
                    }else{
                    $url= $data->image;
                    }
                   @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
                  </div>

 <!-- Link Type -->
                  <div class="form-group">
                     <label for="validate-text">Link Type</label>
                     <div class="input-group col-xs-12">

                       <select class="form-control" name="link_type" id="link_type" value="{{$data->link_type}}">
                          <option value="">Please Status Type</option>
                          <option  value="external"  @if(($data->link_type) == 'external') selected @endif>External link</option>
                          <option value="internal"  @if(($data->link_type) == 'internal') selected @endif>Internal Link</option>
                       </select>  
                       {!! $errors->first('link_type', '<span class="help-block">:message</span>') !!}
 
                     </div>
                  </div>
              

                  <!-- Banner Status -->
                    <div class="form-group">
                     <label for="text">Status</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="status" id="type" value="{{$data->status}}" >
                             <option value="">Please Select Type</option>
                             <option value="1" @if(($data->status) == '1') selected @endif>Active</option>
                             <option value="0" @if(($data->status) == '0') selected @endif>Inactive</option>
                          </select>
                      </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('Update')
                        </button>
                     </div>
                  </div>

               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')

 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>                  

@stop