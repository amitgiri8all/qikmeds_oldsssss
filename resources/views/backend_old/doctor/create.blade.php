@extends('backend/layouts/default')
@section('title')
Doctor Manager::CRM
@stop
@section('header_styles')
<link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

<!-- <style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style> -->
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Doctor Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Doctor</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Doctor
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.doctor.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.doctor.add.post') }}" id="doctor" enctype="multipart/form-data">
                  @csrf
 
         <div class="row ">

         <div class="col-sm-6 form-group">
            <label for="name-f">First Name</label>
            <input type="text" class="form-control" value="{{old('first_name')}}" name="first_name" id="first_name" placeholder="Enter your first name." >
            {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}

         </div>

                
         <div class="col-sm-6 form-group">
            <label for="name-l">Last name</label>
            <input type="text" class="form-control" value="{{old('last_name')}}" name="last_name" id="last_name" placeholder="Enter your last name." >
            {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}

         </div>

         <div class="col-sm-6 form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control"  value="{{old('email')}}"  name="email" id="email" placeholder="Enter your email." >
            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}

         </div>    
         
         <div class="col-sm-6 form-group">
            <label for="email">Mobile Number</label>
            <input type="text" class="form-control"  value="{{old('mobile_number')}}"  name="mobile_number" id="mobile_number" placeholder="Enter your mobile_number." >
            {!! $errors->first('mobile_number', '<span class="help-block">:message</span>') !!}

         </div>   

         <div class="col-sm-6 form-group">
            <label for="email">Reg. No</label>
            <input type="text" class="form-control"  value="{{old('registration_number')}}"  name="registration_number" id="registration_number" placeholder="Enter your Reg. No">
         </div>

         <div class="col-sm-6 form-group">
            <label for="email">Total Experience</label>
            <input type="text" class="form-control num"  value="{{old('experience')}}" maxlength="2"  name="experience" id="experience" placeholder="Enter your experience." >
         </div> 

          <div class="col-sm-2 form-group">
            <label for="pass2">Pin Code</label>
            <input type="text" class="form-control num getdetails" value="{{old('pin_code')}}"  maxlength="6" value="" id="pin_code" name="pin_code" placeholder="Pincode">
          </div>   


         <div class="col-sm-2 form-group">
            <label for="pass2">City</label>
            <input type="text" class="form-control num" id="city" value="" name="city" placeholder="city" readonly="readonly">
          </div> 
 

         <div class="col-sm-2 form-group">
            <label for="pass2">State</label>
               <input type="text" class="form-control num" id="state" value="" name="state" placeholder="state" readonly="readonly">          
         </div>  
         <div class="col-sm-6 form-group">
               <label for="pass2">About Me</label>
               <textarea name="about_info" id="about_info" class="form-control"></textarea>         
         </div> 


         <h5>Doctor Eduction Details</h5>

         <div class="row" style="width:100%;">
                 <div class="col-sm-3 form-group">
                  <label for="pass2">Collage Or University Name</label>
                     <input type="text" class="form-control " id="college" value="" name="college[]" placeholder="college">
               </div> 

               <div class="col-sm-3 form-group">
                  <label for="pass2">Subject</label>
                     <input type="text" class="form-control " id="subject" value="" name="subject[]" placeholder="subject">
               </div>

               <div class="col-sm-2 form-group">
                  <label for="pass2">Starting Year's</label>
                  <select class="form-control" name="start_year_edu[]">
                     @for ($year = date('Y'); $year > date('Y') - 50; $year--)
                     <option value="{{$year}}">
                     {{$year}}
                     </option>
                     @endfor
                  </select>
               </div>   

               <div class="col-sm-2 form-group">
                  <label for="pass2">End Year's</label>
                    <select class="form-control" name="end_year_edu[]">
                           @for ($year = date('Y'); $year > date('Y') - 50; $year--)
                              <option value="{{$year}}">
                                {{$year}}
                              </option>
                           @endfor
                     </select>
               </div>    


               <div class="input-group col-sm-2 form-group">
               <label for="pass2"></label>
                  <a onclick="education_fields();" href="javascript:void(0)"><span class="input-group-text" style="background:green;height: 44px;margin-left: -10px;margin-top: 28px;padding: 14px; padding-top: 10px;"><i style="color:#fff; margin-top: 30px;" class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
                  </div>
               </div>
      
  
     
               <div id="education_fields" style="width: 100%;">

               </div>


<!-- ================================================================== -->


          <h5>Work & Experience</h5>

         <div class="row" style="width:100%;">
                 <div class="col-sm-4 form-group" >
                  <label for="pass2">Wokring Place Name</label>
                     <input type="text" class="form-control " id="woking_place_name" name="woking_place_name[]" placeholder="Work & Experience">
               </div>      
                     
               <div class="col-sm-3 form-group">
                  <label for="pass2">Starting Year's</label>
                  <select class="form-control" name="start_year_work[]">
                     @for ($year = date('Y'); $year > date('Y') - 50; $year--)
                     <option value="{{$year}}">
                     {{$year}}
                     </option>
                     @endfor
                  </select>
               </div>   
               <div class="col-sm-3 form-group" >
                  <label for="pass2">End Year's</label>
                    <select class="form-control" name="end_year_work[]">
                           @for ($year = date('Y'); $year > date('Y') - 50; $year--)
                              <option value="{{$year}}">
                                {{$year}}
                              </option>
                           @endfor
                     </select>
               </div>    
 
               <div class="input-group col-sm-2 form-group" style="margin-top: 30px;">
               <label for="pass2"></label>
                  <a onclick="education_fields2();" href="javascript::void(0)"><span class="input-group-text" style="background:green;height: 44px;margin-left: -10px;margin-top: 28px;padding: 14px; padding-top: 10px;">
   <i style="color:#fff;" class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
                  </div>
               </div>
      
 
     
               <div id="education_fields2" style="width: 100%;">
               </div>



               <!-- =================    <h5>Specializations</h5   ============ -->
               <h5>Specializations</h5>
               <!-- ok thanks -->

         <div class="row" style="width:100%;">
            <div class="col-sm-10 form-group">
                  <label for="pass2">Name</label>
                     <input type="text" class="form-control" name="specializations[]"  placeholder="Specializations Name">
               </div>  

          <div class="input-group col-sm-2 form-group">
               <label for="pass2"></label>
                  <a onclick="specializations();" href="javascript::void(0)"><span class="input-group-text" style="background:green;height: 44px;margin-left: -10px;margin-top: 28px;padding: 14px; padding-top: 10px;"><i style="color:#fff;margin-top: 30px" class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
                  </div>
         </div>
         <div id="specializations" style="width: 100%;">

         </div>

            <div class="col-sm-6 form-group">
            <label for="name-f">Profile Image</label>
            <input type="file" class="form-control" name="image" id="profile-img">
            <img src="" id="profile-img-tag" width="200px" />
         </div>

         <div class="col-sm-6 form-group">
            <label for="name-l">Your Signature Image</label>
            <input type="file" class="form-control" name="signature" id="signature">
            <img src="" id="signature-img-tag" width="200px" />

         </div>

 </div> 

           <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<!--  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script> --> 
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>



<script type="text/javascript">
   $('#profile-img-tag').hide();
   $('#signature-img-tag').hide();
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
             }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURLs(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                 $('#signature-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
      $('#profile-img-tag').show();
        readURL(this);
    });    
    $("#signature").change(function(){
      $('#signature-img-tag').show();
        readURLs(this);
    });
</script>



<!--     <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"></script>
 -->
<script type="text/javascript">
   $('#input-tags').tagsInput();
</script>


 <script type="text/javascript">
var room = 1;
function education_fields() {
    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
   divtest.setAttribute("class", "form-group removeclass"+room);
   var rdiv = 'removeclass'+room;
    divtest.innerHTML = `<div class='row' style='width:100%;'> <div class='col-sm-3 form-group'> <label for='pass2'>Collage Or University Name</label> <input type='text' class='form-control num' id='college[]' value='' name='college[]' placeholder='college'> </div><div class='col-sm-3 form-group'> <label for='pass2'>Subject</label> <input type='text' class='form-control num' id='subject[]' value='' name='subject[]' placeholder='subject[]'> </div><div class='col-sm-2 form-group'> <label for='pass2'>Starting Year's</label><select name='start_year_edu[]' class='form-control'>  
      @foreach(range(date('Y'),1950) as $date)
       <option value="{{$date}}">{{$date}}</option>
     @endforeach 
     </select> </div>
     <div class='col-sm-2 form-group'> <label for='pass2'>End Year's</label> <select name='end_year_edu[]'class='form-control'> @foreach(range(date('Y'),1950) as $date)
       <option value="{{$date}}">{{$date}}</option>
     @endforeach  </select></div><div class='input-group col-sm-2 form-group'> <label for='pass2'></label> <a href='javascript:void(0)' onclick='remove_education_fields(${room});'><span class='input-group-text' style='background:red;height: 44px;margin-left: -10px;margin-top: 28px;padding:14px;padding-top:10px;'><i style='color:#fff;margin-top:30px;' class='fa fa-minus-circle' aria-hidden='true'></i></span></a> </div></div></div>`;
    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
      $('.removeclass'+rid).remove();
   }

 </script>
<script type="text/javascript">
var room2 = 1;
function education_fields2() {
 
    room2++;
    var objTo = document.getElementById('education_fields2');
    var divtest = document.createElement("div");
   divtest.setAttribute("class", "form-group removeclass2"+room2);
   var rdiv = 'removeclass2'+room2;
    divtest.innerHTML = `<div class='row' style='width:100%;'> <div class='col-sm-4 form-group'> <label for='pass2'>Wokring Place Name</label> <input type='text' class='form-control num' id='woking_place_name' value='' name='woking_place_name[]' placeholder='woking_place_name'> </div><div class='col-sm-3 form-group'> <label for='pass2'>Starting Year's</label><select name='start_year_work[]' class='form-control'> 
       @foreach(range(date('Y'),1950) as $date)
       <option value="{{$date}}">{{$date}}</option>
       @endforeach 
      </select> </div><div class='col-sm-3 form-group'> <label for='pass2'>End Year's</label> <select name='end_year_work[]' class='form-control'> @foreach(range(date('Y'),1950) as $date)
       <option value="{{$date}}">{{$date}}</option>
     @endforeach  </select> </div><div class='input-group col-sm-2 form-group'> <label for='pass2'></label> <a href='javascript::void(0)' onclick='remove_education_fields22(${room2});'><span class='input-group-text' style='background:red;height: 44px;margin-left: -10px;margin-top: 28px;padding:14px;padding-top:10px;'><i style='color:#fff;margin-top:30px;' class='fa fa-minus-circle' aria-hidden='true'></i></span></a> </div></div></div>`;
    objTo.appendChild(divtest)
}
   function remove_education_fields22(rid) {
      $('.removeclass2'+rid).remove();
   }

 </script>
 <script type="text/javascript">
var room3 = 1;
function specializations() {
 
    room3++;
    var objTos = document.getElementById('specializations');
    var divtests = document.createElement("div");
   divtests.setAttribute("class", "form-group removeclass2"+room3);
   var rdisv = 'removeclass2'+room3;
    divtests.innerHTML = "<div class='row' style='width:100%;'> <div class='col-sm-10 form-group'> <label for='pass2'>Name</label> <input type='text' class='form-control num' id='specializations' value='' name='specializations[]' placeholder='Specializations Name'> </div><div class='input-group col-sm-2 form-group'> <label for='pass2'></label> <a href='javascript::void(0)' onclick='remove_specializations("+room3+");'><span class='input-group-text' style='background:red;height: 44px;margin-left: -10px;margin-top: 28px;padding:14px;padding-top:10px;'><i style='color:#fff;margin-top:30px;' class='fa fa-minus-circle' aria-hidden='true'></i></span></a> </div></div></div>";
    objTos.appendChild(divtests)
}
   function remove_specializations(rdisv) {
      $('.removeclass2'+rdisv).remove();
   }

 </script>
<script type="text/javascript">
   var delivery ="{{route('delivery-location.get-details.post')}}";

var token       = '{{ csrf_token() }}';
$('#loading').hide();
   $(document).ready(function(){
    $(document).on('keyup', '.getdetails', function()
    {
       //  alert('okkkk')
    var length =  $("#pin_code").val().length;
      if (length == 6) {
          $('select[name="name"]').empty();
           var pin_code        = $("#pin_code").val();
           // alert(pin_code)
           $.ajax({
               url         : delivery,
               type        : 'POST',
               data        : {'_token': token,pin_code:pin_code},
               dataType    : 'json',
               beforeSend: function() {
                 $('#loading').show();
               },
               success     : function(data) 
               {

                  $('select[name="name"]').empty();
                  $('#state').val(data.state);
                  $('#city').val(data.city);
                 $(data.AllState).each(function(index, el) {
                     $('select[name="name"]').append('<option>'+ el +'</option>');
                  });


                },
               complete: function(){
                $('#loading').hide();
               },
               error       : function(response)
               {
                $('#error').html('<div class="alert alert-danger" role="alert">The requested resource is not found</div>');
               }
           });
         }  

    });  
   });  

</script>
@stop
