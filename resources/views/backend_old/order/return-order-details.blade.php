@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
Order Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
 <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
  <style type="text/css">
     .badge-danger {
    color: #fff;
    background-color: #dc3545;
}.badge-success {
    color: #fff;
    background-color: #28a745;
}


  </style>
  <style media="screen">
  .noPrint{ display: block; }
  .yesPrint{ display: block !important; }
</style> 
<style media="print">
  .noPrint{ display: none; }
  .yesPrint{ display: block !important; }
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Order Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li class="active">Return Order Details</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">

   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               Return Order Details
            </h4>
              <div class="pull-right">
      <!--          <a href="javascript::void(0);" onclick="printThis()" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Print </a> -->
            </div>
            <div class="pull-right">
               <a href="{{route('admin.order.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> back </a>
<!--                <a href="javascript:void(0)" class="btn btn-sm btn-default" id="accepte_return"> back </a>
 -->            </div> 
          </div>
         <div class="panel-body">
             <div class="card" data-select2-id="9">
 
   
        <div class="row" data-select2-id="8">
         <div class="col-sm-12" data-select2-id="7">
            <div class="card collapsed-card" data-select2-id="6">
               <div class="table-responsive">
                  <table class="table table-hover box-body text-wrap table-bordered">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Product Image</th>
                           <th>Name</th>
                            <th class="product_price">Price</th>
                           <th class="product_qty">Quantity</th>
                           <th class="product_qty" style="background: #e61f1f;">Returnable Quantity</th>
                           <th class="product_total">Total price</th>
                           <th class="vendor_name">Docter Name</th>
                           <th class="vendor_name">Vendor Name</th>
                           <th class="vendor_name">Driver Name</th>
                           <th class="vendor_name">Action</th>
                         </tr>
                     </thead>
                     <tbody>
                        <?php // echo "<pre>"; print_r($order->OrderItem); die();?>
                      @foreach($order->OrderItem as $key=>$value) 
                     @php $view = URL::to("admin/product/product-details/$value->product_id");
                      @endphp  
                       @php $val = App\Helpers\Helper::get_vendor_product_name($value->vendor_id); @endphp
                        <tr>
                           <td>
                              {{++$key}}
                           </td>
                            <?php  $myArray = json_decode($value->data, true);
                           //  echo $myArray['image'];
                            ?>

                            <td>
                           @php
                           if(!empty($myArray['image'])){
                           $url = $myArray['image'];
                           }
                           else{
                           $url = asset('/public/assets/images/no-image.jpg');
                           }
                          @endphp
                     <img src="<?php echo $url; ?>" width="80" height="50" ></td>

                           
                           <td><a target="__blank" href={{$view}}>{{$value->vendor_id}}    {{$value->ProductOrderItem->medicine_name}}</a></td>
                           <td class="product_total item_id_55">₹ {{$value->ProductOrderItem->mrp}}</td>                           
                           <td class="product_qty">{{$value->quantity_order}}</td>
                           <td class="product_qty">{{$value->quantity_cancelled}}</td>
                           <td class="product_total item_id_55">₹ {{($value->ProductOrderItem->mrp)*$value->quantity_order}}</td>
                           <td>
                            <?php
                              $name_doc = App\Helpers\Helper::doctor_name($value->doctor_id);
                            ?>
                              {{isset($name_doc)?$name_doc:'Not accepted yet';}}
                           </td>
                           <td>
                              {{isset($val->name)?$val->name:'Not accepted yet';}}
                           </td> 
                           <td>
                            <?php
                              $name = App\Helpers\Helper::driver_name($value->driver_id);
                            ?>
                              {{isset($name)?$name:'Not accepted yet';}}
                           </td>
                           <td>
                            @if($value->is_return!=0)
                            <input type="hidden" name="order_id" value="{{$value->order_id}}" id="order_id" class="order_id">
                            <a class="btn btn-success" data-id='{{$value->ord_item_id}}' class="accepte_return" id="accepte_return" href="javascript:void(0)">
                              <i class="fa fa-check"></i> Accept</a>
                            @else
                            <a class="btn btn-info" href="javascript::void(0)"><i class="fa fa-check"></i> Accepted</a>

                            @endif
                          </td> 
                           

                        </tr>
                       @endforeach 
                     </tbody>
                
                  </table>
 

               </div>
            </div>
         </div>
      </div>
   </form>
   
</div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript">
var accepte_url="{{URL::to('admin/order/accepte_return_product')}}";
var token = '{{ csrf_token() }}';

 $(document).on('click', '#accepte_return', function() {
    var order_item_id = $(this).attr("data-id");
    var order_id = $('#order_id').val();
   // alert(order_id)
     event.preventDefault();
    $.ajax({
        url: accepte_url,
        type: "POST",
        data: {
            '_token': token,
            order_id: order_id,
            order_item_id: order_item_id
        },
        dataType: 'json',
        beforeSend: function() {
            $(".accepte_return").html('Validating...');
        },
        success: function(response) {
            //$(".accepte_return").html('Accepted');
            alert('successfully update')
            //toastr[response.status](response.message, "Order");
            setTimeout(function() {
                window.location.reload();
            }, 1000);
        }
    });
});

  </script>

@stop
