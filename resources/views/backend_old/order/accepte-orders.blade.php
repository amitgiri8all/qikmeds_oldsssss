@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
Order Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
 <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
  <style type="text/css">
     .badge-danger {
    color: #fff;
    background-color: #dc3545;
}.badge-success {
    color: #fff;
    background-color: #28a745;
}
  </style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Order Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li class="active">Order list</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">

    <!-- ============= For searching email from the list ============== -->
   <div class="wbox" style="display: none;">
      <a href="javascript:void(0);" class="triger"> 
      <i class="fa fa-plus-square" aria-hidden="true"></i> <i class="fa fa-minus-square" aria-hidden="true"></i>
      &nbsp; Looking for any specific order ? <span>Click here</span> to refine your search based on order.</a>
      <div class="row search-form au-search" style="display: none;">
          <div class="col-lg-3 col-md-3" style="display: none;">
            <label>Date</label>
            <div class="input-group date" id="from_date" data-date-format="YYYY-MM-DD">
               <input type="text" class="form-control from_date" name="from_date" id="from_date" value="">
               <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
         </div>

         <div class="col-lg-3 col-md-3" style="display: none;">
            <label>Date To</label>
            <div class="input-group date" id="to_date" data-date-format="YYYY-MM-DD">
               <input type="text" class="form-control to_date" name="to_date"  id="to_date"  value="">
               <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
         </div>
 

         <div class="form-group col-sm-3">
            <label for="text">Order Status</label>
              <div class="input-group col-xs-12">
                <select class="form-control order_status" name="order_status" id="order_status">
                  <option value="">Please Select Status</option>
                  <option value="accepted">Accepted</option>
                  <option value="new">New</option>
                  <option value="partially done">Partially Done</option>

                 </select>
              </div>
            </div>

 
               <div class="form-group col-sm-3">
                  <label for="text">Vendors</label>
                    <div class="input-group col-xs-12">
                      <select class="form-control vendor_id" name="vendor_id" id="vendor_id">
                        <option value="">Please Select vendor</option>
                        
                      </select>
                    </div>
                  </div>

                  


         <div class="form-group col-sm-4">
            <div class="input-group col-sm-6">
               <button type="submit" style="width:270px;" class="search_btn_list" id="search_btn" page-value="1"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
            </div>
         </div>

       </div>
   </div>
 <!-- ============= For searching email from the list ============== -->
 

   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               Order List
            </h4>
           <!--   <div class="pull-right">
               <a href="{{route('admin.coupon.add')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add New Coupon </a>
            </div> -->
          </div>
         <div class="panel-body">
            <table class="table table-bordered " id="serverside_datatable">
               <thead>
                  <tr class="filters">
                     <th>ID</th>
                     <th>Customer Name</th>
                     <th><i class="fa fa-shopping-cart" aria-hidden="true" title="SubTotal"></i></th>
                     <th><i class="fa fa-truck" aria-hidden="true" title="Shipping"></i></th>
                     <th><i class="fa fa-tags" aria-hidden="true" title="Discount"></i></th>
                     <th>Total</th>
                     <th><i class="fa fa-credit-card" aria-hidden="true" title="Payment method"></i></th>
                     <th>Status</th>
                     <th>Created At</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
 
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
 

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>
<script> 
$(document).ready(function(){
  var dataTable = $('#serverside_datatable').DataTable({
    'processing': true,
    'serverSide': true,
    //'searching': false, // Remove default Search Control
    'ajax': {
       'url':"{{ route('admin.order.accepted-orders-list') }}",
       'data': function(data){
        // $('#loading').show();
          // Read values
          var order_status = $('#order_status').val();
          var from_date = $('#from_date').val();
          var to_date = $('#to_date').val();
          //alert(order_status)
          // Append to data
          data.order_status = order_status;
          data.from_date = from_date;
          data.to_date = to_date;
       }
    },
    'columns': [
            {data: 'order_id', name: 'order_id'},
            {data: 'user_name', name: 'user_name', orderable: false, searchable: false},
            {data: 'sub_total', name: 'sub_total', orderable: false, searchable: false},
            {data: 'shipping_amount', name: 'shipping_amount', orderable: false, searchable: false},
            {data: 'discount_coupon_code', name: 'discount_coupon_code', orderable: false, searchable: false},
            {data: 'total_payed_amount', name: 'total_payed_amount', orderable: false, searchable: false},
            {data: 'payment_method', name: 'payment_method', orderable: false, searchable: false},

           {data: 'status', name: 'status'},
           {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
  });
  $('#search_btn').click(function(){
    //$('#loading').show();
    dataTable.draw();
  });
});




/*$(function () {
    var table = $('#serverside_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.order.list') }}",
        data: function(data){
          var gender = $('#searchByGender').val();
          data.searchByGender = gender;
         }
        },
        columns: [
            {data: 'order_id', name: 'order_id'},
            {data: 'user_name', name: 'user_name', orderable: false, searchable: false},
            {data: 'sub_total', name: 'sub_total', orderable: false, searchable: false},
            {data: 'shipping_amount', name: 'shipping_amount', orderable: false, searchable: false},
            {data: 'discount_coupon_code', name: 'discount_coupon_code', orderable: false, searchable: false},
            {data: 'total_payed_amount', name: 'total_payed_amount', orderable: false, searchable: false},
            {data: 'payment_method', name: 'payment_method', orderable: false, searchable: false},

           {data: 'status', name: 'status'},
           {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });*/
</script>
@stop
