@extends('backend/layouts/default')
@section('title')
Subscription Meta Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Subscription Meta Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.subscriptionmeta.list')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Subscription Meta Manager
         </a>
      </li>
      <li>View Subscription Meta Manager</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  View Subscription Meta
               </h3>
            <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
            
            <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" vendor="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            ID
                        </th>
                        <td>
                            {{ $data->id }}
                        </td>
                    </tr>
                        <tr>
                        <th>
                           Plan Type
                        </th>
                         @if(!empty($subscription_show->plan_name))
                            <td>{{$subscription_show->plan_name}}</td> 
                          @else
                            <td></td>
                          @endif    
                       
                    </tr>
                    <tr>
                        <th>
                            Type
                        </th>
                        <td>
                            {{$data->type}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Title
                        </th>
                        <td>
                           {{$data->title}}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Description
                        </th>
                        <td>
                           {{$data->description}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Price
                        </th>
                        <td>
                           {{$data->price}}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Price Type
                        </th>
                        <td>
                           {{$data->price_type}}
                        </td>
                    </tr>
                    <tr>
                      <th>
                          Image
                      </th>
                      <td>
                        @php
                     if(!empty($data->image)){
                     $url = $data->image;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" ></td>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        Created On
                      </th>
                      <td>
                        {{date('d F,Y',strtotime($data->created_at)); }}
                      </td>
                    </tr>

                </tbody>
            </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>

@stop

