@extends('backend/layouts/default')
@section('title')
Subscription Meta Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Subscription Meta Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Subscription Meta</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Subscription Meta
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.subscriptionmeta.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.subscriptionmeta.add.post') }}" id="subscriptionmeta" enctype="multipart/form-data">
                  @csrf

                     <!--  Subscription Plan -->
                   <div class="form-group col-sm-6">
                     <label for="link_type"> Subscription Plan</label>

                      <select class="form-control valid" id="subscription_plan_id" name="subscription_plan_id" aria-invalid="false" aria-describedby="link_type-error">

                      <option value="">Please Select Subscription Plan </option>  
                      @foreach($subscription_plan as $value)
                      <option value="{{$value->id}}" >{{$value->plan_name}}</option>
                      @endforeach

                      </select>
                      {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                      
                  </div> 

                   <!--  Type -->
                   <div class="form-group col-sm-6">
                     <label for="link_type"> Type</label>
                      <select class="form-control valid" id="type" name="type" aria-invalid="false" aria-describedby="link_type-error">
                      <option value="user" selected="selected">User</option>
                      <option value="vendor">Vendor</option>
                      </select>
                      {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                      
                  </div> 
 
                  <!-- Title -->
                   <div class="form-group col-sm-6">
                     <label for="text">Title</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('title')}}"  name="title" placeholder="title ">
                        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                 <!-- description -->
                   <div class="form-group col-sm-6">
                     <label for="text">Description</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('description')}}"  name="description" placeholder="Description">
                        {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- Price -->
                   <div class="form-group col-sm-6">
                     <label for="text">Price</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('price')}}" name="price" placeholder="Price">
                        {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Price type -->
                     <div class="form-group col-sm-6">
                     <label for="link_type"> Price type</label>
                      <select class="form-control valid" id="price_type" name="price_type" aria-invalid="false">
                      <option value="percentages" selected="selected">Percentages</option>
                      <option value="amount">Amount</option>
                      </select>
                      {!! $errors->first('price_type', '<span class="help-block">:message</span>') !!}
                      
                  </div>   
           
              <!--  Image -->
                  <div class="form-group col-sm-6">
                     <label for="text"> Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="image">
                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>     

                  
                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<!-- <script type="text/javascript">
            $('#us3').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                     locationNameInput: $('#address_name')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script> -->
@stop