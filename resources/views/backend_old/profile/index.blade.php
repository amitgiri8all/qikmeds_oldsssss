@extends('backend/layouts/default')
{{-- Page title --}}
@section('title')
Edit Profile
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/default/css/toastr.css') }}">
<!--end of page level css-->
<style type="text/css">
.vendor{display: none;}
.file-upload-filed{float:left; width:100%; border:1px solid #ced4da; position:relative; padding:0; height:40px; border-radius:3px; display:flex; align-items:center;}
.file-logo-filed{float:left; width:100%; border:1px solid #ced4da; position:relative; padding:0; height:40px; border-radius:3px; display:flex; align-items:center;}

.file_name{float:left; width:100%; line-height:38px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; color:#111; font-size:13px; font-weight:500; padding:0 8px; flex:2; display:flex; align-items:center;}
.file_name span{margin-left:8px; flex:2; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;}
.file_name figure{float:left; min-width:24px; width:24px; height:24px; margin:0;}
.file_name figure img{width:100%; float:left;}
.file-btn{float:left; width:100px; height:100%; background:#373434; color:#fff; font-size:13px; text-align:center; line-height:40px; cursor:pointer;}
.file-btn i{display:inline-block; font-size:16px; margin-right:6px;}
.file-btn input{position:absolute; top:0; left:0; width:100%; height:100%; opacity:0; cursor:pointer;}
img{padding:0; margin:0; max-width:100%; border:none;}

</style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Edit Profile
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{ route('admin.dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000">
         </i>
         Dashboard
         </a>
      </li>
      <li>Profile
      </li>
      <li class="active">Edit Profile
      </li>
   </ol>
</section>
<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-primary">
            <div class="panel-heading clearfix">
               <h3 class="panel-title">
                  <i class="livicon" data-name="user-add" data-size="20" data-c="#fff" data-hc="#fff" data-loop="true">
                  </i>
                  Edit Profile
               </h3>
               <span class="pull-right ">
               <!-- <button class="btn btn-danger" type="button" id="backbtn" data-url='{{ URL::previous()}}'>
               <i class="glyphicon glyphicon-chevron-left">
               </i>
               Back
               </button> -->
               <div class="pull-right" style="margin-top: -11px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
               </span>
            </div>
            <div class="panel-body">
               <!-- errors -->
               <div class="has-error">
               </div>
               <!--main content-->
               <!-- CSRF Token -->
               <ul class="nav nav-tabs faq-cat-tabs" id="tabs">
                  <li class="active">
                     <a href="#tab1" data-toggle="tab">User Profile
                     </a>
                  </li>
                  <li>
                     <a href="#tab2" data-toggle="tab">Change Password 
                     </a>
                  </li>
               </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab1">
                     <form action="{{ route('user.edit.post', 
                    Auth::guard('admin')->user()->id) }}" id="example-form" enctype="multipart/form-data" method="post" class="form-horizontal">
						@csrf
                        <h2 class="hidden">&nbsp;
                        </h2>
                        <div class="form-group">
                           <label for="first_name" class="col-sm-2 control-label">First Name 
                           </label>
                           <div class="col-sm-10">
                              <input id="first_name" name="name" type="text"
                                 placeholder="Name" class="form-control required"
                                 value="{!! old('name',Auth::guard('admin')->user()->name) !!}"/>
                              <div class="has-error">
                                 {!! $errors->first('name', '
                                 <span class="help-block">:message
                                 </span>') !!}
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="email" class="col-sm-2 control-label">Email 
                           </label>
                           <div class="col-sm-10">
                              <input id="email" name="email" placeholder="E-Mail" type="text" readonly
                                 class="form-control required email" value="{!! old('email',Auth::guard('admin')->user()->email) !!}" />
                              <div class="has-error">
                                 {!! $errors->first('email', '
                                 <span class="help-block">:message
                                 </span>') !!}
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="mobileno" class="col-sm-2 control-label">Mobile No 
                           </label>
                           <div class="col-sm-10">
                              <input id="mobileno" name="phone_number" placeholder="Mobile No" type="text"  class="form-control required email" value="{!! old('mobile_number',Auth::guard('admin')->user()->mobile_number) !!}"/>
                           {!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}

                           </div>
                        </div> 


                        {{-- <div class="form-group">
                           <label for="mobileno" class="col-sm-2 control-label">Gender 
                           </label>
                           <div class="col-sm-10">
									<select class="form-control" name="gender" id="type" >
									<option value="">Please Select Gender</option>
									<option value="male" @if(Auth()->user()->gender=='male') {{ 'selected' }} @endif>Male</option>
									<option value="female" @if(Auth()->user()->gender=='female') {{ 'selected' }} @endif>Female</option>
									</select>
									{!! $errors->first('gender', '<span class="help-block">:message</span>') !!}
						        </div>
                          </div> --}}


                        <div class="form-group">
                           <label for="mobileno" class="col-sm-2 control-label">DOB 
                           </label>
                           <div class="col-sm-10">
                              <input id="dob" name="dob" placeholder="dob" type="date"  class="form-control required email" value="{!! old('dob',Auth::guard('admin')->user()->dob) !!}"/>
                           </div>
                        </div> 

                         <div class="form-group">
                           <label for="address" class="col-sm-2 control-label">Address 
                           </label>
                           <div class="col-sm-10">
                              <input id="address" name="address" placeholder="Address" type="text" class="form-control required" value="{!! old('mobileno',Auth::guard('admin')->user()->address) !!}"/>
                           </div>
                        </div>

    					<div class="form-group">
                           <label for="address" class="col-sm-2 control-label">Image 
                           </label>
                           <div class="col-sm-10">

                              <div class="file-upload-filed">
                                <div class="file-btn">
                                  <i class="fa fa-upload"></i>Upload File
                                  <input type="file" name="upload_image" id="upload_image" class="form-control">
                                </div>
                                <div class="file_name upload">{{Auth::guard('admin')->user()->image}}</div>
                              </div>
                              <div class="file_name img_padding_bottom" style="display: block;">
                                <div id="upload_image_holder" style="width:100px;height:100px;" class="img_padding_bottom">
                                  <img src="{{ url('storage/app/public/upload/Thumbnail/'.Auth::guard('admin')->user()->image) }}" class="thumb-image">
                              </div>

                           </div>
                        </div>
                     </div>
                        <div class="form-group">
                           <div class="col-sm-10">
                              <center>
                                 <button type="submit" class="btn btn-primary saveedit finish" >Submit
                                 </button>
                              </center>
                           </div>
                        </div>
                     </form>
                  </div>
                  <div id="tab2" class="tab-pane fade">
                     <div class="row">
                        <div class="col-md-12 pd-top">
                           <form method="POST" action="{{route('change.password')}}" class="form-horizontal">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                              <div class="form-body">
                                 <div class="form-group">
                                    <label for="inputpassword" class="col-md-3 control-label">
                                    Password
                                    <span class='require'>*
                                    </span>
                                    </label>
                                    <div class="col-md-9">
                                       <div class="input-group">
                                          <span class="input-group-addon">
                                          <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000">
                                          </i>
                                          </span>
                                          <input type="password" id="password" name="password" placeholder="Password"
                                             class="form-control"/>
                                       </div>
                                     {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="inputnumber" class="col-md-3 control-label">
                                    Confirm Password
                                    <span class='require'>*
                                    </span>
                                    </label>
                                    <div class="col-md-9">
                                       <div class="input-group">
                                          <span class="input-group-addon">
                                          <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000">
                                          </i>
                                          </span>
                                          <input type="password" id="password_confirm"  name="password_confirm" placeholder="Confirm Password" class="form-control"/>
                                       </div>
                                       {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}

                                    </div>
                                 </div>
                              </div>
                              <div class="form-actions">
                                 <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-primary" id="change-password-btn">Submit
                                    </button>
                                    &nbsp;
                                    <input type="reset" class="btn btn-default hidden-xs" value="Reset">
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--row end-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/edituser.js') }}"></script>
<script src="{{ asset('assets/default/js/toastr.min.js') }}" type="text/javascript"></script>
<script>
   $('#backbtn').click(function(){
     var url=$(this).attr('data-url');
     window.location.href=url;
   }
                      );
</script>
@stop