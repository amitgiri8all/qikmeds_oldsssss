@extends('backend/layouts/default')
@section('title')
Role Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />

@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Role Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Role Manager
         
      </li>
         @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
    @endif
      <li>Add New Role</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Add New Role
               </h3>
            <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
             <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
      
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
        <form action="{{ route("roles.store") }}" method="POST" enctype="multipart/form-data">
           @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($role) ? $role->name : '') }}" required>
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                </p>
            </div>
            <div class="form-group {{ $errors->has('permission') ? 'has-error' : '' }}">
                <label for="permission">Permissions*
                @if($errors->has('permission'))
                    <em class="invalid-feedback">
                        {{ $errors->first('permission') }}
                    </em>
                @endif
                <p class="helper-block">
                   
                </p>
            </div>
            <style>
            #treeview-checkbox-demo > ul,li{
                list-style:none;
            }
            </style>
            <div id="treeview-checkbox-demo">
            <ul>
            @php
                $permissioinmenus = [];
            @endphp
                @foreach(App\Models\Links::getParentMenu() as $parent)

                <li>
                   <i class="fa {{$parent->icon}}"></i> &nbsp;&nbsp;<b>{{$parent->name}}</b>
                 <ul>
                @foreach(App\Models\Links::getSubMenu($parent->id) as $submenu)
                    <li class="{{ request()->is($submenu->link) ? 'active' : '' }}">
                    @php
                     @$managerPermission = explode(',',@$role->permissions);
                    @endphp
                        <input type="checkbox" name="manager_permission[{{$parent->id}}][]" value="{{$submenu->id}}" {{ (isset($role) && in_array($submenu->id,$managerPermission)) ? 'checked' : '' }}> {{$submenu->name}}
                        <ul>
                            @foreach(App\Models\Links::getActionMenu($parent->id) as $action)
                                <li class="">
                                    <input type="checkbox" name="action_permission[{{$submenu->id}}][]" value="{{$action->id}}"  {{ (isset($actionarr) && in_array($action->id,$actionarr)) ? 'checked' : '' }}> {{$action->showing_name}}
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach

                 </ul>
                </li>
                @endforeach
            </ul> 
        </div>

             <div>
                <!-- <input class="btn btn-danger" type="submit" value="Save"> -->
                <button type="submit"  class="btn btn-primary " value="Save">
                        @lang('SAVE')
                        </button>
            </div>
        </form>

        </div>
        </div>
      </div>
    </div>
  </div>
</section>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


@stop
