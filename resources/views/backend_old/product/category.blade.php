<style type="text/css">
   .daredevel-tree li span.daredevel-tree-anchor{
      cursor: default;
    position: absolute;
    /* top: 1px; */
    left: -16px;
   }
</style>
<div class="panel panel-primary" style="border-radius:0;">
   <!--<div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        Category Information
      </h3>
              
      </div>-->
   <div class="col-sm-12">
      <h3 class="inner-heading"> Category Information </h3>
   </div>
   <div class="panel-body border">
      <div id="tree">
@if(!empty($id))
        <form method="POST" id="cat_form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      @foreach($allCategories as $cat)
            <ul>
               <li>
                  <input type="checkbox" name="categorys[]" value="{{ $cat->id }}" id="{{'myCheckbox'.$cat->id}}"  class="mycheck" data-id={{ $cat->id }} 
            @if(!empty($cat_data)) 
            @foreach($cat_data as $cat_datas)
            {{ $cat_datas->category_id == $cat->id ?       'checked="checked"' : '' }}
            @endforeach()   
            @endif
            />
                  <span style="font-size:12px">{{$cat->category_name}}</span>
                @foreach($cat->sub_categorys as $sub_cat)
                  <ul>
                      <li>
                        <input type="checkbox" name="categorys[]" value="{{ $sub_cat->id }}" id="myCheckbox" class="mycheck" data-id={{ $sub_cat->id }} 
                @if(!empty($cat_data)) 
                @foreach($cat_data as $cat_datas)
                {{ $cat_datas->category_id == $sub_cat->id ?       'checked="checked"' : '' }}
                @endforeach()   
                @endif
                />
                        <span style="font-size:12px">{{$sub_cat->category_name}}</span>
                     </li>     
                       
                  </ul>
                  @endforeach() 
            </ul>
          @endforeach()

      </div>
   </div>
   </form>   
       @endif
                        
</div>