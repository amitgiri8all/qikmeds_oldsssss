<div class="panel panel-primary" style=" border-radius:0;">
   <!--<div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        General Information
      </h3>
      </div>-->
   <div class="col-sm-12">
      <h3 class="inner-heading"> Genral Information</h3>
   </div>
   <div class="panel-body">
      <form method="post" id="example-form" class="general_form"  action="{{ route('admin.product.add.post') }}">
           @csrf

         <div class="form-group">
            <label>Product Name</label>
            <div class="input-group col-sm-12">
               <input type="text" class="form-control" name="title" value="{!! old('title') !!}" 
                  placeholder="Enter Product Name" required>
            </div>
            {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
         </div>
         <input type="hidden" name="form_name" value="general">
         <div class="form-group">
            <label for="validate-number">Short Description</label>
                <textarea class="form-control" name="short_description" id="validate-number"
                  placeholder="Short Description" required>{!! old('short_description') !!}</textarea>
          </div>

         <div class="panel panel-success">
            <div class="panel-heading">
               <div class="text-muted bootstrap-admin-box-title editor-clr">
                  <i class="livicon" data-name="thermo-down" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                  Description
               </div>
            </div>
            <div class="bootstrap-admin-panel-content">
               <textarea name="description" id="ckeditor_full" >{!! old('description') !!}</textarea>
            </div>
            {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
         </div>
         <div class="row">
            <div class="col-sm-4">
               <div class="form-group">
                  <label>SKU</label>
                  <div class="input-group col-sm-12">
                     <input type="text" class="form-control" name="sku" value="{!! old('sku') !!}" 
                        placeholder="Enter SKU" required>
                  </div>
                  {!! $errors->first('sku', '<span class="help-block">:message</span>') !!}
               </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label for="">Status</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="status" id="validate-select" required>
                        <option value="">Please select</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                     </select>
                     </span>
                  </div>
                  {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
               </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label for="">Featured</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="is_featured" id="is_featured" >
                        <option value="">Please select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                     </select>
                  </div>
                  {!! $errors->first('is_featured', '<span class="help-block">:message</span>') !!}
               </div>
            </div>
         </div>
         <div class="row">

            <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-text">MRP</label>
                  <div class="input-group col-sm-12">
                     <input  type="number" step="0.01" min="0" id="price" class="form-control price" name="price" value="{!! old('price') !!}" placeholder="Enter Price" required>
                  </div>
                  {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
               </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-phone">Sale Price</label>
                  <div class="input-group col-sm-12">
                     <input  type="number" step="0.01" id="sale_price" min="0" value="" class="form-control price" name="sale_price"  placeholder="Enter Price" >
                    
                  </div>
                </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-phone">Discount In %</label>
                  <div class="input-group col-sm-12">
                     <input  type="text" name="product_discount" id="discount" value="" class="form-control" placeholder="Auto Fill" required>
                  </div>
               </div>
            </div>

         </div>
         <div class="row">
            
               <div class="col-sm-4">
               <div class="form-group">
                  <label for="">Deal Of The Day</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="is_featured" id="is_featured" >
                        <option value="">Please select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                     </select>
                  </div>
                  {!! $errors->first('is_featured', '<span class="help-block">:message</span>') !!}
               </div>

            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-select">Brand</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="brand" id="brand" >
                        <option value="">Please Select</option>
                         @foreach($brand_list as $key=>$value)
                           <option value="{{$value->id}}">{{$value->brand_name}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-select">Set As Home</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="set_as_home" id="set_as_home" >
                        <option value="">Please select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                     </select>
                  </div>
                  {!! $errors->first('set_as_home', '<span class="help-block">:message</span>') !!}
               </div>
            </div>
         </div>

         <div class="col-md-12 mar-10">
            <div class="col-xs-4 col-md-4"></div>
            <div class="col-xs-4 col-md-2">
               <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
             Save
            </div>
         </div>
      </form>
   </div>
</div>