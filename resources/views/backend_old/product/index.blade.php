@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
Product Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
  <style type="text/css">
     .badge-danger {
    color: #fff;
    background-color: #dc3545;
}.badge-success {
    color: #fff;
    background-color: #28a745;
}
  </style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Product Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li class="active">Product list</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">

     <!-- ============= For searching medicine from the list ============== -->
    <div class="wbox" style="display:none;">
       <a href="javascript:void(0);" class="triger"> 
       <i class="fa fa-plus-square" aria-hidden="true"></i> <i class="fa fa-minus-square" aria-hidden="true"></i>
       &nbsp; Looking for any specific medicine ? <span>Click here</span> to refine your search based on medicine.</a>
       <div class="row search-form au-search" style="display: none;">
          <!-- medicine Name -->
          <div class="form-group col-sm-4">
             <label for="text">Medicine name</label>
             <div class="input-group col-sm-6">
                <input class="autocomplete_main form-control" class="form-control" type="text" name="medicine_name" id="autocomplete-medicine-name" placeholder="Typing medicine name and select..." value="" autocomplete="off" style="width: 324px;" />
                <input class="autocomplete_sub form-control" type="text" name="medicine_name" id="autocomplete-medicine-name-x" disabled="disabled" value=""/>
                <input  type="hidden" name="medicine_id" id="medicine_id"  value=""/> 
                <span class="auto_complete_loader" style="display: none;"><i class="fa fa-spinner fa-spin fa-1x fa-fw margin-bottom"></i></span>
             </div>
          </div>
          
          <div class="form-group col-sm-4">
             <div class="input-group col-sm-6">
                <button type="button" class="search_btn_list" id="search_btn" page-value="1"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
             </div>
          </div>
       </div>
    </div>
  <!-- ============= For searching medicine from the list ============== -->

   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               Product List
            </h4>
             <div class="pull-right">
               <a href="{{route('admin.product.import')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-upload"></span> Bulk Upload Product </a>

               <a href="{{route('admin.product.add')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add New Product </a> 
               <a href="{{route('admin.faq-product.add')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add Product Faq </a>
            </div>
          </div>
         <div class="panel-body">
            <table class="table table-bordered " id="serverside_datatable">
               <thead>
                  <tr class="filters">
                     <th>ID</th>
                     <th>Medicine name</th>
                     <th>Image</th>
                     <th>Prescription</th>
                     <th>Type of sell</th>
                     <th>MRP</th>
                      <th>Created On</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
 
<script type="text/javascript" src="{{ asset('assets/js/jquery.autocomplete.js') }}"></script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>
<script> 




      var datatbleurl2= "{!! route('admin.product.list.search') !!}";

$(function () {
      var datatbleurl= "{!! route('admin.product.list') !!}";

   $("#search_btn").click(function() {
    var medicine = $('#medicine_id').val();
     $('#serverside_datatable').DataTable().ajax.url(datatbleurl + "?medicine=" + medicine).load();
});
    var table = $('#serverside_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: datatbleurl,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'medicine_name', name: 'medicine_name'},
            {data: 'image', name: 'image'},
            {data: 'prescription', name: 'prescription'},
            {data: 'type_of_sell', name: 'type_of_sell'},
            {data: 'mrp', name: 'mrp'},
              {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });

$(document).ready(function() {
    // For medicine Name
    $('#autocomplete-medicine-name').autocomplete({
        serviceUrl: datatbleurl2,
        params: {},
        autoSelectFirst: false,
        deferRequestBy: 200,
        noCache: true,
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
        },
        onSearchStart: function() {
            $('.auto_complete_loader').show();
        },
        onSearchComplete: function() {
            $('.auto_complete_loader').hide();
        },
        onSelect: function(suggestion) {
            $('#medicine_id').val(suggestion.data);
        },
        onHint: function(hint) {
            $('#autocomplete-medicine-name-x').val(hint);

        },
        onInvalidateSelection: function() {
            console.log('You selected: none');
        }
    });
    // search key up value clear
    $("#autocomplete-medicine-name").keyup(function() {
        $('#medicine_id').val('');
    });

});
</script>
@stop
