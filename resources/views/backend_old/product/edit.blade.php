@extends('backend.layouts.default')
{{-- Web site Title --}}
@section('title')
Product Manager::CRM
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/default/tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"/>
<link href="{{ asset('assets/css/jquery.tree.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
<style type="text/css">
   input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Product Manager    
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{ route('admin.dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
        Dashboard
         </a>
      </li>
      <li>Product Manager</li>
      <li class="active">
         Edit
      </li>
   </ol>
</section>
<!-- Main content -->





<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Edit Products
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.product.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>

            <div class="panel-body">
               <form method="post" class="ajax_form" id="ajax_form" enctype="multipart/form-data">
                   @csrf

                   <!-- medicine_name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Medicine name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->medicine_name)){!! $data->medicine_name !!}@else{!! old('medicine_name') !!}@endif" name="medicine_name" placeholder="medicine name">
                        {!! $errors->first('medicine_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
   <!-- type_of_sell -->
                   <div class="form-group col-sm-4">
                     <label for="text">Type of sell</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->type_of_sell)){!! $data->type_of_sell !!}@else{!! old('type_of_sell') !!}@endif" name="type_of_sell" placeholder="Type of sell">
                        {!! $errors->first('type_of_sell', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
   <!-- medicine_name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Salt</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->salt)){!! $data->salt !!}@else{!! old('salt') !!}@endif" name="salt" placeholder="salt">
                        {!! $errors->first('salt', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
   <!-- medicine_name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Therapeutic Class</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->therapeutic_class)){!! $data->therapeutic_class !!}@else{!! old('therapeutic_class') !!}@endif" name="therapeutic_class" placeholder="Therapeutic Class">
                        {!! $errors->first('therapeutic_class', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> 
                    <!-- chemical_class -->
                   <div class="form-group col-sm-4">
                     <label for="text">Chemical class</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->chemical_class)){!! $data->chemical_class !!}@else{!! old('chemical_class') !!}@endif" name="chemical_class" placeholder="Chemical Class">
                        {!! $errors->first('chemical_class', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

            <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-select">Manufacturer</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="manufacturer_id" id="{{$data->manufacturer_id}}">
                        <option value="">Please Manufacturer Name</option>
                         @foreach($manufacturer as $key=>$value)
                           <option value="{{$value->id}}" @if(($data->manufacturer_id) ==$value->id) selected @endif>{{$value->manufacturer_name}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
    <!-- chemical_class -->
                   <!-- <div class="form-group col-sm-4">
                     <label for="text">MRP ₹</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('mrp')}}" name="mrp" placeholder="MRP ₹">
                        {!! $errors->first('mrp', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
 -->

       <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-text">MRP</label>
                  <div class="input-group col-sm-12">
                     <input  type="number" step="0.01" min="0" id="price" class="form-control price" name="mrp" value="@if(isset($data->mrp)){!! $data->mrp !!}@else{!! old('mrp') !!}@endif" placeholder="Enter Price" >
                  </div>
                  {!! $errors->first('mrp', '<span class="help-block">:message</span>') !!}
               </div>
            </div>
            <div class="col-sm-4">
               <div class="form-group">
                  <label for="validate-phone">Sale Price</label>
                  <div class="input-group col-sm-12">
                     <input  type="number" step="0.01" name="sale_price" id="sale_price" min="0" value="@if(isset($data->sale_price)){!! $data->sale_price !!}@else{!! old('sale_price') !!}@endif" class="form-control price"   placeholder="Enter Price" >
                    
                  </div>
                </div>
            </div>
            <div class="col-sm-2">
               <div class="form-group">
                  <label for="validate-phone">Discount In %</label>
                  <div class="input-group col-sm-12">
                     <input  type="text" name="product_discount" id="discount" value="@if(isset($data->discount)){!! $data->discount !!}@else{!! old('discount') !!}@endif" class="form-control" placeholder="Auto Fill" readonly="readonly">
                  </div>
               </div>
            </div>
             <div class="col-sm-2">
               <div class="form-group">
                  <label for="validate-phone">Product Qty</label>
                  <div class="input-group col-sm-12">
                     <input  type="text" name="qty" id="qty" value="@if(isset($data->qty)){!! $data->qty !!}@else{!! old('qty') !!}@endif" class="form-control" placeholder="Product Qty" >
                  </div>
               </div>
            </div>

               <div class="col-sm-3">
               <div class="form-group">
                  <label for="">Deal Of The Day</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="deal_of_the_day" id="{{$data->deal_of_the_day}}" >
                        <option value="">Please select</option>
                        <option value="Yes" @if(($data->deal_of_the_day) =="Yes") selected @endif  >Yes</option>
                        <option value="No" @if(($data->deal_of_the_day) =="No") selected @endif >No</option>
                     </select>
                  </div>
                  {!! $errors->first('deal_of_the_day', '<span class="help-block">:message</span>') !!}
               </div>
              </div> 


               <div class="col-sm-3">
               <div class="form-group">
                  <label for="validate-select">Set As Home</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="set_as_home" id="{{$data->set_as_home}}" >
                        <option value="">Please select</option>
                        <option value="Yes" @if(($data->set_as_home)=="Yes") selected @endif >Yes</option>
                        <option value="No" @if(($data->set_as_home)=="No") selected @endif>No</option>
                     </select>
                  </div>
                  {!! $errors->first('set_as_home', '<span class="help-block">:message</span>') !!}
               </div>
            </div> 

              <div class="col-sm-3">
               <div class="form-group">
                  <label for="validate-select">Is Featured</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="is_featured" id="{{$data->is_featured}}" >
                        <option value="">Please select</option>
                        <option value="Yes" @if(($data->is_featured)=="Yes") selected @endif >Yes</option>
                        <option value="No" @if(($data->is_featured)=="No") selected @endif>No</option>
                     </select>
                  </div>
                  {!! $errors->first('is_featured', '<span class="help-block">:message</span>') !!}
               </div>
            </div>

            <!-- Product Expiry -->
           <div class="form-group col-sm-3">
             <label for="text">Product Expiry</label>
             <div class="input-group col-xs-12">
                <input type="Date" class="form-control" id="datepicker" name="product_expiry_date" placeholder="Product Expiry "  value="@if(isset($data->product_expiry_date)){!! $data->product_expiry_date !!}@else{!! old('product_expiry_date') !!}@endif">
                {!! $errors->first('product_expiry_date', '<span class="help-block">:message</span>') !!}
             </div>
          </div>


              <div class="col-sm-3">
               <div class="form-group">
                  <label for="validate-select">Brand</label>
                  <div class="input-group col-sm-12">
                     <select class="form-control" name="brand" id="brand" value="{{$data->brand}}" >
                        <option value="">Please Select</option>
                         @foreach($brand_list as $key=>$value)
                           <option value="{{$value->id}}" @if(($data->brand)==$value->id) selected @endif>{{$value->brand_name}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div> 

         <div class="col-sm-3">
            <div class="form-group">
               <label for="validate-select">Habit forming</label>
               <div class="input-group col-sm-12">
                  <select class="form-control" name="habit_forming" id="habit_forming" value="{{$data->habit_forming}}">
                        <option value="">Please select</option>
                        <option value="Yes" @if(($data->habit_forming)=="Yes") selected @endif>Yes</option>
                        <option value="No" @if(($data->habit_forming)=="No") selected @endif>No</option>
                  </select>
               </div>
            </div>
         </div> 

          <div class="col-sm-3">
            <div class="form-group">
               <label for="validate-select">Prescription</label>
               <div class="input-group col-sm-12">
                  <select class="form-control" name="prescription" id="prescription">
                        <option value="">Please select</option>
                        <option value="1" @if(($data->prescription)=="1") selected @endif >Yes</option>
                        <option value="0" @if(($data->prescription)=="0")  selected @endif >No</option>
 
                  </select>
               </div>
            </div>
         </div>

        <!-- Access Level Status -->
        <div class="form-group col-sm-3">
         <label for="text">Status</label>
         <div class="input-group col-xs-12">
              <select class="form-control" name="status" id="type" value="{{$data->status}}" >
                 <option value=''>Please Status Type</option>
                 <option value='1' @if(($data->status)==1) selected @endif>Active</option>
                 <option value='0' @if(($data->status)==0) selected @endif>Inactive</option>
              </select>
             {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
         </div>
      </div>

        <div class="clearfix"></div>
        <!-- Access Level Status -->
        <div class="form-group col-sm-6">
         <label for="text">Categories </label>
         <?php if(!empty($category_data)){ ?>
         <div class="input-group col-xs-12">
              <select class="form-control" name="category_id" id="category_id" value="{{$category_data->category_id}}">

                 <option value="">Please Status Type</option>
                 @foreach($allCategories as $key=>$value)
                   <option value="{{$value->id}}" @if(($category_data->category_id)==$value->id) selected @endif>{{$value->category_name}}</option>
                 @endforeach
              </select>
             {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
         </div>
       <?php }else{ ?>
        <div class="input-group col-xs-12">
              <select class="form-control" name="category_id" id="category_id" value="">

                 <option value="">Please Status Type</option>
                 @foreach($allCategories as $key=>$value)
                   <option value="{{$value->id}}">{{$value->category_name}}</option>
                 @endforeach
              </select>
             {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
         </div>

      <?php  } ?>
      </div>

        <!-- Access Level Status -->
        <div class="form-group col-sm-6">
         <label for="text">Sub Category</label>
         <div class="input-group col-xs-12">
              <select class="form-control" id="my-select2" name="sub_category[]" multiple>
             
              </select>
             {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
         </div>
        </div>


              <div class="col-sm-12">
               <div class="form-group">
                  <label for="validate-select">Alternate medicines</label>
                  <div class="input-group col-sm-12">
                    <textarea class="form-control" name="alternate_medicines" 
                  placeholder="Alternate Medicines" >@if(isset($data->alternate_medicines)){!! $data->alternate_medicines !!}@else{!! old('alternate_medicines') !!}@endif</textarea>
                  </div>
               </div>
            </div>

              <div class="col-sm-12">
               <div class="form-group">
                  <label for="validate-select">Side effects</label>
                  <div class="input-group col-sm-12">
                    <textarea class="form-control" name="side_effects" 
                  placeholder="Side effects" >@if(isset($data->side_effects)){!! $data->side_effects !!}@else{!! old('side_effects') !!}@endif</textarea>
                  </div>
               </div>
            </div>

              <div class="col-sm-12">
               <div class="form-group">
                  <label for="validate-select">How to use</label>
                  <div class="input-group col-sm-12">
                    <textarea class="form-control" name="how_to_use" 
                  placeholder="how to use" >@if(isset($data->how_to_use)){!! $data->how_to_use !!}@else{!! old('how_to_use') !!}@endif</textarea>
                  </div>
               </div>
            </div>
              <div class="clearfix"></div>
              <!-- Description -->
               <div class="form-group ">
                 <label for="text">Description</label>
                 <div class="input-group col-xs-12">
                     <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="description">
                       @if(isset($data->description)){!! $data->description !!}@else{!! old('description') !!}@endif
                     </textarea>
                     {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                 </div>
              </div>

                <!-- Key Benefits -->
                   <div class="form-group ">
                     <label for="text">Key Benefits</label>
                     <div class="input-group col-xs-12">
                        <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="key_benefits">@if(isset($data->key_benefits)){!! $data->key_benefits !!}@else{!! old('key_benefits') !!}@endif</textarea>
                         {!! $errors->first('key_benefits', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Directions For Use -->
                   <div class="form-group ">
                     <label for="text">Directions For Use</label>
                     <div class="input-group col-xs-12">
                        <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="directions_for_use">@if(isset($data->directions_for_use)){!! $data->directions_for_use !!}@else{!! old('directions_for_use') !!}@endif</textarea>
                          {!! $errors->first('directions_for_use', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                    <!-- Safety Information -->
                   <div class="form-group ">
                     <label for="text">Safety Information</label>
                     <div class="input-group col-xs-12">
                        <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="safety_information">@if(isset($data->safety_information)){!! $data->safety_information !!}@else{!! old('safety_information') !!}@endif</textarea>
                         {!! $errors->first('safety_information', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Other Information -->
                   <div class="form-group ">
                     <label for="text">Other Information</label>
                     <div class="input-group col-xs-12">
                        <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="other_information">@if(isset($data->other_information)){!! $data->other_information !!}@else{!! old('other_information') !!}@endif</textarea>
                         {!! $errors->first('other_information', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Product Highlights -->
                   <div class="form-group ">
                     <label for="text">Product Highlights</label>
                     <div class="input-group col-xs-12">
                        <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="product_highlights">@if(isset($data->product_highlights)){!! $data->product_highlights !!}@else{!! old('product_highlights') !!}@endif</textarea>
                         {!! $errors->first('product_highlights', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>





 
  <div class="col-sm-12">
             <!--   <div class="form-group">
                  <label for="validate-select">image</label>
                  <div class="input-group col-sm-12">

                  <div class="field" align="left">
                  <input type="file" id="files" name="files[]" multiple />

                  </div>
               </div>
            </div> -->


         <h5>Product Image  Details</h5>


        <?php if(count($image_data) == 0){ ?> 
<div class="row" style="width:100%;">
                 <div class="col-sm-3 form-group">
                  <label for="pass2">Image</label>
                     <input type="file" class="form-control " id="image" value="" name="image[]" placeholder="Image">
               </div> 

               <div class="col-sm-3 form-group">
                  <label for="pass2">Caption</label>
                     <input type="text" class="form-control " id="caption" value="" name="caption[]" placeholder="caption">
               </div>

                <div class="col-sm-3 form-group">
                  <label for="pass2">Order</label>
                     <input type="number" class="form-control " id="order" value="" name="order[]" placeholder="order">
               </div>

                <div class="radio col-sm-3 form-group" style="
                      margin-top: 25px;">
                 <input type="radio" id="female-rb" name="set_primary" value=1 />
                 <label for="female-rb">Set Primary</label>
              </div>
  
      <!--          <div class="col-sm-3 form-group">
                   <label for="pass2">Set Primary</label>
                  <input type="radio" value="set_primary" id="set_primary" name="set_primary">
               </div> -->
                
              


               <div class="input-group col-sm-2 form-group">
               <label for="pass2"></label>
                  <a onclick="image_fields();" href="javascript:void(0)"><span class="input-group-text" style="background:green;height: 44px;margin-left: -10px;margin-top: 28px;padding: 14px; padding-top: 10px;"><i style="color:#fff; margin-top: 30px;" class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
                  </div>
               </div>
        <?php }else{ 
        $i = 100;
         foreach ($image_data as $key => $value) {
        ?>
        <!-- <input type="hidden" name="image_id[]" value="{{$value->pro_img_id}}"> -->

         <div class="row removeclass<?= $i; ?>" style="width:100%;">
                 <div class="col-sm-3 form-group">
                  <label for="pass2">Image </label>
                     <input type="file" class="form-control " id="image"  name="new_image[]" placeholder="Image" value="{{$value->image }}"image_array_key="<?= $key; ?>">
                     <input type="hidden" name="old_image[]" value="{{$value->image}}">
                     <input type="hidden" name="new_image_array_key[]">
                      @php
                    if(!empty($value->image)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$value->image);
                    }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                    }
                   @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
               </div> 

               <div class="col-sm-3 form-group">
                  <label for="pass2">Caption</label>
                     <input type="text" class="form-control " id="caption" value="{{$value->caption}}" name="caption[]" placeholder="caption">
               </div>

                <div class="col-sm-3 form-group">
                  <label for="pass2">Order</label>
                     <input type="number" class="form-control " id="order" value="{{$value->order}}" name="order[]" placeholder="order">
               </div>

                <div class="radio col-sm-3 form-group" style="
                      margin-top: 25px;">
                 <input type="radio" id="female-rb{{$key}}i" name="set_primary" value="
                 {{$value->set_primary}}" @if(($value->set_primary) == 'Yes') checked @endif/>
            
                 <label for="female-rb{{$key}}i">Set Primary</label>
              </div>
  

               <div class="input-group col-sm-2 form-group">
               <label for="pass2"></label>
               <?php if($key == 0){ ?>
                  <a onclick="image_fields();" href="javascript:void(0)"><span class="input-group-text" style="background:green;height: 44px;margin-left: -10px;margin-top: 28px;padding: 14px; padding-top: 10px;"><i style="color:#fff; margin-top: 30px;" class="fa fa-plus-circle" aria-hidden="true"></i></span></a>
               <?php }else{ ?>
                  <a href='javascript:void(0)' onclick='remove_image_fields(<?= $i; ?>);'><span class='input-group-text' style='background:red;height: 44px;margin-left: -10px;margin-top: 28px;padding:14px;padding-top:10px;'><i style='color:#fff;margin-top:30px;' class='fa fa-minus-circle' aria-hidden='true'></i></span></a>
               <?php } ?>
                  </div>
               </div>

             <?php  $i++;}}?>

  
     
               <div id="image_fields" style="width: 100%;">

               </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-4">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('UPDATE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>



<section class="content" style="display:none;">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                  <strong>Create Product:</strong> <span> &nbsp;  &nbsp;    </span>
               </h3>
               <div class="pull-right" style="margin-top: -42px">
                  <a href="{{ URL::previous()}}" class="btn btn-sm btn-danger"  style="margin-bottom:-42px;"><span class="btn-label">
                  <i class="glyphicon glyphicon-chevron-left"></i>
                  </span><span style="font-size:13px;margin-left:8px">Back</span></a>
               </div>
            </div>
            <div class="panel-body modal-panel">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Tabbable-Panel Start -->
                     <div class="tabbable-panel">
                        <!-- Tabbablw-line Start -->
                        <div class="tabbable-line">
                           <!-- Nav Nav-tabs Start -->
                           <ul class="nav nav-tabs ">
                              <li class="active">
                                 <a data-toggle="tab" href="#general">
                                 General </a>
                              </li>
                               <li class="@if(isset($id)=='')disabled @endif">
                                 <a data-toggle="tab"   href="@if(isset($id)) #Tag @endif">
                                 Tag </a>
                              </li>
                              <li class="@if(isset($id)=='')disabled @endif">
                                 <a  data-toggle="tab"  href="@if(isset($id)) #MetaInformation @endif">
                                 Meta Information </a>
                              </li>
                              <li class="@if(isset($id)=='')disabled @endif">
                                 <a  data-toggle="tab" href="@if(isset($id)) #Image @endif">
                                 Image </a>
                              </li>
                              <li class="@if(isset($id)=='')disabled @endif">
                                 <a  data-toggle="tab" href="@if(isset($id)) #Invantory @endif">
                                 Inventory </a>
                              </li>
                              <li class="@if(isset($id)=='')disabled @endif">
                                 <a data-toggle="tab" href="@if(isset($id)) #Category @endif">
                                 Category </a>
                              </li> 
                             
                           </ul>
                           <!-- //Nav Nav-tabs End -->
                           <!-- Tab-content Start -->
                           <div class="tab-content">
                              <div id="general" class="tab-pane active">
                                   @include('backend/product/general')
                              </div>
                              <div id="Search" class="tab-pane">
                              </div>
                              <div id="Tag" class="tab-pane">
                                     @include('backend/product/tag')
                              </div>
                              <div id="MetaInformation" class="tab-pane" >
                                  @include('backend/product/metainfo')
                              </div>
                              <div id="Image" class="tab-pane">
                                  @include('backend/product/image')
                              </div>          
                              <div id="Image" class="tab-pane">
                                  @include('backend/product/image')
                              </div>
                            
                              <div id="Invantory" class="tab-pane">
                                  @include('backend/product/inventory')
                              </div> 

                             <div id="Category" class="tab-pane">
                                  @include('backend/product/category')
                              </div>
                              
                              <!-- Tab-content End -->
                           </div>
                           <!-- //Tabbable-line End -->
                        </div>
                        <!-- Tabbable_panel End -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- content -->
@stop
@section('footer_scripts')
<script></script>
 
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"
   type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"
   type="text/javascript"></script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}"
   type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<!--   <script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript"></script>
  <script  src="{{ asset('assets/js/product.js') }}"  type="text/javascript"></script> -->

  <script  src="{{ asset('assets/default/js/seller.js') }}"  type="text/javascript"></script>
  <script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript"></script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" ></script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript"></script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript"></script>
<script  src="{{ asset('assets/js/bootstrap-dialog.min.js') }}"  type="text/javascript"></script>
<script  src="{{ asset('assets/js/jquery.tree.min.js') }}"  type="text/javascript"></script>
 <script  src="{{ asset('assets/default/tagsinput/bootstrap-tagsinput.min.js') }}"  type="text/javascript"></script>


  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script type="text/javascript" src="{{ asset('assets/default/js/priceinventory.js') }}"></script>



<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>
<script>

    $(document).ready(function() {
                $('select[name="category_id"]').on('change', function() {
                    var cat_id = $(this).val();
                    if(cat_id) {
                        $.ajax({
                            url: "{!! url('admin/product/get-sub-category') !!}/"+cat_id,
                            type: "GET",
                            dataType: "json",
                            success:function(response) {
                                $('select[id="my-select2"]').empty();
                                $.each(response.data, function(index, value) {
                                    $('select[id="my-select2"]').append('<option value='+ value.id +'>'+ value.category_name +'</option>');
                                });


                            }
                        });
                    }else{
                        $('select[id="my-select2"]').empty();
                    }
                });
            }); 
          
</script>

<script type="text/javascript">
 
$(document).ready(function() {
    $("#my-select").select2();
    $("#my-select2").select2();
});



   $(function () {
            $("#price,#sale_price").change(function () {
                 //debugger;
                var price = parseFloat($("#price").val());
                var sale_price = parseFloat($("#sale_price").val());
                $("#discount").val(Math.round((price-sale_price)/(price)*100));
            })
        });
  <?php
  if(!empty($id)){
   $ids = $id;
  }else{
   $ids = "";
  }
  
  ?> 


/*  var tagdataurl = '{{ URL::to('admin/product/'.$ids.'/tagshow') }}';

 var imagedataurl = '{{ URL::to('admin/product/'.$ids.'/imageshow') }}';
*/ // var imagedestroy = '{{ URL::to('admin/product/imagedestroy') }}';


$('body').delegate('.mycheck', 'click', function() {
   alert('ok fine')
    var id = $(this).data('id');
     alert(id);

    //~ var categorys = [];
    //~ $('input[type="checkbox"]:checked').each(function() {
    //~ categorys.push($(this).val());
    //~ });

    if ($(this).is(':checked')) {
        var myurl = '{{ URL::to('admin/product/'.$ids.'/cat_add')}}';

    } else {
        var myurl = '{{ URL::to('admin/product/'.$ids.'/cat_remove')}}';

    }
    $.ajax({
        url: myurl,
        type: "post",
        dataType: 'json',
        data: $('#cat_form').serialize(),
        success: function(data) {
            console.log(data);
            toastr[data.status](data.message, "Notifications");
        }
    });
});


$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#files").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove image</span>" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
      console.log(files);
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});

</script>


 <script type="text/javascript">
var room = 1;
function image_fields() {
    room++;
    var objTo = document.getElementById('image_fields')
    var divtest = document.createElement("div");
   divtest.setAttribute("class", "form-group removeclass"+room);
   var rdiv = 'removeclass'+room;
    divtest.innerHTML = "<div class='row' style='width:100%;'> <div class='col-sm-3 form-group'> <label for='pass2'>Image</label> <input type='file' class='form-control num' id='image' value='' name='image[]' placeholder='image'> </div><div class='col-sm-3 form-group'> <label for='pass2'>Caption</label> <input type='text' class='form-control num' id='caption[]' value='' name='caption[]' placeholder='caption'> </div><div class='col-sm-3 form-group'> <label for='pass2'>Order</label><input type='number' class='form-control num' id='order[]' value='' name='order[]' placeholder='order'> </div><div class='radio col-sm-3 form-group' style='margin-top: 25px;'><input type='radio' id='female-rb"+room+"' name='set_primary' value='"+room+"' /><label for='female-rb"+room+"'>Set Primary</label></div><div class='input-group col-sm-2 form-group'> <label for='pass2'></label> <a href='javascript:void(0)' onclick='remove_image_fields("+room+");'><span class='input-group-text' style='background:red;height: 44px;margin-left: -10px;margin-top: 28px;padding:14px;padding-top:10px;'><i style='color:#fff;margin-top:30px;' class='fa fa-minus-circle' aria-hidden='true'></i></span></a> </div></div></div>";
    objTo.appendChild(divtest)
}
   function remove_image_fields(rid) {
      $('.removeclass'+rid).remove();
   }

 </script>

 @stop