<!DOCTYPE html>
<html>
   <head>
      <title>Login</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- global level css -->
      <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
      <!-- end of global level css -->
      <!-- page level css -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/login.css') }}" />
      <link href="{{ asset('assets/vendors/iCheck/css/square/blue.css') }}" rel="stylesheet"/>
             <link rel="icon" type="image" href="{{ asset('public/assets/images/ib-favicon.ico') }}" sizes="128x128"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

      <!-- end of page level css -->
      <style type="text/css">
         .btn-success .login {
         background: ##921616 !important;
         border-color: #921616 !important;
         }
        label {
 display:inline-block;
 max-width:100%;
 margin-bottom:5px;
 font-weight:700;
 color: #f2f1f0 !important;
} 
 .help-block {
          color: #f2f1f0 !important;
        }
      </style>
   </head>
   <body>

      <div class="container">
         <div class="row vertical-offset-100">
            <!-- Notifications -->
            <div class="col-sm-6 col-sm-offset-3  col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
@if ($message = Session::get('error'))
<div class="alert alert-danger alert-dismissable margin5" style="background: #921616; position: absolute; z-index: 9999; color: #fff; width: 527px; height: -33px; margin-top: -63px;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error:</strong> {{ $message }}
</div>
@endif
@if ($message = Session::get('success'))
<div class="alert alert-danger alert-dismissable margin5" style="background: #2b7b2ba6; position: absolute; z-index: 9999; color: #fff; width: 527px; height: -33px; margin-top: -63px; ">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Success:</strong> {{ $message }}
</div>
@endif

  <!-- ============== Message Section ======================= -->
      <div class="alert main_error" style="display:none; width: 526px; margin-top: -55px;">s</div>
      <!-- ============== Message Section ======================= -->
            


               <div id="container_demo">
                  <a class="hiddenanchor" id="toregister"></a>
                  <a class="hiddenanchor" id="tologin"></a>
                  <a class="hiddenanchor" id="toforgot"></a>
                  <div id="wrapper">
                     <div id="login" class="animate form" style="background: rgb(146, 22, 22);">
                        <form method="post" role="form" id="frm_login">
                           {{ method_field('POST') }}
                           <h3 class="white_bg">
                              <img src="{{ asset('public/assets/images/logo.png') }}" width="250px" alt="{{config('constants.admin.title')}}">
                           </h3>
                           <!-- CSRF Token -->
                           @csrf
                           <div class="form-group">
                                <label style="margin-bottom:0px;" for="email" class="uname control-label"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                                E-mail
                                </label>
                                <input id="email" name="email" type="email" placeholder="E-mail" value="{!! old('email') !!}"/>
                                  <span class="help-block"></span>
                                <div class="col-sm-12">
                              </div>
                           </div>
                           <div class="form-group">
                              <label>
                                <a href="{{route('/')}}" id="forgot-password">Back to Login</a>
                              </label>
                           </div>
                           <p class="login button">
                            <center> <button type="submit" class="btn btn-default form-controler" id="btn_submit" style="width: 93%;  margin-left: -20px;">Send</button></center>
                           </p>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- global js -->
      <script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
      <!-- Bootstrap -->
      <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
      <!--livicons-->
      <script src="{{ asset('public/assets/js/admin/forgot-password.js') }}" type="text/javascript"></script>
      <!-- end of global js -->
      <script type="text/javascript">
            var url= "{!! route('admin.forgot-password.post') !!}";
            var token = '{{ csrf_token() }}';

      </script>

   </body>
</html>