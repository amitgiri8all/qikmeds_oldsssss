<!DOCTYPE html>
<html>
   <head>
      <title>Login</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- global level css -->
      <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
      <!-- end of global level css -->
      <!-- page level css -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/login.css') }}" />
      <link href="{{ asset('assets/vendors/iCheck/css/square/blue.css') }}" rel="stylesheet"/>
<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/images/favicon.ico')}}"/>
 
      <!-- end of page level css -->
      <style type="text/css">
         .btn-success .login {
         background: ##921616 !important;
         border-color: #921616 !important;
         }
        label {
 display:inline-block;
 max-width:100%;
 margin-bottom:5px;
 font-weight:700;
 color: #f2f1f0 !important;
} 
 .help-block {
          color: #f2f1f0 !important;
        }
      </style>
   </head>
   <body>

      <div class="container">
         <div class="row vertical-offset-100">
            <!-- Notifications -->
            <div class="col-sm-6 col-sm-offset-3  col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
@if ($message = Session::get('error'))
<div class="alert alert-danger alert-dismissable margin5" style="background: #921616; position: absolute; z-index: 9999;color: #fff;width: 527px;height: -33px;margin-top: -63px;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error:</strong> {{ $message }}
</div>
@endif

               <div id="container_demo">
                  <a class="hiddenanchor" id="toregister"></a>
                  <a class="hiddenanchor" id="tologin"></a>
                  <a class="hiddenanchor" id="toforgot"></a>
                  <div id="wrapper">
                     <div id="login" class="animate form" style="background: #87dadc;">
                        <form action="{{ route('admin.login.post') }}" method="post" role="form">
                           {{ method_field('POST') }}
                           <h3 class="white_bg">
                              <img src="{{asset('public/frontend/images/logo.png')}}" width="250px" alt="{{config('constants.admin.title')}}">
                           </h3>
                           <!-- CSRF Token -->
                           @csrf
                           <div class="form-group {{ $errors->first('email', 'has-error') }}">
                              <label style="margin-bottom:0px;" for="email" class="uname control-label"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                              E-mail
                              </label>
                              <input id="email" name="email" type="email" autocomplete="off" placeholder="E-mail" value="{!! old('email') !!}"/>
                              <div class="col-sm-12">
                                 {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                              </div>
                           </div>
                           <div class="form-group {{ $errors->first('password', 'has-error') }}">
                              <label style="margin-bottom:0px;" for="password" class="youpasswd"> <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                              Password
                              </label>
                              <input id="password" name="password" autocomplete="off" type="password" placeholder="eg. X8df!90EO" />
                              <div class="col-sm-12">
                                 {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                              </div>
                           </div>
                           <div class="form-group" style="display: none;">
                              <label>
                                <a href="#" id="forgot-password">Forgot your password?</a>
                              </label>
                           </div>
                           <p class="login button">
                              <input type="submit" value="Login" class="btn" />
                           </p>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
       <!-- global js -->
      <script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
      <!-- Bootstrap -->
      <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
      <!--livicons-->
      <script src="{{ asset('assets/js/raphael-min.js') }}"></script>
      <script src="{{ asset('assets/js/livicons-1.4.min.js') }}"></script>
      <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
      <script src="{{ asset('assets/js/pages/login.js') }}" type="text/javascript"></script>
      <!-- end of global js -->
   </body>
</html>