@extends('backend/layouts/default')
{{-- Page title --}}
@section('title')
Sub Category Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type="text/css">
.inner_pages #wrapper { padding-left:300px; }
.wbox:after { display:block; content:""; clear:both; }
.wbox { background-color:#fff; -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.12); -moz-box-shadow: 0 2px 5px rgba(0,0,0,.12); box-shadow: 0 2px 5px rgba(0,0,0,.12); padding:15px; margin-bottom:20px; }
.search-form{ display:none; margin-top:15px;}
/* form */
.triger .fa-minus-square{ display:none;}
.triger.expanded .fa-plus-square{ display:none;}
.triger.expanded .fa-minus-square{ display:inline-block;}

.align_center{text-align: center !important;}
.autocomplete_main{position: absolute; z-index: 2; background: transparent;}
.autocomplete_sub{ color: #9C9C9C; position: absolute; background: transparent; z-index: 1;}
.autocomplete-suggestion{height: 30px;background: white;border: 1px solid #e2e2e2;padding: 5px;width: calc(100% + 2px);}
input.autocomplete_main, input.autocomplete_sub{
   display: block;
    height: 38px;
  padding: 8px 12px; min-width:300px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: transparent;
  background-image: none;
  border: 1px solid #e8e8e8;
  border-radius: 4px;}
.search_btn { border:none; border-radius:3px; background-color:#fcaf17; padding:8px 10px;font-weight:normal; color:#fff; font-size:14px; line-height:20px; margin-left: -216px;
    margin-top: 21px;}
/* table default */
.au-search input.autocomplete_main, .au-search input.autocomplete_sub{min-width: 100%; width:100%;}
.au-search input.autocomplete_main{position:relative;}
.au-search input.autocomplete_sub{opacity:0;}

.autocomplete-suggestions .autocomplete-suggestion.autocomplete-selected{background: #e8e8e8;}
.autocomplete-suggestions .autocomplete-suggestion{background: #f4f4f4;}

.autocomplete-suggestion {display: block; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;}
.autocomplete-block .autocomplete_sub{position: unset;  border: none;}
.form-group .auto_complete_loader{position: absolute;  top: 34px; left: 2px;}

</style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Sub Category Manager</h1>
   <ol class="breadcrumb">
      <li>
          <a href="{{ route('dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Sub Category Manager</li>
      <li class="active">Sub Category</li>
   </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">

  <!-- ============= For searching category from the list ============== -->
   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-body">
            <table class="table table-bordered " id="serverside_datatable">
               <thead>
                  <tr class="filters">
                     <th>ID</th>
                     <th>Category Name</th>
                     <th>Created On</th>
                  
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
 
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
 

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>
<script> 
$(document).ready(function(){
  var dataTable = $('#serverside_datatable').DataTable({
    'processing': true,
    'serverSide': true,
    //'searching': false, // Remove default Search Control
    'ajax': {
       'url':"{!! route('admin.category.sub-category.list.data',$id) !!}",
       'data': function(data){
       }
    },
    'columns': [
            {data: 'id', name: 'id'},
            {data: 'category_name', name: 'category_name', orderable: false, searchable: false},
           {data: 'created_at', name: 'created_at'},
        
        ]
  });
  $('#search_btn').click(function(){
    //$('#loading').show();
    dataTable.draw();
  });
});

</script>
@stop
