@extends('backend/layouts/default')
@section('title')
Category Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Category Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Edit  category</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Edit  category
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.category.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" action="{{ route('admin.category.edit.post',$data->id) }}" id="example-form" enctype="multipart/form-data">
                  @csrf

                    <!-- Access Level Status -->
                  <div class="form-group">
                   <label for="text">Select category</label>
                   <div class="input-group col-xs-12">
                        <select class="form-control" name="parent_id">
                          <option value="0">==ROOT==</option>
                        @foreach($categories as $category)
                        <option @if($data->id==$category->id) {{ 'selected' }} @endif  value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach 
                        </select>
                    </div>
                  </div>
                 
                  <!-- Category Name -->
                   <div class="form-group">
                     <label for="text">Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->category_name)){!! $data->category_name !!}@else{!! old('category_name') !!}@endif" name="category_name" placeholder="Category name" required>
                        {!! $errors->first('category_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- Category Image -->
                  <div class="form-group">
                     <label for="text">Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="image">
                        {!! $errors->first('image', '<span class="help-block" required>:message</span>') !!}
                        </span>
                     </div>
                     @php
                     if(!empty($data->image)){
                   
                   $url = $data->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                   @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
                  </div>
                  <!-- Category Image -->
                  <div class="form-group">
                     <label for="text">Is Parent</label>
                     <div class="input-group col-xs-12">
                        <input type="checkbox" class="largerCheckbox" value="1" name="is_parent" {{  ($data->is_parent == 1 ? ' checked' : '') }}>
                        </span>
                     </div>
                  </div>

                  <!-- Sort No Category -->
                  <div class="form-group">
                     <label for="text">Sort No</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control num" value="@if(isset($data->sort_no)){!! $data->sort_no !!}@else{!! old('sort_no') !!}@endif" name="sort_no" placeholder="Sort No" required>
                        {!! $errors->first('sort_no', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
@stop