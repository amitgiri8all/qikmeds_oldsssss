@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
My Order Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
 <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
  <style type="text/css">
     .badge-danger {
    color: #fff;
    background-color: #dc3545;
}.badge-success {
    color: #fff;
    background-color: #28a745;
}
  </style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>My Order Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li class="active">My Order list</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">

 

   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
             My Order List
            </h4>
           <!--   <div class="pull-right">
               <a href="{{route('admin.coupon.add')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add New Coupon </a>
            </div> -->
          </div>
         <div class="panel-body">
            <table class="table table-bordered " id="serverside_datatable">
               <thead>
                  <tr class="filters">
                     <th>Order ID</th>
                     <th>Customer Name</th>
                     <th><i class="fa fa-shopping-cart" aria-hidden="true" title="SubTotal"></i></th>
                     <th><i class="fa fa-truck" aria-hidden="true" title="Shipping"></i></th>
                     <th><i class="fa fa-tags" aria-hidden="true" title="Discount"></i></th>
                     <th>Total</th>
                     <th><i class="fa fa-credit-card" aria-hidden="true" title="Payment method"></i></th>
                     <th>Status</th>
                     <th>Created On</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
 
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
 

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>

<script> 
$(document).ready(function(){
   var id = "<?php echo $my_order_id ?>" ;
  var dataTable = $('#serverside_datatable').DataTable({
    'processing': true,
    'serverSide': true,
    //'searching': false, // Remove default Search Control
    

    'ajax': {

        'url':"{{ route('admin.customer.my-orders-list',$my_order_id) }}",
      
       'data': function(data){
        // $('#loading').show();
          // Read values
         // var order_status = $('#order_status').val();
         // var from_date = $('#from_date').val();
         // var to_date = $('#to_date').val();
          //alert(order_status)
          // Append to data
         // data.order_status = order_status;
         // data.from_date = from_date;
          //data.to_date = to_date;
       }
    },
    'columns': [
            {data: 'order_code', name: 'order_code'},
            {data: 'user_name', name: 'user_name', orderable: false, searchable: false},
            {data: 'sub_total', name: 'sub_total', orderable: false, searchable: false},
            {data: 'shipping_amount', name: 'shipping_amount', orderable: false, searchable: false},
            {data: 'discount_coupon_code', name: 'discount_coupon_code', orderable: false, searchable: false},
            {data: 'total_payed_amount', name: 'total_payed_amount', orderable: false, searchable: false},
            {data: 'payment_method', name: 'payment_method', orderable: false, searchable: false},

           {data: 'status', name: 'status'},
           {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
  });
  $('#search_btn').click(function(){
    //$('#loading').show();
    dataTable.draw();
  });
});




/*$(function () {
    var table = $('#serverside_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.order.list') }}",
        data: function(data){
          var gender = $('#searchByGender').val();
          data.searchByGender = gender;
         }
        },
        columns: [
            {data: 'order_id', name: 'order_id'},
            {data: 'user_name', name: 'user_name', orderable: false, searchable: false},
            {data: 'sub_total', name: 'sub_total', orderable: false, searchable: false},
            {data: 'shipping_amount', name: 'shipping_amount', orderable: false, searchable: false},
            {data: 'discount_coupon_code', name: 'discount_coupon_code', orderable: false, searchable: false},
            {data: 'total_payed_amount', name: 'total_payed_amount', orderable: false, searchable: false},
            {data: 'payment_method', name: 'payment_method', orderable: false, searchable: false},

           {data: 'status', name: 'status'},
           {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });*/
</script>
@stop
