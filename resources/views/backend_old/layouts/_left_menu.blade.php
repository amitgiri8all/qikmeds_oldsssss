<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<ul id="menu" class="page-sidebar-menu">

  <li>
    <a href="{{route('admin.dashboard')}}"> <i class="fa fa-home"></i>  <span class="title">Dashboard</span>
    </a>
  </li>

@if (Auth()->guard('admin')->user()->role_id==10)
     <li>
    <a href="{{URL::to('admin/roles')}}"> <i class="fa fa-home"></i>  <span class="title">Role manager</span>
    </a>
  </li>  
@endif
 
 @php
    $permissionmenu = App\Models\Links::menupermission();
    //echo "<pre>"; print_r($permissionmenu); die;
     @endphp
    @foreach(App\Models\Links::getParentMenu() as $parent)
    @if(in_array($parent->id, $permissionmenu))
      <li class="{{ request()->is($parent->link.'/*') ? 'active' : '' }}">
            <a href="javascript:void(0);"> <i class="fa {{$parent->icon}}"></i>
              <span class="title">{{$parent->name}}</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                @foreach(App\Models\Links::getSubMenu($parent->id) as $submenu)
                <?php  $new_link  = (explode("/",$submenu->link));
                $main =  $new_link['1'];

                $url = $_SERVER['REQUEST_URI']; 
                $aa =  (explode("/",$url));
                
                $new_main = $aa['2'];
              
                ?>
                  <li class="{{ (request()->is($submenu->link) || $new_main.'/add' == $main.'/add' || $new_main.'/edit' == $main.'/edit') ? 'active' : '' }}">
                    <a href="{{URL::to($submenu->link)}}"> <i class="fa fa-angle-double-right"></i>
                     {{$submenu->name}}</a>
                  </li>
                  @endforeach
        </ul>      
          @endif
          @if(in_array($parent->id, $permissionmenu))
              </li>
          @endif
          @endforeach
</ul>
</li>@include('backend/layouts/menu')</ul>


