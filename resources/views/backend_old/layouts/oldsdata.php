<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<ul id="menu" class="page-sidebar-menu">
   <li>
      <a href="{{route('dashboard')}}">
      <i class="fa fa-home"></i>   
      <span class="title">Dashboard</span>
      </a>
   </li>
    <li {!! (Request::is('setting/general') || Request::is('setting/site-setting') || Request::is('setting/payment') || Request::is('setting/social-media') || Request::is('setting/app-setting')|| Request::is('setting/api-setting')||Request::is('setting/app-version')? 'class="active"' : '') !!}>
      <a href="#">
      <i class="fa fa-cogs"></i>
      <span class="title">General Settings</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
          <li {!! (Request::is('setting/general') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('setting/general') }}">
                    <i class="fa fa-angle-double-right"></i>
                    General
                </a>
            </li>
          <li {!! (Request::is('setting/site-setting') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('setting/site-setting') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Site Settings
                </a>
            </li>
          <li {!! (Request::is('setting/payment') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('setting/payment') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Payment Settings
                </a>
            </li>
          <li {!! (Request::is('setting/social-media') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('setting/social-media') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Social Media
                </a>
            </li>
        
          <li {!! (Request::is('setting/api-setting') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('setting/api-setting') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Api Settings
                </a>
            </li>
         <li style="display: none;" {!! (Request::is('setting/app-setting') ? 'class"active" id="active"':'' )!!}">
            <a href="{{ URL::to('setting/app-setting') }}">
            <i class="fa fa-angle-double-right"></i>
            App Setting
            </a>
         </li>
          
      </ul>
   </li>
   <li {!! (Request::is('access-level/list') || Request::is('access-level/edit/*') || Request::is('permission-access/permission-assign') || Request::is('access-level/add')? 'class="active"' : '') !!}>
      <a href="#">
         <i class="fa fa-mortar-board"></i>
         <span class="title">Access Level</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
        <li {!! (Request::is('access-level/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('access-level/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Access Level</a>
         </li>
        <li {!! (Request::is('access-level/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('access-level/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Access Level</a>
         </li>
        <li {!! (Request::is('permission-access/permission-assign') ? 'class="active" id="active"' : '') !!}>
            <a style="display: none;" href="{{ URL::to('permission-access/permission-assign') }}"> <i class="fa fa-angle-double-right"></i>
            Assign Permission Access</a>
         </li>
      </ul>
   </li>

     <li {!! (Request::is('zone/list') || Request::is('zone/create') || Request::is('zone/edit/*') || Request::is('zone/add')? 'class="active"' : '') !!}>
      <a href="#">
         <i class="fa fa-area-chart"></i>
         <span class="title">Zone</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
         <li {!! (Request::is('zone/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('zone/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Zones</a>
         </li>
         <li {!! (Request::is('zone/create') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('zone/create') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Zone</a>
         </li>   
         <li style="display: none;">
            <a href="javascript::void(0)"> <i class="fa fa-angle-double-right"></i>
            Operation Zone</a>
         </li>
      </ul>
   </li>
   <li {!! (Request::is('customer/list') || Request::is('zone/create') || Request::is('zone/add')? 'class="active"' : '') !!}>
      <a href="#">
         <i class="fa fa-users"></i>
         <span class="title">Customer</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
         <li {!! (Request::is('customer/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('customer/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Customers</a>
         </li>
      </ul>
   </li>
     <li {!! (Request::is('category/list') || Request::is('category/add')|| Request::is('category/edit/*')? 'class="active"' : '') !!}>
      <a href="javascript::void()">
         <i class="fa fa-list-alt"></i>
         <span class="title">Shop</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('category/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('category/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Shop</a>
         </li>
            <li {!! (Request::is('category/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('category/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Shop</a>
         </li>
      </ul>
   </li> 
   <li {!! (Request::is('vendor-category/list') || Request::is('vendor-category/add') || Request::is('sub-vendor-category/list/*') || Request::is('vendor-category/edit/*') ? 'class="active"' : '') !!}>
      <a href="javascript::void()">
         <i class="fa fa-list-alt"></i>
         <span class="title"> Vendor Category</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('vendor-category/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('vendor-category/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Vendor Category</a>
         </li>
            <li {!! (Request::is('vendor-category/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('vendor-category/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Vendor Category</a>
         </li>
      </ul>
   </li> 

    <li {!! (Request::is('product/list') || Request::is('product/add') || Request::is('product/edit/*')? 'class="active"' : '') !!}>
      <a href="javascript::void()">
         <i class="fa fa-product-hunt" aria-hidden="true"></i>
         <span class="title">product</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('product/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('product/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Product</a>
         </li>
            <li {!! (Request::is('product/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('product/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Product</a>
         </li>
      </ul>
   </li> 

       <li {!! (Request::is('vendor-product/list') || Request::is('vendor-product/add') || Request::is('vendor-product/edit/*')? 'class="active"' : '') !!}>
      <a href="javascript::void()">
         <i class="fa fa-product-hunt" aria-hidden="true"></i>
         <span class="title">Vendors Product</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('vendor-product/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('vendor-product/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Vendor Product</a>
         </li>
            <li {!! (Request::is('vendor-product/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('vendor-product/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Vendor Product</a>
         </li>
      </ul>
   </li> 

  
    <li {!! (Request::is('measurement-class/list') || Request::is('measurement-class/add') || Request::is('measurement-class/edit/*')? 'class="active"' : '') !!}>
      <a href="javascript::void()">
         <i class="fa fa-balance-scale"></i>
         <span class="title">Measurement Class</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('measurement-class/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('measurement-class/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Measurement Class</a>
         </li>
            <li {!! (Request::is('measurement-class/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('measurement-class/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Measurement Class</a>
         </li>
      </ul>
   </li>  
  
    <li {!! (Request::is('slot-time/list') || Request::is('slot-time/add') || Request::is('slot-time/edit/*')? 'class="active"' : '') !!}>
      <a href="javascript::void()">
         <i class="fa fa-clock-o"></i>
         <span class="title">Slot Time</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('slot-time/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('slot-time/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Slot Time</a>
         </li>
            <li {!! (Request::is('slot-time/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('slot-time/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Slot Time</a>
         </li>
      </ul>
   </li>  

  
    <li {!! (Request::is('delivery-days/list') || Request::is('delivery-time/add') || Request::is('delivery-time/edit/*')? 'class="active"' : '') !!}>
      <a href="javascript::void()">
         <i class="fa fa-calendar"></i>
         <span class="title">Delivery Days</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('delivery-days/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('delivery-days/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Delivery Days</a>
         </li>
            <li {!! (Request::is('delivery-time/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('delivery-time/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Delivery Time</a>
         </li>
      </ul>
   </li>  




   <li {!! (Request::is('coupon/list') || Request::is('coupon/add') || Request::is('coupon/edit')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-gift"></i>
         <span class="title">Coupon</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('coupon/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('coupon/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Coupon</a>
         </li>
            <li {!! (Request::is('coupon/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('coupon/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Coupon</a>
         </li>
      </ul>
   </li>  


      <li {!! (Request::is('city/list') || Request::is('city/add') || Request::is('city/edit/*')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-building-o"></i>
         <span class="title">City</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('city/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('city/list') }}"> <i class="fa fa-angle-double-right"></i>
            All City</a>
         </li>
            <li {!! (Request::is('city/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('city/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New City</a>
         </li>
      </ul>
   </li>  

      <li {!! (Request::is('user/list') || Request::is('user/add') || Request::is('user/edit/*')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-users"></i>
         <span class="title">Shoppers/Drivers/Vendors </span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('user/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('user/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Shoppers/Drivers/Vendors </a>
         </li>
            <li {!! (Request::is('user/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('user/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Shoppers/Drivers/Vendors</a>
         </li>
      </ul>
   </li>  

      <li style="display: none;" {!! (Request::is('region/list') || Request::is('region/add')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-users"></i>
         <span class="title">Region </span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('region/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('region/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Region</a>
         </li>
            <li {!! (Request::is('region/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('region/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New rRegion</a>
         </li>
      </ul>
   </li>  

      <li {!! (Request::is('offer/list') || Request::is('offer/add') || Request::is('offer/edit/*')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-users"></i>
         <span class="title">Offer</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('offer/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('offer/list') }}"> <i class="fa fa-angle-double-right"></i>
            All offers </a>
         </li>
            <li {!! (Request::is('offer/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('offer/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Offer</a>
         </li>
      </ul>
   </li>  

<li {!! (Request::is('cms/list') || Request::is('cms/add') || Request::is('cms/edit')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-users"></i>
         <span class="title">Page</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('cms/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('cms/list') }}"> <i class="fa fa-angle-double-right"></i>
            All CMS Pages </a>
         </li>
            <li {!! (Request::is('cms/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('cms/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add CMS Pages</a>
         </li>
      </ul>
   </li>  
 
   <li {!! (Request::is('banner/list') || Request::is('banner/add') || Request::is('banner/edit/*')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-gift"></i>
         <span class="title">Banner</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('banner/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('banner/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Banner</a>
         </li>
            <li {!! (Request::is('banner/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('banner/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Banner</a>
         </li>
      </ul>
   </li>  

   
   <li {!! (Request::is('week-package/list') || Request::is('week-package/add') || Request::is('week-package/edit')? 'class="active"' : '') !!}>
        <a href="#">
         <i class="fa fa-gift"></i>
         <span class="title">week-package</span>
         <span class="fa arrow"></span>
      </a>
      <ul class="sub-menu">
            <li {!! (Request::is('week-package/list') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('week-package/list') }}"> <i class="fa fa-angle-double-right"></i>
            All Week packages</a>
         </li>
            <li {!! (Request::is('week-package/add') ? 'class="active" id="active"' : '') !!}>
            <a href="{{ URL::to('week-package/add') }}"> <i class="fa fa-angle-double-right"></i>
            Add New Week Package</a>
         </li>
      </ul>
   </li>  
  

      </ul>
   </li>
   @include('admin/layouts/menu')
</ul>