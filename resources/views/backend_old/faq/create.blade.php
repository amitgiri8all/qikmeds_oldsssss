@extends('backend/layouts/default')
@section('title')
Faq Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Faq Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Faq</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Faq
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.faq.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.faq.add.post') }}" id="faq" enctype="multipart/form-data">
                  @csrf
                  <?php //echo "<pre>";print_r($faq_topics);die; ?>
                  <!-- Topic Name -->
                   <div class="form-group col-sm-12">
                     <label for="text">Topic Name</label>
                     <!-- <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('name')}}"  name="name" placeholder="Topic name">
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                     </div> -->
                     <div class="input-group col-xs-12">
                     <select name="topic_id">
                       @foreach($faq_topics as $value)
                       <option class="form-control" value="{{$value->id}}">{{$value->name}} </option>
                       @endforeach
                     </select>
                      </div>
                  </div>

                 <!-- Question -->
                   <div class="form-group col-sm-12">
                     <label for="text">Question</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('question')}}"  name="question" placeholder="Question">
                        {!! $errors->first('question', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                       <!-- Answer -->
                   <div class="form-group col-sm-12">
                     <label for="text">Answer</label>
                     <!-- <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('answer')}}" name="answer" placeholder="Answer ">
                        {!! $errors->first('answer', '<span class="help-block">:message</span>') !!}
                     </div> -->
                      <div class="input-group col-xs-12">
                         <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="answer"></textarea>
                     </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
 <!--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script> -->
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<!-- <script type="text/javascript">
            $('#us3').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                     locationNameInput: $('#address_name')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script> -->
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>
@stop