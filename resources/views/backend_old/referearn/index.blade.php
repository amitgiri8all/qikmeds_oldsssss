@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
Refer Earn Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
  <style type="text/css">
     .badge-danger {
    color: #fff;
    background-color: #dc3545;
}.badge-success {
    color: #fff;
    background-color: #28a745;
}
  </style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Refer Earn Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li class="active">Refer Earn list</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">

   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               ReferEarn List
            </h4>
        <!--      <div class="pull-right">
               <a href="{{route('admin.refer-earn.add')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add New Refer Earn </a>
            </div> -->
          </div>
         <div class="panel-body" style="overflow: auto;">
            <table class="table table-bordered " id="serverside_datatable">
               <thead>
                  <tr class="filters">
                     <th>ID</th>
                     <th>Title</th>
                     <th>Refer Amount</th>
                     <th>Refer By Amount</th>
                     <th>Description</th>
                     <th>Image</th>
                     <th>Created On</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
 

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>
<script> 
$(function () {
    var table = $('#serverside_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.refer-earn.list') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'refer_amount', name: 'refer_amount'},
            {data: 'refer_by_amount', name: 'refer_by_amount'},
            {data: 'description', name: 'description'},
            {data: 'image', name: 'image'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });
</script>
@stop
