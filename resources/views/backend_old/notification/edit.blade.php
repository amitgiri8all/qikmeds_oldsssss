@extends('backend/layouts/default')
@section('title')
Vendor Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Vendor Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{'admin.dashboard'}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Edit new Vendor</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                     Edit Page
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.vendor.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" id="vendor" enctype="multipart/form-data">
                  @csrf
                  
                   <!-- First Name -->
                   <div class="form-group col-sm-6">
                     <label for="text">First Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->first_name)){!! $data->first_name !!}@else{!! old('first_name') !!}@endif"  name="first_name" placeholder="first name">
                        {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Last  Name -->
                   <div class="form-group col-sm-6">
                     <label for="text">Last Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->last_name)){!! $data->last_name !!}@else{!! old('first_name') !!}@endif"  name="last_name" placeholder="last name">
                        {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                      <!-- Email Name -->
                   <div class="form-group col-sm-6">
                     <label for="text">Email</label>
                     <div class="input-group col-xs-12">
                        <input type="email" class="form-control" value="@if(isset($data->email)){!! $data->email !!}
                        @else{!! old('email')!!}@endif" name="email" placeholder="Email Name">
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Address Details -->
                   <div class="form-group col-sm-6">
                     <label for="text">Address</label>
                     <div class="input-group col-xs-12">
                        <input type="text" name="address_name" class="form-control" value="@if(isset($data->address)){!! $data->address !!}
                        @else{!! old('address')!!}@endif"  placeholder="Address Name">
                        {!! $errors->first('address_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

             <!-- City Name -->
                   <div class="form-group col-sm-6">
                     <label for="text">City</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->city)){!! $data->city !!}
                        @else{!! old('city')!!}@endif" name="city" placeholder="City Name">
                        {!! $errors->first('city', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>  
                  <!-- State Name -->
                   <div class="form-group col-sm-6">
                     <label for="text">State</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->state)){!! $data->state !!}
                        @else{!! old('state')!!}@endif" name="state" placeholder="State Name">
                        {!! $errors->first('state', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Country Name -->
                   <div class="form-group col-sm-6">
                     <label for="text">Country</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->country)){!! $data->country !!}
                        @else{!! old('name')!!}@endif"  name="country" placeholder="Country Name">
                        {!! $errors->first('country', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Phone number  -->
                   <div class="form-group col-sm-6">
                     <label for="text">Phone</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->mobile_number)){!! $data->mobile_number !!}
                        @else{!! old('mobile_number')!!}@endif"  name="mobile_number" placeholder="Mobile number">
                        {!! $errors->first('mobile_number', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

               <!-- Pin Code -->
                   <div class="form-group col-sm-6">
                     <label for="text">Postal-code</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->pin_code)){!! $data->pin_code !!}
                        @else{!! old('pin_code')!!}@endif" name="pin_code" placeholder="Pin code Name">
                        {!! $errors->first('pin_code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
          
          {{--  <!-- Password  -->
                  <div class="form-group col-sm-6">
                     <label for="text">Password</label>
                     <div class="input-group col-xs-12">
                        <input type="password" class="form-control" name="password" placeholder="Password " 
                         value="@if(isset($data->password)){!! $data->password !!}
                        @else{!! old('password')!!}@endif"
                        >
                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

             <!-- Confirm Password  -->
                   <div class="form-group col-sm-6">
                     <label for="text">Confirm Password</label>
                     <div class="input-group col-xs-12">
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation Name">
                        {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                --}} 
                 <!-- Vendor Image -->
                  <div class="form-group col-sm-6">
                     <label for="text"> Vendor Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="image">
                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  @php
                    if(!empty($data->image)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$data->image);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                  @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
                  </div>     
              
   {{--
                  <!-- licensed_pharmacist_details documen Image -->
                  <div class="form-group col-sm-6">
                     <label for="text">Licensed pharmacist details</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="licensed_pharmacist_details"  
                         >
                        {!! $errors->first('licensed_pharmacist_details', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                    @php
                    if(!empty($data_image->licensed_pharmacist_details)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$data_image->licensed_pharmacist_details);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                   @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
                  </div>
 
                  <!-- authorized_pharmacy_related_documents documen Image -->
                  <div class="form-group col-sm-6">
                     <label for="text">Authorized pharmacy related documents</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="authorized_pharmacy_related_documents"  
                        >
                        {!! $errors->first('authorized_pharmacy_related_documents', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                   @php
                    if(!empty($data_image->authorized_pharmacy_related_documents)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$data_image->authorized_pharmacy_related_documents);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                   @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
                  </div> 
 
                  <!-- Address Location -->
                  <div class="form-group col-sm-12">
                     <label for="text">Address Location</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="address_name" placeholder="Address name" class="form-control" name="address" value="@if(isset($data->address)){!! $data->address !!}
                        @else{!! old('address')!!}@endif" >
                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  
                  <!-- Address Location -->
                  <div class="form-group col-sm-12">
                     <label for="text">Latitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="lat" readonly placeholder="Address name" class="form-control" name="lat">
                        {!! $errors->first('lat', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  

                  <!-- Address Location -->
                  <div class="form-group col-sm-12">
                     <label for="text">Longitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="long" readonly placeholder="Longitute name" class="form-control" name="lng">
                        {!! $errors->first('lng', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>
                
                  <!-- Address Location -->
                  <div class="form-group col-sm-12">
                      <div class="input-group col-xs-12">
                  <div id="us3" style="width: 100%; height: 300px;"></div>
                        </span>
                     </div>
                  </div> 

                  --}}   


                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('Update')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<!-- <script type="text/javascript">
            $('#us3').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                    // locationNameInput: $('#address_name')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script> -->
@stop