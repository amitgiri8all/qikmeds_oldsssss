@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
Order Status Notifications::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Order Status Notifications</h1>
   <ol class="breadcrumb">
      <li>
         <a href="javascript::void">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Order Status Notifications</li>
      <li class="active">Order Status Notifications</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">
   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               Order Status Notifications
            </h4>
           
         </div>
         <div class="panel-body">
            <table class="table table-bordered " id="table1">
               <thead>
                  <tr class="filters">
                        <th>Id</th>
                        <th>Message Heading</th>
                        <th>Order Code</th>
                        <th>Sendor</th>
                        <th>Action</th>
                 </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>
<script>
   var datatbleurl= "{!! route('admin.notification.orderStatusData') !!}";


   $(function() {

            window.table=$('#table1').DataTable({
                /* dom: "Bfrtip",*/
                buttons: [

                ],
                order: [[4, "desc" ]],
                responsive: true,
                processing: true,
                oLanguage: {
                sProcessing: "<img style='width:50%;height:auto' src='{{asset('public/upload/loader.gif')}}'>"
                },
                serverSide: true,
                ajax: datatbleurl,
                columns: [
                    { data: 'Slno', name: 'Slno' },
                    { data: 'heading', name: 'heading' },
                    { data: 'order_code', name: 'order_code' },
                    { data: 'sendor', name: 'sendor' },
                    { data: 'action', name: 'action',orderable: false, searchable: false }
                ]
            });
        });


      function deleteRow(id){
            $.ajax({
                data: {
                    id:id
                },
                type: "GET",
                url: "{{ url('notification/note') }}/"+id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function( data ) {
                    window.table.draw();
                    alert('You have been successfully delete.')
                },
                error: function( data ) {
                  alert('something is wrong')
                }
            });
        }

</script>
@stop