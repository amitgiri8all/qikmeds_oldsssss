@extends('backend/layouts/default')
@section('title')
Notification Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
<style type="text/css">
  .button-container {
  margin-bottom: 10px;
}
 
</style>
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Notification Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new notification</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Send Push Notification
               </h3>
            <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{route('admin.notification.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
               <form method="post" action="{{ route('admin.notification.add.post') }}" id="example-form" enctype="multipart/form-data">
                   @csrf
                   <!-- Link Type -->
             {{--       <div class="form-group">
                     <label for="validate-text">Link Type</label>
                     <div class="input-group col-xs-12">
                       <select class="form-control" name="link_type" id="link_type">
                          <option value="">Please Status Type</option>
                          <option value="external">External link</option>
                          <option value="internal">Internal Link</option>
                       </select> 
                        {!! $errors->first('link_type', '<span class="help-block">:message</span>') !!}
                       </div>
                  </div>
                <!-- Link Type -->
                  <div class="form-group" id="internal_div" style="display: none;">
                     <label for="validate-text">Category</label>
                     <div class="input-group col-xs-12">
                       <select class="form-control" name="cat_id" id="cat_id">
                          <option value="">Please Category Type</option>
                          @foreach($category as $val)
                           <option value="{{$val->id}}">{{$val->name}}</option>
                          @endforeach 
                       </select>   
                     </div>
                  </div>
                  
                    <!-- banner Image -->
                    <div class="form-group" id="link_div" style="display: none;">
                     <label for="validate-text">Link</label>
                     <div class="input-group col-xs-12">
                        <input type="url" class="form-control" name="link"  id="validate-text"
                           placeholder="Enter url" >
                     </div>
                  </div>
                 --}}
                    <div class="form-group">
                     <label for="validate-text">Message Heading</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control num" name="message_heading" 
                           placeholder="Enter Message Heading">
                         {!! $errors->first('message_heading', '<span class="help-block">:message</span>') !!}
                     </div>
                    </div>

                    <div class="form-group">
                     <label for="validate-text">Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="image">
                         {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                     <div class="form-group">
                     <label for="validate-text">Message</label>
                     <div class="input-group col-xs-12">
                        <textarea class="form-control" name="message"></textarea>
                         {!! $errors->first('message', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Access Level Status -->
                    <div class="form-group">
                     <label for="text">User</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" id="my-select" name="user_ids[]" multiple>
                            @foreach($user as $value)
                            <option value="{{$value->id}}">{{$value->name}} ({{$value->id}})</option>
                            @endforeach
                          </select>
                         {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                    <!-- Access Level Status -->
                    <div class="form-group">
                     <div class="input-group col-xs-2">
                          <div class="button-container">
                            <button type="button" onclick="selectAll()">Select All</button>
                            <button type="button" onclick="deselectAll()">Deselect All</button>
                          </div>
                     </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        Send
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>



<script type="text/javascript">
  $(document).on("keypress",".select2-input",function(event){
    if (event.ctrlKey || event.metaKey) {
        var id =$(this).parents("div[class*='select2-container']").attr("id").replace("s2id_","");
        var element =$("#"+id);
        if (event.which == 97){
            var selected = [];
            element.find("option").each(function(i,e){
                selected[selected.length]=$(e).attr("value");
            });
            element.select2("val", selected);
        } else if (event.which == 100){
            element.select2("val", "");
        }
    }
});

  /*  $("#link_type").change(function(){
            if($(this).val()=='internal'){
                $("#internal_div").show();
                $("#cat_id").attr('required','required');
                $("#link_div").removeClass("has-error");
                $("#link").removeClass("has-error");
                $("#link_div").hide();
                
            }else{
                $("#link_div").addClass("has-error");
                $("#link").addClass("has-error");
                $("#link_div").show();
                $("#internal_div").hide();
            }
        });*/
    $(document).ready(function() {
    $("#my-select").select2();
});

function selectAll() {
    $("#my-select > option").prop("selected", true);
    $("#my-select").trigger("change");
}

function deselectAll() {
    $("#my-select > option").prop("selected", false);
    $("#my-select").trigger("change");
}
</script>
@stop
