@extends('backend/layouts/default')
@section('title')
Delivery location Manager::CRM
@stop
@section('header_styles')

@stop
{{-- Content --}}
@section('content')
 

<section class="content-header">
   <h1>
      Delivery location Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{'admin.dashboard'}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Delivery location</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Delivery location
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.delivery-location.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <span id="error"></span>




            <div class="panel-body">
                <form method="post" action="{{ route('admin.delivery-location.add.post') }}" id="example-form" enctype="multipart/form-data">
                  @csrf
 
 
                  <!-- Category Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Pin Code</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control getdetails" value="" id="pin_code" name="pin_code" placeholder="Pin code">
                        {!! $errors->first('pin_code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  
   
                  <!-- Category Name -->
                    <div class="form-group col-sm-6" style="display:none;">
                      <div class="input-group col-xs-12">
                        <input type="button" class="btn btn-primary" id="getdetails" value="Get Details">
                     </div>
                  </div>
                   <div class="form-group col-sm-4">
                     <label for="text">Name</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="name" id="datatype" >
                             
                          </select>
                      </div>
                  </div>
                  <!-- Category Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">State Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="" id="state" name="state" placeholder="state" disabled="disabled">
                        {!! $errors->first('pin_code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
  <!-- Category Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">City Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" disabled="disabled" value="" id="city" name="city" placeholder="city">
                        {!! $errors->first('pin_code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                 
 
                  <div id="speakerResponse"></div>  


                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script type="text/javascript">
   var delivery ="{{route('admin.delivery-location.get-details.post')}}";

var token       = '{{ csrf_token() }}';
$('#loading').hide();
   $(document).ready(function(){
    $(document).on('keyup', '.getdetails', function()
    {
    var length =  $("#pin_code").val().length;
      if (length == 6) {
          $('select[name="name"]').empty();
           var pin_code        = $("#pin_code").val();
           // alert(pin_code)
           $.ajax({
               url         : delivery,
               type        : 'POST',
               data        : {'_token': token,pin_code:pin_code},
               dataType    : 'json',
               beforeSend: function() {
                 $('#loading').show();
               },
               success     : function(data) 
               {

                  $('select[name="name"]').empty();
                  $('#state').val(data.state);
                  $('#city').val(data.city);
                 $(data.AllState).each(function(index, el) {
                     $('select[name="name"]').append('<option>'+ el +'</option>');
                  });


                },
               complete: function(){
                $('#loading').hide();
               },
               error       : function(response)
               {
                $('#error').html('<div class="alert alert-danger" role="alert">The requested resource is not found</div>');
               }
           });
         }  

    });  
   });  

</script>

 
@stop