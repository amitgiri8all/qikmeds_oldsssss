<div class="panel panel-primary" style=" border-radius:0;">
  <div class="panel-body">
                <form method="post" class="ajax_form" action="{{ route('admin.update.push-notification.setting') }}" id="payment" enctype="multipart/form-data">
                   @csrf
                

                  <h3>Push Notification Message :</h3>

                     <!-- Order Place -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Order Place</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="order_place" id="validate-text"
                           placeholder="Enter Order Place" value="@if(isset($push_notification->order_place)){!! $push_notification->order_place !!}@else{!! old('order_place') !!}@endif">
                         {!! $errors->first('order_place', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- Order Accept-->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Order Accept</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="order_accept"  id="validate-text"
                           placeholder="Enter Order Accept" value="@if(isset($push_notification->order_accept)){!! $push_notification->order_accept !!}@else{!! old('order_accept') !!}@endif">
                         {!! $errors->first('order_accept', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                  
                   <!-- Order Cancel -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Order Cancel</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="order_cancel" id="validate-text"
                           placeholder="Enter Order Cancel" value="@if(isset($push_notification->order_cancel)){!! $push_notification->order_cancel !!}@else{!! old('order_cancel') !!}@endif">
                         {!! $errors->first('order_cancel', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Order Place -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Order Shipped</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="order_shipped" id="validate-text"
                           placeholder="Enter Shipped Order" value="@if(isset($push_notification->order_shipped)){!! $push_notification->order_shipped !!}@else{!! old('order_shipped') !!}@endif">
                         {!! $errors->first('order_shipped', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

               


                  <!-- Order Return -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Order Return</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="order_return"  id="validate-text"
                           placeholder="Enter Order Return" value="@if(isset($push_notification->order_return)){!! $push_notification->order_return !!}@else{!! old('order_return') !!}@endif">
                         {!! $errors->first('order_return', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                     <!-- Order Delivered -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Order Delivered</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="order_deliverd"  id="validate-text"
                           placeholder="Enter Order Delivered" value="@if(isset($push_notification->order_deliverd)){!! $push_notification->order_deliverd !!}@else{!! old('order_deliverd') !!}@endif">
                         {!! $errors->first('order_deliverd', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                 

                   

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
</div>