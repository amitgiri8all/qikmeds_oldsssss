@extends('backend.layouts.default')
{{-- Web site Title --}}
@section('title')
General Setting Manager  r::CRM
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      General Setting Manager    
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{ route('admin.dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         @lang('general.dashboard')
         </a>
      </li>
      <li>Product Manager</li>
      <li class="active">
         Create
      </li>
   </ol>
</section>
<!-- Main content -->
 


<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                  <strong>Create Product:</strong> <span> &nbsp;  &nbsp;    </span>
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{ URL::previous()}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
              <!--  <div class="pull-right" style="margin-top: -42px">
                  <a href="{{ URL::previous()}}" class="btn btn-sm btn-danger"  style="margin-bottom:-42px;"><span class="btn-label">
                  <i class="glyphicon glyphicon-chevron-left"></i>
                  </span><span style="font-size:13px;margin-left:8px">Back</span></a>
               </div> -->
            </div>
            <div class="panel-body modal-panel">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Tabbable-Panel Start -->
                     <div class="tabbable-panel">
                        <!-- Tabbablw-line Start -->
                        <div class="tabbable-line">
                           <!-- Nav Nav-tabs Start -->
                           <ul class="nav nav-tabs ">
                              <li class="active">
                                 <a data-toggle="tab" href="#general">
                                 General Setting</a>
                              </li>
                               <li class="">
                                 <a data-toggle="tab"   href="#site">
                                 Site Setting </a>
                              </li> 
                              <li class="">
                                 <a data-toggle="tab"   href="#paymentSetting">
                                 Payment Setting </a>
                              </li>
                              <li class="">
                                 <a data-toggle="tab"   href="#pushNotification">
                                 Push Notification Setting </a>
                              </li>
                              
                           </ul>
                           <!-- //Nav Nav-tabs End -->
                           <!-- Tab-content Start -->
                           <div class="tab-content">
                              <div id="general" class="tab-pane active">
                                   @include('backend/setting/general')
                              </div>
                           
                              <div id="site" class="tab-pane">
                                     @include('backend/setting/site')
                              </div>
                               <div id="paymentSetting" class="tab-pane">
                                     @include('backend/setting/paymentSetting')
                              </div>
                              <div id="pushNotification" class="tab-pane">
                                     @include('backend/setting/pushNotification')
                              </div>
                               
                              <!-- Tab-content End -->
                           </div>
                           <!-- //Tabbable-line End -->
                        </div>
                        <!-- Tabbable_panel End -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- content -->
@stop
@section('footer_scripts')
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<script type="text/javascript">
   $('#general').attr("disabled", false);  

  /*$('.general"]')attr('disabled','disabled');
   $(document).ready(function(){
          $('input[type="text"]').change(function(){
            alert('sadfsad')
            if($(this).val != ''){
              $('.general"]').removeAttr('disabled');
            }

          });
   });*/
</script>
@stop