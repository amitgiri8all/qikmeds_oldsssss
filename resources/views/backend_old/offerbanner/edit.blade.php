@extends('backend/layouts/default')
@section('title')
Offer-banner Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Offer-banner Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Edit new offer-banner</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                      Edit offer-banner
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.offer-banner.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" id="offer-banner" enctype="multipart/form-data">
                  @csrf
                  
                   <!-- Title Name -->
                   <div class="form-group">
                     <label for="text">Title Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->title)){!! $data->title !!}@else{!! old('title') !!}@endif"  name="title" placeholder="title">
                        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Sub title -->
                   <div class="form-group">
                     <label for="text">Sub title</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="sub_title" placeholder="Sub Title" value="@if(isset($data->sub_title)){!! $data->sub_title !!}@else{!! old('sub_title') !!}@endif">
                        {!! $errors->first('sub_title', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Price Type -->
                    <div class="form-group" style="display:none;">
                     <label for="text"> Price Type</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="price_type" id="price_type" >
                             <option value="">Please Price Type</option>
                             <option value="percentages" @if($data->price_type=='percentages') {{ 'selected' }} @endif>Percentages (%)</option>

                             <option value="amount" @if($data->price_type=='amount') {{ 'selected' }} @endif>Amount (00.0)</option>
                          </select>
                         {!! $errors->first('price_type', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> 
                  <!-- Price Type -->
                    <div class="form-group">
                     <label for="text"> Offer Type</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="offer_type" id="offer_type" >
                             <option value="">Please Offer Type</option>
                             <option value="discount" @if($data->offer_type=='discount') {{ 'selected' }} @endif>Discount</option>

                             <option value="cashback" @if($data->offer_type=='cashback') {{ 'selected' }} @endif>Cashback</option>
                          </select>
                         {!! $errors->first('offer_type', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                  <!-- Minimum Amount -->
                  <div class="form-group">
                     <label for="text">Minimum Amount</label>
                      <div class="input-group col-xs-12">
                        <input type="Number" min="1" name="minimum_amount" class="form-control num" placeholder="Enter Minimum Amount"  value="@if(isset($data->minimum_amount)){!! $data->minimum_amount !!}@else{!! old('minimum_amount') !!}@endif">
                           {!! $errors->first('minimum_amount', '<span class="help-block">:message</span>') !!}
                       </div>
                  </div>

                    <!-- Price -->
                   <div class="form-group">
                     <label for="text">Price</label>
                     <div class="input-group col-xs-12">
                        <input type="number" class="form-control" name="price" placeholder="Price" value="@if(isset($data->price)){!! $data->price !!}@else{!! old('price') !!}@endif">
                        {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Code -->
                   <div class="form-group">
                     <label for="text">Code</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="code" placeholder="Code" value="@if(isset($data->code)){!! $data->code !!}@else{!! old('code') !!}@endif">
                        {!! $errors->first('code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- Image -->
                  <div class="form-group ">
                  <label for="Image">Image</label>
                   <div class="input-group col-xs-12">
                      <input type="file" class="form-control" name="image"  
                         >
                      {!! $errors->first('Image', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                    @php
                    if(!empty($data->image)){
                    $url= $data->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                   @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
                  </div>

                    <!-- Description  -->
                  <div class="form-group">
                     <label for="text">Description</label>
                     <div class="input-group col-xs-12">
                         <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="description">@if(isset($data->description)){!! $data->description !!}@else{!! old('description') !!}@endif</textarea>
                         {!! $errors->first('code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                        <!-- Eligibility  -->
                  <div class="form-group">
                     <label for="text">Eligibility</label>
                     <div class="input-group col-xs-12">
                         <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="eligibility">@if(isset($data->eligibility)){!! $data->eligibility !!}@else{!! old('eligibility') !!}@endif</textarea>
                         {!! $errors->first('eligibility', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                           <!-- How do you get it?  -->
                  <div class="form-group">
                     <label for="text">How do you get it?</label>
                     <div class="input-group col-xs-12">
                         <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="how_get_it">@if(isset($data->how_get_it)){!! $data->how_get_it !!}@else{!! old('how_get_it') !!}@endif</textarea>
                         {!! $errors->first('how_get_it', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                           <!-- Condition  -->
                  <div class="form-group">
                     <label for="text">Condition</label>
                     <div class="input-group col-xs-12">
                         <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="condition_offer">@if(isset($data->condition_offer)){!! $data->condition_offer !!}@else{!! old('condition_offer') !!}@endif</textarea>
                         {!! $errors->first('condition_offer', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                           <!-- Terms & Conditions -->
                  <div class="form-group">
                     <label for="text">Terms & Conditions</label>
                     <div class="input-group col-xs-12">
                         <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="term_condition">@if(isset($data->term_condition)){!! $data->term_condition !!}@else{!! old('term_condition') !!}@endif</textarea>
                         {!! $errors->first('term_condition', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>




                  <!--   <div class="item form-group ">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link_type">Link Type <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <select class="form-control col-md-7 col-xs-12" id="link_type" name="link_type"><option value="external">External link</option><option value="internal" selected="selected">Internal Link</option></select>
                                        
                                    </div>
                            </div> -->


                      <!-- Link Type -->
<!--                    <div class="form-group">
                     <label for="text">Link Type</label>
                      <select class="form-control col-md-7 col-xs-12 " id="link_type" name="link_type" value={{$data->link_type}}>
                      <option value="external" @if(($data->link_type) =='external') selected @endif >External link</option>

                      <option value="internal" @if(($data->link_type) == 'internal') selected @endif >Internal Link</option>
                      </select>
                      {!! $errors->first('link_type', '<span class="help-block">:message</span>') !!}
                      
                  </div> -->

                  <!-- <div id="link_div" class="form-group" style={{ $data->link_type == 'internal' ? 'display:none' : 'display:block' }} >                                
                     <label for="link">Link </label>
                       <div class="input-group col-xs-12">
                        <input type="text" placeholder="link" class="form-control" name="link_url" value="@if(isset($data->link_url)){!! $data->link_url !!}@else{!! old('link_url') !!}@endif">
                      </div>
                      {!! $errors->first('link', '<span class="help-block">:message</span>') !!}

                  </div> -->

                

                   <!-- <div id="internal_div" style={{ $data->link_type == 'external' ? 'display:none' : 'display:block' }}>
                     
                      <div class="form-group ">
                         <label  for="category">Category</label>
                            <div class="input-group col-xs-12">
                              <select class="form-control" id="cat_id" name="cat_id" value="{{$data->cat_id}}">

                            <option selected="selected" value>
                            </option> 
                             @foreach($category_id as $key => $value)
                             <option value="{{$value->id}}"
                             @if(($data->cat_id) == ($value->id)) selected @endif
                                  >{{$value->category_name}}</option>
                                @endforeach
                            </select>
                                        
                            </div>
                        </div>


                           <div id="sub_cat_div" class=" item form-group " style="display: none;">
                           <label  for="subcategory">Sub Category</label>
                               <div class="input-group col-xs-12">
                                <select class="form-control" id="sub_cat_id" name="sub_cat_id">

                                  <option selected="selected" value=""></option>
                                </select>
                               </div>
                           </div>

                            <div class="form-group ">
                                <label  for="product_id">Product</label>
                                    <div class="input-group col-xs-12">
                                        <select class="form-control" id="product_id" name="vendor_product_id"><option selected="selected" value=""></option></select>
                                        
                                    </div>
                            </div>

                          </div>
 -->

 
                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('Update')
                        </button>
                     </div>
                  </div>

               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')

 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>




<script>
    
          $("#link_type").change(function(){

            if($(this).val()=='internal'){
                $("#internal_div").show();
                $("#cat_id").attr('required','required');
                $("#link_div").removeClass("has-error");
                $("#link").removeClass("has-error");
                $("#link_div").hide();
                
            }else{
                $("#cat_id").attr('required',false);
                $("#link_div").addClass("has-error");
                $("#link").addClass("has-error");
                $("#link_div").show();
                $("#internal_div").hide();
            }
          });
          

function getSubcateSelectedVal() {
        var sub_cat_id = "<?php echo $data->sub_cat_id ?>";
        if($("input[name=cat_id]").length >= 1) {
            var cat_id = "<?php echo $data->cat_id ?>";
        } else {
            var cat_id = $("select[name=cat_id] option:selected").val();
        }
        
        if(cat_id == ''){
                $('#sub_cat_id option:not(:first)').remove();
                $('#product_id option:not(:first)').remove();
            }

            $.ajax({
                data: {
                    id:cat_id
                },
                method:'post',
                url: "{{ route('admin.offer-banner.getsubcategory')}}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function( response ) {
               // console.log(response);
                   // if(response.status == 'success'){
                      //alert('ok');

                       $("#sub_cat_div").show();
                       //$('#sub_cat_id').children('option:not(:first)').remove();
                        $('#sub_cat_id option:not(:first)').remove();
                        $('#sub_cat_id').append($("<option selected value=''>Select Sub Category</option>"));
                        console.log(response.data)
                        var optionsToAppend = '<option value="0">Select Sub Category</option>';
                       $.each(response.data, function(key, value) {
                          if(key == sub_cat_id){
                              var selected = 'selected';
                          } else{
                              var selected = '';  
                          }  
                    
                          optionsToAppend += '<option value="'+key+'" '+selected+'>'+value+'</option>';             

                        });
                       $('select[name=sub_cat_id]').html(optionsToAppend);
                    
                    $('#product_id option:not(:first)').remove();
                    $('#product_id').append($("<option selected value=''>Select Product</option>"));
                    $.each(response.productData, function(key, value) {
                             $('#product_id')
                                .append($("<option></option>")
                                .attr("value",key)
                                .text(value));
                        });
                       if(response.data.length==0){
                         $("#sub_cat_div").hide();
                       }
                  //  }

                    if(response.status == 'false'){
                        console.log('here');
                        $("#sub_cat_div").hide();
                    }
                
            },
                error: function( response ) {
                   /* new PNotify({
                        title: 'Error',
                        text: 'something is wrong',
                        type: "error",
                        styling: 'bootstrap3'
                    });*/
                }
            });
    }
    $(document).ready(function(){
            getSubcateSelectedVal();
        $("select[name=cat_id]").change(function(){
            getSubcateSelectedVal();
        });
    }); 


          
        $("#sub_cat_id").change(function(){
            if($(this).val()==''){
             $('#product_id').children('option:not(:first)').remove();
            }
            $.ajax({
                data: {
                    id:$(this).val()
                },
                method:'post',
                url: "{{ route('admin.offer-banner.getsubcat')}}",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function( response ) {
                console.log(response);
                    if(response.status == 'true'){
                    $('#product_id').children('option:not(:first)').remove();
                    $('#product_id').append($("<option selected value=''>Select Product</option>"));
                    $.each(response.productData, function(key, value) {
                             $('#product_id')
                                .append($("<option></option>")
                                .attr("value",key)
                                .text(value));
                        });
                       
                    }

                    if(response.status == 'false'){
                        console.log('here');
                       // $("#sub_cat_div").hide();
                    }
                
            },
                error: function( response ) {
                   /* new PNotify({
                        title: 'Error',
                        text: 'something is wrong',
                        type: "error",
                        styling: 'bootstrap3'
                    });*/
                }
            });
        });
        
    </script>
@stop