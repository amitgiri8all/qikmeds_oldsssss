@extends('backend/layouts/default')
@section('title')
Driver Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Driver Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.driver.list')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Driver Manager
         </a>
      </li>
      <li>View Driver Manager</li>
   </ol>
</section>
<!-- Main content -->
<!-- <section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  View Driver
               </h3>
            <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
     
         </div>
      </div>
   </div>

</section>
 -->

<section class="content">
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                     Driver Info
               </h3>
           
            </div>
            <div class="panel-body">
            
            <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            ID
                        </th>
                        <td>
                            {{ $data->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $data->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Email
                        </th>
                        <td>
                            {{ $data->email }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Mobile Number
                        </th>
                        <td>
                            {{ $data->mobile_number }}
                        </td>
                    </tr>

                       <tr>
                        <th>
                            Address
                        </th>
                        <td>
                            {{ $data->address }}
                        </td>
                    </tr>

                </tbody>
            </table>
            </div>
            <div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
            </div>
         </div>
      </div>

            <div class="col-lg-6">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                   Driver Bank Details
               </h3>
             <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
            
            <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                 
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $driver_bank_data->account_holder_name  ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Account Number
                        </th>
                        <td>
                            {{ $driver_bank_data->account_number ?? '' }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Ifsc code
                        </th>
                        <td>
                            {{ $driver_bank_data->ifsc_code ?? ''}}
                        </td>
                    </tr>
              

                </tbody>
            </table>
            </div>
            <div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>



<section class="content">
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                     Driver Documents
               </h3>
            <!-- <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div> -->
            </div>
            <div class="panel-body">
            
            <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                   

                            <tr>
                      <th>
                       Aadhar Card
                      </th>
                      <td>
                        @php
                     if(!empty($data_driver->aadhar_card)){
                     $url =$data_driver->aadhar_card;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" >
                      </td>
                    </tr>

                      <tr>
                      <th>
                       Pan card
                      </th>
                      <td>
                        @php
                     if(!empty($data_driver->pan_card)){
                     $url =$data_driver->pan_card;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" >
                      </td>
                    </tr>
                         <tr>
                      <th>
                       Driving License
                      </th>
                      <td>
                        @php
                     if(!empty($data_driver->driving_license)){
                     $url =$data_driver->driving_license;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" >
                      </td>
                    </tr>

                         <tr>
                      <th>
                       Plate Number
                      </th>
                      <td>
                        @php
                     if(!empty($data_driver->plate_number)){
                     $url =$data_driver->plate_number;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" >
                      </td>
                    </tr>

                      <tr>
                      <th>
                        Registration Paper
                      </th>
                      <td>
                        @php
                     if(!empty($data_driver->vehicle_registration_certificate)){
                     $url =$data_driver->vehicle_registration_certificate;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" >
                      </td>
                    </tr>
              
                      <tr>
                      <th>
                       Insurance
                      </th>
                      <td>
                        @php
                     if(!empty($data_driver->insurance)){
                     $url =$data_driver->insurance;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" >
                      </td>
                    </tr>

                </tbody>
            </table>
            </div>
            <div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
            </div>
         </div>
      </div>

            <div class="col-lg-6">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                   Driver Session
               </h3>
            <!-- <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div> -->
            </div>
            <div class="panel-body">
            
            <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <?php // echo "<pre>"; print_r($driver_session_data);die; ?>
            <table class="table table-bordered table-striped">
                <tbody>
                
                    <tr>
                        <th>
                            Start Time
                        </th>
                        <td>
                            {{ $driver_session_data->start_time ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            End Time
                        </th>
                        <td>
                            {{ $driver_session_data->end_time ?? '' }}
                        </td>
                    </tr>
                    

                </tbody>
            </table>
            </div>
            <div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>






@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>

@stop
