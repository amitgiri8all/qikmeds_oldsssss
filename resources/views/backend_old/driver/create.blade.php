@extends('backend/layouts/default')
@section('title')
Driver Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Driver Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Driver</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Driver
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.driver.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.driver.add.post') }}" id="driver" enctype="multipart/form-data">
                  @csrf
 
                  <!-- Driver Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Driver Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('name')}}"  name="name" placeholder="Driver name">
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

             <!-- Country Name -->
                  <!--  <div class="form-group col-sm-4">
                     <label for="text">Country</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('country')}}"  name="country" placeholder="Country Name">
                        {!! $errors->first('country', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> -->

             <!-- City Name -->
                  <!--  <div class="form-group col-sm-4">
                     <label for="text">City</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('city')}}" name="city" placeholder="City Name">
                        {!! $errors->first('city', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> -->

                      <!-- Country Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Email</label>
                     <div class="input-group col-xs-12">
                        <input type="email" class="form-control" value="{{old('email')}}" name="email" placeholder="Email Name">
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

             <!-- Password  -->
                   <div class="form-group col-sm-4">
                     <label for="text">Password</label>
                     <div class="input-group col-xs-12">
                        <input type="password" class="form-control" name="password" placeholder="Password Name">
                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Phone number  -->
                   <div class="form-group col-sm-4">
                     <label for="text">Mobile Number</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('mobile_number')}}"  name="mobile_number" placeholder="Mobile number">
                        {!! $errors->first('mobile_number', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                       <!-- Bike Details -->
                   <div class="form-group col-sm-4">
                     <label for="text">Address</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('address_name')}}" name="address_name" placeholder="Address Name">
                        {!! $errors->first('address_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


           <!-- Confirm Password  -->
       
           <div class="form-group col-sm-4" >
             <label for="text">Confirm Password</label>
             <div class="input-group col-xs-12">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation " >
                {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
             </div>
          </div>

                       <!-- Driving license certificate (i Image -->
                  <div class="form-group col-sm-4">
                     <label for="text">Aadhar Card</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="aadhar_card">
                        {!! $errors->first('aadhar_card', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>

                      <!-- Driving license certificate (i Image -->
                  <div class="form-group col-sm-4">
                     <label for="text">Pan Card</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="pan_card">
                        {!! $errors->first('pan_card', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>

                    <!-- Driving license certificate (i Image -->
                  <div class="form-group col-sm-4">
                     <label for="text">Driving license</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="driving_license">
                        {!! $errors->first('driving_license', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>

                              <!-- Driving license certificate (i Image -->
                  <div class="form-group col-sm-4">
                     <label for="text">Plate Number</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="plate_number">
                        {!! $errors->first('plate_number', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>

                  <!-- Vehicle registration certificate (i Image -->
                  <div class="form-group col-sm-4">
                     <label for="text"> Registration certificate</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="vehicle_registration_certificate">
                        {!! $errors->first('vehicle_registration_certificate', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>

                    <!-- Vehicle registration certificate (i Image -->
                  <div class="form-group col-sm-4">
                     <label for="text"> Insurance</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="insurance">
                        {!! $errors->first('insurance', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>

                   <!-- Driver Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Account Holder Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"   name="account_holder_name" placeholder="Account holder name">
                        {!! $errors->first('account_holder_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Driver Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Account Number</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"   name="account_number" placeholder="Account Number ">
                        {!! $errors->first('account_number', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Driver Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Account Number Confirmation</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"   name="account_number_confirmation" placeholder="Account Number Confirm">
                        {!! $errors->first('account_confirmation', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                   <!-- Driver Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Ifsc Code </label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"   name="ifsc_code" placeholder="Ifsc code">
                        {!! $errors->first('ifsc_code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
          

  
                   <!-- Bike Details -->
                 <!--   <div class="form-group col-sm-4">
                     <label for="text">Licence plate number</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('licence_plate_number')}}" name="licence_plate_number" placeholder="Licence plate number">
                        {!! $errors->first('licence_plate_number', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> -->

  
                   <!-- Bank account details Details -->
                  <!--  <div class="form-group col-sm-4">
                     <label for="text">Bank account details</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('bank_account_details')}}" name="bank_account_details" placeholder="Bank account details">
                        {!! $errors->first('bank_account_details', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> -->
   
                  

                  <!-- Upload documen Image -->
                <!--   <div class="form-group col-sm-4">
                     <label for="text">Upload documen</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="document">
                        {!! $errors->first('document', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div> -->
              


                  <!-- id_proof Image -->
                 <!--  <div class="form-group col-sm-4">
                     <label for="text">ID Proof</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="id_proof">
                        {!! $errors->first('id_proof', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div> -->
              
 
                  <!-- Driving license certificate (i Image -->
                 <!--  <div class="form-group col-sm-4">
                     <label for="text">Driving license</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="driving_license">
                        {!! $errors->first('driving_license', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div> -->
                  <!-- Driving license certificate (i Image -->
                 <!--  <div class="form-group col-sm-4">
                     <label for="text">Driver Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="image">
                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div> -->
               

                  <!-- Address Location -->
                  <div class="form-group col-sm-12" style="display: none">
                     <label for="text">Address Location</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="address_name" placeholder="Address name" class="form-control" name="address">
                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  
                  <!-- Address Location -->
                  <div class="form-group col-sm-12" style="display: none">
                     <label for="text">Latitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="lat" readonly placeholder="Address name" class="form-control" name="lat">
                        {!! $errors->first('lat', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  

                  <!-- Address Location -->
                  <div class="form-group col-sm-12" style="display: none">
                     <label for="text">Longitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="long" readonly placeholder="Longitute name" class="form-control" name="lng">
                        {!! $errors->first('lng', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>


                  <!-- Address Location -->
                  <div class="form-group col-sm-12" style="display: none">
                      <div class="input-group col-xs-12">
                  <div id="us3" style="width: 100%; height: 300px;"></div>
                        </span>
                     </div>
                  </div> 



                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<script type="text/javascript">
            $('#us3').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                    //radiusInput: $('#us3-radius'),
                    locationNameInput: $('#address_name')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script>
@stop