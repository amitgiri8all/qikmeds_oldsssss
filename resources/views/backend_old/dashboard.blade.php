@extends('backend/layouts/default')
{{-- Page title --}}
@section('title')
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/pages/calendar_custom.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" media="all" href="{{ asset('assets/vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
<meta name="_token" content="{{ csrf_token() }}">

 @stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Welcome to Dashboard</h1> 
    
   <ol class="breadcrumb">
      <li class="active">
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
         Dashboard
         </a>
      </li>
   </ol>
</section>
 <section class="content">
   <div class="row">

      <!-- Box 1 -->
      <a href="{{route('admin.customer.list')}}">
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Users</span>
                        <div class="number" id="myTargetElement1">
                        {{$users ? $users : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </a>

          <!-- Box 1 -->
      <a href="{{route('admin.driver.list')}}">
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Drivers</span>
                        <div class="number" id="myTargetElement1">
                        {{$drivers ? $drivers : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </a>

          <!-- Box 1 -->
      <a href="{{route('admin.doctor.list')}}">
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Doctors</span>
                        <div class="number" id="myTargetElement1">
                        {{$doctors ? $doctors : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </a>

          <!-- Box 1 -->
      <a href="{{route('admin.vendor.list')}}">
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Vendors</span>
                        <div class="number" id="myTargetElement1">
                        {{$vendors ? $vendors : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </a>

          <!-- Box 1 -->
      <a href="{{route('admin.subscriptionplan.list')}}">
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Premium plans</span>
                        <div class="number" id="myTargetElement1">
                        {{$premium_plans ? $premium_plans : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </a>
           <!-- Box 1 -->
      <a href="{{route('admin.category.list')}}">    
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Categories</span>
                        <div class="number" id="myTargetElement1">
                        {{$category ? $category : ""}}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </a>
           <!-- Box 1 -->
      <a href="{{route('admin.product.list')}}">    
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Products</span>
                        <div class="number" id="myTargetElement1">
                        {{ $product ? $product : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </a>   

     
           <!-- Box 1 -->
      <a href="{{route('admin.coupon.list')}}">     
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Coupons</span>
                        <div class="number" id="myTargetElement1">{{$coupon ? $coupon : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </a>
           <!-- Box 1 -->
    <a href="{{route('admin.order.list')}}" >     
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Order</span>
                        <div class="number" id="myTargetElement1">{{$order ? $order : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </a>
           <!-- Box 1 -->
         <!-- Trans label pie charts strats here-->
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig" style="display: none;">
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Queue</span>
                        <div class="number" id="myTargetElement1">00</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
           <!-- Box 1 -->
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig" style="display: none;">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Zones</span>
                        <div class="number" id="myTargetElement1">00</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
           <!-- Box 1 -->
      <a href="{{route('admin.contact.list')}}" >     
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Contact/Feedback</span>
                        <div class="number" id="myTargetElement1">
                        {{$contact ? $contact : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </a>
           <!-- Box 1 -->
      <a href="{{route('admin.page.list')}}" >
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>CMS</span>
                        <div class="number" id="myTargetElement1">{{$page ? $page : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </a>
           <!-- Box 1 -->
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig" style="display: none;">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Transactions</span>
                        <div class="number" id="myTargetElement1">000</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
           <!-- Box 1 -->
      <a href="{{route('admin.setting.list')}}" >
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Settings</span>
                        <div class="number" id="myTargetElement1">
                        {{$settings ? $settings : "" }}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </a>

            <!-- Box 1 -->
      <a href="{{route('admin.notification.list')}}" >
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Notifications</span>
                        <div class="number" id="myTargetElement1">{{ $admin_notifications ?? '' ;}}</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
     </a>
      
              <!-- Box 1 -->
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig" style="display: none;">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Account management</span>
                        <div class="number" id="myTargetElement1">00</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
              <!-- Box 1 -->
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig" style="display: none;">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Returns and refunds</span>
                        <div class="number" id="myTargetElement1">00</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
      
              <!-- Box 1 -->
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs"style="display: none;">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Bank Accounts</span>
                        <div class="number" id="myTargetElement1">00</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>


      
      
              <!-- Box 1 -->
      <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig" style="display: none;">
         <!-- Trans label pie charts strats here-->
         <div class="lightbluebg no-radius">
            <div class="panel-body squarebox square_boxs">
               <div class="col-xs-12 pull-left nopadmar">
                  <div class="row">
                     <div class="square_box col-xs-7 text-right">
                        <span>Wallet</span>
                        <div class="number" id="myTargetElement1">00</div>
                     </div>
                     <i class="livicon pull-right" data-name="order" data-l="true" data-c="#fff"
                        data-hc="#fff" data-s="70"></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
     
      
      
   </div>
 
 
   </section>
 
  
@stop
{{-- page level scripts --}}
@section('footer_scripts')
 

 


@stop