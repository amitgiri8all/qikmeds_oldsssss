<div style="font-family:Verdana,Arial;font-weight:normal;margin:0;padding:0;text-align:left;color:#333333;background-color:#fff;font-size:12px">

<table style="border-collapse:collapse;padding:0;margin:0 auto;font-size:12px" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0;width:100%" valign="top" align="center">
            <table style="border-collapse:collapse;padding:0;margin:0 auto;width:600px" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0">
                        <table style="border-collapse:collapse;padding:0;margin:0;width:100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:15px 0px 10px 5px;margin:0">
                                    <a href="#" style="color:#3696c2;float:left;display:block" target="_blank" data-saferedirecturl="#">
										<img src="{{ asset(config('constants.admin.logo')) }}" width="200" alt="logo" height="48" width="165">

                                </td>
                            </tr></tbody></table>
</td>
                </tr>
<tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:5px;margin:0;border:1px solid #ebebeb;background:#fff" valign="top">
                    



<table style="border-collapse:collapse;padding:0;margin:0;width:100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0">
            <table style="border-collapse:collapse;padding:0;margin:0" cellspacing="0" cellpadding="0" border="0"><tbody><tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0 1%;margin:0;background:#e1f0f8;border-right:1px dashed #c3ced4;text-align:center;width:58%">
                        <h1 style="font-family:Verdana,Arial;font-weight:700;font-size:16px;margin:1em 0;line-height:20px;text-transform:uppercase;margin-top:25px">Thank you for your order from Quality King.</h1>
                        <p style="font-family:Verdana,Arial;font-weight:normal;line-height:20px;margin:1em 0; font-size:12px;">You can check the status of your order by logging into your account.</p>
                    </td>
                    <td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:2%;margin:0;background:#e1f0f8;width:40%">
                        <h4 style="font-family:Verdana,Arial;font-weight:bold;margin-bottom:5px;font-size:12px;margin-top:13px">Order Questions?</h4>
                        <p style="font-family:Verdana,Arial;font-weight:normal;font-size:11px;line-height:17px;margin:1em 0">
                            
                            <b>Call Us:</b>
                            <a style="color:#3696c2;text-decoration:underline">{{ (config('sitesetting.Admin Phone No')) }}</a><br><span>10 Am to 7 Pm</span><br><b>Email:</b> <a href="#" style="color:#3696c2;text-decoration:underline" target="_blank">{{config('constants.mail.email')}}</a>
                            
                        </p>
                    </td>
                </tr></tbody></table>
</td>
    </tr>
<tr>
<td  style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:5px 15px;margin:0;text-align:center">
            <h3 style="font-family:Verdana,Arial;font-weight:normal;font-size:17px;margin-bottom:10px;margin-top:15px">Your Shipment <span>#{{$Order->shipment->ord_shp_id}}</span>
</h3>
            <p style="font-family:Verdana,Arial;font-weight:normal;font-size:11px;margin:1em 0 15px">Order #{{$Order->order_id}}</p>
        </td>
    </tr>
<tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0">
            
            <table style="border:1px solid #eaeaea;border-collapse:collapse;padding:0;margin:0;width:100%" width="650" cellspacing="0" cellpadding="0" border="0">
				<thead>
				<tr>
					<th style="font-size:13px;padding:3px 9px;font-family:Verdana,Arial;font-weight:normal" bgcolor="#EAEAEA" align="left">Item</th>
					<th style="font-size:13px;padding:3px 9px;font-family:Verdana,Arial;font-weight:normal" bgcolor="#EAEAEA" align="left">Sku</th>
					<th style="font-size:13px;padding:3px 9px;font-family:Verdana,Arial;font-weight:normal" bgcolor="#EAEAEA" align="center">Qty</th>
					<th style="font-size:12px;padding:3px 9px;font-family:Verdana,Arial;font-weight:normal" bgcolor="#EAEAEA" align="right">Subtotal</th>
				</tr>
				</thead>
				
				@foreach($Order->itemDetail as $value)	
				<tbody bgcolor="#F6F6F6">
				<tr>
					<td style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" valign="top" align="left">
					<strong style="font-size:11px;font-family:Verdana,Arial;font-weight:normal">{{$value->product_name}}</strong><br>
				
					<? $attri = json_decode($value->product_option, true);?>
							@foreach($attri as $key=>$attribute)
							<strong style="font-size:11px;font-family:Verdana,Arial;font-weight:normal"><small>{{$key ." : ". $attribute}}</small></strong><br>   
							@endforeach
					</td>
					<td style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" valign="top" align="left">{{$value->product_sku}}</td>
					<td style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" valign="top" align="center">{{$value->quantity_order}}</td>
					<td style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" valign="top" align="right">
					<span style="font-family:Helvetica Neue&quot;,Verdana,Arial,sans-serif">{{config('constants.frontend.currency')}}{{$value->row_total}}</span>            
					</td>
				</tr>
				</tbody>
				@endforeach
		
				
				
				<tbody>
					<tr>
					<td colspan="3" style="padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0; font-size:12px;" align="right">
						Subtotal                    </td>
						<td style=" font-size:12px; padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" align="right">
						<span style="font-family:Helvetica Neue&quot;,Verdana,Arial,sans-serif">{{config('constants.frontend.currency')}}{{$Order->sub_total}}</span></td>
					</tr>

					<tr style="padding-bottom:5px">
						<td colspan="3" style=" font-size:12px; padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" align="right">
						Shipping &amp; Handling                    </td>
						<td style="  font-size:12px; padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" align="right">
						<span  style="font-family:&quot;Helvetica Neue&quot;,Verdana,Arial,sans-serif">{{config('constants.frontend.currency')}}{{$Order->shipping_amount}}</span></td>
					</tr>
					
					<tr>
						<td colspan="3" style=" font-size:12px; padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" align="right">
						Tax            </td>
						<td style=" font-size:12px; padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" align="right"><span style="font-family:&quot;Helvetica Neue&quot;,Verdana,Arial,sans-serif">{{config('constants.frontend.currency')}}{{$Order->tax_amount}}</span></td>
					</tr>
					
					<tr >
						<td colspan="3" style="padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" align="right">
						<strong style="font-family:Verdana,Arial;font-weight:normal">Grand Total</strong>
						</td>
						<td style="padding:3px 9px;font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0" align="right">
						<strong style="font-family:Verdana,Arial;font-weight:normal"><span  style="font-family:&quot;Helvetica Neue&quot;,Verdana,Arial,sans-serif">{{config('constants.frontend.currency')}}{{$Order->grand_total}}</span></strong>
						</td>
					</tr>
					
				</tbody>


</table>


<table style="border-collapse:collapse;padding:0;margin:0;width:100%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
		<tr>
			<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:10px 15px 0;margin:0;padding-top:10px;text-align:left">
			<h6 style="font-family:Verdana,Arial;font-weight:700;font-size:12px;margin-bottom:0px;margin-top:5px;text-transform:uppercase">Ship to:</h6>
			<p style="font-family:Verdana,Arial;font-weight:normal;font-size:12px;line-height:18px;margin-bottom:15px;margin-top:2px"><span >{{$Order->address->first_name}}<br>

			{{$Order->address->address}}, {{$Order->address->city}}<br>
			{{$Order->address->st_name}},  {{$Order->address->cntry_name}}, {{$Order->address->pincode}}<br>
			India<br>
			T: {{$Order->address->phone}}

			</span>
			</p>
			</td>

		</tr>
		
		
		<tr>
			<td  style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:10px 15px 0;margin:0;text-align:left;padding-bottom:10px">
			<h6 style="font-family:Verdana,Arial;font-weight:700;text-align:left;font-size:12px;margin-bottom:0px;margin-top:5px;text-transform:uppercase">Shipping method:</h6>
			<p style="font-family:Verdana,Arial;font-weight:normal;text-align:left;font-size:12px;margin-top:2px;margin-bottom:30px;line-height:18px;padding:0">{{$Order->shipping_method}}</p>
			</td>

			<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:10px 15px 0;margin:0;text-align:left;padding-bottom:10px">
			<h6 style="font-family:Verdana,Arial;font-weight:700;text-align:left;font-size:12px;margin-bottom:0px;margin-top:5px;text-transform:uppercase">Payment method:</h6>
			<p style="font-family:Verdana,Arial;font-weight:normal;text-align:left;font-size:12px;margin-top:2px;margin-bottom:30px;line-height:18px;padding:0"><strong style="font-family:Verdana,Arial;font-weight:normal;text-align:left">{{$Order->payment_method}}</strong></p>
			</td>
		</tr>
		<tr>
			<td  style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:10px 15px 0;margin:0;text-align:left;padding-bottom:10px">
			<h6 style="font-family:Verdana,Arial;font-weight:700;text-align:left;font-size:12px;margin-bottom:0px;margin-top:5px;text-transform:uppercase">Shipped by:</h6>
			<p style="font-family:Verdana,Arial;font-weight:normal;text-align:left;font-size:12px;margin-top:2px;margin-bottom:30px;line-height:18px;padding:0">{{$Order->shipment->courier_name}}</p>
			</td>

			<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:10px 15px 0;margin:0;text-align:left;padding-bottom:10px">
			<h6 style="font-family:Verdana,Arial;font-weight:700;text-align:left;font-size:12px;margin-bottom:0px;margin-top:5px;text-transform:uppercase">Tracking Number:</h6>
			<p style="font-family:Verdana,Arial;font-weight:normal;text-align:left;font-size:12px;margin-top:2px;margin-bottom:30px;line-height:18px;padding:0"><strong style="font-family:Verdana,Arial;font-weight:normal;text-align:left">{{$Order->shipment->tracking_detail}}</strong></p>
			</td>
		</tr>
		
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
</table>

</td>
                </tr>
</tbody></table>
<h5  style="font-family:Verdana,Arial;font-weight:normal;text-align:center;font-size:22px;line-height:32px;margin-bottom:75px;margin-top:30px">Thank you,Quality King !</h5>
        </td>
    </tr></tbody></table><div class="yj6qo"></div><div class="adL">

</div></div>
