<style>
.clr {
	clear:both;
	}
</style>
<div class="panel-body" style="border:1px solid #ccc;padding:0;margin:0;">
<div style="padding: 15px; width:100%;">
	 <div style="padding:15px; margin-top:5px; width:100%;">
                                <div style="display: block;">
                                    <div style="float:left; width:50%;">
                                      
                                      <img src="{{ asset(config('constants.admin.logo')) }}" width="200" alt="logo" height="65">
                                      
                               
                                    </div>
                                    <div style="width:29%; float:right;">
                                        <div  style="float:right;">
                                            <strong></strong><br>
                                            <strong>{{ config('sitesetting.Site Name') }}</strong>
                                            <br/>
                                           {{ config('sitesetting.Site Address') }}
                                                     <br>
                                           
                                           {{ config('sitesetting.Admin Email') }}<br/>
                                            <strong>GST No:</strong>{{ config('sitesetting.GST Number') }}
                                        </div>
                                    </div>
                                     <div style="clear:both;"></div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                    
                      <div class="row" style="padding: 10px 0; width:100%;">
                                    <div style="float:left; width:40%;">
                                        <strong>Invoice To:</strong><br>
                                         {{ucfirst($order->address->first_name)}} {{ucfirst($order->address->last_name)}}<br>
                                        {{$order->address->address}}<br>
                                        {{ucfirst($order->address->st_name)}}, {{$order->address->pincode}}<br>
                                        {{ucfirst($order->address->cntry_name)}}<br>

                                        Contact No: {{$order->address->phone}}
                                    </div>
                                    <div style="float:right; width:20%;">
                                        <div id="invoice" style="text-align:center; background:#ccc; border-radius:4px;padding: 5px;">
	                                      
                                            <h4><strong>Invoice: {{$order->invoice_id}}</strong></h4>
                                            
                                            <h4><strong>Order: {{$order->order_id}}</strong></h4>
                                            <h4><strong>{{date("d M ,Y",strtotime($order->created_at))}}</strong></h4>
                                        </div>
                                    </div>
                                     <div class="clr"></div>
                                </div>
                                
                            
                      <div style="width:100%;">
                                            <table style="width:100%;">
                                                <thead>
                                                <tr style="background:#ccc;">
                                                    <th>SI No.</th>
                                                    <th>Details</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price(₹)</th>
                                                    <th>Tax%</th>
                                                     <th>Tax Amount(₹)</th>
                                                     <th>Discount</th>
                                                    <th>NetSubtotal(₹)</th>
                                                </thead>
                                              <tbody>
	                                            @foreach($OrderProduct as $key=> $item)   
	                                              
                                                <tr>
                                                    <td>{{$key+1}}.</td>
                                                    <td>{{$item->product_name}}</td>
                                                    <td>{{(int)@$item->quantity_order}}</td>
                                                    <td>₹{{number_format(@$item->price-$item->tax_amount,2)}}</td>
                                                     <td>{{$item->tax_percentage}}%</td>
                                                      <td>₹{{number_format( $item->tax_amount, 2, '.', '') }}</td>
                                                 
                                                      <td>₹{{number_format(@$item->base_price - $item->price,2)}}</td>
                                                    
                                                      <td>₹{{number_format(@$item->row_total,2)}}</td>
                                                </tr>
                                             
                                               @endforeach
                                              
                                                 <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Net Total</td>
                                                   <td>₹{{number_format($order->sub_total,2)}}</td>
                                                </tr>
                                                
                                                     <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Discount</td>
                                                   <td>₹{{number_format($order->discount_amount,2)}}</td>
                                                </tr>
                                                <tr>
													  <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Tax </td>
                                                   <td>₹{{number_format($order-> tax_amount ,2)}}</td>
                                                </tr>
                                                
                                           
                                               
                                              
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                  <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td><strong>TOTAL</strong></td>
                                                     <td><strong>₹{{number_format($order->grand_total,2)}}</strong></td>
                                                </tr>
                                                </tbody>
                                        
                                        
                                        
                                        
                                            </table>
 
                       </div>
                               
                        <div class="panel panel-success" style="background:#ccc;padding:10px;" id="footer-bg">
                                    <div class="row">
                                        <div class="col-md-5 pull-left">
                                            <strong>Payment Details</strong><br>
                                            <strong>
                                            @if($order->payment_method =='Pay u' or $order->payment_method=='Paytm' )
                                            Online Payment(Credit Card,Debit Card,Net Banking) 
                                            @else
                                            Cash on delivery 
                                            @endif
                                            </strong><br>
                                                                                    </div>
                                        <div class="col-md-5 pull-right">
                                            <div class="pull-right">
                                               <br>
                                                                                            </div>
                                        </div>
                                    </div>

                                </div>
                           
                                   
                          
                           
</div>
</div>
