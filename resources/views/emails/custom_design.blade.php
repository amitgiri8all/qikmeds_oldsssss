@extends('emails/layouts/default')

@section('content')
    <p>Hello ,</p>

    <p>We have received a new custom design mail.</p>

    <p>The provided details are:</p>

    <p>Name: {{ $data['contact-name'] }}</p>
    
     <p>Mobile: {{ $data['contact-mobile'] }}</p>

    <p>Email: {{ $data['contact-email'] }}</p>

    <p>Description: {{ $data['contact-msg'] }}  </p>

@stop
