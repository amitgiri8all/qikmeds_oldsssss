
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
    
    

    
    
    
    
    <table cellspacing="0" cellpadding="0" align="center" style="width: 100%; max-width: 700px; background:#f9f9f9;margin:0 auto;padding:0;border:1px solid #e8e8e8;font-family:Arial;font-size:12px">

<tbody><tr><td><table style="border-spacing:0;background:#ffbf2e" width="100%">
  <tbody>
    <tr>
      <td style="padding:15px 0px 15px 20px" align="center">
        <div>
          <a href="" style="color:#ffffff;text-decoration:none" rel="noreferrer" target="_blank"><img src="logo.png" alt="" style="outline:none;display:block" width="200px" class="CToWUd"></a>
        </div>
      </td>
      
    </tr>
  </tbody>
</table>
    
    
    
    <table width="100%" style="font-size: 15px;">
  <tbody>
    <tr>
      <td style="padding:15px" align="center">
        <h3 style="font-size: 26px">Hello Mukesh</h3>
    
<p style="font-size: 16px;"><strong>Thank you for joining City Bird Rides.</strong></p>
          <p>We’d like to confirm that your account was created successfully.</p>
          <p>If you experience any issues logging into your account, reach out to us at <a href="mailto:support@citybirdrides.com">support@citybirdrides.com</a></p>
          <p>&nbsp;</p>
          <p>Best Regards,</p>
          <p><strong>Team City Bird Rides</strong></p>
      </td>
      
    </tr>
  </tbody>
</table>




<table style="border-spacing:0;background:#f0f0f0" width="100%">
      <tbody>
        <tr>
          <td style="padding:0px 20px 20px 20px;font-family:Arial,Helvetica;font-size:15px;color:#262626;font-weight:normal;line-height:1.4" align="center">
            <table style="border-spacing:0" width="100%">
              <tbody>

                <tr>
                  <td style="font-family:Montserrat,Droid Sans,Lucida Sans Unicode,Lucida Grande,Helvetica,Georgia,Arial;font-size:14px;color:#2c2c2c;font-weight:normal;line-height:22px;padding:0px" align="center">&nbsp;</td>
                </tr>
                
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style="padding:0px 0px 20px 0px" align="left">
            <table style="border-spacing:0" width="100%">
              <tbody>
                <tr>
                  <td style="padding:0px 0px 10px 0px" align="center">
                    <div style="display:inline-block;width:100%;max-width:135px;padding:0 0 10px 0">
                      <a href="" rel="noreferrer" target="_blank" ><img style="width:134px;outline:0" src="apple_store.png" alt="Download iOS App" class="CToWUd"></a>
                    </div>
                    <div style="display:inline-block;width:100%;max-width:135px;padding:0 0 10px 0;text-align:center">
                      <a href="" target="_blank" ><img style="width:134px;outline:0" src="google_play.png" alt="Download Android App" class="CToWUd"></a>
                    </div>
                  </td>
                </tr>
               
                <tr>
                  <td style="padding:0px 0px 20px 0px;font-family:Montserrat,Arial,Helvetica;font-size:12px;color:#888888;font-weight:normal;line-height:1" align="center">
                  	<a style="padding:5px 10px;color:#888888" href="">Contact Us</a> | 

                  	<a style="padding:5px 10px;color:#888888" href="">About Us</a> 
                  </td></tr></tbody></table></td>
                </tr>
                <tr>
                  <td style="padding:0px 20px 0px 20px;font-family:Montserrat,Arial,Helvetica;font-size:11px;color:#999999;font-weight:normal;line-height:16px" align="center">
                    <table>
                      <tbody><tr>
                      	 <td style="padding:0 10px"><a href="" target="_blank"><img src="fb.png" alt="" width="36"></a></td>
                        <td style="padding:0 10px"><a href="" target="_blank">
                            <img src="twitter.png" alt="" width="36"></a></td>
                        <td style="padding:0 10px"><a href="" target="_blank"><img src="youtube.png" alt="" width="36" class="CToWUd"></a></td>
                      </tr>
                    </tbody></table>
                  </td>
                </tr>
                <tr>
                  <td style="padding:20px 20px 0px 20px;font-family:Montserrat,Arial,Helvetica;font-size:11px;color:#999999;font-weight:normal;line-height:16px" align="center">citybird © 2022. All Rights Reserved.<div>Shop no 03, Vashishtha kunj, Koyal Ghati, Haridwar Road, Rishikesh</div></td>
                </tr>
          
          <tr>
                  <td style="padding:20px 20px 0px 20px;font-family:Montserrat,Arial,Helvetica;font-size:11px;color:#999999;font-weight:normal;line-height:16px" align="center">&nbsp;</td>
                </tr>
               
              </tbody>
          </table>
        
</td></tr></tbody></table>
    
    
</body>
</html>
