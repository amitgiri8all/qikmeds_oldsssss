@extends('emails/layouts/default')

@section('content')

<table border="0" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" style=" box-shadow: 0 0 5px #ccc; background:#fff
    " width="600">
				<tbody>
					<tr>
						<td align="center" height="100" style=" background:#fafafa; border-bottom:solid 2px orange" valign="middle"><img  src="{{ asset(config('constants.frontend.logo')) }}" height="40" width="200" /></td>
					</tr>
					<tr>
						<td>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td width="15">&nbsp;</td>
									<td width="570">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tbody>
											<tr>
												<td height="15">&nbsp;</td>
											</tr>
											<tr>
												<td width="530">
												<table cellpadding="0" cellspacing="0" style="border:solid 1px #eee1e0;  -moz-border-radius:3px;

-webkit-border-radius:3px;
border-radius:3px;                      " width="100%">
													<tbody>
														<tr>
															<td style="font-family:Arial, Helvetica, sans-serif; padding:10px; font-size:13px; color:#ec7475;">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tbody>
																	<tr>
																		<td style="font-family:Arial, Helvetica, sans-serif; padding:10px; font-size:13px; color:#ec7475;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tbody>
																				<tr>
																					<td align="left" height="30" style="font-size:18px; font-family:Verdana, Geneva, sans-serif; color:#404348; padding-bottom:22px;" valign="middle"><span style="font-family:lucida sans unicode,lucida grande,sans-serif;"><strong>Dear {{ $data['firstname'] }} ,</strong></span></td>
																				</tr>
																				<tr>
																					<td align="left" style="    background: orange;
    color: #fff;
    font-size: 17px;
    height: 40px;
    line-height: 27px;
    text-align: center;
    text-transform: uppercase; color:#fff;" valign="middle"><span style="font-family:trebuchet ms,helvetica,sans-serif;">Your Password is Reset Successfully</span></td>
																				</tr>
																				<tr>
																					<td align="left" valign="top">
																					<p style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#303030; padding:12px 0 22px"><span style="font-family:georgia,serif;">Your New Temporary Password is <span style="color:green">{{ $data['password'] }}</span><br />
																					<br />
																					Thank you for being the part of Quality King.<br>
																					<br>
																					</span></p>
																					</td>
																				</tr>
																				
																				<tr>
																					<td align="left" valign="top">
																					<p style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#303030; padding:12px 0 22px"><span style="font-family:georgia,serif;"><br />
																					Don&rsquo;t hesitate in contacting us if you have any questions regarding your account, or about the website in general ;-)</span></p>
																					</td>
																				</tr>
																				<tr>
																					<td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; font-weight:bold; color:#171717; line-height:22px; padding-bottom:15px;" valign="top"><span style="font-family:georgia,serif;">Your Login Details:-<br />
																					Email:&nbsp;Email:{{ $data['email'] }}<br />
																					Password: </span></td>
																				</tr>
																				<tr>
																					<td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:17px;
                                  color:orange;" valign="top"><span style="font-family:georgia,serif;"><span style="font-size: 11px;">Regards,<br />
																					<strong>Quality King Team</strong></span></span></td>
																				</tr>
																			</tbody>
																		</table>
																		</td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
											<tr>
												<td height="15">&nbsp;</td>
											</tr>
										</tbody>
									</table>
									</td>
									<td width="15">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" style="padding:10px;background: none repeat scroll 0 0 #f7f7f7;
    border-top: 1px solid #CCCCCC;
        font-family: Arial, Helvetica, sans-serif; color:#333; font-size:11px;" valign="middle"></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>



@stop
