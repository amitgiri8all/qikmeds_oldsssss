<div class="panel-body" style="border:1px solid #ccc;padding:0;margin:0;">
<div class="row" style="padding: 15px;">
	 <div class="row" style="padding: 15px;margin-top:5px;">
                                <div  class="header">
                                    <div class="col-md-6 pull-left">
                                      
                                      <img src="{{ asset(config('constants.admin.logo')) }}" width="200" alt="logo" height="65">
                                      
                               
                                    </div>
                                    <div class="col-md-6 pull-right">
                                        <div class="pull-right">
                                            <strong></strong><br>
                                            <strong>{{ config('constants.frontend.DefaultTitle') }}</strong><br>
                                           
                                           {{ config('constants.address.address') }}
                                                     <br>
                                           {{ config('constants.address.email') }}<br/>
                                            <strong>VAT No:</strong>{{ config('constants.admin.VAT') }}<br/>
                                            <strong>CST No:</strong>{{ config('constants.admin.CST') }}
                                        </div>
                                    </div>
                                     <div class="clr"></div>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                    
                      <div class="row" style="padding: 10px 0;">
                                    <div class="col-md-9 col-xs-6" style="margin-top:5px;">
                                        <strong>Invoice To:</strong><br>
                                         {{ucfirst($Order->address->first_name)}} {{ucfirst($Order->address->last_name)}}<br>
                                        {{$Order->address->address}}<br>
                                        {{ucfirst($Order->address->city)}}, {{$Order->address->pincode}}<br>
                                        {{ucfirst($Order->address->st_name)}}<br>

                                        Contact No: {{$Order->address->phone}}
                                    </div>
                                    <div class="col-md-3 col-xs-6" >
                                        <div id="invoice" style="text-align:center; background:#ccc; border-radius:4px;padding: 5px;">
	                                      
                                            <h4><strong>Invoice: {{$Order->invoice_id}}</strong></h4>
                                            
                                            <h4><strong>Order: {{$Order->order_id}}</strong></h4>
                                            <h4><strong>{{date("d M ,Y",strtotime($Order->created_at))}}</strong></h4>
                                        </div>
                                    </div>
                                </div>
                                
                            
                      <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr style="background:#ccc;">
                                                    <th>SI No.</th>
                                                    <th>Details</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price(₹)</th>
                                                    <th>Tax%</th>
                                                     <th>Tax Amount(₹)</th>
                                                     <th>Discount</th>
                                                    <th>NetSubtotal(₹)</th>
                                                </thead>
                                              <tbody>
	                                            @foreach($Order->itemDetail as $key=> $item)   
	                                              
                                                <tr>
                                                    <td>{{$key+1}}.</td>
													<td>{{$item->product_name}}
														<? $attri = json_decode($item->product_option, true);?>
														@foreach($attri as $key=>$attribute)
														<small class="cart_ref">{{$key ." : ". $attribute}}</small><br>   
													@endforeach
													 </td>
                                                    <td>{{$item->quantity_order}}</td>
                                                    <td>{{config('constants.frontend.currency')}}{{$item->price}}</td>
                                                     <td>{{config('constants.frontend.currency')}}{{$item->tax_percentage}}%</td>
                                                      <td>{{config('constants.frontend.currency')}}{{$item->tax_amount}}</td>
                                                 
                                                      <td>{{config('constants.frontend.currency')}}</td>
                                                    
                                                      <td>{{config('constants.frontend.currency')}}{{$item->row_total}}</td>
                                                </tr>
                                             
                                               @endforeach
                                              
                                                 <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Net Total</td>
                                                   <td>{{config('constants.frontend.currency')}}{{$Order->sub_total}}</td>
                                                </tr>
                                                
                                                     <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Discount</td>
                                                   <td>{{config('constants.frontend.currency')}}{{$Order->discount_amount}}</td>
                                                </tr>
                                                <tr>
													  <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Tax </td>
                                                   <td>{{config('constants.frontend.currency')}}{{$Order-> tax_amount}}</td>
                                                </tr>
                                                
                                           
                                               
                                              
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                  <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td><strong>TOTAL</strong></td>
                                                     <td><strong>{{$Order->grand_total}}</strong></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        
                                        
                                        
                                        
                                            </table>
                       </div>
                               
                        <div class="panel panel-success" style="background:#ccc;padding:10px;" id="footer-bg">
                                    <div class="row">
                                        <div class="col-md-5 pull-left">
                                            <strong>Payment Details</strong><br>
                                            <strong>
                                            @if($Order->payment_method =='Pay u' or $Order->payment_method=='Paytm' )
                                            Online Payment(Credit Card,Debit Card,Net Banking) 
                                            @else
                                            Cash on delivery 
                                            @endif
                                            </strong><br>
                                                                                    </div>
                                        <div class="col-md-5 pull-right">
                                            <div class="pull-right">
                                               <br>
                                                                                            </div>
                                        </div>
                                    </div>

                                </div>
                           
                                   
                          <div  style="padding:50px;" >  
                               <a  >  <button type="submit" class="btn btn-primary " >Print Invoice </button></a> 
                                    
                           </div>
                           
</div>
</div>
