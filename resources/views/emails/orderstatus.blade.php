@extends('emails/layouts/default')

@section('content')
    <p>Hello , {{ ucfirst($data['user']) }}</p>

    <p>Your Order No - #{{ $order->order_id }} has been {{ ucfirst($data['state']) }}</p>
    <p>Message : {{ $data['comment'] }} </p>
    <p>The provided details are:</p>

      <div class="row" style="padding:15px;">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>SI No.</th>
                                                    <th>Details</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price(₹)</th>
                                                    <th>Tax%</th>
                                                     <th>Tax Amount(₹)</th>
                                                     <th>Discount</th>
                                                    <th>NetSubtotal(₹)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
	                                            @foreach($OrderProduct as $key=> $item)   
	                                              
                                                <tr>
                                                    <td>{{$key+1}}.</td>
                                                    <td>{{$item->product_name}}</td>
                                                    <td>{{(int)@$item->quantity_order}}</td>
                                                    <td>₹{{number_format(@$item->price,2)}}</td>
                                                     <td>{{$item->tax_percentage}}%</td>
                                                      <td>₹{{number_format( $item->tax_amount, 2, '.', '') }}</td>
                                                 
                                                      <td>₹{{number_format(@$item->base_price - $item->price,2)}}</td>
                                                    
                                                      <td>₹{{number_format(@$item->row_total,2)}}</td>
                                                </tr>
                                             
                                               @endforeach
                                              
                                                 <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Net Total</td>
                                                   <td>₹{{number_format($order->sub_total,2)}}</td>
                                                </tr>
                                                
                                                     <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Discount</td>
                                                   <td>₹{{number_format($order->discount_amount,2)}}</td>
                                                </tr>
                                                <tr>
													  <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td>Tax </td>
                                                   <td>₹{{number_format($order-> tax_amount ,2)}}</td>
                                                </tr>
                                                
                                           
                                               
                                              
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                  <td></td>
                                                    <td></td>
                                                     <td></td>
                                                    <td></td>
                                                    <td><strong>TOTAL</strong></td>
                                                     <td><strong>₹{{number_format($order->grand_total,2)}}</strong></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div style="background-color: #eee;padding:15px;" id="footer-bg">
                                                                       <hr>
                                                                                                       </div>
                            </div>

@stop
