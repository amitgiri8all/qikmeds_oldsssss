@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<style type="text/css">
    .login_form .ajax_message { font-family: 'Roboto', sans-serif; font-weight:400; font-size:15px; color:#FD3C43; line-height:20px; text-decoration:none; margin-top:-9px; margin-bottom: 6px; display:inline-block; margin-left:10px; /*text-align:center;*/width:100%}
.login_form .instruction_message { font-family: 'Roboto', sans-serif; font-weight:400; font-size:14px; color:#fcaf17; line-height:20px; text-decoration:none; margin-top:14px; display:inline-block; margin-left:10px; }
 
</style>

@stop


{{-- Page content --}}
@section('content')
 


<section class="middle_section p_30">
    <div class="container">
        <div class="login_blk">
            <div class="row align-items-center">
                <div class="col-sm-7">
                    <div class="login_img"><img src="{{asset('public/frontend/images/pharmacy.png')}}" alt="pharmacy"></div>
                </div>
                <div class="col-sm-5">
                    <div class="login_detail">
                       <form method="POST" id="frm_login"> 
                        <!-- login -->
                           <div class="login_form">
                             <h2>Sign In / Sign Up</h2>
                        <p>Sign up or Sign in to access your orders, special offers, health tips and more!</p>
                              <div class="login">
                                <div class="form-group">
                                        <label>Phone Number</label>
                                        <span>+91</span>
                                        <input type="tel" name="mobile_number" autocomplete="off" id="mobile_number" maxlength="10" minlength="10" class="form-control" placeholder="Enter your mobile no">
                                     </div>
                                     <span class="error_message ajax_message"></span>
                                </div>  

                                    <div class="otp_boxes">
                                        <small>We have sent 6 digit OTP</small>
                                        <ul>
                                            <li><input type="text" class="form-control" name=""></li>
                                            <li><input type="text" class="form-control" name=""></li>
                                            <li><input type="text" class="form-control" name=""></li>
                                            <li><input type="text" class="form-control" name=""></li>
                                            <li><input type="text" class="form-control" name=""></li>
                                            <li><input type="text" class="form-control" name=""></li>
                                        </ul>
                                        <small>Waiting for OTP... 6 Sec</small>
                                        <a href="#">Resend</a>
                                    <div class="form_btns verify">
                                        <button class="cmn_btn">Verify</button>
                                        <div class="or_txt"><span>OR</span></div>
                                        <button class="bdr_btn">Login Using Password</button>
                                    </div>
                                                                      </div>
  

                                <div class="form_btns otp">
                                    <button type="button" id="use_otp" class="cmn_btn">Use OTP</button>
                                    <div class="or_txt"><span>OR</span></div>
                                    <button class="bdr_btn">Use Password</button>
                                </div>



                            </div>   
                            <!-- Register -->

                    <div class="login_detail register">
                        <h2>Create Account</h2>
                        <div class="form-group">
                            <label>Email ID</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Enter your email id">
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter your first name">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="email" class="form-control" id="last_name" name="last_name" placeholder="Enter your last name">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password">
                            <a href="#"><i class="fa fa-eye"></i></a>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Enter your confirm password">
                            <a href="#"><i class="fa fa-eye"></i></a>
                        </div>
                        <div class="otp_boxes">
                            <span>Verifying Number</span>
                            <small>We have sent 6 digit OTP</small>
                            <ul>
                                <li><input type="text" class="form-control" name=""></li>
                                <li><input type="text" class="form-control" name=""></li>
                                <li><input type="text" class="form-control" name=""></li>
                                <li><input type="text" class="form-control" name=""></li>
                                <li><input type="text" class="form-control" name=""></li>
                                <li><input type="text" class="form-control" name=""></li>
                            </ul>
                            <small>Waiting for OTP... 6 Sec</small>
                            <a href="#">Resend</a>
                        </div>
                        <div class="form_btns">
                            <button id="register_btn" class="cmn_btn">Register</button>
                        </div>
                        
                       
                    </div>
                       </form>
                        <div class="login_social">
                            <span>Continue with</span>
                            <a href="#" class="fb"><i class="lab la-facebook-f"></i> Facebook</a>
                            <a href="#" class="google"><i class="lab la-google"></i> Google</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
 
@stop


{{-- page level scripts --}}
@section('footer_scripts')
 
  <script type="text/javascript">
            var url= "{!! route('account.login.post') !!}";
            var signup= "{!! route('account.sign_up.post') !!}";
            var token = '{{ csrf_token() }}';

$('.otp').css('display','block');
$('.verify').css('display','none');
$('.otp_boxes').css('display','none');


$('.login_form').css('display','block');
 $('.register').css('display','none');


// JavaScript Document
jQuery(function($) {
            
            $("#use_otp").click(function(e) {
                e.preventDefault();
                var mobile_number   = $("#mobile_number").val();
                alert ("ok")
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {'_token': token,mobile_number:mobile_number},
                    dataType : 'json',
                    beforeSend : function() {
                        $("#use_otp").html('Validating...');
                        $("#frm_login").find("button[type=submit]").attr("disabled", "disabled");
                    },
                    success : function(json)
                    {
                        console.log(json)
                        // csrf token update
                        if(json['mobile_number']) 
                        {
                            $(".error_message").fadeIn();
                            $(".error_message").html(json['mobile_number']);
                            $(".help-block").html('<strong>'+json['mobile_number']+'</strong>');
                            $("#use_otp").html('Send');
                            $("#frm_login").find("button[type=submit]").removeAttr("disabled");
                            $('.error_message').removeClass('ajax_message');
                            $('.error_message').removeClass('instruction_message');
                            $(".error_message").addClass('ajax_message animated shake');
                        }

                         if(json['mobile_number_save']){ 
                            $('.register').css('display','block');
                            $('.login_form').css('display','none');
                         } 

                         if(json['mobile_number_exit']){ 
                            alert('ok exit')
                            $('.otp_boxes').css('display','block');
                            $('.otp').css('display','none');
                            $('.verify').css('display','block');
                            $('.error_message').removeClass('ajax_message');
                            $('.error_message').removeClass('instruction_message');
                         } 

                        if(json['faild']) 
                        {
                            $(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> '+json['faild']+'</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();
                            $('.help-block').hide();
                            $("#use_otp").html('Send');
                            $("#frm_login").find("button[type=submit]").removeAttr("disabled");

                            return false;
                        }
                        
                        if(json['success']) 
                        {
                            $(".main_error").html('<strong><i class="fa fa-check-circle"></i> '+json['success']+'</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-danger').addClass('alert-success').show();
                          //  window.setTimeout(function(){window.location.href = json['redirect_url'];},3000);
                        }
                        
                    },
                    error:  function(json) 
                    {
                        //alert("ok")
                        //console.log(json)
                        /* alert('Sorry, an error while ajax call'); */
                        $(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> Something Went Wrong Please Contact to administrator!</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();
                    }
                });
                
            });
        });
      </script>

<!-- OPT ========================= -->

 <script type="text/javascript">
     
     jQuery(function($) {
            
            $("#register_btn").click(function(e) {
                e.preventDefault();
                var first_name   = $("#first_name").val();
                alert ("register")
                $.ajax({
                    url: signup,
                    type: 'post',
                    data: {'_token': token,first_name:first_name},
                    dataType : 'json',
                    beforeSend : function() {
                        $("#use_otp").html('Validating...');
                        $("#frm_login").find("button[type=submit]").attr("disabled", "disabled");
                    },
                    success : function(json)
                    {
                        console.log(json)
                        // csrf token update
                        if(json['mobile_number']) 
                        {
                            $(".error_message").fadeIn();
                            $(".error_message").html(json['mobile_number']);
                            $(".help-block").html('<strong>'+json['mobile_number']+'</strong>');
                            $("#use_otp").html('Send');
                            $("#frm_login").find("button[type=submit]").removeAttr("disabled");
                            $('.error_message').removeClass('ajax_message');
                            $('.error_message').removeClass('instruction_message');
                            $(".error_message").addClass('ajax_message animated shake');
                        }

                         if(json['mobile_number_save']){ 
                            $('.register').css('display','block');
                            $('.login_form').css('display','none');
                         } 

                         if(json['mobile_number_exit']){ 
                            alert('ok exit')
                            $('.otp_boxes').css('display','block');
                            $('.otp').css('display','none');
                            $('.verify').css('display','block');
                            $('.error_message').removeClass('ajax_message');
                            $('.error_message').removeClass('instruction_message');
                         } 

                        if(json['faild']) 
                        {
                            $(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> '+json['faild']+'</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();
                            $('.help-block').hide();
                            $("#use_otp").html('Send');
                            $("#frm_login").find("button[type=submit]").removeAttr("disabled");

                            return false;
                        }
                        
                        if(json['success']) 
                        {
                            $(".main_error").html('<strong><i class="fa fa-check-circle"></i> '+json['success']+'</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-danger').addClass('alert-success').show();
                          //  window.setTimeout(function(){window.location.href = json['redirect_url'];},3000);
                        }
                        
                    },
                    error:  function(json) 
                    {
                        //alert("ok")
                        //console.log(json)
                        /* alert('Sorry, an error while ajax call'); */
                        $(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> Something Went Wrong Please Contact to administrator!</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();
                    }
                });
                
            });
        });

 </script>
 

@stop


 