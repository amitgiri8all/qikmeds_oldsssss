<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
 <title>@yield('Qikmeds')</title>
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/images/favicon.ico')}}"/>
<link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet">
<link href="{{asset('public/frontend/css/line-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('public/frontend/css/animate.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/webslidemenu.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/slick.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css"/>
  @yield('header_styles')
  </head>
      
<body>
 <header>
  <div class="header_top">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <div class="left_txt">
            <ul>
              <li><a href="#">Free Shipping for all Order of Rs 100</a></li>
              <li><a href="#"><i class="ti ti-email"></i>support@qikmeds.com</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="right_txt">
            <ul>
              <li><a href="#">Call Qikmeds</a></li>
              <li><a href="#">Offers</a></li>
              <li><a href="#">Need Help?</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="header_btm">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          <div class="header_logo"><a href="index.html"><img src="{{asset('public/frontend/images/logo.png')}}" alt="logo"></a></div>
        </div>
        <div class="col-sm-10">
          <div class="header_right">
            <div class="header_right_top">
              <div class="search_blk">
                <div class="pincode_blk">
                  <span> Delivering to <br>
                    <input type="tel" maxlength="6" value="302020">
                  </span>
                  <button type="button" title="Delivery"></button>
                </div>
                <div class="search_bar">
                  <input type="text" placeholder="Search for Medicines and Health Products...">
                  <button type="submit"></button>
                </div>
              </div>
              <div class="upload_blk">Don’t have a prescription? <a href="#">Get Started <i class="ti ti-angle-down"></i></a></div>
              <div class="user_info">
                <ul>
                  <li><a href="#"><i class="ti ti-user"></i> SIGN IN / SIGN UP</a></li>
                  <li><a href="#"><i class="ti ti-bell"></i></a></li>
                  <li>
                    <i class="ti ti-shopping-cart"></i>
                    <span class="qty">2</span>
                    <span>$100.00</span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="header_right_btm">
              <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link" href="#">COVID Essentials</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Diabetes</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Ayush</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Fitness</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Personal Care</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Moms & Baby</a>
                    </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">Devices</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Surgical</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Sexual Wellness</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Treatments</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>




  @yield('content')

  

<footer>
  <div class="footer_top">
    <div class="container">
      <div class="footer_section">
        <div class="row">
          <div class="col-sm-4">
            <div class="company_info">
              <div class="info_icon"><i class="las la-map-marker-alt"></i></div>
              <div class="info_txt">
                <h2>Address</h2>
                <p>90, 10 B Scheme, Usha Vihar, Triveni Nagar,<br> Arjun Nagar, Jaipur, Rajasthan (302018)</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="company_info pl_5">
              <div class="info_icon"><i class="lab la-whatsapp"></i></div>
              <div class="info_txt">
                <h2>Whatsapp Us</h2>
                <h3>+91 9876543210</h3>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="downld_txt">
              <h4>Download the app now</h4>
              <a href="#"><img src="{{asset('public/frontend/images/google-play.png')}}" alt="play store"></a>
              <a href="#"><img src="{{asset('public/frontend/images/app-store.png')}}" alt="app store"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer_mdl">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="footer_txt">
            <div class="footer_logo"><img src="{{asset('public/frontend/images/logo.png')}}" alt="logo"></div>
            <p>qikmeds.com brings to you an online platform, which can be accessed for all your health needs. We are trying to make healthcare a hassle-free experience for you. Get your allopathic, ayurvedic, homeopathic medicines, vitamins & nutrition supplements and other health-related products delivered at home.</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-4">
              <div class="footer_txt">
                <h2>Company</h2>
                <ul>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> About Qikmeds</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Customers Speak</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Blog</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> <i class="ti ti-angle-double-right"></i> Career</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Contact Us</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> FAQs</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="footer_txt">
                <h2>Categories</h2>
                <ul>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Ayush</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Daibities</li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Family Care</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Fitness</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Lifestyle</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Personal Care</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="footer_txt">
                <h2>Our Policies</h2>
                <ul>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Return Policy</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Returns & Cancellations</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Customer Support Policy</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Editorial Policy</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Privacy Policy</a></li>
                  <li><a href="#"><i class="ti ti-angle-double-right"></i> Terms & Conditions</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="footer_txt">
            <h2>Social</h2>
            <ul class="social">
              <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i> Facebook</a></li>
              <li><a href="#" class="twitter"><i class="fab fa-twitter"></i> Twitter</a></li>
              <li><a href="#" class="instagram"><i class="fab fa-instagram"></i> Instagram</a></li>
              <li><a href="#" class="linkedin"><i class="fab fa-linkedin"></i> LinkedIn</a></li>
              <li><a href="#" class="youtube"><i class="fab fa-youtube"></i> Youtube</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="copyright_blk">
    <div class="container">
      <p>Copyright © 2021 Qikmeds. All Rights Reserved.</p>
    </div>
  </div>
</footer>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('public/frontend/js/slick.js')}}"></script>
 <script src="{{asset('public/frontend/js/custom.js')}}"></script>
 
  
      @yield('footer_scripts')
</body>
</html>