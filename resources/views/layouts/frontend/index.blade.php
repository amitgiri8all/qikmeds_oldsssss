@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


@stop



{{-- Page content --}}
@section('content')
 

 

<section class="main_navigation">
    <div class="container">
        <div class="explore_menus">
            <ul>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-1.png')}}" alt="icon"></i> Medical
                        <small>Over 25000 Products</small>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-2.png')}}" alt="icon"></i> Wellness
                        <small>Health Products</small>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-3.png')}}" alt="icon"></i> Health Corner
                        <small>Trending from Health Experts</small>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-4.png')}}" alt="icon"></i> Others
                        <small>More Info</small>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="banner_blk">
    <button class="prev slick-arrow"><i class="ti-angle-left"></i></button>
    <div class="banner_slider">
        <div class="slider single-item">
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-1.jpg')}}" alt="banner">
            </div>
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-2.jpg')}}" alt="banner">
            </div>
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-3.jpg')}}" alt="banner">
            </div>
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-4.jpg')}}" alt="banner">
            </div>
        </div>
    </div>
    <button class="next slick-arrow"><i class="ti-angle-right"></i></button>
</section>

<section class="middle_section">
    <div class="container">
        <div class="feature_blk">
            <ul>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-1.jpg')}}" alt="icon"></i> Free Delivery
                    <small>For all orders over $120</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-2.jpg')}}" alt="icon"></i> Safe Payment
                    <small>100% secure payment</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-3.jpg')}}" alt="icon"></i> Shop With Confidence
                    <small>If goods have problems</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-4.jpg')}}" alt="icon"></i> 24/7 Help Center
                    <small>Dedicated 24/7 support</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-5.jpg')}}" alt="icon"></i> Friendly Service
                    <small>30 day satisfaction guarantee</small>
                </li>
            </ul>
        </div>
        <div class="deal_blk cmn_blk">
            <div class="title_blk">
                <h2>Deal Of The Day</h2>
                <span class="timer" id="demo"></span>
            </div>
            <div class="product_blk">
                <div class="slider autoplay">
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-1.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Fitness, Health Condition</small>
                                <h3><a href="#">Himalaya Evecare Syrup</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 130 <span class="strike">Rs 148</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-2.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Personal Care, Sexual Wellness</small>
                                <h3><a href="#">Siro Lorastad Sp</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 140 <span class="strike">Rs 158</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-3.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Fitness, Health Condition</small>
                                <h3><a href="#">Dabur Tulsi Drops Natural</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 150 <span class="strike">Rs 168</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-4.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Diabetes, Fitness</small>
                                <h3><a href="#">Organika Salmon Collagen</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 160 <span class="strike">Rs 178</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-5.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Otc Deals, Treatments</small>
                                <h3><a href="#">Oral irrigator Electric</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 170 <span class="strike">Rs 188</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-1.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Fitness, Health Condition</small>
                                <h3><a href="#">Himalaya Evecare Syrup</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 130 <span class="strike">Rs 148</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="offers_blk">
            <div class="title_blk">
                <h2>Offers Just For You</h2>
            </div>
            <div class="offer_content">
                <div class="slider autoplay-2">
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-1.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-2.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-3.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-4.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-5.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-6.jpg')}}" alt="offer"></a></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="category_blk cmn_blk">
            <div class="title_blk">
                <h2>Popular Categories</h2>
            </div>
            <div class="category_content">
                <div class="slider autoplay-1">
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-1.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Covid Essentials</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-2.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Diabities</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-3.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Ayush</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-4.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Fitness</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-5.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Baby Care</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-6.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Feminine Hygiene</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-7.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Sexual Wellness</a></div>
                        </div>
                    </div>
                    <div>
                        <div class="category_info">
                            <div class="category_img"><a href="#"><img src="{{asset('public/frontend/images/categorios-8.jpg')}}" alt="category"></a></div>
                            <div class="category_name"><a href="#">Personal Care</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="deal_blk cmn_blk">
            <div class="title_blk">
                <h2>Health Products</h2>
            </div>
            <div class="product_blk">
                <div class="slider autoplay-3">
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-1.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Fitness, Health Condition</small>
                                <h3><a href="#">Himalaya Evecare Syrup</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 130 <span class="strike">Rs 148</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-2.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Personal Care, Sexual Wellness</small>
                                <h3><a href="#">Siro Lorastad Sp</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 140 <span class="strike">Rs 158</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-3.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Fitness, Health Condition</small>
                                <h3><a href="#">Dabur Tulsi Drops Natural</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 150 <span class="strike">Rs 168</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-4.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Diabetes, Fitness</small>
                                <h3><a href="#">Organika Salmon Collagen</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 160 <span class="strike">Rs 178</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-5.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Otc Deals, Treatments</small>
                                <h3><a href="#">Oral irrigator Electric</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 170 <span class="strike">Rs 188</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-1.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>Fitness, Health Condition</small>
                                <h3><a href="#">Himalaya Evecare Syrup</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                    <h4>Rs 130 <span class="strike">Rs 148</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="brand_blk">
            <div class="title_blk">
                <h2>Featured Brands</h2>
            </div>
            <div class="brand_content">
                <div class="slider autoplay-4">
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-1.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Baidyanath</a></h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-2.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Colgate</a></h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-3.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Dettol</a></h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-4.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Nivea</a></h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-5.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Accu Chek</a></h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-6.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Horlicks</a></h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-7.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Emami</a></h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src="{{asset('public/frontend/images/brand-8.jpg')}}" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">Freestyle</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="blog_blk cmn_blk">
            <div class="title_blk">
                <h2>From Our Blog</h2>
            </div>
            <div class="blog_content">
                <div class="slider autoplay-5">
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-1.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>Quick Guide to Identify Slipped or Bulging Disc</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-2.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>Thyroid in Kids: Everything You Need to Know</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-3.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>What’s the Link between Lupus and Arthritis?</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-4.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>Hypertension: Most Commonly Asked Questions</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-5.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>Can a Newborn Baby Have Low Sugar Levels at Birth?</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-6.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>Understanding Different Types of Bone and Joint Diseases</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-7.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>Know All About Robotic Knee Replacement Surgery</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="blog_info">
                            <div class="blog_img"><a href="#"><img src="{{asset('public/frontend/images/blog-8.jpg')}}" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: 14 April 2020</small>
                                <h3>6 Things to Include in a Diet for High Blood Pressure</h3>
                                <a href="#"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="achivemnt_blk cmn_blk">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-1.jpg')}}" alt="blog"></i>
                            <h2>20 Lakh +</h2>
                            <h3>Families Served</h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-2.jpg')}}" alt="blog"></i>
                            <h2>50 Lakh +</h2>
                            <h3>Orders Delivered</h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-3.jpg')}}" alt="blog"></i>
                            <h2>10000 +</h2>
                            <h3>Pincodes Served</h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-4.jpg')}}" alt="blog"></i>
                            <h2>1 Lakh +</h2>
                            <h3>Medicines Available</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="testimonial_blk cmn_blk">
            <div class="title_blk">
                <h2>What Our Customers Say</h2>
            </div>
            <div class="testimonial_content">
                <div class="slider autoplay-6">
                    <div>
                        <div class="testimonial_info">
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            <p>Very good Website. Would recommend it to everyone requiring fast and efficient delivery of medicinal products at the doorstep.</p>
                            <div class="profile_box">
                                <div class="profile_img"><img src="{{asset('public/frontend/images/user.jpg')}}"></div>
                                <div class="profile_name">
                                    <h3>Ankit Choursiya</h3>
                                    <span>20 April 2021</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial_info">
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            <p>Very good Website. Would recommend it to everyone requiring fast and efficient delivery of medicinal products at the doorstep.</p>
                            <div class="profile_box">
                                <div class="profile_img"><img src="{{asset('public/frontend/images/user.jpg')}}"></div>
                                <div class="profile_name">
                                    <h3>Ankit Choursiya</h3>
                                    <span>20 April 2021</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial_info">
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            <p>Very good Website. Would recommend it to everyone requiring fast and efficient delivery of medicinal products at the doorstep.</p>
                            <div class="profile_box">
                                <div class="profile_img"><img src="{{asset('public/frontend/images/user.jpg')}}"></div>
                                <div class="profile_name">
                                    <h3>Ankit Choursiya</h3>
                                    <span>20 April 2021</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial_info">
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            <p>Very good Website. Would recommend it to everyone requiring fast and efficient delivery of medicinal products at the doorstep.</p>
                            <div class="profile_box">
                                <div class="profile_img"><img src="{{asset('public/frontend/images/user.jpg')}}"></div>
                                <div class="profile_name">
                                    <h3>Ankit Choursiya</h3>
                                    <span>20 April 2021</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial_info">
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            <p>Very good Website. Would recommend it to everyone requiring fast and efficient delivery of medicinal products at the doorstep.</p>
                            <div class="profile_box">
                                <div class="profile_img"><img src="{{asset('public/frontend/images/user.jpg')}}"></div>
                                <div class="profile_name">
                                    <h3>Ankit Choursiya</h3>
                                    <span>20 April 2021</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="testimonial_info">
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            <p>Very good Website. Would recommend it to everyone requiring fast and efficient delivery of medicinal products at the doorstep.</p>
                            <div class="profile_box">
                                <div class="profile_img"><img src="{{asset('public/frontend/images/user.jpg')}}"></div>
                                <div class="profile_name">
                                    <h3>Ankit Choursiya</h3>
                                    <span>20 April 2021</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="newsletter_blk">
            <div class="newsletter_content">
                <h2>Sign Up For Newsletter</h2>
                <p>Join 60.000+ Subscribers and get a new<br> discount coupon on every Saturday</p>
                <div class="newsletter_form">
                    <input type="text" class="form-control" name="" placeholder="Enter your email address">
                    <button type="submit" value="subscribe">Subscribe</button>
                </div>
            </div>
        </div>

    </div>
</section>
 

 
@stop


{{-- page level scripts --}}
@section('footer_scripts')



@stop


 