@extends('backend/layouts/default')
@section('title')
Coupon Manager::CRM
@stop
@section('header_styles')
 
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Coupon Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Coupon</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Coupon
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.coupon.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" action="{{ route('admin.coupon.add.post') }}" id="example-form" enctype="multipart/form-data">
                  @csrf
                     <!-- Coupon Name -->
                    <div class="form-group">
                     <label for="text">Coupon Name</label>
                      <div class="input-group col-xs-12">
                        <input type="text" name="name" class="form-control" placeholder="Enter Coupon Code">
                           {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                       </div>
                     </div> 
                    <!-- Coupon Name -->
                    <div class="form-group">
                     <label for="text">Coupon Code</label>
                      <div class="input-group col-xs-12">
                        <input type="text" name="code" class="form-control" placeholder="Enter Coupon Code">
                           {!! $errors->first('code', '<span class="help-block">:message</span>') !!}
                       </div>
                     </div>
                    <!-- Access Level Status -->
                    <div class="form-group">
                     <label for="text">Coupon Type</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="coupon_type" id="type" >
                             <option value="">Please Coupon Type</option>
                             <option value="percentages">Percentages (%)</option>
                             <option value="amount">Amount (00.0)</option>
                          </select>
                         {!! $errors->first('coupon_type', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="text">Coupon Value</label>
                      <div class="input-group col-xs-12">
                        <input type="text" min="1" max="100" name="coupon_value" class="form-control num" placeholder="Enter Coupon Value">
                           {!! $errors->first('coupon_value', '<span class="help-block">:message</span>') !!}
                       </div>
                  </div>

                   <div class="form-group">
                     <label for="text">Coupon Minimum Value</label>
                      <div class="input-group col-xs-12">
                        <input type="text"  name="coupon_minimum_value" class="form-control num" placeholder="Enter Coupon Minimum Value">
                           {!! $errors->first('coupon_minimum_value', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  
                  <div class="form-group">
                     <label for="text">From Date</label>
                      <div class="input-group col-xs-12">
                        <input type="date" name="from_time" class="form-control" placeholder="Select data">
                           {!! $errors->first('from_time', '<span class="help-block">:message</span>') !!}
                       </div>
                  </div>

                  <div class="form-group">
                     <label for="text">To Date</label>
                      <div class="input-group col-xs-12">
                        <input type="date" name="to_time" class="form-control" placeholder="Select data">
                           {!! $errors->first('to_time', '<span class="help-block">:message</span>') !!}
                       </div>
                  </div>

                 <!--  <div class="form-group">
                     <label for="text">Max Number of use per user</label>
                      <div class="input-group col-xs-12">
                        <input type="text" min="1" max="100" name="number_of_use" class="form-control num" placeholder="Enter Max Number of use per user">
                           {!! $errors->first('number_of_use', '<span class="help-block">:message</span>') !!}
                       </div>
                  </div> -->

                    
                  <!-- Access Level Status -->
                    <div class="form-group">
                     <label for="text">Status</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="status" id="type" >
                             <option value="">Please Status Type</option>
                             <option value="1">Active</option>
                             <option value="0">Inactive</option>
                          </select>
                         {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                  
                
                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
 
@stop
