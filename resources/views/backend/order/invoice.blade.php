@extends('backend.layouts.default') {{-- Page title --}} @section('title') Order Invoice Manager::CRM @parent @stop {{-- page level styles --}} @section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet" />
<style type="text/css">
.badge-danger {
   color: #fff;
   background-color: #dc3545;
}

.badge-success {
   color: #fff;
   background-color: #28a745;
}
</style> @stop {{-- Page content --}} @section('content')
<section class="content-header">
   <h1>Order Invoice Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}"> <i class="livicon" data-name="home" data-size="14" data-color="#000"></i> Dashboard </a>
      </li>
      <li class="active">Order Invoice View</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">
   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               Order Invoice
            </h4>
               <div class="pull-right">
               <a href="{{route('admin.order.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> back </a>
            </div>  </div>
         <div class="panel-body" style="border:1px solid #ccc;padding:0;margin:0;">
            <div class="row" style="padding: 15px;">
               <div class="row" style="padding: 15px;margin-top:5px;">
                  <div class="header">
                     <div class="col-md-6 pull-left"> <img src="{{asset('public/frontend/images/logo.png')}}" alt="logo" width="200" alt="logo" height="65"> </div>
                     <div class="col-md-6 pull-right">
                        <div class="pull-right"> <strong></strong>
                           <br> <strong>Qikmeds </strong><sub>Your Local Pharmancy</sub>
                           <br> 90, 10 B Scheme, Usha Vihar,<br> Triveni Nagar,Arjun Nagar,<br> Jaipur, Rajasthan (302018)
                           <br> quikmeds@info.com
<!--                            <br/> <strong>GST No:</strong>GST Number </div>
 -->                     </div>
                     <div class="clr"></div>
                  </div>
                  <div class="clr"></div>
               </div>
               <div class="row" style="padding: 10px 0;">
                  <div class="col-md-6 col-xs-6" style="margin-top:5px; margin-left:10px"> <strong>Invoice To:</strong>
                     <br> {{ucwords(($order->deliveryaddress->name) ?? "")}}  <span class="badge" style="float:unset;">{{($order->deliveryaddress->address_type ?? "")}}</span>
                     <br> {{ucwords(($order->deliveryaddress->address_delivery ?? ""))}}
                     <br> {{($order->deliveryaddress->city ?? "")}}, {{($order->deliveryaddress->state ?? "")}} - {{($order->deliveryaddress->pin_code ?? "")}}
                     <br> Contact No: {{($order->deliveryaddress->mobile_number ?? "")}} </div>
                  <div class="col-md-3 col-xs-6 pull-right">
                     <div id="invoice" style="text-align:center; background:#ccc; border-radius:4px;padding: 5px;">
                        <h4><strong>Invoice: INV{{$order->order_code}}</strong></h4>
                        <h4><strong>Order: {{$order->order_code}}</strong></h4>
                        <h4><strong>{{date("d M ,Y",strtotime($order->created_at))}}</strong></h4> </div>
                  </div>
               </div>
               <div class="table-responsive">
                  <table class="table table-bordered">
                     <thead>
                        <tr style="background:#ccc;">
                           <th>SI No.</th>
                           <th>Name</th>
                           <th>Quantity</th>
                           <th>Price(₹)</th>
                            <th>NetSubtotal(₹)</th>
                     </thead>
                     <tbody>
                     @foreach($order->OrderItem as $key=>$value) 
                        <tr>
                           <td>{{++$key}}</td>
                           <td>{{$value->ProductOrderItem->medicine_name ?? "" }}</td>
                           <td> {{$value->quantity_order}}</td>
                           <td>₹ {{$value->ProductOrderItem->mrp ?? ""}}</td>
                            <td>₹ {{@($value->ProductOrderItem->mrp)*$value->quantity_order ?? ""}}</td>
                         </tr>
                     @endforeach 
                        <tr>
                            <td></td>
                           <td></td>
                           <td></td>
                           <td><b>Net Total</b></td>
                           <td>₹ {{$order->sub_total}}</td>
                        </tr>
                   @if(!empty($order->discount_amount && $order->discount_amount>0))   
                        <tr>
                            <td></td>
                           <td></td>
                           <td></td>
                           <td><b>Qikmeds Discount</b></td>
                           <td>₹ {{$order->discount_amount}}</td>
                        </tr>
                       @endif   

                      @if(!empty($order->discount_coupon_code && $order->discount_coupon_code>0))   
                         <tr>
                            <td></td>
                           <td></td>
                           <td></td>
                           <td><b>Promo Discount</b></td>
                           <td>₹ {{$order->discount_coupon_code}}</td>
                        </tr>
                       @endif   
                         
                           <tr>
                               <td></td>
                              <td></td>
                              <td></td>
                              <td><strong>Delivery Charge</strong></td>
                              <td><?php if($order->shipping_amount>0){?>
                                    <span>₹ {{$order->shipping_amount}}</span>
                                <?php }else{ ?>
                                    <span>Free</span>
                                    <?php  }?></td>
                           </tr>

                            <tr>
                               <td></td>
                              <td></td>
                              <td></td>
                              <td><strong>Net Amount Payable</strong></td>
                              <td><strong>₹ {{$order->total_payed_amount}}</strong></td>
                           </tr>
                            <tr>
                               <td></td>
                              <td></td>
                              <td></td>
                              <td><strong>Payment Mode</strong></td>
                              <td>{{ucfirst(($order->orderpayment->payment_method ?? ""))}}</td>
                           </tr>
                     </tbody>
                   </table>
               </div>
              <!--  <div style="background-color: #eee;padding:5px 15px;" id="footer-bg">
                  <hr style="margin-bottom:3px;">
                  <a style="margin-left:200px;">
                     <button type="submit" class="btn btn-primary btn-sm Print ">Print Invoice </button>
                  </a> &nbsp; &nbsp; &nbsp;
                  <a style="margin-left:300px;">
                     <button type="submit" class="btn btn-primary btn-sm EmailInvoice ">Email Invoice </button>
                  </a>
               </div> -->
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section> 
@stop 
{{-- page level scripts --}} 
@section('footer_scripts') 

@stop