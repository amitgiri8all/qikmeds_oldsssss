<div class="panel panel-primary" style=" border-radius:0;">
   <!--<div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        General Information
      </h3>
      </div>-->
   <div class="col-sm-12">
      <h3 class="inner-heading"> Payment Setting</h3>
   </div>
  <div class="panel-body">
                <form method="post" class="ajax_form" action="{{ route('admin.update.payment-setting.setting') }}" id="payment" enctype="multipart/form-data">
                   @csrf
                      <div class="form-group col-sm-12">
                     <label for="validate-text">Active Payment Page</label>
                      <div >
                        <select class="form-control" name="active_payment_page" id="type">
                            <option value="">Please Select</option>
                          <option value="yes" @if($payments->active_payment_page=='yes') {{ 'selected' }} @endif>Yes</option>
                          <option value="no" @if($payments->active_payment_page=='no') {{ 'selected' }} @endif>No</option>
                         </select>
                         {!! $errors->first('active_payment_page', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                  <h3>Payment Options:</h3>
                  <!-- App ENV -->
                    <div class="form-group col-sm-6">
                     <label for="validate-text">Cash On Delivery</label>
                      <div class="input-group col-xs-12">
                        <select class="form-control" name="cash_on_delivery" id="type">
                            <option value="">Please Select</option>
                          <option value="yes" @if($payments->cash_on_delivery=='yes') {{ 'selected' }} @endif>Yes</option>
                          <option value="no" @if($payments->cash_on_delivery=='no') {{ 'selected' }} @endif>No</option>
                        </select>
                         {!! $errors->first('cash_on_delivery', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                  <!-- App Debug -->  
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Wallet</label>
                       <div class="input-group col-xs-12">
                        <select class="form-control" name="wallet" id="type">
                             <option value="">Please Select</option>
                          <option value="yes" @if($payments->wallet=='yes') {{ 'selected' }} @endif>Yes</option>
                          <option value="no" @if($payments->wallet=='no') {{ 'selected' }} @endif>No</option>
                        </select>
                         {!! $errors->first('wallet', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Log Level -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Credit Card</label>
                       <div class="input-group col-xs-12">
                        <select class="form-control" name="credit_card" id="type">
                             <option value="">Please Select</option>
                          <option value="yes" @if($payments->credit_card=='yes') {{ 'selected' }} @endif>Yes</option>
                          <option value="no" @if($payments->credit_card=='no') {{ 'selected' }} @endif>No</option>
                        </select>
                         {!! $errors->first('credit_card', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App URL -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Paypal</label>
                     <div class="input-group col-xs-12">
                        <select class="form-control" name="paypal" id="type">
                           <option value="">Please Select</option>
                          <option value="yes" @if($payments->paypal=='yes') {{ 'selected' }} @endif>Yes</option>
                          <option value="no" @if($payments->paypal=='no') {{ 'selected' }} @endif>No</option>
                        </select>
                         {!! $errors->first('paypal', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <h3>Payment Gateway Settings :</h3>
                  <!-- App Mail Driver -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Stripe Secret Key</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="stripe_secret_key"  id="validate-text"
                           placeholder="Enter Stripe Secret key" value="@if(isset($payments->stripe_secret_key)){!! $payments->stripe_secret_key !!}@else{!! old('stripe_secret_key') !!}@endif">
                         {!! $errors->first('stripe_secret_key', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- App Mail Host -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Stripe Public Key</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="stripe_public_key" id="validate-text"
                           placeholder="Enter Stripe Public Key" value="@if(isset($payments->stripe_public_key)){!! $payments->stripe_public_key !!}@else{!! old('stripe_public_key') !!}@endif">
                         {!! $errors->first('stripe_public_key', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- App Mail Username -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Paypal Account Email</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="paypal_account_email"  id="validate-text"
                           placeholder="Enter Paypal Account Email" value="@if(isset($payments->paypal_account_email)){!! $payments->paypal_account_email !!}@else{!! old('paypal_account_email') !!}@endif">
                         {!! $errors->first('paypal_account_email', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Mail Password -->
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Paypal Currency</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="paypal_currency" id="validate-text"
                           placeholder="Enter Paypal Currency" value="@if(isset($payments->paypal_currency)){!! $payments->paypal_currency !!}@else{!! old('paypal_currency') !!}@endif">
                         {!! $errors->first('paypal_currency', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
</div>