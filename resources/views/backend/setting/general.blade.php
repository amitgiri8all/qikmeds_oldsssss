<div class="panel panel-primary" style=" border-radius:0;">
   <!--<div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        General Information
      </h3>
      </div>-->
   <div class="col-sm-12">
      <h3 class="inner-heading"> Genral Setting</h3>
   </div>

   
 <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.update.general.setting') }}"   id="general" enctype="multipart/form-data">
                   @csrf
                  
              <!-- App Name -->
                  <div class="form-group col-sm-6">
                     <label for="text">App Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text"class="form-control" value="@if(isset($setting->app_name)){!! $setting->app_name !!}@else{!! old('app_name') !!}@endif" name="app_name" placeholder="Enter App Name">
                         {!! $errors->first('app_name', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>
                  

                  <!-- App ENV -->
                  <div class="form-group col-sm-6">
                     <label for="select">App Env</label>
                     <div class="input-group col-xs-12">
                        <select class="form-control" name="app_env">
                           <option value="">Please Select Type</option>
                           <option value="app_env" @if($setting->app_env=='app_env') {{ 'selected' }} @endif>App Env</option>
                           <option value="production" @if($setting->app_env=='production') {{ 'selected' }} @endif>Production</option>
                           <option value="local" @if($setting->app_env=='local') {{ 'selected' }} @endif>Local</option>
                         </select>
                        </div>
                        {!! $errors->first('app_env', '<span class="help-block">:message</span>') !!}
                     </div>
          
                  <!-- App Debug -->  
                  <div class="form-group col-sm-6">
                        <label for="text">App Debug</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control" name="app_debug" placeholder="Enter App Debug" value="@if(isset($setting->app_debug)){!! $setting->app_debug !!}@else{!! old('app_debug') !!}@endif">
                           {!! $errors->first('app_debug', '<span class="help-block">:message</span>') !!}
                       </div>
                  </div>

                  <!-- App Log Level -->
                  <div class="form-group col-sm-6">
                     <label for="text">App Log Level</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="app_log_level"  value="@if(isset($setting->app_log_level)){!! $setting->app_log_level !!}@else{!! old('app_log_levels') !!}@endif" placeholder="Enter App Log Level" >
                         {!! $errors->first('app_log_level', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App URL -->
                  <div class="form-group col-sm-6">
                     <label for="text">App URL</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="app_url" value="@if(isset($setting->app_url)){!! $setting->app_url !!}@else{!! old('app_url') !!}@endif" placeholder="Enter App URL" >
                         {!! $errors->first('app_url', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- App Mail Driver -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail Driver</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="mail_driver" value="@if(isset($setting->mail_driver)){!! $setting->mail_driver !!}@else{!! old('mail_driver') !!}@endif"placeholder="Enter Mail Driver" >
                         {!! $errors->first('mail_driver', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- App Mail Host -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail Host</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="mail_host" value="@if(isset($setting->mail_host)){!! $setting->mail_host !!}@else{!! old('mail_host') !!}@endif" placeholder="Enter Host" >
                         {!! $errors->first('mail_host', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- App Mail Username -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail Username</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="mail_username" value="@if(isset($setting->mail_username)){!! $setting->mail_username !!}@else{!! old('mail_username') !!}@endif" placeholder="Enter Username" >
                         {!! $errors->first('mail_username', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Mail Password -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail Password</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="mail_password" value="@if(isset($setting->mail_password)){!! $setting->mail_password !!}@else{!! old('mail_password') !!}@endif" placeholder="Enter Mail Password" >
                         {!! $errors->first('mail_password', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Mail Encryption -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail Encryption</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="mail_encryption" value="@if(isset($setting->mail_encryption)){!! $setting->mail_encryption !!}@else{!! old('mail_encryption') !!}@endif" placeholder="Enter Encryption">
                         {!! $errors->first('mail_password', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <div class="clearfix"></div>


                  <!-- App Mail From Address -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail From Address</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="mail_from_address" value="@if(isset($setting->mail_from_address)){!! $setting->mail_from_address !!}@else{!! old('mail_from_address') !!}@endif" placeholder="Enter Mail From Address" >
                         {!! $errors->first('mail_from_address', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Mail port -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail From Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="mail_from_name" value="@if(isset($setting->mail_from_name)){!! $setting->mail_from_name !!}@else{!! old('mail_from_name') !!}@endif" placeholder="Enter From Name">
                         {!! $errors->first('mail_from_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Mail port -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail URL Android</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="app_url_android" value="@if(isset($setting->app_url_android)){!! $setting->app_url_android !!}@else{!! old('app_url_android') !!}@endif" placeholder="Enter Mail URL Android" >
                         {!! $errors->first('app_url_android', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Mail port -->
                  <div class="form-group col-sm-6">
                     <label for="text">Mail URL IOS</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="app_url_ios" value="@if(isset($setting->app_url_ios)){!! $setting->app_url_ios !!}@else{!! old('app_url_ios') !!}@endif" >
                         {!! $errors->first('app_url_ios', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Under Maintenance -->
                  <div class="form-group col-sm-6">
                     <label for="text">Under Maintenance</label>
                      <div class="input-group col-xs-12">
                          <select class="form-control" name="under_maintenance" id="type" >
                             <option value="">Please Select Type</option>
                             <option value="true" @if($setting->under_maintenance=='true') {{ 'selected' }} @endif>TRUE</option>
                             <option value="false" @if($setting->under_maintenance=='false') {{ 'selected' }} @endif>FALSE</option>
                          </select>
                         {!! $errors->first('under_maintenance', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Mail Timezone -->
                  <div class="form-group col-sm-6">
                     <label for="text">Timezone</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="timezone" value="@if(isset($setting->timezone)){!! $setting->timezone !!}@else{!! old('timezone') !!}@endif"
                           placeholder="Enter Timezone" >
                         {!! $errors->first('timezone', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
              
                  <!-- App Pagination Limit -->
                  <div class="form-group col-sm-6">
                     <label for="text">Pagination Limit</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control num" name="pagination_limit" value="@if(isset($setting->pagination_limit)){!! $setting->pagination_limit !!}@else{!! old('pagination_limit') !!}@endif" placeholder="Enter Pagination Limit" >
                         {!! $errors->first('pagination_limit', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Email -->
                  <div class="form-group col-sm-6">
                     <label for="text">Email</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="email" value="@if(isset($setting->email)){!! $setting->email !!}@else{!! old('email') !!}@endif" 
                           placeholder="Enter Email" >
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                  <!-- App Phone -->
                  <div class="form-group col-sm-6">
                     <label for="text">Phone</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control num" name="phone" value="@if(isset($setting->phone)){!! $setting->phone !!}@else{!! old('phone') !!}@endif"
                           placeholder="Enter Phone Number" >
                         {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- App Phone -->
                  <div class="form-group col-sm-6">
                     <label for="text">Currency</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="currency" value="@if(isset($setting->currency)){!! $setting->currency !!}@else{!! old('currency') !!}@endif" 
                           placeholder="Enter Currency" >
                         {!! $errors->first('currency', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

            <!-- App Phone -->
                  <div class="form-group col-sm-6" style="display:none;">
                     <label for="text">Currency</label>
                     <div class="input-group col-xs-12">
                        <select name="date_format" class="form-control" required>
                                <option value="">Select Date Format
                                </option>
                                <option value="%d/%m/%y" @if($setting->date_format=='%d/%m/%y'){{'selected'}} @endif>dd/mm/yy (Ex: 01/02/21)
                                </option>
                                <option value="%m/%d/%y" @if($setting->date_format=='%m/%d/%y'){{'selected'}} @endif>mm/dd/yy (Ex: 02/01/21)
                                </option>
                                <option value="%y/%m/%d" @if($setting->date_format=='%y/%m/%d'){{'selected'}} @endif>yy/mm/dd (Ex: 21/02/01)
                                </option>
                                <option value="%d-%m-%Y" @if($setting->date_format=='%d-%m-%Y'){{'selected'}} @endif>dd-mm-yy (Ex: 01-02-21)
                                </option>
                                <option value="%m-%d-%y" @if($setting->date_format=='%m-%d-%y'){{'selected'}} @endif>mm-dd-yy (Ex: 02-01-21)
                                </option>
                                <option value="%y-%m-%d" @if($setting->date_format=='%y-%m-%d'){{'selected'}} @endif>yy-mm-dd (Ex: 02-01-21)
                                </option>
                                <option value="%d/%M/%y" @if($setting->date_format=='%d/%M/%y'){{'selected'}} @endif>dd/MM/yy (Ex: 01/february/21)
                                </option>
                                <option value="%M/%d/%y" @if($setting->date_format=='%M/%d/%y'){{'selected'}} @endif> MM/dd/yy (Ex: february/01/21)
                                </option>
                                <option value="%y/%M/%d" @if($setting->date_format=='%y/%M/%d'){{'selected'}} @endif> yy/MM/dd (Ex: 21/february/01)
                                </option>
                                <option value="%d-%M-%y" @if($setting->date_format=='%d-%M-%y'){{'selected'}} @endif> dd-MM-yy (Ex: 01-feb-21)
                                </option>
                                <option value="%M-%d-%y" @if($setting->date_format=='%M-%d-%y'){{'selected'}} @endif> MM-dd-yy (Ex: feb-01-21)
                                </option>
                                <option value="%y-%M-%d" @if($setting->date_format=='%y-%M-%d'){{'selected'}} @endif> yy-MM-dd (Ex: 21-feb-01)
                                </option>
                                <option value="%d, %M %Y" @if($setting->date_format=='%d, %M %Y'){{'selected'}} @endif> dd, MM yyyy (Ex: 01,february 2021)
                                </option>
                                <option value="%M %d %Y" @if($setting->date_format=='%M %d %Y'){{'selected'}} @endif> MM dd yyyy (Ex: february 01 2021 )
                                </option>
                                <option value="%Y %M %d" @if($setting->date_format=='%Y %M %d'){{'selected'}} @endif> yyyy MM dd (Ex: 2021 february 01 )
                                </option>
                                <option value="%d, %b %Y" @if($setting->date_format=='%d, %b %Y'){{'selected'}} @endif> dd, M yyyy (Ex: 01,feb 2021)
                                </option>
                                <option value="%b %d %Y" @if($setting->date_format=='%b %d %Y'){{'selected'}} @endif> M dd yyyy (Ex: feb 01 2021)
                                </option>
                                <option value="%Y %b%d" @if($setting->date_format=='%Y %b%d'){{'selected'}} @endif> yyyy M dd (Ex: 2021 feb 01)
                                </option>
                        </select>
                         {!! $errors->first('currency', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

           
                  <!-- App Phone -->
                  <div class="form-group col-sm-6">
                     <label for="text">Address</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="address" value="@if(isset($setting->address)){!! $setting->address !!}@else{!! old('address') !!}@endif" 
                           placeholder="Enter Your Address" >
                         {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                 
                  <!-- App Phone -->
                  <div class="form-group col-sm-6">
                     <label for="text">App Logo </label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="app_logo" value="@if(isset($setting->app_logo)){!! $setting->app_logo !!}@else{!! old('app_logo') !!}@endif" 
                           placeholder="Enter Your App Logo" >
                      {!! $errors->first('app_logo', '<span class="help-block">:message</span>') !!}
                     </div>
                    <img alt="{{$setting->app_logo}}" src="{{ url('storage/app/public/upload/image/'.$setting->app_logo) }}"height="75" width="75">
                  </div>

                     <!-- App Phone -->
                  <div class="form-group col-sm-6">
                     <label for="text">Short Descriptions</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="short_descriptions" value="@if(isset($setting->short_descriptions)){!! $setting->short_descriptions !!}@else{!! old('short_descriptions') !!}@endif" 
                           placeholder="Enter Your Short Descriptions" >
                         {!! $errors->first('short_descriptions', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit" id="general"  class="btn btn-primary btn-block btn-md btn-responsive general">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>

            </div>
</div>