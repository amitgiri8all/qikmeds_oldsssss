<div class="panel panel-primary" style=" border-radius:0;">
   <!--<div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        General Information
      </h3>
      </div>-->
   <div class="col-sm-12">
      <h3 class="inner-heading"> Site Setting</h3>
   </div>
  <div class="panel-body">
                <form method="post" class="ajax_form" action="{{ route('admin.update.site-setting.setting') }}" id="site" enctype="multipart/form-data">
                   @csrf
            
                    <div class="form-group col-sm-6">
                     <label for="validate-text">Phone</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control num" name="phone" value="@if(isset($site->phone)){!! $site->phone !!}@else{!! old('phone') !!}@endif"  id="validate-text" placeholder="Enter Phone Number">
                         {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <div class="form-group col-sm-6">
                     <label for="validate-text">Whatsup Number</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control num" value="@if(isset($site->whats_up)){!! $site->whats_up !!}@else{!! old('whats_up') !!}@endif"  name="whats_up"  id="validate-text" placeholder="Enter Whatsup Number">
                         {!! $errors->first('whats_up', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
           
                    <div class="form-group col-sm-6">
                     <label for="validate-text">Facebook Page</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="facebook" value="@if(isset($site->facebook)){!! $site->facebook !!}@else{!! old('facebook') !!}@endif"  id="validate-text" placeholder="Enter Facebook Url">
                         {!! $errors->first('facebook', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <div class="form-group col-sm-6">
                     <label for="validate-text">Twitter Page</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($site->twitter)){!! $site->twitter !!}@else{!! old('twitter') !!}@endif"  name="twitter" id="validate-text" placeholder="Enter Twitter Url">
                         {!! $errors->first('twitter', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <div class="form-group col-sm-6">
                     <label for="validate-text">Instagram Page</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="instagram" value="@if(isset($site->instagram)){!! $site->instagram !!}@else{!! old('instagram') !!}@endif"  id="validate-text" placeholder="Enter Instagram Url">
                         {!! $errors->first('instagram', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <div class="form-group col-sm-6">
                     <label for="validate-text">Linkedin Page</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="linkedin" value="@if(isset($site->linkedin)){!! $site->linkedin !!}@else{!! old('linkedin') !!}@endif"  id="validate-text" placeholder="Enter Linkedin Url">
                         {!! $errors->first('linkedin', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
     
                  <div class="form-group col-sm-6">
                     <label for="validate-text">Youtube</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="youtube" value="@if(isset($site->youtube)){!! $site->youtube !!}@else{!! old('youtube') !!}@endif"  id="validate-text" placeholder="Enter Youtube Url">
                         {!! $errors->first('youtube', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
 
                  <div class="form-group col-sm-6">
                       <label for="validate-text">Referral Amount</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control num" name="referral_amount" value="@if(isset($site->referral_amount)){!! $site->referral_amount !!}@else{!! old('referral_amount') !!}@endif"  id="validate-text"
                             placeholder="Enter referral_amount">
                         {!! $errors->first('referral_amount', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div>
  
                  <div class="form-group col-sm-6">
                       <label for="validate-text">Referred By Amount</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control num" name="referred_by_amount" value="@if(isset($site->referred_by_amount)){!! $site->referred_by_amount !!}@else{!! old('referred_by_amount') !!}@endif"  id="validate-text"
                             placeholder="Enter referred by amount">
                         {!! $errors->first('referred_by_amount', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div>  

                  <div class="form-group col-sm-6">
                       <label for="validate-text">Delivery Charge</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control num" name="delivery_charge" value="@if(isset($site->delivery_charge)){!! $site->delivery_charge !!}@else{!! old('delivery_charge') !!}@endif"
                             placeholder="Enter delivery charge">
                         {!! $errors->first('delivery_charge', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div>  

                  <div class="form-group col-sm-6">
                       <label for="validate-text">Min Price For Free Delivery Charge</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control num" name="min_price" value="@if(isset($site->min_price)){!! $site->min_price !!}@else{!! old('min_price') !!}@endif"
                             placeholder="Enter min price for delivery charge">
                         {!! $errors->first('min_price', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div> 

                  <div class="form-group col-sm-6">
                       <label for="validate-text">Qikmeds Discount</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control num" name="qikmeds_discount" value="@if(isset($site->qikmeds_discount)){!! $site->qikmeds_discount !!}@else{!! old('qikmeds_discount') !!}@endif"
                             placeholder="Enter qikmeds discount">
                         {!! $errors->first('qikmeds_discount', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div> 

                    <div class="form-group col-sm-6">
                       <label for="validate-text">New user qikmeds discount</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control num" name="new_user_qikmeds_discount" value="@if(isset($site->new_user_qikmeds_discount)){!! $site->new_user_qikmeds_discount !!}@else{!! old('new_user_qikmeds_discount') !!}@endif"
                             placeholder="Enter new user qikmeds discount">
                         {!! $errors->first('new_user_qikmeds_discount', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div>  

                    <div class="form-group col-sm-6">
                       <label for="validate-text">Near By Queue Time</label>
                       <div class="input-group col-xs-12">
                          <input type="time" class="form-control num" name="near_by_queue_time" value="@if(isset($site->near_by_queue_time)){!! $site->near_by_queue_time !!}@else{!! old('near_by_queue_time') !!}@endif">
                         {!! $errors->first('near_by_queue_time', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div>     
                    
                     <div class="form-group col-sm-6">
                       <label for="validate-text">City Queue Time</label>
                       <div class="input-group col-xs-12">
                          <input type="time" class="form-control num" name="city_queue_time" value="@if(isset($site->city_queue_time)){!! $site->city_queue_time !!}@else{!! old('city_queue_time') !!}@endif">
                         {!! $errors->first('city_queue_time', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div> 


                     <div class="form-group col-sm-6">
                       <label for="validate-text">Dont have prescription</label>
                       <div class="input-group col-xs-12">
                          <input type="text" class="form-control" name="dont_have_prescription" value="@if(isset($site->dont_have_prescription)){!! $site->dont_have_prescription !!}@else{!! old('dont_have_prescription') !!}@endif">
                         {!! $errors->first('dont_have_prescription', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div> 

                     <div class="form-group col-sm-6">
                       <label for="validate-text">Lock delivery time</label>
                       <div class="input-group col-xs-12">
                          <input type="time" class="form-control" name="lock_delivery_times" value="@if(isset($site->lock_delivery_times)){!! $site->lock_delivery_times !!}@else{!! old('lock_delivery_times') !!}@endif">
                         {!! $errors->first('lock_delivery_times', '<span class="help-block">:message</span>') !!}
                       </div>
                    </div> 
                    
                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
</div>