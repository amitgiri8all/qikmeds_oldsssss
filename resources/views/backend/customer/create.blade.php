@extends('backend/layouts/default')
@section('title')
Customer Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
  
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Customer Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Customer</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new customer
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.customer.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.customer.add.post') }}" id="customer" enctype="multipart/form-data">
                  @csrf
 
                  <div class="row ">

         <div class="col-sm-6 form-group">
            <label for="name-f">First Name</label>
            <input type="text" class="form-control" value="{{old('first_name')}}" name="first_name" id="first_name" placeholder="Enter your first name." >
            {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}

         </div>

                
         <div class="col-sm-6 form-group">
            <label for="name-l">Last name</label>
            <input type="text" class="form-control" value="{{old('last_name')}}" name="last_name" id="last_name" placeholder="Enter your last name." >
            {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}

         </div>

         <div class="col-sm-6 form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control"  value="{{old('email')}}"  name="email" id="email" placeholder="Enter your email." >
            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}

         </div>    
         
         <div class="col-sm-6 form-group">
            <label for="email">Mobile Number</label>
            <input type="text" class="form-control"  value="{{old('mobile_number')}}"  name="mobile_number" id="mobile_number" placeholder="Enter your mobile_number." >
            {!! $errors->first('mobile_number', '<span class="help-block">:message</span>') !!}

         </div> 

            <!-- Password  -->
           <div class="form-group col-sm-6">
             <label for="text">Password</label>
             <div class="input-group col-xs-12">
                <input type="password" class="form-control" name="password" placeholder="Password Name">
                {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
             </div>
          </div>

         <!-- Date of Birth  -->
           <div class="form-group col-sm-6">
             <label for="text">Date Of Birth</label>
             <div class="input-group col-xs-12">
                <input type="Date" class="form-control" id="datepicker" name="dob" placeholder="Date of Birth">
                {!! $errors->first('dob', '<span class="help-block">:message</span>') !!}
             </div>
          </div>

            <!-- Confirm Password  -->
           <div class="form-group col-sm-6">
             <label for="text">Confirm Password</label>
             <div class="input-group col-xs-12">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation Name">
                {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
             </div>
          </div>

          <div class="form-group col-sm-6">
               <label for="text"> Customer Image</label>
               <div class="input-group col-xs-12">
                  <input type="file" class="form-control" name="image">
                  {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                  </span>
               </div>
          </div> 

  
          <label>Gender</label>
          <input type="radio" value="Male" id="male" name="gender">
          <label for="male">Male</label>
          <input type="radio" value="Female" id="female" name="gender">
          <label for="female">Female</label> 
 
         </div> 

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>



<!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
 -->
@stop

