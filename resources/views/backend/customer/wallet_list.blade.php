@extends('backend/layouts/default')
{{-- Page title --}}
@section('title')
Wallet History Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type="text/css">
  .search_btn_list { border:none; border-radius:3px; background-color:#fcaf17; padding:8px 10px;font-weight:normal; color:#fff; font-size:14px; line-height:20px;
    margin-top: 21px;}
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Wallet History Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Wallet History Manager</li>
    </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">
 
   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               Wallet History
            </h4>
               </div>
         <div class="panel-body">
            <table class="table table-bordered " id="table2">
                  <thead>
                    <tr>
                        <th>Id</th>
                        <th>Transaction Id</th>
                        <th>Amount</th>
                        <th>Description</th>
                    </tr>
                  </thead>
               <tbody>
                
                @foreach ($wallet_history as $key => $value)
                <tr>
                  <td>{{ $value->id }}</td>
                  <td>{{ $value->transaction_id}}</td>
                  <td>{{ $value->amount }}</td>
                  <td>{{ $value->description }}</td>
                </tr>
              
              @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}

