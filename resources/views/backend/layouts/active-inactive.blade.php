	<div class="modal-header">
		<h4 class="modal-title" id="myModalLabel60">Change Status</h4>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<h5>Are you sure ? You want to <?php if($status=="Active"){ echo "<b style='color: green;'>Active</b>"; } else { echo "<b style='color: red;'>Inactive</b>"; };?> this record.</h5>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
		<a href="{{$route_link}}" class="btn btn-primary">Confirm</a>
	</div>
