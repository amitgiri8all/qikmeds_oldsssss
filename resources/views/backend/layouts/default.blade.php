<!DOCTYPE html>
<html >
   <head>
      <meta charset="UTF-8">
      <title>
         @section('title')
         Admin
         @show
      </title>
      <base href='/'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/images/favicon.ico')}}"/>
      <meta property="userid" name="userid" content="7">
      <link href="{{ asset('public/assets/css/app.css') }}" rel="stylesheet" type="text/css"/>
      <link href="{{ asset('public/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css">
        <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>


      <!-- font Awesome -->
      <style>
      ul.dropdown-menu.dropdown-messages.mr-auto {overflow-y: auto;height: 250px;width: 300px;}

       .search_btn_list { border:none; border-radius:3px; background-color:#fcaf17; padding:8px 10px;font-weight:normal; color:#fff; font-size:14px; line-height:20px;
    margin-top: 21px;}
.back-to-top {
    cursor: pointer;
    position: fixed;
    bottom: 30px;
    right: 30px;
    display: none;
    border-radius: 50%;
    padding: 6px;
    font-size: 10px;
    opacity: 0.7;
    z-index: 9999;
    font-size: 15px;
    width: 35px;
    height: 35px;
    background-color: #992e2c !important;
    border: 1px solid #c54947 !important;
}
.help-block{font-weight: bold;color: red !important;}
.inner_pages #wrapper { padding-left:300px; }
.wbox:after { display:block; content:""; clear:both; }
.wbox { background-color:#fff; -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.12); -moz-box-shadow: 0 2px 5px rgba(0,0,0,.12); box-shadow: 0 2px 5px rgba(0,0,0,.12); padding:15px; margin-bottom:20px; }
.search-form{ display:none; margin-top:15px;}
/* form */
.triger .fa-minus-square{ display:none;}
.triger.expanded .fa-plus-square{ display:none;}
.triger.expanded .fa-minus-square{ display:inline-block;}
.align_center{text-align: center !important;}
.autocomplete_main{position: absolute; z-index: 2; background: transparent;}
.autocomplete_sub{ color: #9C9C9C; position: absolute; background: transparent; z-index: 1;}
.autocomplete-suggestion{height: 30px;background: white;border: 1px solid #e2e2e2;padding: 5px;width: calc(100% + 2px);}
         /*
         input.autocomplete_main, input.autocomplete_sub{
         display: block;
         height: 38px;
         padding: 8px 12px; min-width:300px;
         font-size: 14px;
         line-height: 1.42857143;
         color: #555;
         background-color: transparent;
         background-image: none;
         border: 1px solid #e8e8e8;
         border-radius: 4px;}
         */
         .search_btn { border:none; border-radius:3px; background-color:#fcaf17; padding:8px 10px;font-weight:normal; color:#fff; font-size:14px; line-height:20px; margin-left: -216px;
         margin-top: 21px;}
         /* table default */
         .au-search input.autocomplete_main, .au-search input.autocomplete_sub{min-width: 100%; width:100%;}
         .au-search input.autocomplete_main{position:relative;}
         .au-search input.autocomplete_sub{opacity:0;}
         .autocomplete-suggestions .autocomplete-suggestion.autocomplete-selected{background: #e8e8e8;}
         .autocomplete-suggestions .autocomplete-suggestion{background: #f4f4f4;}
         .autocomplete-suggestion {display: block; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;}
         .autocomplete-block .autocomplete_sub{position: unset;  border: none;}
         .form-group .auto_complete_loader{position: absolute;  top: 34px; left: 2px;}
         .skin-josh .logo{background: #eee;}
         img {
     vertical-align: inherit;
}
  /**** custome ckeckbox rario ****/
[type="radio"]:checked, [type="radio"]:not(:checked) { position: absolute; left: -9999px; }
[type="radio"]:checked + label, [type="radio"]:not(:checked) + label { position: relative; padding-left: 28px; cursor: pointer; line-height: 20px; display: inline-block; color: #666; }
[type="radio"]:checked + label:before, [type="radio"]:not(:checked) + label:before { content: ''; position: absolute; left: 0; top: 0; width: 28px; height: 28px; border: 1px solid #b2b2b2; border-radius: 100%; background: #fff; }
[type="radio"]:checked + label:after, [type="radio"]:not(:checked) + label:after { content: ''; width: 20px; height: 20px; background: #a94442; position: absolute; top: 4px; left: 4px; border-radius: 100%; webkit-transition: all 0.2s ease; transition: all 0.2s ease; }
[type="radio"]:not(:checked) + label:after { opacity: 0; -webkit-transform: scale(0); transform: scale(0); }
[type="radio"]:checked + label:after { opacity: 1; -webkit-transform: scale(1); transform: scale(1); } 

 /* loader */
 #admin_loader {position: absolute;  z-index: 10;  top: calc(50% - 20px);  left: calc(50% - 20px);  background: #fff; padding: 5px; box-shadow: 0px 0 3px rgba(0, 0, 0, 0.35);
   border-radius: 4px;}
   #admin_loader_hotel img{width:30px; height:30px;}
   /* loader */
   #admin_loader_hotel {position: absolute;  z-index: 10;  top: calc(50% - 20px);  left: calc(50% - 20px);  background: #fff; padding: 5px; box-shadow: 0px 0 3px rgba(0, 0, 0, 0.35);
       border-radius: 4px;}
   #admin_loader img{width:50px; height:50px;}

ul.sub-menu.in.collapse li.active {
   /* background: #c5c2c2 !important;*/
    color: #fff;
}

input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
#loading {
  position: fixed;
  display: block;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  text-align: center;
  opacity: 0.7;
  background-color: #fff;
  z-index: 99;
}

#loading-image {
  position: absolute;
  top: 200px;
  left: 600px;
  z-index: 100;
}
      </style>
      <!-- end of global css -->
 
      <!--page level css-->
      @yield('header_styles')
      <!--end of page level css-->
   <body class="skin-josh">
      <div id="loading">
  <img id="loading-image" src="{{asset('public/frontend/images/loading.gif')}}" alt="Loading..." />
</div>
      <header class="header">
         <a href="{{route('admin.dashboard')}}" style="color: rgb(146, 22, 22); font-size: xx-large;" class="logo">
             <img src="{{asset('public/frontend/images/logo.png')}}"  width="200" alt="logo" height="50"> 
           
         </a>
         <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <div>
               <a href="javascript:void(0);" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                  <div class="responsive_nav"></div>
               </a>
            </div>
            <div class="nav_icons">
               <ul class="sidebar_threeicons">
                   <li style="display: none;">
                     <a href="">
                         <i class="livicon" data-name="rotate-right" title="Cache Clear" data-loop="true"
                            data-color="#fff" data-hc="#fff" data-s="25"></i>
                     </a>
                     </li>
               </ul>
            </div>
            <div class="navbar-right">
               <ul class="nav navbar-nav">
                @include('backend.layouts.notifications')
 
                  <li class="dropdown user user-menu">
                     <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        @php
                          $url= asset('storage/app/public/upload/Thumbnail/'.Auth()->guard('admin')->user()->image);
                        @endphp
                        
                        <img src="http://172.105.36.210/ibazar/storage/app/public/upload/Thumbnail/user/163120754452.jpg" width="35"
                           class="img-circle img-responsive pull-left" height="35" alt="riot">
                        <div class="riot">
                           <div>
                              {{ ucfirst(Auth()->guard('admin')->user()->name) }}
                              <span>
                              <i class="caret"></i>
                              </span>
                           </div>
                        </div>
                     </a>
                     <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                           <img src="http://172.105.36.210/ibazar/storage/app/public/upload/Thumbnail/user/163120754452.jpg"
                              class="img-responsive img-circle" alt="User Image">
                           <p class="topprofiletext">{{ ucfirst(Auth()->guard('admin')->user()->name) }} </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                           <div class="pull-left">
                              <a href="{{ route('profile') }}">
                              <i class="livicon" data-name="user" data-s="18"></i>
                              Profile
                              </a>
                           </div>
                           <div class="pull-right">
                              <a href="{{ route('admin.logout.post') }}">
                              <i class="livicon" data-name="sign-out" data-s="18"></i>
                              Logout
                              </a>
                           </div>
                        </li>
                     </ul>
                  </li>
               </ul>
            </div>
         </nav>
      </header>
      <div class="wrapper row-offcanvas row-offcanvas-left">
         <!-- Left side column. contains the logo and sidebar -->
         <aside class="left-side sidebar-offcanvas"  style="min-height: 1461px;">
            <section class="sidebar ">
               <div class="page-sidebar  sidebar-nav">
                  <div class="clearfix"></div>
                  <!-- BEGIN SIDEBAR MENU -->
                  @include('backend.layouts._left_menu')
                  <!-- END SIDEBAR MENU -->
               </div>
            </section>
         </aside>
         <aside class="right-side">
                      @include('notifications')

            @yield('content')
           
         </aside>

         <!-- right-side -->
      </div>
      <a id="back-to-top" href="javascript:void(0);" class="btn btn-primary  back-to-top" role="button" title="Return to top"
         data-toggle="tooltip" data-placement="left">
      <i class="fa fa-arrow-up"></i>
      </a>
       
      <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/assets/js/moment.min.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/assets/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/assets/js/ib.js') }}" type="text/javascript"></script>
      <script type="text/javascript" src="{{ asset('public/assets/js/admin/comman.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/js-yaml/3.13.1/js-yaml.min.js"></script>


<script type="text/javascript">
   $('#loading').hide();
  $("li.active") .parents("ul").addClass("in").removeAttr("style");
  $("li.active").parents("li").addClass("active");
</script>
      <!-- begin page level js -->
      @yield('footer_scripts')
      <!-- end page level js -->

   </body>
</html>