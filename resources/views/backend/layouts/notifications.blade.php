@php $users = App\Helpers\Helper::UnreadNotifications(Auth::guard('admin')->user()->id);@endphp

<?php  //echo "<pre>"; print_r($users->notifications); die;?>

<li class="dropdown messages-menu ">
   <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenuLink" aria-haspopup="true" aria-expanded="false"><i class="livicon" data-name="bell" data-loop="true" data-color="#fff"
           data-hovercolor="#faeaee" data-size="28"></i>   <span class="label label-warning">{{count($users->notifications);}}</span>

   </a>

   <ul class="dropdown-menu dropdown-messages mr-auto" aria-labelledby="dropdownMenuLink">
      @forelse ($users->notifications as $notification)
         <li>{{ $notification->data['name']; }} {{$notification->created_at;}}
            <small class="pull-right">
            <i class="fa fa-clock-o"></i>
            {{ $notification->created_at->diffForHumans() }}
            </small>
         </li>
         <br>
         <br>
        @empty
      <li class="dropdown-title">There is no unread notifications</li>
     @endforelse

     
      <li class="footer">
         <a href="javascript:void(0);">View all</a>
      </li>
   </ul>
</li>

<li class="dropdown notifications-menu" style="display: none;">
   <a href="#" class="dropdown-toggle" data-toggle="dropdown">
   <i class="livicon" data-name="phone" data-loop="true" data-color="#fff"
      data-hovercolor="#faeaee" data-size="28"></i>
   <span class="label label-warning">45</span>
   </a>
   <ul class=" notifications dropdown-menu">
      <li class="dropdown-title">You have 5 New Feedback</li>
       
      <li>
         <ul class="menu">
    
      
      <li class="dropdown-title">Nothing to show</li>
       <li class="footer">
         <a href="#">View all</a>
      </li>
   </ul>
</li>
</ul></li>