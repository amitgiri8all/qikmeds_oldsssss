@extends('backend/layouts/default')
@section('title')
Reason-return Manager::CRM
@stop
@section('header_styles')
<link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

<!-- <style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style> -->
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Reason-return Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Edit Reason-return</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                     Edit Reason-return
               </h3>
             <div class="pull-right" style="margin-top: -25px;">
                <a href="{{route('admin.reason-return.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
             </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" id="reason-return" enctype="multipart/form-data">
                  @csrf
 
         <div class="row ">

         <div class="col-sm-12 form-group">
            <label for="name-f">Reason For Return</label>
            <input type="text" class="form-control" value="@if(isset($data->name)){!! $data->name !!}@else{!! old('name') !!}@endif" name="name" id="name" placeholder="Enter your reason for return." >
            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
         </div>

 </div> 

           <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('Update')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

@stop