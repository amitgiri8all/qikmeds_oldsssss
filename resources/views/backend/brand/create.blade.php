@extends('backend/layouts/default')
@section('title')
Brand Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Brand Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{'admin.dashboard'}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Brand</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Brand
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.brand.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" action="{{ route('admin.brand.add.post') }}" id="example-form" enctype="multipart/form-data">
                  @csrf
 
                  <!-- Category Name -->
                   <div class="form-group">
                     <label for="text">Brand Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="brand_name" placeholder="Brand name">
                        {!! $errors->first('brand_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- Category Image -->
                  <div class="form-group">
                     <label for="text">Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="image">
                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>
              

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
@stop