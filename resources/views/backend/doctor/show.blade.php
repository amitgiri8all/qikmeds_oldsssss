@extends('backend/layouts/default')
@section('title')
Doctor Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Doctor Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.doctor.list')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Doctor Manager
         </a>
      </li>
      <li>View Doctor Manager</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  View Doctor
               </h3>
            <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
            
            <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            ID
                        </th>
                        <td>
                            {{ $data->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $data->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Email
                        </th>
                        <td>
                            {{ $data->email }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Mobile Number
                        </th>
                        <td>
                            {{ $data->mobile_number }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Reg. No
                        </th>
                        <td>
                            {{ $data->registration_number }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Education Detail's
                        </th>
                        <td>
                            @foreach($data_doctor_education_info as $key => $value)
                        
                         Subject Name :   {{$value->subject  }} </br> 
                         College Name :   {{$value->college_name  }} </br> 
                         Start year :   {{$value->start_year  }} </br> 
                         End year :   {{$value->end_year  }} </br> <br>

                            @endforeach 
                        </td>
                    </tr>

                     <tr>
                        <th>
                           Work & Experience
                        <td>
                            @foreach($data_doctor_work_info as $key => $value)
                        
                         Wokring Place Name :   {{$value->woking_place_name  }} </br> 
                         Start year :   {{$value->start_year  }} </br> 
                         End year :   {{$value->end_year  }} </br> <br>

                            @endforeach 
                        </td>
                    </tr>

                     <tr>
                        <th>
                          Specializations
                        <td>
                            @foreach($data_doctor_specializations_info as $key => $value)
                        
                           {{$value->specializations_name  }} </br> 
                        

                            @endforeach 
                        </td>
                    </tr>
                 
                    <tr>
                        <th>Doctor's Degree Name</th>
                        <td>{{$data->degree_name}}</td>
                    </tr>
        
                     <tr>
                      <th>
                        Doctor's Image
                      </th>
                      <td>
                        @php
                     if(!empty($data->image)){
                     $url = $data->image;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100">
                      </td>
                    </tr>

                         <tr>
                      <th>
                        Doctor's Signature Image
                      </th>
                      <td>
                      @php
                     if(!empty($data_doctor->signature_image)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$data_doctor->signature_image);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100">
                      </td>
                    </tr>

                </tbody>
            </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>

@stop
