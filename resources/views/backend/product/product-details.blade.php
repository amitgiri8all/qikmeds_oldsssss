@extends('backend/layouts/default')
@section('title')
Product Details Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Product Details Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.product.list')}}"> 
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Product Details Manager
         </a>
      </li>
      <li>View Product Details Manager</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  View Product Details
               </h3>
            <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
            
             <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            ID
                        </th>
                        <td>
                            {{ $data->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Medicine Name
                        </th>
                        <td>
                            {{ $data->medicine_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Igst
                        </th>
                        <td>
                            {{ $data->igst }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Hsn
                        </th>
                        <td>
                            {{ $data->hsn }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Composition
                        </th>
                        <td>
                            {{ $data->composition }}
                        </td>
                    </tr>


                    <tr>
                        <th>
                            Packing Type
                        </th>
                        <td>
                            {{ $data->packing_type }}
                        </td>
                    </tr>


                    <tr>
                        <th>
                            Packaging
                        </th>
                        <td>
                            {{ $data->packaging }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Schedule
                        </th>
                        <td>
                            {{ $data->schedule }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Sellable
                        </th>
                        <td>
                            {{ $data->sellable }}
                        </td>
                    </tr>


                     <tr>
                        <th>
                            Manufacturer
                        </th>
                        <td>
                            {{ $data_manufactoru->manufacturer_name }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            MRP
                        </th>
                        <td>
                            {{ $data->mrp }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Sale Price
                        </th>
                        <td>
                            {{ $data->sale_price }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Discount In %
                        </th>
                        <td>
                            {{ $data->discount }}
                        </td>
                    </tr>

                    
                    <tr>
                        <th>
                            Product Expiry Date
                        </th>
                        <td>
                            @if(!empty($data->product_expiry_date))
                            {{ date('d-m-Y',strtotime($data->product_expiry_date)) }}
                            @endif
                        </td>
                    </tr>

                    
                     <tr>
                        <th>
                            prescription
                        </th>
                        <td>
                       @if($data->prescription=='1')
                        <span class='badge badge-success'>Yes</span>
                       @else
                        <span class='badge badge-danger'>No</span>
                       @endif
                    </td>

                    <tr>
                        <th>
                            Status
                        </th>
                        <td>
                            {{ $data->status == 1 ? 'Active' : 'Inactive' }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Categories
                        </th>
                        <td>
                          {{$data_category_name->category_name ?? ''}}  
                        </td>
                    </tr>

                     {{-- <tr>
                        <th>
                            Sub Category
                        </th>
                        <td>
                          {{$data_sub_category_name->category_name ?? ''}}  
                        </td>
                    </tr> --}}


                     <tr>
                        <th>
                            MRP
                        </th>
                        <td>
                            {{ $data->mrp }}
                        </td>
                    </tr>


                    <tr>
                        <th>
                            Usage
                        </th>
                        <td>
                            {{ $data->usage }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            About Salt
                        </th>
                        <td>
                            {{ $data->about_salt }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Mechanism Of Action
                        </th>
                        <td>
                            {{ $data->mechanism_of_action }}
                        </td>
                    </tr>


                    <tr>
                        <th>
                            Pharmacokinets
                        </th>
                        <td>
                            {{ $data->pharmacokinets }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Onset Of Action
                        </th>
                        <td>
                            {{ $data->onset_of_action }}
                        </td>
                    </tr>


                      <tr>
                        <th>
                            Duration Of Action
                        </th>
                        <td>
                            {{ $data->duration_of_action }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Half Life
                        </th>
                        <td>
                            {{ $data->half_life }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Side Effects
                        </th>
                        <td>
                            {{ $data->side_effects }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                           Contra Indications
                        </th>
                        <td>
                            {{ $data->contra_indications }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                           Special Precautions While Taking
                        </th>
                        <td>
                            {{ $data->special_precautions_while_taking }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                           Pregnancy Related Information
                        </th>
                        <td>
                            {{ $data->pregnancy_related_Information }}
                        </td>
                    </tr>


                     <tr>
                        <th>
                           Product And Alcohol Interaction
                        </th>
                        <td>
                            {{ $data->product_and_alcohol_interaction }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                           Old Age Related Information
                        </th>
                        <td>
                            {{ $data->old_age_related_information }}
                        </td>
                    </tr>

                      <tr>
                        <th>
                          Breast Feeding Related Information
                        </th>
                        <td>
                            {{ $data->breast_feeding_related_information }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                           Children Related Information
                        </th>
                        <td>
                            {{ $data->children_related_information }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                           Indications
                        </th>
                        <td>
                            {{ $data->indications }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                           Typical Dosage
                        </th>
                        <td>
                            {{ $data->typical_dosage }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                           Storage Requirements
                        </th>
                        <td>
                            {{ $data->storage_requirements }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                           Effects Of Missed Dosage
                        </th>
                        <td>
                            {{ $data->fffects_of_missed_dosage }}
                        </td>
                    </tr>


                     <tr>
                        <th>
                           Effect Of Overdose
                        </th>
                        <td>
                            {{ $data->effects_of_overdose }}
                        </td>
                    </tr>


                     <tr>
                        <th>
                          Expert Advice
                        </th>
                        <td>
                            {{ $data->expert_advice }}
                        </td>
                    </tr>


                     <tr>
                        <th>
                           How To Use 
                        </th>
                        <td>
                            {{ $data->how_to_use }}
                        </td>
                    </tr>


                     <tr>
                        <th>
                          Faqs
                        </th>
                        <td>
                            {!! $data->faqs !!}
                        </td>
                    </tr>


                     <tr>
                        <th>
                           Deal Of The Day
                        </th>
                        <td>
                            {{ $data->deal_of_the_day }}
                        </td>
                    </tr>


                 

                       {{-- 
                     <tr>
                      <th>
                       Image
                      </th>
                      <td>
                        @php
                     if(!empty($data->image)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$data->image);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100">
                      </td>
                    </tr>
                    --}}

                </tbody>
            </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

               
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>

@stop

