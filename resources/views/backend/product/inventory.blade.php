<div class="panel panel-primary" style="border-radius:0;">
   <!--<div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        Inventory Information
      </h3>
      
      </div>-->
   <div class="col-sm-12">
      <h3 class="inner-heading"> Inventory Information </h3>
   </div>
   <div class="panel-body border">
      <form id="inventory-form" @if(isset($id)) action="{{route('admin.product.added.post',$id)}}" @endif method="post"  class="form-horizontal form-bordered ajaxforminventory">
         @csrf
         <div class="form-group">
            <label class="col-md-3 control-label" for="example-text-input">Manage Stoke</label>
            <div class="col-md-6">
               <select  id="manage_stoke" name="manage_stoke" class="form-control" size="1" required>
               <option value="Yes">Yes</option>
               <option value="No">No</option>
               </select>
            </div>
         </div>
         <input type="hidden" name="form_name" value="inventory">
         <div class="form-group">
            <label class="col-md-3 control-label" for="example-text-input">Quantity</label>
            <div class="col-md-6">
               <input type="number" min="1" id="example-text-input" name="quantity" class="form-control" value="{!! old('quantity') !!}" placeholder="Quantity" Required>
            </div>
         </div>
         <div class="form-group">
            <label class="col-md-3 control-label" for="example-text-input">Stoke Availability</label>
            <div class="col-md-6">
               <select id="stoke_availability" name="stoke_availability" class="form-control" size="1" required>
               <option value="Instoke">In Stoke</option>
               <option value="Outstoke">Out of Stoke</option>
               </select>
            </div>
         </div>
         <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
               <button type="submit"  class="btn btn-effect-ripple btn-primary submitbtn">
               @lang('button.save')
               </button>
            </div>
         </div>
      </form>
   </div>
</div>