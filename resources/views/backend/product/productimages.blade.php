 @if(!empty($productimage))
   <table class="table table-bordered productimagetb" id="productimagetb">
                        <thead>
                        <tr class="filters">

                            <th>Image</th>
                            <th>Caption</th>
                            <th>Action</th>



                        </tr>
                        </thead>

<tbody >
    @for($i=0;isset($productimage[$i]);$i++)
                                            <tr>

                                                <td><img src="{{ URL::to('storage/app/uploads/product/'.$productimage[$i]->product_id.'/'.$productimage[$i]->image)}}" height="100px" width="100px" ></td>  
                                                <td>
                                    <form method="post" role="form" class="form-horizontal form-bordered ajaxform smart-forms" enctype="multipart/form-data" id="image-form"  action="{{ URL::to('admin/product/'.$productimage[$i]->product_id.'/image') }}" >
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                       <div class="col-sm-8">
                                          <input type="text" value="{{ $productimage[$i]->caption }}" class="form-control" name="caption">
                                       </div>
                                       <div class="col-sm-4">
                                          <button type="button" data-id="{{ $productimage[$i]->pro_img_id }}" class="btn btn-success btn-sm savecaption">
                                             <i class="fa fa-trsh"></i>
                                             Save
                                          </button>
                                       </div>
                                       </form>
                                    </td>

                                                <td>
                                    @if($productimage[$i]->set_primary=="No")
                                    <a href="javascript:" data-id="{{ $productimage[$i]->pro_img_id }}" class="btn default btn-xs" id="delete_pro_img" >
                                       <i class="fa fa-trsh"></i>
                                          Delete
                                    </a>
                                    <button type="button" data-id="{{ $productimage[$i]->pro_img_id }}" class="btn btn-primary btn-xs purple setPrimary" >
                                       <i class="fa fa-trsh"></i>
                                          Make Primary
                                    </button>
                                    @else
                                    <button type="button" data-id="{{ $productimage[$i]->pro_img_id }}" class="btn default btn-xs purple" disabled>
                                       <i class="fa fa-trsh"></i>
                                          It's primary image
                                    </button>
                                    @endif
                                                </td>
                                            </tr>
            @endfor

                                        </tbody>

 </table>
 
@endif

