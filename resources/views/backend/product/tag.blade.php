<div class="panel panel-primary" style=" border-radius:0;">
   <!--<div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        Tag Information 
      </h3>
              
      </div>-->
   <div class="col-sm-12">
      <h3 class="inner-heading"> Tag Information </h3>
   </div>
   <div class="panel-body border">
      <br>
      <br>
       <form @if(isset($id)) action="{{ URL::to('admin/product/'.$id.'/tag') }}" @endif  id="tag-form" method="post"  class="form-horizontal form-bordered ajaxformtag">
       
         @csrf 
         <div class="form-group">
            <label class="col-md-3 control-label" for="example-text-input">Tag</label>
            <div class="col-md-6">
               <input type="text"  name="tag_name" id="tag_name" class="form-control" value="" placeholder="tag" Required>
               <input type="hidden" name="tag_id" id="tag_id" value="">
            </div>
         </div>
         <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
               <button type="submit"  class="btn btn-effect-ripple btn-primary submitbtn">
               @lang('button.save')
               </button> 
            </div>
         </div>
      </form>
   </div>
    <div class="tagrecord">
   
   


   </div>
</div>