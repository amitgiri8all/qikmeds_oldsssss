@extends('backend/layouts/default')
@section('title')
Faq Manager::CRM
@stop
@section('header_styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/default/tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>

<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Faq Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{'admin.dashboard'}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Faq</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Faq
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.faq.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.faq-product.add.post') }}" id="faq" enctype="multipart/form-data">
                  @csrf
 
                 

                   <div class="form-group col-sm-12">
                     <label for="text">Product</label>
                   
                      <select class="form-control"  name="product_id">
                        <option value="">Select Product</option>
                       @foreach($product as $value)
                        <option class="form-control" name="product_id" multiple value="{{$value->id}}">  {{$value->medicine_name}} </option>
                       @endforeach
                     </select>
                     {!! $errors->first('product_id', '<span class="help-block">:message</span>') !!}

                   </div>  

                    

                 <!-- Question -->
<div class="field_wrapper">
    <div>

                   <div class="form-group col-sm-11">
                     <label for="text">Question</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="{{old('question')}}"  name="question[]" placeholder="Question">
                        {!! $errors->first('question', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                  <div class="form-group col-sm-1">
                     <label for="text">Add more </label>
                     <div class="input-group col-xs-12">
                        <button type="button" class="form-control btn btn-primary add_button" ><i class="fa fa-plus-circle" aria-hidden="true"></i> </button>
                      </div>
                  </div>
                   <div class="form-group col-sm-12">
                     <label for="text">Answer</label>
                      <div class="input-group col-xs-12">
                         <textarea class="form-control ckeditor" rows="2"  cols="5" name="answer[]"></textarea>
                     </div>
                  </div>
   </div>               
</div>



                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')

  
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = "<div><div class='form-group col-sm-11 after-add-more'> <label for='text'>Question</label> <div class='input-group col-xs-12'> <input type='text' class='form-control' value='' name='question[]' placeholder='Question'></div></div><div class='form-group col-sm-1'> <label for='text'>Remove </label> <div class='input-group col-xs-12'> <button type='button' class='form-control btn btn-primary remove_button' ><i class='fa fa-minus-circle' aria-hidden='true'></i> </button> </div></div><div class='form-group col-sm-12'> <label for='text'>Answer</label> <div class='input-group col-xs-12'> <textarea class='form-control ckeditor' rows='2' cols='5' name='answer[]'></textarea> </div></div></div>"; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('.remove_div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>











<script type="text/javascript">
//
$(document).ready(function() {
    $("#my-select").select2();
    $("#my-select2").select2();
});

   $(".product").hide();
   $(".other").hide();
   $("#type").change(function(){
    if($(this).val()=='product'){
        $(".product").show();
        $(".other").hide();
    }else{
        $(".product").hide();
        $(".other").show();
    }
    
 });

</script>
 
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
 <script  src="{{ asset('assets/default/tagsinput/bootstrap-tagsinput.min.js') }}"  type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
 @stop