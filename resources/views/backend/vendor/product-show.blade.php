@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
Product-details Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
 <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
  <style type="text/css">
     .badge-danger {
    color: #fff;
    background-color: #dc3545;
}.badge-success {
    color: #fff;
    background-color: #28a745;
}


  </style>
  <style media="screen">
  .noPrint{ display: block; }
  .yesPrint{ display: block !important; }
</style> 
<style media="print">
  .noPrint{ display: none; }
  .yesPrint{ display: block !important; }
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Product View Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li class="active">Product View  list</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">

   <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
              Product View  List
            </h4>
              <div class="pull-right">
      <!--          <a href="javascript::void(0);" onclick="printThis()" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Print </a> -->
            </div>
            <div class="pull-right">
               <a href="{{route('admin.vendor.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> back </a>
            </div> 
          </div>
         <div class="panel-body">
             <div class="card" data-select2-id="9">
 
   <div class="row" id="order-body">
      <div class="col-sm-6">
         <table class="table table-hover box-body text-wrap table-bordered">
            <tbody>
              
               <tr>
                  <td class="td-title">Customer Name :</td>
                  <td>{{$order->user->name}}</td>
               </tr>
               
               <tr>
                  <td class="td-title">Phone:</td>
                  <td> {{$order->user->mobile_number}}</td>
               </tr>
               <tr>
                  <td class="td-title">Email:</td>
                  <td>{{$order->user->email}}</td>
               </tr>
               <tr>
                  <td class="td-title">Delivery Address :</td>
                  <td><h4>{{($order->deliveryaddress->name ?? '')}}</h4>
                    <h5>{{($order->deliveryaddress->mobile_number ?? '')}}</h5>

                  <span class="badge" style="float:right;">{{($order->deliveryaddress->address_type ?? '')}}</span>
                  <p>{{($order->deliveryaddress->address_delivery ?? '')}}<br>
                   {{($order->deliveryaddress->city ?? '')}},
                    {{($order->deliveryaddress->state ?? '')}} - {{($order->deliveryaddress->pin_code ?? '')}}</p></td>
               </tr>
               
               
            </tbody>
         </table>
      </div>
      <div class="col-sm-6">
         <table class="table table-bordered">
            <tbody>
               <tr>
                  <td class="td-title">Order status:</td>
                  <td>{{App\Helpers\Helper::$order_status[$order->order_status] ?? ""}}</td>
               </tr>

               <tr>
                  <td>Shipping status:</td>
                  <td>Not sent</td>
               </tr>

               <tr>
                  <td>Payment status:</td>
                  <td>Unpaid</td>
               </tr>
              
               <tr>
                  <td>Payment method:</td>
                  <td>{{($order->orderpayment->payment_method ?? '')}}</td>
               </tr>
               <td class="td-title"><i class="far fa-money-bill-alt nav-icon"></i> Currency:</td>
                  <td>₹</td>
               <tr>
                  <td> Created at:</td>
                  <td>{{date('d F,Y h:i A',strtotime($order->created_at))}}</td>
               </tr>
            </tbody>
         </table>
    
      </div>
   </div>
        <div class="row" data-select2-id="8">
         <div class="col-sm-12" data-select2-id="7">
            <div class="card collapsed-card" data-select2-id="6">
               <div class="table-responsive">
                  <table class="table table-hover box-body text-wrap table-bordered">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Product Image</th>
                           <th>Name</th>
                            <th class="product_price">Price</th>
                           <th class="product_qty">Quantity</th>
                           
                         </tr>
                     </thead>
                    <tbody>
                        <?php ///echo "<pre>"; print_r($order->OrderItem); die();?>
                      @foreach($order->OrderItem as $key=>$value) 
                       @php $val = App\Helpers\Helper::get_vendor_product_name($value->vendor_id); @endphp
                        <tr>
                           <td>
                               {{$value->order_id ?? ''}} 
                              {{--  @if(empty($val->name))
                              <div class="input-group col-xs-12">
                                <input type="checkbox" class="largerCheckbox checkbox" value="{{$value->ord_item_id}}" name="order">
                              </div>
                              @else
                              <span class="badge badge-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
                              @endif
                              --}}
                           </td>

                           <td>
                            <?php
                        if(!empty($value->data)){ 
                       $data= json_decode($value->data, true);
                               
                     // print_r( $data['product']['image']) ;die;   
                           //$obj = json_decode($value->data,true);

                          
                              $url = $data['product']['image'] ?? $data['image']; 
                              //echo $url.'========'; 
                     }
                           ?>  

            <img src="<?php echo $url ; ?>" border="0" width="100" height="100" class="img-rounded" align="center" /> 

                            </td>
                           <td>
                               {{ $value->product_name}}</td>
                           <td class="product_total item_id_55">
                            ₹ {{ $value->product_price}}</td>                           <td class="product_qty">
                              {{$value->quantity_order}}</td>
                          
                        </tr>
                       @endforeach 
                     </tbody>
                
                  </table>

      {{--      @if($order->status!='accepted')
                <tr>
                   <td>
                       <div class="form-group">
                      <div class="input-group col-xs-4">
                          <select class="form-control" name="vendor_id" id="vendor_id" >
                             <option value="">Please Select Vendor</option>
                             @foreach($vendors as $key=>$value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                             @endforeach
                          </select>
                      </div>
                      </div>
             <div class="input-group col-xs-2">
                <button type="submit" id="accepted" data-id="{{$order->order_id}}" class="btn btn-primary btn-block btn-md btn-responsive">
                        Assign To Vendors
               </button>
            </div>   
               
                  <br>
               </td>

               </tr> 

               @endif

               --}} 

               </div>
            </div>
         </div>
      </div>
   </form>
   <div class="row">
   {{--   <div class="col-sm-6">
         <div class="card collapsed-card">
            <table class="table table-bordered">
               <tbody>
                  <tr>
                     <td class="td-title-normal">SubTotal:</td>
                     <td style="text-align:right" class="data-subtotal">₹ {{$order->Order[$key]['sub_total']}}</td>
                  </tr>
                  <tr>
                     <td>Qikmeds Discount(-):</td>
                     <td style="text-align:right">₹ {{$order->Order[$key]['discount_amount']}}</td>
                  </tr> 
                  @if(!empty($order->Order))
                   <tr>
                     <td>Promo Discount</td>
                     <td style="text-align:right">₹
                     {{$order->Order[$key]['discount_coupon_code']}} </td>
                  </tr>
                  @endif
                  
                   <tr>
                     <td>Delivery Charge</td>
                     <td style="text-align:right">
                                    <?php if($order->Order[$key]['shipping_amount']>0){?>
                                    <span>₹ {{$order->Order[$key]['shipping_amount']}}</span>
                                <?php }else{ ?>
                                    <span>Free</span>
                                    <?php  }?></td>
                  </tr>
                  <tr style="background:#f5f3f3;font-weight: bold;">
                     <td>Net Amount Payable</td>
                     <td style="text-align:right" class="data-total">₹ 
                      {{$order->Order[$key]['total_payed_amount']}}
                    </td>
                  </tr>
                  
                  
               </tbody>
            </table>
         </div>
      </div>  --}}
    <!--   <div class="col-sm-6">
         <div class="card">
            <table class="table table-hover box-body text-wrap table-bordered">
               <tbody>
                  <tr>
                     <td class="td-title">Note:</td>
                     <td>Empty</a>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         
      </div> -->
   </div>
</div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')

 <script type="text/javascript">
 $(document).ready(function(){
    $('#myPageForm').click(function(){
        //Filling the hidden input
        var htmlToPrint = $(".paddingleft_right15").html(); //I'm using a class and not an ID 'cause .NET will change your GridView's ID when rendering you page
        $("#htmlToPrint").value(htmlToPrint);
        return true;
    });
});

 function myFunction()
{
    window.print();
}

 </script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
 

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>

<script type="text/javascript">

var assing_vendor="{{URL::to('admin/order/assign-vendor')}}";

var token = '{{ csrf_token() }}';

//Accepted All Orders
$(document).ready(function(){
    $('#accepted').on('click',function(){
            var order_id = $(this).attr("data-id");
            var vendor_id = $("#vendor_id").val();
         //  alert(order_id+'==='+vendor_id)
         if($('.checkbox').is(":checked")){
           var ord_item_id=[];
             $("input:checkbox[name=order]:checked").each(function(){
                  ord_item_id.push($(this).val());
                });
           //  alert(ord_item_id)
             event.preventDefault();
          $.ajax({
            url:  assing_vendor,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id,vendor_id:vendor_id},
            dataType: 'json',
            success: function(response) {
             alert(response.message);
             //window.setTimeout(function(){location.reload()},3000)
//
            location.reload(); 
            // setTimeout(function(){
            // $("#overlay").fadeOut(1000);
             //  $("#accepted").html("Accepted");
             //  $('#decline').hide();
             //  $('#accepted').hide();
              // $("#datatable").load(document.URL + " #datatable");
              // toastr[response.status](response.message, "Order");
           // },1000);

            }
            });
        }else{
         alert('Checkbox is not checked')
        }
    });
});

 
 /*$('#selectors').on('change',function(){
  // alert('ok')
var vendor_id =  $( "select option:selected" ).val();
 var order_id = $(this).attr("data-id");
//alert(order_id)
 $.ajax({
    url:assing_vendor,
    type: 'POST',
    data: { _token :token,vendor_id:vendor_id,order_id:order_id },
    success:function(msg){

       alert("You has been created successfully assing vendor order.");
       //window.setTimeout(function(){location.reload()},3000)

      location.reload(); 
      // You has been created successfully accepte order.
    }
 });


})*/


  </script>

@stop
