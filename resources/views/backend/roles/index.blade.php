@extends('backend.layouts.default')

{{-- Page title --}}
@section('title')
    Region Manager::CRM
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .badge {
    display: inline-block;
    min-width: 14px;
    padding: 9px 3px;
    font-size: 15px;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    margin: 0.5mm;
    white-space: nowrap;
    vertical-align: baseline;
    background-color: #a94442;
    border-radius: 10px;
}
</style>

<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Role Manager</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('admin.dashboard')}}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li>Role Manager</li>
            <li class="active">View All Roles</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
                       <div class="panel panel-primary ">
                              <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Role List
                    </h4>
                    <div class="pull-right">
                    <a href="{{route('roles.create')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add New Role</a>
                    </div>
                </div>

                             
                <div class="panel-body">
                    <table class="table table-bordered " id="table1">
                       <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Permissions</th>
                        <th>Action</th>
                    </tr>
                </thead>
                 <tbody>
                    @foreach($roles as $key => $role)
                        <tr data-entry-id="{{ $role->id }}">
                            <td>
                                {{ $role->id ?? '' }}
                            </td>
                            <td>
                                {{ $role->name ?? '' }}
                            </td>
                            <td>
                                @foreach($role->permissions()->pluck('showing_name') as $permission)
                                    <span class="badge badge-info">{{ $permission }}</span>
                                @endforeach
                            </td>
                            <td>
                                   <a class="btn btn-info" href="{{ route('roles.edit', $role->id) }}" style="margin:1px;">
                                    Edit
                                   </a>
                                   <a class="btn btn-primary" href="{{ route('roles.show', $role->id) }}" style="margin:1px;">
                                    View
                                   </a>
                                <form action="{{ route('roles.destroy', $role->id) }}" method="POST" onsubmit="return confirm('Are you sure to delete this Record?');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
                </div>
            </div>
        </div>    <!-- row-->
    </section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
    <script>
       
    </script>
@stop
