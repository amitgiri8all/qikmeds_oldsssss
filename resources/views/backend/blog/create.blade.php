@extends('backend/layouts/default')
@section('title')
Blog Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Blog Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{'admin.dashboard'}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Add new Blog</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Create a new Blog
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.blog.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" action="{{ route('admin.blog.add.post') }}" id="example-form" enctype="multipart/form-data">
                  @csrf

                  <!-- Category Name -->
                   <div class="form-group">
                     <label for="text">Blog Title</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" name="title" placeholder="title">
                        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>
                      <!-- Blog Image -->
                   <div class="form-group">
                     <label for="text">Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" value="{{old('image')}}" name="image">
                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- Category Name -->
                  <div class="form-group">
                     <label for="text">Description</label>
                     <div class="input-group col-xs-12">
                         <textarea class="form-control" rows="2" id="ckeditor_full" cols="5" name="description"></textarea>
                     </div>
                  </div>

                    <!-- Blog Status -->
                    <div class="form-group">
                     <label for="text">Status</label>
                     <div class="input-group col-xs-12">
                          <select class="form-control" name="status" id="type" >
                             <option value="">Please Select Type</option>
                             <option value="1">Active</option>
                             <option value="0">Inactive</option>
                          </select>
                         {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>

@stop
