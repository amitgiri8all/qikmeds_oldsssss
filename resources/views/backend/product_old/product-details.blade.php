@extends('backend/layouts/default')
@section('title')
Product Details Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Product Details Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.product.list')}}"> 
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Product Details Manager
         </a>
      </li>
      <li>View Product Details Manager</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  View Product Details
               </h3>
            <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ url()->previous() }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
            
             <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            ID
                        </th>
                        <td>
                            {{ $data->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Medicine Name
                        </th>
                        <td>
                            {{ $data->medicine_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Type of sell
                        </th>
                        <td>
                            {{ $data->type_of_sell }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            Salt
                        </th>
                        <td>
                            {{ $data->salt }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Therapeutic Class
                        </th>
                        <td>
                            {{ $data->therapeutic_class }}
                        </td>
                    </tr>


                    <tr>
                        <th>
                            Chemical class
                        </th>
                        <td>
                            {{ $data->chemical_class }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Manufacturer
                        </th>
                        <td>
                            {{ $data_manufactoru->manufacturer_name }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            MRP
                        </th>
                        <td>
                            {{ $data->mrp }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Sale Price
                        </th>
                        <td>
                            {{ $data->sale_price }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Discount In %
                        </th>
                        <td>
                            {{ $data->discount }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Deal Of The Day
                        </th>
                        <td>
                            {{ $data->deal_of_the_day }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Set As Home
                        </th>
                        <td>
                            {{ $data->set_as_home }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Set As Home
                        </th>
                        <td>
                            {{ $data->set_as_home }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Is Featured
                        </th>
                        <td>
                            {{ $data->is_featured }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Product Expiry Date
                        </th>
                        <td>
                            @if(!empty($data->product_expiry_date))
                            {{ date('d-m-Y',strtotime($data->product_expiry_date)) }}
                            @endif
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Brand
                        </th>
                        <td>
                            {{ $data_brand->brand_name ?? ''}}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Habit forming
                        </th>
                        <td>
                            {{ $data->habit_forming }}
                        </td>
                    </tr>
                     
                     <tr>
                        <th>
                            prescription
                        </th>
                        <td>
                       @if($data->prescription=='1')
                        <span class='badge badge-success'>Yes</span>
                       @else
                        <span class='badge badge-danger'>No</span>
                       @endif
                    </td>

                    <tr>
                        <th>
                            Status
                        </th>
                        <td>
                            {{ $data->status == 1 ? 'Active' : 'Inactive' }}
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Categories
                        </th>
                        <td>
                          {{$data_category_name->category_name ?? ''}}  
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Sub Category
                        </th>
                        <td>
                          {{$data_sub_category_name->category_name ?? ''}}  
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Alternate medicines
                        </th>
                        <td>
                          {{$data->alternate_medicines}}  
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Side effects
                        </th>
                        <td>
                           {{$data->side_effects}}   
                        </td>
                    </tr>

                     <tr>
                        <th>
                           How to use
                        </th>
                        <td>
                          {{$data->how_to_use}}    
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Description
                        </th>
                        <td>
                           {!!$data->description!!}   
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Key Benefits
                        </th>
                        <td>
                           {!!$data->key_benefits!!}   
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Directions For Use
                        </th>
                        <td>
                           {!!$data->directions_for_use!!}   
                        </td>
                    </tr>

                     <tr>
                        <th>
                            Safety Information  
                        </th>
                        <td>
                          {!!$data->safety_information!!}    
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Other Information
                        </th>
                        <td>
                          {!!$data->other_information!!}    
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Product Highlights
                        </th>
                        <td>
                           {!!$data->product_highlights!!}   
                        </td>
                    </tr>
                       {{-- 
                     <tr>
                      <th>
                       Image
                      </th>
                      <td>
                        @php
                     if(!empty($data->image)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$data->image);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100">
                      </td>
                    </tr>
                    --}}

                </tbody>
            </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

               
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>

@stop

