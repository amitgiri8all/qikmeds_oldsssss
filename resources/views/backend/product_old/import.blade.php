@extends('backend.layouts.default')
{{-- Web site Title --}}
@section('title')
Product Manager::CRM
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/default/tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"/>
<link href="{{ asset('assets/css/jquery.tree.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Product Manager    
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{ route('admin.dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         @lang('general.dashboard')
         </a>
      </li>
      <li>Product Manager</li>
      <li class="active">
         Create
      </li>
   </ol>
</section>
<!-- Main content -->





<!-- Main content -->
<section class="content">
   <div class="row">

      <div class="col-lg-6">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Import New Products
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.product.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>

            <div class="panel-body">
               <form method="post" action="{{ route('admin.product.import.post') }}" class="ajax_form" id="ajax_form" enctype="multipart/form-data">
                   @csrf
                   <!-- Banner title -->
                   <div class="form-group">
                     <label for="text">Import Product</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="file">
                        {!! $errors->first('file', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>

      <div class="col-lg-6">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Import Image Products
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.product.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>

            <div class="panel-body">
               <form method="post" action="{{ route('admin.product.store_zip.post') }}" class="ajax_form" id="ajax_formss" enctype="multipart/form-data">
                   @csrf
                   <!-- Banner title -->
                   <div class="form-group">
                     <label for="text">Import Product</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="export">
                        {!! $errors->first('export', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>







   </div>
   <!-- row-->
</section>











 
<!-- content -->
@stop
@section('footer_scripts')
<script></script>
 
 <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"
   type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"
   type="text/javascript"></script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}"
   type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
  <script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript"></script>
    <script  src="{{ asset('assets/js/product.js') }}"  type="text/javascript"></script>
 
@stop