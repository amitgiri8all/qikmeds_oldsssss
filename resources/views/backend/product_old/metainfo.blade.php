<div class="panel panel-primary" style=" border-radius:0;">
   <div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
             data-hc="white"></i>
        Meta Information
      </h3>
      
      </div> 
   <div class="col-sm-12">
      <h3 class="inner-heading"> Meta Information </h3>
   </div>
   <div class="panel-body">
  
      <form method="post" id="meta-form" class="ajaxform-meta" @if(isset($id)) action="{{route('admin.product.added.post',$id)}}" @endif >
       
          @csrf
         <div class="form-group">
            <label for="validate-text">Page Meta Title</label>
            <div class="input-group col-sm-12">
               <input type="text" class="form-control" name="meta_title" id="meta_title"
                  value="{!! old('meta_title') !!}" placeholder="Meta title">
            </div>
         </div>
         <input type="hidden" name="form_name" value="meta_form">
         <div class="form-group">
            <label for="validate-length">Page Meta Keywords </label>
            <div class="input-group col-sm-12">
               <input type="text" class="form-control" value="{!! old('meta_keyword') !!}" name="meta_keyword" id="validate-length" placeholder="Meta Keyword">
            </div>
         </div>
         <div class="form-group">
            <label for="validate-number">Page Meta Description</label>
            <div class="input-group col-sm-12">
               <textarea class="form-control" name="meta_description"  id="validate-number"
                  placeholder="Meta Description" >{!! old('meta_description') !!}</textarea>
            </div>
         </div>
         <div class="col-md-12 mar-10">
            <div class="col-xs-4 col-md-4"></div>
            <div class="col-xs-4 col-md-2">
               <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive submitbtn">
               Save
               </button>
            </div>
         </div>
      </form>
   </div>
</div>