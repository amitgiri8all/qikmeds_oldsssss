<style>
   .btn-file {
   position: relative;
   overflow: hidden;
   }
   .btn-file input[type=file] {
   position: absolute;
   top: 0;
   right: 0;
   min-width: 100%;
   min-height: 100%;
   font-size: 100px;
   text-align: right;
   filter: alpha(opacity=0);
   opacity: 0;
   outline: none;
   background: white;
   cursor: inherit;
   display: block;
   }
</style>
<div class="panel panel-default" style="border-radius:0;">
 <!--   <div class="panel-heading">
      <h3 class="panel-title">
         <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
            data-hc="white"></i>
         Image Information
      </h3>
   </div> -->
   <div class="panel-body">
      <div class="imagerecord">
      </div>
   <form @if(isset($id)) action="{{ URL::to('admin/product/'.$id.'/image') }}" @endif id="image_form" method="post" enctype="multipart/form-data"  class="form-horizontal form-bordered ajaxform">
   
       @csrf 
      <div class="row">
         <div class="col-sm-3">
            <lable>Showing Order</lable>
            <input type="text" class="form-control" name="order" />  
         </div>
         <div class="col-sm-3">
            <lable>Set As Primary</lable>
            <select class="form-control" name="set_primary">
               <option value="Yes">Yes</option>
               <option selected  value="No">No</option>
            </select>
         </div>
         <div class="col-sm-3">
            <lable>&nbsp;</lable>
            <label class="field prepend-icon file">
            <span class="button"> Choose File </span>
            <input class="gui-file" name="image" id="image" required onchange="document.getElementById('uploader1').value = this.value;" type="file">
            <input class="gui-input" id="uploader1" placeholder="no file selected" readonly="" type="text">
            <span class="field-icon"><i class="fa fa-upload"></i></span>
            {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
            </label>     
         </div>
         <lable>&nbsp;</lable>
         <div class=" col-sm-3" style="margin-top:5px;">
            <button type="submit"  class="btn btn-effect-ripple btn-primary submitbtn">
            Save
            </button>
         </div>
      </div>
      </form>
   </div>


</div>


