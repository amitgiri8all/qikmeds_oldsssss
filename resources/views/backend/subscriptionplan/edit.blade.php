@extends('backend/layouts/default')
@section('title')
Subscription Plan Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Subscription Plan Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Edit new Subscription Plan</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                     Edit Page
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.subscriptionplan.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" id="subscriptionmeta" enctype="multipart/form-data">
                  @csrf

                       <!-- Type -->
                   <div class="form-group">
                     <label for="text"> Type</label>
                      <select class="form-control col-md-7 col-xs-12 " id="type" name="plan_type" value={{$data->plan_type}}>
                      <option value="user" @if(($data->plan_type) =='user') selected @endif >User</option>

                      <option value="vendor" @if(($data->plan_type) == 'vendor') selected @endif >Vendor</option>
                      </select>
                      {!! $errors->first('plan_type', '<span class="help-block">:message</span>') !!}
                      
                  </div>
                  
                   <!-- Plan Name -->
                   <div class="form-group ">
                     <label for="text">Plan Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->plan_name)){!! $data->plan_name !!}@else{!! old('plan_name') !!}@endif"  name="plan_name" placeholder="plan_name">
                        {!! $errors->first('plan_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                  <!-- Duration -->
                  <div class="form-group ">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Duration<span class="required" aria-required="true">*</span>
                      </label>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                          <?php
                          //view number of value
                           $duration_val =  $data->duration;
                           $duration_number =  substr($duration_val, 0, 1);
                          ?>
                          <input class="form-control col-md-6 col-xs-12 valid"  id="duration_value" name="duration_value" type="text"  aria-invalid="false" value="@if(isset($duration_number)){!! $duration_number!!}@else{!! old('$duration_number') !!}@endif">
                        </div>
                      <div class="col-md-3 col-sm-6 col-xs-12">
                          <?php 
                           //view month and year 
                           $duration_time =  $data->duration;
                           $str_year = substr($duration_time, 1);
                           //echo $str_year;die;
                           ?>
                          <select class="form-control col-md-6 col-xs-12 valid" id="duration_class" name="duration_class" aria-invalid="false"
                           value={{$str_year}}
                          > 
                            <option value="month" @if(trim($str_year) =="month") selected @endif>month(s)</option>

                            <option value="year"  @if(trim($str_year) =="year") selected @endif>year(s)</option>

                          </select>
                          <input type="hidden" name="duration" id="duration" aria-invalid="false" aria-describedby="duration-error" class="valid" 
                          value="@if(isset($data->duration)){!! $data->duration !!}@else{!! old('duration') !!}@endif"><span id="duration-error" class="help-block error-help-block"></span>
                        </div>
                        
                    </div>


                  <!-- Price -->
                   <div class="form-group">
                     <label for="text" style="
                                    /* padding: 36px; */
                                    margin-top: 50px;
                                    margin-left: -767px;
                                ">Price</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->price)){!! $data->price !!}@else{!! old('price') !!}@endif" name="price"  id="price" placeholder="Price">
                        {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                     <!-- Sell Price -->
                   <div class="form-group ">
                     <label for="text">Sale Price</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"  value="@if(isset($data->sale_price)){!! $data->sale_price !!}@else{!! old('sale_price') !!}@endif"  id="sale_price" name="sale_price" placeholder="Sale price">
                        {!! $errors->first('sale_price', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Discount -->

              <div class="form-group">
                  <label for="text">Discount</label>
                  <div class="input-group col-xs-12">
                     <input  type="text" name="product_discount" id="discount" value="@if(isset($data->discount)){!! $data->discount !!}@else{!! old('discount') !!}@endif" class="form-control" placeholder="Auto Fill" readonly="readonly">
                  </div>
            </div>  
           
                 <!--  Image -->
                  <div class="form-group">
                     <label for="text">  Image</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="image">
                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  @php
                    if(!empty($data->image)){
                    $url= $data->image;
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   }
                  @endphp
                      <img src="<?php echo $url; ?>" width="100" height="100">
                  </div>     
              
           <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('Update')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<!-- <script type="text/javascript">
            $('#us3').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                    // locationNameInput: $('#address_name')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script> -->
<script>
   $('#duration_class').change(function(){
            var duration_value = $('#duration_value').val();
            var duration_class = $(this).val();
            var duration = duration_value+" "+duration_class;
            $('#duration').val(duration);
            });

            $('#duration_value').change(function(){
            var duration_class = $('#duration_class').val();
            var duration_value = $(this).val();
            var duration = duration_value+" "+duration_class;
            $('#duration').val(duration);
            });
</script> 
<script>
     $(function () {
            $("#price,#sale_price").change(function () {
                var price = parseFloat($("#price").val());
                var sale_price = parseFloat($("#sale_price").val());
                $("#discount").val(sale_price*100/price);
            })
        });
</script>   
@stop