@extends('backend/layouts/default')
@section('title')
Access Level Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>
<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Zone Manager
   </h1>
   <ol class="breadcrumb">
      <li>
          <a href="{{ route('dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Zone Manager</li>
   </ol>
</section>
<!-- Main content -->

<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Edit Zone 
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ URL::to('admin/zone/list') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
                   {!! Form::model($zone,['route' => ['update.zone',$zone->id],'method'=>'post','class'=>'form-horizontal form-label-left validation','enctype'=>'multipart/form-data']) !!}
                                {{csrf_field()}}
                                {{method_field('post')}}
                             <div id="map" style="height: 300px;"></div>
                            <br>
                                 <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        {!!  Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control col-md-7 col-xs-12' )) !!}
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!!  Form::textarea('description', null, array('placeholder' => 'Name','class' => 'form-control col-md-7 col-xs-12','rows'=>'2' )) !!}
                                    </div>
                                </div>

 
                             <input type="hidden" name="point" value="{{json_encode($zone->points)}}">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Status <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {!!  Form::select('status', ['1'=>'Active','0'=>'Inactive'],$zone->status, array('class' => 'form-control col-md-7 col-xs-12')) !!}                                        
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        
                                        <button type="reset" class="btn btn-primary">Reset</button>
                                        <button id="send" type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/intl-tel-input/js/intlTelInput.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/validation.js') }}" type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/ckeditor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/jquery.js') }}"  type="text/javascript" >
</script>
<script  src="{{ asset('assets/vendors/ckeditor/js/config.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/pages/editor.js') }}"  type="text/javascript">
</script>
<script  src="{{ asset('assets/js/timezones.full.js') }}"  type="text/javascript">
</script>



 
   
    <script>
        // This example requires the Drawing library. Include the libraries=drawing
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
        window.shape=null;
        function initMap() {
           var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 25.14901470, lng: 75.83543170},
                zoom: 5
            });

            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [/*'marker', 'circle','polyline', 'rectangle',*/ 'polygon']
                },
                markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                circleOptions: {
                    fillColor: '#ffff00',
                    fillOpacity: 1,
                    strokeWeight: 5,
                    clickable: false,
                    editable: true,
                    zIndex: 1
                }
            });
            var zones = <?php echo json_encode($zones) ?>;
            for (var i in zones){
                var bermudaTriangle_all = new google.maps.Polygon({
                    paths: zones[i]['points'],
                    strokeColor: '#2aff20',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#2529ff',
                    fillOpacity: 0.35
                });
                bermudaTriangle_all.setMap(map);
            }


            var bermudaTriangle = new google.maps.Polygon({
                paths: <?php echo json_encode($zone->points) ?>,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });
            bermudaTriangle.setMap(map);
            
            drawingManager.setMap(map);

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {

                if(shape!=null){
                    shape.overlay.setMap(null);
                }
                if (event.type == 'polygon') {
                    shape=event;
                    var radius = event.overlay.getPath().getArray();

                    //console.log(JSON.stringify(radius));
                    $("[name=point]").val(JSON.stringify(radius));
                }
            });
        }
 
 $("#zone_form").submit(function(){
    var pointVal = $("[name=point]").val();
    if(pointVal == ''){
        alert('Please draw a zone area first');
        return false;
    }

 });

    </script>
 <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=drawing&callback=initMap"
            async defer></script>
@stop