@extends('backend.layouts.default')
{{-- Page title --}}
@section('title')
Operation Manager::CRM
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
 <link href="{{ asset('assets/css/toastr.css') }}" rel="stylesheet" type="text/css"/>

<style type="text/css">
  .search_btn_list { border:none; border-radius:3px; background-color:#fcaf17; padding:8px 10px;font-weight:normal; color:#fff; font-size:14px; line-height:20px;
    margin-top: 21px;}

</style>
@stop
{{-- Page content --}}
@section('content')
<section class="content-header">
   <h1>Operation Manager</h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{ route('dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Operation Manager</li>
      <li class="active">Operation</li>
   </ol>
</section>
<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
      <div class="panel panel-primary ">
         <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
               Operation List
            </h4>
           <!--  <div class="pull-right">
               <a href="{{route('user.add')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add New user</a>
            </div> -->
         </div>
            <div class="clearfix"></div>
            <div id="map" style="height: 300px;"></div> 
         <div class="panel-body">
            <table class="table table-bordered " id="table1">
                <input type="hidden" value="{{csrf_token()}}" id="token">
               <thead>
                  <tr class="filters">
                    <th>S.No</th>
                    <th>Id</th>
                    <th>Zone</th>
                    <th>Vendor</th>
                     <th>Driver</th>
<!--                     <th>Delivery Charges</th>
 -->                    <th>Delivery Times</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script  src="{{ asset('assets/js/toastr.min.js') }}"  type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.autocomplete.js') }}"></script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content"></div>
   </div>
</div>  
<script>
   $(function() {
   var table = $('#table1').DataTable({
           processing: true,
           serverSide: true,
           bDestroy: true,
           ajax: '{!! route('operation_view.list.datatable') !!}',
               columns: [
            { data: 'Slno', name: 'Slno' },
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'vendor', name: 'vendor',orderable: false, searchable: false },
             { data: 'driver', name: 'driver' ,orderable: false, searchable: false},
/*            { data: 'delivery_charges', name: 'delivery_charges' ,orderable: false, searchable: false},
*/            { data: 'delivery_times', name: 'delivery_times',orderable: false, searchable: false },
        ]
   });
    table.on( 'draw', function () {
      $('.livicon').each(function(){
       $(this).updateLivicon();
     });
    } );

 });

    // This example requires the Drawing library. Include the libraries=drawing
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
    window.shape=null;
    function initMap() {
       var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 25.14901470, lng: 75.83543170},
                zoom: 5
            });

        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['marker', 'circle','polyline', 'rectangle', 'polygon']
            },
            markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
            circleOptions: {
                fillColor: '#ffff00',
                fillOpacity: 1,
                strokeWeight: 5,
                clickable: false,
                editable: true,
                zIndex: 1
            }
        });

        var zones = <?php echo json_encode($zones) ?>;
        for (var i in zones){
            var bermudaTriangle_all = new google.maps.Polygon({
                paths: zones[i]['points'],
                strokeColor: '#2aff20',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#2529ff',
                fillOpacity: 0.35
            });
            bermudaTriangle_all.setMap(map);
        }

        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
            bermudaTriangle.setMap(null);
            if(shape!=null){
                shape.overlay.setMap(null);
            }
            if (event.type == 'polygon') {
                shape=event;
                var radius = event.overlay.getPath().getArray();

                //console.log(JSON.stringify(radius));
                $("[name=point]").val(JSON.stringify(radius));
            }
        });
    }
/*
function updateUserZone(obj,zone_id,user_id) {

    $.ajax({
        data: {
            zone_id:zone_id,
            old_user_id:$(obj).siblings().val(),
            _method:'PATCH'
        },
        type: "PATCH",
        url: "{!! url('operation') !!}/"+(user_id=='' ? 0 : user_id),
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function( data ) {
           alert('ok')
            $(obj).siblings().val(user_id);
        },
        error: function( data ) {
            alert('error')
           toastr['error']('something is wrong', "Error");
        }
    });
    
}*/

function updateUserZone(obj,zone_id,user_id) {
  //alert(obj+'==='+zone_id+'==='+user_id)
    $.ajax({
        data: {
            zone_id:zone_id,
            old_user_id:$(obj).siblings().val(),
            type:$(obj).attr('data-type'),
            _method:'PATCH'
        },
        type: "PATCH",
        url: "{!! url('operation') !!}/"+(user_id=='' ? 0 : user_id),
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function( data ) {
            $(obj).siblings().val(user_id);
           toastr['success']('You have been successfully update', "Success");
            setTimeout(function(){ location.reload(); }, 5000);

        },
        error: function( data ) {
           toastr['error']('something is wrong', "Error");
        }
    });
    
}

function updateDelivaryCharges(deliveryCharge,zone_id) {
     if(isNaN(deliveryCharge)){
        alert("please enter valid number");
        return false
    }
    $.ajax({
        data: {
            zone_id:zone_id,
            delivery_charges:parseFloat(deliveryCharge),
            _method:'PATCH'
        },
        type: "POST",
        url: "{!! url('zone') !!}/"+zone_id,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function( data ) {
           // alert('ok')
            console.log(data);
         toastr['success']('You have been successfully update', "Success");
                     setTimeout(function(){ location.reload(); }, 5000);

        },
        error: function( data ) {
            alert('error')
                       alert('error')
           toastr['error']('something is wrong', "Error");


        }
    });

}

function updateDeliveryTimes(deliveryTimes,zone_id) {
    $.ajax({
        data: {
            zone_id:zone_id,
            package_id:deliveryTimes,
            _method:'PATCH'
        },
        type: "POST",
        url: "{!! url('zone') !!}/"+zone_id,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function( data ) {
           
         toastr['success']('You have been successfully update', "Success");
        },
        error: function( data ) {
            toastr['error']('something is wrong', "Error");
        }
    });

}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=drawing&callback=initMap"
        async defer></script>
@stop