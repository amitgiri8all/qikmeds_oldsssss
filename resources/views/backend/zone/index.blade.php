@extends('backend.layouts.default')

{{-- Page title --}}
@section('title')
    Zones Manager::CRM
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />

    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
 <link href="{{ asset('assets/css/toastr.css') }}" rel="stylesheet" type="text/css"/>


<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type="text/css">
    .no_active{display: none;}
    .show_active{display: block;}
</style>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Zones Manager</h1>
        <ol class="breadcrumb">
            <li>
                 <a href="{{ route('dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
            </li>
            <li>Zones Manager</li>
            <li class="active">Zones</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
	                     <div class="panel panel-primary ">
                              <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Zones List
                    </h4>
                    <div class="pull-right">
                    <a href="{{url('admin/zone/create')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Add New Zone</a>
                    </div>
                </div>

                        <div class="x_title">
                           <div class="clearfix"></div>
                            <div id="map" style="height: 300px;"></div>
                        </div>

                 <div class="panel-body">
                    <table class="table table-bordered " id="serverside_datatable">
                       <thead>
                          <tr class="filters">
                             <th>ID</th>
                             <th>Name</th>
                             <th>Description</th>
                              <th>Created On</th>
                             <th>Actions</th>
                          </tr>
                       </thead>
                       <tbody>
                       </tbody>
                    </table>
                </div>
            </div>
        </div>    <!-- row-->
    </section>
 @stop

{{-- page level scripts --}}
@section('footer_scripts')
<script  src="{{ asset('assets/js/toastr.min.js') }}"  type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('public/assets/js/zone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    var datatbleurl2= "{!! route('zone.data') !!}";
    var statusurl = "{!! route('admin.zone.status') !!}";
    var defaulturl = "{!! route('admin.zone.default') !!}";
    var token = '{{ csrf_token() }}';
</script>
 <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=drawing&callback=initMap"
            async defer></script>
<script> 
    $(function () {
        var table = $('#serverside_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: datatbleurl2,
            columns: [
                 {data: 'id', name: 'id'},
                 {data: 'name', name: 'name'},
                 {data: 'description', name: 'description'},
                 {data: 'created_at', name: 'created_at'},
                 {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
      });
</script>


@stop
