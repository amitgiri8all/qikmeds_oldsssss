@extends('backend/layouts/default')
@section('title')
Access Level Manager::CRM
@stop
@section('header_styles')
<link href="{{ asset('assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" media="screen"/>

<link href="{{ asset('assets/css/pages/editor.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
@stop
{{-- Content --}}

@section('content')
<section class="content-header">
   <h1>
      Zone Manager
   </h1>
   <ol class="breadcrumb">
      <li>
          <a href="{{ route('admin.dashboard') }}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Zone Manager</li>
   </ol>
</section>
<!-- Main content -->

<div class="form-group col-sm-12">
                     <label for="text">Address Location</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="address_name" placeholder="Address name" class="form-control" name="address">
                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  

<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                  Add New Zone 
               </h3>
                 <div class="pull-right" style="margin-top: -25px;">
                    <a href="{{ URL::to('admin/zone/list') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
                    </div>
            </div>
            <div class="panel-body">
                 {!! Form::open(['route' => 'zone.store','method'=>'post','class'=>'form-horizontal form-label-left validation','enctype'=>'multipart/form-data','id'=>'zone_form']) !!}

                                {{csrf_field()}}
                            <div id="map" style="height: 300px;"></div>
<br>                  
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input type="text" class="form-control" name="name" value="" placeholder="name">
                                      {!! $errors->first('name','<span class="help-block">:message</span>') !!}

                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control" name="description" rows="2"></textarea>
                                                {!! $errors->first('description','<span class="help-block">:message</span>') !!}

                                    </div>
                                </div>

                                <input type="hidden" name="point" value="">
                                <div class="item form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Status <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        {!!  Form::select('status', ['1'=>'Active','0'=>'Inactive'],null, array('class' => 'form-control col-md-7 col-xs-12')) !!}
                                         {!! $errors->first('status','<span class="help-block">:message</span>') !!}
                                       
                                    </div>
                                </div>




                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                       {{-- <button type="submit" class="btn btn-primary">Cancel</button>--}}
                                        {!!  Form::submit('Submit',array('class'=>'btn btn-success')) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&libraries=places,geometry,drawing&callback=initMap"></script>


   <script src="http://172.105.36.210/qikmeds/public/frontend/js/locationpicker.jquery.min.js"></script>



 
   
    <script type="text/javascript">
    window.shape=null;
            $('#map').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 1,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                    //radiusInput: $('#us3-radius'),
                    locationNameInput: $('#address_name')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {

           var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: currentLocation.latitude, lng: currentLocation.longitude},
                zoom: 10
            });

            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [/*'marker', 'circle','polyline', 'rectangle', */'polygon']
                },
                markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                circleOptions: {
                    fillColor: '#ffff00',
                    fillOpacity: 1,
                    strokeWeight: 5,
                    clickable: false,
                    editable: true,
                    zIndex: 1
                }
            });
            var zones = <?php echo json_encode($zones) ?>;
            for (var i in zones){
                var bermudaTriangle_all = new google.maps.Polygon({
                    paths: zones[i]['points'],
                    strokeColor: '#2aff20',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#2529ff',
                    fillOpacity: 0.35
                });
                bermudaTriangle_all.setMap(map);
            }
            drawingManager.setMap(map);

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {

                if(shape!=null){
                    shape.overlay.setMap(null);
                }
                if (event.type == 'polygon') {
                    shape=event;
                    var radius = event.overlay.getPath().getArray();

                    //console.log(JSON.stringify(radius));
                    $("[name=point]").val(JSON.stringify(radius));
                }
            });
                   //initMap();
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script>

@stop