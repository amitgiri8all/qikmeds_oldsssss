@extends('backend/layouts/default')
@section('title')
Driver Manager::CRM
@stop
@section('header_styles')
<style type="text/css">
    input.largerCheckbox {
            width: 20px;
            height: 20px;
        }
</style>
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
   <h1>
      Driver Manager
   </h1>
   <ol class="breadcrumb">
      <li>
         <a href="{{route('admin.dashboard')}}">
         <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
         Dashboard
         </a>
      </li>
      <li>Edit Driver</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h3 class="panel-title">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                     Edit Driver
               </h3>
               <div class="pull-right" style="margin-top: -25px;">
                  <a href="{{route('admin.driver.list')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
               </div>
            </div>
            <div class="panel-body">
               <form method="post" class="ajax_form" action="{{ route('admin.driver.edit.post',$data->id) }}" id="driver" enctype="multipart/form-data">
                  @csrf
 
                  <!-- Driver Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Driver Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->name)){!! $data->name !!}@else{!! old('name') !!}@endif"  name="name" placeholder="Driver name">
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

             <!-- Country Name -->
                 <!--   <div class="form-group col-sm-4">
                     <label for="text">Country</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->country)){!! $data->country !!}@else{!! old('country') !!}@endif"  name="country" placeholder="Country Name">
                        {!! $errors->first('country', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> -->

             <!-- City Name -->
                  <!--  <div class="form-group col-sm-4">
                     <label for="text">City</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"
                        value="@if(isset($data->city)){!! $data->city !!}@else{!! old('city') !!}@endif"
                          name="city" placeholder="City Name">
                        {!! $errors->first('city', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div> -->

                      <!-- Country Name -->
                   <div class="form-group col-sm-4">
                     <label for="text">Email</label>
                     <div class="input-group col-xs-12">
                        <input type="email" class="form-control" 
                        value="@if(isset($data->email)){!! $data->email !!}@else{!! old('email') !!}@endif"
                         name="email" placeholder="Email Name">
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                                     <!-- Password  -->
                 <div class="form-group col-sm-4">
                   <label for="text">Password</label>
                   <div class="input-group col-xs-12">
                      <input type="password" class="form-control" name="password" placeholder="Password Name">
                      {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                   </div>
                </div>
                   <!-- Phone number  -->
                   <div class="form-group col-sm-4">
                     <label for="text">Mobile Number</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"  value="@if(isset($data->mobile_number)){!! $data->mobile_number !!}@else{!! old('mobile_number') !!}@endif"  name="mobile_number" placeholder="Mobile number">
                        {!! $errors->first('mobile_number', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                   <!-- Address -->
                   <div class="form-group col-sm-4">
                     <label for="text">Address</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data->address)){!! $data->address !!}@else{!! old('address_name') !!}@endif" name="address_name" placeholder="Address Name">
                        {!! $errors->first('address_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                     <!-- Confirm Password  -->
       
           <div class="form-group col-sm-4" >
             <label for="text">Confirm Password</label>
             <div class="input-group col-xs-12">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation Name" >
                {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
             </div>
          </div>


          <!-- Aadhar Card Image -->
          <div class="form-group col-sm-4">
             <label for="text">Aadhar Card</label>
             <div class="input-group col-xs-12">
                <input type="file" class="form-control" name="aadhar_card" >
                {!! $errors->first('aadhar_card', '<span class="help-block">:message</span>') !!}
                </span>
             </div>
           @php
            if(!empty($data_driver->aadhar_card)){
            $url= $data_driver->aadhar_card;
            }else{
            $url= asset('/public/assets/images/no-image.jpg');
            }
           @endphp
              <img src="<?php echo $url; ?>" width="100" height="100">
          </div>

                 
                 <!-- Pan card Image -->
          <div class="form-group col-sm-4">
             <label for="text">Pan Card</label>
             <div class="input-group col-xs-12">
                <input type="file" class="form-control" name="pan_card" >
                {!! $errors->first('pan_card', '<span class="help-block">:message</span>') !!}
                </span>
             </div>
           @php
            if(!empty($data_driver->pan_card)){
            $url= $data_driver->pan_card;
            }else{
            $url= asset('/public/assets/images/no-image.jpg');
            }
           @endphp
              <img src="<?php echo $url; ?>" width="100" height="100">
          </div>

            <!-- Driving license certificate (i Image -->
                  <div class="form-group col-sm-4">
                     <label for="text">Driving license</label>
                     <div class="input-group col-xs-12">
                        <input type="file" class="form-control" name="driving_license"  >
                        {!! $errors->first('driving_license', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                     @php
                     if(!empty($data_driver->driving_license)){
                     $url = $data_driver->driving_license;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="100" height="100" >
                  </div>

                   <!--Plate Number -->
          <div class="form-group col-sm-4">
             <label for="text">Plate Number</label>
             <div class="input-group col-xs-12">
                <input type="file" class="form-control" name="plate_number" >
                {!! $errors->first('plate_number', '<span class="help-block">:message</span>') !!}
                </span>
             </div>
           @php
            if(!empty($data_driver->plate_number)){
            $url= $data_driver->plate_number;
            }else{
            $url= asset('/public/assets/images/no-image.jpg');
            }
           @endphp
              <img src="<?php echo $url; ?>" width="100" height="100">
          </div>
          
          <!-- Vehicle registration certificate (i Image -->
          <div class="form-group col-sm-4">
             <label for="text"> Registration certificate</label>
             <div class="input-group col-xs-12">
                <input type="file" class="form-control" name="vehicle_registration_certificate"  >
                {!! $errors->first('vehicle_registration_certificate', '<span class="help-block">:message</span>') !!}
                </span>
             </div>
             @php
             if(!empty($data_driver->vehicle_registration_certificate)){
             $url =$data_driver->vehicle_registration_certificate;
             }
             else{
             $url = asset('/public/assets/images/no-image.jpg');
           }
             @endphp
             <img src="<?php echo $url; ?>" width="100" height="100">
          </div>


                   <!-- Insurance -->
          <div class="form-group col-sm-4">
             <label for="text">Insurance</label>
             <div class="input-group col-xs-12">
                <input type="file" class="form-control" name="insurance" >
                {!! $errors->first('insurance', '<span class="help-block">:message</span>') !!}
                </span>
             </div>
           @php
            if(!empty($data_driver->insurance)){
            $url= $data_driver->insurance;
            }else{
            $url= asset('/public/assets/images/no-image.jpg');
            }
           @endphp
              <img src="<?php echo $url; ?>" width="100" height="100">
          </div>


            <!-- Account holder name-->
                   <div class="form-group col-sm-4">
                     <label for="text">Account Holder Name</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data_driver_bank->account_holder_name)){!! $data_driver_bank->account_holder_name !!}@else{!! old('account_holder_name') !!}@endif"  name="account_holder_name" placeholder="account_holder_name">
                        {!! $errors->first('account_holder_name', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>


                    <!-- Account holder name-->
                   <div class="form-group col-sm-4">
                     <label for="text">Account Number</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data_driver_bank->account_number)){!! $data_driver_bank->account_number !!}@else{!! old('account_number') !!}@endif"  name="account_number" placeholder="account_number">
                        {!! $errors->first('account_number', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

                    <!-- Ifsc code-->
                   <div class="form-group col-sm-4">
                     <label for="text">Ifsc Code</label>
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control" value="@if(isset($data_driver_bank->ifsc_code)){!! $data_driver_bank->ifsc_code !!}@else{!! old('ifsc_code') !!}@endif"  name="ifsc_code" placeholder="ifsc_code">
                        {!! $errors->first('ifsc_code', '<span class="help-block">:message</span>') !!}
                     </div>
                  </div>

    
                  <!-- Address Location -->
            {{--     <div class="form-group col-sm-12">
                     <label for="text">Address Location</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="address_name" placeholder="Address name" class="form-control" name="address">
                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  
                  <!-- Address Location -->
                  <div class="form-group col-sm-12">
                     <label for="text">Latitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="lat" readonly placeholder="Address name" class="form-control" name="lat">
                        {!! $errors->first('lat', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  

                  <!-- Address Location -->
                  <div class="form-group col-sm-12">
                     <label for="text">Longitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="long" readonly placeholder="Longitute name" class="form-control" name="lng">
                        {!! $errors->first('lng', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>


                  <!-- Address Location -->
                  <div class="form-group col-sm-12">
                      <div class="input-group col-xs-12">
                  <div id="us3" style="width: 100%; height: 300px;"></div>
                        </span>
                     </div>
                  </div> 
                --}}


                  <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('Update')
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- row-->
</section>
@stop
@section('footer_scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<script type="text/javascript">
            $('#us3').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                    //radiusInput: $('#us3-radius'),
                    locationNameInput: $('#address_name')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script>
@stop