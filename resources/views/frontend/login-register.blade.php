@extends('frontend.layouts.default')
{{-- Page title --}}
@section('title')
Home ::Quikmeds
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
@stop
{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
   <div class="container">
      <div class="login_blk">
         <div class="row align-items-center">
            <div class="col-sm-7">

               <div class="login_img"><img src="{{asset('public/frontend/images/pharmacy.png')}}" alt="pharmacy"></div>
            </div>
            <div class="col-sm-5">

         <?php
         $val = Session::get('register_form_show');
         $otp_form_show = Session::get('otp_form_show');
         $otptime = Session::get('otptime');
         $mobile_number = Session::get('mobile_number');
         $otp = Session::get('otp');
         $password = Session::get('password');
         ?>



               <div class="login_detail loginform">
                  <form method="POST" id="frm_login" class="ajax_form" action="{{ route('account.sendotp.post') }}" <?php if(!empty($val || $password || $otp)){?> style="display: none;" <?php } ?> >
                      <!-- login -->
                     @csrf
                     <div class="login_form">
                        <h2>Sign In / Sign Up</h2>
                        <p>Sign up or Sign in to access your orders, special offers, health tips and more!</p>
                         <div class="login">
                           <div class="form-group">
                              <label>Phone Number</label>
                              <span>+91</span>
                              <input type="tel" name="mobile_number" autocomplete="off" id="mobile_number" maxlength="10" minlength="10" class="form-control allow-numeric " placeholder="Enter your mobile no">
                              <input type="hidden" name="type" id="logtype" value="">
                              <?php if(!empty($mobile_number)){?> <a id="change" class="change" href="javascript::void(0)">Change</a><?php } ?>
                           </div>
                        </div>
                        <div class="form_btns otp" <?php if(!empty($mobile_number)){?> style="display: none;" <?php }else{?> style="display: block;" <?php } ?>>
                           <button type="submit" class="cmn_btn otpbtn loginb" data-val="otp" id="otpbutton">Use OTP</button>
                           <div class="or_txt"><span>OR</span></div>
                           <button class="bdr_btn loginpassword" data-val="password" id="passwordBtn">Use Password</button>
                        </div>

                     </div>
                    </form>
               </div>

<!-- Otp Login Form -->

    <div class="login_detail loginotp"  <?php if(!empty($otp)){?> style="display: block;" <?php }else{?> style="display: none;" <?php } ?>>
                  <form method="POST" id="frm_login" class="ajax_form" action="{{ route('account.login.post') }}">
                    @csrf
                    <input type="hidden" name="logintype" value="otp">
                        <h2>Sign In</h2>
                        <p>Sign up or Sign in to access your orders, special offers, health tips and more!</p>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <span>+91</span>
                            <input type="tel" maxlength="10" name="mobileno" minlength="10" class="form-control mobileno allow-numeric" value="<?php if(!empty($mobile_number)){echo $mobile_number;}?>" <?php if(!empty($mobile_number)){?>readonly='readonly'<?php } ?> readonly="">
                               <a id="change" class="change" href="javascript:void(0)">Change</a>
                        </div>
                        <div class="otp_boxes">
                            <small>We have sent 6 digit OTP</small>
                            <ul class="otp-event">
                                <li><input type="text" id="otp-number-input-1" maxlength="1" class="form-control otp-number-input ap-otp-input" name="otp1"></li>
                                <li><input type="text" id="otp-number-input-2" maxlength="1" class="form-control otp-number-input ap-otp-input" name="otp2"></li>
                                <li><input type="text" id="otp-number-input-3" maxlength="1" class="form-control otp-number-input ap-otp-input" name="otp3"></li>
                                <li><input type="text" id="otp-number-input-4" maxlength="1" class="form-control otp-number-input ap-otp-input" name="otp4"></li>
                                <li><input type="text" id="otp-number-input-5" maxlength="1" class="form-control otp-number-input ap-otp-input" name="otp5"></li>
                                <li><input type="text" id="otp-number-input-6" maxlength="1" class="form-control otp-number-input ap-otp-input" name="otp6"></li>
                            </ul>
                            <small>Waiting for OTP... 6 Sec</small>
                            <a href="javascript:void(0)">Resend</a>
                        </div>

                        <div class="form_btns">
                            <button class="cmn_btn">Verify</button>
                        </div>
                        </form>
              </div>


<!-- Login Wiith Password -->


<div class="login_detail loginpassword" <?php if(!empty($password)){?> style="display: block;" <?php }else{?> style="display: none;" <?php } ?>>
                  <form method="POST" id="frm_login" class="ajax_form" action="{{ route('account.login.post') }}">
                    @csrf
                    <input type="hidden" name="logintype" value="password">
                        <h2>Sign In</h2>
                        <p>Sign up or Sign in to access your orders, special offers, health tips and more!</p>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <span>+91</span>
                            <input type="tel" maxlength="10" name="mobileno" minlength="10" class="form-control mobileno allow-numeric" value="<?php if(!empty($mobile_number)){echo $mobile_number;}?>" <?php if(!empty($mobile_number)){?>readonly='readonly'<?php } ?> readonly="">
                               <a id="change" class="change" href="javascript:void(0)">Changes</a>
                        </div>
                         <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password">
                            <a href="javascript:void(0)"><i class="fa fa-eye toggle-password1"></i></a>
                        </div>
                        <div class="form_btns">
                            <button class="bdr_btn">Login Using Password</button>
                        </div>
                </form>
</div>

<!-- Register From ====================================== -->




                  <div class="login_detail register">
                     <form method="POST" id="frm_res" class="ajax_form" action="{{ route('account.register.post') }}" <?php if(!empty($val)){?> style="display: block;" <?php }else{?> style="display: none;" <?php } ?>>
                     <!-- login -->
                     @csrf
                        <div class="otp_boxes">
                            <span>Verifying Number</span>
                            <small>We have sent 6 digit OTP</small>
                            <ul>
                                <li><input type="text" class="form-control ap-otp-input" maxlength="1" name="otp1"></li>
                                <li><input type="text" class="form-control ap-otp-input" maxlength="1" name="otp2"></li>
                                <li><input type="text" class="form-control ap-otp-input" maxlength="1" name="otp3"></li>
                                <li><input type="text" class="form-control ap-otp-input" maxlength="1" name="otp4"></li>
                                <li><input type="text" class="form-control ap-otp-input" maxlength="1" name="otp5"></li>
                                <li><input type="text" class="form-control ap-otp-input" maxlength="1" name="otp6"></li>
                            </ul>
                            <small>Waiting for OTP... <b class="countdown"></b> Sec</small>
                            <a href="#">Resend</a>
                        </div>
                        <div class="form_btns">
                            <input type="submit" id="register_btn" class="cmn_btn" value="Register" style="width: 100%;">
                        </div>
                      </form>
                    </div>

                  <div class="login_social">
                     <span>Continue with</span>
                     <a href="#" class="fb"><i class="lab la-facebook-f"></i> Facebook</a>
                     <a href="{{ url('auth/google') }}" class="google"><i class="lab la-google"></i> Google</a>
                  </div>

            </div>
         </div>
      </div>
   </div>
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript">

const $inp = $(".ap-otp-input");
$inp.on({
  paste(ev) { // Handle Pasting
  
    const clip = ev.originalEvent.clipboardData.getData('text').trim();
    // Allow numbers only
    if (!/\d{6}/.test(clip)) return ev.preventDefault(); // Invalid. Exit here
    // Split string to Array or characters
    const s = [...clip];
    // Populate inputs. Focus last input.
    $inp.val(i => s[i]).eq(5).focus(); 
  },
  input(ev) { // Handle typing
    
    const i = $inp.index(this);
    if (this.value) $inp.eq(i + 1).focus();
  },
  keydown(ev) { // Handle Deleting
    
    const i = $inp.index(this);
    if (!this.value && ev.key === "Backspace" && i) $inp.eq(i - 1).focus();
  }
  
});











$(document).on('click', '#otpbutton', function() {
    //alert('ok')
   $("#logtype").val("otp");
    if($('#mobile_number').val()!=""){
    $('.mobileno').val($('#mobile_number').val());
         //$('.loginform').hide();
        //$('.loginotp').show();
      //  $('.loginpassword').hide();
    }
   // alert($("#logtype").val());
 });


$(document).on('click', '#passwordBtn', function() {
    //alert('okkk')
   $("#logtype").val("password");
if($('#mobile_number').val()!=""){
   // alert('ok filledlbe')
    $('.mobileno').val($('#mobile_number').val());
  //  $('.loginform').hide();
   // $('.loginotp').hide();
    //$('.loginpassword').show();

 }
   // alert($("#logtype").val());
});


/*$("body").on('click', '.otpbtn', function() {
    alert('ok')
    $('#type').append('otp');
});
$(".passbtn").on("click", "#passwordBtn", function () {
    $('#type').append('password');
});*/


  // $('#frm_res').css('display','none');
/*var timer2 = <?=$otptime ?>;
var interval = setInterval(function() {


  var timer = timer2.split(':');
  //by parsing integer, I avoid all extra string processing
  var minutes = parseInt(timer[0], 10);
  var seconds = parseInt(timer[1], 10);
  --seconds;
  minutes = (seconds < 0) ? --minutes : minutes;
  if (minutes < 0) clearInterval(interval);
  seconds = (seconds < 0) ? 59 : seconds;
  seconds = (seconds < 10) ? '0' + seconds : seconds;
  //minutes = (minutes < 10) ?  minutes : minutes;
  $('.countdown').html(minutes + ':' + seconds);
  timer2 = minutes + ':' + seconds;
}, 1000);
*/
/*var url= "{!! route('account.session-forgot.post') !!}";
 $token = '@csrf;'
$("#sessionclose").click(function (e) {
    alert('ok')
    $.ajax({
        type: 'POST',
        token: $token,
        url: url,
        success: function() {
            console.log('session destroye')
        }
    });
});*/

/*$("body").on('click', '.toggle-password', function() {
    alert('ok')
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $("#password");
  if (input.attr("type") === "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }

});*/



</script>

 <script type="text/javascript">
                var token = '{{ csrf_token() }}';

     var url= "{!! route('account.session-forgot.post') !!}";
     jQuery(function($) {

            $(".change").click(function(e) {
                e.preventDefault();
                //alert('ok')
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {'_token': token},
                    dataType : 'json',
                    success : function(json)
                    {
                       window.setTimeout(function(){window.location.href = json['redirect_url'];},100);
                       //$( "#frm_login" ).load(window.location.href + " #frm_login" );

                    },
                    error:  function(json)
                    {
                         alert("error")

                    }
                });

            });
        });

 </script>

<script type="text/javascript">
 $("body").on('click', '.toggle-password2', function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $("#password_confirmation");
  if (input.attr("type") === "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }

});

$("body").on('click', '.toggle-password1', function() {
   $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $("#password");
  if (input.attr("type") === "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }

});



</script>

<script type="text/javascript">
$(document).ready(function() {
    $('.allow-numeric').keyup(function() {
        var numbers = $(this).val();
        $(this).val(numbers.replace(/\D/, ''));
    });
});
</script>

@stop
