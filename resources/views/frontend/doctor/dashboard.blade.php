@extends('frontend/doctor/layouts/default')
<style>
    .user_txt span{width: 100%;display: inline-block;background: #f9f9f9;border:1px solid #f0f0f0;border-radius: 3px;/* height: 46px; */padding: 12px 15px;}

</style>
@section('content')
  <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">My Profile</h4>
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">My Profile</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
 
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="doctor_profile">
                                            <div class="doc-info-left">
                                                <div class="doctor-img">
                                                    <img src="{{Auth::guard('doctor')->user()->image}}" class="img-fluid" alt="User Image">
                                                </div>
                                                <div class="doc-info-cont">
                                                    <h4 class="doc-name">Dr. {{Auth::guard('doctor')->user()->name}}</h4>
                                                <?php $myArray = array(); 
                                                 foreach($doctor_education_info as $key => $value){
                                                    $myArray[] = '<span>'.$value->specializations_name.'</span>';

                                                 }
                                                 
                                                 
                                                 echo implode(',',$myArray) ?>
                                                   
                                                    <p>Reg. No - {{Auth::guard('doctor')->user()->registration_number}}</p>
                                                    <p><i class="ri-phone-line"></i> +91 {{Auth::guard('doctor')->user()->mobile_number}}</p>
                                                    <p><i class="ri-mail-line"></i> 
                                                        <a href="javascript:void(0)">{{Auth::guard('doctor')->user()->email}}</a></p>
                                                </div>
                                            </div>
                                            <div class="doctor_info">
                                                <h2>About Me</h2>
                                                <p>{{Auth::guard('doctor')->user()->about_info;}}</p>
                                            </div>
                                            <div class="doctor_info">
                                                <h2>Education</h2>
                                                <ul class="experience-list">
             @php $eduinfo = App\Helpers\Helper::get_doctor_education_info(Auth::guard('doctor')->user()->id); @endphp
                @foreach($eduinfo as $key=>$val)
                    <li>
                        <div class="experience-user">
                            <div class="before-circle"></div>
                        </div>
                        <div class="experience-content">
                            <div class="timeline-content">
                                <span>{{$val->college_name}}</span>
                                <div>{{$val->subject}}</div>
                                <small>{{$val->start_year}} - {{$val->end_year}}</small>
                            </div>
                        </div>
                    </li>
                @endforeach
                                                    
                                                </ul>
                                            </div>
                                            <div class="doctor_info">
                                                <h2>Work &amp; Experience</h2>
                                                <ul class="experience-list">
                         @php $workinfo = App\Helpers\Helper::get_doctor_work_info(Auth::guard('doctor')->user()->id); @endphp
                                                    @foreach($workinfo as $key=>$val)
                                                    <li>
                                                        <div class="experience-user">
                                                            <div class="before-circle"></div>
                                                            </div>
                                                            <div class="experience-content">
                                                                <div class="timeline-content">
                                                                     <span>{{$val->woking_place_name}}</span>
                                                            <small>{{$val->start_year}} - {{$val->end_year}}</small>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="doctor_info clearfix">
                                                <h2>Specializations</h2>
                                                <ul class="service-list">
                     @php $specializationsInfo = App\Helpers\Helper::get_specializations_info(Auth::guard('doctor')->user()->id); @endphp   
                          @foreach($specializationsInfo as $key=>$val)
                                <li><i class="ri-check-double-line"></i> {{$val->specializations_name}}</li>
                         @endforeach
                                                </ul>
                                            </div>
       <hr>

        <div class="col-lg-6">
         <div class="panel panel-primary">
            <div class="panel-heading">
               <h4 class="panel-title" style="font-size: 1.22rem;">
                  <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff"
                     data-hc="white"></i>
                    Bank Details
               </h4>
           
            </div>
            <div class="panel-body">
            
            <section id="group-selection">
  <div class="row">
    <div class="col-12">
      <div class="card">
    
        <div class="card-content collapse show">
          <div class="card-body card-dashboard">
            @if (\Session::get('success'))
            <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
              <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Success!</strong> {{ \Session::get('success') }}.
            </div>            
            @endif
            <table class="table table-bordered table-striped">
                <tbody>
                 
                    <tr>
                        <th class="timeline-content"  style="font-size: 15px;">
                            Name
                        </th>
                        <td style="font-size: 14px;">
                            {{ $bank_details->account_holder_name  ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th class="timeline-content" style="font-size: 15px;">
                            Account Number
                        </th>
                        <td style="font-size: 14px;">
                            {{ $bank_details->account_number ?? '' }}
                        </td>
                    </tr>
                     <tr>
                        <th class="timeline-content" style="font-size: 15px;">
                            Ifsc code
                        </th>
                        <td style="font-size: 14px;">
                            {{ $bank_details->ifsc_code ?? ''}}
                        </td>
                    </tr>
                    </tbody>
                  </table>
                 </div>
                <div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>
             <div class="vendor_profile" style="padding-top: 15px;">

         <form method="post"   action="{{route('doctor.document')}}"  id="document"  enctype="multipart/form-data">
                  @csrf
        <h2>Documents</h2>
        <div class="row">
            <div class="col-sm-6">
                @php
                     if(!empty($doctor_documents->doctor_license)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$doctor_documents->doctor_license);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                @endphp
                <label>Doctor License</label>
                 <input type="file" class="form-control btn btn-secondary" value="{{old('doctor_license')}}" name="doctor_license">
                 {!! $errors->first('doctor_license', '<span class="help-block">:message</span>') !!}
                {{-- <div class="doc_view"> <a href="{{$url}}" target="_blank" class="btn btn-primary">View</a>
                </div> --}}
                 <img src="<?php echo $url; ?>" width="220" height="100" >
                
            </div>
       
            <div class="col-sm-6">
                <label>Educational Certificate</label>
                @php
                     if(!empty($doctor_documents->educational_certificate)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$doctor_documents->educational_certificate);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                @endphp
                 <input type="file" class="form-control btn btn-secondary" value="{{old('educational_certificate')}}" name="educational_certificate">
              {{--   <div class="doc_view"> <a href="{{$url}}" class="btn btn-primary">View</a>
                    <img src="<?php echo $url; ?>" width="100" height="100" >
                </div> --}}
                <img src="<?php echo $url; ?>" width="220" height="100" >
                
            </div>
        </div>
           <div class="col-md-12 mar-10">
                     <div class="col-md-4" style="margin-top: 10px;">
                        <button type="submit" class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
    </div>



            </div>
         </div>
      </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                 Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

@endsection
