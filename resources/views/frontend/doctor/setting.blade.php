@extends('frontend/doctor/layouts/default')
<style type="text/css">
    .cmn_btn{ background: #24aeb1; border:2px solid #24aeb1; border-radius: 3px; color: #fff; display: inline-block; font-weight: 500; height: 46px; line-height: 22px; letter-spacing: 0.5px; padding: 11px 20px; text-transform: uppercase;}


.right_sidebar { background: #fff; border-radius: 5px; box-shadow: 0 1px 2px rgba(0,0,0,.06); position: relative;}
.right_sidebar h2{ border-bottom: 1px solid #eee; font-size: 20px; padding: 18px 20px;}
.cmn_area{width: 100%;display: inline-block;padding: 25px 20px 30px 20px;}
.user_txt{ margin-bottom: 25px; }
.user_txt label{ color:#24aeb1;  display: block; font-size: 14px; margin-bottom: 8px;}
.user_txt span{ width: 100%; display: inline-block; background: #f9f9f9; border:1px solid #f0f0f0; border-radius: 3px; height: 46px; padding: 12px 15px;}
.cmn_btn{ background: #24aeb1; border:2px solid #24aeb1; border-radius: 3px; color: #fff; display: inline-block; font-weight: 500; height: 46px; line-height: 22px; letter-spacing: 0.5px; padding: 11px 20px; text-transform: uppercase;}
.cmn_btn:hover{ background: #333; border-color: #333; color: #fff;}
.cmn_btn i{ margin-right: 5px; position: relative; top: 1px;}
.cncl_btn{ background: none; border:0px; border-radius: 3px; color: #888; display: inline-block; font-weight: 500; height: 46px; margin-left: 25px; line-height: 22px; letter-spacing: 0.5px; padding: 11px 0px; text-transform: uppercase;}
.cncl_btn:hover{ color: #333;}

.radio_btn{display: inline-block;margin-right: 40px;}
.right_sidebar a.add_new{ font-size: 14px; font-weight: 500; color: #555; position:absolute; right:20px; top:19px;}
.right_sidebar a.add_new:hover{ color: #24aeb1;}

</style>
<link href="http://172.105.36.210/qikmeds/public/frontend/css/toastr.css" rel="stylesheet" type="text/css"/>

@section('content')
  <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">My Profile</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">My Profile</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
 
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="doctor_profile">
                                             


   <div class="col-sm-9">
                    <div class="right_sidebar">
                        <h2>Personal Information</h2>
                        <div class="user_detail_blk cmn_area">
                         <form method="post" id="pa" action="{{route('doctor.update_profile_doctor')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Full Name</label>
                                        <span><input type="text" name="name" class="form-control" value="{{Auth::guard('doctor')->user()->name}}" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Email Address</label>
                                        <span><input type="text" name="email" class="form-control" value="{{Auth::guard('doctor')->user()->email}}" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Mobile Number</label>
                                          <span><input type="text" readonly name="mobile_number" class="form-control" value="{{Auth::guard('doctor')->user()->mobile_number}}" ></span>                                    
                                        </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Image</label>
                                            <input type="file"  name="image" id="file"  onchange="loadFile(event)" class="form-control">  
                                        </div>
                                </div>  
                              // For view image of Doctor
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                         <p><img src="{{Auth::guard('doctor')->user()->image}}" id="output" width="200" /></p>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="user_txt">
                                        <label>Gender</label>
                                        <div class="radio_btn">
                                            <?php $gender = Auth::guard('doctor')->user()->gender ?>
                                            <input type="radio" name="gender" value="male" id="r-1" <?php if($gender == "male") { echo 'checked'; } ?>>
                                            <label for="r-1">Male</label>
                                        </div>
                                        <div class="radio_btn">
                                            <input type="radio" name="gender" value="female" id="r-2"<?php if($gender == "female") { echo 'checked'; } ?>>
                                            <label for="r-2">Female</label>
                                        </div>
                                        <div class="radio_btn">
                                            <input type="radio" name="gender" value="other" id="r-3"<?php if($gender == "other") { echo 'checked'; } ?>>
                                            <label for="r-3">Others</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="cmn_btn"><i class="fa fa-edit"></i> Edit</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div class="right_sidebar">
                        <h2>Change Password</h2>
                        <div class="user_detail_blk cmn_area">
                         <form method="post" id="change-passwords" action="{{route('doctor.change-password-doctor')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                            <div class="row">
                              @if(!empty(Auth::guard('doctor')->user()->password))
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Old password</label>
                                        <span><input type="password" name="old_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="1">
                              @else
                                <input type="hidden" name="type" value="0">
                                
                              @endif  
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>New password</label>
                                        <span><input type="password" name="new_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Confirm Password</label>
                                        <span><input type="password" name="confirm_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <br /><br />

                              
                                    
                                <div class="col-sm-12" style="margin-top: 10px;">
                                    <button type="submit" class="cmn_btn"> <i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>

                     <div class="right_sidebar">
                        <h2>Bank Details</h2>
                        <div class="user_detail_blk cmn_area">
                         <form method="post" id="bank_detail" action="{{route('doctor.bank-details')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                            <div class="row">
             

                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Account Holder Name</label>
                                        <span><input type="text" name="account_holder_name" class="form-control" placeholder="Account holder name"></span>
                                        {!! $errors->first('account_holder_name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                   <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Account Number</label>
                                        <span><input type="text" name="account_number" class="form-control" placeholder="Account Number"></span>
                                        {!! $errors->first('account_number', '<span class="help-block">:message</span>') !!}
                                    </div>
                                     </div>


                                    <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Ifsc Code</label>
                                        <span><input type="text" name="ifsc_code" class="form-control" placeholder="Ifsc code"></span>
                                        {!! $errors->first('ifsc_code', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Confirm Account Number </label>
                                        <span><input type="text" name="account_number_confirmation" class="form-control" placeholder="Account Number Confirmation"></span>
                                        {!! $errors->first('account_number_confirmation', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                               
                             
                               
                                

                              
                                    
                                <div class="col-sm-12" style="margin-top: 10px;">
                                    <button type="submit" class="cmn_btn"> <i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>



                                       </div>
                                </div>
                            </div>
                        </div>
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                 Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

@endsection

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script src="http://172.105.36.210/qikmeds/public/frontend/js/jquery.form.js"></script>
<script src="http://172.105.36.210/qikmeds/public/frontend/js/formClass.js"></script>
<script src="http://172.105.36.210/qikmeds/public/frontend/js/toastr.min.js"></script

<script>
var select_address ="{{route('user.select-address')}}";
var token = '{{ csrf_token() }}'; 


var loadFile = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};
</script>

@stop