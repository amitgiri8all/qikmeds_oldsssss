@extends('frontend/doctor/layouts/default')
@section('content')
@section('header_styles')
<style type="text/css">
   .card-body .nav-justified .nav-link{background: #cac6c67a;font-size: 16px;font-weight: 400;padding:15px 0px;}

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop  
<div class="main-content">
   <div class="page-content">
      <div class="container-fluid">
         <!-- start page title -->
         <div class="row">
            <div class="col-12">
               <div class="page-title-box d-flex align-items-center justify-content-between">
                  <h4 class="mb-0">Orders</h4>
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                        <li class="breadcrumb-item active">Orders</li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <!-- end page title -->
         <div class="row">
            <div class="col-xl-12">
               <div class="card">
                  <div class="card-body">
                     <!-- Nav tabs -->
                     <div class="row">
                        <div class="col-sm-6">
                           <ul class="nav nav-pills nav-justified" role="tablist">
                              <li class="nav-item waves-effect waves-light">
                                 <a class="nav-link active" data-toggle="tab" href="#nearby" role="tab">
                                 <span class="d-none d-sm-block">New Orders </span> 
                                 </a>
                              </li>
                              <li class="nav-item waves-effect waves-light">
                                 <a class="nav-link" data-toggle="tab" href="#progress" role="tab">
                                 <span class="d-none d-sm-block">In Progress Consultation </span> 
                                 </a>
                              </li>
                              <li class="nav-item waves-effect waves-light">
                                 <a class="nav-link" data-toggle="tab" href="#completed" role="tab">
                                 <span class="d-none d-sm-block">Completed Consultation </span> 
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <!-- Tab panes -->
                     <div class="tab-content">
                        <div class="tab-pane active" id="nearby" role="tabpanel">
                           <div class="new_order">
                              <div class="row">
                <?php //echo "<pre>"; print_r($doctor_orders);die; ?>                 
                @forelse($doctor_orders as $val)
                                 @php $doctor_id = Auth::guard('doctor')->user()->id;
                                 $data =  App\Models\DeclineProducts::remove_order_by_doctor($doctor_id,$val->id);
                                 @endphp  
                                 @if($data)
                                 <div class="col-sm-6">
                                    <div class="order_details">
                                       <div class="order_info">
                                          <div class="left_txt">Order ID - {{$val->order_code;}} <span></span></div>
                                          <div class="right_txt">Order Date - <span>{{date('d-m-Y', strtotime($val->created_at));}} - {{date('h:i A', strtotime($val->created_at));}}</span></div>
                                       </div>
                                       <h3>{{$val->user->name;}} <em>({{$val->user->mobile_number;}})</em></h3>
                                       <span class="price_tag">₹ {{number_format($val->total_payed_amount,2)}} </span>
                                       <p>{{$val->deliveryaddress->address_delivery}} {{$val->deliveryaddress->mobile_number}}, {{$val->deliveryaddress->city}}, {{$val->deliveryaddress->state}} - {{$val->deliveryaddress->pin_code}}</p>
                                       <ul>
                                          @php $count = 0;  @endphp  
                                          @foreach($val->OrderItem as $key=>$value)
                                          @php 
                                          $count++; 
                                          @endphp
                                          @if($key< 200000)    
                                          <li><i><img src="{{asset('public/vendor/assets/images/red-cross.png')}}"></i> {{$value->ProductOrderItem->medicine_name??''}}</li>
                                          @endif                               
                                          @endforeach
                                          see more  <span class="green">(Available : {{$count}})</span> <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg{{$val->id}}"><i class="ri-todo-line"></i>See Prescription</a> 
            <!-- ============= Modal  ================ -->
               <div class="modal fade bs-example-modal-lg{{$val->id}} prescription" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0" id="myModalLabel1{{$val->id}}">Prescription</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  @if(!empty($val->UserPrescription->image))
                                    <div class="prescription_img"><img src="{{$val->UserPrescription->image}}" alt="prescription"></div>
                                  @else
                                    <div class="prescription_img"><img src="http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg" alt="prescription"></div>
                                  @endif  
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>

                                       </ul>
                                       <div class="order_btm">
                                          <p>Payment - <span>{{ucfirst($val->orderpayment->payment_method)}}</span></p>
                                          <div class="action_btns">
                                             <button  type="button" id="decline{{$val->id}}" data-id="{{$val->id}}" class="decline btn btn-danger waves-effect waves-light">Remove</button>

                                             <a href="javascript::void(0)" data-id="{{$val->id}}"  id="accepted{{$val->id}}" class="accepted btn cmn_btn btn-success waves-effect waves-light loaded_btn">Accept</a>

                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                @endif
                                 
                                 @empty
                                   <h4>No Order Found Yet</h4>
                                 @endforelse
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane" id="progress" role="tabpanel">
                           <div class="new_order">
                             <div class="row">
                                 @forelse($accepted_doctor_orders as $val)
                                 @php $doctor_id = Auth::guard('doctor')->user()->id;
                                 $data =  App\Models\DeclineProducts::remove_order_by_doctor($doctor_id,$val->order_id);
                                 @endphp  
                                 @if($data)
                                 <div class="col-sm-6">
                                    <div class="order_details">
                                       <div class="order_info">
                                          <div class="left_txt">Order ID - {{$val->order_code;}} <span></span></div>
                                          <div class="right_txt">Order Date - <span>{{date('d-m-Y', strtotime($val->created_at));}} - {{date('h:i A', strtotime($val->created_at));}}</span></div>
                                       </div>
                                       <h3>{{$val->user->name??'';}} <em>({{$val->user->mobile_number??'';}})</em></h3>
                                       <span class="price_tag">₹ {{number_format($val->total_payed_amount,2)}} </span>
                                       <p>{{isset($val->deliveryaddress->address_delivery)?$val->deliveryaddress->address_delivery:'';}} {{isset($val->deliveryaddress->mobile_number)?$val->deliveryaddress->mobile_number:'';}}, {{isset($val->deliveryaddress->city)?$val->deliveryaddress->city:'';}}, {{isset($val->deliveryaddress->state)?$val->deliveryaddress->state:'';}} - {{isset($val->deliveryaddress->pin_code)?$val->deliveryaddress->pin_code:'';}}</p>
                                       <ul>
                                          @php $count = 0;  @endphp  
                                          @foreach($val->OrderItem as $key=>$value)
                                          @php 
                                          $count++; 
                                          @endphp
                                          @if($key< 200000)    
                                          <li><i><img src="{{asset('public/vendor/assets/images/red-cross.png')}}"></i> {{$value->ProductOrderItem->medicine_name??''}}</li>
                                          @endif                               
                                          @endforeach
                                          see more  <span class="green">(Available : {{$count}})</span> <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg{{$val->id}}"><i class="ri-todo-line"></i>See Prescription</a> 
            <!-- ============= Modal  ================ -->
               <div class="modal fade bs-example-modal-lg{{$val->id}} prescription" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0" id="myModalLabel2{{$val->id}}">Prescription</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  @if(!empty($val->UserPrescription->image))
                                    <div class="prescription_img"><img src="{{$val->UserPrescription->image}}" alt="prescription"></div>
                                  @else
                                    <div class="prescription_img"><img src="http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg" alt="prescription"></div>
                                  @endif  
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div> 
                                       </ul>
                                       <div class="order_btm">
                                          <p>Payment - <span>{{ucfirst($val->orderpayment->payment_method)}}</span></p>
                                          <div class="action_btns">
                                               <?php $ids = encrypt($val->id);?> 
                                             <a href="{{url('doctor/order-prescription',$ids)}}"  class="btn cmn_btn btn-success waves-effect waves-light loaded_btn">View</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endif
                                 @empty
                                 <h4>No Order Found Yet</h4>
                                 @endforelse
                              </div>
                           </div>
                        </div>
                           
                           <div class="tab-pane" id="completed" role="tabpanel">
                           <div class="new_order">
                            <div class="row">
                                 @forelse($completed_consultation_order as $val)
                                 @php $doctor_id = Auth::guard('doctor')->user()->id;
                                 $data =  App\Models\DeclineProducts::remove_order_by_doctor($doctor_id,$val->order_id);
                                 @endphp  
                                 @if($data)
                                 <div class="col-sm-6">
                                    <div class="order_details">
                                       <div class="order_info">
                                          <div class="left_txt">Order ID - {{$val->order_code;}} <span></span></div>
                                          <div class="right_txt">Order Date - <span>{{date('d-m-Y', strtotime($val->created_at));}} - {{date('h:i A', strtotime($val->created_at));}}</span></div>
                                       </div>
                                       <h3>{{$val->user->name;}} <em>({{$val->user->mobile_number;}})</em></h3>
                                       <span class="price_tag">₹ {{number_format($val->total_payed_amount,2)}} </span>
                                       <p>{{isset($val->deliveryaddress->address_delivery)?$val->deliveryaddress->address_delivery:'';}} {{isset($val->deliveryaddress->mobile_number)?$val->deliveryaddress->mobile_number:'';}}, {{isset($val->deliveryaddress->city)?$val->deliveryaddress->city:'';}}, {{isset($val->deliveryaddress->state)?$val->deliveryaddress->state:'';}} - {{isset($val->deliveryaddress->pin_code)?$val->deliveryaddress->pin_code:'';}}</p>
                                       <ul>
                                          @php $count = 0;  @endphp  
                                          @foreach($val->OrderItem as $key=>$value)
                                          @php 
                                          $count++; 
                                          @endphp
                                          @if($key< 200000)    
                                          <li><i><img src="{{asset('public/vendor/assets/images/red-cross.png')}}"></i> {{$value->ProductOrderItem->medicine_name??''}}</li>
                                          @endif                               
                                          @endforeach
                                          see more  <span class="green">(Available : {{$count}})</span> <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg3{{$val->id}}"><i class="ri-todo-line"></i>See Prescription</a> 
            <!-- ============= Modal  ================ -->
               <div class="modal fade bs-example-modal-lg3{{$val->id}} prescription" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0" id="myModalLabel3{{$val->id}}">Prescription</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  @if(!empty($val->UserPrescription->image))
                                    <div class="prescription_img"><img src="{{$val->UserPrescription->image}}" alt="prescription"></div>
                                  @else
                                    <div class="prescription_img"><img src="http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg" alt="prescription"></div>
                                  @endif  
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div> 
                                       </ul>
                                       <div class="order_btm">
                                          <p>Payment - <span>{{ucfirst($val->orderpayment->payment_method)}}</span></p>
                                          <div class="action_btns">
                                               <?php $ids = encrypt($val->id);?> 
                                            <!--  <a href="{{url('doctor/order-prescription',$ids)}}"  class="btn cmn_btn btn-success waves-effect waves-light loaded_btn">View</a> -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endif
                                 @empty
                                 <h4>No Order Found Yet</h4>
                                 @endforelse
                              </div>
                           </div>
                           </div>  
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- container-fluid -->
   </div>


   <!-- End Page-content -->
   <footer class="footer">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               Copyright © 2021 Qikmeds. All Rights Reserved.
            </div>
         </div>
      </div>
   </footer>
</div>
@endsection
{{-- page level scripts --}}
@section('footer_scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script type="text/javascript">
   var accepted_url ="{{URL::to('doctor/accepte-orders')}}";
   var decline_url ="{{URL::to('doctor/decline-orders')}}";
   var token       = '{{ csrf_token() }}';
</script>
<script type="text/javascript">
//Accepted All Orders
$(document).ready(function(){
    $('.accepted').on('click',function(event){
         order_id = $(this).attr("data-id");
         ord_item_id = '';
         event.preventDefault();
          $.ajax({
            url:  accepted_url,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
            dataType: 'json',
            beforeSend : function() {
                  $("#accepted"+order_id).html('Please wait &nbsp;<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            success: function(response) {
             //alert("test");
             setTimeout(function(){
             $("#accepted"+order_id).html('Accepted');
               window.location.href = response.url;
             $("#nearby").load(document.URL + " #nearby");
               toastr[response.status](response.message, "Order");
            },1000);

            }
            });
              
        
    });
});



   //Decline_orders Products All Orders
   $(document).ready(function(){
   $('.decline').on('click',function(){
          order_id = $(this).attr("data-id");
         // alert(order_id)
           var ord_item_id=[];
             event.preventDefault();
         $.ajax({
           url:  decline_url,
           type: "POST",
           data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
           dataType: 'json',
           beforeSend : function() {
                  $("#decline"+order_id).html('Please wait &nbsp;<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
           success: function(response) {
                setTimeout(function(){
               $("#decline"+order_id).html('Remove');

                  location.reload(); 
                 //$("#nearby").load(document.URL + " #nearby");
                  toastr[response.status](response.message, "Order");

                  //window.load();
               },3000);
   
           }
           });
             
       
   });
   });
   
</script>
@stop