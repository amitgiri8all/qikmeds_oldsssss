<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>@yield('Qikmeds Doctor')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
<meta content="Themesdesign" name="author" />
<!-- App favicon -->
<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/images/favicon.ico')}}"/>

<!-- DataTables -->
 <!-- DataTables -->
<link href="{{asset('public/doctor/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

<!-- Responsive datatable examples -->
<!-- <link href="{{asset('public/doctor/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />  
 --><!-- Bootstrap Css -->
<link href="{{asset('public/doctor/assets/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="{{asset('public/doctor/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="{{asset('public/doctor/assets/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />
<style type="text/css">

#overlay{
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% {
    transform: rotate(360deg);
  }
}
.is-hide{
  display:none;
}
</style>
 @yield('header_styles')
</head>
     <body>
        <div id="overlay">
  <div class="cv-spinner">
    <span class="spinner"></span>
  </div>
</div>
   <!-- Begin page -->
        <div id="layout-wrapper">
         @php $settings = App\Models\Setting::first(); 
         @endphp
            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box">
                            <a href="{{route('dashboard')}}" class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="{{asset('public/doctor/assets/images/logo-sm-dark.png')}}" alt="">
                                </span>
                                <span class="logo-lg">
                                    <!-- <img src="{{asset('public/doctor/assets/images/logo-dark.png')}}" alt=""> -->
                                    <img src="{{ url('storage/app/public/upload/image/'.$settings->app_logo) }}">
                                </span>
                            </a>

                           <!--  <a href="index.html" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="{{asset('public/doctor/assets/images/logo-sm-light.png')}}" alt="">
                                </span>
                                <span class="logo-lg">
                                    <img src="{{asset('public/doctor/assets/images/logo-light.png')}}" alt="">
                                </span>
                            </a> -->
                        </div>
                        

                        <!-- App Search-->
                        <form class="app-search d-none d-lg-block ml-4">
                            <div class="position-relative" style="display: none;">
                                <input type="text" class="form-control" placeholder="Search...dddddd">
                                <span class="ri-search-line"></span>
                            </div>
                        </form>
                    </div>
                    <div class="d-flex">
                        <div class="dropdown d-inline-block">
                            @php if(isset(Auth::guard('doctor')->user()->id))
                             $doctors = App\Helpers\Helper::UnreadNotificationsDoctor(Auth::guard('doctor')->user()->id);
                             @endphp
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ri-notification-3-line"></i><sup> @if(isset($doctors))
                                    {{count($doctors)}}
                                 @endif
                                </sup>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                                aria-labelledby="page-header-notifications-dropdown">
                                <div class="p-3">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="m-0"> Notifications </h6>
                                        </div>
                                        <div class="col-auto">
                                            <a href="{{route('notification')}}" class="small"> View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div data-simplebar style="max-height: 230px;">
                                
                                 @forelse ($doctors->slice(0, 5) as $notification)

                                    <a href="{{url('doctor/orders')}}" class="text-reset notification-item">
                                        <div class="media">
                                            <div class="avatar-xs mr-3">
                                                <span class="avatar-title bg-primary rounded-circle font-size-16">
                                                    <i class="ri-shopping-cart-line"></i>
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1"><!-- {{ $notification->data['name']; }} -->
                                                @php
                                                if(isset($notification->data)){
                                                $data_value=   json_decode($notification->data) ;
                                                
                                                }
                                               
                                                @endphp
                                                {{$data_value->message ?? ' ' }}
                                                </h6>
                                                <div class="font-size-12 text-muted">
<!--                                                     <p class="mb-1">If several languages coalesce the grammar</p>
 -->                                                    <p class="mb-0"><i class="mdi mdi-clock-outline">   </i>  {{ $notification->created_at->diffForHumans() }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @empty
                                    <li class="dropdown-title">There is no unread notifications</li>
                                @endforelse


                                </div>
                                <div class="p-2 border-top">
                                    <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="{{route('notification')}}">
                                        <i class="mdi mdi-arrow-right-circle mr-1"></i> View More..
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown d-inline-block user-dropdown">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user" src="{{Auth::guard('doctor')->user()->image}}"
                                    alt="Dr. {{Auth::guard('doctor')->user()->name}}">
                                <span class="d-none d-xl-inline-block ml-1">Dr. {{Auth::guard('doctor')->user()->name}}</span>
                                <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a class="dropdown-item" href="{{url('doctor/my-dashboard')}}"><i class="ri-user-line align-middle mr-1"></i> Profile</a>
<!--                                 <a class="dropdown-item" href="#"><i class="ri-wallet-2-line align-middle mr-1"></i> My Wallet</a>
 -->                                <a class="dropdown-item d-block" href="{{url('doctor/setting')}}"><i class="ri-settings-2-line align-middle mr-1"></i> Settings</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="{{route('logoutdoctor.doctor')}}"><i class="ri-shut-down-line align-middle mr-1 text-danger"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
 

            <!-- ========== Left Sidebar Start ========== -->
            <div class="vertical-menu">

                <div data-simplebar class="h-100">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                    <div class="vendor_info">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center">
                                    <img src="{{Auth::guard('doctor')->user()->image}}" alt="" class="rounded-circle avatar-md mt-2 mb-4">
                                    <div class="media-body">
                                        <h5 class="text-truncate"><a href="#" class="text-dark">Dr. {{Auth::guard('doctor')->user()->name}}</a></h5>
                                        <p class="text-muted">
                                            @php $specializationsInfo = App\Helpers\Helper::get_specializations_info_doctor(Auth::guard('doctor')->user()->id); @endphp 
                                @foreach($specializationsInfo as $key => $value ) 
                                <ul>            
                                  <li>{{$value->specializations_name??''}}</li>
                                </ul>
                                @endforeach

                                        </p>
                                    </div>
                                </div>

                                <hr class="my-3">

                                <div class="row text-center">
                                    <div class="col-6">
                                        <p class="text-muted mb-2">Products</p>
                                        <h5> @php echo $accepted_products = App\Helpers\Helper::get_doctor_product_accepted();@endphp</h5>
                                        
                                    </div>
                                    <div class="col-6">
                                        <p class="text-muted mb-2">Available</p>
                                        <!-- <input type="checkbox" id="switch1" switch="none" checked="">
                                        <label for="switch1" data-on-label="On" data-off-label="Off"></label> -->
                                        @php  $accepted_products = App\Helpers\Helper::get_doctor_available();@endphp

                                        <input type="checkbox"  switch="none" id="switch1" value="{{$accepted_products->is_online}}"  
                                        @if(($accepted_products->is_online) == "Yes") checked @endif>
                                         
                                        <label for="switch1" data-on-label="On" data-off-label="Off"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <!-- Left Menu Start -->
                        <ul class="metismenu list-unstyled" id="side-menu">
                              
                            <li>
                                <a href="{{url('doctor/my-dashboard')}}" class="waves-effect">
                                    <i class="ri-account-circle-line"></i>
                                    <span>My Profile</span>
                                </a>
                            </li>
                            
                             <li>
                                <a href="{{url('doctor/orders')}}" class="waves-effect">
                                    <i class="ri-shopping-cart-line"></i>
                                    <span>Orders</span>
                                </a>
                                
                            </li>

                              <li>
                                <a href="{{url('doctor/notification')}}" class="waves-effect">
                                    <i class="ri-shopping-cart-line"></i>
                                    <span>Notification</span>
                                </a>
                                
                            </li>  
                        
                            
                            
                        </ul>
                    </div>
                    <!-- Sidebar -->
                </div>
            </div>
            <!-- Left Sidebar End -->

            @yield('content')

        </div>

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                 Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        <!-- END layout-wrapper -->

      <!-- JAVASCRIPT -->
        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

        <script src="{{asset('public/doctor/assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

        <script src="{{asset('public/doctor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
        <script src="{{asset('public/doctor/assets/libs/simplebar/simplebar.min.js')}}"></script>
 
        <!-- apexcharts -->
<!--         <script src="{{asset('public/doctor/assets/js/pages/dashboard.init.js')}}"></script>
 --> 
<!--         <script src="{{asset('public/doctor/assets/js/app.js')}}"></script>
 --> @yield('footer_scripts')
    </body>
</html>
<script type="text/javascript">
   var token = '{{ csrf_token() }}';
   var url = "{!! route('doctor-availablity') !!}";

    $('#switch1').on('change', function() {
    //alert( this.value );
    var data = this.value;
    //alert(data);

       $.ajax({
                    url: url,
                    type: 'post',
                    data: {'_token': token,data:data},
                    dataType : 'json',
            success : function(status)
                    {
                    // alert("sucess")
                     toastr.success(status.message);
                    },
                    error:  function(status)
                    {

                         //alert("error")
                       
                    }
        });
});
</script>
 <script type="text/javascript">
    $(".small").click(function(){
    var token_view = '{{ csrf_token() }}';
    var id = '{{ Auth::guard('doctor')->user()->id  ?? '' }}' ; 
    //alert(id);
    $.ajax(
    {
        url: "view_all/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token_view,
        },
        success: function (){
           // toastr['success']("Delete Successfully", "Delete");
           window.location.reload();

        }
    });
  }); 
</script>