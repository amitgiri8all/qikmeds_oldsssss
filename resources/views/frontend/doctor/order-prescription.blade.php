@extends('frontend/doctor/layouts/default')
@section('content')
@section('header_styles')
<style type="text/css">
   .card-body .nav-justified .nav-link{background: #cac6c67a;font-size: 16px;font-weight: 400;padding:15px 0px;}
   .ui-widget.ui-widget-content { border: none !important; max-height: 350px; overflow: auto; box-shadow: 0 1px 5px rgb(0 0 0 / 12%); border-radius: 0px 0px 5px 5px;}
.ui-widget.ui-widget-content li { padding: 0px; border-bottom: 1px solid #e9e9e9;}
.ui-menu .ui-menu-item-wrapper{ padding: 15px !important; }
.ui-widget.ui-widget-content .item_img{width: 70px;padding: 3px;}
.ui-widget.ui-widget-content .cmn_btn{ font-size: 12px !important; height: 38px; line-height: 18px; letter-spacing: 0px; padding: 9px 8px;}
.ui-widget.ui-widget-content .item_info{ max-width: 100%; width: calc(100% - 200px);}
.ui-widget.ui-widget-content .item_info h4{ margin-bottom: 0px;}
.ui-widget.ui-widget-content .item_info .prodct_price_blk { margin-top: 5px;}

.item_qty{ margin-left: auto; text-align: right;}
.item_qty a i.fa-trash{ color:#e11919;}
ul#ui-id-1 {
    z-index: 9999;
}

.cart_blk h1{ font-size: 24px; margin-bottom: 25px;}
.cart_detail_blk { background: #fff; border-radius: 5px; box-shadow: 0 1px 2px rgba(0,0,0,.06); position: relative;}
.cart_detail_blk h2{ border-bottom: 1px solid #eee; font-size: 20px; padding: 15px 20px;}
.cart_items_detail{ padding: 0px 20px;}
.cart_item{ border-bottom: 1px solid #eee; display: flex; align-items: center; padding: 25px 0px;}
.cart_item:last-child{ border-bottom: 0px;}
.item_img{ width:110px; border: 1px solid #eaeaea; background: #fff; margin-right:15px; overflow:hidden; text-align:center; padding: 5px;}
.item_info{ max-width: 70%;}
.item_info h4{ font-size: 16px; font-weight: 500; line-height: 24px; margin-bottom: 5px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;}
.item_info em{ color: #1aab2a; display: block; font-size: 13px; font-style: normal; margin-bottom: 5px;}
.item_info span{ color: #888;}
.item_info .prodct_price_blk{ margin-top: 10px; }
.item_info .prodct_price_blk h3{ font-size: 20px;}
.item_seller span{ color: #888; display:block;}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

@stop  
<div class="main-content">
   <div class="page-content">
                   <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Validate Order</h4>
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">Validate Order</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                        <div class="row">
                            <div class="col-xl-7 order_val">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="vendor_profile">
                                            <h2>Patient Info</h2>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label>Patients Name</label>
                                                    <input type="text" class="form-control" value="{{$patient_address->patient_name ?? ''}}" readonly>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Age</label>
                                                    <input type="text" class="form-control" value="{{$patient_address->age ?? ''}}" readonly>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Sex</label>
                                                    <input type="text" class="form-control" value="{{$patient_address->sex ?? ''}}" readonly>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Phone</label>
                                                    <input type="text" class="form-control" value="{{$patient_address->mobile_number ?? ''}}" readonly>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Prefered Time For Call</label>
                                                    <input type="text" class="form-control" value="{{$patient_address->prefered_time_call ?? ''}}" readonly>
                                                </div>
                                                <div class="form-group col-sm-6" style="visibility:hidden;">
                                                    <label>Phone</label>
                                                    <input type="text" class="form-control" value="{{$patient_address->mobile_number ?? ''}}" readonly>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Complain <sup style="color: red;">*</sup></label>
                                                    <textarea class="form-control" name="complain" id="complain" rows="3" placeholder="Enter complain">{{$orderprescription->complain ?? '';}}</textarea>
                                                    <div id="complainError" class="error"></div>

                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Observation <sup style="color: red;">*</sup></label>
                                                    <textarea class="form-control" name="observation" id="observation" rows="3" placeholder="Enter observation">{{$orderprescription->observation ?? '';}}</textarea>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Diagnosis <sup style="color: red;">*</sup></label>
                                                    <textarea class="form-control" id="diagnosis" name="diagnosis" rows="3" placeholder="Enter diagnosis">{{$orderprescription->diagnosis ?? '';}}</textarea>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>Notes/Instruction <sup style="color: red;">*</sup></label>
                                                    <textarea class="form-control" rows="3" id="instruction" name="instruction" placeholder="Enter Notes/Instruction">{{$orderprescription->instruction ?? '';}}</textarea>
                                                </div>

                                                <input type="hidden" id="order_id" name="order_id" value="{{encrypt($order->id)}}">
                                                <input type="hidden" id="user_id" name="user_id" value="{{encrypt($order->user_id)}}">
                                                <input type="hidden" id="patient_id" name="patient_id" value="{{encrypt($order->patient_id)}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="action_btns">
                                    <a href="#" class="btn cmn_btn btn-success waves-effect waves-light">Preview</a>
                                    <a href="#" id="save_pre" class="btn cmn_btn btn-success waves-effect waves-light save_pre">Save Info</a>
                                </div>

<!--    <div class="add_new_product">
                            <div class="form-group">
                                <label>Medicine Name</label>
                                <input class="form-control" type="text" autocomplete="off" id="search" value="" placeholder="Enter medicine name">
                            </div>
                            <div class="text-center">
                                <button class="btn cmn_btn btn-primary">Add Medicine</button>
                            </div>
                        </div>
 -->


                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-centered mb-0 table-nowrap" id="datamytable">
                                                <thead class="bg-light">
                                                    <tr>
                                                        <th style="width: 120px">Product</th>
                                                        <th>Product Desc</th>
                                                        <th>Doses</th>
                                                        <th>Advice</th>
                                                        <th>Quantity</th>
                                                        <!-- <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                       <?php 
                                         $usercount = 0;?>             
                                       <?php $doctorcount = 0; $sendBtn=0;?>             
                                       @foreach($order->OrderItemDriver as $key=>$value)
                                             <?php
                                             $imagedata =json_decode($value->data);
                                             if(!empty($value->added_by)){
                                                $sendBtn = 1;
                                             }else{
                                                $sendBtn = 0;
                                             }
                                             ?>   

                                            @if($value->added_by=='doctor')
                                               <?php $usercount++;?>
                                               <tr style="background: #e1dede;">
                                            @else
                                               <?php $doctorcount;?>
                                               <tr style="background: #c2f3c296;">
                                            @endif
                                                   
                                                        <td><img src="{{$imagedata->image ?? ""}}" alt="product-img"></td>
                                                        <td>
                                                            <h5 class="font-size-15 text-truncate"><a href="javascript:void(0);" data-toggle="modal" data-target="#product_detail"> {{$value->ProductOrderItem->medicine_name??''}} </a></h5>
                                                        </td>
                                                        <td class="check">
                                                            <div class="checkbox-wrapper-mail">
                                                                <input value="m" class="doses" data-id="{{encrypt($value->ord_item_id)}}" data-type="morning" name="doses" type="checkbox" id="ordercheck{{$value->ord_item_id}}"{{  ($value->morning == 'm' ? ' checked' : '') }}>
                                                                <label for="ordercheck{{$value->ord_item_id}}" class="toggle"></label>
                                                                M 
                                                            </div>
                                                            <div class="checkbox-wrapper-mail">
                                                                <input value="n" class="doses" data-id="{{encrypt($value->ord_item_id)}}"  data-type="night"  name="doses" type="checkbox" id="ordercheck2{{$value->ord_item_id}}"{{  ($value->night == 'n' ? ' checked' : '') }}>
                                                                <label for="ordercheck2{{$value->ord_item_id}}" class="toggle"></label>
                                                                N
                                                            </div>
                                                            <div class="checkbox-wrapper-mail">
                                                                <input value="e" data-type="evening" class="doses" data-id="{{encrypt($value->ord_item_id)}}"  name="doses" type="checkbox" id="ordercheck3{{$value->ord_item_id}}"{{  ($value->evening == 'e' ? ' checked' : '') }}>
                                                                <label for="ordercheck3{{$value->ord_item_id}}" class="toggle"></label>
                                                                E
                                                            </div>
                                                        </td>
                                                         <td>

                                                <select style="width: 122px;" data-id="{{encrypt($value->ord_item_id)}}" class="form-control select-options">
                                                    <option >Please select</option>
                                                    <option value="before-meal" @if($value->advise=='before-meal') {{ 'selected' }} @endif>Before Meal</option>
                                                    <option value="after-meal" @if($value->advise=='after-meal') {{ 'selected' }} @endif>After Meal</option>
                                                </select>

                                                        </td>
                                                        <td>{{$value->quantity_order}}</td>
                                                        <td>
                                                            <!-- <a href="javascript:void(0);" class="mr-3 text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-size-18"></i></a> -->

                                                     <!-- <a href="javascript:void(0);" class="text-danger remove_product"  data-id="{{encrypt($value->ord_item_id)}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a> -->
                                                        </td>
                                                    </tr>
                                        @endforeach          
                                                        
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="action_btns" id="sendbtndata">
                                    <!-- <a href="javascript:void(0);" class="btn cmn_btn btn-primary" data-toggle="modal" data-target="#add_product"><i class="mdi mdi-plus mr-2"></i> Add New Medicine</a>
                                    @if($sendBtn>0)
                                    <a href="javascript:void(0);" id="sendrequest" class="btn cmn_btn btn-primary"><i class="fa fa-send-o"></i> Confrim & Send </a>
                                    @endif -->  
                                    <a href="javascript:void(0);" data-id="{{encrypt($order->id)}}" id="order_id_cc" class="btn cmn_btn btn-primary"><i class="fa fa-tasks" aria-hidden="true"></i> Completed Consultation </a>  
                                </div>
                            </div>
                            <div class="col-xl-5">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="uploaded_pres"><i class="ri-todo-line"></i> Prescription updated by the doctor <a href="#" class="btn-primary" data-toggle="modal" data-target="#prescription">View</a></div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        @if(!empty($order->prescription_image))
                                    <div class="prescription_img"><img src="{{$order->prescription_image}}" alt="prescription"></div>
                                  @endif 
                                      
                                     </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-7">
                                                    <label>Customer Notes</label>
                                                    <textarea class="form-control" rows="3" disabled >{{$order->customer_notes ?? '';}}</textarea>
                                                </div>
                        </div>
                 </div>
         
      </div>
      <!-- container-fluid -->
   </div>
   <!-- End Page-content -->
        <div id="add_product" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel">Add New Medicine</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="add_new_product">
                            <div class="form-group">
                                <label>Medicine Name</label>

                              <input type="hidden" id="order_id_add" name="order_id_add" value="{{encrypt($order->id)}}">
                              <input type="hidden" id="product_id_add" name="product_id_add" value="">

                                <input class="form-control selectval" type="text" autocomplete="off" id="search" value="" placeholder="Enter medicine name">
                            </div>
                            <div class="text-center">
                                <button id="addMedicine" class="btn cmn_btn btn-primary">Add Medicine</button>
                            </div>
                        </div> 
  

                    </div>
                </div>
            </div>
        </div>
       <div id="prescription" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content" style="width: 650px;">
         <div class="modal-header">
            <h5 class="modal-title mt-0" id="myModalLabel">Prescription</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div style="max-width:568px; width:100%; margin:auto; padding:0px; font-family: Arial; font-size: 14px; line-height: 18px">
               <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #333; padding:0px 15px;">
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="3" rowspan="3" align="left" valign="top" style="font-weight:bold;">ID: 3147500{{Auth::guard('doctor')->user()->id}}</td>
                     <td colspan="2" align="right" valign="top" style="font-size:20px; font-weight:bold; padding-bottom:5px;">Dr.{{Auth::guard('doctor')->user()->name}}</td>
                  </tr>
                  <tr>
                     <td colspan="2" align="right" valign="top" style="padding-bottom:3px;">Reg. No. {{Auth::guard('doctor')->user()->registration_number}}</td>
                  </tr>
                  <tr>
                     <td colspan="2" align="right" valign="top" style="font-weight:bold;">
                         {{ ucfirst(Auth::guard('doctor')->user()->degree_name) ?? '';  }}
                     </td>
                  </tr>
                  <tr>
                     <td colspan="5" style="border-bottom:1px solid #333;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td width="11%" rowspan="3" align="left" valign="middle"><img src="{{asset('public/frontend/images/rx-img.png')}}" width="70" height="74" alt="rx"/></td>
                     <td colspan="2" align="left" valign="middle">Patient Name: <span style="font-weight:bold;">{{$patient_address->patient_name ?? ''}}</span></td>
                     <td colspan="2" rowspan="3" align="left" valign="middle" style="padding: 16px;">Date/Time: <span style="font-weight:bold;">
                        <?php  echo date('d-m-Y',strtotime($order->created_at ?? '')); ?></span></td>
                  </tr>
                  <tr>
                     <td colspan="2" align="left" valign="middle">Age: <span style="font-weight:bold;">{{$patient_address->age ?? ''}} Year(S)</span></td>
                  </tr>
                  <tr>
                     <td colspan="2" align="left" valign="middle">Gender: <span style="font-weight:bold;">{{$patient_address->sex ?? ''}}</span></td>
                  </tr>
                  <tr>
                     <td colspan="5" style="border-bottom:1px solid #000;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" align="left" valign="middle">Diagnosis: <span style="font-weight:bold;">{{$orderprescription->diagnosis ?? ''}}</span></td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>


                  <tr>
                     <td align="left" valign="middle" style="border-bottom:1px solid #333; border-top:1px solid #333; padding:10px 0px;">S. No.</td>
                     <td width="39%" align="left" valign="middle" style="border-bottom:1px solid #333; border-top:1px solid #333; padding:10px 0px;">Prescribed Medicines</td>
                     <td width="13%" align="left" valign="middle" style="border-bottom:1px solid #333; border-top:1px solid #333; padding:10px 0px;">Dosage</td>
                     {{-- <td width="20%" align="left" valign="middle" style="border-bottom:1px solid #333; border-top:1px solid #333; padding:10px 0px;">Duration</td> --}}
                     <td width="17%" align="left" valign="middle" style="border-bottom:1px solid #333; border-top:1px solid #333; padding:10px 0px;">Advice</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>

             
             @foreach($order->OrderItemVendorNull as $key=>$value)   

                  <tr>
                     <td height="25" colspan="5" align="left" valign="middle">&nbsp;</td>
                  </tr>
                  <tr>
                     <td align="left" valign="middle"><span>{{++$key}}</span></td>
                     <td align="left" valign="middle">
                        <p style="font-weight:bold; margin-left:-51px; padding-bottom:5px;">{{$value->ProductOrderItem->medicine_name??''}}</p>
                      </td>
                     <td align="left" valign="middle">
                        <p style="margin:0px;">Daily</p>
                        <p style="margin:0px;">{{$value->morning}}-{{$value->night}}-{{$value->evening}}</p>
                     </td>
                     {{-- <td align="left" valign="middle">1-Week(s)</td> --}}
                     <td align="left" valign="middle">{{ucfirst($value->advise)}}</td>
                  </tr>
                  <tr>

               @endforeach   
      

             <td colspan="5" style="border-bottom:1px solid #333;">&nbsp;</td>
                  </tr>

                   <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px;">Complain for patient</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px; font-weight:bold;">{{$orderprescription->complain ?? '';}}</td>
                  </tr>

                   <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px;">Observation for patient</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px; font-weight:bold;">{{$orderprescription->observation ?? '';}}</td>
                  </tr>

                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px;">Instructions for patient</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px; font-weight:bold;">{{$orderprescription->instruction ?? '';}}</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px;">Note to Pharmacist</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:15px; font-weight:bold; padding-bottom:5px;">Kindly substitude brands as needed.</td>
                  </tr>

@php
if(!empty($doctorInfo->signature_image)){
$url= asset('storage/app/public/upload/Thumbnail/'.$doctorInfo->signature_image);
}else{
$url= asset('/public/assets/images/no-image.jpg');
}  
@endphp

                  <tr>
                      <td colspan="5" style="padding-bottom:8px;"><img src="{{$url}}" width="120" height="85" alt="sign"/></td>
                  </tr>
                  <tr>
                     <td colspan="5" style="font-size:20px; font-weight:bold; padding-bottom:10px;">Dr {{Auth::guard('doctor')->user()->name}}</td>
                  </tr>
                  <tr>
                     <td colspan="5">Reg. No. {{Auth::guard('doctor')->user()->registration_number}}</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="border-bottom:1px solid #333;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="padding:10px 0px 5px;">For any queries/complaints, contact</td>
                  </tr>
                  <tr>
                     <td colspan="5" style="padding:5px 0px;">Email: medicotechnologies2019@gmail.com</td>
                  </tr>
                  <tr>
                     <td colspan="5">&nbsp;</td>
                  </tr>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
 

   <footer class="footer">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
               Copyright © {{date("Y")}} Qikmeds. All Rights Reserved
           </div>
         </div>
      </div>
   </footer>
</div>
@endsection
{{-- page level scripts --}}        
 
@section('footer_scripts')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 --><script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script src="{{asset('public/assets/js/frontend/doctor.js')}}"></script>
<script type="text/javascript" src="http://172.105.36.210/ibazar/assets/js/jquery.autocomplete.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript"> 
   var update_info          ="{{URL::to('doctor/save-order-prescription')}}";
   var update_product_items ="{{URL::to('doctor/update-product-items')}}";
   var update_product_doses ="{{URL::to('doctor/update-product-doses')}}";
   var autocomplet_search ="{{URL::to('doctor/autocomplet-search-product')}}";
   var add_medicine_extra ="{{URL::to('doctor/add-medicine-extra')}}";
   var send_request ="{{URL::to('doctor/send-request-to-user')}}";
   var remove_product_items ="{{URL::to('doctor/remove-product-items')}}";
   var completedConsultation ="{{URL::to('doctor/completedConsultation')}}";
   var token                = '{{ csrf_token() }}';

//Select Options Values
$(document).ready(function(){
    $('#addMedicine').on('click',function(event){
          var id = $('#order_id_add').val();
          //alert(order_id_add)
          var product_id_add = $('#product_id_add').val();
         if (product_id_add =='') {
             toastr['error']('Please type medicine name...', "Info");
          }else{
          event.preventDefault();
          $.ajax({
            url:  add_medicine_extra,
            type: "POST",
            data: {'_token': token,id:id,product_id_add:product_id_add},
            dataType: 'json',
            beforeSend : function() {
                  $("#addMedicine").html('Please wait &nbsp;<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            success: function(response) {
                 $("#addMedicine").html('Add Medicine');
                console.log(response.status)
                    if(response.status=='validation'){
                        toastr['error']('Mandatory fields are required.', "Info");
                    }
                 $('#myForm').trigger("reset");   
                 toastr[response.status](response.message, "Info");
                setTimeout(function(){
                    $('#add_product').modal('hide');
                      $("#datamytable").load(document.URL + " #datamytable");  
                      $("#sendbtndata").load(document.URL + " #sendbtndata");  
                        //window.location.reload();

                },1000);
                }
            });
          }     
    });
});

//Select Options Values
$(document).ready(function(){
    $('#sendrequest').on('click',function(event){
          var order_id = $('#order_id_add').val();
          event.preventDefault();
          $.ajax({
            url:  send_request,
            type: "POST",
            data: {'_token': token,order_id:order_id},
            dataType: 'json',
            beforeSend : function() {
                  $("#sendrequest").html('Please wait &nbsp;<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            success: function(response) {
                 $("#sendrequest").html('Confrim & Send');
                console.log(response.status)
                    if(response.status=='validation'){
                        toastr['error']('Mandatory fields are required.', "Info");
                    }
                 $('#myForm').trigger("reset");   
                 toastr[response.status](response.message, "Info");
                setTimeout(function(){
                    $('#add_product').modal('hide');
                },1000);
                }
            });
    });
});

//Select Options Values
$(document).ready(function(){
    $('#order_id_cc').on('click',function(event){
       // alert('dsafsad')
          var com = $("#complain").val() || "";
          var obs = $("#observation").val() || "";
          var dia = $("#diagnosis").val() || "";
          var ins = $("#instruction").val() || "";
          //console.log(com,obs,dia,ins);
          if(!com || !obs || !dia || !ins){
            toastr['error']('Mandatory fields are required.', "Info");
                return false;

          }
          var order_id = $(this).attr("data-id");
         // alert(order_id)
          event.preventDefault();
          $.ajax({
            url:  completedConsultation,
            type: "POST",
            data: {'_token': token,order_id:order_id},
            dataType: 'json',
            beforeSend : function() {
                  $("#order_id_cc").html('Please wait &nbsp;<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            success: function(response) {
                 $("#sendrequest").html('Confrim & Send');
                console.log(response.status)
                    if(response.status=='validation'){
                        toastr['error']('Mandatory fields are required.', "Info");
                    }
                 $('#myForm').trigger("reset");   
                 toastr[response.status](response.message, "Info");
                setTimeout(function(){
                     window.location.href = response.url;
                   // $('#add_product').modal('hide');
                },1000);
                }
            });
    });
});




//Select Options Values
$(document).ready(function(){
    $('.remove_product').on('click',function(event){
        var order_item_id = $(this).attr("data-id");
          event.preventDefault();
          $.ajax({
            url:  remove_product_items,
            type: "POST",
            data: {'_token': token,order_item_id:order_item_id},
            dataType: 'json',
            success: function(response) {
                console.log(response.status)
                setTimeout(function(){
                 toastr[response.status](response.message, "Info");
                },1000);
                }
            });
    });
});



//Select Options Values
$(document).ready(function(){
    $('.select-options').on('change',function(event){
        var order_item_id = $(this).attr("data-id");
        var advise =  $(this).val();
          event.preventDefault();
          $.ajax({
            url:  update_product_items,
            type: "POST",
            data: {'_token': token,order_item_id:order_item_id,advise:advise},
            dataType: 'json',
           
            success: function(response) {
                console.log(response.status)
                    if(response.status=='validation'){
                        toastr['error']('Mandatory fields are required.', "Info");
                    }
                       toastr[response.status](response.message, "Info");
                    setTimeout(function(){
                       window.location.reload();

                    },2000);
                }
            });
              
        
    });
});


$(document).ready(function(){
     $('input[name="doses"]').on('change', function(event) {
        var order_item_id = $(this).attr("data-id");
        var doses = $('input[name="doses"]:checked').val();
         var type = $(this).attr("data-type");
          event.preventDefault();
          $.ajax({
            url:  update_product_doses,
            type: "POST",
            data: {'_token': token,order_item_id:order_item_id,doses:doses,type:type},
            dataType: 'json',
           
            success: function(response) {
                setTimeout(function(){
                   toastr[response.status](response.message, "Info");
                 },1000);
                }
            });
    });
});
 

$(document).ready(function(){
    $("#search").autocomplete({
        source: "{{ url('products/autocompleteajax') }}",
            focus: function( event, ui ) {
             $( "#search" ).val( ui.item.title );
             // uncomment this line if you want to select value to search box
             return false;
        },
        select: function( event, ui ) {
            console.log(ui);
             $( "#search" ).val( ui.item.title ); 
              $( "#product_id_add" ).val( ui.item.product_id );
             // uncomment this line if you want to select value to search box
            return false;
        }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
     var inner_html ='<div class="cart_item"><div class="item_img"><img width="100%" src="http://139.162.35.233/qikmeds/images/prodct-1.jpg" alt="product"></div><div class="item_info"><h4>'+item.title+'</h4> <span>Mfr: Nestle India Ltd</span><div class="prodct_price_blk"><h3>₹ 711.55</h3> <span class="strike">₹ 749.00</span> <small>5% Off</small></div></div><div class="item_qty"> <i class="fa fa-medkit"></i></div></div>';
        return $( "<li></li>" )
                .data( "item.autocomplete",item)
                .append(inner_html)
                .appendTo( ul );
    };
});

</script>
@stop
 