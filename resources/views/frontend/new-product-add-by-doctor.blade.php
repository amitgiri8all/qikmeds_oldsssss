@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

{{-- <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 --}} <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/js-offcanvas.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
<style type="text/css">
    .c-offcanvas--right {
  height: 100%;
  width: 29em !important;
  right: 0;
  transform: translate3d(400px, 0, 0);
}
a.js-offcanvas-trigger.c-button.cmn_btn {
    text-align: -webkit-center;
    width: 21em;
    margin-bottom: 1em;
}
</style>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Order Review</li>
              </ol>
            </nav>
        </div>
        <div class="cart_blk">
<!--             <h1>Order Review</h1>
 -->            <div class="row">
                <div class="col-sm-9">
<!--                     <h2 class="review_title">Products</h2>
 -->                    <div class="cart_detail_blk review">
    @if($productDetail)   
                        <div class="cart_items_detail">
                            <div class="cart_item">
                                <div class="delivery_date">This Product Added By Doctor<span> 22-August-2021</span></div>
                                <div class="item_img"><img src="{{asset('public/frontend/images/prodct-1.jpg')}}"> </div>
                                <div class="item_info">
                                    <h4>{{$productDetail->medicine_name}} - {{$productDetail->type_of_sell}}</h4>
                                    <div class="item_seller">
                                        <span>Mfr: Nestle India Ltd</span>
                                         <small>Expiry: Sep 2022</small>
                                    </div>
                                </div>
                                <div class="prodct_price_blk">
                                    <h3>₹ {{$productDetail->mrp}}</h3>
                                    <span class="strike">₹ 749.00</span>
                                 </div>
                            </div>
                        </div>

                          @else
                            <h3>Not Product Yet :)</h3>
                         @endif

                    </div>
                   

            
                     
                </div>
           <div class="col-sm-3">
                    <div class="order_smry_blk">
                        <div class="order_price_blk">
                             <div class="payment_details">
                                <?php
                                $product_id = encrypt($productDetail->id);
                                $order_id = $order_id;
                                ?>
                                 <a href="{{url('extra-product-payment/'.$product_id.'/'.$order_id)}}" type="button" class="cmn_btn" style="color:#fff; width: 100%; text-align: center; margin-bottom: 24px;">Accept & Pay</a> 
                                 <a type="button" class="cmn_btn" style="width: 100%; text-align: center; color:#fff; background: #be1818e6;">Cancel</a>
                            </div>
                        </div>
                        <div class="safety_txt">Products will be safely packed &amp; Sanitized. Pay online for contactless delivery.</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
 


@stop
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/js-offcanvas.pkgd.js')}}"></script>

<script type="text/javascript">

var select_address ="{{route('user.select-address')}}";
var select_patient ="{{route('user.select-patient')}}";
var delivery_address_check ="{{route('delivery_address_check')}}";
 var token = '{{ csrf_token() }}';

 $(document).on('click', '.update_address', function() {
            selected_id= $(this).attr("data-id");
         event.preventDefault();
          $.ajax({
            url:  select_address,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
                toastr[response.status]("Updated in your cart", "Cart");
                // $("#select_address").load(document.URL + " #select_address");
                  window.location.reload();
            }
            });
 });

$(document).on('click', '.update_info_patient', function() {
            selected_id= $(this).attr("data-id");
           // alert(selected_id)
        // event.preventDefault();
          $.ajax({
            url:  select_patient,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
                toastr[response.status]("Updated in your cart", "Cart");
                // $("#select_address").load(document.URL + " #select_address");
                  window.location.reload();
            }
            });
 });

 $(document).on('click', '.delivery_address_check', function() {
         event.preventDefault();
          $.ajax({
            url:  delivery_address_check,
            type: "POST",
            data: {'_token': token},
            dataType: 'json',
            success: function(response) {
                if(response.redirect_url){
                    window.setTimeout(function(){window.location.href = response.redirect_url;},300);
                }
                toastr[response.status](response.message,response.status);
                $(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> '+response.message+'</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();            }
            });
 });
 $('body').on('click', '.close', function(){
        $(".main_error").html('').removeClass('alert-danger').hide();
     });
</script>

<script type="text/javascript">
   var delivery ="{{route('delivery-location.get-details.post')}}";

var token       = '{{ csrf_token() }}';
$('#loading').hide();
   $(document).ready(function(){
    $(document).on('keyup', '.getdetails', function()
    {
      //  alert('okkkk')
    var length =  $("#pin_code").val().length;
      if (length == 6) {
          $('select[name="name"]').empty();
           var pin_code        = $("#pin_code").val();
           // alert(pin_code)
           $.ajax({
               url         : delivery,
               type        : 'POST',
               data        : {'_token': token,pin_code:pin_code},
               dataType    : 'json',
               beforeSend: function() {
                 $('#loading').show();
               },
               success     : function(data) 
               {

                  $('select[name="name"]').empty();
                  $('#state').val(data.state);
                  $('#city').val(data.city);
                 $(data.AllState).each(function(index, el) {
                     $('select[name="name"]').append('<option>'+ el +'</option>');
                  });


                },
               complete: function(){
                $('#loading').hide();
               },
               error       : function(response)
               {
                $('#error').html('<div class="alert alert-danger" role="alert">The requested resource is not found</div>');
               }
           });
         }  

    });  
   });  

</script>

@stop
