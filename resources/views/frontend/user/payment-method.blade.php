
@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


@stop



{{-- Page content --}}
@section('content')
 
<!--                                   
 -->
<section class="middle_section p_30">
	<div class="container">
		<div class="breadcrumb_blk">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Account</li>
			  </ol>
			</nav>
		</div>
		<div class="product_listing_blk">
			<div class="row">
				   @include('frontend.user.leftpart')

				<div class="col-lg-9 col-md-8">
					<div class="right_sidebar">
						<h2>Payment Methods</h2>
						<div class="user_detail_blk cmn_area">
							<div class="row">
							@foreach($payment_methods as $kye=>$val)	
								<div class="col-sm-12">
									<div class="card-box">
										<h4>{{$val->bank_name}} <span><a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalEdit{{$val->id}}"></i> Edit</a></h4> 
										<p>{{-- <img src="images/visa-credit-card.png" alt="img">  --}}

										@php
                    if(!empty($val->image)){
                    $url= asset('storage/app/public/upload/Thumbnail/'.$val->image);
                    }else{
                     $url= asset('/public/assets/images/no-image.jpg');
                    }
                  @endphp
                      <img src="<?php echo $url ?>" alt="img" width="40" height="30" >
											<?php $number_card = App\Helpers\Helper::ccMasking($val->card_number);?>  {{$number_card }}  </p> 
										<!-- <span class="card-delete"><a href="#"><i class="ti-trash"></i></a> </span> -->
										<span class="card-delete"><button type="button" class="edit_btn dlt deleteRecord"  data-id="{{ $val->id }}" ><i class="ti-trash"></i></button></span>
									 </div>
								</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalEdit{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('user.payment-method-edit.post',$val->id)}}" id="payment_method" class="ajax_form" method="post">
      	@csrf
		      <div class="modal-body">
								<div class="user_address_blk cmn_area">
								  <div class="row">
									<div class="form-group col-sm-12">
										<label>Card Number</label>
										<input type="text" class="form-control" name="card_number" value="{{$val->card_number}}" placeholder="Enter Card Number">
									</div>	
									<div class="form-group col-sm-12">
										<label>Bank Name</label>
										<input type="text" class="form-control" name="bank_name" value="{{$val->bank_name}}" placeholder="Enter Bank Name">
									</div>
									<div class="col-sm-12">
									  <label>Expiration Date</label>
									  <div class="row">
										<div class="form-group col-sm-12">
											<input type="text" class="form-control" value="{{$val->expiration_month}}" name="expiration_month" placeholder= "MM">
										</div>
									  <label>Expiration Year
									  </label>
										<div class="form-group col-sm-12">
											<input type="text" class="form-control" value="{{$val->expiration_year}}" name="expiration_year" placeholder="YYYY">
										</div>
									  </div>
									</div>
									 <div class="form-group col-sm-6">
										<label>Card CVC</label>
										<input type="text" class="form-control" value="{{$val->cvc}}"   name="cvc" placeholder="Enter csv Number">
									</div> 
									<div class="form-group col-sm-6">
										<label>Card Image</label>
										<input type="file" class="form-control" name="image">
									</div>
									<div class="form-group col-sm-12">
										<label>Cardholder Name</label>
										<input type="text" class="form-control" value="{{$val->cardholder_name}}"  name="cardholder_name" placeholder="Enter Name">
									</div>
								  </div>
								</div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="cmn_btn">Update This Card</button>
		      </div>
		  </form>
    </div>
  </div>
</div>
   @endforeach		
								<div class="col-sm-12">
									<a href="#" class="cmn_btn" data-toggle="modal" data-target="#exampleModal"><i class="ti-plus"></i> ADD NEW CARD</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('user.payment-method.post')}}" id="payment_method" class="ajax_form" method="post">
      	@csrf
		      <div class="modal-body">
								<div class="user_address_blk cmn_area">
								  <div class="row">
									<div class="form-group col-sm-12">
										<label>Card Number</label>
										<input type="text" class="form-control" name="card_number" placeholder="Enter Card Number">
									</div>	
									<div class="form-group col-sm-12">
										<label>Bank Name</label>
										<input type="text" class="form-control" name="bank_name" placeholder="Enter Bank Name">
									</div>
									<div class="col-sm-12">
									  <label>Expiration Date</label>
									  <div class="row">
										<div class="form-group col-sm-12">
											<input type="text" class="form-control" name="expiration_month" placeholder= "MM">
										</div>
										<div class="form-group col-sm-12">
											<input type="text" class="form-control" name="expiration_year" placeholder="YYYY">
										</div>
									  </div>
									</div>
									 <div class="form-group col-sm-6">
										<label>Card CVC</label>
										<input type="text" class="form-control" name="cvc" placeholder="Enter csv Number">
									</div> 
									<div class="form-group col-sm-6">
										<label>Card Image</label>
										<input type="file" class="form-control" name="image">
									</div>
									<div class="form-group col-sm-12">
										<label>Cardholder Name</label>
										<input type="text" class="form-control" name="cardholder_name" placeholder="Enter Name">
									</div>
								  </div>
								</div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="cmn_btn">Save This Card</button>
		      </div>
		  </form>
    </div>
  </div>
</div>

@stop


{{-- page level scripts --}}
@section('footer_scripts')



<script type="text/javascript">
	 /*===========================*/
 $(".deleteRecord").click(function(){
    var id = $(this).data("id");
    var token = '{{ csrf_token() }}';
    //alert(id)
    $.ajax(
    {
        url: "deletePayment/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
            toastr['success']("Delete Successfully", "Delete");
            window.location.reload();

        }
    });
   
});

</script>
@stop



 