@extends('frontend.layouts.default')
{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
@stop
{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Wallet</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
               
                @include('frontend.user.leftpart')

<div class="col-sm-9">
                    <div class="right_sidebar">
                      

<!-- Addd patient  Info -->

    <div class="address_change_blk">
        <h2>Add New Patient Info</h2>
         <form class="ajax_form" id="delivery_address" action="{{route('user.save-patient-info')}}" method="post">
        @csrf
                        <div class="user_address_blk cmn_area">
                          <div class="row">
                             <div class="form-group col-sm-12" style="display:none;">
                                <button class="cmn_btn" type="button"><i class="ti-target"></i> Use my Current Location</button>
                            </div>

                                    <div class="form-group col-sm-12">
                                        <label>Patients Name <sup>*</sup></label>
                                        <input type="text" class="form-control" value="{{old('patient_name')}}" name="patient_name" placeholder="Patients Name">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label>Age <sup>*</sup></label>
                                        <input type="text" maxlength="100" class="form-control num" value="{{old('mobile_number')}}" name="age" placeholder="Age">
                                    </div>

                                     
                                    <div class="form-group col-sm-12">
                                         <label>Sex<sup>*</sup></label>
                                        <select class="form-control" name="sex" id="sex">
                                            <option value="male">Male</option>
                                            <option value="female">FeMale</option>
                                        </select>
                                    </div>

                                    

                                   

                                <div class="form-group col-sm-12">
                                    <label>Mobile number <sup>*</sup></label>
                                    <input type="text" maxlength="10" class="form-control num" value="{{old('mobile_number')}}" name="mobile_number" placeholder="10 - digit mobile number">
                                </div>

                                <div class="form-group col-sm-12">
                                         <label>Prefered Time For Call<sup>*</sup></label>
                                        <select class="form-control" name="prefered_time_call" id="prefered_time_call">
                                            <option value="">Please Select Time For Call</option>
                                            <option value="morning">Morning</option>
                                            <option value="afternoon">After Noon</option>
                                            <option value="evening">Evening</option>
                                        </select>
                                </div>

                            <div class="form-group col-sm-12 mgn_0">
                                <button type="submit" class="cmn_btn">Save</button>

                            </div>



                           </div>
                        </div>
                      </form>
        </div>
    </div>

                    </div>
                </div> 
        </div>
    </div>
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
@stop


 