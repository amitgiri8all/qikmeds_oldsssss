@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop



{{-- Page content --}}
@section('content')
 
<!--                                   
 -->


<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Delivery Addresses</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
               
                @include('frontend.user.leftpart')

              <div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                        <h2>Delivery Addresses</h2>
                        <div class="user_address_blk cmn_area" >
                          <div class="row">
                            @foreach($addresses as $key=>$value)

                             <div class="col-sm-6">
                                <div class="address_blk">
                                  <div id="select_address">  

                                    <div class="radio_btn">
                                        <input type="radio" class="update_address" data-id="{{$value->id}}" name="radio" id="r-{{$value->id}}" {{ ($value->status=="1")? "checked" : "" }}>
                                        <label for="r-{{$value->id}}">&nbsp;</label>
                                    </div>
                                </div>                                    
                                    <h4 style="padding-top: 20px;">{{$value->name}}</h4>
                                    <h5>{{$value->mobile_number}}</h5>
                                    <span>{{$value->address_type}}</span>

                                    <p>{{$value->address_delivery}},{{$value->locality}}<br> {{$value->city}}, {{$value->state}} - {{$value->pin_code}}</p>
                                    
                                    <a class="btn btn-success" href="{{$edit_url = URL::to("user/edit/$value->id")}}"><i class="fa fa-edit"></i>Edit </a>
                                    <button type="button" class="edit_btn dlt deleteRecord"  data-id="{{ $value->id }}" >Delete</button>
                                </div>
                            </div>

                         @endforeach   

                            <div class="col-sm-12">
                                <a href="{{route('user.add-new-address')}}" class="cmn_btn"><i class="ti-plus"></i> ADD NEW ADDRESS</a>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
 
@stop


{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<script type="text/javascript">

var select_address ="{{route('user.select-address')}}";
 var token = '{{ csrf_token() }}'; 

 $(document).on('click', '.update_address', function() { 
            selected_id= $(this).attr("data-id");
         event.preventDefault();
          $.ajax({
            url:  select_address,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
                //toastr[response.status]("Updated in your cart", "Cart");
                // $("#select_address").load(document.URL + " #select_address");
                window.location.reload();
                 }
            });

          $('html, body').animate({
      scrollTop: $("#select_address").offset().top
  }, 1000);
});
 

 /*===========================*/
 $(".deleteRecord").click(function(){
    var id = $(this).data("id");
    var token = '{{ csrf_token() }}';
 //  alert(id)
    $.ajax(
    {
        url: "deleteDeliveryAddress/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
            toastr['success']("Delivery Address Delete Successfully", "Delete");
            window.location.reload();

        }
    });
   
});

</script>
@stop


 