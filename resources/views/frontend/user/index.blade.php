@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<style type="text/css">
    img#output {
    width: 200px;
    height: 89px;
}
</style>
<link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
@stop

{{-- Page content --}}
@section('content')
 
<!--                                   
 -->

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Account</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
               
                @include('frontend.user.leftpart')

                <div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                        <h2>Personal Information</h2>
                        <div class="user_detail_blk cmn_area">
                         <form method="post" action="{{route('user.update_profile_user')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Full Name</label>
                                        <span><input type="text" name="name" class="form-control" value="{{Auth::guard('web')->user()->name}}" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Email Address</label>
                                        <span><input type="text" name="email" class="form-control" value="{{Auth::guard('web')->user()->email}}" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Mobile Number</label>
                                          <span><input type="text" readonly name="mobile_number" class="form-control" value="{{Auth::guard('web')->user()->mobile_number}}" ></span>                                    
                                        </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Image</label>
                                            <input type="file"  name="image" id="file"  onchange="loadFile(event)" class="form-control">  
                                        </div>
                                </div>  
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Date of Birth</label>
                                          <span><input type="date" name="dob" class="form-control" value="{{Auth::guard('web')->user()->dob}}" ></span> 
                                   
                                    </div>
                                </div>
 
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                         <p><img src="{{Auth::guard('web')->user()->image}}" id="output" width="200" /></p>
                                   
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="user_txt">
                                        <label>Gender</label>
                                        <div class="radio_btn">
                                            <?php $gender = Auth::guard('web')->user()->gender ?>
                                            <input type="radio" name="gender" value="male" id="r-1" <?php if($gender == "male") { echo 'checked'; } ?>>
                                            <label for="r-1">Male</label>
                                        </div>
                                        <div class="radio_btn">
                                            <input type="radio" name="gender" value="female" id="r-2"<?php if($gender == "female") { echo 'checked'; } ?>>
                                            <label for="r-2">Female</label>
                                        </div>
                                        <div class="radio_btn">
                                            <input type="radio" name="gender" value="other" id="r-3"<?php if($gender == "other") { echo 'checked'; } ?>>
                                            <label for="r-3">Others</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="cmn_btn"><i class="fa fa-edit"></i> Edit</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div class="right_sidebar">
                        <h2>Change Password</h2>
                        <div class="user_detail_blk cmn_area">
                         <form method="post" id="change-password" action="{{route('user.change-password')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                            <div class="row">
                              @if(!empty(Auth::guard('web')->user()->password))
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Old password</label>
                                        <span><input type="password" name="old_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="1">
                              @else
                                <input type="hidden" name="type" value="0">
                                
                              @endif  
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>New password</label>
                                        <span><input type="password" name="new_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Confirm Password</label>
                                        <span><input type="password" name="confirm_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <br /><br />

                              
                                    
                                <div class="col-sm-12" style="margin-top: 10px;">
                                    <button type="submit" class="cmn_btn"> <i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 
@stop


{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


<script>
var select_address ="{{route('user.select-address')}}";
var token = '{{ csrf_token() }}'; 


var loadFile = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};
</script>

@stop


 