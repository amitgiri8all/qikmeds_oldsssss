@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


@stop



{{-- Page content --}}
@section('content')
 
<!--                                   
 -->

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">My Orders</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                @include('frontend.user.leftpart')

     <div class="col-lg-9 col-md-8">
        <div class="right_sidebar">
            <h2>Notification</h2>
            <div class="orders_blk cmn_area">
              <div class="row">
               
                     <div class="col-md-12">
                      <div class="tabbable-panel">
                        <div class="tabbable-line">
                           <ul class="nav nav-pills nav-justified" role="tablist">
                           <li><a class="nav-link active" role="tab" href="#tab_default_1" data-toggle="tab">
                           Promotions</a>
                         </li> 
                          <li>
                             <a class="nav-link" role="tab" href="#tab_default_2" data-toggle="tab">
                             Alert </a>
                         </li>   
               </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_default_1">
                       <div class="panel-body">
                              
                               <tbody>
                                @foreach($order_notification as $key => $value)
                               
                                <div class="row" style="padding-top: 30px;">
                                 <div class="col-sm-3" > 
                              <tr>
                              <td>
                                @php
                               if(!empty($value->image)){
                               $url = asset('storage/app/public/upload/Thumbnail/'.$value->image);
                               }
                               else{
                               $url = asset('/public/assets/images/no-image.jpg');
                               }
                               @endphp
                               <img src="<?php echo $url; ?>" width="100" height="100" ></td>
                                </div>  
                                <div class="col-sm-9" >
                               <td>{{$value->created_at}} </td><br>
                               <td>{{$value->message_heading}} </td><br>
                               <td>{{$value->message}} </td>
                               </div>
                           </div>

                                </tr>
                                @endforeach
                               </tbody>
                         </div>

                  
                  </div>


                  <div class="tab-pane" id="tab_default_2">
                       <div class="row">
                        @foreach($alert_notification as $key => $value)
                        
                        <div class="col-sm-4" style="padding-top: 30px;"> 
                        <div class="card " style="width: 18rem;">
                          <div class="card-body">
                            <h6 >Date <td>{{$value->created_at}}</td></h6>

                            <tr><td>Notification</td> <br></tr>

                           <?php 
                            if(isset($value->data)){
                            $myArray = json_decode($value->data, true);
                            //print_r($myArray);
                            ?>
                           
                            <p class="card-text"> Message : <td>{{$myArray['message'] ?? ' ' }}</td></p>
                          
                            <p class="card-text"> Status:   <td></td></p>
                          <?php } ?>
                          </div>
                        </div>
                        </div>
                     
                         @endforeach
                         </div>
                        </div>
                       </div>
                    </div>
                 </div>
              </div> 
          </div>
        </div>
    </div>
</div> 
</div>
</div>
</div>
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')



@stop


 