
@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop 

{{-- Page content --}}
@section('content')
 
<!--                                   
 -->
<section class="middle_section p_30">
	<div class="container">
		<div class="breadcrumb_blk">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Prescription</li>
			  </ol>
			</nav>
		</div>
		<div class="product_listing_blk">
			<div class="row">
				   @include('frontend.user.leftpart')
				  <div class="col-lg-9 col-md-8">
					<div class="right_sidebar">
						<div class="subscription_blk cmn_area">
							<h3>Qikmeds Premium</h3>
							<h4>Reduce your medical expenses with our premium membership benefits</h4>
						  <div class="row">
						  @foreach($subscription_meta as $key=>$val)	
						  	<div class="col-sm-4">
						  		<div class="subscription_benefts">
						  			 @php
			                     if(!empty($val->image)){
			                     $url = $val->image;
			                     }
			                     else{
			                     $url = asset('/public/assets/images/no-image.jpg');
			                     }
			                     @endphp
			                     
									<i><img src="<?php echo $url; ?>"></i>
						  			<h5><!-- {{$val->price}}  -->{{$val->title}}</h5>
						  			<p>{{$val->description}}</p>
						  		</div>
						  	</div>
						  @endforeach	

						<!--   	<div class="col-sm-4">
						  		<div class="subscription_benefts">
						  			<i><img src="images/benefit-2.png"></i>
						  			<h5>Free Doctor Teleconsultation</h5>
						  			<p>Get free teleconsultation from experts</p>
						  		</div>
						  	</div>
						  	<div class="col-sm-4">
						  		<div class="subscription_benefts">
						  			<i><img src="images/benefit-3.png"></i>
						  			<h5>No delivery Charge</h5>
						  			<p>Enjoy free delivery on your order*</p>
						  		</div>
						  	</div> -->


						  </div>
						</div>
					</div>
					<div class="benefit_btn">
						<a href="{{route('user.subscription-plan')}}" class="cmn_btn">Get Qikmeds Premium</a>
					</div>
					<div class="right_sidebar">
						<div class="faq_content cmn_area">
							<div id="accordion1">

							@foreach($subscription_faq as $key=>$val)		
							  <div class="card">
								<div class="card-header" id="heading11{{$val->id}}">
									<a href="#accordion" data-toggle="collapse" data-target="#collapse11{{$val->id}}" aria-expanded="false" aria-controls="collapse11">{{$val->question}}</a>
								</div>
								<div id="collapse11{{$val->id}}" class="collapse" aria-labelledby="heading11{{$val->id}}" data-parent="#accordion1">
								  <div class="card-body">
									<p>{{$val->answer}}</p>
								  </div>
								</div>
							  </div>
							  @endforeach

<!-- 
							  <div class="card">
								<div class="card-header" id="heading12">
									<a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">How long is my Qikmeds Premium membership valid for?</a>
								</div>
								<div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion1">
								  <div class="card-body">
									<p>Your PharmEasy Plus membership is valid for 3 or 6 months from the enrollment date of the program. Log in to know more.</p>
								  </div>
								</div>
							  </div>


							  <div class="card">
								<div class="card-header" id="heading13">
									<a href="#accordion" class="collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">When will I get the cashback?</a>
								</div>
								<div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion1">
								  <div class="card-body">
									<p>Cashback will be credited into your Qikmeds wallet within 48 hours of order delivery.</p>
								  </div>
								</div>
							  </div> -->


							</div>
						</div>
					</div>
				</div>
 				  
		</div>
	</div>
</section>
 

 



@stop


{{-- page level scripts --}}
@section('footer_scripts')
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


@stop


 