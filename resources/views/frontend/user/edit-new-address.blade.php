@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


@stop



{{-- Page content --}}
@section('content')
 
<!--                                   
 -->

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Account</li>
              </ol>
            </nav>
        </div>

        <div class="product_listing_blk">
            <div class="row">
               
                @include('frontend.user.leftpart')

                <div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                        <h2>Edit New Address</h2>
                             <form class="ajax_form" id="delivery_address"  method="post">                    
                         @csrf
                        <div class="user_address_blk cmn_area">
                          <div class="row">
                             <div class="form-group col-sm-12" style="display:none">
                                <button class="cmn_btn" type="button"><i class="ti-target"></i> Use my Current Location</button>
                            </div>
                                 <?php  //echo "<pre>";print_r($data);die; ?>
                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" name="name" placeholder="Name" value="@if(isset($data->name)){!! $data->name !!}@else{!! old('name') !!}@endif">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control num" value="@if(isset($data->mobile_number)){!! $data->mobile_number !!}@else{!! old('mobile_number') !!}@endif" name="mobile_number" placeholder="10 - digit mobile number">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control num" value="@if(isset($data->pin_code)){!! $data->pin_code !!}@else{!! old('pin_code') !!}@endif" name="pin_code" placeholder="Pincode">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="@if(isset($data->locality)){!! $data->locality !!}@else{!! old('locality') !!}@endif" name="locality" placeholder="Locality">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <textarea class="form-control" rows="5"  name="address_delivery" placeholder="Address (Area and Street)">@if(isset($data->address_delivery)){!! $data->address_delivery !!}@else{!! old('address_delivery') !!}@endif</textarea>
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="@if(isset($data->city)){!! $data->city !!}@else{!! old('city') !!}@endif" name="city" placeholder="City/District/Town">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="@if(isset($data->state)){!! $data->state !!}@else{!! old('state') !!}@endif" name="state" placeholder="State">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" name="landmark" value="@if(isset($data->landmark)){!! $data->landmark !!}@else{!! old('landmark') !!}@endif" class="form-control" placeholder="Landmark(Optional)">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="@if(isset($data->alternate_phone)){!! $data->alternate_phone !!}@else{!! old('alternate_phone') !!}@endif" name="alternate_phone" placeholder="Alternate Phone (Optional)">
                                    </div>

                                              <!-- Address Location -->
                                      <div class="form-group col-sm-12" >
                                         <label for="text">Address Location</label>
                                         <div class="input-group col-xs-12">
                                            <input type="text" id="address_name_user" placeholder="Address name" class="form-control" name="address_location">
                                            {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                            </span>
                                         </div>
                                      </div>  
                                      <!-- Address Location -->
                                      <div class="form-group col-sm-12" >
                                         <label for="text">Latitute</label>
                                         <div class="input-group col-xs-12">
                                            <input type="text" id="lat_user" readonly placeholder="Address name" class="form-control" name="latitute" value="@if(isset($data->latitute)){!! $data->latitute  !!}@else{!! old('latitute') !!}@endif">
                                            {!! $errors->first('latitute', '<span class="help-block">:message</span>') !!}
                                            </span>
                                         </div>
                                      </div>  

                                      <!-- Address Location -->
                                      <div class="form-group col-sm-12" >
                                         <label for="text">Longitute</label>
                                         <div class="input-group col-xs-12">
                                            <input type="text" id="long_user" readonly placeholder="Longitute name" class="form-control" name="longitute" value="@if(isset($data->longitute)){!! $data->longitute !!}@else{!! old('longitute') !!}@endif"

                                            >
                                            {!! $errors->first('longitute', '<span class="help-block">:message</span>') !!}
                                            </span>
                                         </div>
                                      </div>
                                      

                                      <!-- Address Location -->
                                      <div class="form-group col-sm-12" >
                                          <div class="input-group col-xs-12">
                                       <div id="usser3" style="width: 100%; height: 300px;"></div> 
                                            </span>
                                         </div>
                                      </div> 


                                    <div class="col-sm-12">
                                        <div class="user_txt">
                                            <label>Address Type</label>
                                            <div class="radio_btn">
                                                <input type="radio" name="address_type" id="r-1" value="home" <?php if($data->address_type == "home") { echo 'checked'; } ?>>
                                                <label for="r-1">Home</label>
                                            </div>
                                            <div class="radio_btn">
                                                <input type="radio" value="work" name="address_type" id="r-2" <?php if($data->address_type == "work") { echo 'checked'; } ?>>
                                                <label for="r-2">Work</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-12 mgn_0">
                                        <button type="submit" class="cmn_btn">Update</button>
                                        <button type="submit" class="cncl_btn">Cancel</button>
                                    </div>



                                   </div>
                                </div>
                              </form>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         
@stop


{{-- page level scripts --}}
@section('footer_scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
<script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
<script type="text/javascript">
            $('#usser3').locationpicker({
                location: {
                    latitude: <?php echo $data->latitute ;?>,
                    longitude: <?php echo $data->longitute ;?>
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat_user'),
                    longitudeInput: $('#long_user'),
                    //radiusInput: $('#us3-radius'),
                    locationNameInput: $('#address_name_user')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script>

@stop




 