@extends('frontend.layouts.default')
{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
@stop
{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Wallet</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
               
                @include('frontend.user.leftpart')

<div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                        <h2>My Wallet</h2>
                        <div class="wallet_blk cmn_area">
                            <div class="balance_blk">
                                <div class="row align-items-center">
                                    <div class="col-sm-6">
                                        <div class="wallet_left">
                                            <h3>My Wallet Balance <span>₹ {{-- 200.00 --}}</span></h3>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="wallet_right">
                                            <img src="images/wallet.jpg" alt="wallet">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="transaction_blk">
                                <h3>All Transactions</h3>
                                <ul>
                                   {{--  <li>
                                        <i><img src="images/debit.png" alt="debit"></i>
                                        <div class="transaction_left">
                                            <h4>Cashback Debited</h4>
                                            <p>Order Id: 20526621</p>
                                        </div>
                                        <div class="transaction_right">
                                            <h4 class="red">- ₹100.00</h4>
                                            <p>10th May, 2021</p>
                                        </div>
                                    </li>
                                    <li>
                                        <i><img src="images/credit.png" alt="credit"></i>
                                        <div class="transaction_left">
                                            <h4>Cashback Credited</h4>
                                            <p>Order Id: 20526621</p>
                                        </div>
                                        <div class="transaction_right">
                                            <h4 class="green">₹100.00</h4>
                                            <p>30th April, 2021</p>
                                        </div>
                                    </li>
                                    <li>
                                        <i><img src="images/debit.png" alt="debit"></i>
                                        <div class="transaction_left">
                                            <h4>Cashback Debited</h4>
                                            <p>Order Id: 20526621</p>
                                        </div>
                                        <div class="transaction_right">
                                            <h4 class="red">- ₹100.00</h4>
                                            <p>10th May, 2021</p>
                                        </div>
                                    </li>
                                    <li>
                                        <i><img src="images/credit.png" alt="credit"></i>
                                        <div class="transaction_left">
                                            <h4>Cashback Credited</h4>
                                            <p>Order Id: 20526621</p>
                                        </div>
                                        <div class="transaction_right">
                                            <h4 class="green">₹100.00</h4>
                                            <p>30th April, 2021</p>
                                        </div>
                                    </li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> 
        </div>
    </div>
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
@stop


 