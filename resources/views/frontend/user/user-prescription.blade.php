
@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <style type="text/css">
 	.deletebtn{
  padding:10px 25px;
  text-align:center;
}
 </style>

@stop 

{{-- Page content --}}
@section('content')
 
<!--                                   
 -->
<section class="middle_section p_30">
	<div class="container">
		<div class="breadcrumb_blk">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Prescription</li>
			  </ol>
			</nav>
		</div>
		<div class="product_listing_blk">
			<div class="row">
				   @include('frontend.user.leftpart')
				   <div class="col-lg-9 col-md-8">
					<div class="right_sidebar">
						<h2>My Prescription</h2>
 						  	 		 @csrf			
						<div class="user_address_blk cmn_area">
						  <div class="row">
						  	@if(!empty($userprescription->image))
						  	 <img src="{{$userprescription->image}}" width="350px" height="250px">
						  	@else
								<img src="http://172.105.36.210/qikmeds/public/assets/images/no-image.jpg" style="width: 30%;">		 @endif	 
						 </div>
					</div>
				</div>
				 <div class="col-sm-12 deletebtn" style="display:block;">
                       <button type="button" class="edit_btn dlt deleteRecord cmn_btn btn-danger"  data-id="{{Auth::guard('web')->user()->id}}" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                 </div>

				<div class="drop-zone">
               <form method="post" id="zone" action="{{route('save-upload-prescription')}}" enctype="multipart/form-data" class="ajax_form">   
                @csrf      
                    <div class="col-sm-6" style="display:block;">
                    <div class="user_txt">
                       <label>Image</label>
                          <input type="file"  name="image" id="file"  onchange="loadFile(event)" class="form-control">  
                        </div>
                                           
                    </div> 
                     <div class="action_btns" style="width:250px;">
                          <button type="submit"  class="cmn_btn">Upload</button>
                        </div> 
                    </form> 
             </div> 
 				  
		</div>
	</div>


</section>
 

@stop


{{-- page level scripts --}}
@section('footer_scripts')
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

 <script>
 $(".deleteRecord").click(function(){
    var id = $(this).data("id");
    var token = '{{ csrf_token() }}';
    //alert(id)
    $.ajax(
    {
        url: "deletePrescription/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (response){
        	if(response.status=='error'){
            		toastr['error']("Please insert image", );
                      window.location.reload();
                    }else{
                     toastr['success'](" Delete Successfully", "Delete");
                      window.location.reload();
                    }
           

        }
    });
   
});

</script>
@stop


 