@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
  

  <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop
{{-- Page content --}}
@section('content')
 
<!--                                   
 -->

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">My Orders</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                @include('frontend.user.leftpart')

                <div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                        <h2>My Orders</h2>
                        <div class="orders_blk cmn_area">
                          <div class="row">
                        @forelse($orders as $key=>$value) 
                        <?php // echo "<pre>";print_r($orders);die; ?>   
                         @php $order_status = $value->order_status; @endphp
                            <div class="col-sm-6">
                                <div class="order_details">
                                    <h3>{{$value->user->name;}}</h3>
                                    <span>₹ {{number_format($value->total_payed_amount,2)}}</span>
                                    @if($order_status=='N') 
                                    <p class="green">Delivered on {{ 
                                        date("d M Y", strtotime($value->delivered_date))
                                         }}</p>
                                     @endif    


                                    <ul>
                                            @php
                                             $count = 0;
                                             $new_ids = array(); 
                                            @endphp  
                                            @foreach($value->OrderItem as $key=>$value)
                                                @php
                                                 $count++; 
                                                $new_ids[]=$value->product_id.',';
                                                @endphp
                                                      @if($key< 2)    
                                                        <li><i><img src="{{asset('public/vendor/assets/images/red-cross.png')}}"></i> {{$value->ProductOrderItem->medicine_name??''}}</li>
                                                      @endif                               
                                            @endforeach
                                        <li><i><img src="images/red-cross.png"></i> + {{$count}} More Products.</li>
                                    </ul>
                                    <div class="action_btns">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a href="{{ url('/user/order-detail/'.encrypt($value->order_id)) }}"  class="cncl_btn">View  Details</a>
                                            </div>
                                          @if($order_status=='N')  
                                            <div class="col-sm-6">
                                                <button type="button" data-id="{{encrypt($value->order_id)}}" id="cn_order" style="background-color:#ad2929; border-color:#ad2929;" class="cmn_btn cancel_order">Cancel order</button>
                                            </div>  
                                          @else  
                                            <div class="col-sm-6">
                                                <button type="button" id="re-order" data-id='<?php echo  implode("",$new_ids); ?>' class="cmn_btn re-order">Reorder</button>
                                            </div>
                                          @endif  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <h6>Looks like you haven’t placed an order with us yet.</h6>
                        @endforelse
 
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<script type="text/javascript">

    var order_cancel ="{{URL::to('order-cancel')}}";
    var re_order ="{{URL::to('re-order')}}";
    var token       = '{{ csrf_token() }}';

 $(document).on('click', '#re-order', function() {
    var product_ids = $(this).attr("data-id");
    //alert(product_ids);
     event.preventDefault();
    $.ajax({
        url: re_order,
        type: "POST",
        data: {
            '_token': token,
            product_ids: product_ids
        },
        dataType: 'json',
        beforeSend: function() {
            $("#re-order").html('Validating...');
        },
        success: function(response) {
            $("#re-order").html('Reorder');
            toastr[response.status](response.message, "Reorder");
            setTimeout(function() {
               window.location = response.url;
            }, 1000);
        }
    });
});

 $(document).on('click', '#cn_order', function() {
    var order_id = $(this).attr("data-id");
     event.preventDefault();
    $.ajax({
        url: order_cancel,
        type: "POST",
        data: {
            '_token': token,
            order_id: order_id
        },
        dataType: 'json',
        beforeSend: function() {
            $(".re-order").html('Validating...');
        },
        success: function(response) {
            $(".re-order").html('Cancelled');
            toastr[response.status](response.message, "Order");
            setTimeout(function() {
                window.location.reload();
            }, 1000);
        }
    });
});
</script>

@stop


 