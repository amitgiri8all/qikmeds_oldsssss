
@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop 

{{-- Page content --}}
@section('content')
 
<!--                                   
 -->
<section class="middle_section p_30">
	<div class="container">
		<div class="breadcrumb_blk">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Account</li>
			  </ol>
			</nav>
		</div>
		<div class="product_listing_blk">
			<div class="row">
				   @include('frontend.user.leftpart')
				   <div class="col-lg-9 col-md-8">
					<div class="right_sidebar">
						<h2>Contact Us</h2>
			 <form class="ajax_form" id="contact_us" action="{{route('user.save-contact-us')}}" method="post"> 
						  	 		 @csrf			
						<div class="user_address_blk cmn_area">
						  <div class="row">
						  	
											<div class="col-sm-12">
												<div class="user_txt">
													<label>How would you like us to contact you?*</label>
													<div class="form-check form-check-inline">
													  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="mobile">
													  <label class="form-check-label" for="inlineRadio1">Mobile</label>
													</div>
													<div class="form-check form-check-inline">
													  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="email">
													  <label class="form-check-label" for="inlineRadio2">Email ID</label>
													</div>



													{{-- <div class="radio_btn">
														<input type="radio" name="radio" value="mobile" id="r-1" checked="checked">
														<label for="r-1">Mobile</label>
													</div>
													<div class="radio_btn">
														<input type="radio" name="radio" value="email" id="r-2">
														<label for="r-2">Email ID</label>
													</div> --}}
												</div>
											</div>
											<div class="form-group col-sm-8">
												<label>Purpose of Contact*</label>
												<select class="form-control" name="purpose_of_contact">
													<option disabled="" selected="" value="">Select the reason</option>
													@foreach($contactpurpose as $key=>$value)
														<option value="{{$value->id}}">{{$value->name}}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group col-sm-8">
												<label>Please state your message here*</label>
												<textarea class="form-control" rows="5" name="message" placeholder="Max 500 Characters"></textarea>
											</div>
											<div class="form-group col-sm-8">
												<div class="checkbox_btn">
													<input type="checkbox" name="checkbox" id="c-1">
 												</div>
											</div>
											<div class="form-group col-sm-12 mgn_0">
												<button type="submit" class="cmn_btn">Submit</button>
											</div>
										  </div>
										</div>
					</div>
				</div>
					    </form>					
				  
		</div>
	</div>
</section>
 

 

@stop


{{-- page level scripts --}}
@section('footer_scripts')
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>


@stop


 