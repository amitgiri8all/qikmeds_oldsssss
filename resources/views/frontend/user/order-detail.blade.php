@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 
@stop
 
{{-- Page content --}}
@section('content')
   
<section class="middle_section p_30">
    <div class="container">
        
        <div class="order_detail_blk">
            <div class="row">
                <div class="col-sm-9">
                    <div class="order_details_content">
                        <div class="order_delivery_info">
                            <h2>Order Status</h2>
                            <div class="delivered_date">
                                <h3><i><img src="{{asset('public/frontend/images/tick.png')}}" alt="tick"></i>
                                    @if(!empty($order->order_status))
                                    {{ 
                                      App\Helpers\Helper::$order_status[$order->order_status];
                                    }}
                                      @endif
                                     
                                 </h3>
                                <p>{{date('d F,Y h:i A',strtotime(@$order->created_at))}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="order_delivery_info">
                                    <h2>Order Details</h2>
                                    <div class="payment_details order_info">
                                        <ul>
                                            <li>
                                                <span>Customer Name</span>
                                                <span>{{$order->user->name??'';}}</span>
                                            </li>
                                            <li>
                                                <span>Order ID</span>
                                                <span>{{$order->order_code??'';}}</span>
                                            </li>
                                            <li>
                                                <span>Order Placed</span>
                                                <span>{{date('d F,Y h:i A',strtotime(@$order->created_at))}}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="order_delivery_info">
                                    <h2>Delivery Address</h2>
                                    <div class="address_blk">
                                        <h4>{{$order->deliveryaddress->name??'';}}</h4>
                                        <h5>{{$order->deliveryaddress->mobile_number??'';}}</h5>
                                        <span>{{$order->deliveryaddress->address_type??'';}}</span>
                                        <p>{{$order->deliveryaddress->address_delivery??'';}}<br> {{$order->deliveryaddress->city??'';}}, {{$order->deliveryaddress->state??'';}} - {{$order->deliveryaddress->pin_code??'';}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @if(!empty($removeproduct))    
                        <div class="order_delivery_info change" style="display:none;">
                            <h2>Change Request</h2>
                            <div class="order_name">
                                <div class="req_title">
                                    <span>Description</span>
                                    <span>Status</span>
                                </div>
                                <ul>
                                   @foreach($removeproduct as $key=>$value)  
                                    <li>
                                        <i><img src="{{asset('public/frontend/images/red-cross.png')}}"></i>
                                        <div class="medicine_detail">
                                            <h3>{{$value->ProductOrderItem->medicine_name}}</h3>
                                            <p><span>Quantity:</span> {{$value->quantity_order}}</p>
                                        </div>
                                        <h5 class="red">Removed</h5>
                                        <em>Price - ₹ {{$value->ProductOrderItem->mrp}}</em>
                                    </li>
                                    @endforeach
                                     @foreach($newproductadded as $key=>$value)  

                                     <input type="text" value="{{$value->product_id}}" name="product_id[]">

                                    <li>
                                        <i><img src="{{asset('public/frontend/images/red-cross.png')}}"></i>
                                        <div class="medicine_detail">
                                            <h3>   {{$value->ProductOrderItem->medicine_name}}</h3>
                                            <p><span>Quantity:</span> {{$value->quantity_order}}</p>
                                        </div>
                                        <h5 class="green">Added</h5>
                                        <em>Price - ₹ {{$value->ProductOrderItem->mrp}}</em>

                                    </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                     @endif   
                            
                        <div class="order_delivery_info">
                            <h2>Order</h2>
                            <div class="order_name">
                                <ul>
                                @if(isset($order) && !empty($order))
                                  @foreach($order->OrderItem as $key=>$value)  
                                    <li>
                                        <i><img src="{{asset('public/frontend/images/red-cross.png')}}"></i>
                                        <div class="medicine_detail">
                                            <h3>{{$value->ProductOrderItem->medicine_name}}</h3>
                                            <p><span>Quantity:</span> {{$value->quantity_order}}</p>
                                            <p><span>Seller:</span> Apollo Pharmacy, Gopalpura Bypass, Jaipur</p>
                                        </div>
                                        <h4>₹ {{$value->ProductOrderItem->mrp}}</h4>
                                    </li>
                                   @endforeach 
                                   @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                 @if(isset($order) && !empty($order))
                <div class="col-sm-3">
                    <div class="order_price_blk">
                        <h2>Payment Details</h2>
                        <div class="payment_details order_info">
                           
                            <ul>
                                <li>
                                    <span>Sub Total</span>
                                    <span>₹ {{$order->sub_total}}</span>
                                </li>
                                <li>
                                    <span>Qikmeds Discount</span>
                                    <span>₹ {{$order->discount_amount}}</span>
                                </li> 
                                @if(!empty($order->discount_amount))
                                <li>
                                    <span>Promo Discount</span>
                                    <span>₹ {{$order->discount_coupon_code}}</span>
                                </li>
                                @endif
                                <li>
                                    <span>Delivery Fee</span>
                                    <?php if($order->shipping_amount>0){?>
                                    <span>₹ {{$order->shipping_amount}}</span>
                                <?php }else{ ?>
                                    <span>Free</span>
                                    <?php  }?>
                                </li>
                                <li>
                                    <span>Net Amount Payable</span>
                                    <span>₹ {{$order->total_payed_amount}}</span>
                                </li>
                                <li>
                                    <span>Payment Mode</span>
                                    <span>{{ucfirst($order->orderpayment->payment_method)}}</span>
                                </li>
                            </ul>
                            <a type="button" download href="{{$order->invoice_pdf}}" class="invoice_btn">Download Invoice</a>
                        </div>

                        <div class="action_btns" style="display:none;">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="javascript::void(0);" class="cncl_btn">Cancel</a>
                                </div>
                                <?php
                                $order_id = encrypt($order->order_id);
                                ?>
                                <div class="col-sm-6">
                                         <a href="{{url('extra-product-payment/'.$order_id)}}" type="button" class="cmn_btn" style="color:#fff; width: 143px; text-align: center; margin-bottom: 24px;">Accept & Pay</a> 
                                   </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')



@stop


 