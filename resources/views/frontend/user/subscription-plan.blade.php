
@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop 

{{-- Page content --}}
@section('content')
 
<!--                                   
 -->
<section class="middle_section p_30">
	<div class="container">
		<div class="breadcrumb_blk">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.html">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Prescription</li>
			  </ol>
			</nav>
		</div>
		<div class="product_listing_blk">
			<div class="row">
				   @include('frontend.user.leftpart')
				  <div class="col-lg-9 col-md-8">
					<div class="right_sidebar">
						<div class="subscription_blk cmn_area">
							<h3>Qikmeds Premium</h3>
							<h4>Reduce your medical expenses with our premium membership benefits</h4>
						  <div class="row">
						  @foreach($subscription_meta as $key=>$val)	
						  	<div class="col-sm-4">
						  		<div class="subscription_benefts">
						  			<i><img src="{{$val->image}}"></i>
						  			<h5><!-- {{$val->price}}  -->{{$val->title}}</h5>
						  			<p>{{$val->description}}</p>
						  		</div>
						  	</div>
						  @endforeach	

						  @foreach($subscription_plan as $key=>$val)
							<div class="col-sm-12">
								<div class="card" style="background:#eef5fb; margin:20px 0px;">
								<div class="card-body">
									<div class="radio_btn" style="float:left;">
										 <input type="radio" value="{{$val->id}}" name="plan_id" id="r-{{$key}}">
										 <label for="r-{{$key}}">{{$val->duration}}</label>
 									</div>
									<div class="radio_btn" style="float:right;">
										<span ><img src="https://assets.pharmeasy.in/web-assets/dist/6aa9e076.png" style="position: absolute;">{{$val->discount}}Off </span> &nbsp;&nbsp;&nbsp;
										<span>Rs.{{$val->sale_price}} </span>&nbsp;&nbsp;&nbsp;
										<span class="strike">Rs. {{$val->price}}</span>
 									</div>
             								
                                </div>
								</div>
							</div>
						  @endforeach	

 						@if(session('cart'))
						  <div class="action_btns">
                                <button type="submit" id="add_plan" class="cmn_btn">Add To Cart</button>
                            </div>
						@else
						    <div class="action_btns">
                                <button type="submit" id="add_plan" class="cmn_btn">By Now</button>
                            </div>
						@endif



						  </div>
						</div>
					</div>
					 
				 
		</div>
	</div>
</section>
 

 
 

@stop


{{-- page level scripts --}}
@section('footer_scripts')
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script type="text/javascript">
	var add_subscription_plan="{{URL::to('add-subscription-plan')}}";

 var token = '{{ csrf_token() }}';
</script>
<script type="text/javascript">
	//Accepted All Orders
$(document).ready(function(){
    $('#add_plan').on('click',function(event){
        var plan_id =  $('input[name="plan_id"]:checked').val();
      //  alert(plan_id)
         event.preventDefault();
          $.ajax({
            url:  add_subscription_plan,
            type: "POST",
            data: {'_token': token,plan_id:plan_id},
            dataType: 'json',
            beforeSend : function() {
                  $("#add_plan").html('Please wait &nbsp;<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            success: function(response) {
                console.log(response.status)
                    if(response.status=='validation'){
                        toastr['error']('Please Select a Plan.', "Info");
                    }
                setTimeout(function(){
                 // $("#add_plan").html('Add To Cart');
                  toastr[response.status](response.message, "Info");
                   window.location.href = response.redirect_url;

                },1000);
                }
            });
              
        
    });
});
</script>
@stop


 