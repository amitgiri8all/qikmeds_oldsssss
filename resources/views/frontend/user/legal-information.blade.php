@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


@stop



{{-- Page content --}}
@section('content')
 
<!--                                   
 -->

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">legal Information</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                @include('frontend.user.leftpart')

     <div class="col-lg-9 col-md-8">
        <div class="right_sidebar">
            <h2>Legal Information</h2>
            <div class="orders_blk cmn_area">
              <div class="row">
               
               {!! $legal_information_data->description !!}
              </div> 
          </div>
        </div>
    </div>
</div> 
</div>
</div>
</div>
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')



@stop


 