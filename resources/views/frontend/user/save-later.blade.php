@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
@stop
{{-- Page content --}}
@section('content')
 
<!--                                   
 -->

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">My Orders</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                @include('frontend.user.leftpart')

                <div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                      <div id="countval"> <h2>Save for Later ({{count($product);}})</h2> </div>
                        <div class="wishlist_blk cmn_area">
                         <div id="free">  
                         <?php  //echo "<pre>";print_r($product);die;   ?>  
                             @foreach($product as $key=>$val)

                                            <div class="cart_item">
                                                <div class="item_img"><img src="{{ asset('storage/app/public/upload/Thumbnail/'.$val->ProductImage->image)  }}" alt="product" width    ="50" height="60">
                                                </div>
                                                <div class="item_info">
                                                    <h4>{{$val->Product->medicine_name}} - {{$val->Product->type_of_sell}}</h4>
                                                    {{-- <span>Mfr: Nestle India Ltd</span> --}}
                                                    <div class="prodct_price_blk">
                                                        <h3>₹ {{$val->Product->sale_price}}</h3>
                                                        <span class="strike">₹ {{$val->Product->mrp}}</span>
                                                        <small>{{$val->Product->discount}}% Off</small>
                                                    </div>
                                                </div>
                                                <div class="item_qty">
                                                    <button type="button" class="cmn_btn">Buy Now</button>
                                                    <a href="#" data-id="{{$val->id}}" class="remove-save-later" ><i class="fa fa-trash" title="Delete"></i></a>
                                                </div>
                                            </div>
                                @endforeach
                     
                        </div>   
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>

<script type="text/javascript">
var save_for_later_remove ="{{URL::to('user/save_for_later_remove')}}";
var token       = '{{ csrf_token() }}';

 $(document).on('click', '.remove-save-later', function() {
         id = $(this).attr("data-id");
          event.preventDefault();
          $.ajax({
            url: save_for_later_remove,
            type: "POST",
            data: {'_token': token,id:id},
            dataType: 'json',
            success: function(response) {
                    $("#free").load(document.URL + " #free");
                    $("#countval").load(document.URL + " #countval");
                    toastr[response.status](response.message, 'Product');
            }
            });
});

</script>
@stop


 