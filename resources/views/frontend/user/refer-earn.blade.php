@extends('frontend.layouts.default')
{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
@stop
{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Account</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
               
                @include('frontend.user.leftpart')

            <div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                        <h2>Refer &amp; Earn</h2>
                        <div class="refer_earn_blk cmn_area">
                          <div class="row">
                            @if(!empty($data))
                            <div class="col-sm-6">
                                <div class="refer_left">
                                    <h3>{{$data->title}}</h3>
                                   {!!$data->description!!}
                                    <h4>Share your code with friends</h4>
                                    <ul>
                                        <li><a href="https://api.whatsapp.com/send?text=www.google.com"><img src="images/whatsapp.png" alt="whatsapp"><span>whatsapp</span></a></li>
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://172.105.36.210/qikmeds.com&display=popup"><img src="images/facebook.png" alt="facebook"><span>Facebook</span></a></li>
                                        <li><a href="https://www.instagram.com/?hl=en"><img src="images/instagram.png" alt="instagram"><span>Instagram</span></a></li>
                                        <li><a href="https://twitter.com/login?lang=en"><img src="images/twitter.png" alt="twitter"><span>Twitter</span></a></li>
                                    </ul>
                                    <div class="referral_blk">
                                        <h5>YOUR REFERRAL CODE</h5>
                                        <span>{{Auth::guard('web')->user()->referrer_code;}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="refer_right">
                                    <img src="{{$data->image}}" alt="{{$data->title}}">
                                </div>
                            </div>
                            @endif
                          </div>

                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
@stop


 