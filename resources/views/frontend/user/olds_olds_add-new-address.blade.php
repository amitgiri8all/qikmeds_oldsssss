@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')


@stop



{{-- Page content --}}
@section('content')
 
<!--                                   
 -->

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Account</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
               
                @include('frontend.user.leftpart')

                <div class="col-lg-9 col-md-8">
                    <div class="right_sidebar">
                        <h2>Add New Address</h2>
                             <form class="ajax_form" id="delivery_address" action="{{route('user.save-delivery-address')}}" method="post">                    
        @csrf
                        <div class="user_address_blk cmn_area">
                          <div class="row">
                             <div class="form-group col-sm-12">
                                <button class="cmn_btn" type="button"><i class="ti-target"></i> Use my Current Location</button>
                            </div>
                        
                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="{{old('name')}}" name="name" placeholder="Name">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control num" value="{{old('mobile_number')}}" name="mobile_number" placeholder="10 - digit mobile number">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control num" value="{{old('pin_code')}}" name="pin_code" placeholder="Pincode">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="{{old('locality')}}" name="locality" placeholder="Locality">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <textarea class="form-control" rows="5" value="{{old('address_delivery')}}" name="address_delivery" placeholder="Address (Area and Street)"></textarea>
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="{{old('city')}}" name="city" placeholder="City/District/Town">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="{{old('state')}}" name="state" placeholder="State">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" name="landmark" value="{{old('landmark')}}" class="form-control" placeholder="Landmark(Optional)">
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <input type="text" class="form-control" value="{{old('alternate_phone')}}" name="alternate_phone" placeholder="Alternate Phone (Optional)">
                                    </div>


                            <div class="col-sm-12">
                                <div class="user_txt">
                                    <label>Address Type</label>
                                    <div class="radio_btn">
                                        <input type="radio" name="address_type" id="r-1" value="home" checked="checked">
                                        <label for="r-1">Home</label>
                                    </div>
                                    <div class="radio_btn">
                                        <input type="radio" value="work" name="address_type" id="r-2">
                                        <label for="r-2">Work</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-12 mgn_0">
                                <button type="submit" class="cmn_btn">Save</button>
                                <button type="submit" class="cncl_btn">Cancel</button>
                            </div>



                           </div>
                        </div>
                      </form>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 
@stop


{{-- page level scripts --}}
@section('footer_scripts')


@stop


 