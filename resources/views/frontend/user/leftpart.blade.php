 
<div class="col-lg-3 col-md-4">
   <div class="left_sidebar">
      <div class="profile_box_detail">
         <div class="user_profile_box">
            <div class="user_profile_img"><img src="{{Auth::guard('web')->user()->image}}"></div>
            <div class="user_profile_name">
               <h2>{{Auth::guard('web')->user()->name}}</h2>
               <span>{{Auth::guard('web')->user()->email}}<br>{{Auth::guard('web')->user()->mobile_number}}</span>
            </div>
         </div>
      </div>
      <div class="profile_menu">
         <ul>
            <li><a href="{{route('user.profile')}}" class="{{ Request::routeIs('user.profile') ? 'active' : '' }}"><i class="ti-user"></i>My profile <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.orders')}}" class="{{ Request::routeIs('user.orders') ? 'active' : '' }}"><i class="ti-shopping-cart"></i>My Orders <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.addresses')}}" class="{{ Request::routeIs('user.addresses') ? 'active' : '' }}"><i class="ti-location-pin"></i>Delivery Addresses <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.prescription')}}" class="{{ Request::routeIs('user.prescription') ? 'active' : '' }}"><i class="ti-support"></i>My Prescription <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.patient-manage')}}" class="{{ Request::routeIs('user.patient-manage') ? 'active' : '' }}"><i class="ti-support"></i>Manage Patient <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.subscription')}}" class="{{ Request::routeIs('user.subscription') ? 'active' : '' }}"><i class="ti-receipt"></i>My Subscription <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.payment-method')}}" class="{{ Request::routeIs('user.payment-method') ? 'active' : '' }}"><i class="ti-credit-card"></i>Payment Methods <i class="arrow ti-angle-right"></i></a></li>
             <li><a href="{{route('user.notification')}}" class="{{ Request::routeIs('user.notification') ? 'active' : '' }}"><i class="ti-bell"></i>Notification <i class="arrow ti-angle-right"></i></a></li>
            <li><a  href="{{route('user.wallet')}}" class="{{ Request::routeIs('user.wallet') ? 'active' : '' }}"><i class="ti-wallet"></i>My Wallet <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.save-for-later')}}" class="{{ Request::routeIs('user.save-for-later') ? 'active' : '' }}"><i class="ti-heart"></i>Save for Later <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.wishlist')}}" class="{{ Request::routeIs('user.wishlist') ? 'active' : '' }}"><i class="ti-heart"></i>Wishlist <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.refer-earn')}}" class="{{ Request::routeIs('user.refer-earn') ? 'active' : '' }}"><i class="ti-gift"></i>Refer & Earn <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('user.legal-information')}}" class="{{ Request::routeIs('user.legal-information') ? 'active' : '' }}"><i class="ti-notepad"></i>Legal Information <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('contact-us')}}" class="{{ Request::routeIs('contact-us') ? 'active' : '' }}"><i class="ti-headphone"></i>Contact Us <i class="arrow ti-angle-right"></i></a></li>
            <li><a href="{{route('logout.post')}}"><i class="ti-export"></i>Logout <i class="arrow ti-angle-right"></i></a></li>
         </ul>
      </div>
   </div>
</div>
