@extends('frontend/vendor/layouts/default')
@section('content')
@section('header_styles')

  <!-- Style to set the size of checkbox -->
    <style>
        input.checkbox {
            width: 20px;
            height: 40px;
        }
         input.check {
            width: 20px;
            height: 40px;
        }
    </style>
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap4.min.css">
@stop    
 
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Validate Order</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">Validate Order</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                         <?php // echo "<pre>";print_r($order);die; ?>
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="order_delivery_info">
                                            <h2>Order Details</h2>
                                            <div class="payment_details">
                                                <ul>
                                                    <li>
                                                        <span>Customer Name</span>
                                                        <span>{{$order->user->name;}}</span>
                                                    </li>
                                                    <li>
                                                        <span>Order ID</span>
                                                        <span>{{$order->order_code}}</span>
                                                    </li>
                                                    <li>
                                                        <span>Order Placed </span>
                                                        <span>
                               {{
                                date('M,d-Y', strtotime($order->delivered_date));}}

                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="order_delivery_info">
                                            <h2>Delivery Address</h2>
                                            <div class="address_blk">
                                                <h4>{{$order->deliveryaddress->name}}</h4>
                                                <h5>{{$order->deliveryaddress->mobile_number}}</h5>
                                                <span>{{$order->deliveryaddress->address_type}}</span>
                                                <p>{{$order->deliveryaddress->address_delivery}}<br>
                                                    {{$order->deliveryaddress->city}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="order_price_blk">
                                        <h2>Payment Details</h2>
                                        <div class="payment_details">
                                            <ul>
                                                <li>
                                                    <span>Payment Mode</span>
                                                    <span>{{ucfirst($order->orderpayment->payment_method)}}</span>
                                                </li>
                                                <li>
                                                    <span>Delivery Fee</span>
                                                    <span>{{$order->shipping_amount == 0.00 ? "Free" : $order->shipping_amount }}</span>
                                                </li>
                                                <li>
                                                    <span>Order Total</span>
                                                    <span>₹ {{$order->total_payed_amount}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-8 order_val">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                           <!--  <table id="datatable" class="table table-centered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th style="width: 20px;">
                                                            <div class="checkbox-wrapper-mail">
                                                                <input type="checkbox" id="ordercheck">
                                                                <label for="ordercheck" class="toggle"></label>
                                                            </div>
                                                        </th>
                                                        <th>Item</th>
                                                        <th>Item Desc</th>
                                                        <th>Type</th>
                                                        <th>Quantity</th>
                                                        <th>Price</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                
                                            </table> -->
                                            <table id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; border: none; width: 100%;">
                                                <thead>
                                                  <tr>
                                                    <th style="width: 20px;">
                                                       <input type="checkbox" class="check" id="select_all" value="0" />
                                                    </th>
                                                    <th>Item</th>
                                                    <th>Item Desc</th>
                                                    <th>Type</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <th>Status</th>
                                                 </tr>
                                                </thead>
                                                <tbody> 
                                        @foreach($order->OrderItemVendorNull as $key=>$value)  
                                             
                                            @php $vendor_id = Auth::guard('vendor')->user()->id;
                                               $data =  App\Models\DeclineProducts::remove_decline_order_products($value->ord_item_id,$vendor_id,$order->id);
                                            $imagedata =json_decode($value->data);
                                             @endphp  
                                             @if($data)
                                                    <tr>
                                                        <td>
                                                           @if($value->vendor_id == $vendor_id) 
                                            }
                         <div class="badge badge-soft-success font-size-12"><i class="fa fa-check-circle"></i></div>
                @else
                    <input type="checkbox" name="order" class="checkbox" value="{{$value->ord_item_id}}" />
                @endif

                                                         </td>
                                                        <td><img src="{{$imagedata->image??''}}" alt=""></td>
                                                        <td>{{$value->ProductOrderItem->medicine_name??''}}</td>
                                                        <td>{{$value->ProductOrderItem->type_of_sell??''}}</td>
                                                        <td>{{$value->quantity_order??''}}</td>
                                                        <td>₹ {{$value->ProductOrderItem->mrp??''}}</td>
                                                        <td>
                                                            <div class="badge badge-soft-success font-size-12">In Stock</div>
                                                        </td>
                                                    </tr>
                                                @endif    
                                        @endforeach          
                                                </tbody>
       
                                                    </table>
                                        </div>
                                    </div>
                                </div>
                               <div class="action_btns">
                                    <button type="button" id="decline" data-id="{{$order->id}}" class="btn cmn_btn btn-danger waves-effect waves-light">Decline</button>

                                    <a href="javascript:void(0)" data-id="{{$order->id}}"  id="accepted" class="btn cmn_btn btn-success waves-effect waves-light loaded_btn">Accept</a>

                                 </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="prescription_img"><img src="{{asset('public/vendor/assets/images/prescription.jpg')}}" alt="prescription"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-7">
                                                    <label>Customer Notes</label>
                                                    <textarea class="form-control" rows="3" disabled >{{$order->customer_notes ?? '';}}</textarea>
                                                </div>
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                 Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 
<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap4.min.js"></script>

 <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable();
} );
var accepted_url ="{{URL::to('vendor/accepte-orders')}}";
var decline_url ="{{URL::to('vendor/decline-orders')}}";

var token       = '{{ csrf_token() }}';



$(document).ready(function(){
    $('#select_all').on('click',function(){
        $('.checkbox:checked').each(function() {
         // alert($(this).val());
        });
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    $('.checkbox').on('click',function(){
       // alert('select_all')
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });


    $('.checkbox:checked').each(function() {
       // alert($(this).val());
    });


});

//Accepted All Orders
$(document).ready(function(){
    $('#accepted').on('click',function(){
        $("#accepted").html("Submitting...");
         $("#overlay").fadeIn(500);　
         order_id = $(this).attr("data-id");
         //alert(order_id)
         if($('.checkbox').is(":checked")){
           var ord_item_id=[];
             $("input:checkbox[name=order]:checked").each(function(){
                  ord_item_id.push($(this).val());
                });
             event.preventDefault();
          $.ajax({
            url:  accepted_url,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
            dataType: 'json',
            success: function(response) {
                console.log(response.url);
             setTimeout(function(){
             $("#overlay").fadeOut(1000);
               $("#accepted").html("Accepted");
               $('#decline').hide();
               $('#accepted').hide();
               $("#datatable").load(document.URL + " #datatable");
               toastr[response.status](response.message, "Order");
            },1000);
             window.location.replace(response.url);
            }
            });
              
        }else{
          $("#accepted").html("Accept");
          toastr['success']('Checkbox is not checked', "Order");
          //toastr['error']('Checkbox is not checked', "Order");
          $("#overlay").removeClass("has-error"); 
          $("#overlay").fadeOut(1000);

        }
    });
});

//Decline_orders Products All Orders
$(document).ready(function(){
    $('#decline').on('click',function(){
             $("#overlay").fadeIn(500);　
           order_id = $(this).attr("data-id");
          if($('.checkbox').is(":checked")){
           var ord_item_id=[];
             $("input:checkbox[name=order]:checked").each(function(){
                  ord_item_id.push($(this).val());
                });
             event.preventDefault();

          $.ajax({
            url:  decline_url,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
            dataType: 'json',
            success: function(response) {
                alert(response.url)
                console.log(response.url)
                 setTimeout(function(){
                 $("#overlay").fadeOut(1000);
                     $("#datatable").load(document.URL + " #datatable");
                   toastr[response.status](response.message, "Order");
                   window.location.href = response.url;
                },1000);
               //////  window.location.replace(response.url);
                 

            }
            });
              
        }else{
         alert("checkbox is not checked");
         location.reload();
        }
    });
});

</script>
@stop
