@extends('frontend/vendor/layouts/default')
@section('content')
@section('header_styles')

<!-- Style to set the size of checkbox -->
<style>
input.checkbox {
width: 20px;
height: 40px;
}
input.check {
width: 20px;
height: 40px;
}
</style>
<link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap4.min.css">

<link href="{{asset('public/vendor/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

@stop    

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

<div class="page-content">
<div class="container-fluid">

<!-- start page title -->
<div class="row">
<div class="col-12">
    <div class="page-title-box d-flex align-items-center justify-content-between">
        <h4 class="mb-0">Accept Orders</h4>

        <div class="page-title-right">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                <li class="breadcrumb-item active">Accept Orders</li>
            </ol>
        </div>

    </div>
</div>
</div>
<!-- end page title -->


<div class="row">
<div class="col-xl-12 order_val">
    <div class="card">
        <div class="card-body">
           <form  method="get">
 

<div class="row">

      <label for="example-text-input" class="col-auto col-form-label">Select Date</label>
      <div class="col-md-4">
         
            <input type="date" class="form-control" name="start">
            <input type="date" class="form-control" name="end">
        
      </div>
      <div >
          <button type="submit" value="submit" class="btn btn-primary">Submit</button> 
        
      </div>
</div>

    </form>
{{--    <form  method="get">
    <label for="example-text-input" class="col-auto col-form-label">Select Date</label>
    <table>
        <tr>
            <input type="date" class=" col-sm-4 form-control" name="start">
            <input type="date" class=" col-sm-4 form-control" name="end">
            <td><input type="submit" value="Submit"></td>
        </tr>

    </table>
</form>  --}}
    
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; border: none; width: 100%;">
                    <thead>
                  <tr class="filters">
                     <th>ID</th>
                     <th>Customer Name</th>
                     <th><i class="fa fa-phone" aria-hidden="true" title="SubTotal"></i></th>
                     <th>Total</th>
                     <th>Created At</th>
                    <th>Actions</th>
                  </tr>
               </thead>
                    <tbody>
                         @foreach($vendor_orders as $key=>$value)
                            <tr>
                               @php $val = App\Helpers\Helper::get_vendor_product_status($value->id); @endphp

                                <td>{{++$key}}</td>
                                 <td>{{@$value->user->name}}</td>
                                <td>{{@$value->user->mobile_number}}</td>
                                <td>₹ {{@$value->total_payed_amount}}</td>
                                <td>{{date('d-m-Y', strtotime($value->updated_at));}} - {{date('h:i A', strtotime($value->updated_at));}}</td> 
                               
                                <td> <a href="{{ url('/vendor/accept-order-detail/'.encrypt($value->id)) }}"  class="btn btn-primary waves-effect waves-light">View Order Details</a>
                                    
                                </td>
                         </tr>
                        @endforeach    
                    </tbody>
                   </table>
            </div>
        </div>
    </div>
  
</div>

</div>

</div> 
<!-- container-fluid -->
</div>
<!-- End Page-content -->

<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12">
     Copyright © 2021 Qikmeds. All Rights Reserved.
</div>
</div>
</div>
</footer>
</div>
<!-- end main content-->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{asset('public/vendor/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
  <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
  <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap4.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
$('#datatable').DataTable();
} );
</script>


<script type="text/javascript">
var updateVendorOrderProductStatus ="{{URL::to('vendor/updateVendorOrderProductStatus')}}";
var token       = '{{ csrf_token() }}';

 $(document).on('click', '.change_status', function() {
         id = $(this).attr("data-id");
         //alert(id)
          event.preventDefault();
          $.ajax({
            url: updateVendorOrderProductStatus,
            type: "POST",
            data: {'_token': token,id:id},
            dataType: 'json',
            success: function(response) {
                    $("#datatable").load(document.URL + " #datatable");
                    toastr[response.status](response.message, 'Order');
            }
            });
});

</script>


@stop





{{-- page level scripts --}}
@section('footer_scripts')

@stop


