@extends('frontend/vendor/layouts/default')
@section('content')
@section('header_styles')

  <!-- Style to set the size of checkbox -->
    <style>
        input.checkbox {
            width: 20px;
            height: 40px;
        }
         input.check {
            width: 20px;
            height: 40px;
        }
    </style>
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap4.min.css">
@stop    
 
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Accept Order Details </h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">Accept Order Details</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                        
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="order_delivery_info">
                                            <h2>Order Details</h2>
                                            <div class="payment_details">
                                                <ul>
                                                    <li>
                                                        <span>Customer Name</span>
                                                        <span>{{$order->user->name ?? '';}}</span>
                                                    </li>
                                                    <li>
                                                        <span>Order ID</span>
                                                        <span>{{$order->order_code ?? '';}}</span>
                                                    </li>
                                                    <li>
                                                        <span>Order Placed</span>
                                                        <span>{{date('d F,Y ',strtotime($order->created_at))}}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $delivery_address_info =json_decode($order->delivery_address_info,true);
                                    //echo "<pre>"; print_r($delivery_address_info); die;
                                     ?>
                                    <div class="col-xl-4">
                                        <div class="order_delivery_info">
                                            <h2>Delivery Address</h2>
                                            <div class="address_blk">
                                                <h4>{{$delivery_address_info['name']??'';}}</h4>
                                                <h5>{{$delivery_address_info['mobile_number']??'';}}</h5>
                                                <span>{{$delivery_address_info['address_type']??'';}}</span>
                                                <p>{{$delivery_address_info['address_delivery']??'';}}<br> {{$delivery_address_info['city']??'';}}, {{$delivery_address_info['state']??'';}} - {{$delivery_address_info['pin_code']??'';}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="order_price_blk">
                                        <h2>Payment Details</h2>
                                        <div class="payment_details">
                                            <ul>
                                                <li>
                                                    <span>Payment Mode</span>
                                                    <span>{{$order->OrderPayment->payment_method??'';}}</span>
                                                </li>
                                                <li>
                                                    <span>Delivery Fee</span>
                                                    <span>Free</span>
                                                </li>
                                                <li>
                                                    <span>Order Total</span>
                                                    <span>₹ {{$order->total_payed_amount??'';}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-8 order_val">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                           <!--  <table id="datatable" class="table table-centered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th style="width: 20px;">
                                                            <div class="checkbox-wrapper-mail">
                                                                <input type="checkbox" id="ordercheck">
                                                                <label for="ordercheck" class="toggle"></label>
                                                            </div>
                                                        </th>
                                                        <th>Item</th>
                                                        <th>Item Desc</th>
                                                        <th>Type</th>
                                                        <th>Quantity</th>
                                                        <th>Price</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                
                                            </table> -->
                                            <table id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; border: none; width: 100%;">
                                                <thead>
                                                  <tr>
                                                    <th style="width: 20px;">
                                                       #
                                                    </th>
                                                    <th>Item</th>
                                                    <th>Item Desc</th>
                                                    <th>Type</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                        @foreach($order->OrderItem as $key=>$value)   
                                            @php $vendor_id = Auth::guard('vendor')->user()->id;

                                               $data =  App\Models\DeclineProducts::remove_decline_order_products($value->ord_item_id,$vendor_id,$order->order_id);
                                               $imagedata =json_decode($value->data);
                                             @endphp  
                                             @if($data)
                                                    <tr>
                                                        <td>
                                                           @if($value->vendor_id == $vendor_id) 
                         <div class="badge badge-soft-success font-size-12"><i class="fa fa-check-circle"></i></div>
                @else
                    <input type="checkbox" name="order" class="checkbox" value="{{$value->ord_item_id}}" />
                @endif

                                                         </td>
                                                        <td><img src="{{$imagedata->image}}" alt=""></td>
                                                        <td>{{$value->ProductOrderItem->medicine_name}}</td>
                                                        <td>{{$value->ProductOrderItem->type_of_sell}}</td>
                                                        <td>{{$value->quantity_order}}</td>
                                                        <td>₹ {{$value->ProductOrderItem->mrp}}</td>
                                                         
                                                    </tr>
                                                @endif    
                                        @endforeach          
                                                </tbody>
       
                                                    </table>
                               @php $val = App\Helpers\Helper::get_vendor_product_status($order->id); @endphp


                                        </div>
                                    </div>
                                </div>
                                 <div class="action_btns" id="fulfilled">
 
                                   @if($val->status=='accept')
                                    <button type="button" data-id="{{$val->id}}" class="btn btn-info change_status">Fulfilled</button>

                                    @else
                                    <div class="badge badge-soft-success font-size-14">{{ucfirst($val->status)}}</div>

                                   @endif 
                                 </div>
                              
                            </div>
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="prescription_img"><img src="{{asset('public/vendor/assets/images/prescription.jpg')}}" alt="prescription"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                 Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->
@stop
{{-- page level scripts --}}
<!-- @section('footer_scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 
<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap4.min.js"></script> -->


@section('footer_scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 
<script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>

<!-- apexcharts -->
<script src="{{asset('public/vendor/assets/libs/apexcharts/apexcharts.min.js')}}"></script>


<!-- Required datatable js -->
<script src="{{asset('public/vendor/assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script href="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

<!-- Responsive examples -->
<script src="{{asset('public/vendor/assets/libs/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>

<script src="{{asset('public/vendor/assets/libs/datatables.net-bs4/js/responsive.bootstrap4.min.js')}}"></script>

<script src="{{asset('public/vendor/assets/js/pages/dashboard.init.js')}}"></script>

<script src="{{asset('public/vendor/assets/js/app.js')}}"></script>





<script type="text/javascript">
var updateVendorOrderProductStatus ="{{URL::to('vendor/updateVendorOrderProductStatus')}}";
var token       = '{{ csrf_token() }}';

 $(document).on('click', '.change_status', function() {
         id = $(this).attr("data-id");
         //alert(id)
          event.preventDefault();
          $.ajax({
            url: updateVendorOrderProductStatus,
            type: "POST",
            data: {'_token': token,id:id},
            dataType: 'json',
            success: function(response) {
                    $("#fulfilled").load(document.URL + " #fulfilled");
                    toastr[response.status](response.message, 'Order');
            }
            });
});

</script>
@stop
