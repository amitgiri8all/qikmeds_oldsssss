@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Vendor Register ::Qikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
@stop
{{-- Page content --}}
@section('content')
 
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Vendor Register</li>
              </ol>
            </nav>
        </div>
        <div class="container">
   <form method="post" id="vendorform" class="ajax_form" action="{{url('vendor/account/signup')}}">
      @csrf
      <h2 class="text-center">Join Our Network</h2>
      <div class="row jumbotron">

         <div class="col-sm-6 form-group">
            <label for="name-f">First Name</label>
            <input type="text" class="form-control" value="{{old('first_name')}}" name="first_name" id="first_name" placeholder="Enter your first name." >
         </div>

         <div class="col-sm-6 form-group">
            <label for="name-l">Last name</label>
            <input type="text" class="form-control" value="{{old('last_name')}}" name="last_name" id="first_name" placeholder="Enter your last name." >
         </div>

         <div class="col-sm-6 form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control"  value="{{old('email')}}"  name="email" id="email" placeholder="Enter your email." >
         </div>

         <div class="col-sm-6 form-group">
            <label for="address-1">Address</label>
            <input type="address" class="form-control" value="{{old('address')}}"  name="address" id="address" placeholder="Locality/House/Street no." >
         </div>
         
         <div class="col-sm-6 form-group">
            <label for="State">City</label>
            <input type="address" class="form-control" value="{{old('city')}}" name="city" id="city" placeholder="Enter your state name." >
         </div> 

          <div class="col-sm-6 form-group">
            <label for="State">State</label>
            <input type="address" class="form-control" value="{{old('state')}}" name="state" id="state" placeholder="Enter your state name." >
         </div>
         
         <div class="col-sm-6 form-group">
            <label for="Country">Country</label>
          <input type="address" class="form-control" value="{{old('country')}}" name="country" id="country" placeholder="Enter your country name." >
         </div>
           <div class="col-sm-6 form-group">
            <label for="Country">Vendor Firm Name</label>
          <input type="address" class="form-control" value="{{old('farm_name')}}" name="farm_name" id="farm_name" placeholder="Enter your farm name." >
         </div>
          
         <div class="col-sm-6 form-group">
            <label for="tel">Phone</label>
            <input type="text" name="mobile_number" value="{{old('mobile_number')}}" class="form-control num" maxlength="10" id="mobile_number" placeholder="Enter Your Contact Number." >
         </div>

         <div class="col-sm-6 form-group">
            <label for="zip">Postal-Code</label>
            <input type="zip" class="form-control" value="{{old('pin_code')}}" name="pin_code" id="pin_code" placeholder="Postal-Code." >
         </div>

         <div class="col-sm-6 form-group">
            <label for="pass">Password</label>
            <input type="Password" name="password" class="form-control"  value="{{old('password')}}"  id="pass" placeholder="Enter your password." >
         </div>

         <div class="col-sm-6 form-group">
            <label for="pass2">Confirm Password</label>
            <input type="Password" name="confirm-password" class="form-control" id="pass2" placeholder="Re-enter your password." >
         </div>   

         <div class="col-sm-6 form-group">
            <label for="pass2">Vendor Image</label>
            <input type="file" name="image" onchange="loadFile(event)"  class="form-control">
            <p><img id="output" width="200" /></p>
         </div>    


          <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                     <label for="text">Address Location</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="address_name_vendor" placeholder="Address name" class="form-control" name="address_location">
                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  
                  <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                     <label for="text">Latitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="lat" readonly placeholder="Address name" class="form-control" name="latitute">
                        {!! $errors->first('lat', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  

                  <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                     <label for="text">Longitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="long" readonly placeholder="Longitute name" class="form-control" name="longitute">
                        {!! $errors->first('lng', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>


                  <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                      <div class="input-group col-xs-12">
                  <div id="us3New" style="width: 100%; height: 300px;"></div>
                        </span>
                     </div>
                  </div> 


      

         <div class="col-sm-12">
            <input type="checkbox" class="form-check d-inline" id="chb" ><label for="chb" class="form-check-label">&nbsp;I accept all terms and conditions.
            </label>
         </div>
         <div class="col-sm-12 form-group mb-0">
            <button type="submit"  class="btn btn-primary float-right">Submit</button>
         </div>
      </div>
   </form>
</div>
    </div>
</section>        

@stop
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
<script>
var loadFile = function(event) {
   var image = document.getElementById('output');
   image.src = URL.createObjectURL(event.target.files[0]);
};
</script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
<script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>


<script type="text/javascript">
            $('#us3New').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#long'),
                    //radiusInput: $('#us3-radius'),
                    locationNameInput: $('#address_name_vendor')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script>
@stop
