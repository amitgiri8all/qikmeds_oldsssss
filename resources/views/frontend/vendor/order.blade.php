@extends('frontend/vendor/layouts/default')
@section('content')
@section('header_styles')

 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop    

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Orders</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                <li class="breadcrumb-item active">Orders</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- Nav tabs -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="nav nav-pills nav-justified" role="tablist">
                                        <li class="nav-item waves-effect waves-light">
                                            <a class="nav-link active" data-toggle="tab" href="#nearby" role="tab">
                                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                <span class="d-none d-sm-block">Nearby Orders</span> 
                                            </a>
                                        </li>
                                        <li class="nav-item waves-effect waves-light">
                                            <a class="nav-link" data-toggle="tab" href="#city" role="tab">
                                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                <span class="d-none d-sm-block">City Orders</span> 
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="nearby" role="tabpanel">
            <div class="new_order">
                <div class="row">
 @foreach($vendor_orders as $key=>$val)  
            @php $vendor_id = Auth::guard('vendor')->user()->id;
            $data =  App\Models\DeclineProducts::remove_order_by_vendor($vendor_id,$val->id);
            @endphp  
            @if($data)
                    <div class="col-sm-6">
                        <div class="order_details">
                            <div class="order_info">
                                <div class="left_txt">Order ID - {{$val->order_code;}} <span></span></div>
                                <div class="right_txt">Order Date - <span>{{date('d-m-Y', strtotime($val->created_at));}} - {{date('h:i A', strtotime($val->created_at));}}</span></div>
                            </div>
                            <h3>{{$val->user->name;}} <em>({{$val->user->mobile_number;}})</em></h3>
                            <span class="price_tag">₹ {{number_format($val->total_payed_amount,2)}} </span>
                            <p>{{$val->deliveryaddress->address_delivery}} {{$val->deliveryaddress->mobile_number}}, {{$val->deliveryaddress->city}}, {{$val->deliveryaddress->state}} - {{$val->deliveryaddress->pin_code}}</p>
                            <ul>
                              @php $count = 0;  @endphp  
                              @foreach($val->OrderItem as $key=>$value)
                                @php 
                                   $count++; 
                                @endphp

                                 @if($key< 2)    
                                <li><i><img src="{{asset('public/vendor/assets/images/red-cross.png')}}"></i> {{$value->ProductOrderItem->medicine_name}}</li>
                               @endif                               
                              @endforeach

                               see more  <span class="green">(Available : {{$count}})</span> <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg_{{$val->id}}"><i class="ri-todo-line"></i>See Prescription</a>  
                            </ul>
                            <div class="order_btm">
                                <p>Payment - <span>{{ucfirst($val->orderpayment->payment_method)}}</span></p>
                                <div class="action_btns">
                                    <button  type="button" id="decline" data-id="{{$val->id}}" class="decline btn btn-danger waves-effect waves-light">Remove</button>
                                    <a href="{{ url('/vendor/order-validate/'.encrypt($val->id)) }}"  class="btn btn-primary waves-effect waves-light">Validate</a>
                                     <!-- <a href="javascript::void(0)" data-id="{{$val->id}}"  id="accepted" class="accepted btn cmn_btn btn-success waves-effect waves-light loaded_btn">Accept</a> -->
                                </div>
                            </div>
                        </div>
                                <!-- Prescription Model -->
                                            <div class="modal fade bs-example-modal-lg_{{$val->id}} prescription" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                      <h5 class="modal-title mt-0" id="myModalLabel">Prescription</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                  </div>

                                                  <div class="modal-body">
                                                      <div class="prescription_img">
                         <?php
                     $pre_id = $val->user_id;
                     $prescription_img_data = 
                      App\Helpers\Helper::prescription_img($pre_id);
                                               

                                          ?>
                                           @php
                     if(!empty($prescription_img_data->image)){
                     $url = $prescription_img_data->image;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="700" height="800" alt="prescription" >

                                                        </div>
                                                  </div>
                                              </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                            </div>
                    </div>
                    @endif
                  @endforeach                                   
                </div>
            </div>
                                </div>
                                <div class="tab-pane" id="city" role="tabpanel">
                                                                       <div class="new_order">
                                        <div class="row">
                         @foreach($vendor_orders_city_quee as $key=>$val)  
                                    @php  $vendor_id = Auth::guard('vendor')->user()->id;
                                    $data =  App\Models\DeclineProducts::remove_order_by_vendor($vendor_id,$val->id);
                                    @endphp  
                                    <?php // echo "<pre>"; print_r($vendor_orders_city_quee);die; ?>
                                    @if($data)
                                            <div class="col-sm-6">
                                                <div class="order_details">
                                                    <div class="order_info">
                                                        <div class="left_txt">Order ID - {{$val->order_code;}} <span></span></div>
                                                        <div class="right_txt">Order Date - <span>{{date('d-m-Y', strtotime($val->created_at));}} - {{date('h:i A', strtotime($val->created_at));}}</span></div>
                                                    </div>
                                                    <h3>{{@$val->user->name;}} <em>({{@$val->user->mobile_number;}})</em></h3>
                                                    <span class="price_tag">₹ {{$val->sub_total;}} </span>
                                                    <p>{{@$val->deliveryaddress->address_delivery}} {{@$val->deliveryaddress->mobile_number}}, {{@$val->deliveryaddress->city}}, {{@$val->deliveryaddress->state}} - {{@$val->deliveryaddress->pin_code}}</p>
                                                     <ul>
                                                      @php $count = 0;  @endphp  
                                                      @foreach($val->OrderItem as $key=>$value)

                                                        @php 
                                                           $count++; 
                                                        @endphp

                                                         @if($key< 2)    
                                                        <li><i><img src="{{asset('public/vendor/assets/images/red-cross.png')}}"></i> {{$value->ProductOrderItem->medicine_name??''}}</li>
                                                       @endif                               
                                                      @endforeach

                                                       see more  <span class="green">(Available : {{$count}})</span> <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg_{{$val->id}}"><i class="ri-todo-line"></i>See Prescription</a>  
                                                    </ul>
                                                    <div class="order_btm">
                                                        <p>Payment - <span>{{ucfirst($val->orderpayment->payment_method)}}</span></p>
                                                        <div class="action_btns">
                                                            <button  type="button" id="decline" data-id="{{$val->id}}" class="decline btn btn-danger waves-effect waves-light">Remove</button>
                                                            <a href="{{ url('/vendor/order-validate/'.encrypt($val->id)) }}"  class="btn btn-primary waves-effect waves-light">Validate</a>
                                                             <!-- <a href="javascript::void(0)" data-id="{{$val->id}}"  id="accepted" class="accepted btn cmn_btn btn-success waves-effect waves-light loaded_btn">Accept</a> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                 
                                                    
                                           <!-- Prescription Model -->
                                            <div class="modal fade bs-example-modal-lg_{{$val->id}} prescription" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                      <h5 class="modal-title mt-0" id="myModalLabel">Prescription</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                  </div>

                                                  <div class="modal-body">
                                                      <div class="prescription_img">
                                        <?php
                                          $pre_id = $val->user_id;
                                           $prescription_img_data = 
                                               App\Helpers\Helper::prescription_img($pre_id);
                                               

                                          ?>
                                           @php
                     if(!empty($prescription_img_data->image)){
                     $url = $prescription_img_data->image;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                     <img src="<?php echo $url; ?>" width="700" height="800" alt="prescription" >

                                                        </div>
                                                  </div>
                                              </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                            </div>
                                            </div>
                                            @endif
                                          @endforeach                                   
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            
        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                     Copyright © 2021 Qikmeds. All Rights Reserved.
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- end main content-->
 <!-- END layout-wrapper -->



@endsection

{{-- page level scripts --}}
@section('footer_scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>

<script type="text/javascript">
var accepted_url ="{{URL::to('vendor/accepte-orders')}}";
var decline_url = "{{URL::to('vendor/decline-orders')}}";
var token       = '{{ csrf_token() }}';


//Accepted All Orders
$(document).ready(function(){
    $('.accepted').on('click',function(event){
        $("#accepted").html("Submitting...");
         $("#overlay").fadeIn(500);　
         order_id = $(this).attr("data-id");
         ord_item_id = '';
         event.preventDefault();
          $.ajax({
            url:  accepted_url,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
            dataType: 'json',
            success: function(response) {
             setTimeout(function(){
             $("#overlay").fadeOut(1000);
                    $("#nearby").load(document.URL + " #nearby");
                    $("#city").load(document.URL + " #city");
               toastr[response.status](response.message, "Order");
            },1000);

            }
            });
              
        
    });
});

//Decline_orders Products All Orders
$(document).ready(function(){   
    $('.decline').on('click',function(){
             $("#overlay").fadeIn(500);　
           order_id = $(this).attr("data-id");
            var ord_item_id=[];
              event.preventDefault();
             // alert(order_id)
          $.ajax({
            url:  decline_url,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
            dataType: 'json',
            success: function(response) {
                //alert('successfully');
                 setTimeout(function(){
                 $("#overlay").fadeOut(1000);
                     $("#nearby").load(document.URL + " #nearby");
                     $("#city").load(document.URL + " #city");
                   toastr[response.status](response.message, "Order");
                },1000);

            }
            });
              
        
    });
});

</script>

@stop
