@extends('frontend/vendor/layouts/default')
@section('header_styles')

 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>


@stop
@section('content')
    <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                             <div class="col-sm-9">
                    <div class="right_sidebar">
                        <h2>Personal Information</h2>
                        <div class="user_detail_blk cmn_area">
                         <form method="post" action="{{route('vendor.update_profile_vendor')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Full Name</label>
                                        <span><input type="text" name="name" class="form-control" value="{{Auth::guard('vendor')->user()->name}}" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Email Address</label>
                                        <span><input type="text" readonly name="email" class="form-control" value="{{Auth::guard('vendor')->user()->email}}" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Mobile Number</label>
                                          <span><input type="text" readonly name="mobile_number" class="form-control" value="{{Auth::guard('vendor')->user()->mobile_number}}" ></span>                                    
                                        </div>
                                </div>

                                 <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Firm Name</label>
                                        <span><input type="text" name="farm_name" class="form-control" value="{{Auth::guard('vendor')->user()->farm_name}}" ></span>
                                    </div>
                                </div>

                                 <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Address</label>
                                        <span><input type="text" name="address" class="form-control" value="{{Auth::guard('vendor')->user()->address}}" ></span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Image</label>
                                            <input type="file"  name="image" id="file"
                                            value="{{Auth::guard('vendor')->user()->image}}"

                                            value="@if(isset(Auth::guard('vendor')->user()->image)){!! Auth::guard('vendor')->user()->image !!}@else{!! old('image') !!}@endif"  

                                            onchange="loadFile(event)" class="form-control">  
                                        </div>
                                </div>  
                               <!--  <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Date of Birth</label>
                                          <span><input type="date" name="dob" class="form-control" value="{{Auth::guard('vendor')->user()->dob}}" ></span> 
                                   
                                    </div>
                                </div>
  -->
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                         <p><img src="{{Auth::guard('vendor')->user()->image}}" id="output" width="200" /></p>
                                   
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="user_txt">
                                        <label>Gender</label>
                                        <div class="radio_btn">
                                            <?php $gender = Auth::guard('vendor')->user()->gender ?>
                                            <input type="radio" name="gender" value="male" id="r-1" <?php if($gender == "male") { echo 'checked'; } ?>>
                                            <label for="r-1">Male</label>
                                        </div>
                                        <div class="radio_btn">
                                            <input type="radio" name="gender" value="female" id="r-2"<?php if($gender == "female") { echo 'checked'; } ?>>
                                            <label for="r-2">Female</label>
                                        </div>
                                        <div class="radio_btn">
                                            <input type="radio" name="gender" value="other" id="r-3"<?php if($gender == "other") { echo 'checked'; } ?>>
                                            <label for="r-3">Others</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit"  style="background-color: #24aeb1;"    class="btn btn-primary"><i class=" fa fa-edit"></i> Edit</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>


                    <div class="right_sidebar" style="
    margin-top: 100px;
">
                        <h2>Change Password</h2>
                        <div class="user_detail_blk cmn_area">
                         <form method="post" id="change-password" action="{{route('vendor.change-password')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                            <div class="row">
                              @if(!empty(Auth::guard('vendor')->user()->password))
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Old password</label>
                                        <span><input type="password" name="old_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="1">
                              @else
                                <input type="hidden" name="type" value="0">
                                
                              @endif  
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>New password</label>
                                        <span><input type="password" name="new_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="user_txt">
                                        <label>Confirm Password</label>
                                        <span><input type="password" name="confirm_password" class="form-control" value=""></span>
                                    </div>
                                </div>
                                <br /><br />

                              
                                    
                                <div class="col-sm-12" style="margin-top: 10px;">
                                    <button type="submit" class="cmn_btn btn btn-primary"> <i class="fa fa-save"></i> Save</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                        </div>
                        <!-- end page title -->
                        

                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                 Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

@endsection

{{-- page level scripts --}}

<!-- <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script> -->
 @section('footer_scripts')
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>


<script>
var select_address ="{{route('user.select-address')}}";
var token = '{{ csrf_token() }}'; 


var loadFile = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};
</script>

@stop

