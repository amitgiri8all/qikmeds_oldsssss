@extends('frontend/vendor/layouts/default')
@section('header_styles')

 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop
@section('content')
    <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">My Profile</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">My Profile</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                        
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="vendor_profile">
                                            <h2>Personal Info</h2>
                        <form method="post" action="{{route('vendor.edit_profile_vendor')}}" enctype="multipart/form-data" class="ajax_form">   
                            @csrf
                                            <div class="row">

                                 <div class="col-sm-4">
                                    <div class="user_txt">
                                        <label> Name</label>
                                        <p><input type="text" name="name" class="form-control" value="{{$profile_data->name}}" ></p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="user_txt">
                                        <label>Firm Name</label>
                                        <p><input type="text" name="farm_name" class="form-control" value="{{$profile_data->farm_name}}" ></p>
                                    </div>
                                </div>

                                 <div class="col-sm-4">
                                    <div class="user_txt">
                                        <label>Email</label>
                                        <p><input type="text" readonly name="email" class="form-control" value="{{$profile_data->email}}" ></p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="user_txt">
                                        <label>Phone</label>
                                        <p><input type="text" readonly name="mobile_number" class="form-control" value="{{$profile_data->mobile_number}}" ></p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="user_txt">
                                        <label>Address</label>
                                        <p><input type="text"  name="address" class="form-control" value="{{$profile_data->address}}" ></p>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="user_txt">
                                        <label>Pin Code</label>
                                        <p><input type="text"  name="pin_code" class="form-control" value="{{$profile_data->pin_code}}" ></p>
                                    </div>
                                </div>

                                 <div class="col-sm-4">
                                    <div class="user_txt">
                                        <label>Registration No.</label>
                                        <p><input type="text"  name="registration_number" class="form-control" value="{{$profile_data->registration_number}}" ></p>
                                    </div>
                                 </div>
                                <div class="col-sm-8">
                                    <label>Commission</label>
                                   <div class="row">
                                       <div class="col-sm-4"><p>Medicine - <span  style="
    margin: 50px;
">
                                        <input type="text"  name="comm_medicine"  value="{{$profile_data->comm_medicine}}" style="
    width: 50px;" >%
                                    </span></p></div>
                                       <div class="col-sm-4"><p>Beauty Products - <span><input type="text"  name="comm_beauty_pro"  value="{{$profile_data->comm_beauty_pro}}" style="
    width: 50px;" >%</span></p></div>
                                       <div class="col-sm-4"><p>Devices / Surgicals - <span><input type="text"  name="comm_device_surgiacl"  value="{{$profile_data->comm_device_surgiacl}}" style="
    width: 50px;" >%</span></p></div>
                                   </div>
                                </div>

                                 <div class="col-md-12 mar-10">
                                  <div class="col-xs-4 col-md-4"></div>
                                   <div class="col-xs-4 col-md-2">
                                    <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                                    @lang('Edit')
                                    </button>
                                 </div>
                              </div>
                              
                                </div>
                                </form>            
                                        </div>

       <?php //echo "<pre>"; print_r($doc_data);die; ?>                                 
    
    <div class="vendor_profile" style="padding-top: 15px;">

         <form method="post"   action="{{route('vendor.document')}}"  id="document"  enctype="multipart/form-data">
                  @csrf
        <h2>Documents</h2>
        <div class="row">
            <div class="col-sm-4">
                @php
                     if(!empty($doc_data->drug_license)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$doc_data->drug_license);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                @endphp
                <label>Drug License</label>
                 <input type="file" class="form-control btn btn-secondary" value="{{old('drug_license')}}" name="drug_license">
                 {!! $errors->first('drug_license', '<span class="help-block">:message</span>') !!}
                <div class="doc_view"> <a href="{{$url}}" target="_blank" class="btn btn-primary">View</a>
                 <img src="<?php echo $url; ?>" width="100" height="100" >
                </div>
                

            </div>


            <div class="col-sm-4">
                 @php
                     if(!empty($doc_data->gstin)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$doc_data->gstin);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                @endphp
                <label>GSTIN</label>
                 <input type="file" class="form-control btn btn-secondary" value="{{old('gstin')}}" name="gstin">
                <div class="doc_view"><a href="{{$url}}" class="btn btn-primary">View</a>
                 <img src="<?php echo $url; ?>" width="100" height="100" >
                </div>
               
            </div>

            <div class="col-sm-4">
                @php
                     if(!empty($doc_data->signature)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$doc_data->signature);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                @endphp
                <label>Signature</label>
                 <input type="file" class="form-control btn btn-secondary" value="{{old('signature')}}" name="signature">
                <div class="doc_view"> <a href="{{$url}}" class="btn btn-primary">View</a>
                <img src="<?php echo $url; ?>" width="100" height="100" >
                </div>
                
            </div>
            <div class="col-sm-4">
                @php
                     if(!empty($doc_data->bank)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$doc_data->bank);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                @endphp
                <label>Bank</label>
                 <input type="file" class="form-control btn btn-secondary" value="{{old('bank')}}" name="bank">
                <div class="doc_view"> <a href="{{$url}}" class="btn btn-primary">View</a>
                <img src="<?php echo $url; ?>" width="100" height="100" >
                </div>
                
            </div>
            <div class="col-sm-4">
                <label>Educational Certificate</label>
                @php
                     if(!empty($doc_data->educational_certificate)){
                     $url = asset('storage/app/public/upload/Thumbnail/'.$doc_data->educational_certificate);
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                @endphp
                 <input type="file" class="form-control btn btn-secondary" value="{{old('educational_certificate')}}" name="educational_certificate">
                <div class="doc_view"> <a href="{{$url}}" class="btn btn-primary">View</a>
                    <img src="<?php echo $url; ?>" width="100" height="100" >
                </div>
                
            </div>
        </div>
           <div class="col-md-12 mar-10">
                     <div class="col-xs-4 col-md-4"></div>
                     <div class="col-xs-4 col-md-2">
                        <button type="submit"  class="btn btn-primary btn-block btn-md btn-responsive">
                        @lang('SAVE')
                        </button>
                     </div>
                  </div>
               </form>
    </div>
</div>
</div>
</div>
</div>
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                 Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

@endsection
{{-- page level scripts --}}

@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
    <script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

 <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>
@stop


