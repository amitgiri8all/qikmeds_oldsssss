@extends('frontend/vendor/layouts/default')

@section('header_styles')

 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop
@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

<div class="page-content">
<div class="container-fluid">

<!-- start page title -->
<div class="row">
<div class="col-12">
    <div class="page-title-box d-flex align-items-center justify-content-between">
        <h4 class="mb-0">My Subscriptions</h4>

        <div class="page-title-right">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                <li class="breadcrumb-item active">My Subscriptions</li>
            </ol>
        </div>

    </div>
</div>
</div>
<!-- end page title -->

<div class="row">
<div class="col-xl-12">
    <div class="card">
        <div class="card-body">
            <div class="subscription_blk cmn_area">
                <h3>Qikmeds Premium Pharmacy</h3>
                <h4>Reduce your medical expenses with our premium membership benefits</h4>
              <div class="row">
               {{--  @foreach($subscription_meta as $key=>$val)
                <div class="col-sm-4">
                    <div class="subscription_benefts">

                     @php
                     if(!empty($val->image)){
                     $url = $val->image;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                        <i><img src="<?php echo $url; ?>" width="50px" height="50px"></i>
                        <h5>{{$val->title}}</h5>
                        <p>{{$val->description}}</p>
                    </div>
                </div> 
                @endforeach
                --}}
             <!--    <div class="col-sm-4">
                    <div class="subscription_benefts">
                        <i><img src="assets/images/benefit-2.png"></i>
                        <h5>Priority Order Routing</h5>
                    </div>
                </div> -->
                <!-- <div class="col-sm-4">
                    <div class="subscription_benefts">
                        <i><img src="assets/images/benefit-3.png"></i>
                        <h5>Multiple Device Access</h5>
                    </div>
                </div> -->
             
              

               @foreach($subscription_plan_data as $key=>$val)
              <div class="card col-sm-12" style="background:#eef5fb; margin-bottom: 25px;">
                <div class="card-body">
                  <div class="radio_btn" style="float:left;">
                     <input type="radio" value="{{$val->id}}" name="plan_id" id="r-{{$key}}">
                     <label for="r-{{$key}}">{{$val->duration}}</label>
                  </div>
                  <div class="radio_btn" style="float:right;">

                  <a href="{{$val->image}}" target="_blank">  <img src="{{$val->image}}" width="50" height="30" > </a>
                    <span ><img src="https://assets.pharmeasy.in/web-assets/dist/6aa9e076.png" style="position: absolute;">{{$val->discount}}% Off </span> &nbsp;&nbsp;&nbsp;
                    <span>Rs.{{$val->sale_price}} </span>&nbsp;&nbsp;&nbsp;

                    <span class="strike">Rs.<strike>{{$val->price}}</strike></span>
                  </div>
                            
                                </div>
              </div>
              @endforeach 

            @if(session('cart'))
              <div class="action_btns">
                                <button type="submit" id="add_plan" class="cmn_btn">Add To Cart</button>
                            </div>
            @else
                <div class="action_btns">
                                <button type="submit" id="add_plan" class=" btn btn-primary cmn_btn">By Now</button>
                            </div>
            @endif
          </div>
            </div>
            </div>
        </div>
    </div>
@endsection 


{{-- page level scripts --}}
@section('footer_scripts')
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>
<script type="text/javascript">
  var add_subscription_plan="{{URL::to('add-subscription-plan')}}";

 var token = '{{ csrf_token() }}';
</script>
<!-- <script type="text/javascript">
  //Accepted All Orders
$(document).ready(function(){
    $('#add_plan').on('click',function(event){
        var plan_id =  $('input[name="plan_id"]:checked').val();
      //  alert(plan_id)
         event.preventDefault();
          $.ajax({
            url:  add_subscription_plan,
            type: "POST",
            data: {'_token': token,plan_id:plan_id},
            dataType: 'json',
            beforeSend : function() {
                  $("#add_plan").html('Please wait &nbsp;<i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            success: function(response) {
                console.log(response.status)
                    if(response.status=='validation'){
                        toastr['error']('Please Select a Plan.', "Info");
                    }
                setTimeout(function(){
                 // $("#add_plan").html('Add To Cart');
                  toastr[response.status](response.message, "Info");
                   window.location.href = response.redirect_url;

                },1000);
                }
            });
              
        
    });
});
</script> -->
@stop


 