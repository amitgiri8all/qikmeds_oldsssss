@extends('frontend/vendor/layouts/default')
@section('content')
@section('header_styles')

  <!-- Style to set the size of checkbox -->
    <style>
        input.checkbox {
            width: 20px;
            height: 40px;
        }
         input.check {
            width: 20px;
            height: 40px;
        }
    </style>
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

@stop    
 
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Validate Order</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">Validate Order</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="order_delivery_info">
                                            <h2>Order Details</h2>
                                            <div class="payment_details">
                                                <ul>
                                                    <li>
                                                        <span>Customer Name</span>
                                                        <span>{{$order->user->name}}</span>
                                                    </li>
                                                    <li>
                                                        <span>Order ID</span>
                                                        <span>{{$order->order_id}}</span>
                                                    </li>
                                                    <li>
                                                        <span>Order Placed</span>
                                                        <span>{{date('d-m-Y', strtotime($order->created_at));}} - {{date('h:i A', strtotime($order->created_at));}}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="order_delivery_info">
                                            <h2>Delivery Address</h2>
                                            <div class="address_blk">
                                                <h4>{{$order->deliveryaddress->name}}</h4>
                                                <h5>{{$order->deliveryaddress->mobile_number}}</h5>
                                                <span>{{$order->deliveryaddress->address_type}}</span>
                                               <p>{{$order->deliveryaddress->address_delivery}}<br> {{$order->deliveryaddress->city}}, {{$order->deliveryaddress->state}} - {{$order->deliveryaddress->pin_code}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="order_price_blk">
                                        <h2>Payment Details</h2>
                                        <div class="payment_details">
                                           <ul>
                                                <li>
                                                    <span>Sub Total</span>
                                                    <span>₹ {{$order->sub_total}}</span>
                                                </li>
                                                <li>
                                                    <span>Qikmeds Discount</span>
                                                    <span>₹ {{$order->discount_amount}}</span>
                                                </li> 
                                                @if(!empty($order->discount_amount))
                                                <li>
                                                    <span>Promo Discount</span>
                                                    <span>₹ {{$order->discount_coupon_code}}</span>
                                                </li>
                                                @endif
                                                <li>
                                                    <span>Delivery Fee</span>
                                                    <?php if($order->shipping_amount>0){?>
                                                    <span>₹ {{$order->shipping_amount}}</span>
                                                <?php }else{ ?>
                                                    <span>Free</span>
                                                    <?php  }?>
                                                </li>
                                                <li>
                                                    <span>Net Amount Payable</span>
                                                    <span>₹ {{$order->total_payed_amount}}</span>
                                                </li>
                                                <li>
                                                    <span>Payment Mode</span>
                                                    <span>{{ucfirst($order->orderpayment->payment_method)}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-8 order_val">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="datatable" class="table table-centered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th style="width: 20px;">
                                                       <input type="checkbox" class="check" id="select_all" value="0" />
                                                    </th>
                                                    <th>Item</th>
                                                    <th>Item Desc</th>
                                                    <th>Type</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <th>Status</th>
                                                 </tr>
                                                </thead>
                                                <tbody>
                                        @foreach($order->OrderItem as $key=>$value)   
                                            @php $vendor_id = Auth::guard('web')->user()->id;

                                               $data =  App\Models\DeclineProducts::remove_decline_order_products($value->ord_item_id,$vendor_id,$order->order_id);
                                             @endphp  
                                             @if($data)
                                                    <tr>
                                                        <td>
                                                           @if($value->vendor_id == $vendor_id) 
                         <div class="badge badge-soft-success font-size-12"><i class="fa fa-check-circle"></i></div>
                @else
                    <input type="checkbox" name="order" class="checkbox" value="{{$value->ord_item_id}}" />
                @endif

                                                         </td>
                                                        <td><img src="{{asset('public/frontend/images/product-1.jpg')}}" alt=""></td>
                                                        <td>{{$value->ProductOrderItem->medicine_name}}</td>
                                                        <td>{{$value->ProductOrderItem->type_of_sell}}</td>
                                                        <td>{{$value->quantity_order}}</td>
                                                        <td>₹ {{$value->ProductOrderItem->mrp}}</td>
                                                        <td>
                                                            <div class="badge badge-soft-success font-size-12">In Stock</div>
                                                        </td>
                                                    </tr>
                                                @endif    
                                        @endforeach          
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="action_btns">
                                    <button type="button" id="decline" data-id="{{$order->order_id}}" class="btn cmn_btn btn-danger waves-effect waves-light">Decline</button>
                                    <a href="javascript::void(0)" data-id="{{$order->order_id}}"  id="accepted" class="btn cmn_btn btn-success waves-effect waves-light loaded_btn">Accept</a>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="prescription_img"><img src="{{asset('public/vendor/assets/images/prescription.jpg')}}" alt="prescription"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

            <!-- end main content-->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<script type="text/javascript">
var accepted_url ="{{URL::to('vendor/accepte-orders')}}";
var decline_url ="{{URL::to('vendor/decline-orders')}}";

var token       = '{{ csrf_token() }}';



$(document).ready(function(){
    $('#select_all').on('click',function(){
        $('.checkbox:checked').each(function() {
         // alert($(this).val());
        });
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    $('.checkbox').on('click',function(){
       // alert('select_all')
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });


    $('.checkbox:checked').each(function() {
       // alert($(this).val());
    });


});

//Accepted All Orders
$(document).ready(function(){
    $('#accepted').on('click',function(){
        $("#accepted").html("Submitting...");
         $("#overlay").fadeIn(500);　
         order_id = $(this).attr("data-id");
         //alert(order_id)
         if($('.checkbox').is(":checked")){
           var ord_item_id=[];
             $("input:checkbox[name=order]:checked").each(function(){
                  ord_item_id.push($(this).val());
                });
             event.preventDefault();
          $.ajax({
            url:  accepted_url,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
            dataType: 'json',
            success: function(response) {
             setTimeout(function(){
             $("#overlay").fadeOut(1000);
                 $("#datatable").load(document.URL + " #datatable");
               toastr[response.status](response.message, "Order");
            },1000);

            }
            });
              
        }else{
          $("#accepted").html("Accept");
          toastr['error']('Checkbox is not checked', "Error");
          $("#overlay").fadeOut(1000);
        }
    });
});

//Decline_orders Products All Orders
$(document).ready(function(){
    $('#decline').on('click',function(){
             $("#overlay").fadeIn(500);　
           order_id = $(this).attr("data-id");
          if($('.checkbox').is(":checked")){
           var ord_item_id=[];
             $("input:checkbox[name=order]:checked").each(function(){
                  ord_item_id.push($(this).val());
                });
             event.preventDefault();

          $.ajax({
            url:  decline_url,
            type: "POST",
            data: {'_token': token,ord_item_id:ord_item_id,order_id:order_id},
            dataType: 'json',
            success: function(response) {
                 setTimeout(function(){
                 $("#overlay").fadeOut(1000);
                     $("#datatable").load(document.URL + " #datatable");
                   toastr[response.status](response.message, "Order");
                },1000);

            }
            });
              
        }else{
         alert("checkbox is not checked");
        }
    });
});

</script>
@stop
