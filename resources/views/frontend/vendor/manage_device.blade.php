@extends('frontend/vendor/layouts/default')
@section('content')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

<div class="page-content">
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0">Manage Devices / Surgicals</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                        <li class="breadcrumb-item active">Manage Devices / Surgicals</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    

    <div class="row">
        <div class="col-xl-12 manage_blk">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <ul class="nav nav-pills nav-justified" role="tablist">
                               <li class="nav-item waves-effect waves-light">
                                    <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
                                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                        <span class="d-none d-sm-block">Devices</span> 
                                    </a>
                                </li>
                                <li class="nav-item waves-effect waves-light">
                                    <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
                                        <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                        <span class="d-none d-sm-block">Surgicals</span> 
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="slct_categry mb-4">
                            <select class="form-control" id="sub-cate" name="sub-cate">
                                <option>Select Category</option>
                                @foreach ($categories_data as $key=>$value)
                                <option value="{{ $value->id }}">{{$value->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="tab-pane active" id="home1" role="tabpanel">
                                            <div class="table-responsive">
                           <table id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; border: none; width: 100%;">
                                    <thead>
                                  <tr class="filters">
                                     <th>Product</th>
                                     <th>Product Desc</th>
                                     <th>Price</th>
                                     <th>Discount</th>
                                     <th>Quantity</th>
                                    <th>Action</th>
                                  </tr>
                               </thead>
                                    <tbody>
                                        @foreach($devices as $key=>$val)
                                            <tr>
                                                <td><img src="{{$val->image}}"></td>
                                                <td>     
                                                <h5 class="font-size-14 text-truncate"><a href="#" data-toggle="modal" data-target="#product_detail{{$val->id}}">{{$val->medicine_name}}</a></h5>
                                                <p class="mb-0">Mfr : <span class="font-weight-medium">{{$val->manufacturer_name}}</span></p>

<div id="product_detail{{$val->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<button type="button" class="close free_close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
<div class="modal-body">
    <div class="product_detail">
        <div class="product_img"><img src="{{$val->image}}" alt="product"></div>
        <div class="product_detail_txt">
            <h2>{{$val->medicine_name}}</h2>
           
            <div class="prodct_price_blk">
                <h3>₹ {{$val->sale_price}}</h3>
                <span class="strike">₹ {{$val->mrp}}</span>
                <small>% {{$val->discount}} Off</small>
            </div>
            <div class="prodct_highlits">
                <h3>Product Description</h3>
                <p>{{$val->description}}</p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

                                            </td>
                                                <td>{{$val->mrp}}</td>
                                                <td>{{$val->discount}}</td>
                                                <td>{{$val->qty}}</td> 
                                                <td>
                                                     <a href="#" class="mr-3 text-primary" data-toggle="modal" data-target="#edit_product{{$val->id}}"><i class="mdi mdi-pencil font-size-18"></i></a>
 

<div id="edit_product{{$val->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <h5 class="modal-title mt-0" id="myModalLabel">Edit ({{$val->medicine_name}}) Product</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
  <form method="post"  enctype="multipart/form-data" id="productdataedit" class="ajaxform" action="{{ route('add-vendor-product') }}" >  
    @csrf
    <div class="add_new_product">
        <div class="form-group">
            <label>Product Name</label>
            <input class="form-control" type="text" name="product_name"  placeholder="Enter product name" id="product_name" value="{{$val->medicine_name}}">

        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label>Select Category</label>
                <select class="form-control category_change" name="category" id="category">
                    <option>Select Category</option>
                      @foreach($allCategories as $key=>$value)
                        <option value="{{$value->id}}" @if($val->category_id==$value->id) selected @endif>{{$value->category_name}}</option>
                      @endforeach
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label>Select Sub Category</label>
                <select class="form-control category_sub" id="category_sub" name="category_sub">
                 </select>
            </div>
        </div>
        <div class="row">
                      <div class="form-group col-sm-8">
                <label>Manufacturer</label>
                 <select class="form-control" name="manufacturer_id" id="manufacturer_id">
                        <option value="">Please Manufacturer Name</option>
                         @foreach($manufacturer as $key=>$value)
                           <option value="{{$value->id}}" @if($val->manufacturer_id==$value->id) selected @endif>{{$value->manufacturer_name}}</option>

                        @endforeach
                     </select>

            </div>

            <div class="form-group col-sm-4">
            <label>Product Qty</label>
            <input class="form-control" type="text" name="qty" placeholder="Enter product qty" id="qty" value="{{$val->qty}}">

        </div>

        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label>Product Price</label>
                <input class="form-control" type="text" placeholder="Enter product price" value="{{$val->mrp}}" id="price_device" name="price">
            </div>
            <div class="form-group col-sm-3">
                <label>Sale Price</label>
                <input class="form-control" type="text" value="{{$val->sale_price}}" name="sale_price" id="sale_price_device" placeholder="Sale Price">
            </div>
            <div class="form-group col-sm-3">
                <label>Discount In %</label>
                <input class="form-control" readonly="readonly" name="discount" id="discount_device" type="text" value="{{$val->discount}}" placeholder="Discount">
            </div>
        </div>
        <div class="form-group">
            <label>Product Image</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            <img src="{{$val->image}}">
        </div>
        <div class="form-group">
            <label>Product Description</label>
            <textarea id="description" name="description" class="form-control" rows="5">{{$val->description}}</textarea>
        </div>
        <div class="text-center">
            <button type="submit" class="btn cmn_btn btn-primary">Submit</button>
        </div>
    </div>
 </form>   
</div>
</div>
</div>
</div>

                                                <a href="javascript:void(0);" data-id="{{ $val->id }}" class="text-danger deleteRecord" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                             </td>
                                         </tr>
                                        @endforeach 
                                     </tbody>
                                   </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="profile1" role="tabpanel">
                            <div class="table-responsive">
                               <table id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; border: none; width: 100%;">
                                    <thead>
                                  <tr class="filters">
                                     <th>Product</th>
                                     <th>Product Desc</th>
                                     <th>Price</th>
                                     <th>Discount</th>
                                     <th>Quantity</th>
                                    <th>Action</th>
                                  </tr>
                               </thead>
                                    <tbody>
                                        @foreach($surgicals as $key=>$val)
                                            <tr>
                                                <td><img src="{{$val->image}}"></td>
                                                  <td>     
                                                <h5 class="font-size-14 text-truncate"><a href="#" data-toggle="modal" data-target="#product_detail{{$val->id}}">{{$val->medicine_name}}</a></h5>
                                                <p class="mb-0">Mfr : <span class="font-weight-medium">{{$val->manufacturer_name}}</span></p>

<div id="product_detail{{$val->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<button type="button" class="close free_close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
<div class="modal-body">
    <div class="product_detail">
        <div class="product_img"><img src="{{$val->image}}" alt="product"></div>
        <div class="product_detail_txt">
            <h2>{{$val->medicine_name}}</h2>
           
            <div class="prodct_price_blk">
                <h3>₹ {{$val->sale_price}}</h3>
                <span class="strike">₹ {{$val->mrp}}</span>
                <small>% {{$val->discount}} Off</small>
            </div>
            <div class="prodct_highlits">
                <h3>Product Description</h3>
                <p>{{$val->description}}</p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

                                            </td>
                                                <td>{{$val->sale_price}}</td>
                                                <td>{{$val->discount}}</td>
                                                <td>{{$val->qty}}</td> 
                                                <td><a style="display:none;" href="javascript:void(0);" class="mr-3 text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-size-18"></i></a>

                                                    <a href="#" class="mr-3 text-primary" data-toggle="modal" data-target="#edit_product{{$val->id}}"><i class="mdi mdi-pencil font-size-18"></i></a>

    <a href="javascript:void(0);" data-id="{{ $val->id }}"  class="text-danger deleteRecord" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>

<div id="edit_product{{$val->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <h5 class="modal-title mt-0" id="myModalLabel">Edit ({{$val->medicine_name}}) Product</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
  <form method="post"  enctype="multipart/form-data" id="productdataedit" class="ajaxform" action="{{ route('add-vendor-product') }}" >  
    @csrf
    <div class="add_new_product">
        <div class="form-group">
            <label>Product Name</label>
            <input class="form-control" type="text" name="product_name"  placeholder="Enter product name" id="product_name" value="{{$val->medicine_name}}">

        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label>Select Category</label>
                <select class="form-control category_change" name="category" id="category">
                    <option>Select Category</option>
                      @foreach($allCategories as $key=>$value)
                        <option value="{{$value->id}}">{{$value->category_name}}</option>
                      @endforeach
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label>Select Sub Category</label>
                <select class="form-control category_sub" id="category_sub" name="category_sub">
                 </select>
            </div>
        </div>
        <div class="row">
                      <div class="form-group col-sm-8">
                <label>Manufacturer</label>
                 <select class="form-control" name="manufacturer_id" id="manufacturer_id">
                        <option value="">Please Manufacturer Name</option>
                         @foreach($manufacturer as $key=>$value)
                           <option value="{{$value->id}}" 
                            @if($val->manufacturer_id==$value->id) selected @endif>{{$value->manufacturer_name}}</option>
                        @endforeach
                     </select>

            </div>

            <div class="form-group col-sm-4">
            <label>Product Qty</label>
            <input class="form-control" type="text" name="qty" placeholder="Enter product qty" id="qty" value="{{$val->qty}}">

        </div>

        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label>Product Price</label>
                <input class="form-control" type="text" placeholder="Enter product price" value="{{$val->mrp}}" id="price_surgical" name="price">
            </div>
            <div class="form-group col-sm-3">
                <label>Sale Price</label>
                <input class="form-control" type="text" value="{{$val->sale_price}}" name="sale_price" id="sale_price_surgical" placeholder="Sale Price">
            </div>
            <div class="form-group col-sm-3">
                <label>Discount In %</label>
                <input class="form-control" readonly="readonly" name="discount" id="discount_surgical" type="text" value="{{$val->discount}}" placeholder="Discount">
            </div>
        </div>
        <div class="form-group">
            <label>Product Image</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            <img src="{{$val->image}}">
        </div>
        <div class="form-group">
            <label>Product Description</label>
            <textarea id="description" name="description" class="form-control" rows="5">{{$val->description}}</textarea>
        </div>
        <div class="text-center">
            <button type="submit" class="btn cmn_btn btn-primary">Submit</button>
        </div>
    </div>
 </form>   
</div>
</div>
</div>
</div>






                                             </td>
                                         </tr>
                                        @endforeach 
                                     </tbody>
                                   </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="action_btns">
                <a href="#" class="btn cmn_btn btn-primary" data-toggle="modal" data-target="#add_product"><i class="mdi mdi-plus mr-2"></i> Add New Product</a>
            </div>
        </div>
    </div>
    
</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

<footer class="footer">
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
             Copyright © 2021 Qikmeds. All Rights Reserved.
        </div>
    </div>
</div>
</footer>
</div>
<!-- end main content-->
<!-- END layout-wrapper -->



<div id="add_product" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <h5 class="modal-title mt-0" id="myModalLabel">Add New Product</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
  <form method="post"  enctype="multipart/form-data" id="productdata" class="ajaxform" action="{{ route('add-vendor-product') }}" >  
    @csrf
    <div class="add_new_product">
        <div class="form-group">
            <label>Product Name</label>
            <input class="form-control" type="text" name="product_name" placeholder="Enter product name" id="product_name" value="">

        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label>Select Category</label>
                <select class="form-control category_change" name="category" id="category_product_add">
                    <?php // echo "<pre>"; print_r($allCategories);die; ?>
                    <option>Select Category </option>
                      @foreach($allCategories as $key=>$value)
                        <option value="{{$value->id}}">{{$value->category_name}} </option>
                      @endforeach
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label>Select Sub Category</label>
                <select class="form-control category_sub" id="category_sub1" name="category_sub">
                 </select>
            </div>
        </div>
        <div class="row">
                      <div class="form-group col-sm-8">
                <label>Manufacturer</label>
                 <select class="form-control" name="manufacturer_id" id="manufacturer_id">
                        <option value="">Please Manufacturer Name</option>
                         @foreach($manufacturer as $key=>$value)
                           <option value="{{$value->id}}">{{$value->manufacturer_name}}</option>
                        @endforeach
                     </select>

            </div>

            <div class="form-group col-sm-4">
            <label>Product Qty</label>
            <input class="form-control" type="text" name="qty" placeholder="Enter product qty" id="qty" value="">

        </div>

        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label>Product Price</label>
                <input class="form-control" type="text" placeholder="Enter product price" value="" id="price_add" name="price">
            </div>
            <div class="form-group col-sm-3">
                <label>Sale Price</label>
                <input class="form-control" type="text" value="" name="sale_price" id="sale_price_add" placeholder="Sale Price">
            </div>
            <div class="form-group col-sm-3">
                <label>Discount In %</label>
                <input class="form-control " readonly="readonly" name="discount" id="discount_add" type="text" value="" placeholder="Discount">
            </div>
        </div>
        <div class="form-group">
            <label>Product Image</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
        <div class="form-group">
            <label>Product Description</label>
            <textarea id="description" name="description" class="form-control" rows="5"></textarea>
        </div>
        <div class="text-center">
            <button type="submit" class="btn cmn_btn btn-primary">Submit</button>
        </div>
    </div>
 </form>   
</div>
</div>
</div>
</div>

@endsection
@section('footer_scripts')
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
    <script type="text/javascript" src="http://172.105.36.210/ibazar/public/assets/js/forms.js"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.1/js/dataTables.bootstrap4.min.js"></script>
 <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>


<script type="text/javascript">
       var urlmy= "{!! route('fetchSub_category') !!}";
       $(document).ready(function () {
            $('.category_change').on('change', function () {
             //alert('sdfasdf')
                var category_id = this.value;
                $(".category_sub").html('');
                $.ajax({
                    url: urlmy,
                    type: "POST",
                    data: {
                        category_id: category_id,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        $('.category_sub').html('<option value="">Select Sub Category</option>');
                        $.each(result.subcategory, function (key, value) {
                            $(".category_sub").append('<option value="' + value
                                .id + '">' + value.category_name + '</option>');
                        });
                     }
                });
            });
        });

</script>

<script type="text/javascript">
$(document).ready(function() {
 $('#datatable').DataTable();
} );
$(".deleteRecord").click(function(){
    if(!confirm("Do you really want to do this?")) {
     return false;
    }

    var id = $(this).data("id");
    var token = '{{ csrf_token() }}';
    $.ajax(
    {
        url: "destroy/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
            alert('Record deleted successfully!');
            location.reload();
            console.log("it Works");
        }
    });
   
});



</script>
<script type="text/javascript">
   
$(function () {
            $("#price_add,#sale_price_add").change(function () {
                 //debugger;
                var price = parseFloat($("#price_add").val());
                var sale_price = parseFloat($("#sale_price_add").val());
                $("#discount_add").val(Math.round((price-sale_price)/(price)*100));
            })
    });

</script>

<script type="text/javascript">
   
$(function () {
            $("#price_surgical,#sale_price_surgical").change(function () {
                 //debugger;
                var price_sur = parseFloat($("#price_surgical").val());
                var sale_price_sur = parseFloat($("#sale_price_surgical").val());
                $("#discount_surgical").val(Math.round((price_sur-sale_price_sur)/(price_sur)*100));
            })
    });

</script>

<script type="text/javascript">
   
$(function () {
            $("#price_device,#sale_price_device").change(function () {
                 //debugger;
                var price_dev = parseFloat($("#price_device").val());
                var sale_price_dev = parseFloat($("#sale_price_device").val());
                $("#discount_device").val(Math.round((price_dev-sale_price_dev)/(price_dev)*100));
            })
    });

</script>

@stop
