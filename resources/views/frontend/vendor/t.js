function slideToElement(e, t) {
    var a = $(e);
    $("html, body").animate({
        scrollTop: a.offset().top - 100
    }, 500)
}

function slideToDiv(e) {
    $("html, body").animate({
        scrollTop: $(e).offset().top - 50
    }, 1e3)
}

function slideToTop() {
    $("html, body").animate({
        scrollTop: 50
    }, 1e3)
}

function isset(e) {
    return null != e
}

function hide_alert_message() {
    setTimeout(function() {
        $(".alert.alert-dismissable").fadeOut(1e3)
    }, 3e3)
}
$(document).ready(function() {
    $(document).on("submit", ".ajaxform", function(e) {
        var t, a = $(this).attr("action"),
            s = $(this).attr("data-callback_function");
        if (s && 0 == callbackForm(s)) return !1;
        if (i = $(this).attr("id")) var i = "#" + i;
        else i = ".ajaxform";
        return $(this).ajaxSubmit({
            url: a,
            dataType: "json",
            beforeSend: function() {
                $(".formmessage").remove(), $(i).find(".alert").removeClass("alert-success").removeClass("alert-danger").removeClass("alert-info"), $(i).find(".alert").addClass("alert-info").children(".ajax_message").html("<p><strong>Please wait! </strong>Your action is in proccess...</p>"), isset(t = $(i).find("input[type=submit]").html()) || (t = $(i).find("button[type=submit]").html()), $(i).find("input[type=submit]").html('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Please wait'), $(i).find("button[type=submit]").html('<i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Please wait'), $(i).find("input[type=submit]").attr("disabled", "disabled"), $(i).find("button[type=submit]").attr("disabled", "disabled")
            },
            success: function(e) {
                $(i).find("input[type=submit]").removeAttr("disabled"), $(i).find("button[type=submit]").removeAttr("disabled"), $(i).find("input[type=submit]").html(t), $(i).find("button[type=submit]").html(t), $("#wait-div").hide(), $(i).find(".alert").removeClass("alert-success").removeClass("alert-danger").removeClass("alert-info"), e.restore_error ? ($(i).find(".alert").show(), $(i).find(".alert").html(e.restore_error)) : e.message ? (e.success ? toastr[e.status](e.message, "Notifications") : e.status && toastr[e.status](e.message, "Notifications"), e.messageNot ? $(i).find(".alert").fadeOut(100) : ($(i).find(".alert").fadeIn(200), e.success ? ($(i).find(".alert").fadeIn(), $(i).find(".alert").addClass("alert-success").children(".ajax_message").html(e.message)) : ($(i).find(".alert").fadeIn(), $(i).find(".alert").addClass("alert-danger").children(".ajax_message").html(e.message)))) : $(i).find(".alert").fadeOut(100), e.set_table && $("#edit_set").html(e.set_table), e.effect && ($("#cb").html(e.current_balance), $("#wa").html(e.withdrawl)), 1 == e.reload && location.reload(), e.resetform && $(i).resetForm(), e.url && (window.location.href = e.url), e.parentUrl && (window.top.location.href = e.parentUrl), e.selfReload && window.location.reload(), e.slideToThisDiv && slideToDiv(e.divId), e.slideToTop && slideToTop(), e.scrollToThisForm && slideToElement(i), e.ajaxPageCallBack && (e.formid = i, ajaxPageCallBack(e)), e.popup && popup(e.mobileno), e.ajaxPageCallBackData && (e.formid = i, ajaxPageCallBackData(e)), e.hideModel && setTimeout(function() {
                    $(".modal").modal("hide")
                }, 500), setTimeout(function() {
                    $(i).find(".ajax_report").fadeOut(1e3)
                }, 7e3)
            },
            error: function(e) {
                $(i).find(".alert").fadeOut(100), $(i).find("input[type=submit]").removeAttr("disabled"), $(i).find("button[type=submit]").removeAttr("disabled"), $(i).find("input[type=submit]").html(t), $(i).find("button[type=submit]").html(t);
                var a = e.responseJSON;
                $.each(a, function(e, t) {
                    console.log(e + " => " + t);
                    var a = '<label class="error formmessage error2" for="' + e + '"  style="color:red">' + t + "</label>";
                    $(i).find('input[name="' + e + '"], select[name="' + e + '"],textarea[name="' + e + '"]').last().addClass("inputTxtError").after(a)
                })
            }
        }), !1
    }), $(document).on("click", ".alert .close", function(e) {
        $(this).closest(".ajax_report").hide(), $(this).closest(".alert").hide()
    })
});