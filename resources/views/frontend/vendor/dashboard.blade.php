@extends('frontend/vendor/layouts/default')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@section('content')
   <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Dashboard</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">Dashboard</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col-xl-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                            <a href="{{url('vendor/orders')}}">
                                                <div class="media">
                                                    <div class="media-body overflow-hidden">
                                                        <p class="text-truncate font-size-15 mb-2">Active Orders</p>
                                                        <h4 class="mb-0">
                                                        {{ $total_active_orders ?? '' }}</h4>
                                                    </div>
                                                    <div class="text-success">
                                                        <i class="ri-shopping-cart-line font-size-24"></i>
                                                    </div>
                                                </div>
                                            </a>
                                            </div>

                                            <div class="card-body border-top py-3">
                                                <div class="text-truncate">
                                                    {{-- <span class="badge badge-soft-success font-size-11">2.4% </span>
                                                    <span class="text-muted ml-2">From yesterday</span> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                       <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <a href="{{url('vendor/accept-orders')}}">
                                                <div class="media">
                                                    <div class="media-body overflow-hidden">
                                                        <p class="text-truncate font-size-15 mb-2">Orders Fulfilled</p>
                                                        <h4 class="mb-0">{{$total_accepte_orders ?? ''}}</h4>
                                                    </div>
                                                    <div class="text-danger">
                                                        <i class="ri-shopping-cart-line font-size-24"></i>
                                                    </div>
                                                </div>
                                                </a>
                                            </div>

                                            <div class="card-body border-top py-3">
                                                <div class="text-truncate">
                                                    {{-- <span class="badge badge-soft-danger font-size-11">2.4% </span>
                                                    <span class="text-muted ml-2">From yesterday</span> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                                 <div class="media">
                                                    <div class="media-body overflow-hidden">
                                                       <a href=""> <p class="text-truncate font-size-15 mb-2">Orders Rejected</p></a>
                                                        <h4 class="mb-0">{{$totalRejectedOrders ?? ''}}</h4>
                                                    </div>
                                                    <div class="text-danger">
                                                        <i class="ri-shopping-cart-line font-size-24"></i>
                                                    </div>
                                                </div>
                                             </div>
                                            <div class="card-body border-top py-3">
                                                <div class="text-truncate">
                                                    {{-- <span class="badge badge-soft-danger font-size-11">2.4% </span>
                                                    <span class="text-muted ml-2">From yesterday</span> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->

                                <div class="card">
                                    <div class="card-body">
                                        <div class="float-right d-none d-md-inline-block">
                                            <div class="btn-group mb-2">
                                                <button type="button" class="btn btn-sm btn-light">Today</button>
                                                <button type="button" class="btn btn-sm btn-light active">Weekly</button>
                                                <button type="button" class="btn btn-sm btn-light">Monthly</button>
                                            </div>
                                        </div>
                                        <h4 class="card-title mb-4">Revenue Analytics</h4>
                                        <div>
                                            <div id="line-column-chart" class="apex-charts" dir="ltr"></div>
                                        </div>
                                    </div>

                                    <div class="card-body border-top text-center">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <p class="text-muted text-truncate mb-0"><i class="mdi mdi-circle text-primary font-size-10 mr-1"></i>This month</p>
                                                <div class="d-inline-flex" style="padding-top:5px;">
                                                    <h5 class="mr-2"><i class="fa fa-inr"></i>12,253</h5>
                                                    <div class="text-success">
                                                        <i class="mdi mdi-menu-up font-size-14"> </i>2.2 %
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="mt-4 mt-sm-0">
                                                    <p class="mb-2 text-muted text-truncate"><i class="mdi mdi-circle text-primary font-size-10 mr-1"></i> This Year :</p>
                                                    <div class="d-inline-flex">
                                                        <h5 class="mb-0 mr-2"><i class="fa fa-inr" aria-hidden="true"></i> 34,254</h5>
                                                        <div class="text-success">
                                                            <i class="mdi mdi-menu-up font-size-14"> </i>2.1 %
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="mt-4 mt-sm-0">
                                                    <p class="mb-2 text-muted text-truncate"><i class="mdi mdi-circle text-success font-size-10 mr-1"></i> Previous Year :</p>
                                                    <div class="d-inline-flex">
                                                        <h5 class="mb-0"><i class="fa fa-inr" aria-hidden="true"></i> 32,695</h5>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="float-right">
                                            <select class="custom-select custom-select-sm">
                                                <option selected>May</option>
                                                <option value="1">Apr</option>
                                                <option value="2">Mar</option>
                                                <option value="3">Feb</option>
                                                <option value="4">Jan</option>
                                            </select>
                                        </div>
                                        <h4 class="card-title mb-4">Sales Analytics</h4>

                                        <div id="donut-chart" class="apex-charts"></div>

                                        <div class="row">
                                            <div class="col-4">
                                                <div class="text-center mt-4">
                                                    <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-primary font-size-10 mr-1"></i> Medicines</p>
                                                    <h5>42 %</h5>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-center mt-4">
                                                    <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-success font-size-10 mr-1"></i> Devices</p>
                                                    <h5>26 %</h5>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="text-center mt-4">
                                                    <p class="mb-2 text-truncate"><i class="mdi mdi-circle text-warning font-size-10 mr-1"></i> Surgicals</p>
                                                    <h5>32 %</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-body">
                                        <div class="dropdown float-right">
                                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                                <i class="mdi mdi-dots-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <!-- item-->
                                                <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                                <!-- item-->
                                                <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                                <!-- item-->
                                                <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                                <!-- item-->
                                                <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                            </div>
                                        </div>

                                        <h4 class="card-title mb-4">Earning Reports</h4>
                                        <div class="text-center">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div>
                                                        <div class="mb-3">
                                                            <div id="radialchart-1" class="apex-charts"></div>
                                                        </div>

                                                        <p class="text-muted text-truncate mb-2">Weekly Earnings</p>
                                                        <h5 class="mb-0"><i class="fa fa-inr" aria-hidden="true"></i>2,523</h5>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="mt-5 mt-sm-0">
                                                        <div class="mb-3">
                                                            <div id="radialchart-2" class="apex-charts"></div>
                                                        </div>

                                                        <p class="text-muted text-truncate mb-2">Monthly Earnings</p>
                                                        <h5 class="mb-0"><i class="fa fa-inr" aria-hidden="true"></i>12,253</h5>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

@endsection

@section('footer_scripts')
 
        <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
        <script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>
 
        <!-- apexcharts -->
        <script src="{{asset('public/vendor/assets/libs/apexcharts/apexcharts.min.js')}}"></script>


        <!-- Required datatable js -->
        <script src="{{asset('public/vendor/assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script href="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
        
        <!-- Responsive examples -->
        <script src="{{asset('public/vendor/assets/libs/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>

        <script src="{{asset('public/vendor/assets/libs/datatables.net-bs4/js/responsive.bootstrap4.min.js')}}"></script>

        <script src="{{asset('public/vendor/assets/js/pages/dashboard.init.js')}}"></script>
 
        <script src="{{asset('public/vendor/assets/js/app.js')}}"></script>

        


@stop