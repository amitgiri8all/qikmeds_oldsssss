@extends('frontend/vendor/layouts/default')
@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

<div class="page-content">
<div class="container-fluid">

<!-- start page title -->
<div class="row">
<div class="col-12">
    <div class="page-title-box d-flex align-items-center justify-content-between">
        <h4 class="mb-0">My Subscriptions</h4>

        <div class="page-title-right">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                <li class="breadcrumb-item active">My Subscriptions</li>
            </ol>
        </div>

    </div>
</div>
</div>
<!-- end page title -->

<div class="row">
<div class="col-xl-12">
    <div class="card">
        <div class="card-body">
            <div class="subscription_blk cmn_area">
                <h3>Qikmeds Premium Pharmacy</h3>
                <h4>Reduce your medical expenses with our premium membership benefits</h4>
              <div class="row">
             {{--    @foreach($subscription_meta as $key=>$val)
                <div class="col-sm-4">
                    <div class="subscription_benefts">

                     @php
                     if(!empty($val->image)){
                     $url = $val->image;
                     }
                     else{
                     $url = asset('/public/assets/images/no-image.jpg');
                     }
                     @endphp
                        <i><img src="<?php echo $url; ?>" width="50px" height="50px"></i>
                        <h5>{{$val->title}}</h5>
                        <p>{{$val->description}}</p>
                    </div>
                </div> 
                @endforeach
                --}}
             <!--    <div class="col-sm-4">
                    <div class="subscription_benefts">
                        <i><img src="assets/images/benefit-2.png"></i>
                        <h5>Priority Order Routing</h5>
                    </div>
                </div> -->
                <!-- <div class="col-sm-4">
                    <div class="subscription_benefts">
                        <i><img src="assets/images/benefit-3.png"></i>
                        <h5>Multiple Device Access</h5>
                    </div>
                </div> -->
              </div>
            </div>
        </div>
    </div>
    <div class="action_btns mem_btn">
       <!--  <button type="button" class="btn cmn_btn btn-primary">Get Qikmeds Pharmacy Membership</button> -->
       <a href="{{route('document.subscription-plan')}}" class="btn cmn_btn btn-primary">Get Qikmeds Pharmacy Membership</a>
    </div>



    <div class="card">
        <div class="card-body">

            <div id="accordion" class="custom-accordion">
                @foreach($subscription_faq as $key=>$val)
                <div class="card mb-1 shadow-none">
                    <a href="#collapseOne{{$val->id}}" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
                        <div class="card-header" id="headingOne{{$val->id}}">
                            <h6 class="m-0">{{$val->question}}<i class="mdi mdi-minus float-right accor-plus-icon"></i></h6>
                        </div>
                    </a>

                    <div id="collapseOne{{$val->id}}" class="collapse show" aria-labelledby="headingOne{{$val->id}}" data-parent="#accordion">
                        <div class="card-body">
                            <p>{{$val->answer}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- <div class="card mb-1 shadow-none">
                    <a href="#collapseTwo" class="text-dark collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
                        <div class="card-header" id="headingTwo">
                            <h6 class="m-0"> How long is my Qikmeds Premium Pharmacy membership valid for? <i class="mdi mdi-minus float-right accor-plus-icon"></i></h6>
                        </div>
                    </a>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            <p>Your Qikmeds Premium Pharmacy membership is valid for 3 or 6 months from the enrollment date of the program. Log in to know more</p>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="card mb-0 shadow-none">
                    <a href="#collapseThree" class="text-dark collapsed" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree">
                        <div class="card-header" id="headingThree">
                            <h6 class="m-0">When will I get the cashback?<i class="mdi mdi-minus float-right accor-plus-icon"></i></h6>
                        </div>
                    </a>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            <p>Cashback will be credited into your Qikmeds wallet within 48 hours of order delivery.</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>


</div>
</div>

</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->



@endsection
{{-- page level scripts --}}
@section('footer_scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>