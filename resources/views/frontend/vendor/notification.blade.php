@extends('frontend/vendor/layouts/default')
@section('content')
   <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Notification</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Qikmeds</a></li>
                                            <li class="breadcrumb-item active">Notification</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                              <div class="row">
                        @foreach($alert_notification as $key => $value)
                        
                        <div class="col-sm-4" style="padding-top: 30px;"> 
                        <div class="card " style="width: 18rem;">
                          <div class="card-body">
                            <h6 >Date <td>{{$value->created_at}}</td></h6>

                            <tr><td>Notification</td> <br></tr>

                           <?php 
                            if(isset($value->data)){
                            $myArray = json_decode($value->data, true);
                            //print_r($myArray);
                            ?>
                           
                            <p class="card-text"> Message : <td>{{$myArray['message'] ?? ' ' }}</td></p>
                          
                            <p class="card-text"> Status:   <td></td></p>
                          <?php } ?>
                          </div>
                        </div>
                        </div>
                     
                         @endforeach
                         </div>
  
                   
                        <!-- end row -->                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                Copyright © 2021 Qikmeds. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

@endsection

@section('footer_scripts')
 
        <script src="{{asset('public/vendor/assets/libs/metismenu/metisMenu.min.js')}}"></script>
        <script src="{{asset('public/vendor/assets/libs/simplebar/simplebar.min.js')}}"></script>
 
        <!-- apexcharts -->
        <script src="{{asset('public/vendor/assets/libs/apexcharts/apexcharts.min.js')}}"></script>


        <!-- Required datatable js -->
        <script src="{{asset('public/vendor/assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script href="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
        
        <!-- Responsive examples -->
        <script src="{{asset('public/vendor/assets/libs/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>

        <script src="{{asset('public/vendor/assets/libs/datatables.net-bs4/js/responsive.bootstrap4.min.js')}}"></script>

        <script src="{{asset('public/vendor/assets/js/pages/dashboard.init.js')}}"></script>
 
        <script src="{{asset('public/vendor/assets/js/app.js')}}"></script>


@stop