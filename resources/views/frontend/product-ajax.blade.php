<div class="row">
@if(isset($productDetail) && count($productDetail)>0)
  @foreach($productDetail as $key=> $value)
    @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp
  <div class="col-sm-4">
                    <div class="product_card">
                        <?php
                            if($value->image){
                             $url =$value->image;
                            }else{
                             $url = $url= asset('/public/img_not_available.png');
                           }
                        ?>
                        <div class="product_img"><a href="{{url('/product-details/'.$value->slug)}}"><img style="width: 300px; height: 150px;" src="{{$value->image}}" alt="product"></a></div>
                        <div class="product_info">
                        @if(Auth::guard('web')->user())
                              @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  
                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav not_login"><i class="ti ti-heart"></i></a></div>
                            @endif
                            <small>{{$value->salt}}</small>
                            <h3><a href="{{url('/product-details/'.$value->slug)}}">{{$value->medicine_name}}</a></h3>
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            {{-- <div class="price_blk">
                                <h4>Rs {{$value->mrp}} @if(!empty($value->sale_price)) </span> @endif</h4>
                            </div> --}}
                             <div class="price_blk">
                                @if(!empty($value->sale_price))
                                <h4>Rs {{$value->sale_price}} <span class="strike">Rs {{$value->mrp}}</span></h4>
                                @else
                                <h4>Rs {{$value->mrp}} </h4>
                                @endif
                            </div>
                             <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                                    @else 

                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>    
                        </div>
                    </div>
   </div>
 @endforeach()
@else
    <h3>Comming Soon</h3>
@endif

</div>