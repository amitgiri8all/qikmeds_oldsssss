@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
{{--   <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  --}}@stop

  {{-- Page content --}}
  <style type="text/css">
      .pagination_blk .pagination div.flex {
display: none;
}
.pagination_blk .pagination span {
display: inline-block;
}
.pagination_blk .pagination span.relative, .pagination_blk .pagination span a {
display: inline-block;
padding: 8px 15px !important;
box-shadow: none !important;
}
.pagination_blk .pagination p {
text-align: center;
}
.pagination_blk .pagination {
justify-content: center !important;
}
.pagination svg {
width: 20px;
}

.loader-brand {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('http://172.105.36.210/qikmeds/public/assets/images/loader.gif') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
  </style>
@section('content')

<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                <div class="col-sm-3">
                    <div class="product_filter_blk">
                        <div class="category_filter_blk">
                            <h2>Categories</h2>
                            <div class="accordion" id="categories">
                                  <a  href="{{url('/list/all')}}">&nbsp;&nbsp; All</a>
                           @foreach($allCategories as $cat)
                                <div class="card">
                                    <div class="card-header" id="heading1">
                                        <h3 class="mb-0">
                                             <a  class="btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$cat->id}}"><i class="ti ti-angle-right"></i> {{$cat->category_name}}</a>
                                        </h3>
                                    </div>
                                    <div id="collapse{{$cat->id}}" class="collapse @if(request('cate_slug') == $cat->slug) show @endif" aria-labelledby="heading1" data-parent="#categories">

                                      <?php 
                                        //$url_path = collect(request()->segments())->last() ;
                                        $url_path = URL::current();

                                       ?>
                                        <div class="card-body">
                                            <ul>
                                            @foreach($cat->sub_categorys as $sub_cat)
                                                <li id="subcategoryList" class="{{ Request::Is('http://localhost/qikmeds/list/'.$sub_cat->category_name)== $url_path  ? 'active' : '' }}" >
                                                    <a  href="{{url('/list/'.$cat->slug.'/'.$sub_cat->slug)}}"  @if(request('sub_cate_slug') == $sub_cat->slug) class="text-info" @endif >{{$sub_cat->category_name}}</a>
                                                </li>
                                             @endforeach()
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach()
                            </div>
                        </div>

                        <div class="category_filter_blk">
                            <h2>Filters</h2>
                            <div class="filter_detail_blk">
                                <div class="filter_blk">
                                    <h3>Availability</h3>
                                    <ul>
                                        <li>
                                            <span class="checkbox_btn">
                                                <input type="checkbox" name="c-0" id="c-0">
                                                <label for="c-0">Exclude out of stock ({{$out_of_stock}})</label>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                               
                                <div class="filter_blk">
                                    <h3>Manufacturers</h3>
                                    <input type="text" class="form-control" name="manufacturers_name" placeholder="Search Manufacturers" id="manufacturers_name">
                                   <form method="GET" id="manufacturer">  
                                    <ul id="manufacturersdata">
                                        @foreach($manufacturer as $key=>$value)
                                            <li>
                                            <span class="checkbox_btn">
                                            <input type="checkbox" value="{{$value->id}}" name="manufacturer[]" id="c-a{{$value->id}}">
                                            <label for="c-a{{$value->id}}">{{$value->manufacturer_name}} <small>({{$value->ManTot}})</small></label>
                                            </span>
                                            </li>
                                        @endforeach
                                    </ul>
                                   </form> 
                                </div>

                                <div class="filter_blk">
                                    <h3>Discount</h3>
                                    <ul>
                                        <li>
                                            <span class="checkbox_btn">
                                                <input type="checkbox" name="c-k" id="c-k">
                                                <label for="c-k">Less than 10% <small>(1500)</small></label>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="checkbox_btn">
                                                <input type="checkbox" name="c-l" id="c-l">
                                                <label for="c-l">10% and above <small>(300)</small></label>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="checkbox_btn">
                                                <input type="checkbox" name="c-m" id="c-m">
                                                <label for="c-m">20% and above <small>(100)</small></label>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="checkbox_btn">
                                                <input type="checkbox" name="c-n" id="c-n">
                                                <label for="c-n">30% and above <small>(50)</small></label>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-9">
                    <div class="product_listing_section">
                        <div class="listing_bnr"><img src="{{asset('/public/1628178709_Category_web.jpg')}}" alt="banner"></div>
                        <div class="product_listing_content">
                            <div class="product_view">
                                <p>Showing <span>40</span> of <span>172</span> items</p>
                                <div class="custom-slct" id="select-box">
                                    <span>Sort By</span>
                                    <ul class="listing" id="sortBy">
                                        <li id="rel" class="selected"><a>Relevance</a></li>
                                        <li id="1"><a>Average Customer Rating</a></li>
                                        <li id="2"><a>Price: Low to High</a></li>
                                        <li id="3"><a>Price: High to Low</a></li>
                                        <li id="4"><a>Discount</a></li>
                                    </ul>
                                </div>
                            </div></div><div></div>

        <div class="product_listing" id="product_list">
            <div class="row">
@if(isset($productDetail) && count($productDetail)>0)
  @foreach($productDetail as $key=> $value)
    @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp



  <div class="col-sm-4">
                    <div class="product_card">
                        <?php

                            if($value->image){
                             $url =$value->image;
                            }else{
                             $url = $url= asset('/public/img_not_available.png');
                           }
                        ?>
                        <div class="product_img"><a href="{{url('/product-details/'.$value->slug)}}"><img style="width: 300px; height: 150px;" src="{{$value->image}}" alt="product"></a></div>
                        <div class="product_info">
                        @if(Auth::guard('web')->user())
                              @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  
                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav not_login"><i class="ti ti-heart"></i></a></div>
                            @endif
                            <small>{{$value->salt}}</small> 
                            <h3><a href="{{url('/product-details/'.$value->slug)}}">{{$value->medicine_name}}</a></h3>
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                    <li><a href="#"><i class="lar la-star"></i></a></li>
                                </ul>
                            </div>
                            {{-- <div class="price_blk">
                                <h4>Rs {{$value->mrp}} @if(!empty($value->sale_price)) @endif</h4>
                            </div> --}}
                            <div class="price_blk">
                                @if(!empty($value->sale_price))
                                <h4>Rs {{$value->sale_price}} <span class="strike">Rs {{$value->mrp}}</span></h4>
                                @else
                                <h4>Rs {{$value->mrp}} </h4>
                                @endif
                            </div>

                             <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                                    @else 

                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>    
                        </div>
                    </div>
   </div>
 @endforeach()
@else
    <h3>Comming Soon</h3>
@endif

</div></div>


            </div>
        </div>

                            <div class="pagination_blk">
                                <nav aria-label="Page navigation example">
                                  <ul class="pagination justify-content-end">
                                    <?php echo $productDetail->render(); ?>
                                  </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 

  <div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-body" style="margin-top: 80px;">
        <div id="ajax_loader">
            <img src="https://www.govindfoundation.com/assets/default/images/103.gif" style="display: block; margin-left: auto; margin-right: auto;">

        </div>
    </div>
</div>



@stop


 @section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
<script type="text/javascript">
var purl ="{{URL::to('product-ajax')}}";
var burl ="{{URL::to('brand-ajax')}}";
var murl ="{{URL::to('manufacturer-ajax')}}";
var surl ="{{URL::to('sorting-ajax')}}";

var token    = '{{ csrf_token() }}';

$('input:checkbox,input:radio,select').on('click',function(){
     page = 2;
     //alert('adsfasd')
     $("#overlay").fadeIn(100);　
        $.ajax({
          url: purl,
          type: "GET",
          data: $('#manufacturer,#brand').serialize(),
          dataType: "json",
           beforeSend: function(){
            $("#loading-overlay").show();
        },
          success: function(data) {
             $("#overlay").fadeOut(1000);   
                $('#product_list').html(data.content);
                $('#no_of_product').html(data.countnew);
             },
            complete: function(){
                $("#overlay").fadeOut(1000);
            },
        });
}); 

$('#brand_name').on('keypress',function(){
    var brand_name = $('#brand_name').val();
   // alert('brand_name')
         $.ajax({
          url: burl,
          type: "POST",
          data: {'_token': token,brand_name:brand_name},
          dataType: "json",
           beforeSend: function(){
            $("#loading-overlay").show();
        },
          success: function(data) {
            // $("#overlay").fadeOut(1000);   
                $('#branddata').html(data.content);
                //$('#no_of_product').html(data.countnew);
             },
            complete: function(){
                   //  $(".loader-brand").show();
            },
        });
});

$('#manufacturers_name').on('keypress',function(){
    var manufacturers_name = $('#manufacturers_name').val();
          $.ajax({
          url: murl,
          type: "POST",
          data: {'_token': token,manufacturers_name:manufacturers_name},
          dataType: "json",
           beforeSend: function(){
            $("#loading-overlay").show();
        },
          success: function(data) {
            // $("#overlay").fadeOut(1000);   
                $('#manufacturersdata').html(data.content);
                //$('#no_of_product').html(data.countnew);
             },
            complete: function(){
                   //  $(".loader-brand").show();
            },
        });
});


    $("#sortBy li").click(function() {
    $("#overlay").fadeIn(100);　
     var sort  = this.id;
    // alert(sort);
          $.ajax({
          url: purl,
          type: "GET",
          data: {'_token': token,sort:sort},
          dataType: "json",
           beforeSend: function(){
            $("#loading-overlay").show();
        },
          success: function(data) {
            $('#product_list').html(data.content);
             // console.log(data.content);
             },
            complete: function(){
                $("#overlay").fadeOut(1000);
            },
           
        });
    
});
</script>
<script>
    $('#categories').on('change', function () {
    $("#subcategoryList").attr('disabled', false); //enable subcategory select
   // $("#subcategoryList").val("");
   // $(".subcategory").attr('disabled', true); //disable all category option
    //$(".subcategory").hide(); //hide all subcategory option
    $(".parent-" + $(this).val()).attr('disabled', false); //enable subcategory of selected category/parent
    $(".parent-" + $(this).val()).show(); 
});
</script>
@stop


