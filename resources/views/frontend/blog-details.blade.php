@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

{{-- <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 --}} <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/js-offcanvas.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
<style type="text/css">
    .c-offcanvas--right {
  height: 100%;
  width: 29em !important;
  right: 0;
  transform: translate3d(400px, 0, 0);
}
a.js-offcanvas-trigger.c-button.cmn_btn {
    text-align: -webkit-center;
    width: 21em;
    margin-bottom: 1em;
}
 
</style>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Blog Details</li>
              </ol>
            </nav>
        </div>
        <div class="cart_blk">
              <div class="row">
                  <div class="col-sm-3"><img src="<?php echo $blog_details->image; ?>" alt="blog"></div>
                  <div class="col-sm-9">
                    <h3>{{$blog_details->title;}}</h3>
                    <p>{!!$blog_details->description;!!}</p></div>
                                <small>Posted on: {{date("d-MM-Y",strtotime($blog_details->created_at));}}</small>
              </div>
        </div>
        
    </div>
</section>
 
 

@stop
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script>
 
var loadFile = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};
</script>

@stop
