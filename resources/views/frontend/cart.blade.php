@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
{{-- <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 --}} 
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cart')}}">My Cart</a></li>
              </ol>
            </nav>
        </div>
        <div class="cart_blk">
            <div class="row">
                <div class="col-sm-9">
                    <div class="cart_detail_blk">
                      @if(session('cart'))
                       <h2><a href="javascript:void(0) " class="clear_cart">Clear Cart </a></h2> @endif
                        <div class="cart_items_detail">
                            @php $total=0; @endphp

 <div id="cartdel">
@if(!empty($subscriptionplan))
    <div class="card col-sm-12" style="background:#c1d5e7; margin-bottom: 25px;">
                                <div class="card-body">
                                    <div class="radio_sbtn" style="float:left;">
                                         <label for="sfad">{{$subscriptionplan->duration}}</label>
                                    </div>
                                    <div class="radio_btn" style="float:right;">
                                        <span ><img src="https://assets.pharmeasy.in/web-assets/dist/6aa9e076.png" style="position: absolute;">{{$subscriptionplan->discount}} Off </span> &nbsp;&nbsp;&nbsp;
                                        <span>Rs. {{$subscriptionplan->sale_price}} </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span class="strike">Rs. {{$subscriptionplan->price}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                     <a href="javascript:void(0);" style="color: red;" data-id="{{$subscriptionplan->id}}" class="remove_plan"><i class="fa fa-trash"></i></a>
                                    </div>
                                            
                                </div>
                            </div>

@endif



                    @if(session('cart'))
                        @foreach(session('cart') as $key => $details)
                        <?php $price =  str_replace("₹","",$details['price']);?>
                         @php $total = ((int)$price * (int)$details['quantity']); @endphp
                         @if(Auth::guard('web')->user())
                         <?php
                          $chekc_save_for_later = App\Models\SaveForLater::chekc_save_for_later($details['id']);
                        // echo "<pre>"; print_r($data); die();
                          ?> 
                         @endif 
                         <?php  $data = App\Helpers\ProductHelper::product_session($details['id']);?> 
                                <div class="cart_item">
                                    <div class="item_img">
                                     <a href="@{{url('/product-details/'.$data->slug)}}">   
                                        <img style="width:75px; height:75px;" src="{{$data->image ?? ''}}" alt="product"></div>
                                    <div class="item_info">
                                        <h4> {{ @$details['name']; }}</h4>
    <!--                                     <em>Only 2 left in stock</em>
     -->                                    <span>Mfr: {{$data->manufacturer_name??''}}</span>
                                        <div class="prodct_price_blk">
                                            <h3>₹ {{$data->sale_price??''; }}</h3>
                                            <span class="strike">₹ {{$data->mrp??''; }}</span>
                                            <small>{{$data->discount??'';}}% Off {{$data->prescription??''}} </small>
                                        </div>
                                        </a>
                                    </div>
                                <div class="item_qty">
                                   <div class="cart-table__quantity input-numbers number{{$details['id']}}">
                                        <input class="form-control input-number__input" name="quantity" type="number" id="number{{$details['id']}}" value="{{$details['quantity']}}" min="1">
                                        <div value="Decrease Value" data-id="{{$details['id']}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$details['id']}})"></div>
                                        <div value="Increase Value" data-id="{{$details['id']}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$details['id']}})" ></div>
                                    </div>
                                     <div class="prodct_optn">
                                      @if(Auth::guard('web')->user())  
                                        <a href="javascript:void(0);" @if($chekc_save_for_later==0)style="color: black;" @endif data-id="{{$details['id']}}" class="save_for_later"><i class="fa fa-heart"></i>Save for Later </a>

                                      @else
                                      <a href="javascript:void(0);" class="not_login"><i class="fa fa-heart"></i>Save for Later </a>
                                      @endif  
                                        <a href="javascript:void(0);" data-id="{{$details['id']}}" class="remove-from-cart"><i class="fa fa-trash"></i>Remove</a>
                                    </div>
<!--
 -->
<!--                                       <p><a href="#"><i class="fa fa-heart"></i>Save for Later </a></p>
 -->                                      <p>Delivery by {{App\Helpers\Helper::get_delivery_days();}}</p>
                                </div>
                            </div>
                        @endforeach
                         @else
                            @include('frontend.layouts.cart-blank')
                         @endif
                            </div>
                        </div>
                    </div>
                     <div class="deal_blk cmn_blk">

                        <div class="title_blk">
                            <h2>Most Selling Products</h2>
                        </div>
                        <div class="product_blk">
                          <div class="slider autoplay-7">
                            @foreach(App\Helpers\ProductHelper::ProductListByType(3) as $key=>$value)
                              @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp
                          <div>
                        <div class="product_card">
                            <div class="product_img"><a  href="{{url('/product-details/'.$value->slug)}}"><img style="width: 300px; height: 150px;" src="{{$value->image??''}}" alt="product"></a></div>
                            <div class="product_info">
                           @if(Auth::guard('web')->user())
                                                       @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  

                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav not_login"><i class="ti ti-heart"></i></a></div>
                            @endif

                                <small>{!! substr($value->uses,0,10)!!} - {!!substr($value->type_of_sell,0,30)!!}</small>
                                <h3><a href="{{url('/product-details/'.$value->slug)}}">{{$value->medicine_name}}</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                <h4>Rs {{$value->mrp}} @if(!empty($value->sales_price))<span class="strike">Rs 400</span>@endif</h4>
                                </div>

                                    <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>

                                    @else 

                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>   
                               </form>
                            </div>
                        </div>
                    </div>
                            @endforeach

                            </div>
                        </div>
                    </div> 
                        <div class="title_blk" style="display:none;">
                            <h2>Save for Later</h2>
                        </div>
                        <div class="product_blk"  style="display:none;">
                            <div class="slider autoplay-7">
                            @foreach($product as $key=>$value)
                              @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp

                                <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img style="width:300px; height: 150px;" src="{{$value->ProductImage->image??''}}" alt="product"></a></div>
                            <div class="product_info">
                           @if(Auth::guard('web')->user())
                            @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  

                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav not_login"><i class="ti ti-heart"></i></a></div>
                            @endif

                                <small>- {{$value->Product->uses ?? '' }} - {{$value->Product->type_of_sell ?? ''}}</small>
                                <h3><a href="#">{{$value->Product->medicine_name ?? ''}} </a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                <h4>Rs  {{$value->Product->mrp ?? ''}}  @if(!empty($value->Product->sale_price ?? '' ))<span class="strike">Rs 400</span>@endif</h4>
                                </div>

                                    <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                                    @else 
                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>   
                               </form>
                            </div>
                        </div>
                    </div>
                            @endforeach

                            </div>
                        </div>
                 </div>

                <div class="col-sm-3">
                    <div class="order_smry_blk">

                      @if(session('cart'))
                        <div class="apply_code">
                            <a data-toggle="collapse"  href="#collapseExample"role="button" aria-expanded="false" aria-controls="collapseExample">
                                <span><img src="{{asset('public/frontend/images/offer.png')}}" alt="offer"></span>Apply Promo Code<i class="ti-angle-down"></i>
                            </a>
                        <div class="collapse" id="collapseExample">
                                 <?php  $session_data = session('coupan_id') ?>
                                <div class="card card-body">
                                    <div class="coupon_blk">
                                        <ul>

                                           @foreach($coupon_code as $key=>$value)
                                           
                                              <li class="promo_code">
                                                 <div class="">
                                                  <input type="hidden" class="coupon_id" name="coupon_id" id="coupon_id{{$value->id}}" value="{{$value->id}}">

                                                   {{-- <input type="radio" name="radio" id="r-{{$value->id}}" value="{{$value->id}}" @if($value->id == $session_data ) checked @endif  > --}}

   <div class="custom-control custom-radio">
  <input type="radio" class="custom-control-input" id="r-{{$value->id}}" name="groupOfDefaultRadios"
  value="{{$value->id}}" {{ ($value->id==$session_data)? "checked" : "" }}  >
  <label class="custom-control-label" for="r-{{$value->id}}">{{$value->code}}</label>
  <p>{{$value->name}}</p>
</div>


                                                   
                                                          {{--   <label for="r-{{$value->id}}">{{$value->code}}</label>
                                                            <p>{{$value->name}}</p> --}}
                                                        </div>
                                                    </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif


                        @php $siteSetting = App\Models\Sitesettings::first();
                        @endphp

                        <div class="free_delivery">Order above ₹{{$siteSetting->delivery_charge}} to save on delivery charges. Free delivery with cart value above ₹{{$siteSetting->min_price}}</div>

                        @php $total = 0 @endphp
                        @foreach((array) session('cart') as $id => $details)
                            <?php $price= str_replace('₹', '',$details['price']); ?>
                            @php $total += (int) $price * (int) $details['quantity']; @endphp
                        @endforeach

                      
                        <div class="order_price_blk">
                            
                            <div class="payment_details">
                              @if(Auth::guard('web')->user())
                              <h2>Payment Details</h2>
                                <ul>

                                    <li>
                                        <span>Item Total</span>
                                        <span id="tot">₹ {{number_format($total,2)}}</span>
                                    </li>
                                    <li>
                                        <span>Qikmeds Discount</span>
                                        <span>₹ {{$site_setting->qikmeds_discount;}}</span>
                                    </li>
                                    @if($site_setting->min_price > $total)
                                     @php $delivery_charge = $site_setting->delivery_charge; @endphp
                                    <li>
                                        <span>Delivery Charge</span>
                                        <span id="charge">₹{{$delivery_charge;}}</span>
                                    </li>
                                    @else
                                    <li>
                                        <span>Delivery Charge</span>
                                        @php $delivery_charge = 0; @endphp
                                        <span id="free">Free</span>
                                    </li>
                                    @endif
                                     <li>
                                        <span>Promo Discount</span>
                                        <span id="promo_dis"></span>
                                    </li> 
                                    @if(!empty($subscriptionplan))
                                     <li>
                                        <span>Qikmeds Subscription Plan.</span>
                                        <span id="subscription_plan">₹ {{$subscriptionplan->sale_price}}</span>
                                    </li>
                                    @endif

                                   <?php $total_amount = $total+$delivery_charge-$site_setting->qikmeds_discount+isset($subscriptionplan->sale_price); ?>
                                    <li class="total_amnt">
                                        <span>Total Amount</span>
                                        <span id="sum_total_amount">₹ {{number_format($total_amount,2);}}</span>
                                    </li>
                                </ul>
                                <?php
                                $total_saving = $total_amount-$site_setting->qikmeds_discount;
                                ?>
                                <div class="total_sav" id="total_sav">TOTAL SAVINGS <span class="total_saving"> ₹ {{number_format($site_setting->qikmeds_discount,2)}}</span></div>

                                @endif

 @if(session('cart'))
                                @if(Auth::guard('web')->user())
                                  <a href="{{route('order-review')}}"> <button type="submit" class="cmn_btn">Proceed</button></a>
                                 @else
                                  <a href="javascript:void(0)"> <button type="submit" class="cmn_btn not_login">Proceed</button></a>
                                @endif
 @endif
                            </div>
                        </div>

                        <div class="safety_txt">Products will be safely packed & Sanitized. Pay online for contactless delivery.</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@stop
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>

 <script type="text/javascript">
  

var remove_cart ="{{URL::to('remove-from-cart')}}";
var remove_plan ="{{URL::to('remove-plan')}}";
var redirect_url_login ="{{URL::to('customer/account/login')}}";

var promocode_calculation ="{{URL::to('promocode-calculation')}}";
var save_for_later ="{{URL::to('save-for-later')}}";

var token    = '{{ csrf_token() }}';



 </script>

@stop
