@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

{{-- <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 --}} <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/js-offcanvas.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
<style type="text/css">
    .c-offcanvas--right {
  height: 100%;
  width: 29em !important;
  right: 0;
  transform: translate3d(400px, 0, 0);
}
a.js-offcanvas-trigger.c-button.cmn_btn {
    text-align: -webkit-center;
    width: 21em;
    margin-bottom: 1em;
}
</style>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('cart')}}">Cart</a></li>
                <li class="breadcrumb-item active" aria-current="page">Order Review</li>
              </ol>
            </nav>
        </div>
        <div class="cart_blk">
<!--             <h1>Order Review</h1>
 -->            <div class="row">
                <div class="col-sm-9">
<!--                     <h2 class="review_title">Products</h2>
 -->                    <div class="cart_detail_blk review">

    @if(!empty($subscriptionplan))
    <div class="card col-sm-12" style="background:#c1d5e7; margin-bottom: 25px;">
                                <div class="card-body">
                                    <div class="radio_sbtn" style="float:left;">
                                         <label for="sfad">{{$subscriptionplan->duration}}</label>
                                    </div>
                                    <div class="radio_btn" style="float:right;">
                                        <span ><img src="https://assets.pharmeasy.in/web-assets/dist/6aa9e076.png" style="position: absolute;">{{$subscriptionplan->discount}} Off </span> &nbsp;&nbsp;&nbsp;
                                        <span>Rs.{{$subscriptionplan->sale_price}} </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span class="strike">Rs. {{$subscriptionplan->price}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     </div>
                                            
                                </div>
                            </div>

@endif
    @if(session('cart'))   
                        @foreach(session('cart') as $key => $details)
                        <?php $price =  str_replace("₹","",$details['price']);?>
                         @php $total = ((int)$price * (int)$details['quantity']); @endphp
                          <?php  $data = App\Helpers\ProductHelper::product_session($details['id']);
                      ?> 


                        <div class="cart_items_detail">
                            <div class="cart_item">
                                <div class="delivery_date">Delivery Estimate <span> {{App\Helpers\Helper::get_delivery_time();}}</span></div>
                                <div class="item_img"><img style="width:75px; height:75px;" src="{{$data->image ?? ''}}"> </div>
                                <div class="item_info">
                                    <h4>{{ @$details['name']; }}</h4>
                                    <div class="item_seller">
                                        <span>Mfr: {{$data->manufacturer_name??''}}</span>
                                         <small>Expiry:
                                            {{date(' F Y ',strtotime(@$data->product_expiry_date))}}
                                         </small>
                                    </div>
                                </div>
                                <div class="prodct_price_blk">
                                    <h3>₹ {{$data->sale_price??''; }}</h3>
                                    <span class="strike">₹ {{$data->mrp??''; }}</span>
                                    <p>Qty: <b>{{$details['quantity']}}</b></p>
                                </div>
                            </div>
                        </div>
            @endforeach
                         @else
                            <h3>Cart is Emapty</h3>
                         @endif

                    </div>
                    @php
                        $coupan_name = Session::get('coupan_name');
                        $coupan_code = Session::get('coupan_code');
                        $promo_val = Session::get('promo_val');
                    @endphp
                   

                    <div class="order_delivery_info">
                          <!-- ============== Message Section ======================= -->
             <!-- ============== Message Section ======================= -->
                     
                    </div>
                    <div class="order_delivery_info">
                        <h2>Customer Notes</h2>
                        <textarea class="form-control" rows="6" id="customer_note"></textarea>
                    </div>
                </div>
            <div class="col-sm-3">
                @if(!empty($patient_address)) 

                                    <div class="address_blk" style="background:rgba(36,174,177,0.08); margin-top: -30px; border: 1px dashed #24aeb1;">
                                        <h4>{{ucfirst($patient_address->patient_name);}}</h4> 

                                        <h5>{{$patient_address->mobile_number;}} Age:{{$patient_address->age;}} Years</h5>
                                         <span>{{$patient_address->sex;}}</span>

                                        <a class="js-offcanvas-trigger c-button" data-offcanvas-trigger="patient" href="#patient" role="button" aria-controls="right" aria-expanded="false"><span class="c-button__text">Change Patient Info</span></a>
                                    </div>
                            @endif
                              @if(empty($patient_address)) 
                        <a class="js-offcanvas-trigger c-button cmn_btn" data-offcanvas-trigger="patient" href="#patient" role="button" aria-controls="right" aria-expanded="false"><span class="c-button__text">Add Patient Info</span></a>
                        @endif 
 
             @if(!empty($promo_val))
        
                <div class="apply_code" style="margin-bottom: 25px; background: #e2eff3;">
                    <a data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span><img src="http://172.105.36.210/qikmeds/public/frontend/images/offer.png" alt="offer"></span>{{$coupan_code}}
                    </a>
                </div>
            @endif
                           @if(!empty($delivery_address)) 

                                    <div class="address_blk" style="background:rgba(36,174,177,0.08); border: 1px dashed #24aeb1;">
                                        <h4>{{ucfirst($delivery_address->name);}}</h4> 
                                        <h5>{{$delivery_address->mobile_number;}}</h5>
                                        <span>{{$delivery_address->address_type;}}</span>
                                        <p>{{ucwords($delivery_address->address_delivery);}} {{$delivery_address->locality;}} {{$delivery_address->city;}}, {{$delivery_address->state;}} - {{$delivery_address->pin_code;}}</p>
                                        <a class="js-offcanvas-trigger c-button" data-offcanvas-trigger="right" href="#right" role="button" aria-controls="right" aria-expanded="false"><span class="c-button__text">Change Address</span></a>

<!--                                         <a style="float: right;" class="js-offcanvas-trigger c-button" data-offcanvas-trigger="patient" href="#patient" role="button" aria-controls="right" aria-expanded="false"><span class="c-button__text">Patient Info</span></a>
 -->                                    </div>
                            @endif

                    <div class="order_smry_blk">
                        <div class="order_price_blk">
                        @if(empty($delivery_address)) 
                        <a class="js-offcanvas-trigger c-button cmn_btn" data-offcanvas-trigger="right" href="#right" role="button" aria-controls="right" aria-expanded="false"><span class="c-button__text">Add Delivery Address</span></a>
                        @endif 
                            <h2>Order Summary</h2>
                            <div class="payment_details">
                                <ul>
                                    @php $total = 0 @endphp
                                    @foreach((array) session('cart') as $id => $details)
                                        <?php $price= str_replace('₹', '',$details['price']); ?>
                                        @php $total += (int) $price * (int) $details['quantity']; @endphp
                                    @endforeach
                                    <li>
                                        <span>Item Total</span>
                                        <span id="tot">₹ {{number_format($total,2)}}</span>
                                    </li>
                                    <li>
                                        <span>Qikmeds Discount</span>
                                        <span>₹ {{$site_setting->qikmeds_discount;}}</span>
                                    </li>
                                    @if($site_setting->min_price > $total)
                                     @php $delivery_charge = $site_setting->delivery_charge; @endphp
                                    <li>
                                        <span>Delivery Charge</span>
                                        <span id="charge">{{$delivery_charge;}}</span>
                                    </li>
                                    @else
                                    <li>
                                        <span>Delivery Charge</span>
                                        @php $delivery_charge = 0; @endphp
                                        <span id="free">Free</span>
                                    </li>
                                    @endif
                                    <?php   
                                    $promo_val = Session::get('promo_val');
                                    $total_amt = Session::get('total_amt');
                                    $total_saving = Session::get('total_saving');
                                    ?>
                                    </li>
                                     @if(!empty($promo_val))
                                      <li>
                                        <span>Promo Discount</span>
                                        <span id="promo_dis">₹ {{$promo_val;}}</span>
                                    </li>
                                    @endif
                                     @if(!empty($subscriptionplan))
                                     <li>
                                        <span>Qikmeds Subscription Plan.</span>
                                        <span id="subscription_plan">₹ {{$subscriptionplan->sale_price}}</span>
                                    </li>
                                    @endif

                                  <?php $total_amount = $total+$delivery_charge-$site_setting->qikmeds_discount-$promo_val+isset($subscriptionplan->sale_price); ?>
                                    <li class="total_amnt">
                                        <span>Total Amount</span>
                                        <span id="sum_total_amount">₹ {{number_format($total_amount,2);}}</span>
                                    </li>

                                    <?php
                                    $total_savings = $site_setting->qikmeds_discount+$promo_val;
                                    ?>

                                </ul>
                                <div class="total_sav" id="total_sav">TOTAL SAVINGS <span>₹ {{number_format($total_savings,2)}}</span></div>
                                
                            @if(session('cart'))
                                @if(Auth::guard('web')->user())
                                <a href="{{route('checkout')}}"> <button type="submit" class="cmn_btn delivery_address_check">Pay Amount</button></a>
                                 @else
                                <a href="javascript:void(0)"> <button type="submit" class="cmn_btn not_login">Proceed</button></a>
                                @endif
                            @endif
                        </div>
                        </div>
                        <div class="safety_txt">Products will be safely packed & Sanitized. Pay online for contactless delivery.</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- Change Delivery Address ================================== -->
<aside class="js-offcanvas" data-offcanvas-options='{"modifiers":"right,overlay"}' id="right" role="complementary">
    <div class="address_change_blk">
        <h2>Change Delivery Address</h2>
        <div class="row">
             @foreach($addresses as $key=>$value)
                             <div class="col-sm-12">
                                <div class="address_blk"  style="padding-left: 43px;">
                                  <div id="select_address">

                                    <div class="radio_btn">
                                        <input type="radio" class="update_address" data-id="{{$value->id}}" name="radio" id="r-{{$value->id}}" {{ ($value->status=="1")? "checked" : "" }}>
                                        <label for="r-{{$value->id}}">&nbsp;</label>
                                    </div>
                                </div>
                                    <h4>{{$value->name}}</h4>
                                    <h5>{{$value->mobile_number}}</h5>
                                    <span>{{$value->address_type}}</span>

                                    <p>{{$value->address_delivery}},{{$value->locality}}<br> {{$value->city}}, {{$value->state}} - {{$value->pin_code}}</p>

                                </div>
                            </div>

                         @endforeach

            <div class="col-sm-12">
                <a href="#right1" class="cmn_btn js-offcanvas-trigger" data-offcanvas-trigger="right1" ><i class="ti-plus"></i> ADD NEW ADDRESS</a>
            </div>
          </div>
    </div>
    <button class="js-offcanvas-close" data-button-options='{"modifiers":"m1,m2"}'><i class="ti ti-close"></i></button>
</aside>
<!-- Change Delivery Address End ================================== -->
<!-- Add Deilervery info -->
<aside class="js-offcanvas" data-offcanvas-options='{"modifiers":"right,overlay"}' id="right1" role="complementary">
    <div class="address_change_blk">
        <h2>Add New Address</h2>
         <form class="ajax_form" id="delivery_address" action="{{route('user.save-delivery-address')}}" method="post">
        @csrf
                        <div class="user_address_blk cmn_area">
                          <div class="row">
                             <div class="form-group col-sm-12" style="display:none;">
                                <button class="cmn_btn" type="button"><i class="ti-target"></i> Use my Current Location</button>
                            </div>

                                    <div class="form-group col-sm-12">
                                        <label>Name <sup>*</sup></label>
                                        <input type="text" class="form-control" value="{{old('name')}}" name="name" placeholder="Name">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label>Mobile number <sup>*</sup></label>
                                        <input type="text" maxlength="10" class="form-control num" value="{{old('mobile_number')}}" name="mobile_number" placeholder="10 - digit mobile number">
                                    </div>

                                    <div class="form-group col-sm-6">
                                         <label>Pin Code <sup>*</sup></label>
                    <input type="text" class="form-control num getdetails" maxlength="6" value="{{old('pin_code')}}"  id="pin_code" name="pin_code" placeholder="Pincode">

                                    </div>
                <div class="form-group col-sm-6">
                         <label>City <sup>*</sup></label>
                        <input type="text" class="form-control num" id="city" value="{{old('pin_code')}}" name="pin_code" placeholder="city" disabled="disabled">
                    </div> 
                     <div class="form-group col-sm-12">
                         <label>State <sup>*</sup></label>
                        <input type="text" class="form-control num" id="state" value="{{old('pin_code')}}" name="state" placeholder="state" disabled="disabled">
                    </div>
               
                                    <div class="form-group col-sm-12">
                                         <label>Locality <sup>*</sup></label>
                                        <input type="text" class="form-control" value="{{old('locality')}}" name="locality" placeholder="Locality">
                                    </div>

                                    <div class="form-group col-sm-12">
                                         <label>Area and Street <sup>*</sup></label>
                                        <textarea class="form-control" rows="5" value="{{old('address_delivery')}}" name="address_delivery" placeholder="Address (Area and Street)"></textarea>
                                    </div>

                                   <!--  <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" value="{{old('city')}}" name="city" placeholder="City/District/Town">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" value="{{old('state')}}" name="state" placeholder="State">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="text" name="landmark" value="{{old('landmark')}}" class="form-control" placeholder="Landmark(Optional)">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" value="{{old('alternate_phone')}}" name="alternate_phone" placeholder="Alternate Phone (Optional)">
                                    </div>
 -->

                           <div class="col-sm-12">
                                <div class="user_txt">
                                    <label>Address Type</label>

                                    <div class="radio_btn">
                                        <input type="radio" value="home" name="address_type" id="r-4" value="home" checked="checked">
                                        <label for="r-4">Home</label>
                                    </div>

                                    <div class="radio_btn">
                                        <input type="radio" value="work" name="address_type" id="r-5">
                                        <label for="r-5">Work</label>
                                    </div>

                                    <div class="radio_btn">
                                        <input type="radio" value="others" name="address_type" id="r-6">
                                        <label for="r-6">Others</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-12 mgn_0">
                                <button type="submit" class="cmn_btn">Save</button>

                            </div>



                           </div>
                        </div>
                      </form>
        </div>
    </div>
    <button class="js-offcanvas-close" data-button-options='{"modifiers":"m1,m2"}'><i class="ti ti-close"></i></button>
</aside>
<!-- Change Delivery Address ================================== -->


<!-- Pasent info -->
<aside class="js-offcanvas" data-offcanvas-options='{"modifiers":"right,overlay"}' id="patient" role="complementary">
    <div class="address_change_blk">
        <h2>Change Patient Info</h2>
        <div class="row">
             @foreach($patient as $key=>$value)
                             <div class="col-sm-12">
                                <div class="address_blk"  style="padding-left: 43px;">
                                  <div id="select_address">

                                    <div class="radio_btn">
                                        <input type="radio" class="update_info_patient" data-id="{{$value->id}}" name="radios" id="p-{{$value->id}}" {{ ($value->status=="1")? "checked" : "" }}>
                                        <label for="p-{{$value->id}}">&nbsp;</label>
                                    </div>
                                </div>
                                    <h4>Name :{{$value->patient_name}}</h4>
                                    <h5>{{$value->mobile_number}}</h5>
                                    <span>Age : {{$value->age}}</span>

                                    <p>Sex:{{$value->sex}}</p>
                                </div>
                                <span class="card-delete"><button type="button" style="margin-top: 50px;" class=" dlt deleteRecord"  data-id="{{ $value->id }}" ><i class="ti-trash"></i></button></span>
                            </div>
                         @endforeach
            <div class="col-sm-12">
                <a href="#patientadd" class="cmn_btn js-offcanvas-trigger" data-offcanvas-trigger="patientadd" ><i class="ti-plus"></i> Add New Patient Info</a>
            </div>
          </div>
    </div>
    <button class="js-offcanvas-close" data-button-options='{"modifiers":"m1,m2"}'><i class="ti ti-close"></i></button>
</aside>

<!-- Addd patient  Info -->
<aside class="js-offcanvas" data-offcanvas-options='{"modifiers":"right,overlay"}' 
id="patientadd" role="complementary">
    <div class="address_change_blk">
        <h2>Add New Patient Info</h2>
         <form class="ajax_form" id="delivery_address" action="{{route('user.save-patient-info')}}" method="post">
        @csrf
                        <div class="user_address_blk cmn_area">
                          <div class="row">
                             <div class="form-group col-sm-12" style="display:none;">
                                <button class="cmn_btn" type="button"><i class="ti-target"></i> Use my Current Location</button>
                            </div>

                                    <div class="form-group col-sm-12">
                                        <label>Patients Name <sup>*</sup></label>
                                        <input type="text" class="form-control" value="{{old('patient_name')}}" name="patient_name" placeholder="Patients Name">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label>Age <sup>*</sup></label>
                                        <input type="text" maxlength="100" class="form-control num" value="{{old('mobile_number')}}" name="age" placeholder="Age">
                                    </div>

                                     
                                    <div class="form-group col-sm-12">
                                         <label>Sex<sup>*</sup></label>
                                        <select class="form-control" name="sex" id="sex">
                                            <option value="male">Male</option>
                                            <option value="female">FeMale</option>
                                        </select>
                                    </div>

                        

                                <div class="form-group col-sm-12">
                                    <label>Mobile number <sup>*</sup></label>
                                    <input type="text" maxlength="10" class="form-control num" value="{{old('mobile_number')}}" name="mobile_number" placeholder="10 - digit mobile number">
                                </div>

                                 <div class="form-group col-sm-12">
                                           <label>Prefered Time For Call<sup>*</sup></label>
                                            <select class="form-control" name="prefered_time_call" id="prefered_time_call">
                                                <option value="">Please Select Time For Call</option>
                                                <option value="morning">Morning</option>
                                                <option value="afternoon">After Noon</option>
                                                <option value="evening">Evening</option>
                                            </select>
                                </div>

                            <div class="form-group col-sm-12 mgn_0">
                                <button type="submit" class="cmn_btn">Save</button>

                            </div>



                           </div>
                        </div>
                      </form>
        </div>
    </div>
    <button class="js-offcanvas-close" data-button-options='{"modifiers":"m1,m2"}'><i class="ti ti-close"></i></button>
</aside>



@stop
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/js-offcanvas.pkgd.js')}}"></script>

<script type="text/javascript">

var select_address ="{{route('user.select-address')}}";
var select_patient ="{{route('user.select-patient')}}";
var delivery_address_check ="{{route('delivery_address_check')}}";
 var token = '{{ csrf_token() }}';

 $(document).on('click', '.update_address', function() {
            selected_id= $(this).attr("data-id");
         event.preventDefault();
          $.ajax({
            url:  select_address,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
                 //toastr[response.status]("Your delivery addresses updated successfully", "Cart");
                // $("#select_address").load(document.URL + " #select_address");
                  window.location.reload();
            }
            });
 });

$(document).on('click', '.update_info_patient', function() {
            selected_id= $(this).attr("data-id");
           // alert(selected_id)
        // event.preventDefault();
          $.ajax({
            url:  select_patient,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
                toastr[response.status]("Patient information added successfully", "Cart");
                // $("#select_address").load(document.URL + " #select_address");
                  window.location.reload();
            }
            });
 });

 $(document).on('click', '.delivery_address_check', function() {
         event.preventDefault();
          $.ajax({
            url:  delivery_address_check,
            type: "POST",
            data: {'_token': token},
            dataType: 'json',
            success: function(response) {
                if(response.redirect_url){
                    window.setTimeout(function(){window.location.href = response.redirect_url;},300);
                }
                toastr[response.status](response.message,response.status);
                $(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> '+response.message+'</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();            }
            });
 });
 $('body').on('click', '.close', function(){
        $(".main_error").html('').removeClass('alert-danger').hide();
     });
</script>

<script type="text/javascript">
   var delivery ="{{route('delivery-location.get-details.post')}}";

var token       = '{{ csrf_token() }}';
$('#loading').hide();
   $(document).ready(function(){
    $(document).on('keyup', '.getdetails', function()
    {
      //  alert('okkkk')
    var length =  $("#pin_code").val().length;
      if (length == 6) {
          $('select[name="name"]').empty();
           var pin_code        = $("#pin_code").val();
           // alert(pin_code)
           $.ajax({
               url         : delivery,
               type        : 'POST',
               data        : {'_token': token,pin_code:pin_code},
               dataType    : 'json',
               beforeSend: function() {
                 $('#loading').show();
               },
               success     : function(data) 
               {

                  $('select[name="name"]').empty();
                  $('#state').val(data.state);
                  $('#city').val(data.city);
                 $(data.AllState).each(function(index, el) {
                     $('select[name="name"]').append('<option>'+ el +'</option>');
                  });


                },
               complete: function(){
                $('#loading').hide();
               },
               error       : function(response)
               {
                $('#error').html('<div class="alert alert-danger" role="alert">The requested resource is not found</div>');
               }
           });
         }  

    });  
   });  

</script>

 <script type="text/javascript">
   /*===========================*/
 $(".deleteRecord").click(function(){
    var id = $(this).data("id");
    var token = '{{ csrf_token() }}';
    //alert(id)
    $.ajax(
    {
        url: "deletePatientAddress/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
            toastr['success']("Delete Successfully", "Delete");
            window.location.reload();

        }
    });
   
});

</script>

<script type="text/javascript">

var customer_notes_route ="{{route('customer-notes')}}";
var token       = '{{ csrf_token() }}';

$(document).on('keyup', '.order_delivery_info', function() {
            var customr_note   = $("#customer_note").val();
            //alert(customr_note);
            $.ajax({
            url:  customer_notes_route,
            type: "POST",
            data: {'_token': token,customr_note:customr_note},
            dataType: 'json',
            success: function(response) {
                  window.location.reload();
            }
            });
 });

</script>

@stop
