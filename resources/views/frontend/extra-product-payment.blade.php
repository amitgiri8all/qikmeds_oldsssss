@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    CheckOut ::Qikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
{{-- <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 --}} <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/js-offcanvas.css')}}" rel="stylesheet" type="text/css"/>
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
	<div class="container">
		<div class="breadcrumb_blk">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page"> Extra product payment</li>
			  </ol>
			</nav>
		</div>
		<div class="cart_blk">
			<h1>Payment Details</h1>
			<div class="row">
				<div class="col-sm-9">
					<div class="payment_blk">
						<h3>Prefered Payment</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/Free_Charge_lo.png')}}"></i>
									<h4>Freecharge</h4>
									<p>Get up to Rs. 50 Freecharge cashback (Flat 15%) on your first transaction using Freecharge wallet on Netmeds. Offer valid ONCE per user till 31st August 2021. TCA.</p>
									<a class="js-offcanvas-trigger" data-offcanvas-trigger="otp-1" href="#otp-1">LINK</a>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/Mobikwik_lo.png')}}"></i>
									<h4>MobiKwik</h4>
									<p>Pay via MobiKwik for your order and get assured MobiKwik cashback up to Rs. 600*. Min. order: Rs. 750. Valid once per user. TCA.</p>
									<a href="#">LINK</a>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/Paytm_lo.png')}}"></i>
									<h4>Paytm</h4>
									<p>Pay using Paytm UPI/Wallet during the offer period and get guaranteed 2500 Paytm cashback points. MOV: Rs. 799. Valid for all users. TCA.</p>
									<a href="#">LINK</a>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/amazonpay_new.png')}}"></i>
									<h4>Amazon Pay</h4>
									<p>Pay via Amazon Pay and get cashback up to Rs. 600 on orders above Rs. 100. Valid once per user.</p>
									<div class="radio_btn">
										<input type="radio" name="radio" id="r-1">
										<label for="r-1">&nbsp;</label>
									</div>
									<button type="button" class="cmn_btn">Pay RS. 546.00</button>
								</li>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Other Payment</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/Phone_Pay_lo.png')}}"></i>
									<h4>PhonePe</h4>
									<p>Pay via your PhonePe wallet and get assured cashback between Rs. 20 to Rs. 500. Valid once for new users only during the offer period. Min. order: Rs. 500. TCA.
</p>
									<div class="radio_btn">
										<input type="radio" name="radio" id="r-2">
										<label for="r-2">&nbsp;</label>
									</div>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/googlepaylogo.png')}}"></i>
									<h4>Google Pay</h4>
									<div class="radio_btn">
										<input type="radio" name="radio" id="r-3">
										<label for="r-3">&nbsp;</label>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>UPI Payment</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/upiLogo6.gif')}}"></i>
									<h4>UPI</h4>
									<div class="radio_btn">
										<input type="radio" name="radio" id="r-4">
										<label for="r-4">&nbsp;</label>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Credit & Debit Cards</h3>
						<div class="pay_wallet_blk">
							<ul class="card_no">
								<li>
									<div class="card-box">
										<h4>ICICI Bank Credit Card </h4>
										<p><img src="{{asset('public/frontend/images/visa-credit-card.png')}}" alt="img">  5587 55** **** 1005</p>
									 </div>
									<div class="radio_btn">
										<input type="radio" name="radio" id="r-5">
										<label for="r-5">&nbsp;</label>
									</div>
								</li>
								<li>
									<div class="card-box">
										<h4>HDFC Bank Credit Card </h4>
										<p><img src="{{asset('public/frontend/images/visa-credit-card.png')}}" alt="img">  5587 55** **** 1005</p>
									 </div>
									<div class="radio_btn">
										<input type="radio" name="radio" id="r-6">
										<label for="r-6">&nbsp;</label>
									</div>
								</li>
								<a class="js-offcanvas-trigger" data-offcanvas-trigger="otp-2" href="#otp-2">ADD NEW CARD</a>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Net Banking</h3>
						<div class="net_banking">
							<ul>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/axis.png')}}" alt="img"></i>
										<small>Axis Bank</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/hdfc.png')}}" alt="img"></i>
										<small>HDFC Bank</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/icici.png')}}" alt="img"></i>
										<small>ICICI Netbanking</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/kotak.png')}}" alt="img"></i>
										<small>Kotak Bank</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/sbi.png')}}" alt="img"></i>
										<small>State Bank of India</small>
									</a>
								</li>
							</ul>
							<div class="more_bank">
								<select class="form-control col-sm-4">
									<option value="SELECT_BANK">---Select Bank---</option>
									<option value="AIRTELMONEY">Airtel Payments Bank</option>
									<option value="AUSMALLFINBANK">AU Small Finance Bank</option>
									<option value="BANDHAN">Bandhan Bank</option>
									<option value="BASSIENCATHOLICCOOPB">Bassien Catholic Co-Operative Bank</option>
									<option value="BNPPARIBAS">BNP Paribas</option>
									<option value="BOBAHRAIN">Bank of Bahrain and Kuwait</option>
									<option value="BOBARODA">Bank of Baroda</option>
									<option value="BOBARODAC">Bank of Baroda Corporate</option>
									<option value="BOBARODAR">Bank of Baroda Retail</option>
									<option value="BOI">Bank of India</option>
									<option value="BOM">Bank of Maharashtra</option>
									<option value="CANARA">Canara Bank</option>
									<option value="CATHOLICSYRIAN">Catholic Syrian Bank</option>
									<option value="CBI">Central Bank</option>
									<option value="CITYUNION">City Union Bank</option>
									<option value="CORPORATION">Corporation Bank</option>
									<option value="COSMOS">Cosmos Co-op Bank</option>
									<option value="DBS">digibank by DBS</option>
									<option value="DCB">DCB BANK LTD</option>
									<option value="DENA">Dena Bank</option>
									<option value="DEUTSCHE">Deutsche Bank</option>
									<option value="DHANBANK">Dhanalakshmi Bank</option>
									<option value="FEDERALBANK">Federal Bank</option>
									<option value="HSBC">HSBC</option>
									<option value="IDBI">IDBI Bank</option>
									<option value="IDFC">IDFC FIRST Bank</option>
									<option value="INDIANBANK">Indian Bank</option>
									<option value="INDUSIND">IndusInd Bank</option>
									<option value="IOB">Indian Overseas Bank</option>
									<option value="JANATABANKPUNE">JANATA SAHAKARI BANK LTD PUNE</option>
									<option value="JKBANK">J&amp;K Bank</option>
									<option value="KARNATAKA">Karnataka Bank</option>
									<option value="KARURVYSYA">Karur Vysya Bank</option>
									<option value="LAKSHMIVILAS">Lakshmi Vilas Bank - Retail</option>
									<option value="LAKSHMIVILASC">Lakshmi Vilas Bank - Corporate</option>
									<option value="PNB">Punjab National Bank</option>
									<option value="PNBC">Punjab National Bank Corporate</option>
									<option value="PNSB">Punjab &amp; Sind Bank</option>
									<option value="PUNJABMAHA">Punjab &amp; Maharashtra Co-op Bank</option>
									<option value="RATNAKAR">RBL Bank Limited</option>
									<option value="RBS">RBS</option>
									<option value="SARASWAT">Saraswat Co-op Bank</option>
									<option value="SHAMRAOVITHAL">Shamrao Vithal Co-op Bank</option>
									<option value="SHIVAMERCOOP">Shivalik Mercantile Co-op Bank</option>
									<option value="SOUTHINDIAN">The South Indian Bank</option>
									<option value="STANC">Standard Chartered Bank</option>
									<option value="TMBANK">Tamilnad Mercantile Bank Limited</option>
									<option value="TNMERCANTILE">Tamil Nadu Merchantile Bank</option>
									<option value="TNSC">TNSC Bank</option>
									<option value="UCO">UCO Bank</option>
									<option value="UNIONBANK">Union Bank of India</option>
									<option value="YESBANK">Yes Bank</option>
									<option value="ZOROACOPBANK">The Zoroastrian Co-Operative Bank</option>
								</select>
							</div>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Cash on Delivery</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/cod.png')}}"></i>
									<h4>Cash</h4>
									<p>Hear us out! Pay online and earn 100% QMS SuperCash (up to Rs. 3000) on all prepaid orders</p>
									<div class="radio_btn">
										<input type="radio" class="payment_method" value="cash on delivery" name="radio" id="r-7">
										<label for="r-7">&nbsp;</label>
									</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
				<div class="col-sm-3">
					<div class="order_smry_blk">
                        <div class="order_price_blk">
                            <h2>Payment Details</h2>
                            <div class="payment_details">
                                <ul>
                                	<input type="hidden" value="{{$order_id}}" name="order_id" id="order_id">
                                   <li>
                                        <span>Item Total</span>
                                        <span id="tot">₹ {{$total_amt}}</span>
                                    </li>
                                    

                                </ul>
                                

		                        <button type="submit" class="cmn_btn extra_place_order">Pay Amount</button>
		
                             </div>
                        </div>
						<div class="safety_txt">Products will be safely packed & Sanitized. Pay online for contactless delivery.</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

@stop


{{-- page level scripts --}}
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/js-offcanvas.pkgd.js')}}"></script>

<script type="text/javascript">
var place_order ="{{URL::to('extra-place-order')}}";
var token       = '{{ csrf_token() }}';

 $(document).on('click', '.extra_place_order', function() {
		 $("#overlay").fadeIn(500);　
 		var payment_method = $(".payment_method").val();
 		var order_id = $("#order_id").val();
 		var product_id = $("#product_id").val();
 	    alert(order_id)
 	//	alert(payment_method)
     event.preventDefault();
          $.ajax({
            url: place_order,
            type: "POST",
            data: {'_token': token,payment_method:payment_method,order_id:order_id,product_id:product_id},
            dataType: 'json',
            success: function(response) {
                     toastr[response.status]("Your order successfully placed", "Order");
					setTimeout(function(){
					 $("#overlay").fadeOut(5000);
                      window.location = response.url;
					},5000);
            }
            });
});
</script>



@stop


