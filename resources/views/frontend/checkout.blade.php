@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    CheckOut ::Qikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
{{-- <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 --}} <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/js-offcanvas.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.error_msg{
		color: red;
	}
		.succes_msg{
		color: green;
	}
	.input-group-prepend {
    margin-top: -15px;
}
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
	<div class="container">
		<div class="breadcrumb_blk">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{route('cart')}}">Cart</a></li>
				<li class="breadcrumb-item"><a href="{{route('order-review')}}">Order Review</a></li>
				<li class="breadcrumb-item active" aria-current="page">Payment Details</li>
			  </ol>
			</nav>
		</div>
		<div class="cart_blk">
			<h1>Payment Details</h1>
			<div class="row">
				<div class="col-sm-9">
					<div class="payment_blk">
						<h3>Prefered Payment</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/Free_Charge_lo.png')}}"></i>
									<h4>Freecharge</h4>
									<p>Get up to Rs. 50 Freecharge cashback (Flat 15%) on your first transaction using Freecharge wallet on Netmeds. Offer valid ONCE per user till 31st August 2021. TCA.</p>
									<a class="js-offcanvas-trigger" data-offcanvas-trigger="otp-1" href="#otp-1">LINK</a>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/Mobikwik_lo.png')}}"></i>
									<h4>MobiKwik</h4>
									<p>Pay via MobiKwik for your order and get assured MobiKwik cashback up to Rs. 600*. Min. order: Rs. 750. Valid once per user. TCA.</p>
									<a href="#">LINK</a>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/Paytm_lo.png')}}"></i>
									<h4>Paytm</h4>
									<p>Pay using Paytm UPI/Wallet during the offer period and get guaranteed 2500 Paytm cashback points. MOV: Rs. 799. Valid for all users. TCA.</p>
									<a href="#">LINK</a>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/amazonpay_new.png')}}"></i>
									<h4>Amazon Pay</h4>
									<p>Pay via Amazon Pay and get cashback up to Rs. 600 on orders above Rs. 100. Valid once per user.</p>
									<div class="radio_btn">
										<input type="radio" value="online" class="payment_method_val" name="radio" id="r-1">
										<label for="r-1">&nbsp;</label>
									</div>
									<button type="button" class="cmn_btn">Pay RS. 546.00</button>
								</li>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Other Payment</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/Phone_Pay_lo.png')}}"></i>
									<h4>PhonePe</h4>
									<p>Pay via your PhonePe wallet and get assured cashback between Rs. 20 to Rs. 500. Valid once for new users only during the offer period. Min. order: Rs. 500. TCA.
                                    </p>
									<div class="radio_btn">
										<input type="radio" value="online" class="payment_method_val" name="radio" id="r-2">
										<label for="r-2">&nbsp;</label>
									</div>
								</li>
								<li>
									<i><img src="{{asset('public/frontend/images/googlepaylogo.png')}}"></i>
									<h4>Google Pay</h4>
									<div class="radio_btn">
										<input type="radio" class="payment_method_val" value="online" name="radio" id="r-3">
										<label for="r-3">&nbsp;</label>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>UPI Payment</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/upiLogo6.gif')}}"></i>
									<h4>UPI</h4>
									<div class="radio_btn">
										<input type="radio" class="payment_method_val" value="online" name="radio" id="r-4">
										<label for="r-4">&nbsp;</label>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Credit & Debit Cards</h3>
						<div class="pay_wallet_blk">
							<ul class="card_no">
								<li>
									<div class="card-box">
										<h4>ICICI Bank Credit Card </h4>
										<p><img src="{{asset('public/frontend/images/visa-credit-card.png')}}" alt="img">  5587 55** **** 1005</p>
									 </div>
									<div class="radio_btn">
										<input type="radio" class="payment_method_val" value="online" name="radio" id="r-5">
										<label for="r-5">&nbsp;</label>
									</div>
								</li>
								<li>
									<div class="card-box">
										<h4>HDFC Bank Credit Card </h4>
										<p><img src="{{asset('public/frontend/images/visa-credit-card.png')}}" alt="img">  5587 55** **** 1005</p>
									 </div>
									<div class="radio_btn">
										<input type="radio" class="payment_method_val" value="online" name="radio" id="r-6">
										<label for="r-6">&nbsp;</label>
									</div>
								</li>
								<a class="js-offcanvas-trigger" data-offcanvas-trigger="otp-2" href="#otp-2">ADD NEW CARD</a>
							</ul>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Net Banking</h3>
						<div class="net_banking">
							<ul>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/axis.png')}}" alt="img"></i>
										<small>Axis Bank</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/hdfc.png')}}" alt="img"></i>
										<small>HDFC Bank</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/icici.png')}}" alt="img"></i>
										<small>ICICI Netbanking</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/kotak.png')}}" alt="img"></i>
										<small>Kotak Bank</small>
									</a>
								</li>
								<li>
									<a href="#">
										<i><img src="{{asset('public/frontend/images/sbi.png')}}" alt="img"></i>
										<small>State Bank of India</small>
									</a>
								</li>
							</ul>
							<div class="more_bank">
								<select class="form-control col-sm-4">
									<option value="SELECT_BANK">---Select Bank---</option>
									<option value="AIRTELMONEY">Airtel Payments Bank</option>
									<option value="AUSMALLFINBANK">AU Small Finance Bank</option>
									<option value="BANDHAN">Bandhan Bank</option>
									<option value="BASSIENCATHOLICCOOPB">Bassien Catholic Co-Operative Bank</option>
									<option value="BNPPARIBAS">BNP Paribas</option>
									<option value="BOBAHRAIN">Bank of Bahrain and Kuwait</option>
									<option value="BOBARODA">Bank of Baroda</option>
									<option value="BOBARODAC">Bank of Baroda Corporate</option>
									<option value="BOBARODAR">Bank of Baroda Retail</option>
									<option value="BOI">Bank of India</option>
									<option value="BOM">Bank of Maharashtra</option>
									<option value="CANARA">Canara Bank</option>
									<option value="CATHOLICSYRIAN">Catholic Syrian Bank</option>
									<option value="CBI">Central Bank</option>
									<option value="CITYUNION">City Union Bank</option>
									<option value="CORPORATION">Corporation Bank</option>
									<option value="COSMOS">Cosmos Co-op Bank</option>
									<option value="DBS">digibank by DBS</option>
									<option value="DCB">DCB BANK LTD</option>
									<option value="DENA">Dena Bank</option>
									<option value="DEUTSCHE">Deutsche Bank</option>
									<option value="DHANBANK">Dhanalakshmi Bank</option>
									<option value="FEDERALBANK">Federal Bank</option>
									<option value="HSBC">HSBC</option>
									<option value="IDBI">IDBI Bank</option>
									<option value="IDFC">IDFC FIRST Bank</option>
									<option value="INDIANBANK">Indian Bank</option>
									<option value="INDUSIND">IndusInd Bank</option>
									<option value="IOB">Indian Overseas Bank</option>
									<option value="JANATABANKPUNE">JANATA SAHAKARI BANK LTD PUNE</option>
									<option value="JKBANK">J&amp;K Bank</option>
									<option value="KARNATAKA">Karnataka Bank</option>
									<option value="KARURVYSYA">Karur Vysya Bank</option>
									<option value="LAKSHMIVILAS">Lakshmi Vilas Bank - Retail</option>
									<option value="LAKSHMIVILASC">Lakshmi Vilas Bank - Corporate</option>
									<option value="PNB">Punjab National Bank</option>
									<option value="PNBC">Punjab National Bank Corporate</option>
									<option value="PNSB">Punjab &amp; Sind Bank</option>
									<option value="PUNJABMAHA">Punjab &amp; Maharashtra Co-op Bank</option>
									<option value="RATNAKAR">RBL Bank Limited</option>
									<option value="RBS">RBS</option>
									<option value="SARASWAT">Saraswat Co-op Bank</option>
									<option value="SHAMRAOVITHAL">Shamrao Vithal Co-op Bank</option>
									<option value="SHIVAMERCOOP">Shivalik Mercantile Co-op Bank</option>
									<option value="SOUTHINDIAN">The South Indian Bank</option>
									<option value="STANC">Standard Chartered Bank</option>
									<option value="TMBANK">Tamilnad Mercantile Bank Limited</option>
									<option value="TNMERCANTILE">Tamil Nadu Merchantile Bank</option>
									<option value="TNSC">TNSC Bank</option>
									<option value="UCO">UCO Bank</option>
									<option value="UNIONBANK">Union Bank of India</option>
									<option value="YESBANK">Yes Bank</option>
									<option value="ZOROACOPBANK">The Zoroastrian Co-Operative Bank</option>
								</select>
							</div>
						</div>
					</div>

					<div class="payment_blk">
						<h3>Cash on Delivery</h3>
						<div class="pay_wallet_blk">
							<ul>
								<li>
									<i><img src="{{asset('public/frontend/images/cod.png')}}"></i>
									<h4>Cash</h4>
									<p>Hear us out! Pay online and earn 100% QMS SuperCash (up to Rs. 3000) on all prepaid orders</p>
									<div class="radio_btn">
										<input type="radio" value="cash on delivery" name="radio" id="r-7">
										<label for="r-7">&nbsp;</label>
									</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
				<div class="col-sm-3">
					<div class="order_smry_blk">
                        <div class="order_price_blk">
                        <?php   Session::get('customer_note') ; ?>
                            <h2>Payment Details (UXn8WyGJ) (OFF123)   {{Session::get('referrer_code')}} {{Session::get('offer_id_aapyed')}}</h2>
                            <div class="payment_details">
   @if($check_first_order=='0')                         	
 		@if(empty(Session::get('referrer_code')))
 			<div id="succes_msg_div">			
				  <div class="col-lg-12 mb-3">
				    <div class="input-group">
				      <input type="text" class="form-control rounded-0" id="ref_code" placeholder="Referral code" aria-describedby="inputGroupPrepend2">
				      <div class="input-group-prepend">
				        <button type="button" class="cmn_btn btn-sm rounded-0" id="referralcode">Apply</button>
				      </div>
				    </div>
      				  <span id="errorClass" class=""></span>
				  </div>
			</div>	  
		@else
		<div id="succes_msg_div">
			 <div class="col-lg-12 mb-3">
				    <div class="input-group">
				      <input type="text" class="form-control rounded-0" value="{{Session::get('referrer_code')}}" id="ref_code" disabled placeholder="Referral code" aria-describedby="inputGroupPrepend2">
				      <div class="input-group-prepend">
				        <button type="button" class="btn btn-danger btn-sm rounded-0" id="referralcodeclear"><i class="fa fa-trash"></i></button>
				      </div>
				    </div>
      				  <span class="succes_msg">Referral code applyed</span>
				  </div>
			</div>	  
		@endif				  
@endif

@if(empty(Session::get('offer_code')))	
<div id="succes_msg_div_offer">			
				  <div class="col-lg-12 mb-3">
				    <div class="input-group">
				      <input type="text" class="form-control rounded-0" id="offer_code" placeholder="Offer code" aria-describedby="inputGroupPrepend2">
				      <div class="input-group-prepend">
				        <button type="button" class="cmn_btn btn-sm rounded-0" id="offercode">Apply</button>
				      </div>
				    </div>
      				  <span id="errorClassB" class=""></span>
				  </div>
			</div>

@else
	<div id="succes_msg_div_offer">
			 <div class="col-lg-12 mb-3">
				    <div class="input-group">
				      <input type="text" class="form-control rounded-0" value="{{Session::get('offer_code')}}" id="ref_code" disabled placeholder="Referral code" aria-describedby="inputGroupPrepend2">
				      <div class="input-group-prepend">
				        <button type="button" class="btn btn-danger btn-sm rounded-0" id="offercodeclear"><i class="fa fa-trash"></i></button>
				      </div>
				    </div>
 				  </div>
    				  <span class="succes_msg">Offer code applyed</span>
			</div>	


	@endif	








                                <ul>
                                    @php $total = 0 @endphp
                                    @foreach((array) session('cart') as $id => $details)
                                        <?php $price= str_replace('₹', '',$details['price']); ?>
                                        @php $total += (int) $price * (int) $details['quantity']; @endphp
                                    @endforeach


							@if(session('cart')) 
                                    <li>
                                        <span>Item Total</span>
                                        <span id="tot">₹ {{number_format($total,2)}}</span>
                                    </li>

                                    <li>
                                        <span>Qikmeds Discount</span>
                                        <span>₹ {{$site_setting->qikmeds_discount;}}</span>
                                    </li>
                                

                                    @if($site_setting->min_price > $total)
                                     @php $delivery_charge = $site_setting->delivery_charge; @endphp
                                    <li>
                                        <span>Delivery Charge</span>
                                        <span id="charge"> ₹  {{$delivery_charge;}}</span>
                                    </li>
                                    @else
                                    <li>
                                        <span>Delivery Charge</span>
                                        @php $delivery_charge = 0; @endphp
                                        <span id="free">Free</span>
                                    </li>
                                    @endif
   							



                                    <?php   
                                    $promo_val = Session::get('promo_val');
                                    $total_amt = Session::get('total_amt');
                                    $total_saving = Session::get('total_saving');
                                    $offer_code_discount = Session::get('offer_code_discount');
                                    $offer_cashback = Session::get('offer_cashback');
                                    ?>
                                     
                                    @if(!empty($promo_val))
                                      <li>
                                        <span>Promo Discount</span>
                                        <span id="promo_dis">₹ {{$promo_val;}}</span>
                                    </li>
                                    @endif 

                                    @if(!empty($offer_code_discount))
                                      <li>
                                        <span>Offer Discount</span>
                                        <span id="Discount">₹ {{$offer_code_discount}}</span>
                                    </li>
                                    @endif

                                   @if(!empty($offer_cashback))
                                      <li>
                                        <span>Offer Cashback</span>
                                        <span id="Cashbackoff">₹ {{$offer_cashback}}</span>
                                    </li>
                                    @endif

							@endif 


                                @if(!empty($subscriptionplan))
                                     <li>
                                        <span>Qikmeds Subscription Plan.</span>
                                        <span id="subscription_plan">₹ {{$subscriptionplan->sale_price}}</span>
                                    </li>
                                @endif

                        @if(session('cart'))         	<?php
	                        //echo $total.'==='.$delivery_charge.'==='.$offer_code_discount.'==='.$site_setting->qikmeds_discount.'==='.$promo_val; die;
                        ?>

                                   <?php $total_amount = $total+$delivery_charge-$offer_code_discount-$site_setting->qikmeds_discount-$promo_val+isset($subscriptionplan->sale_price); ?>
                                    <li class="total_amnt">
                                        <span>Total Amount</span>
                                        <input type="hidden" id="sum_total_amount_val" value="{{$total_amount}}" name="">
                                        <span id="sum_total_amount">₹ {{number_format($total_amount,2);}}</span>
                                    </li>
                        @endif


                                </ul>
                               @if(session('cart')) 
                                 @php
                                    $total_savings = $site_setting->qikmeds_discount+$promo_val+Session::get('offer_code_discount');
                                 @endphp

                                <div class="total_sav" id="total_sav">TOTAL SAVINGS <span>₹ {{number_format($total_savings,2)}}</span></div>
                              @endif

									@if(session('cart') && !empty($subscriptionplan))
									  <button type="submit" class="cmn_btn place_order">Pay Amount</button>
									@elseif(!empty($subscriptionplan) && empty(session('cart')))
									  <button type="submit" class="cmn_btn place_order">Pay Amount</button>
									@else
                                   	     <button type="submit" class="cmn_btn place_order">Pay Amount</button>
                                    @endif

                             </div>
                        </div>
						<div class="safety_txt">Products will be safely packed & Sanitized. Pay online for contactless delivery.</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/js-offcanvas.pkgd.js')}}"></script>

<script type="text/javascript">
var place_order ="{{URL::to('place-order')}}";
var check_referral_code ="{{URL::to('check_referral_code')}}";
var check_offer_code ="{{URL::to('check_offer_code')}}";
var remove_referrer_code ="{{URL::to('remove_referrer_code')}}";
var remove_offer_code ="{{URL::to('remove_offer_code')}}";
var token       = '{{ csrf_token() }}';

/*=========================================================================================================*/

 $(document).on('click', '.place_order', function() {
	 
	   if($('input[class="payment_method_val"]:checked').val() !== undefined){
 		var payment_method = $('input[class="payment_method_val"]:checked').val();
 		//alert(payment_method)
 	   }else{
	 	   var payment_method = '';
 	   }	
 		
	   $("#overlay").fadeIn(100);　	
			event.preventDefault();
				$.ajax({
				url: place_order,
				type: "POST",
				data: {'_token': token,payment_method:payment_method},
				dataType: 'json',
				success: function(response) {
					if(response.status=='success'){
				         toastr[response.status](response.message, "Order");
						setTimeout(function(){
						 $("#overlay").fadeOut(5000);
				          window.location = response.url;
						},5000);
					}else{
					 $("#overlay").fadeOut(1000);	
			         toastr[response.status](response.message, "Order");
					}	

				}
			});

 			//alert('ok')
 		//}
});

/*=========================================================================================================*/
$(document).on('click', '#offercodeclear', function() {
  	 event.preventDefault();
          $.ajax({
            url: remove_offer_code,
            type: "POST",
            data: {'_token': token},
            dataType: 'json',
		    success: function(response) {

            		if(response.status=='error'){
            		$("#errorClassB").addClass("error_msg").text(response.message);
                    }else{
                     $("#errorClassB").addClass("succes_msg").text(response.message);
                    }
                    $("#referralcode").html('Apply');
                     $("#referralcode").find("button[type=button]").removeAttr("disabled");

					toastr[response.status](response.message, "Offer Code");
					setTimeout(function(){
				      window.location.reload();
					},2000);
            }
            });
});

/*=========================================================================================================*/

 $(document).on('click', '#offercode', function() {
		var offer_code = $("#offer_code").val();
		var sum_total_amount_val = $("#sum_total_amount_val").val();
		// alert(sum_total_amount_val)
 	 event.preventDefault();
          $.ajax({
            url: check_offer_code,
            type: "POST",
            data: {'_token': token,offer_code:offer_code,sum_total_amount_val:sum_total_amount_val},
            dataType: 'json',
			beforeSend : function() {
				$("#offercode").html('Validating...');
			    $("#offercode").find("button[type=button]").attr("disabled", "disabled");

			},
            success: function(response) {

            		if(response.status=='error'){
            		$("#errorClassB").addClass("error_msg").text(response.message);
                    }else{
                    $("#succes_msg_div_offer").load(document.URL + " #succes_msg_div_offer");
                     $("#errorClassB").addClass("succes_msg").text(response.message);
                    }
                    $("#offercode").html('Apply');
                     $("#offercode").find("button[type=button]").removeAttr("disabled");

					toastr[response.status](response.message, "Offer Code");
					setTimeout(function(){
					 $("#overlay").fadeOut(5000);
                      window.location.reload();
					},5000);
            }
            });
});

/*=========================================================================================================*/

 $(document).on('click', '#referralcode', function() {
		var referrer_code = $("#ref_code").val();
		//alert(referrer_code)
 	 event.preventDefault();
          $.ajax({
            url: check_referral_code,
            type: "POST",
            data: {'_token': token,referrer_code:referrer_code},
            dataType: 'json',
			beforeSend : function() {
				$("#referralcode").html('Validating...');
			    $("#referralcode").find("button[type=button]").attr("disabled", "disabled");

			},
            success: function(response) {

            		if(response.status=='error'){
            		$("#errorClass").addClass("error_msg").text(response.message);
                    }else{
                    $("#succes_msg_div").load(document.URL + " #succes_msg_div");
                     $("#errorClass").addClass("succes_msg").text(response.message);
                    }
                    $("#referralcode").html('Apply');
                     $("#referralcode").find("button[type=button]").removeAttr("disabled");

					toastr[response.status](response.message, "Referrer Code");
					setTimeout(function(){
					 $("#overlay").fadeOut(5000);
                      //window.location = response.url;
					},5000);
            }
            });
});
/*=========================================================================================================*/

 $(document).on('click', '#referralcodeclear', function() {
  	 event.preventDefault();
          $.ajax({
            url: remove_referrer_code,
            type: "POST",
            data: {'_token': token},
            dataType: 'json',
			beforeSend : function() {
				$("#referralcode").html('Validating...');
			    $("#referralcode").find("button[type=button]").attr("disabled", "disabled");

			},
            success: function(response) {

            		if(response.status=='error'){
            		$("#errorClass").addClass("error_msg").text(response.message);
                    }else{
                     $("#errorClass").addClass("succes_msg").text(response.message);
                    }
                    $("#referralcode").html('Apply');
                     $("#referralcode").find("button[type=button]").removeAttr("disabled");

					toastr[response.status](response.message, "Referrer Code");
					setTimeout(function(){
				      window.location.reload();
					},2000);
            }
            });
});
</script>



@stop


