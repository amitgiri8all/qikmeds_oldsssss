<html>
<?php
  if(!Request::is('customer/account/login')){
    Session::forget('register_form_show');
    Session::forget('otp_form_show');
    Session::forget('mobile_number');
    Session::forget('password');
    Session::forget('otp');
  // Session::forget('cart');
  }
?>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
 <title>@yield('Qikmeds')</title>
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/images/favicon.ico')}}"/>
<link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet">
<link href="{{asset('public/frontend/css/line-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('public/frontend/css/animate.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/webslidemenu.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/slick.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
<link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/js-offcanvas.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
<style type="text/css">
    .c-offcanvas--right {
  height: 100%;
  width: 29em !important;
  right: 0;
  transform: translate3d(400px, 0, 0);
}
a.js-offcanvas-trigger.c-button.cmn_btn {
    text-align: -webkit-center;
    width: 21em;
    margin-bottom: 1em;
}
</style>

<style type="text/css">
.form-group label sup{color:#f00;}

i.ti.ti-heart {
    line-height: inherit;
}
#overlay{
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% {
    transform: rotate(360deg);
  }
}
.is-hide{
  display:none;
}
</style>
  @yield('header_styles')
  </head>

<body>

<div id="overlay">
  <div class="cv-spinner">
    <span class="spinner"></span>
  </div>
</div>
@php $settings = App\Models\Setting::first(); @endphp
@php $sitesettings = App\Models\Sitesettings::first();
 @endphp
 <header>
  <div class="header_top">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-7">
          <div class="left_txt">
            <ul>
 
              <li><a href="#">Free Shipping for all Order of Rs. {{$sitesettings->min_price;}}</a></li>
 
 
              <li><a href="https://mail.google.com/mail/?view=cm&fs=1&to=support@qikmeds.com"><i class="ti ti-email"></i>{{$settings->email;}}</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-5">
          <div class="right_txt">
            <ul>
              <li><a href="tel:{{$settings->phone}}"><span data-toggle="tooltip" data-placement="top" title="Call:{{$settings->phone}}">Call Qikmeds </span> </a>
               </li>
               <li><a href="{{route('offer-just-for-you-list')}}">Offers</a></li>
              <li><a href="{{route('faq')}}">Need Help?</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="header_btm">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          <div class="header_logo"><a href="{{route('home')}}"><img src="{{ url('storage/app/public/upload/image/'.$settings->app_logo) }}" alt="{{$settings->app_name}}"></a></div>
        </div>
        <div class="col-sm-10">
          <div class="header_right">
            <div class="header_right_top">


              <div class="search_blk">
                
                <div class="pincode_blk">
                  <span> Delivering to <br>
                   <!--  <input type="text"  value="{{App\Helpers\Helper::GetCurrentPincode()}}"> -->
                   
                    @php  $delivery_address = App\Helpers\Helper::get_delivery_address();@endphp
                        @if(!empty($delivery_address))  
                   <input type="text"  value="{{$delivery_address->pin_code}} ">
                   @else
                   <input type="text"  value="302012">
                   @endif 
                    <br>
                  <!--  <span style="width: 170px; margin: -1px; margin-left: -25px;font-weight: bold;">{{App\Helpers\Helper::GetCurrentStateCity()}}</span> -->
                  </span>
                  
                     

                        <a class="js-offcanvas-trigger" data-offcanvas-trigger="right" href="#right" role="button"  aria-controls="right" aria-expanded="false" title="Delivery"><i class="fa fa-edit" style="width: 10px;height: 10px;border: 0px;position: absolute; bottom: 10px;
                         right: 17px;"></i></a>
                         
                </div>


                <div class="search_bar">
                  <input type="text" autocomplete="off" id="search" placeholder="Search for Medicines and Health Products...">
                  <button type="submit"></button>
                </div>
              </div>



<?php if(Auth::guard('web')->user()){?>
<div class="order_smry_blk">
                  

<!-- Change Delivery Address ================================== -->
<aside class="js-offcanvas" data-offcanvas-options='{"modifiers":"right,overlay"}' id="right" role="complementary">
    <div class="address_change_blk">
        <h2>Change Delivery Address</h2>
        <div class="row">
              @php  $addresses = App\Helpers\Helper::get_address_user();@endphp
             @foreach($addresses as $key=>$value)
                             <div class="col-sm-12">
                                <div class="address_blk"  style="padding-left: 43px;">
                                  <div id="select_address">

                                    <div class="radio_btn">
                                        <input type="radio" class="update_address" data-id="{{$value->id}}" name="radio" id="r-{{$value->id}}" {{ ($value->status=="1")? "checked" : "" }}>
                                        <label for="r-{{$value->id}}">&nbsp;</label>
                                    </div>
                                </div>
                                    <h4>{{$value->name}}</h4>
                                    <h5>{{$value->mobile_number}}</h5>
                                    <span>{{$value->address_type}}</span>

                                    <p>{{$value->address_delivery}},{{$value->locality}}<br> {{$value->city}}, {{$value->state}} - {{$value->pin_code}}</p>

                                </div>
                            </div>

                         @endforeach 

                         

            <div class="col-sm-12">
                <a href="#right1" class="cmn_btn js-offcanvas-trigger" data-offcanvas-trigger="right1" ><i class="ti-plus"></i> ADD NEW ADDRESS</a>
            </div>
          </div>
    </div>
    <button class="js-offcanvas-close" data-button-options='{"modifiers":"m1,m2"}'><i class="ti ti-close"></i></button>
</aside>
<!-- Change Delivery Address End ================================== -->
<!-- Add Deilervery info -->
<aside class="js-offcanvas" data-offcanvas-options='{"modifiers":"right,overlay"}' id="right1" role="complementary">
    <div class="address_change_blk">
        <h2>Add New Address</h2>
         <form class="ajax_form" id="delivery_address" action="{{route('user.save-delivery-address')}}" method="post">
        @csrf
                        <div class="user_address_blk cmn_area">
                          <div class="row">
                             <div class="form-group col-sm-12" style="display:none;">
                                <button class="cmn_btn" type="button"><i class="ti-target"></i> Use my Current Location</button>
                            </div>

                                    <div class="form-group col-sm-12">
                                        <label>Name <sup>*</sup></label>
                                        <input type="text" class="form-control" value="{{old('name')}}" name="name" placeholder="Name">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label>Mobile number <sup>*</sup></label>
                                        <input type="text" maxlength="10" class="form-control num" value="{{old('mobile_number')}}" name="mobile_number" placeholder="10 - digit mobile number">
                                    </div>

                                    <div class="form-group col-sm-6">
                                         <label>Pin Code <sup>*</sup></label>
                    <input type="text" class="form-control num getdetails" maxlength="6" value="{{old('pin_code')}}"  id="pin_code" name="pin_code" placeholder="Pincode">

                                    </div>
                <div class="form-group col-sm-6">
                         <label>City <sup>*</sup></label>
                        <input type="text" class="form-control num" id="city" value="{{old('pin_code')}}" name="pin_code" placeholder="city" disabled="disabled">
                    </div> 
                     <div class="form-group col-sm-12">
                         <label>State <sup>*</sup></label>
                        <input type="text" class="form-control num" id="state" value="{{old('pin_code')}}" name="state" placeholder="state" disabled="disabled">
                    </div>
               
                                    <div class="form-group col-sm-12">
                                         <label>Locality <sup>*</sup></label>
                                        <input type="text" class="form-control" value="{{old('locality')}}" name="locality" placeholder="Locality">
                                    </div>

                                    <div class="form-group col-sm-12">
                                         <label>Area and Street <sup>*</sup></label>
                                        <textarea class="form-control" rows="5" value="{{old('address_delivery')}}" name="address_delivery" placeholder="Address (Area and Street)"></textarea>
                                    </div>

                                   <!--  <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" value="{{old('city')}}" name="city" placeholder="City/District/Town">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" value="{{old('state')}}" name="state" placeholder="State">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="text" name="landmark" value="{{old('landmark')}}" class="form-control" placeholder="Landmark(Optional)">
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" value="{{old('alternate_phone')}}" name="alternate_phone" placeholder="Alternate Phone (Optional)">
                                    </div>
 -->

  <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                     <label for="text">Address Location</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="address_name_a" placeholder="Address name" class="form-control" name="address_location">
                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  
                  <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                     <label for="text">Latitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="lat_a" readonly placeholder="Address name" class="form-control" name="latitute">
                        {!! $errors->first('lat', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>  

                  <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                     <label for="text">Longitute</label>
                     <div class="input-group col-xs-12">
                        <input type="text" id="long_a" readonly placeholder="Longitute name" class="form-control" name="longitute">
                        {!! $errors->first('lng', '<span class="help-block">:message</span>') !!}
                        </span>
                     </div>
                  </div>


                  <!-- Address Location -->
                  <div class="form-group col-sm-12" >
                      <div class="input-group col-xs-12">
                  <div id="a" style="width: 100%; height: 300px;"></div>
                        </span>
                     </div>
                  </div> 


                           <div class="col-sm-12">
                                <div class="user_txt">
                                    <label>Address Type</label>

                                    <div class="radio_btn">
                                        <input type="radio" value="home" name="address_type" id="r-4" value="home" checked="checked">
                                        <label for="r-4">Home</label>
                                    </div>

                                    <div class="radio_btn">
                                        <input type="radio" value="work" name="address_type" id="r-5">
                                        <label for="r-5">Work</label>
                                    </div>

                                    <div class="radio_btn">
                                        <input type="radio" value="others" name="address_type" id="r-6">
                                        <label for="r-6">Others</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-sm-12 mgn_0">
                                <button type="submit" class="cmn_btn">Save</button>

                            </div>



                           </div>
                        </div>
                      </form>
        </div>
    </div>
    <!-- <button class="js-offcanvas-close" data-button-options='{"modifiers":"m1,m2"}'><i class="ti ti-close"></i></button> -->
</aside>
<?php } ?>
<!-- Change Delivery Address ================================== -->





      <?php if(Auth::guard('web')->user()){?>
    
              <div class="upload_blk prescription-btn">Don’t have a prescription ? <a href="{{route('upload-prescription')}}">Get Started</a></div>
 <?php }else{ ?>
              <div class="upload_blk prescription-btn">
                <span data-toggle="tooltip" data-placement="top" title="Please Login First"> Don’t have a prescription ? </span> <a href="{{route('account.login')}}">Get Started</a></div>

 <?php } ?>


              <div class="user_info">
                @php
if(!empty(Auth::guard('web')->user()->image)){
$url= Auth::guard('web')->user()->image;
}else{
$url= asset('/public/assets/images/no-image.jpg');
}  
@endphp
                <ul>
                  <?php if(Auth::guard('web')->user()){?>
                  <li>
                    <div class="user_tgl"> <span class="userimg"><img src="{{$url}}"></span>
                      <div class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">{!!substr(Auth::guard('web')->user()->name,0,10)!!} </a>
                        <ul class="dropdown-menu">
                          <li><a href="{{route('user.profile')}}"><i class="ti-user"></i> My Profile</a></li>
                          <li><a href="{{route('user.prescription')}}"><i class="ti-support"></i>My Prescription</a></li>
                          <li><a href="{{route('logout.post')}}"><i class="ti-export"></i> Logout</a></li>
                        </ul>
                      </div>
                      </div>
                  </li>

                  <!-- <li><a href="#"><i class="ti ti-bell"></i></a></li> -->
                       @php if(isset(Auth::guard('web')->user()->id))
                        $users = App\Helpers\Helper::UnreadNotificationsUser(Auth::guard('web')->user()->id);
                        @endphp
                       <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ti ti-bell"></i><sup>

                                  @if(isset($users))
                                  {{count($users)}}
                                  @endif
                                </sup>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                                aria-labelledby="page-header-notifications-dropdown">
                                <div class="p-3"  style="width: 300px;" >
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h6 class="m-0"> Notifications </h6>
                                        </div>
                                        <div class="col-auto">
                                            <a href="{{route('user.notification')}}" class="small" id="view"> View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div data-simplebar style="max-height: 230px;" class="notification-box">
                                
                                @if(isset($users))
                                 @forelse ($users->slice(0, 5) as $notification)

                                
                                    <a class="text-reset notification-item">
                                        <div class="media">
                                            <div class="avatar-xs mr-3">
                                                <span class="avatar-title bg-primary rounded-circle font-size-16">
                                                    <i class="ri-shopping-cart-line"></i>
                                                </span>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="mt-0 mb-1">
                                                @php
                                                if(isset($notification->data)){
                                                $data_value=   json_decode($notification->data) ;
                                                
                                               }
                                               
                                                @endphp
                                                {{$data_value->message ?? ' ' }}


                                                   </h6>
                                                <div class="font-size-12 text-muted">
<!-- <p class="mb-1">If several languages coalesce the grammar</p>
 -->   <p class="mb-0"><i class="mdi mdi-clock-outline">   </i> 
  {{ $notification->created_at}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @empty
                                    <li class="dropdown-title">There is no unread notifications</li>
                                @endforelse
                                @endif
                               
                                </div>
                                <div class="p-2 border-top">
                                    <a class="btn btn-sm btn-link text-btn font-size-14 btn-block text-center" href="{{route('user.notification')}}">
                                        <i class="mdi mdi-arrow-right-circle mr-1"></i> View More..
                                    </a>
                                </div>
                            </div>
                        </div>

                <?php }else{ ?>
                  <li><a href="{{route('account.login')}}"><i class="ti ti-user"></i> <span class="signin-txt">SIGN IN / SIGN UP</span></a></li>
                  <?php } ?>
                                    <li>
                    <a href="{{route('cart')}}">
                    <i class="ti ti-shopping-cart"></i>
                    <span class="qty"> <div id="cartcount">{{ count((array) session('cart')) }}</div></span>
                     @php $total = 0 @endphp
                        @foreach((array) session('cart') as $id => $details)
                            <?php $price= str_replace('₹', '',$details['price']); ?>
                            @php $total += (int) $price * (int) $details['quantity']; @endphp
                        @endforeach
                    <span id="total_cart_value">₹ {{$total;}}</span>
                  </a>
                  </li>
                  <li class="menu-btn">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="true" aria-label="Toggle navigation">
                  <i class="las la-bars"></i>
                  </button>                  
                </ul>
              </div>
            </div>
            <div class="header_right_btm">
              <nav class="navbar navbar-expand-lg">
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                  <ul class="navbar-nav">
                  @if(!empty($categoryHeader))
                   @foreach($categoryHeader as $category)
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('/list/'.$category->slug)}}">{{$category->category_name}}</a>
                    </li>
                    @endforeach
                   @endif
                  </ul>
                </div>
                <div class="header_right_btm">
              <nav class="navbar navbar-expand-lg">
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                  <ul class="navbar-nav">
                 
                   @foreach(App\Helpers\ProductHelper::category(10) as $category)
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('/list/'.$category->slug)}}">{{$category->category_name}}</a>
                    </li>
                    @endforeach
                  
                  </ul>
                </div>
              </nav>
            </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>




  @yield('content')



<footer>
  <div class="footer_top" id="still_need_help">
    <div class="container">
      <div class="footer_section">
        <div class="row">
          <div class="col-sm-4">
            <div class="company_info">
              <div class="info_icon"><span><i class="las la-map-marker-alt"></i></span></div>
              <div class="info_txt">
                <h2>Address</h2>
                <p>{{$settings->address}}</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="company_info pl_5">
              <div class="info_icon"><span><i class="lab la-whatsapp"></i></span></div>
              <div class="info_txt">
                <h2>Whatsapp Us</h2>
                <h3> <a href="//api.whatsapp.com/send?phone=91{{$sitesettings->whats_up}}&text=WHATEVER_LINK_OR_TEXT_YOU_WANT_TO_SEND" title="Share on whatsapp">{{$sitesettings->whats_up}}</a>
                    </h3>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="downld_txt">
              <h4>Download the app now</h4>
              <a href="javascript:myAndroid()"><img src="{{asset('public/frontend/images/google-play.png')}}" alt="play store"></a>
              <a href="javascript:myIphone()"><img src="{{asset('public/frontend/images/app-store.png')}}" alt="app store"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer_mdl">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="footer_txt">
            <div class="footer_logo"><img src="{{ url('storage/app/public/upload/image/'.$settings->app_logo) }}" alt="{{$settings->app_name}}"></div>
    <p>{{$settings->short_descriptions}}</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-4">
              <div class="footer_txt">
                <h2>Company</h2>
                <ul>
                  @php  $our_policy = App\Helpers\Helper::company(); @endphp
                    @foreach($our_policy as $val)
                       <li><a href="{{ route('staticpages',$val->slug) }}"><i class="ti ti-angle-double-right"></i> {{$val->title}}</a></li>
                    @endforeach 
                </ul>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="footer_txt">
                <h2>Categories</h2>
                <ul>
                    @foreach(App\Models\Category::categoryFooter() as $category)
                      <li><a href="{{url('/list/'.$category->slug)}}"><i class="ti ti-angle-double-right"></i> {{$category->category_name}}</a></li>
                    @endforeach
                </ul>
              </div>
            </div>
            
            <div class="col-sm-4">
              <div class="footer_txt">
                <h2>Our Policies</h2>
                <ul>
                 @php  $our_policy = App\Helpers\Helper::our_policy(); @endphp
                    @foreach($our_policy as $val)
                       <li><a href="{{ route('staticpages',$val->slug) }}"><i class="ti ti-angle-double-right"></i> {{$val->title}}</a></li>
                    @endforeach 
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="footer_txt">
            <h2>Social</h2>
            <ul class="social">
              <li><a href="{{$sitesettings->facebook}}" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i> Facebook</a></li>
              <li><a href="{{$sitesettings->twitter}}" target="_blank" class="twitter"><i class="fab fa-twitter"></i> Twitter</a></li>
              <li><a href="{{$sitesettings->instagram}}" target="_blank" class="instagram"><i class="fab fa-instagram"></i> Instagram</a></li>
              <li><a href="{{$sitesettings->linkedin}}" target="_blank" class="linkedin"><i class="fab fa-linkedin"></i> LinkedIn</a></li>
              <li><a href="{{$sitesettings->youtube}}" target="_blank" class="youtube"><i class="fab fa-youtube"></i> Youtube</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="copyright_blk">
    <div class="container">
      <p>Copyright © {{date("Y");}} Qikmeds. All Rights Reserved.</p>
    </div>
  </div>
</footer>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" type="text/css" media="all"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
<script src="{{asset('public/frontend/js/formClass.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('public/frontend/js/slick.js')}}"></script>
 <script src="{{asset('public/frontend/js/custom.js')}}"></script>
 <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


  <script src="{{asset('public/frontend/js/number.js')}}"></script>
<!--   <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 -->  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/js-offcanvas.pkgd.js')}}"></script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC63C7UGlNd9s0QaZbzPNrVD5NiwpKj2nA&sensor=false&libraries=places"></script>
<script src="{{asset('public/frontend/js/locationpicker.jquery.min.js')}}"></script>

 <script type="text/javascript">
            $('#a').locationpicker({
                location: {
                    latitude: 28.612912,
                    longitude: 77.2295097
                },
                radius: 300,
                inputBinding: {
                    latitudeInput: $('#lat_a'),
                    longitudeInput: $('#long_a'),
                    //radiusInput: $('#us3-radius'),
                    locationNameInput: $('#address_name_a')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
</script>


<script type="text/javascript">

$(document).ready(function(){
  
    $("#search").autocomplete({
        source: "{{ url('products/autocompleteajax') }}",
            focus: function( event, ui ) {
            //$( "#search" ).val( ui.item.title ); // uncomment this line if you want to select value to search box
            return false;
        },
        select: function( event, ui ) {
            window.location.href = ui.item.url;
        }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
       // var inner_html = '<a href="' + item.url + '" ><div class="list_item_container"><div class="label"><h4><b>' + item.title + '</b></h4></div></div></a>';
       var inner_html ='<div class="cart_item"><div class="item_img"><img src='+item.image+' alt="product"></div><div class="item_info"><h4>'+item.title+'</h4> <span>Mfr: '+item.manufacturer_name+'</span><div class="prodct_price_blk"><h3><i class="fa fa-inr"></i> '+item.sale_price+'</h3> <span class="strike"><i class="fa fa-inr"></i> '+item.mrp+'</span> <small>'+item.discount+'% Off</small></div></div><div class="item_qty"> <i class="fa fa-medkit"></i></div></div>';
        return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append(inner_html)
                .appendTo( ul );
    };
});
</script>

 <script type="text/javascript">

var addtocart_route="{{URL::to('add-to-cart')}}";
var update_cart="{{URL::to('update-cart')}}";
var clear_cart="{{URL::to('clear-cart')}}";
var redirect_url_login ="{{route('account.login')}}";
var wish_list="{{URL::to('add-to-wishlist')}}";

 var token = '{{ csrf_token() }}';

 </script>

 <script type="text/javascript">
 $(".col-auto").click(function(){
    var token_view = '{{ csrf_token() }}';
    var id = '{{ Auth::guard('web')->user()->id  ?? '' }}' ; 
    //alert(id);
    $.ajax(
    {
        url: "view_all/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token_view,
        },
        success: function (){
           // toastr['success']("Delete Successfully", "Delete");
           window.location.reload();

        }
    });
  }); 
</script>

      @yield('footer_scripts')
</body>
</html>




<script>
function myAndroid() {
    if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){ window.location.href = 'https://itunes.apple.com/my/app/flipbizz/idexampleapp'; }

    if(navigator.userAgent.toLowerCase().indexOf("android") > -1){ window.location.href = 'https://play.google.com/store/apps/details?id=com.exampleapp'; }

    //Update #2
    if (!navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
         window.location.href = 'http://play.google.com/store/apps/details?id=com.truecaller&hl=en'; //Desktop Browser
    }
}
</script>

<script>
function myIphone() {
    if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){ window.location.href = 'https://itunes.apple.com/my/app/flipbizz/idexampleapp'; }

    if(navigator.userAgent.toLowerCase().indexOf("android") > -1){ window.location.href = 'https://play.google.com/store/apps/details?id=com.exampleapp'; }

    //Update #2
    if (!navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
         window.location.href = 'https://itunes.apple.com/my/app/flipbizz/idexampleapp'; //Desktop Browser
    }
}
</script>

<script type="text/javascript">

var select_address ="{{route('user.select-address')}}";
var select_patient ="{{route('user.select-patient')}}";
var delivery_address_check ="{{route('delivery_address_check')}}";
 var token = '{{ csrf_token() }}';

 $(document).on('click', '.update_address', function() {
            selected_id= $(this).attr("data-id");
         event.preventDefault();
          $.ajax({
            url:  select_address,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
                toastr[response.status]("Your delivery addresses updated successfully", "Cart");
                // $("#select_address").load(document.URL + " #select_address");
                  window.location.reload();
            }
            });
 });

$(document).on('click', '.update_info_patient', function() {
            selected_id= $(this).attr("data-id");
           // alert(selected_id)
        // event.preventDefault();
          $.ajax({
            url:  select_patient,
            type: "POST",
            data: {'_token': token,selected_id:selected_id},
            dataType: 'json',
            success: function(response) {
                // $("#select_address").load(document.URL + " #select_address");
                  window.location.reload();
            }
            });
 });

 $(document).on('click', '.delivery_address_check', function() {
         event.preventDefault();
          $.ajax({
            url:  delivery_address_check,
            type: "POST",
            data: {'_token': token},
            dataType: 'json',
            success: function(response) {
                if(response.redirect_url){
                    window.setTimeout(function(){window.location.href = response.redirect_url;},300);
                }
                toastr[response.status](response.message,response.status);
                $(".main_error").html('<strong><i class="fa fa-exclamation-triangle"></i> '+response.message+'</strong> <i class="fa fa-times close" aria-hidden="true"></i>').removeClass('alert-success').addClass('alert-danger').show();            }
            });
 });
 $('body').on('click', '.close', function(){
        $(".main_error").html('').removeClass('alert-danger').hide();
     });
</script>

<script type="text/javascript">
   var delivery ="{{route('delivery-location.get-details.post')}}";

var token       = '{{ csrf_token() }}';
$('#loading').hide();
   $(document).ready(function(){
    $(document).on('keyup', '.getdetails', function()
    {
      //  alert('okkkk')
    var length =  $("#pin_code").val().length;
      if (length == 6) {
          $('select[name="name"]').empty();
           var pin_code        = $("#pin_code").val();
           // alert(pin_code)
           $.ajax({
               url         : delivery,
               type        : 'POST',
               data        : {'_token': token,pin_code:pin_code},
               dataType    : 'json',
               beforeSend: function() {
                 $('#loading').show();
               },
               success     : function(data) 
               {

                  $('select[name="name"]').empty();
                  $('#state').val(data.state);
                  $('#city').val(data.city);
                 $(data.AllState).each(function(index, el) {
                     $('select[name="name"]').append('<option>'+ el +'</option>');
                  });


                },
               complete: function(){
                $('#loading').hide();
               },
               error       : function(response)
               {
                $('#error').html('<div class="alert alert-danger" role="alert">The requested resource is not found</div>');
               }
           });
         }  

    });  
   });  

</script>

 <script type="text/javascript">
   /*===========================*/
 $(".deleteRecord").click(function(){
    var id = $(this).data("id");
    var token = '{{ csrf_token() }}';
    //alert(id)
    $.ajax(
    {
        url: "deletePatientAddress/"+id,
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
            toastr['success']("Delete Successfully", "Delete");
            window.location.reload();

        }
    });
   
});


$('.owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})
 $( ".owl-prev").html('<i class="ti-angle-left"></i>');
 $( ".owl-next").html('<i class="ti-angle-right"></i>');

$(document).ready(function(){ 
    $('.tab-a').click(function(){  
      $(".tab").removeClass('tab-active');
      $(".tab[data-id='"+$(this).attr('data-id')+"']").addClass("tab-active");
      $(".tab-a").removeClass('active-a');
      $(this).parent().find(".tab-a").addClass('active-a');
     });
});

</script>

