@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

  <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/line-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/slick.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css"/>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Legal Information</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row" style="padding: 50px;">
              <!-- {!! $legal_information_data->title !!} -->
                <div class="col-sm-12">
                    <div>
                        <div class="row">
                         {!! $legal_information_data->description !!}
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
 



@stop
@section('footer_scripts')

 <script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/custom.js')}}"></script> 

@stop
