@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

<link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/line-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/slick.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css"/>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Help</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                <div class="col-sm-12">
                    <div id="parentVerticalTab" class="resp-vtabs hor_1" style="display: block; width: 100%; margin: 0px;">
                        <div class="row">
                            <div class="col-sm-3">
                                <ul class="resp-tabs-list hor_1" style="margin-top: 3px;">
                                   @foreach($faq_topics as $key => $value)
                                    <li class="resp-tab-item hor_1" id="dddd" aria-controls="hor_1_tab_item-{{$key}}" role="tab" style="background-color: white; border-color: rgb(193, 193, 193);">{{$value->name}} <i class="arrow ti-angle-right"></i></li>
                                    @endforeach

                                   
                                </ul>
                            </div>
                        <div class="col-sm-9">
                            <div class="resp-tabs-container hor_1" style="border-color: rgb(193, 193, 193); background-color: #f3f5f9;">
                                <h2 class="resp-accordion hor_1 resp-tab-active" role="tab" aria-controls="hor_1_tab_item-0" style="background-color: white; border-color: rgb(193, 193, 193);"><span class="resp-arrow"></span>Order Enquiry <i class="arrow ti-angle-right"></i></h2>
                              
                                <?php $url_value =  "<script> 
                                document.writeln(location.href.toString().substring(location.href.toString().indexOf('#') + 1)); </script>";
                                echo '&nbsp;' ; 
                                ?>
                                @foreach($faq_topics as $key => $value)
                                @if($url_value == "parentVerticalTab".$key)
                                {{--   @if(!empty($key == 0))--}}
                                
                                <div class="resp-tab-content hor_1 resp-tab-content-active" aria-labelledby="hor_1_tab_item-{{$key+1}}" style="display:block;">
                                @else
                               
                                <div class="resp-tab-content hor_1 resp-tab-content-active" aria-labelledby="hor_1_tab_item-{{$key+1}}" style="display:none;">
                                @endif


                            <!--    <div class="resp-tab-content hor_1 resp-tab-content-active" aria-labelledby="hor_1_tab_item-{{$key+1}}" 
                                 style="{{ ($key == 0) ? 'display:block' : 'display:none' }}"> -->
                                <div class="faq_content" >
                                    
                                    <h2>{{$value->name}}</h2>
                                   
                                    <div id="accordion{{$key+1}}">
                                      
                                     @foreach($value->faqData as $keys => $values)
                                      <div class="card">
                                        <div class="card-header" id="heading{{$key+1}}">
                                            <a href="#accordion{{$key+1}}" data-toggle="collapse" data-target="#collapse{{$key+1}}{{$keys+1}}" aria-expanded="false" aria-controls="collapse{{$key+1}}{{$keys+1}}">{{$values->question}}</a>
                                        </div>
                                        <div id="collapse{{$key+1}}{{$keys+1}}" class="collapse" aria-labelledby="heading{{$key+1}}" data-parent="#accordion{{$key+1}}" >
                                          <div class="card-body">
                                            <p>{!!$values->answer!!}</p>
                                          </div>
                                        </div>
                                      </div> 
                                      @endforeach 
                                    </div>
                                    
                                </div>
                            </div>
                            @endforeach
                           
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12 text-center mt_20"><a href="#still_need_help" class="cmn_btn">Still Need Help</a></div>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 



@stop
@section('footer_scripts')

 <script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/custom.js')}}"></script> 
 <script type="text/javascript">
   $(document).ready(function() {
    //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });

   jQuery(function ($) {
    var $active = $('#accordion .panel-collapse.in').prev().addClass('active');
    $active.find('a').prepend('<i class="glyphicon glyphicon-minus"></i>');
    $('#accordion .panel-heading').not($active).find('a').prepend('<i class="glyphicon glyphicon-plus"></i>');
    $('#accordion').on('show.bs.collapse', function (e) {
        $('#accordion .panel-heading.active').removeClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
        $(e.target).prev().addClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    })
});
 </script>

@stop
