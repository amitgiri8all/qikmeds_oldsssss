@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 

<style type="text/css">
   .add_fav_pro{ width: 30px; height: 30px; background: #f3f5f9; border-radius: 100%; line-height: 32px; text-align: center;  right: 8px; top: 8px; transition: all 0.3s ease-in-out;}
.add_fav_pro a{ display: block; color: #888; transition: all 0.3s ease-in-out;}
.add_fav_pro a i{ transition: all 0.3s ease-in-out;}
.add_fav_pro:hover{ background: #24aeb1;}
.add_fav_pro:hover i{ color: #fff;}

</style>
  
<style type="text/css">
 
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}
.tabbable-panel .tab-menu ul{
    margin: 0;
    padding: 0;
}
.tabbable-panel .tab-menu ul li {
    padding: 0px;
    display: block;
    width: 100%;
}
.tabbable-panel .tab-menu ul li a {
    text-align: center;
    padding: 15px;
    width: 100%;
    display: inline-block;
    border-radius: 3px;
}
.tabbable-panel .tab-menu ul li a.tab-a.active-a {
    color: #fff;
    background-color: #24aeb1;
}
.tabbable-panel .tab{
    display: none;
}
.tabbable-panel .tab-active{
    display: block;
}
.tabbable-panel .tab-menu .owl-nav {
    position: absolute;
    left: 0;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    width: 100%;
}
.tabbable-panel .tab-menu .owl-nav button.owl-prev {
    position: absolute;
    left: -50px !important;
    font-size: 18px !important;
    height: 40px;
    width: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    top: -20px;
    border-radius: 50%;
    background-color: #fff;
    color: #303a7a;
}
.tabbable-panel .tab-menu .owl-nav button.owl-next{
    position: absolute;
    right: -50px !important;
    font-size:18px !important;
    height: 40px;
    width: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    top: -20px;
    border-radius: 50%;
    background-color: #fff;
    color: #303a7a;
}
.tabbable-panel .tab-menu ul li a:hover{color:#000;}
</style>
@stop


{{-- Page content --}}
@section('content')

 <section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a  href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a  href="{{route('home')}}">Wellness</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$productDetail->medicine_name}} - {{$productDetail->type_of_sell}}</li>
              </ol>
            </nav>
        </div>
        <div class="product_detail_blk">
            <div class="product_detail_top cmn_blk">
                <div class="row">
                    <div class="col-sm-6">
            <div class="product_detail_img">
              <div class="slider slider-nav">
                @foreach($product_images_data as $keys => $values)
                <div>
                  @if(!empty($values->image))
                  <div class="detail_img"><img style="width: 300px; height: 150px;" src="{{asset('storage/app/public/qikmeds/'.$values->image)}}"  alt="product"></div>
                  </div>
                  @else
                  <div class="detail_img"><img style="width: 300px; height: 150px;" src="{{asset('/public/assets/images/no-image.jpg')}}"  alt="product"></div>

                  @endif
                @endforeach
<!-- 
                <div>
                  <div class="detail_img"><img src="{{asset('public/frontend/images/prodct-2.jpg')}}" alt="product"></div>
                </div>

                <div>
                  <div class="detail_img"><img src="{{asset('public/frontend/images/prodct-3.jpg')}}" alt="product"></div>
                </div>

                <div>
                  <div class="detail_img"><img src="{{asset('public/frontend/images/prodct-4.jpg')}}" alt="product"></div>
                </div>

                <div>
                  <div class="detail_img"><img src="{{asset('public/frontend/images/prodct-5.jpg')}}" alt="product"></div>
                </div> -->

              </div>



              <div class="slider slider-for">

                @foreach($product_images_data as $keys => $values)
                <?php 
                if(!empty($values->image)){
                    $url= asset('storage/app/public/qikmeds/'.$values->image);
                   }else{
                    $url= asset('/public/assets/images/no-image.jpg');
                   } 
                 ?>  
                <div>
                  <div class="detail_img"><img src="{{$url}}" alt="product">
                  </div>
                </div>
                @endforeach

                
              </div>
            </div>
          </div>
                    <div class="col-sm-6">
                        <div class="product_detail_txt">
                            <h2>{{$productDetail->medicine_name}} - {{$productDetail->packaging}}</h2>
                            <div class="categry">
                                <small>{{$productDetail->uses}}</small>
                                @if(!empty($productDetail->category_name))<small>{{$productDetail->category_name}}</small>@endif
                            </div>
                            <div class="rating_blk">
                                <ul>
                                    <li><a href="#" class="active" tabindex="-1"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active" tabindex="-1"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active" tabindex="-1"><i class="las la-star"></i></a></li>
                                    <li><a href="#" class="active" tabindex="-1"><i class="las la-star"></i></a></li>
                                    <li><a href="#" tabindex="-1"><i class="lar la-star"></i></a></li>
                                    <li><span>(34 Ratings)</span></li>
                                </ul>
                            </div>
                            <div class="prodct_price_blk">
                                <h3>₹ {{$productDetail->mrp}}</h3>

                                @if(!empty($productDetail->sale_price))<span class="strike">₹ {{$productDetail->sale_price}}</span> @endif

                                @if(!empty($productDetail->discount))<small>{{ substr($productDetail->discount, 0 , 3 )}}% Off</small>@endif

                            </div>
                            <div class="tax">(Inclusive of all taxes)</div>
                            <div class="prodct_info">
                                <ul>
                                    <li><span>Manufacturer:</span> {{$productDetail->manufacturer_name ?? '' }}</li>
                                    <li><span>Country of origin:</span> India</li>
                                </ul>
                            </div>
                            <div class="prodct_highlits">
                                <h3>Product Highlights</h3>
                                <ul>
                                    <li>{!!$productDetail->packaging??''!!}</li>
                                    <li>{!!$productDetail->packing_type??''!!}</li>
                                    <li>{!!$productDetail->usage??''!!}</li>
                           
                                </ul>
                            </div>
                            @php $qtyval =  App\Models\Product::CheckQty($productDetail->id); @endphp
                    <div id="ref{{$productDetail->id}}">
                            <div class="add_cart">
                             @if($qtyval<=0)
                                <button type="submit"  data-id="{{$productDetail->id}}" class="cmn_btn addToCart btnCart{{$productDetail->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart </button>
                             @else 
                                <div class="cart-table__quantity input-number number{{$productDetail->id}}" style="float:left; margin-left: inherit;">
                                    <input class="form-control input-number__input qty{{$productDetail->id}}" name="quantity" type="number" id="number{{$productDetail->id}}" min="1" value="{{$qtyval}}">
                                    <div value="Decrease Value" data-id="{{$productDetail->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue('{{$productDetail->id}}')"></div>
                                    <div value="Increase Value" data-id="{{$productDetail->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue('{{$productDetail->id}}')" ></div>
                                </div>
                            @endif    
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                               @if(Auth::guard('web')->user())
                                @php $check_favorites = App\Models\Wishlist::check_favorites($productDetail->id); @endphp  

                               <div id="heart{{$productDetail->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav_pro add_to_wishlist" data-id="{{$productDetail->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav_pro not_login"><i class="ti ti-heart"></i></a></div>
                                @endif

                                <!-- <a href="#"><i class="ti ti-heart"></i></a> -->
                            </div>
                        </div>
                            <div class="offer_info" style="display: none;">
                                <h3>Offers Applicable</h3>
                                <div class="offer_detail">
                                    <i class="fas fa-ticket-alt"></i>
                                    <h4>QMS40KK <span>Save 20%</span></h4>
                                    <p>Flat 20% off + 20% cashback on your 1st medicine order. T&C Apply</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          

<div class="container pt-lg-4 pt-3">
   <div class="row">
      <div class="col-md-12">
          <div class="tabbable-panel">
            <div class="tabbable-line">
                    <div class="tab-menu row px-5 mb-3">
                      <ul class="owl-carousel owl-theme resp-tabs-list col-lg-7">
                         <li><a href="javascript:void(0);" class="tab-a active-a" data-id="usage">Usage</a></li>
                    @if($productDetail->about_salt!='')
                         <li><a href="javascript:void(0);" class="tab-a" data-id="about_salt">About Salt</a></li>
                    @endif
                    
                    @if($productDetail->mechanism_of_action!='')
                             <li><a href="javascript:void(0);" class="tab-a" data-id="mechanism_of_action">Mechanism of Action</a></li>
                    @endif 
                    
                    @if($productDetail->pharmacokinets!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="pharmacokinets">Pharmacokinets</a></li>
                    @endif 

                    @if($productDetail->onset_of_action!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="onset_of_action">Onset of Action</a></li>
                    @endif

                    @if($productDetail->duration_of_action!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="duration_of_action">Duration of action</a></li>
                    @endif 
                          
                    @if($productDetail->half_life!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="half_life">Half Life</a></li>
                    @endif 
                    
                    @if($productDetail->side_effects!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="side_effects">Side Effects</a></li>
                    @endif

                    @if($productDetail->contra_indications!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="contra_indications">Contra Indications</a></li>
                    @endif 
                    
                    @if($productDetail->special_precautions_while_taking!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="special_precautions_while_taking">Special Precautions While Taking</a></li>
                    @endif 
                    
                    @if($productDetail->pregnancy_related_Information!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="pregnancy_related_Information">Pregnancy related information</a></li>
                    @endif 

                    @if($productDetail->product_and_alcohol_interaction!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="product_and_alcohol_interaction">Product and alcohol interaction</a></li>
                    @endif 
                           
                     @if($productDetail->old_age_related_information!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="old_age_related_information">Old age related information</a></li>
                    @endif 
                    
                    @if($productDetail->old_age_related_information!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="old_age_related_information">Old age related information</a></li>
                    @endif 
                    
                    @if($productDetail->breast_feeding_related_information!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="breast_feeding_related_information">Breast feeding related information</a></li>
                    @endif  
                    @if($productDetail->children_related_information!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="children_related_information">Children related information</a></li>
                    @endif 
                          
                    @if($productDetail->indications!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="indications">Indications</a></li>
                    @endif 
                    
                    @if($productDetail->typical_dosage!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="typical_dosage">Typical Dosage</a></li>
                    @endif 
                          
                    @if($productDetail->storage_requirements!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="storage_requirements">Storage Requirements</a></li>
                    @endif 
                                  
                    @if($productDetail->fffects_of_missed_dosage!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="fffects_of_missed_dosage">effects of missed dosage</a></li>
                    @endif 
                    @if($productDetail->effects_of_overdose!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="effects_of_overdose">effects of overdose</a></li>
                    @endif 
                    
                    @if($productDetail->expert_advice!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="expert_advice">Expert Advice</a></li>
                    @endif 
                           
                    @if($productDetail->how_to_use!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="how_to_use">How to use</a></li>
                    @endif   

                     @if($productDetail->faqs!='')
                        <li><a href="javascript:void(0);" class="tab-a" data-id="faqs">Faqs</a></li>
                    @endif 
                          
                    
                        
                      </ul>
                   </div>
                   <div  class="tab tab-active" data-id="usage">
                      <p>{{$productDetail->usage}}</p>                    
                   </div>
                   <div  class="tab " data-id="about_salt">
                      <p>{{$productDetail->about_salt}}</p>
                   </div>
                   <div  class="tab " data-id="mechanism_of_action">
                      <p>{{$productDetail->mechanism_of_action}}</p>     
                   </div>  
                    <div  class="tab " data-id="onset_of_action">
                      <p>{{$productDetail->onset_of_action}}</p>     
                   </div>    
                   <div  class="tab " data-id="duration_of_action">
                      <p>{{$productDetail->duration_of_action}}</p>     
                   </div> 
                    <div  class="tab " data-id="side_effects">
                      <p>{{$productDetail->side_effects}}</p>     
                   </div> 
                   <div  class="tab " data-id="contra_indications">
                      <p>{{$productDetail->contra_indications}}</p>     
                   </div>  
                   <div  class="tab " data-id="special_precautions_while_taking">
                      <p>{{$productDetail->special_precautions_while_taking}}</p>     
                   </div> 
                   <div  class="tab " data-id="pregnancy_related_Information">
                      <p>{{$productDetail->pregnancy_related_Information}}</p>     
                   </div>  
                   <div  class="tab " data-id="product_and_alcohol_interaction">
                      <p>{{$productDetail->product_and_alcohol_interaction}}</p>     
                   </div>  
                    <div  class="tab " data-id="old_age_related_information">
                      <p>{{$productDetail->old_age_related_information}}</p>     
                   </div> 
                   <div  class="tab " data-id="breast_feeding_related_information">
                      <p>{{$productDetail->breast_feeding_related_information}}</p>     
                   </div>  
                   <div  class="tab " data-id="children_related_information">
                      <p>{{$productDetail->children_related_information}}</p>     
                   </div>   
                   <div  class="tab " data-id="indications">
                      <p>{{$productDetail->indications}}</p>     
                   </div>
                   <div  class="tab " data-id="typical_dosage">
                      <p>{{$productDetail->typical_dosage}}</p>     
                   </div>   
                   <div  class="tab " data-id="storage_requirements">
                      <p>{{$productDetail->storage_requirements}}</p>     
                   </div> 

                   <div  class="tab " data-id="fffects_of_missed_dosage">
                      <p>{{$productDetail->fffects_of_missed_dosage}}</p>     
                   </div>   
                   <div  class="tab " data-id="effects_of_overdose">
                      <p>{{$productDetail->effects_of_overdose}}</p>     
                   </div>
                    <div  class="tab " data-id="how_to_use">
                      <p>{{$productDetail->how_to_use}}</p>     
                   </div>  
                   <div  class="tab " data-id="faqs">
                      <p>{{$productDetail->faqs}}</p>     
                   </div> 
                            
            </div>
         </div>
      </div>
   </div>
</div>


          

<div class="container" style="display:none;">
   <div class="row">
      <div class="col-md-12">
          <div class="tabbable-panel">
            <div class="tabbable-line">
               <ul class="nav nav-tabs ">


                  <li class="active">
                     <a href="#tab_default_1" data-toggle="tab" class="active">
                     Description </a>
                  </li>
                  <li>
                     <a href="#tab_default_2" data-toggle="tab">
                     Key Benefits </a>
                  </li>   

                   <li>
                     <a href="#tab_default_3" data-toggle="tab">
                     Directions For Use </a>
                  </li>  
                  <li>
                     <a href="#tab_default_4" data-toggle="tab">
                     Safety Information </a>
                  </li>  
                   <li>
                     <a href="#tab_default_5" data-toggle="tab">
                     Other Information </a>
                  </li> 
               </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_default_1">
                     <p>
                       {!!$productDetail->description??''!!}
                     </p>
                  
                  </div>
                  <div class="tab-pane" id="tab_default_2">
                     
                     <p>
                       {!!$productDetail->key_benefits??''!!}
                     </p>
                  </div>
                  <div class="tab-pane" id="tab_default_3">
                     <p>
                       {!!$productDetail->directions_for_use??''!!}
                     </p>
                  </div>
 <div class="tab-pane" id="tab_default_4">
                     
                     <p>
                            {!!$productDetail->safety_information??''!!}

                     </p>
                  </div>
 <div class="tab-pane" id="tab_default_5">
                     <p>
                       {!!$productDetail->other_information??''!!}
                     </p>
                  </div>

               </div>


               
            </div>
         </div>
      </div>
   </div>
</div>





 
        </div>
        @if(!empty ($related_products))
<div class="deal_blk cmn_blk">
                        <div class="title_blk">
                            <h2>Related Products</h2>
                        </div>
                        <div class="product_blk">
                            <div class="slider autoplay-7">
                            @foreach($related_products as $key=>$value)
                              @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp
                                <div>
                        <div class="product_card">
                            <div class="product_img"><a href="{{url('/product-details/'.$value->slug)}}"><img style="width: 300px; height: 150px;" src="{{$value->image}}" alt="product"></a></div>
                            <div class="product_info">


                                @if(Auth::guard('web')->user())
                                @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  

                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav_pro add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav_pro not_login"><i class="ti ti-heart"></i></a></div>
                                @endif

                                
                                <!-- <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div> -->


                                <small>{!! substr($value->uses,0,10)!!} - {!!substr($value->type_of_sell,0,30)!!}</small>
                                <h3><a href="{{url('/product-details/'.$value->slug)}}">{!! substr($value->medicine_name,0,20)!!}</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                <h4>Rs {{$value->mrp}} @if(!empty($value->sales_price))<span class="strike">Rs 400</span>@endif</h4>
                                </div>

                                    <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                                    @else 

                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>   
                               </form>
                            </div>
                        </div>
                    </div>
                            @endforeach

                            </div>
                        </div>
                    </div>
                </div>
    </div>
    @endif
</section>


@stop


{{-- page level scripts --}}
@section('footer_scripts')


 <script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>

 

  <script type="text/javascript">
      $(document).on('click', '.addToCart', function() {
            location.reload(); 
        });
  </script>
@stop


