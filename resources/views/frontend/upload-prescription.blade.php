@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

{{-- <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 --}} <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/js-offcanvas.css')}}" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .input-numbers { display:block; width:100px; margin-bottom: 13px; margin-left: auto; position:relative;}
</style>
<style type="text/css">
    .c-offcanvas--right {
  height: 100%;
  width: 29em !important;
  right: 0;
  transform: translate3d(400px, 0, 0);
}
a.js-offcanvas-trigger.c-button.cmn_btn {
    text-align: -webkit-center;
    width: 21em;
    margin-bottom: 1em;
}
 
</style>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Attach Prescription</li>
              </ol>
            </nav>
        </div>
        <div class="cart_blk">
            <div class="row">
                <div class="col-sm-9">
                  <!--   <div class="payment_blk">
                        <h3>Attach Prescription</h3>
                        <div class="pay_wallet_blk upload_pre">
                            <ul>
                                <li>
                                    <h4>Upload</h4>
                                    <p>Please upload images of valid prescription from your doctor</p>
                                    <a href="#" data-toggle="modal" data-target="#prescription">Valid Prescription?</a>
                                    <div class="file-upload">
                                        <div class="image-upload-wrap">
                                            <input class="file-upload-input" type="file" onchange="readURL(this);" accept="image/*">
                                            <div class="drag-text">
                                                <i class="las la-file-prescription"></i>
                                                <h3>Upload and Drag Prescription</h3>
                                            </div>
                                          </div>
                                          <div class="file-upload-content">
                                            <img class="file-upload-image" src="#" alt="your image">
                                            <div class="image-title-wrap">
                                              <button type="button" onclick="removeUpload()" class="remove-image">Remove - <span class="image-title">Uploaded Image</span></button>
                                            </div>
                                          </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> -->

                <div class="drop-zone">
               <form method="post" id="zone" action="{{route('save-upload-prescription')}}" enctype="multipart/form-data" class="ajax_form">   
                @csrf      
                    <div class="col-sm-6" style="display:block;">
                    <div class="user_txt">
                       <label>Image</label>
                          <input type="file"  name="image" id="file"  onchange="loadFile(event)" class="form-control">  
                        </div>
                                           
                    </div> 
                     <div class="action_btns" style="width:250px;">
                          <button type="submit"  class="cmn_btn">Upload</button>
                        </div> 
                    </form> 
        </div> 
                </div>
                <div class="col-sm-3">
                    <div class="order_smry_blk">
                        <div class="order_price_blk">
                            <h2>Order Info</h2>
                            <div class="pay_wallet_blk upload_info">
                                <p>Select how do you want to proceed with the order</p>
                                <ul>
                                    <li>
                                        <i class="las la-first-aid"></i>
                                        <h4>Search and Add medicines</h4>
                                        <p>Manually search and add medicines in the cart</p>
                                        <div class="radio_btn">
                                            <input type="radio" name="radio" checked="checked" id="r-1">
                                            <label for="r-1">&nbsp;</label>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="las la-phone"></i>
                                        <h4>Get call from Qikmeds</h4>
                                        <p>Qikmeds pharmacist/doctors will call to confirm medicines</p>
                                        <div class="radio_btn">
                                            <input type="radio" name="radio" id="r-2">
                                            <label for="r-2">&nbsp;</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="action_btns">
                                <button type="submit" class="cmn_btn"><a href="{{ route('home')}}"> Continue</a></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
 
 

@stop
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script>
 
var loadFile = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};
</script>

@stop
