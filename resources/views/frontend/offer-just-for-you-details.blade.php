@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

  <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/line-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/slick.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css"/>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{route('offer-just-for-you-list')}}">offer just for you list</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$offer_banner->title}}</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
                <div class="col-sm-12">
                      <div class="col-sm-12">
                      <img class="card-img-top" width="100%" height= "35%"; src="<?php echo $offer_banner->image;?>" alt="Card image cap">
                    <div class="card" style="width: 80rem; margin:10px;">
                      <div class="card-body">
                        <h5 class="carsd-titlse" style="float:right;">Code: {{$offer_banner->code}}</h5>
                        <h5 class="card-title">{{$offer_banner->title}}</h5>
                        <p class="card-text">{{$offer_banner->sub_title}}</p>
                        <p class="card-text">{!!$offer_banner->description!!}</p>
                        <h4>Description</h4>
                        <p class="card-text">{!!$offer_banner->description!!}</p>
                        <h4>Eligibility</h4>
                        <p class="card-text">{!!$offer_banner->eligibility!!}</p>
                        <h4>How to get it</h4>
                        <p class="card-text">{!!$offer_banner->how_get_it!!}</p>
                        <h4>Condition offer</h4>
                        <p class="card-text">{!!$offer_banner->condition_offer!!}</p>
                        <h4>Term Condition</h4>
                        <p class="card-text">{!!$offer_banner->term_condition!!}</p>
                        <br>
                        
                      </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>
 

@stop
@section('footer_scripts')

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
 <script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/custom.js')}}"></script>
 
 
@stop
