@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
{{--   <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  --}}<style type="text/css">
 
</style>
@stop
{{-- Page content --}}
@section('content')
 

 <section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a  href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a  href="{{route('home')}}">{{ucwords(str_replace('-',' ',$pageTitle))}}</a></li>
               </ol>
            </nav>
        </div>
        <div class="product_detail_blk">
            <?php
            //echo "<pre>"; print_r($getPageData); die('=============ok');
            ?>
                <h1 class="title-big text-center section-title-style2">
                <span >
                {{ $getPageData->title ?? '' }}
                </span>
                </h1>
               <p>{!! $getPageData->description  ?? ''!!}</p>
          
        </div>
 
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
@stop


