@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

  <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/line-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/slick.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css"/>

@stop

{{-- Page content --}}
@section('content')
<section class="middle_section p_30">
    <div class="container">
        <div class="breadcrumb_blk">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Offer just for you</li>
              </ol>
            </nav>
        </div>
        <div class="product_listing_blk">
            <div class="row">
              @foreach($offer_banner  as $key=>$val)
                <div class="col-sm-6">
                    <div class="card" style="width: 36rem; margin:10px;">
                      <img class="card-img-top" src="<?php echo $val->image;?>" alt="Card image cap">
                      <div class="card-body">
                        <h5 class="carsd-titlse" style="float:right;">Code: {{$val->code}}</h5>
                        <h5 class="card-title">{{$val->title}}</h5>
                        <p class="card-text">{{$val->sub_title}}</p>
                        <br>
                        <a href="{{url('/offer-just-for-you-details/'.$val->slug)}}" class="btn btn-primary">View more detais...</a>
                      </div>
                    </div>
                </div>

              @endforeach


            </div>
        </div>
    </div>
</section>
 



@stop
@section('footer_scripts')

<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
 <script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
  <script src="{{asset('public/frontend/js/slick.js')}}"></script>
  <script src="{{asset('public/frontend/js/custom.js')}}"></script>
 
 
@stop
