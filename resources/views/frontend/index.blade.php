@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
{{--   <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  --}}<style type="text/css">
 
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="main_navigation">
    <div class="container">
        <div class="explore_menus">
            <ul>
                 @foreach(App\Helpers\ProductHelper::Frontcontain(1) as $key=>$val)
                    <li>
                         <i><img src='{{$val->image_icon}}'  alt="{{$val->title}}" width="40" height="40" style="border-radius: 50%;"></i> {!!substr($val->title,0,18)!!}
                        <small>{!!substr($val->short_description,0,30)!!}</small>
                  </li>
                @endforeach  
            </ul>
        </div>
    </div>
</section>

<section class="banner_blk">
    <button class="prev slick-arrow"><i class="ti-angle-left"></i></button>
    <div class="banner_slider">
        <div class="slider single-item">
             @foreach($banner as $key=>$val)
                <div>
                <a href="{{route('offer-just-for-you-list')}}"><img class="img-fluid" style="width: 1920px; height:550px;" src='{{$val->image}}' alt="banner">
                    </a>
               </div>
                @endforeach 
          
        </div>
    </div>
    <button class="next slick-arrow"><i class="ti-angle-right"></i></button>
</section>

<section class="middle_section">
    <div class="container">
        <div class="feature_blk">
            <ul>
                @foreach(App\Helpers\ProductHelper::Frontcontain(2) as $key=>$val)
                    <li>
                         <i><img src='{{$val->image_icon}}'  alt="{{$val->title}}" width="40" height="40" style="border-radius: 50%;"></i> {!!substr($val->title,0,18)!!}
                        <small>{!!substr($val->short_description,0,30)!!}</small>
                  </li>
                @endforeach  
               
            </ul>
        </div>
         <div class="deal_blk cmn_blk">
            <div class="title_blk">
                <h2>Deal Of The Day</h2>
                <span class="timer" id="demo"> </span>
            </div>
            <div class="product_blk">
                <div class="slider autoplay">
                  @foreach(App\Helpers\ProductHelper::ProductListByType(1) as $key=>$value)
                    @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp
                     <div>
                        @if($value->prescription_required=='Yes')   
                        <img src="{{asset('storage/app/public/rx-icon.png')}}" alt="rx" class="rxicon">
                        @endif
                        <div class="product_card numbersss{{$value->id}}">
                            <div class="product_img"><a href="{{url('/product-details/'.$value->slug)}}"><img style="width: 300px; height:150px;" src="{{$value->image}}" alt="product"></a></div>
                            <div class="product_info">
                                @if(Auth::guard('web')->user())
                                @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  

                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav not_login"><i class="ti ti-heart"></i></a></div>
                                @endif
                                <small>{!! substr($value->uses,0,10)!!} - {!!substr($value->type_of_sell,0,30)!!}</small>
                                <h3><a href="{{url('/product-details/'.$value->slug)}}">{{$value->medicine_name}}</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                <h4>Rs {{$value->mrp}} @if(!empty($value->sales_price))<span class="strike">Rs 400</span>@endif</h4>
                                </div>
                                <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                                    @else 

                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>    
                              </form>
                            </div>
                        </div>
                    </div>
                  @endforeach
                 </div>
            </div>
        </div>
         <div class="offers_blk">
            <div class="title_blk">
                <h2>Offers Just For You</h2>
            </div>
            <div class="offer_content">
                <div class="slider autoplay-2">
                @foreach($offer_banner as $key=>$val)    
                    <div>
                        <div class="offer_img"><a href="{{route('offer-just-for-you-list')}}"><img src='<?php echo $val->image;?>'  alt="{{$val->title}}"></a></div>
                    </div>
                @endforeach    
                </div>
            </div>
        </div>

        <div class="category_blk cmn_blk">
            <div class="title_blk">
                <h2>Popular Categories</h2>
            </div>
            <div class="category_content">
                <div class="slider autoplay-1">
                    @foreach(App\Helpers\ProductHelper::category(10) as $category)
                  
                     <div>
                        <div class="category_info">
                            <div class="category_img"><a href="{{url('/list/'.$category->slug)}}"><img src='<?php echo $category->image;?>' width="200" height="200" alt="{{$category->category_name}}"></a></div>
                            <div class="category_name"><a href="{{url('/list/'.$category->slug)}}">{{$category->category_name}}</a></div>
                        </div>
                    </div>
                    @endforeach
                 </div>
            </div>
        </div>

        <div class="deal_blk cmn_blk">
            <div class="title_blk">
                <h2>Health Products</h2>
            </div>

             <div class="product_blk">
                <div class="slider autoplay">
                  @foreach(App\Helpers\ProductHelper::ProductListByType(2) as $key=>$value)
                    @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp
                     <div>
                        @if($value->prescription_required=='Yes')   
                        <img src="{{asset('storage/app/public/rx-icon.png')}}" alt="rx" class="rxicon">
                        @endif
                        <div class="product_card numbersss{{$value->id}}">
                            <div class="product_img"><a href="{{url('/product-details/'.$value->slug)}}"><img style="width: 300px; height:150px;" src="{{$value->image}}" alt="product"></a></div>
                            <div class="product_info">
                                @if(Auth::guard('web')->user())
                                                    @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  

                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav not_login"><i class="ti ti-heart"></i></a></div>
                                @endif
                                <small>{!!substr($value->uses,0,30)!!} - {!!substr($value->type_of_sell,0,30)!!}</small>
                                <h3><a href="{{url('/product-details/'.$value->slug)}}">{{$value->medicine_name}} </a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                <h4>Rs {{$value->mrp}} @if(!empty($value->sales_price))<span class="strike">Rs 400</span>@endif</h4>
                                </div>{{-- 
                                <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                                    @else 

                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>--}}    
                              </form>
                            </div>
                        </div>
                    </div>
                  @endforeach
                 </div>
            </div>

        </div>
 

        <div class="blog_blk cmn_blk">
            <div class="title_blk">
                <h2>From Our Blog</h2>
            </div>
            <div class="blog_content">
                <div class="slider autoplay-5">
                @foreach($blog as $key=>$val)
                    <div>
                       
                        <div class="blog_info">
                            <div class="blog_img"><a href="javascript:void(0)"><img style="width: 317px; height:159px;" src="<?php echo $val->image; ?>" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: {{date("d-MM-Y",strtotime($val->created_at));}}</small>
                                <h3>{{$val->title;}}</h3>
                                <a href="{{url('/blog-details/'.$val->slug)}}"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>

        <div class="achivemnt_blk cmn_blk">
            <div class="container">
                <div class="row">

                   

                 
                  @foreach(App\Helpers\ProductHelper::Frontcontain(10) as $key=>$val)
                 
                   <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src='{{$val->image_icon}}'  alt="{{$val->title}}" width="40" height="40" style="border-radius: 50%;"></i>
                            <h2>{!!substr($val->title,0,18)!!}</h2>
                            <h3>{!!substr($val->short_description,0,30)!!}</h3>
                        </div>
                    </div>
                @endforeach 

                </div>
            </div>
        </div>

        <div class="testimonial_blk cmn_blk">
           <div class="container">
  
            <div class="title_blk">
                <h2>What Our Customers Say</h2>
            </div>
            <div class="testimonial_content">

                <div class="slider autoplay-6">

<?php
//echo "<pre>"; print_r($testimonials); die();
?>
                    @foreach($testimonials as $key=>$val)
                        <div class="testimonial_info" style="min-height:315px;">
                            <div class="rating_blk">
                                <ul>
<?php
for($i=1;$i<=5;$i++) {
$selected = "";
if(!empty($val["rating"]) && $i<=$val["rating"]) {
$selected = "active";
}
?>
<li><a href="javascript:void(0)" class='<?php echo $selected; ?>'><i class="las la-star"></i></a></li>

<?php }  ?>

                                </ul>
                            </div>
                            <p>{!!$val->description!!}</p>
                            <div class="profile_box">
                       
                                <div class="profile_img"><img src="{{$val->image}}"></div>
                                <div class="profile_name">
                                    <h3>{{$val->name;}}</h3>
                                    <span>{{date("d-M-Y",strtotime($val->created_at));}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
         </div>
        </div>
   <form class="ajax_form" method="post" id="newsletter" action="{{route('newsletters')}}">
        <div class="newsletter_blk">
            <div class="newsletter_content">
                <h2>Sign Up For Newsletter</h2>
                <p>Join 60.000+ Subscribers and get a new<br> discount coupon on every Saturday</p>


                <div class="newsletter_form">

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="text" class="form-control" name="email" placeholder="Enter your email address">
                    <button type="submit" value="subscribe">Subscribe</button>
                </div>
            </div>
        </div>
  </form>
    </div>
</section>



@stop


{{-- page level scripts --}}
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
@stop
