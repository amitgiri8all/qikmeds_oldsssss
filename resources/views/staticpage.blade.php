@extends('frontend.layouts.default')

{{-- Page title --}}
@section('title')
    Home ::Quikmeds
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
 <link href="{{asset('public/frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
{{--   <link href="{{asset('public/frontend/css/easy-responsive-tabs.css')}}" rel="stylesheet" type="text/css"/>
  --}}<style type="text/css">
 
</style>
@stop
{{-- Page content --}}
@section('content')
<section class="main_navigation">
    <div class="container">
        <div class="explore_menus">
            <ul>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-1.png')}}" alt="icon"></i> Medical
                        <small>Over 25000 Products</small>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-2.png')}}" alt="icon"></i> Wellness
                        <small>Health Products</small>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-3.png')}}" alt="icon"></i> Health Corner
                        <small>Trending from Health Experts</small>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i><img src="{{asset('public/frontend/images/icon-4.png')}}" alt="icon"></i> Others
                        <small>More Info</small>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="banner_blk">
    <button class="prev slick-arrow"><i class="ti-angle-left"></i></button>
    <div class="banner_slider">
        <div class="slider single-item">
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-1.jpg')}}" alt="banner">
            </div>
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-2.jpg')}}" alt="banner">
            </div>
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-3.jpg')}}" alt="banner">
            </div>
            <div>
                <img class="img-fluid" src="{{asset('public/frontend/images/banner-4.jpg')}}" alt="banner">
            </div>
        </div>
    </div>
    <button class="next slick-arrow"><i class="ti-angle-right"></i></button>
</section>

<section class="middle_section">
    <div class="container">
        <div class="feature_blk">
            <ul>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-1.jpg')}}" alt="icon"></i> Free Delivery
                    <small>For all orders over $120</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-2.jpg')}}" alt="icon"></i> Safe Payment
                    <small>100% secure payment</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-3.jpg')}}" alt="icon"></i> Shop With Confidence
                    <small>If goods have problems</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-4.jpg')}}" alt="icon"></i> 24/7 Help Center
                    <small>Dedicated 24/7 support</small>
                </li>
                <li>
                    <i><img src="{{asset('public/frontend/images/feature-5.jpg')}}" alt="icon"></i> Friendly Service
                    <small>30 day satisfaction guarantee</small>
                </li>
            </ul>
        </div>
         <div class="deal_blk cmn_blk">
            <div class="title_blk">
                <h2>Deal Of The Day</h2>
                <span class="timer" id="demo"></span>
            </div>
            <div class="product_blk">
                <div class="slider autoplay">
                  @foreach($productDetail as $key=>$value)
                    @php $qtyval =  App\Models\Product::CheckQty($value->id); @endphp
                     <div>
                        <div class="product_card numbersss{{$value->id}}">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-1.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                @if(Auth::guard('web')->user())
                                                    @php $check_favorites = App\Models\Wishlist::check_favorites($value->id); @endphp  

                               <div id="heart{{$value->id}}">
                                <div @if($check_favorites==0) style="background: #24aeb1;" @endif class="add_fav add_to_wishlist" data-id="{{$value->id}}"><a href="javascript:void(0)" ><i     @if($check_favorites==0) style="color: #fbfbfb;" @endif class="ti ti-heart"></i></a></div>
                                </div> 
                                @else
                                <div class="add_fav not_login"><i class="ti ti-heart"></i></a></div>
                                @endif
                                <small>{{$value->uses}} - {{$value->type_of_sell}}</small>
                                <h3><a href="#">{{$value->medicine_name}}</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                <h4>Rs {{$value->mrp}} @if(!empty($value->sales_price))<span class="strike">Rs 400</span>@endif</h4>
                                </div>
                                <div id="ref{{$value->id}}">
                                    @if($qtyval<=0)
                                    <button type="submit"  data-id="{{$value->id}}" class="addToCart btnCart{{$value->id}}"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                                    @else 

                                    <div class="cart-table__quantity input-number number{{$value->id}}" style="float:left;">
                                            <input class="form-control input-number__input qty{{$value->id}}" name="quantity" type="number" id="number{{$value->id}}" min="1" value="{{$qtyval}}">
                                            <div value="Decrease Value" data-id="{{$value->id}}" class="input-number__add update_cart" id="decrease"  onclick="increaseValue({{$value->id}})"></div>
                                            <div value="Increase Value" data-id="{{$value->id}}" class="input-number__sub update_cart" id="increase" onclick="decreaseValue({{$value->id}})" ></div>
                                        </div>
                                    @endif    
                                </div>    
                              </form>
                            </div>
                        </div>
                    </div>
                  @endforeach
                 </div>
            </div>
        </div>
         <div class="offers_blk">
            <div class="title_blk">
                <h2>Offers Just For You</h2>
            </div>
            <div class="offer_content">
                <div class="slider autoplay-2">
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-1.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-2.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-3.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-4.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-5.jpg')}}" alt="offer"></a></div>
                    </div>
                    <div>
                        <div class="offer_img"><a href="#"><img src="{{asset('public/frontend/images/offer-6.jpg')}}" alt="offer"></a></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="category_blk cmn_blk">
            <div class="title_blk">
                <h2>Popular Categories</h2>
            </div>
            <div class="category_content">
                <div class="slider autoplay-1">
                    @foreach($category as $category)
                  
                     <div>
                        <div class="category_info">
                            <div class="category_img"><a href="{{url('/list/'.$category->slug)}}"><img src='<?php echo $category->image;?>' width="100" height="180" alt="{{$category->category_name}}"></a></div>
                            <div class="category_name"><a href="{{url('/list/'.$category->slug)}}">{{$category->category_name}}</a></div>
                        </div>
                    </div>
                    @endforeach
                 </div>
            </div>
        </div>

        <div class="deal_blk cmn_blk">
            <div class="title_blk">
                <h2>Health Products</h2>
            </div>
            <div class="product_blk">
                <div class="slider autoplay-3">

                     <div>
                        <div class="product_card">
                            <div class="product_img"><a href="#"><img src="{{asset('public/frontend/images/product-1.jpg')}}" alt="product"></a></div>
                            <div class="product_info">
                                <div class="add_fav"><a href="#"><i class="ti ti-heart"></i></a></div>
                                <small>sdsd, sdsd</small>
                                <h3><a href="#">dsdsds</a></h3>
                                <div class="rating_blk">
                                    <ul>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#" class="active"><i class="las la-star"></i></a></li>
                                        <li><a href="#"><i class="lar la-star"></i></a></li>
                                    </ul>
                                </div>
                                <div class="price_blk">
                                <h4>Rs 55555 <span class="strike">Rs 55555</span></h4>
                                </div>
                                <button type="button"><i class="ti ti-shopping-cart"></i> Add To Cart</button>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>

        <div class="brand_blk">
            <div class="title_blk">
                <h2>Featured Brands</h2>
            </div>
            <div class="brand_content">
                <div class="slider autoplay-4">
                  @foreach($brandlist as $brand)
                   
                    <div>
                        <div class="brand_info">
                            <div class="brand_img"><a href="#"><img src='<?php echo $brand->image;?>' width="100" height="180" alt="brand"></a></div>
                            <div class="brand_txt">
                                <h3><a href="#">{{$brand->brand_name}}</a></h3>
                            </div>
                        </div>
                    </div>
                   @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="blog_blk cmn_blk">
            <div class="title_blk">
                <h2>From Our Blog</h2>
            </div>
            <div class="blog_content">
                <div class="slider autoplay-5">
                @foreach($blog as $key=>$val)
                    <div>
                       
                        <div class="blog_info">
                            <div class="blog_img"><a href="javascript:void(0)"><img src="<?php echo $val->image; ?>" alt="blog"></a></div>
                            <div class="blog_txt">
                                <small>Posted on: {{date("d-MM-Y",strtotime($val->created_at));}}</small>
                                <h3>{{$val->title;}}</h3>
                                <a href="javascript:void(0)"><i class="ti ti-plus"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>

        <div class="achivemnt_blk cmn_blk">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-1.jpg')}}" alt="blog"></i>
                            <h2>20 Lakh +</h2>
                            <h3>Families Served</h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-2.jpg')}}" alt="blog"></i>
                            <h2>50 Lakh +</h2>
                            <h3>Orders Delivered</h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-3.jpg')}}" alt="blog"></i>
                            <h2>10000 +</h2>
                            <h3>Pincodes Served</h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="achivemnt_txt">
                            <i><img src="{{asset('public/frontend/images/achive-4.jpg')}}" alt="blog"></i>
                            <h2>1 Lakh +</h2>
                            <h3>Medicines Available</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="testimonial_blk cmn_blk">
            <div class="title_blk">
                <h2>What Our Customers Say</h2>
            </div>
            <div class="testimonial_content">

                <div class="slider autoplay-6">

                    @foreach($testimonials as $key=>$val)
                        <div class="testimonial_info">
                            <div class="rating_blk">
                                <ul>
<?php
for($i=1;$i<=5;$i++) {
$selected = "";
if(!empty($val["rating"]) && $i<=$val["rating"]) {
$selected = "active";
}
?>
<li><a href="javascript:void(0)" class='<?php echo $selected; ?>'><i class="las la-star"></i></a></li>

<?php }  ?>

                                </ul>
                            </div>
                            <p>{!!$val->description!!}</p>
                            <div class="profile_box">
                       
                                <div class="profile_img"><img src="{{$val->imagel}}"></div>
                                <div class="profile_name">
                                    <h3>{{$val->name;}}</h3>
                                    <span>{{date("d-MM-Y",strtotime($val->created_at));}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
   <form class="ajax_form" method="post" id="newsletter" action="{{route('newsletters')}}">
        <div class="newsletter_blk">
            <div class="newsletter_content">
                <h2>Sign Up For Newsletter</h2>
                <p>Join 60.000+ Subscribers and get a new<br> discount coupon on every Saturday</p>


                <div class="newsletter_form">

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="text" class="form-control" name="email" placeholder="Enter your email address">
                    <button type="submit" value="subscribe">Subscribe</button>
                </div>
            </div>
        </div>
  </form>
    </div>
</section>



@stop


{{-- page level scripts --}}
@section('footer_scripts')

<script src="{{asset('public/frontend/js/jquery.form.js')}}"></script>..
 <script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
 <script src="{{asset('public/frontend/js/number.js')}}"></script>
 <script src="{{asset('public/frontend/js/cart.js')}}"></script>
 <script src="{{asset('public/frontend/js/easyResponsiveTabs.js')}}"></script>
@stop


